@echo off
if not "%~1"=="__reinvoked" (
    start cmd /k "%~f0" __reinvoked %*
    exit /b
)

@echo Starting Coyo in the command line ... please wait a few seconds for Coyo to start. You may close this command line to stop coyo again.
play/play run coyo -Duser.timezone=Etc/UTC
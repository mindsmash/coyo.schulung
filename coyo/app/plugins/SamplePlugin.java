package plugins;

import models.Settings;
import play.mvc.Http.Request;

@LoadPlugin
public class SamplePlugin extends PluginBase {

	@Override
	public String getKey() {
		return "samplePlugin";
	}

	@Override
	public String getName() {
		return "Sample Plugin";
	}

	// hook into the application
	@PluginHook(value = PluginHooks.AFTER_SIDEBAR, actions = "ActivityStream.index")
	public void sidebarWidget() {
		String text = getSettings().getString("text");

		// hand data to the template
		renderArgs.put("text", text);

		// render the template
		render("sidebarWidget.html");
	}

	// render the form for the plugin settings
	@Override
	public void renderSettings() {
		String text = getSettings().getString("text");

		// hand data to the template
		renderArgs.put("text", text);

		// render the template
		render("settings.html");
	}

	// save the submitted data from the settings form
	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("text", request.params.get("text"));
		settings.save();
	}
}

package models.app;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import play.data.validation.Required;
import apps.SampleAppDescriptor;

@Entity
@Table(name = "app_sample")
public class SampleApp extends App {

	// custom fields

	@Lob
	@Required
	public String text;

	@Required
	public String color;

	public static Map<String, String> getColors() {
		Map<String, String> colors = new HashMap<String, String>();
		colors.put("#000", "Black");
		colors.put("#999", "Grey");
		colors.put("#F00", "Red");
		return colors;
	}
	
	@Override
	public String getAppKey() {
		return SampleAppDescriptor.KEY;
	}
}

package apps;

import models.app.App;
import models.app.SampleApp;

@LoadApp
public class SampleAppDescriptor extends AppDescriptor {

	public static final String KEY = "sampleApp";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		// use this to localize
		// return Messages.get("app.sample.title");
		return "Sample App";
	}

	@Override
	public String getDescription() {
		return "This is just a sample app";
	}

	@Override
	public String getIconClass() {
		// icons from http://twitter.github.com/bootstrap/base-css.html#icons
		return "icon-heart";
	}

	@Override
	public Class<? extends App> getAppModel() {
		return SampleApp.class;
	}
}

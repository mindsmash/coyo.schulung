package jobs;

import models.AbstractTenant;
import models.Settings;
import models.Tenant;
import models.User;
import play.Logger;
import play.Play;
import play.jobs.OnApplicationStart;
import plugins.PluginBase;
import plugins.PluginManager;
import apps.AppDescriptor;
import apps.AppManager;
import conf.ApplicationSettings;

/**
 * <p>
 * This job loads initial data into the development db upon application startup.
 * </p>
 * <p>
 * Job is named with a "Z" in front so it is loaded after all major Coyo jobs.
 * </p>
 * 
 * @author mindsmash GmbH
 */
@OnApplicationStart
public class ZBootstrapDevelopment extends MultiTenantJob {

	@Override
	public void doJob() throws Exception {
		if (Play.mode.isDev() && User.count() == 0) {
			Tenant tenant = new Tenant();
			tenant.active = true;
			tenant.save();
			super.doJob();
		}
	}

	public final void doTenantJob(AbstractTenant tenant) {
		// check if the database is empty
		if (Play.mode.isDev() && User.count() == 0) {

			// clear data
			if (Play.getFile("data/attachments") != null) {
				play.test.Fixtures.deleteDirectory("data/attachments");
			}

			// clear indices
			if (Play.getFile("data/search") != null) {
				play.test.Fixtures.deleteDirectory("data/search");
			}

			// load default data
			play.test.Fixtures.loadModels("initial-data.yml");

			Logger.info("development user login is now: user@coyoapp.com, PW: password");
			Logger.info("development user login is now: admin@coyoapp.com, PW: password");

			// settings
			Settings settings = Settings.findApplicationSettings();
			settings.setProperty(ApplicationSettings.DOWNLOADS, "true");
			settings.setProperty(ApplicationSettings.REGISTRATION, "true");
			settings.setProperty(ApplicationSettings.WORKSPACES, "true");
			settings.setProperty(ApplicationSettings.EMBEDDABLES, "true");
			settings.setProperty(ApplicationSettings.APPLICATION_NAME, "Coyo Developer Edition");
			settings.save();

			// activate all plugins and apps
			for (PluginBase plugin : PluginManager.getPlugins()) {
				PluginManager.activate(plugin);
			}
			for (AppDescriptor app : AppManager.getApps()) {
				AppManager.toggle(app, "page", true);
				AppManager.toggle(app, "workspace", true);
			}

			// deactivate tour plugin
			PluginManager.deactivate(PluginManager.getPlugin("tour"));
		}
	}
}

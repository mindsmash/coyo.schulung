package multitenancy;

import models.AbstractTenant;
import play.mvc.Http.Request;

/**
 * Extend this model in your application to define the logic for how an active
 * tenant is determined (e.g. via current domain).
 * 
 * Define the classpath of your implementation in your application.conf using
 * the parameter "mindsmash.multitenancy.tenantresolver.class".
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public interface TenantResolver {

	public AbstractTenant getActiveTenant(Request request);
}

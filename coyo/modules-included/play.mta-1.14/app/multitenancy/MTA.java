package multitenancy;

import injection.Inject;
import injection.InjectionSupport;

import java.util.List;

import models.AbstractTenant;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.Filter;
import org.hibernate.Session;

import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Router;
import util.Util;

/**
 * MTA helper class.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@InjectionSupport
public class MTA {

	public static final String CONF_TENANT_RESOLVER_KEY = "";
	public static final String CONF_EXCLUDE_KEY = "mindsmash.multitenancy.exclude";
	public static final String CONF_REDIRECT_URL_KEY = "mindsmash.multitenancy.redirect.url";
	public static final String CONF_REDIRECT_ACTION_KEY = "mindsmash.multitenancy.redirect.action";

	@Inject(configuration = "mindsmash.multitenancy.tenantresolver.class")
	private static TenantResolver resolver;

	private static List<String> excludes;

	/**
	 * Can be used to force an active tenant for the current thread.
	 */
	protected static ThreadLocal<AbstractTenant> forceActiveTenant = new ThreadLocal<AbstractTenant>();

	/**
	 * MTA can be forced to be inactive.
	 */
	protected static ThreadLocal<Boolean> forceDeactiveMultitanency = new ThreadLocal<Boolean>();

	private MTA() {
		// hide
	}

	/**
	 * Retrieve the active Tenant
	 * 
	 * @return
	 */
	public static AbstractTenant getActiveTenant() {
		if (forceActiveTenant.get() != null) {
			return forceActiveTenant.get();
		}
		if (Request.current() == null) {
			return null;
		}
		return resolver.getActiveTenant(Request.current());
	}

	/**
	 * Null safe get the active tenant.
	 * 
	 * @return
	 * @throws IllegalStateException
	 */
	public static AbstractTenant nullSafeGetActiveTenant() {
		AbstractTenant tenant = getActiveTenant();
		if (null != tenant) {
			return tenant;
		}
		throw new IllegalStateException("Expecting tenant to be resolveable");
	}

	/**
	 * Get the excluded action strings.
	 * 
	 * @return
	 */
	public static List<String> getExcludes() {
		if (excludes == null) {
			excludes = Util.parseList(Play.configuration.getProperty(CONF_EXCLUDE_KEY), ",");
		}
		return excludes;
	}

	/**
	 * Check whether MTA is activated.
	 * 
	 * @return True if MTA is active
	 */
	public static boolean isMultitenancyActive() {
		boolean filterActive = isMultitenancyFilterActive();
		if (!isMultitenancyDeactivationForced() && !filterActive) {
			throw new IllegalStateException(
					"Multitenancy filter must not be deactivated without MTA beeing forced inactive");
		}
		return filterActive;
	}

	/**
	 * Check whether MTA filter is set.
	 * 
	 * @return
	 */
	protected static boolean isMultitenancyFilterActive() {
		return ((Session) JPA.em().getDelegate()).getEnabledFilter("tenant") != null;
	}

	/**
	 * Activates MTA but only if an active and valid tenant is found.
	 * 
	 * @return True if a valid tenant was found and activated.
	 */
	public static boolean activateMultitenancy() {
		Logger.trace("activating multitenancy");

		// TODO : cache active tenant for session
		AbstractTenant current = MTA.getActiveTenant();

		// prepare filter
		Filter filter = ((Session) JPA.em().getDelegate()).enableFilter("tenant");
		filter.setParameter("tenant_id", 0L);

		// load filter params
		if (current != null) {
			Logger.trace("found active tenant [%s]", current);
			forceDeactiveMultitanency.set(false);
			filter.setParameter("tenant_id", current.id);
			return true;
		}

		String action = "norequest";
		if (Request.current() != null) {
			action = Request.current().action;
		}
		Logger.warn("trying to activate [%s] multitenancy without an active tenant", action);

		if (play.Logger.isDebugEnabled()) {
			try {
				throw new IllegalStateException("DEBUG Multitenancy without an active tenant");
			} catch (IllegalStateException ex) {
				Logger.debug(ex.getLocalizedMessage(), ex);
				if (Request.current() != null) {
					Logger.debug(ToStringBuilder.reflectionToString(Http.Request.current(), ToStringStyle.MULTI_LINE_STYLE));
				}
			}
		}

		return false;
	}

	/**
	 * Activates MTA with a forced tenant.
	 * 
	 * @param forcedTenant
	 * @return True if a valid tenant was found and activated.
	 */
	public static boolean activateMultitenancy(AbstractTenant forcedTenant) {
		forceActiveTenant.set(forcedTenant);
		return activateMultitenancy();
	}

	/**
	 * Cleares the forced tenant and re-activates MTA with the normal tenant
	 * (from tenant resolver).
	 * 
	 * @return True if a valid tenant was found and activated.
	 */
	public static boolean clearForcedTenant() {
		forceActiveTenant.remove();
		return activateMultitenancy();
	}

	/**
	 * Deactivates MTA.
	 */
	public static void forceMultitenancyDeactivation() {
		if (Logger.isDebugEnabled()) {
			Logger.debug("forcing MTA inactive");
		}

		if (JPA.isInsideTransaction() && isMultitenancyFilterActive()) {
			((Session) JPA.em().getDelegate()).disableFilter("tenant");
		}
		forceDeactiveMultitanency.set(true);
	}

	/**
	 * True when MTA is forced inactive.
	 * 
	 * @return
	 */
	protected static boolean isMultitenancyDeactivationForced() {
		return null != forceDeactiveMultitanency.get() && forceDeactiveMultitanency.get();
	}

	/**
	 * Get the redirect URL to the default page.
	 * 
	 * @return
	 */
	public static String getRedirectUrl() {
		if (Play.configuration.containsKey(CONF_REDIRECT_ACTION_KEY)) {
			return Router.reverse(Play.configuration.getProperty(CONF_REDIRECT_ACTION_KEY)).url;
		}
		return Play.configuration.getProperty(CONF_REDIRECT_URL_KEY);
	}

	public static void clearThreadLocals() {
		forceActiveTenant.remove();
		forceDeactiveMultitanency.remove();
	}
}

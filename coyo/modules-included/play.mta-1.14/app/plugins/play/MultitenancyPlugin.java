package plugins.play;

import java.lang.reflect.Method;

import multitenancy.MTA;
import multitenancy.MultitenancyExclude;
import multitenancy.TenantResolver;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.mvc.Http.Request;
import play.mvc.results.Redirect;
import util.Util;

/**
 * This plugin is responsible for activating multitenancy before each action
 * invocation. It calls your implementaion of the {@link TenantResolver} to
 * determine the active tenant.
 * 
 * You can exclude certain controllers or actions from multitenancy by either
 * annotate the action or controller with {@link MultitenancyExclude} or by
 * adding a comma separated list of "Controller.action" names in your
 * application.conf using the parameter "mindsmash.multitenancy.exclude". You
 * can use "*" wildcards. e.g. "Users.*, System.index"
 * 
 * If an action is called and an active tenant cannot be found you can set the
 * application.conf parameter "mindsmash.multitenancy.redirect.url" to define an
 * URL to redirect to (e.g. your marketing or company website). Define
 * "mindsmash.multitenancy.redirect.action" to redirect to an action. (Be
 * careful with infinity redirects)
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class MultitenancyPlugin extends PlayPlugin {

	@Override
	public void beforeInvocation() {
		MTA.clearThreadLocals();
	}

	@Override
	public void beforeActionInvocation(Method actionMethod) {
		// check if current action is excluded by annotation
		if (actionMethod.isAnnotationPresent(MultitenancyExclude.class)
				|| actionMethod.getDeclaringClass().isAnnotationPresent(MultitenancyExclude.class)) {
			Logger.debug("current action [%s] is excluded from multitanency", Request.current().action);
			MTA.forceMultitenancyDeactivation();
			return;
		}

		// check if current action is excluded by configuration
		for (String exclude : MTA.getExcludes()) {
			if (Util.matches(Request.current().action, exclude)) {
				Logger.debug("current action [%s] is excluded from multitenancy", Request.current().action);
				MTA.forceMultitenancyDeactivation();
				return;
			}
		}

		// check if current action is the redirect destination
		if (Play.configuration.containsKey(MTA.CONF_REDIRECT_ACTION_KEY)) {
			if (Util.matches(Request.current().action, Play.configuration.getProperty(MTA.CONF_REDIRECT_ACTION_KEY))) {
				Logger.debug("current action [%s] is excluded from multitenancy", Request.current().action);
				MTA.forceMultitenancyDeactivation();
				return;
			}
		}

		// activate and check for error
		if (!MTA.activateMultitenancy()) {
			if (MTA.getRedirectUrl() != null) {
				throw new Redirect(MTA.getRedirectUrl());
			}
			throw new IllegalStateException("trying to invoke an action without an active tenant ["
					+ Request.current().action + "]");
		}
	}

	@Override
	public void afterInvocation() {
		MTA.clearThreadLocals();
	}
}

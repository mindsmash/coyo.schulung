package exceptions;

public class TenantException extends RuntimeException {

	/**
	 * Constructs an <code>TenantException</code> without a detail message.
	 */
	public TenantException() {
		super();
	}

	/**
	 * Constructs an <code>TenantException</code> with a detail message.
	 *
	 * @param s
	 *            the detail message.
	 */
	public TenantException(String s) {
		super(s);
	}
}

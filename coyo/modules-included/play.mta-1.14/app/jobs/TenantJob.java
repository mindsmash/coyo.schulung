package jobs;

import models.AbstractTenant;
import multitenancy.MTA;
import play.jobs.Job;

/**
 * A job that is executed in scope of the given tenant.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 * @param <V>
 */
public abstract class TenantJob<V> extends Job<V> {

	protected AbstractTenant tenant;

	public TenantJob(AbstractTenant tenant) {
		this.tenant = tenant;

	}

	@Override
	public final void doJob() throws Exception {
		try {
			MTA.activateMultitenancy(tenant);
			doTenantJob();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public final V doJobWithResult() throws Exception {
		try {
			MTA.activateMultitenancy(tenant);
			return doTenantJobWithResult();
		} catch (Exception e) {
			throw e;
		}
	}

	public V doTenantJobWithResult() throws Exception {
		doTenantJob();
		return null;
	}

	public void doTenantJob() throws Exception {
	}
}

package jobs;

import java.util.List;

import models.AbstractTenant;
import multitenancy.MTA;
import play.jobs.Job;

public abstract class MultiTenantJob extends Job {

	@Override
	public void doJob() throws Exception {
		try {
			List<AbstractTenant> tenants = AbstractTenant.findAll();
			for (AbstractTenant tenant : tenants) {
				MTA.activateMultitenancy(tenant);
				doTenantJob(tenant);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public abstract void doTenantJob(AbstractTenant tenant) throws Exception;
}

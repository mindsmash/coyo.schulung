package models;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import multitenancy.MTA;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import exceptions.TenantException;

/**
 * Parent model for all models that are subject to multitenancy.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@MappedSuperclass
@FilterDef(name = "tenant", parameters = @ParamDef(type = "long", name = "tenant_id"), defaultCondition = "tenant_id = :tenant_id")
@Filter(name = "tenant", condition = "tenant_id = :tenant_id")
public abstract class TenantModel extends BaseModel {

	@ManyToOne(optional = false)
	public AbstractTenant tenant;

	public void setTenant(AbstractTenant tenant) {
		if (this.tenant != null && tenant != this.tenant && MTA.isMultitenancyActive()) {
			throw new TenantException("changing an entity's [" + this + "] tenant is not allowed");
		}
		this.tenant = tenant;
	}

	@Override
	protected void beforeInsert() throws Exception {
		if (tenant == null) {
			tenant = MTA.getActiveTenant();
		}
		if (tenant == null) {
			throw new TenantException("trying to insert a tenant entity [" + this
					+ "] but no active tenant could be found");
		}
		super.beforeInsert();
	}

	@Override
	protected void beforeUpdate() throws Exception {
		checkTenant();
		super.beforeUpdate();
	}

	@Override
	protected void beforeDelete() throws Exception {
		checkTenant();
		super.beforeDelete();
	}

	@Override
	protected void afterSelect() throws Exception {
		checkTenant();
		super.afterSelect();
	}

	protected void checkTenant() throws IllegalAccessException {
		if (MTA.isMultitenancyActive()) {
			AbstractTenant active = MTA.getActiveTenant();
			if (!tenant.id.equals(active.id)) {
				throw new TenantException("trying to read/update/delete an entity [" + this + "] with tenant ["
						+ this.tenant + "] that does not belong to the active tenant [" + active + "]");
			}
		}
	}
}

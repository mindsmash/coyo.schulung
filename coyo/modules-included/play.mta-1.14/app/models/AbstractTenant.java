package models;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Base model for tenants in your multitenancy system. Simply have your tenant
 * model extend this model class.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "tenant")
public abstract class AbstractTenant extends BaseModel {
	
	abstract public boolean isActive();
}
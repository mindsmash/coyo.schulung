package events;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import play.Logger;

/**
 * 
 * Holds EventQueues that are identified by an ID.
 * 
 * @author Daniel Busch, Piers Wermbter mindsmash GmbH
 * 
 */
public class IdentifiedEventQueues {
	protected final ConcurrentHashMap<Long, EventQueue> queues = new ConcurrentHashMap<Long, EventQueue>();

	/**
	 * Returns the EventQueue for the given ID.
	 */
	protected EventQueue get(Long id) {
		return this.queues.get(id);
	}

	/**
	 * Returns if the EventQueue for the given ID exists.
	 */
	protected boolean exists(Long id) {
		return this.queues.containsKey(id);
	}

	/**
	 * Returns all EventQueues.
	 */
	public Map<Long, EventQueue> all() {
		return this.queues;
	}

	/**
	 * Closes the EventQueue for the given ID if existent.
	 */
	public void close(Long id) {
		if (exists(id)) {
			if (Logger.isDebugEnabled()) {
				Logger.debug("Closing EventStream for: %s", id);
			}

			this.queues.remove(id);
		}
	}

	/**
	 * Returns the EventQueue for the given ID. If not existent an EventQueue is
	 * created.
	 */
	public EventQueue getOrCreate(Long id) {
		if (!exists(id)) {

			if (Logger.isDebugEnabled()) {
				Logger.debug("[EventService] Opening EventStream for: %s", id);
			}

			EventQueue queue = new EventQueue();
			this.queues.putIfAbsent(id, queue);
		} else {
			if (Logger.isDebugEnabled()) {
				Logger.debug("[EventService] Alreay opened for: %s", id);
			}
		}
		return get(id);
	}

	/**
	 * Publishes an Event into the EventQueue for the given ID, if one is
	 * existent.
	 */
	public void publishIfExists(Long id, Event event) {
		if (this.exists(id)) {
			this.get(id).publish(event);

			if (Logger.isDebugEnabled()) {
				Logger.debug("[EventService] Publishing event: '%s' successfully for: %s", event.getType(), id);
			}
		} else {

			if (Logger.isDebugEnabled()) {
				Logger.debug("[EventService] Event: '%s' ignored for: %s no Eventstream opened", event.getType(), id);
			}
		}
	}
}

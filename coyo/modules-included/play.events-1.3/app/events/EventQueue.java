package events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ConcurrentSkipListMap;

import play.Play;
import play.libs.F.Promise;

/**
 * 
 * Holds a list of Events. The first Event that goes in goes first out.
 * 
 * @author Piers Wermbter, mindsmash GmbH
 * 
 */
public class EventQueue {
	final int maxSize = Integer.parseInt(Play.configuration.getProperty("mindsmash.events.maxQueueSize", "20"));
//	final ConcurrentLinkedDeque<Event> events = new ConcurrentLinkedDeque<Event>();
	final ConcurrentSkipListMap<Long, Event> events = new ConcurrentSkipListMap<>();
	final List<FilterTask> waiting = Collections.synchronizedList(new ArrayList<FilterTask>());

	/**
	 * 
	 * Returns a list of events that happened after the given ID. If the ID is
	 * equal or higher than the last ID in the queue a Future is returned.
	 * 
	 */
	public synchronized Promise<List<Event>> nextEvents(Long lastSeen) {
		if (events.size() == 0) {
			lastSeen = 0L;
		} else if (lastSeen > events.lastKey()) {
			lastSeen = (long) (events.lastKey());
		}
		FilterTask filter = new FilterTask(lastSeen);
		waiting.add(filter);
		notifyNewEvent();
		return filter;
	}

	/**
	 * 
	 * returns the ID of the newest Event in the queue.
	 * 
	 */
	public Long getLastEventId() {
		if (events.size() == 0) {
			return 0L;
		}
		return events.lastKey();
	}

	/**
	 * 
	 * Publishes an event into the queue.
	 * 
	 */
	public void publish(Event event) {
		if (events.size() >= maxSize) {
			// remove oldest event to make space
			events.remove(events.firstKey());
		}
		events.put(event.getId(), event);
		notifyNewEvent();
	}

	/**
	 * 
	 * Notifys all open futures about a new event.
	 * 
	 */
	private synchronized void notifyNewEvent() {
		synchronized (waiting) {
			for (ListIterator<FilterTask> it = waiting.listIterator(); it.hasNext();) {
				FilterTask filter = it.next();
				for (Event event : events.values()) {
					filter.propose(event);
				}
				if (filter.trigger()) {
					it.remove();
				}
			}
		}
	}

	class FilterTask extends Promise<List<Event>> {

		private List<Event> newEvents = new ArrayList<Event>();
		private final Long lastEventSeen;
		private boolean canceled = false;

		public FilterTask(Long lastEventSeen) {
			this.lastEventSeen = lastEventSeen;
		}

		public void propose(Event event) {
			if (null == lastEventSeen || (event.getId() > lastEventSeen)) {
				newEvents.add(event);
			}
		}

		public boolean trigger() {
			if (newEvents.isEmpty()) {
				return false;
			}
			invoke(newEvents);
			return true;
		}

		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			if (!isCancelled() && (!mayInterruptIfRunning || newEvents.isEmpty())) {
				newEvents = null;
				canceled = true;
				return waiting.remove(this);
			}
			return false;
		}

		@Override
		public boolean isCancelled() {
			return canceled;
		}
	}
}

package events;

import java.util.Map;

import play.libs.F.Promise;

/**
 * 
 * Manages all actions regarding to events and eventqueues.
 * 
 * @author Piers Wermbter, mindsmash GmbH
 * 
 */
abstract public class EventProvider {

	protected final IdentifiedEventQueues queues = new IdentifiedEventQueues();

	public static boolean init = false;

	public synchronized void initialize() {
		init = true;
	}

	/**
	 * 
	 * Returns the EventQueue for the given user, a queue will be created if
	 * none exists.
	 * 
	 */
	abstract public Promise<EventQueue> getOrCreateEventStream(Long id);

	/**
	 * 
	 * Publishes an event into the EventQueue of the given User.
	 * 
	 */
	public void publishEvent(Event event, Long id) {
		event.setId(getNextEventId());
		publishEventOverTransport(event, id);
	}

	abstract protected void publishEventOverTransport(Event event, Long id);

	abstract protected Long getNextEventId();

	public void closeQueue(Long id) {
		queues.close(id);
	}

	public Map<Long, EventQueue> getQueues() {
		return queues.all();
	}
}

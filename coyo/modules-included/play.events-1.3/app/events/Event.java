package events;

import java.io.Serializable;

/**
 * Events for the global EventService. Holds an Object and needs an ID. Events
 * can be categorized by different types.
 * 
 * @author pierswermbter
 * 
 */
public class Event implements Serializable {

	protected Long id;
	protected Object object;
	protected String type;

	public Event(String type) {
		setType(type);
	}

	public Event(String type, Object data) {
		this(type);
		setData(data);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getData() {
		return object;
	}

	public void setData(Object object) {
		this.object = object;
	}
}

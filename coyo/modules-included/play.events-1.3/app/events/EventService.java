package events;

import injection.Inject;
import injection.InjectionSupport;

import java.util.Map;

import play.libs.F.Promise;

/**
 * 
 * Global EventService injects the needed EventProvider.
 * 
 * @author Piers Wermbter, mindsmash GmbH
 * 
 */
@InjectionSupport
public class EventService {

	@Inject(configuration = "mindsmash.eventprovider.class", defaultClass = LocalEventProvider.class)
	protected static EventProvider instance;

	public static EventProvider getInstance() {
		if (!instance.init) {
			synchronized (instance) {
				if (!instance.init) {
					instance.initialize();
				}
			}

		}
		return instance;
	}

	/**
	 * 
	 * Closes the EventQueue for the given ID.
	 * 
	 */
	public static void closeQueue(Long id) {
		getInstance().closeQueue(id);
	}

	/**
	 * 
	 * Returns all open EventQueues.
	 * 
	 */
	public static Map<Long, EventQueue> getQueues() {
		return getInstance().getQueues();
	}

	/**
	 * Publishes an event for the given user.
	 */
	public static void publishEvent(Event event, Long id) {
		getInstance().publishEvent(event, id);
	}

	public synchronized static Promise<EventQueue> getOrCreateEventStream(Long id) {
		return getInstance().getOrCreateEventStream(id);
	}
}

package events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import play.libs.F.Promise;

/**
 * 
 * Holds a list of futures that are identified by an ID.
 * 
 * @author Daniel Busch, Piers Wermbter mindsmash GmbH
 * 
 */
public class AsyncTasks<T> {

	private final List<AsyncTask> waiting = Collections.synchronizedList(new ArrayList<AsyncTask>());

	/**
	 * Resolves all waiting taskes that are waiting for a object for the given
	 * ID.
	 */
	public void resolve(Long id, T object) {
		synchronized (waiting) {
			for (ListIterator<AsyncTask> it = waiting.listIterator(); it.hasNext();) {
				AsyncTask task = it.next();
				task.propose(id, object);
				if (task.trigger()) {
					it.remove();
				}
			}
		}
	}

	/**
	 * 
	 * Create a new task for the given ID and returns it.
	 */
	public AsyncTask defer(Long id) {
		AsyncTask task = new AsyncTask(id);
		waiting.add(task);
		return task;
	}

	public class AsyncTask extends Promise<T> {

		private boolean canceled = false;
		private Long id;
		private T object = null;

		public AsyncTask(Long id) {
			this.id = id;
		}

		public void propose(Long proposedId, T object) {
			if (id.equals(proposedId)) {
				this.object = object;
			}
		}

		public boolean trigger() {
			if (object == null) {
				return false;
			}
			invoke(object);
			return true;
		}

		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			if (!isCancelled() && (!mayInterruptIfRunning)) {
				canceled = true;
				return waiting.remove(this);
			}
			return false;
		}

		@Override
		public boolean isCancelled() {
			return canceled;
		}
	}
}

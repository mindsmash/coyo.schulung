package events;

import java.util.concurrent.atomic.AtomicLong;

import play.Logger;
import play.libs.F.Promise;

/**
 * 
 * Simple EventProvider for non multi server environments.
 * 
 * @author Piers Wermbter , mindsmash GmbH
 * 
 */
public class LocalEventProvider extends EventProvider {

	// thread save ID counter
	private static AtomicLong id = new AtomicLong(1);

	@Override
	public Promise<EventQueue> getOrCreateEventStream(Long id) {
		Promise<EventQueue> promise = new Promise<EventQueue>();
		promise.invoke(queues.getOrCreate(id));
		return promise;
	}

	@Override
	protected void publishEventOverTransport(Event event, Long id) {
		if (Logger.isDebugEnabled()) {
			Logger.debug("[EventService] Publishing Event: '%s' for: %s", event.getType(), id);
		}

		queues.publishIfExists(id, event);
	}

	@Override
	protected Long getNextEventId() {
		return id.incrementAndGet();
	}
}

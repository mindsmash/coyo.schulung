package license.v2;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import play.Play;
import play.exceptions.UnexpectedException;

public class KeyPairGenerator {

	public static void generate(int keysize) {
		try {
			String algo = "RSA";
			java.security.KeyPairGenerator pairgen = java.security.KeyPairGenerator.getInstance(algo);
			pairgen.initialize(keysize);
			KeyPair keyPair = pairgen.generateKeyPair();

			KeyFactory fact = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pub = fact.getKeySpec(keyPair.getPublic(), RSAPublicKeySpec.class);
			RSAPrivateKeySpec priv = fact.getKeySpec(keyPair.getPrivate(), RSAPrivateKeySpec.class);

			exportRSAPrivate(priv, new FileOutputStream(Play.getFile("conf/priv.key")));
			exportRSAPublic(pub, new FileOutputStream(Play.getFile("conf/pub.key")));
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	private static void exportRSAPublic(RSAPublicKeySpec key, OutputStream out) {
		String exp = key.getPublicExponent().toString(16);
		String mod = key.getModulus().toString(16);
		PrintStream printStream = new PrintStream(out);
		printStream.print(exp + "\n" + mod);
		printStream.close();
	}

	private static void exportRSAPrivate(RSAPrivateKeySpec key, OutputStream out) {
		String exp = key.getPrivateExponent().toString(16);
		String mod = key.getModulus().toString(16);
		PrintStream printStream = new PrintStream(out);
		printStream.print(exp + "\n" + mod);
		printStream.close();
	}
}

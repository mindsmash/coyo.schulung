package license.v2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import play.Logger;
import play.Play;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * A license key basically is no more than encrypted JSON. This class can be used to both encrypt or decrypt JSON.
 * However, you should only include the public key in your deliverables so that others cannot generate license keys.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class License {

	private static final String DECRYPT_KEY = Play.configuration.getProperty("license.decryptKey", "conf/decrypt.key");
	private static final String ENCRYPT_KEY = Play.configuration.getProperty("license.encryptKey", "conf/encrypt.key");

	/**
	 * Evaluates the content of a license by decrypting to JSON and then parsing the JSON to a {@link JsonObject}.
	 * 
	 * @param license
	 * @return Decrypted data or null if failed.
	 */
	public static JsonObject evaluate(String license) {
		try {
			String clean = decrypt(license).trim();
			return new JsonParser().parse(clean).getAsJsonObject();
		} catch (Exception e) {
			Logger.error(e, "[License] Error evaluating license [%s]", license);
			return null;
		}
	}

	/**
	 * Generates a license key from a {@link JsonObject}.
	 * 
	 * @param o
	 * @return License string
	 */
	public static String generate(JsonObject o) throws Exception {
		String json = new GsonBuilder().create().toJson(o);
		byte[] utf8 = json.getBytes("UTF8");

		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, importPrivateKey());
		// byte[] enc = cipher.doFinal(utf8);
		byte[] enc = blockRSA(cipher, utf8, Cipher.ENCRYPT_MODE);

		return new BASE64Encoder().encode(enc).replaceAll("\\n", "");
	}

	private static String decrypt(String license) throws Exception {
		byte[] dec = new BASE64Decoder().decodeBuffer(license);

		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, importPublicKey());
		// byte[] utf8 = cipher.doFinal(dec);
		byte[] utf8 = blockRSA(cipher, dec, Cipher.DECRYPT_MODE);

		return new String(utf8, "UTF8");
	}

	private static byte[] blockRSA(Cipher cipher, byte[] bytes, int mode) throws Exception {
		byte[] scrambled = new byte[0];
		byte[] result = new byte[0];

		// if we encrypt we use 100 byte long blocks. decryption requires 128 byte long blocks (because of RSA)
		int length = (mode == Cipher.ENCRYPT_MODE) ? 100 : 128;

		byte[] buffer = new byte[length];
		for (int i = 0; i < bytes.length; i++) {
			if ((i > 0) && (i % length == 0)) {
				scrambled = cipher.doFinal(buffer);
				result = append(result, scrambled);
				int newlength = length;

				// if newlength would be longer than remaining bytes in the bytes array we shorten it.
				if (i + length > bytes.length) {
					newlength = bytes.length - i;
				}

				// clean the buffer array
				buffer = new byte[newlength];
			}
			buffer[i % length] = bytes[i];
		}

		scrambled = cipher.doFinal(buffer);
		result = append(result, scrambled);

		return result;
	}

	private static byte[] append(byte[] prefix, byte[] suffix) {
		byte[] toReturn = new byte[prefix.length + suffix.length];
		for (int i = 0; i < prefix.length; i++) {
			toReturn[i] = prefix[i];
		}
		for (int i = 0; i < suffix.length; i++) {
			toReturn[i + prefix.length] = suffix[i];
		}
		return toReturn;
	}

	private static RSAPrivateKey importPrivateKey() throws Exception {
		File file = Play.getVirtualFile(ENCRYPT_KEY).getRealFile();
		assertFileExists(file);

		BufferedReader bufR = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

		String exp = bufR.readLine();
		String mod = bufR.readLine();
		bufR.close();

		RSAPrivateKeySpec privKeySpec = new RSAPrivateKeySpec(new BigInteger(mod, 16), new BigInteger(exp, 16));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return (RSAPrivateKey) keyFactory.generatePrivate(privKeySpec);
	}

	private static RSAPublicKey importPublicKey() throws Exception {
		File file = Play.getVirtualFile(DECRYPT_KEY).getRealFile();
		assertFileExists(file);

		BufferedReader bufR = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

		String exp = bufR.readLine();
		String mod = bufR.readLine();
		bufR.close();

		RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(new BigInteger(mod, 16), new BigInteger(exp, 16));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);
	}

	private static void assertFileExists(File file) throws FileNotFoundException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException("File not found: " + file.getAbsolutePath());
		}
	}
}

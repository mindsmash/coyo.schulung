package validation.checks;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import util.HttpUtil;
import validation.SimpleURLWithRelative;
import validation.SimpleURL;

/**
 * URL check that derives from {@linkplain SimpleURLCheck} but also allows relative URL
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class SimpleURLWithRelativeCheck extends AbstractAnnotationCheck<SimpleURLWithRelative> {

	@Override
	public void configure(SimpleURLWithRelative url) {
		setMessage("validation.url");
	}

	@Override
	public boolean isSatisfied(Object arg0, Object value, OValContext arg2, Validator arg3) throws OValException {
		if (value == null || value.toString().length() == 0) {
			return true;
		}
		String url = "" + value;
		return HttpUtil.isValidUrlString(url) && (HttpUtil.hasValidProtocol(url) || url.startsWith("/"));
	}
	
}

package validation.checks;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import util.HttpUtil;
import validation.SimpleURL;

public class SimpleURLCheck extends AbstractAnnotationCheck<SimpleURL> {

	@Override
	public void configure(SimpleURL url) {
		setMessage("validation.url");
	}

	@Override
	public boolean isSatisfied(Object arg0, Object value, OValContext arg2, Validator arg3) throws OValException {
		if (value == null || value.toString().length() == 0) {
			return true;
		}
		String url = "" + value;
		return HttpUtil.isValidUrlString(url) && HttpUtil.hasValidProtocol(url);
	}
	
}

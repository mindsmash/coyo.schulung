package validation.checks;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import validation.ColorCode;

public class ColorCodeCheck extends AbstractAnnotationCheck<ColorCode> {

	public static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";
	public static final String MESSAGE = "validation.colorCode";
	
	@Override
	public void configure(ColorCode cc) {
		setMessage(MESSAGE);
	}

	@Override
	public boolean isSatisfied(Object arg0, Object value, OValContext arg2, Validator arg3) throws OValException {
		if (value == null || value.toString().length() == 0) {
			return true;
		}
		String color = "" + value;
		return check(color);
	}

	public static boolean check(String color) {
		return color.matches(HEX_PATTERN);
	}
}

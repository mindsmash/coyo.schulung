package exceptions;

import play.i18n.Messages;

/**
 * The exception message is resolved by the exception error string via Play
 * messages and may very well be used as user feedback.
 * 
 * @author Jan Marquardt
 */
public class FlashException extends RuntimeException {

	private String message;

	public FlashException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return Messages.get(message);
	}

	@Override
	public String getLocalizedMessage() {
		return getMessage();
	}
}

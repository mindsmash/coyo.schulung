package controllers;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import play.Logger;
import play.Play;
import play.Play.Mode;
import play.mvc.Controller;

/**
 * Renders an application's javadoc in dev mode
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class Javadoc extends Controller {

	public static void index(String file, boolean gen) throws IOException, InterruptedException {
		if (Play.mode == Mode.PROD) {
			redirect("/");
		}

		// generate if no javadoc exists or older than one day
		File javadoc = Play.getFile("javadoc");
		if (gen || !javadoc.exists() || new Date(javadoc.lastModified()).before(DateUtils.addDays(new Date(), -1))) {
			Logger.debug("generating javadoc");

			Process p = Runtime.getRuntime().exec("play javadoc " + Play.getFile("").getAbsolutePath());
			if (p.waitFor() > 0) {
				Logger.error("could not generate javadoc");
			}
		}

		if (file == null) {
			file = "index.html";
		}
		renderTemplate("../../javadoc/" + file);
	}

	public static void redirect() throws IOException, InterruptedException {
		index(null, false);
	}
}

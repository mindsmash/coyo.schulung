package controllers;

import play.Logger;
import play.Play;
import play.mvc.Controller;
import theme.Theme;

public class ThemeResources extends Controller {

	/**
	 * Delivers a resource using Etag headers. See
	 * http://de.wikipedia.org/wiki/HTTP_ETag for details.
	 * 
	 * This is used with multiple themes that can be changed at runtime because
	 * the routes are static in Play and we cannot change them based on the
	 * active theme.
	 * 
	 * @param resource
	 */
	public static void load(String resource) {
		// allow plugins to intersect
		if (Play.pluginCollection.serveStatic(Theme.getVFResource(resource), request, response)) {
			return;
		}

		if (request.headers.containsKey("if-none-match")
				&& resource.equals(request.headers.get("if-none-match").value())) {
			notModified();
		}

		try {
			response.setHeader("Etag", resource);
			renderBinary(Theme.getResource(resource));
		} catch (Exception e) {
			Logger.warn(e, "[ThemeResources] Resource[%s] not found", resource);
		}

		notFound();
	}
}

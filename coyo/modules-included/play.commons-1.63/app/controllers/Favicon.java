package controllers;

import play.Play;
import play.mvc.Controller;
import play.vfs.VirtualFile;

public abstract class Favicon extends Controller {

	private static final String FAVICON_PATH = Play.configuration.getProperty("favicon.path",
			"public/images/favicon.ico");

	public static void favicon() {
		VirtualFile vf = Play.getVirtualFile(FAVICON_PATH);

		if (vf != null && vf.exists()) {
			renderBinary(vf.getRealFile());
		}

		notFound();
	}
}

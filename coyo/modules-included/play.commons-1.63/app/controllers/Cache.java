package controllers;

import java.util.Date;
import java.util.List;

import models.BaseModel;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import play.Logger;
import play.Play;
import play.i18n.Lang;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import play.utils.Utils;

public abstract class Cache extends Controller {

	public static boolean ACTIVE = Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.caching.active",
			"true"));

	public static final int CACHE_DURATION_SECONDS = 900; // 15min

	private static String getEntityValues(BaseModel entity) {
		return entity.getClass().getName() + "-" + entity.id + "-" + entity.modified.getTime();
	}

	private static String generateETag(BaseModel entity) {
		return generateETag(getEntityValues(entity));
	}

	private static String generateETag(String entityValues) {
		return Codec.hexMD5(Lang.get() + "-" + entityValues + "-" + request.url);
	}

	/**
	 * Caches any basemodel entity. This can be used to cache entity images or any other data related to an entity. The
	 * cache turns invalid when the entity is saved.
	 * 
	 * Call this method before you deliver your content.
	 * 
	 * @param entity
	 *            Entity to cache
	 */
	@Util
	public static void cacheEntity(BaseModel entity, String privateKey) {
		cacheEntity(entity, entity.modified, privateKey);
	}

	/**
	 * Caches any basemodel entity. This can be used to cache entity images or any other data related to an entity. The
	 * cache turns invalid when the entity is saved.
	 * 
	 * Call this method before you deliver your content.
	 * 
	 * @param entity
	 *            Entity to cache
	 */
	@Util
	public static void cacheEntity(BaseModel entity, Date modified, String privateKey) {
		if (entity == null) {
			return;
		}
		cache(generateETag(entity), modified.getTime(), privateKey);
	}

	/**
	 * Caches a list of entities based on both their generated checksum and on the modification date of the entity in
	 * the list that was last modified.
	 * 
	 * Call this method before you deliver your content.
	 * 
	 * @param entities
	 */
	@Util
	public static void cacheEntities(List<? extends BaseModel> entities, String privateKey) {
		if (entities == null || entities.size() == 0) {
			return;
		}

		Long lastModified = null;
		StringBuilder result = new StringBuilder();
		result.append(Lang.get()).append(",");

		for (BaseModel entity : entities) {
			if (lastModified == null || entity.modified.getTime() > lastModified) {
				lastModified = entity.modified.getTime();
			}

			result.append(getEntityValues(entity)).append(",");
		}

		cache(generateETag(result.toString()), lastModified, privateKey);
	}

	/**
	 * Activates HTTP caching based on the "Last-Modified" header. Use this caching method when you want to cache
	 * something that has a timestamp for its last modification.
	 * 
	 * Call this method before you deliver your content.
	 * 
	 * @param lastModified
	 */
	@Util
	public static void cache(long lastModified) {
		cache(null, lastModified, null);
	}

	/**
	 * Activates HTTP caching based on the "ETag" header. The etag usually is a checksum of some generated data and this
	 * HTTP caching method is therefore best suited for queries and list data.
	 * 
	 * Call this method before you deliver your content.
	 * 
	 * @param etag
	 */
	@Util
	public static void cache(String etag) {
		cache(etag, null, null);
	}

	/**
	 * Activates both etag and last modified HTTP caching. Caching will only kick in if both methods return a
	 * not-modified result.
	 * 
	 * @param etag
	 * @param lastModified
	 */
	@Util
	public static void cache(String etag, Long lastModified, String privateKey) {
		if (!ACTIVE) {
			return;
		}

		boolean modified = false;

		// set cache-control header
		if (privateKey != null) {
			response.setHeader("Cache-Control", "private, no-cache");
			etag = etag + "-" + privateKey;
		} else {
			response.setHeader("Cache-Control", "max-age=" + CACHE_DURATION_SECONDS);
			
			/* set expires header in future */
			DateTime expiration = new DateTime().plusSeconds(CACHE_DURATION_SECONDS);
			response.setHeader("Expires", Utils.getHttpDateFormatter().format(expiration.toDate()));
		}
		

		if (!StringUtils.isEmpty(etag)) {
			response.setHeader("ETag", etag);
			if (isModified(etag)) {
				modified = true;
			}
		}

		if (lastModified != null) {
			response.setHeader("Last-Modified", Utils.getHttpDateFormatter().format(new Date(lastModified)));
			if (isModified(lastModified)) {
				modified = true;
			}
		}

		if (!modified) {
			notModified();
		}
	}

	private static boolean isModified(String etag) {
		try {
			if (!request.headers.containsKey("if-none-match")) {
				return true;
			}
			if (StringUtils.isEmpty(etag)) {
				return true;
			}
			return !request.headers.get("if-none-match").value().equals(etag);
		} catch (Exception e) {
			Logger.warn(e, "[Cache] Error checking etag");
		}
		return true;
	}

	private static boolean isModified(long lastModified) {
		try {
			if (!request.headers.containsKey("if-modified-since")) {
				return true;
			}

			long lastModifiedSecondBased = lastModified / 1000;
			long modifiedSinceSecondBased = Utils.getHttpDateFormatter()
					.parse(request.headers.get("if-modified-since").value()).getTime() / 1000;
			if (modifiedSinceSecondBased >= lastModifiedSecondBased) {
				return false;
			}
		} catch (Exception e) {
			Logger.warn(e, "error checking etag");
		}
		return true;
	}
}
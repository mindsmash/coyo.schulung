package models;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Extends {@link GenericBaseModel} and supplies a Long id.
 * 
 * @author Jan Marquardt
 */
@MappedSuperclass
public abstract class BaseModel extends GenericBaseModel {

	@Id
	@GeneratedValue
	public Long id;

	public Long getId() {
		return id;
	}

	@Override
	public Object _key() {
		return getId();
	}
}

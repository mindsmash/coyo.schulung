package models;

import org.hibernate.Session;
import play.data.binding.NoBinding;
import play.db.jpa.GenericModel;
import play.db.jpa.JPABase;

import javax.persistence.*;
import java.util.Date;

/**
 * A superclass for models that care about the time they were created or
 * modified. Also supplies methods to all JPA hooks.
 *
 * @author Jan Marquardt
 */
@MappedSuperclass
public abstract class GenericBaseModel extends GenericModel {

	@NoBinding
	public Date created;

	@NoBinding
	public Date modified;

	public GenericBaseModel() {
		this.created = new Date();
		this.modified = new Date();
	}

	@PrePersist
	protected void beforeInsert() throws Exception {
		beforeSave();
	}

	@PreUpdate
	protected void beforeUpdate() throws Exception {
		beforeSave();
	}

	protected void beforeSave() throws Exception {

	}

	@PreRemove
	protected void beforeDelete() throws Exception {
	}

	@PostLoad
	protected void afterSelect() throws Exception {
	}

	@PostPersist
	protected void afterInsert() throws Exception {
		afterSave();
	}

	@PostUpdate
	protected void afterUpdate() throws Exception {
		afterSave();
	}

	protected void afterSave() throws Exception {

	}

	@PostRemove
	protected void afterDelete() throws Exception {
	}

	@Override
	public <T extends JPABase> T save() {
		// do not remove
		this.modified = new Date();
		return super.save();
	}

	protected Session getSession() {
		return (Session) em().getDelegate();
	}
}

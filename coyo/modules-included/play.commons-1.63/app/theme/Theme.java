package theme;

import java.io.File;
import java.util.List;

import play.Logger;
import play.Play;
import play.exceptions.TemplateNotFoundException;
import play.mvc.Http.Cookie;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.Router;
import play.templates.TemplateLoader;
import play.vfs.VirtualFile;
import util.Util;

public class Theme {

	public static String THEME_COOKIE_KEY = "theme";
	public static String THEME_CONF_KEY = "theme";
	public static String THEME_DIR_THEMES = "themes";
	public static String THEME_DIR_VIEWS = "views";
	public static String THEME_DIR_PUBLIC = "public";

	public static final List<String> themes = Util.parseList(Play.configuration.getProperty(THEME_CONF_KEY), ",");

	public static String getActiveTheme() {
		if (themes.size() == 0) {
			return null;
		}
		// look in cookie if multiple themes
		if (themes.size() > 1 && Request.current() != null && Request.current().cookies.containsKey(THEME_COOKIE_KEY)) {
			String activeTheme = Request.current().cookies.get(THEME_COOKIE_KEY).value;

			if (!themes.contains(activeTheme)) {
				Response.current().removeCookie(THEME_COOKIE_KEY);
			} else {
				return activeTheme;
			}
		}
		return themes.get(0);
	}

	public static void setActiveTheme(String theme) {
		if (themes.contains(theme) && Response.current() != null && Request.current() != null
				&& !getActiveTheme().equals(theme)) {
			Logger.debug("changing theme to '%s'", theme);

			// set for current request
			Cookie current = Request.current().cookies.get(THEME_COOKIE_KEY);
			if (current == null) {
				current = new Cookie();
			}
			current.value = theme;
			Request.current().cookies.put(THEME_COOKIE_KEY, current);

			// set for future requests
			Response.current().setCookie(THEME_COOKIE_KEY, theme);
		}
	}

	public static String getThemePublicPath(String theme) {
		return (theme == null) ? THEME_DIR_PUBLIC : THEME_DIR_THEMES + "/" + theme + "/" + THEME_DIR_PUBLIC;
	}

	public static String getThemeViewsPath(String theme) {
		return (theme == null) ? THEME_DIR_VIEWS : THEME_DIR_THEMES + "/" + theme + "/" + THEME_DIR_VIEWS;
	}

	public static String getThemePublicPath() {
		return getThemePublicPath(getActiveTheme());
	}

	public static String getThemeViewsPath() {
		return getThemeViewsPath(getActiveTheme());
	}

	public static VirtualFile getVFResource(String resource) {
		return VirtualFile.open(getResource(resource));
	}

	public static boolean templateExists(String template) {
		try {
			TemplateLoader.load(template);
		} catch (TemplateNotFoundException e) {
			return false;
		}
		return true;
	}

	public static File getResource(String resource) {
		// try to load from theme
		VirtualFile themeFile = Play.getVirtualFile(getThemePublicPath(getActiveTheme()) + "/" + resource);
		if (getActiveTheme() != null && themeFile != null) {
			Logger.trace("found resource [%s] in theme [%s] public", resource, getActiveTheme());
			return themeFile.getRealFile();
		}

		// try to load from play default public
		File publicFile = new File(Play.applicationPath, "public/" + resource);
		if (publicFile != null && publicFile.exists()) {
			Logger.trace("found resource [%s] in default public", resource);
			return publicFile;
		}

		// try to load from modules
		for (VirtualFile module : Play.modules.values()) {
			if (module.child("public/" + resource).exists()) {
				Logger.trace("found resource [%s] in module[%s]", resource, module.getName());
				return module.child("public/" + resource).getRealFile();
			}
		}

		throw new IllegalArgumentException("resource '" + resource
				+ "' not found in theme resources or play public resources");
	}

	public static boolean resourceExists(String resource) {
		try {
			return getResource(resource).exists();
		} catch (Exception ignored) {
			return false;
		}
	}

	public static String getResourceUrl(String resource) {
		return getResourceUrl(resource, false);
	}

	public static String getResourceUrl(String resource, boolean absolute) {
		try {
			return Router.reverse(getVFResource(resource), absolute);
		} catch (Exception e) {
			throw new RuntimeException("could not load route for resource '" + resource + "'", e);
		}
	}
}

package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import play.Play;
import play.exceptions.TemplateNotFoundException;
import play.mvc.Router;
import play.templates.BaseTemplate;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.TemplateLoader;
import play.vfs.VirtualFile;
import util.HttpUtil;
import util.HttpUtil.QueryString;

public class CommonFastTags extends FastTags {

	public static void _includeParent(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		try {
			String path = template.template.name.replaceFirst("/app/views/", "");

			// ignores first result (which is the default)
			BaseTemplate tpl = null;
			boolean firstMatch = false;
			for (VirtualFile vf : Play.templatesPath) {
				if (vf == null) {
					continue;
				}
				VirtualFile tf = vf.child(path);
				if (tf.exists()) {
					tpl = (BaseTemplate) TemplateLoader.load(tf);
					if (!firstMatch) {
						firstMatch = true;
					} else {
						break;
					}
				}
			}

			Map<String, Object> newArgs = new HashMap<String, Object>();
			newArgs.putAll(template.getBinding().getVariables());
			tpl.render(newArgs);
		} catch (TemplateNotFoundException e) {
			throw new TemplateNotFoundException(e.getPath(), template.template, fromLine);
		}
	}

	/**
	 * Appends query parameters to a URL.
	 * 
	 * e.g. #{url @Controller.action(value), 'paramName':'myValue' /} e.g. #{url
	 * 'http://www.mindsmash.com', 'paramName':'myValue' /}
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 * @throws MalformedURLException
	 */
	public static void _url(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine)
			throws MalformedURLException {
		QueryString qs = null;
		for (Map.Entry<?, ?> entry : args.entrySet()) {
			if (!entry.getKey().equals("arg")) {
				if (qs == null) {
					qs = HttpUtil.createQueryString(entry.getKey() + "", entry.getValue() + "");
				} else {
					qs.add(entry.getKey() + "", entry.getValue() + "");
				}
			}
		}
		out.print(HttpUtil.appendQueryString(args.get("arg") + "", qs.getQuery()));
	}

	/**
	 * Returns the reverse URL of the specified string action and adds args.
	 * 
	 * e.g. #{action 'Users.edit', id:5 /}
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _action(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String action = args.remove("arg") + "";
		out.print(Router.reverse(action, (Map<String, Object>) args).url);
	}
}

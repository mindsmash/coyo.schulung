package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import libs.DateI18N;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import play.i18n.Lang;
import play.i18n.Messages;
import play.libs.I18N;
import play.templates.FastTags;
import play.templates.FastTags.Namespace;
import play.templates.GroovyTemplate.ExecutableTemplate;

@Namespace("date")
public class DateFastTags extends FastTags {

	private static DateTime getDateTime(Object arg) {
		if (arg == null) {
			return null;
		} else if (arg instanceof DateTime) {
			return (DateTime) arg;
		} else if (arg instanceof Date) {
			return new DateTime((Date) arg);
		}
		throw new IllegalArgumentException(arg + " is not a date");
	}
	
	private static DateTime addTimeZone(DateTime date, Object arg) {
		if (date != null && arg != null && arg instanceof DateTimeZone) {
			DateTimeZone timezone = (DateTimeZone) arg;
			return date.withZone(timezone);
		}
		return date;
	}

	/**
	 * Returns a since time format of a date, e.g. "2 seconds ago"
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _smart(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String r = "";

		DateTime date = getDateTime(args.get("arg"));
		Date now = new Date();
		if (date != null) {
			if (date.isBeforeNow()) {
				long delta = (now.getTime() - date.getMillis()) / 1000;

				if (delta == 0) {
					r = Messages.get("since.just");
				} else if (delta < 60) {
					r = Messages.get("since.second" + pluralize(delta), delta);
				} else if (delta < 60 * 60) {
					long minutes = delta / 60;
					r = Messages.get("since.minute" + pluralize(minutes), minutes);
				} else if (delta < 24 * 60 * 60) {
					long hours = delta / (60 * 60);
					r = Messages.get("since.hour" + pluralize(hours), hours);
				} else if (delta < 30 * 24 * 60 * 60) {
					// consider midnight
					long days = delta / (24 * 60 * 60);
					DateTime nowDateTime = new DateTime(now);
					if (nowDateTime.hourOfDay().get() < date.hourOfDay().get()
							|| nowDateTime.minuteOfHour().get() < date.minuteOfHour().get()
							|| nowDateTime.secondOfMinute().get() < date.secondOfMinute().get()) {
						days++;
					}
					r = Messages.get("since.day" + pluralize(days), days);
				} else {
					r = date.toString(I18N.getDateFormat(), Lang.getLocale());
				}
			}

			out.print(r);
		}
	}
	
	
	
	/**
	 * Returns a date in ISO 8601 pattern.
	 * see http://de.wikipedia.org/wiki/ISO_8601
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _iso8601(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DateTime date = getDateTime(args.get("arg"));
		if (date != null) {
			date = addTimeZone(date, args.get("timezone"));
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
			out.print(date.toString(formatter));
		}
	}
	
	

	/**
	 * Returns a full date with time information.
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _full(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DateTime date = getDateTime(args.get("arg"));
		if (date != null) {
			date = addTimeZone(date, args.get("timezone"));
			out.print(date.toString(DateI18N.getFullFormat(), Lang.getLocale()));
		}
	}

	/**
	 * Returns a short date without time information.
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _short(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DateTime date = getDateTime(args.get("arg"));
		if (date != null) {
			date = addTimeZone(date, args.get("timezone"));
			out.print(date.toString(DateI18N.getShortFormat(), Lang.getLocale()));
		}
	}

	/**
	 * Returns a time.
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _time(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DateTime date = getDateTime(args.get("arg"));
		if (date != null) {
			date = addTimeZone(date, args.get("timezone"));
			out.print(date.toString(DateI18N.getTimeFormat(), Lang.getLocale()));
		}
	}

	/**
	 * Returns a normal date without time information.
	 * 
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _normal(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DateTime date = getDateTime(args.get("arg"));
		if (date != null) {
			date = addTimeZone(date, args.get("timezone"));
			out.print(date.toString(DateI18N.getNormalFormat(), Lang.getLocale()));
		}
	}

	public static String pluralize(Number n) {
		long l = n.longValue();
		if (l != 1) {
			return "s";
		}
		return "";
	}
}

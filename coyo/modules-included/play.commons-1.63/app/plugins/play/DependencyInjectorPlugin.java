package plugins.play;

import injection.Injector;
import play.PlayPlugin;

/**
 * Plugin for injecting dependencies into supported classes.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class DependencyInjectorPlugin extends PlayPlugin {

	@Override
	public void onApplicationStart() {
		Injector.inject();
	}
}

package plugins.play;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.vfs.VirtualFile;
import util.MapUtil;

/**
 * Reorders the modules as loaded by Play in a configurable order. This is
 * helpful when overwriting views of one module by another module.
 * 
 * The pattern is the same as with the Play plugins configuration. Add the file
 * "conf/play.modules" to define your module's order the following way:
 * 
 * e.g. 100:com.module.some 200:crud
 * 
 * Each line for one module. First the priority, followed by a colon and then
 * the module's name without the version (e.g. "crud-2.0" -> "crud").
 * 
 * You don't need to add all modules. Modules that are not in the config will be
 * left in their original order but will always come after all ordered modules.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class ModuleOrderPlugin extends PlayPlugin {

	private static final String MODULE_ORDER_FILE = "play.modules";

	private static Map<String, Integer> moduleOrder = new LinkedHashMap<String, Integer>();

	@Override
	public void onApplicationReady() {
		Logger.info("[ModuleOrderPlugin] Sorting modules");

		try {
			Enumeration<URL> moduleOrderFiles = Play.classloader.getResources(MODULE_ORDER_FILE);
			while (moduleOrderFiles.hasMoreElements()) {
				File file = new File(moduleOrderFiles.nextElement().getPath());
				for (String line : FileUtils.readLines(file)) {
					String[] order = line.split(":");
					moduleOrder.put(order[1], Integer.valueOf(order[0]));
				}
			}
		} catch (Exception e) {
			throw new Error("error loading module order", e);
		}

		Logger.debug("[ModuleOrderPlugin] Loaded module order: " + moduleOrder);

		if (moduleOrder.size() > 0) {
			Logger.debug("[ModuleOrderPlugin] Sorting modules");
			Play.modules = MapUtil.sortByValue(Play.modules, new ModulePathComparator());

			Logger.debug("[ModuleOrderPlugin] Sorting module routes");
			Play.modulesRoutes = MapUtil.sortByValue(Play.modulesRoutes, new ModulePathComparator());

			Logger.debug("[ModuleOrderPlugin] Sorting template paths");
			Collections.sort(Play.templatesPath, new ModulePathComparator());

			Logger.debug("[ModuleOrderPlugin] Sorting roots");
			Collections.sort(Play.roots, new ModulePathComparator());
		}
	}

	private class ModulePathComparator implements Comparator<VirtualFile> {

		@Override
		public int compare(VirtualFile arg0, VirtualFile arg1) {
			String mod0 = getModuleName(arg0);
			String mod1 = getModuleName(arg1);

			// if both paths are modules
			if (mod0 != null && mod1 != null) {
				if (moduleOrder.containsKey(mod0) && moduleOrder.containsKey(mod1)) {
					return moduleOrder.get(mod0).intValue() - moduleOrder.get(mod1).intValue();
				} else if (moduleOrder.containsKey(mod0)) {
					return -1;
				} else if (moduleOrder.containsKey(mod1)) {
					return 1;
				}
			}

			return 0;
		}

		private String getModuleName(VirtualFile vf) {
			String path = vf.getRealFile().getAbsolutePath();
			for (String module : Play.modules.keySet()) {
				if (path.startsWith(Play.modules.get(module).getRealFile().getAbsolutePath())) {
					return module;
				}
			}
			return null;
		}
	}
}

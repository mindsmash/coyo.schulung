package plugins.play;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.exceptions.TemplateNotFoundException;
import play.templates.Template;
import play.templates.TemplateLoader;
import play.vfs.VirtualFile;
import theme.Theme;

/**
 * <p>
 * <strong>Attention: You only need to load this plugin if you use more than one theme and want to be able to change
 * themes at runtime.</strong>
 * </p>
 * 
 * The list of template paths is static in Play. That means that we cannot change those paths on a request basis and
 * Play will always search in the same directories to find templates.
 * 
 * This plugin checks if Play loads templates from the wrong theme and changes them to the correct theme and path.
 * 
 * @author Jan Marquardt
 */
public class ThemePlugin extends PlayPlugin {

	private int pathIndex = 0;

	@Override
	public void onApplicationReady() {
		Logger.info("[Theme] Loading themes");

		for (String theme : Theme.themes) {
			Logger.info("[Theme] Loading theme: %s", theme);

			String viewsPath = Theme.getThemeViewsPath(theme);
			String publicPath = Theme.getThemePublicPath(theme);

			// search in main app
			addPath(VirtualFile.open(Play.applicationPath).child(viewsPath));
			addPath(VirtualFile.open(Play.applicationPath).child(publicPath));

			// search in modules
			for (VirtualFile module : Play.modules.values()) {
				addPath(module.child(viewsPath));
				addPath(module.child(publicPath));
			}
		}
	}

	private void addPath(VirtualFile vf) {
		if (vf.exists() && !Play.templatesPath.contains(vf)) {
			Logger.info("[Theme] Adding theme template path: %s", vf.relativePath());
			Play.templatesPath.add(pathIndex++, vf);
		}
	}

	@Override
	public Template loadTemplate(VirtualFile file) {
		if (Theme.themes.size() > 1) {
			String relativePath = removeModulePrefix(file.relativePath());

			String themePrefix = "/" + Theme.THEME_DIR_THEMES;
			String activeThemePrefix = themePrefix + "/" + Theme.getActiveTheme();

			// check if resource from other theme and fix this
			if (relativePath.startsWith(themePrefix) && !relativePath.startsWith(activeThemePrefix)) {

				String rawPath = relativePath.replaceFirst(themePrefix + "/", "");
				rawPath = rawPath.substring(rawPath.indexOf("/") + 1);
				if (rawPath.startsWith(Theme.THEME_DIR_PUBLIC)) {
					rawPath = rawPath.replaceFirst(Theme.THEME_DIR_PUBLIC, "");
				}
				if (rawPath.startsWith(Theme.THEME_DIR_VIEWS)) {
					rawPath = rawPath.replaceFirst(Theme.THEME_DIR_VIEWS, "");
				}

				// now search again but leave out inactive themes
				for (VirtualFile vf : Play.templatesPath) {
					String vfPath = removeModulePrefix(vf.relativePath());
					if (vf == null || (vfPath.startsWith(themePrefix) && !vfPath.startsWith(activeThemePrefix))) {
						continue;
					}
					VirtualFile tf = vf.child(rawPath);
					if (tf.exists()) {
						if (Logger.isTraceEnabled()) {
							Logger.trace("[Theme] Switching loaded template [%s] to [%s]", relativePath,
									tf.relativePath());
						}
						return TemplateLoader.load(tf);
					}
				}

				throw new TemplateNotFoundException(rawPath);
			}
		}
		return null;
	}

	private String removeModulePrefix(String path) {
		// remove module prefix if present, e.g.
		// {module:some.module-head}/theme/.. -> /theme/..
		if (path.contains("}")) {
			path = path.substring(path.indexOf("}") + 1);
		}
		return path;
	}
}

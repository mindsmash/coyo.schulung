package plugins.play;

import java.util.List;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.exceptions.UnexpectedException;
import conf.DynamicPropertiesWrapper;
import conf.PropertyResolver;
import conf.Resolve;

/**
 * Replace play's default configuration by {@link DynamicPropertiesWrapper} in
 * order to allow dynamically loadable configuration values.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class DynamicConfigurationPlugin extends PlayPlugin {

	@Override
	public void onConfigurationRead() {
		if (!(Play.configuration instanceof DynamicPropertiesWrapper)) {
			Play.configuration = new DynamicPropertiesWrapper(Play.configuration);
		}
	}

	@Override
	public void onApplicationStart() {

		List<ApplicationClass> resolverClasses = Play.classes.getAnnotatedClasses(Resolve.class);

		for (ApplicationClass resolverClass : resolverClasses) {
			Resolve annotation = resolverClass.javaClass.getAnnotation(Resolve.class);
			try {
				DynamicPropertiesWrapper.resolvers.put(annotation.value(),
						(PropertyResolver) resolverClass.javaClass.newInstance());
				Logger.debug("Loaded configuration resolver [%s]", resolverClass.name);
			} catch (Exception e) {
				throw new UnexpectedException("Cannot load property resolvers", e);
			}
		}

	}
}
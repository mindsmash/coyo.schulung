package plugins.play;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.mvc.Http.Request;
import play.mvc.Mailer;
import util.Util;

/**
 * Sends exceptions that occur in the application to a list of recipients.
 * 
 * Activate plugin by adding a "play.plugins" file to your conf folder and add
 * the following line:
 * 
 * 1000:plugins.ExceptionMailerPlugin
 * 
 * Configure recipients in the play application configuration using the
 * parameter "mail.exception.to". Recipients should be comma separated.
 * 
 * @author mindsmash GmbH
 */
public class ExceptionMailerPlugin extends PlayPlugin {

	private static final String CONF_PROP = "mail.exception.to";

	@Override
	public void onInvocationException(Throwable t) {
		if (Play.mode.isProd() && Play.configuration.containsKey(CONF_PROP)) {
			Logger.debug("exception mailer cought exception[%s]", t);
			ExceptionMailer.sendException(t);
		}
	}

	public static class ExceptionMailer extends Mailer {

		public static void sendException(Throwable exception) {
			setFrom(Play.configuration.getProperty("mail.from"));
			setSubject("Exception: " + exception.getClass().getName() + " - on server: " + Request.current().getBase());

			String to = Play.configuration.getProperty(CONF_PROP);
			for (String recipient : Util.parseEmailList(to)) {
				addRecipient(StringUtils.strip(recipient));
			}

			Logger.debug("exception mailer is sending exception[%s] to recipients[%s]", exception, to);
			try {
				String params = Arrays.asList(Request.current().params).toString();
				String headers = Request.current().headers.toString();
				String host = Request.current().host;
				send("ExceptionMailer/sendException", exception, params, headers, host);
			} catch (Exception e) {
				Logger.error(e, "error sending exception mail");
			}
		}
	}
}

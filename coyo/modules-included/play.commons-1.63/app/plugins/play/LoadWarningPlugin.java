package plugins.play;

import play.Invoker;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.mvc.results.Error;

/**
 * This plugin checks the request queue size and outputs a warning to the logs
 * if the load is too high.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class LoadWarningPlugin extends PlayPlugin {

	// maximum request queue size
	private static final int MAX_LOAD = Integer.parseInt(Play.configuration.getProperty("mindsmash.load.max", "0"));
	private static final int CHECK_INTERVAL_MILLIS = 300000; // 5mins
	private static long lastCheck = 0L;

	@Override
	public void beforeInvocation() {
		if (MAX_LOAD > 0 && (System.currentTimeMillis() - lastCheck) > CHECK_INTERVAL_MILLIS) {
			int queueSize = Invoker.executor.getQueue().size();
			if (queueSize > MAX_LOAD) {
				Logger.warn(
						"maximum load warning: the current load [%s] is higher than the maximum of [%s]. please consider upgrading your system.",
						queueSize, MAX_LOAD);
				lastCheck = System.currentTimeMillis();
			}
		}
	}
}
package extensions;

import org.joda.time.DateTime;

import play.i18n.Lang;
import play.libs.I18N;
import play.templates.JavaExtensions;

public class JodaJavaExtensions extends JavaExtensions {

	public static String format(DateTime dt) {
		return dt.toString(I18N.getDateFormat(), Lang.getLocale());
	}

	public static String format(DateTime dt, String pattern) {
		return dt.toString(pattern, Lang.getLocale());
	}

	public static boolean sameDay(DateTime d1, DateTime d2) {
		return d1.getYear() == d2.getYear() && d1.getDayOfYear() == d2.getDayOfYear();
	}

	public static boolean today(DateTime dt) {
		return sameDay(dt, new DateTime());
	}
}

package date;

import java.util.LinkedHashMap;
import java.util.Map;

import org.joda.time.DateTimeZone;

/**
 * Stores world time zone informations. Can be used to generate and evaluate
 * forms.
 * 
 * @author Jan Marquardt
 */
// TODO: replace by JODA time
public class GMTZone {

	public String id;
	public String name;
	public String cities;

	public GMTZone(String id, String name, String cities) {
		this.id = id;
		this.name = name;
		this.cities = cities;
	}

	public DateTimeZone getDateTimeZone() {
		return DateTimeZone.forID(id);
	}

	public static GMTZone GMT1100M = new GMTZone("Pacific/Pago_Pago", "GMT-11:00", "Pago Pago, Alofi");
	public static GMTZone GMT1000M = new GMTZone("Pacific/Honolulu", "GMT-10:00", "Papeete, Honolulu");
	public static GMTZone GMT0900M = new GMTZone("America/Anchorage", "GMT-9:00", "Anchorage, Juneau, Mangareva");
	public static GMTZone GMT0800M = new GMTZone("America/Los_Angeles", "GMT-8:00",
			"Seattle, Portland, San Francisco, Los Angeles, Las Vegas, Vancouver, Whitehorse, Tijuana");
	public static GMTZone GMT0700M = new GMTZone("America/Denver", "GMT-7:00",
			"Denver, Phoenix, Salt Lake City, Calgary, Yellowknife, Hermosillo, Ciudad Juárez, Mazatlán");
	public static GMTZone GMT0600M = new GMTZone(
			"America/Chicago",
			"GMT-6:00",
			"Belize City, Chicago, Dallas, Guadalajara, Guatemala City, Houston, Managua, Mexico City, Minneapolis, Monterrey, New Orleans, Regina, St. Louis, San José, San Salvador, Tegucigalpa");
	public static GMTZone GMT0500M = new GMTZone(
			"America/New_York",
			"GMT-5:00",
			"Boston, New York, Philadelphia, Washington, D.C., Atlanta, Miami, Cleveland, Cincinnati, Detroit, Lima, Ottawa, Montréal, Québec, Quito, Toronto, Bogotá, Havana, Port-au-Prince, Kingston, Iqaluit");
	public static GMTZone GMT0430M = new GMTZone("America/Caracas", "GMT-4:30", "Caracas");
	public static GMTZone GMT0400M = new GMTZone("America/Santiago", "GMT-4:00",
			"Santiago de Chile, La Paz, San Juan de Puerto Rico, Rio Branco, Halifax");
	public static GMTZone GMT0330M = new GMTZone("America/St_Johns", "GMT-3:30", "Saint John's");
	public static GMTZone GMT0300M = new GMTZone("America/Sao_Paulo", "GMT-3:00",
			"Rio de Janeiro, Sao Paulo, Recife, Nuuk, Buenos Aires, Montevideo, Cayenne");
	public static GMTZone GMT0200M = new GMTZone("America/Noronha", "GMT-2:00", "Vila dos Remedios, King Edward Point");
	public static GMTZone GMT0100M = new GMTZone("America/Scoresbysund", "GMT-1:00", "Praia, Ponta Delgada");
	public static GMTZone GMT0000 = new GMTZone("Europe/London", "GMT+0:00",
			"London, Dublin, Abidjan, Casablanca, Accra, Lisbon");
	public static GMTZone GMT0100P = new GMTZone(
			"Europe/Berlin",
			"GMT+1:00",
			"Amsterdam, Belgrade, Berlin, Budapest, Vienna, Prague, Brussels, Kinshasa, Lagos, Madrid, Paris, Zagreb, Rome, Stockholm, Oslo, Warsaw, Skopje");
	public static GMTZone GMT0200P = new GMTZone("Europe/Athens", "GMT+2:00",
			"Athens, Sofia, Cairo, Riga, Istanbul, Helsinki, Jerusalem, Johannesburg, Bucharest, Vilnius");
	public static GMTZone GMT0300P = new GMTZone("Africa/Nairobi", "GMT+3:00",
			"Nairobi, Baghdad, Khartoum, Mogadishu, Riyadh");
	public static GMTZone GMT0330P = new GMTZone("Asia/Tehran", "GMT+3:30", "Tehran");
	public static GMTZone GMT0400P = new GMTZone("Asia/Baku", "GMT+4:00", "Baku, Tbilisi, Yerevan, Dubai, Moscow");
	public static GMTZone GMT0430P = new GMTZone("Asia/Kabul", "GMT+4:30", "Kabul");
	public static GMTZone GMT0500P = new GMTZone("Asia/Karachi", "GMT+5:00", "Karachi, Lahore, Faisalabad, Tashkent");
	public static GMTZone GMT0530P = new GMTZone("Asia/Colombo", "GMT+5:30",
			"Ahmedabad, Bengaluru, Chennai, Colombo, Delhi, Hyderabad, Kolkata, Mumbai, Thiruvananthapuram");
	public static GMTZone GMT0545P = new GMTZone("Asia/Kathmandu", "GMT+5:45", "Kathmandu, Pokhara");
	public static GMTZone GMT0600P = new GMTZone("Asia/Dhaka", "GMT+6:00",
			"Astana, Almaty, Bishkek, Dhaka, Chittagong, Khulna, Sylhet, Thimphu");
	public static GMTZone GMT0630P = new GMTZone("Asia/Rangoon", "GMT+6:30", "Rangoon, Yangon, Mandalay");
	public static GMTZone GMT0700P = new GMTZone("Asia/Jakarta", "GMT+7:00", "Jakarta, Bangkok, Phnom Pehn, Hanoi");
	public static GMTZone GMT0800P = new GMTZone("Asia/Singapore", "GMT+8:00",
			"Perth, Singapore, Kuala Lumpur, Denpasar, Krasnoyarsk");
	public static GMTZone GMT0845P = new GMTZone("Australia/Eucla", "GMT+8:45", "Eucla");
	public static GMTZone GMT0900P = new GMTZone("Asia/Tokyo", "GMT+9:00", "Seoul, Tokyo");
	public static GMTZone GMT0930P = new GMTZone("Australia/Adelaide", "GMT+9:30", "Adelaide, Darwin");
	public static GMTZone GMT1000P = new GMTZone("Australia/Sydney", "GMT+10:00",
			"Sydney, Melbourne, Yakutsk, Port Moresby");
	public static GMTZone GMT1030P = new GMTZone("Australia/Lord_Howe", "GMT+10:30", "Lord Howe Island");
	public static GMTZone GMT1100P = new GMTZone("Asia/Vladivostok", "GMT+11:00", "Vladivostok, Noumea, Port Vila");
	public static GMTZone GMT1130P = new GMTZone("Pacific/Norfolk", "GMT+11:30", "Norfolk Island");
	public static GMTZone GMT1200P = new GMTZone("Pacific/Auckland", "GMT+12:00", "Auckland, Wellington, Suva, Magadan");
	public static GMTZone GMT1245P = new GMTZone("Pacific/Chatham", "GMT+12:45", "Chatham Islands");
	public static GMTZone GMT1300P = new GMTZone("Pacific/Enderbury", "GMT+13:00", "Apia, Nukualofa, Enderbury");
	public static GMTZone GMT1400P = new GMTZone("Pacific/Kiritimati", "GMT+14:00", "Line Islands, Tokelau, Kiritimati");

	public static GMTZone get(String id) {
		return all().get(id);
	}

	public static Map<String, GMTZone> all() {
		Map<String, GMTZone> r = new LinkedHashMap<String, GMTZone>();
		r.put(GMT1100M.id, GMT1100M);
		r.put(GMT1000M.id, GMT1000M);
		r.put(GMT0900M.id, GMT0900M);
		r.put(GMT0800M.id, GMT0800M);
		r.put(GMT0700M.id, GMT0700M);
		r.put(GMT0600M.id, GMT0600M);
		r.put(GMT0500M.id, GMT0500M);
		r.put(GMT0430M.id, GMT0430M);
		r.put(GMT0400M.id, GMT0400M);
		r.put(GMT0330M.id, GMT0330M);
		r.put(GMT0300M.id, GMT0300M);
		r.put(GMT0200M.id, GMT0200M);
		r.put(GMT0100M.id, GMT0100M);
		r.put(GMT0000.id, GMT0000);
		r.put(GMT0100P.id, GMT0100P);
		r.put(GMT0200P.id, GMT0200P);
		r.put(GMT0300P.id, GMT0300P);
		r.put(GMT0330P.id, GMT0330P);
		r.put(GMT0400P.id, GMT0400P);
		r.put(GMT0430P.id, GMT0430P);
		r.put(GMT0500P.id, GMT0500P);
		r.put(GMT0530P.id, GMT0530P);
		r.put(GMT0545P.id, GMT0545P);
		r.put(GMT0600P.id, GMT0600P);
		r.put(GMT0630P.id, GMT0630P);
		r.put(GMT0700P.id, GMT0700P);
		r.put(GMT0800P.id, GMT0800P);
		r.put(GMT0845P.id, GMT0845P);
		r.put(GMT0900P.id, GMT0900P);
		r.put(GMT0930P.id, GMT0930P);
		r.put(GMT1000P.id, GMT1000P);
		r.put(GMT1030P.id, GMT1030P);
		r.put(GMT1100P.id, GMT1100P);
		r.put(GMT1130P.id, GMT1130P);
		r.put(GMT1200P.id, GMT1200P);
		r.put(GMT1245P.id, GMT1245P);
		r.put(GMT1300P.id, GMT1300P);
		r.put(GMT1400P.id, GMT1400P);
		return r;
	}
}

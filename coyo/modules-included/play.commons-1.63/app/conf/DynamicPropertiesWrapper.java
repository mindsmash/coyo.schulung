package conf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Enumeration;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Wrap properties to dynamically load values.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class DynamicPropertiesWrapper extends Properties {

	public final static Map<String, PropertyResolver> resolvers = new ConcurrentHashMap<String, PropertyResolver>();

	protected Properties origin;

	public DynamicPropertiesWrapper() {
		this(new Properties());
	}

	public DynamicPropertiesWrapper(Properties origin) {
		this.origin = origin;
	}

	@Override
	public String getProperty(String key) {
		if (resolvers.containsKey(key)) {
			return resolvers.get(key).resolve();
		}
		return origin.getProperty(key);
	}

	@Override
	public synchronized Object setProperty(String key, String value) {
		return origin.setProperty(key, value);
	}

	@Override
	public synchronized void load(Reader reader) throws IOException {
		origin.load(reader);
	}

	@Override
	public synchronized void load(InputStream inStream) throws IOException {
		origin.load(inStream);
	}

	@Override
	@Deprecated
	public void save(OutputStream out, String comments) {
		origin.save(out, comments);
	}

	@Override
	public void store(Writer writer, String comments) throws IOException {
		origin.store(writer, comments);
	}

	@Override
	public void store(OutputStream out, String comments) throws IOException {
		origin.store(out, comments);
	}

	@Override
	public synchronized void loadFromXML(InputStream in) throws IOException, InvalidPropertiesFormatException {
		origin.loadFromXML(in);
	}

	@Override
	public void storeToXML(OutputStream os, String comment) throws IOException {
		origin.storeToXML(os, comment);
	}

	@Override
	public void storeToXML(OutputStream os, String comment, String encoding) throws IOException {
		origin.storeToXML(os, comment, encoding);
	}

	@Override
	public String getProperty(String key, String defaultValue) {
		String val = getProperty(key);
		return (val == null) ? defaultValue : val;
	}

	@Override
	public Enumeration<?> propertyNames() {
		return origin.propertyNames();
	}

	@Override
	public Set<String> stringPropertyNames() {
		return origin.stringPropertyNames();
	}

	@Override
	public void list(PrintStream out) {
		origin.list(out);
	}

	@Override
	public void list(PrintWriter out) {
		origin.list(out);
	}

	@Override
	public synchronized int size() {
		return origin.size();
	}

	@Override
	public synchronized boolean isEmpty() {
		return origin.isEmpty();
	}

	@Override
	public synchronized Enumeration<Object> keys() {
		return origin.keys();
	}

	@Override
	public synchronized Enumeration<Object> elements() {
		// todo
		return origin.elements();
	}

	@Override
	public synchronized boolean contains(Object value) {
		return origin.contains(value);
	}

	@Override
	public boolean containsValue(Object value) {
		// TODO
		return origin.containsValue(value);
	}

	@Override
	public synchronized boolean containsKey(Object key) {
		return origin.containsKey(key);
	}

	@Override
	public synchronized Object get(Object key) {
		return getProperty((String) key);
	}

	@Override
	public synchronized Object put(Object key, Object value) {
		return origin.put(key, value);
	}

	@Override
	public synchronized Object remove(Object key) {
		return origin.remove(key);
	}

	@Override
	public synchronized void putAll(Map<? extends Object, ? extends Object> t) {
		origin.putAll(t);
	}

	@Override
	public synchronized void clear() {
		origin.clear();
	}

	@Override
	public synchronized Object clone() {
		return origin.clone();
	}

	@Override
	public synchronized String toString() {
		return origin.toString();
	}

	@Override
	public Set<Object> keySet() {
		return origin.keySet();
	}

	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return origin.entrySet();
	}

	@Override
	public Collection<Object> values() {
		// TODO
		return origin.values();
	}

	@Override
	public synchronized boolean equals(Object o) {
		return origin.equals(o);
	}

	@Override
	public synchronized int hashCode() {
		return origin.hashCode();
	}
}

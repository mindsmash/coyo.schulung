package conf;

/**
 * Resolver for configuration property.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public interface PropertyResolver {

	public abstract String resolve();
}
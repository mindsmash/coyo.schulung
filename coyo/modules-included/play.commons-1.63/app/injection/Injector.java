package injection;

import injection.Inject.NULL;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.exceptions.UnexpectedException;

/**
 * Injects beans into static attributes off classes annotated with {@link InjectionSupport}.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class Injector {

	private static Map<Class<?>, InjectionFactory<?>> factories;

	// holds singletons
	private static Map<String, Object> instances = new HashMap<>();

	/**
	 * For now, inject into static fields.
	 */
	public static void inject() {
		List<Class> classes = Play.classloader.getAnnotatedClasses(InjectionSupport.class);

		for (Class<?> clazz : classes) {
			for (Field field : clazz.getDeclaredFields()) {
				if (Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(Inject.class)) {
					Inject inject = field.getAnnotation(Inject.class);
					Class<?> type = field.getType();
					field.setAccessible(true);

					try {
						Object bean = getBean(type, inject);
						Logger.debug("[Injector] Injecting %s into %s.%s", bean, clazz.getName(), field.getName());
						field.set(null, bean);
					} catch (RuntimeException e) {
						throw e;
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
	}

	/**
	 * Receive all factories that create beans to be injected.
	 */
	private static Map<Class<?>, InjectionFactory<?>> getFactories() {
		if (factories == null) {
			factories = new HashMap<Class<?>, InjectionFactory<?>>();

			List<ApplicationClass> factoryClasses = Play.classes.getAnnotatedClasses(For.class);

			for (ApplicationClass factoryClass : factoryClasses) {
				For annotation = factoryClass.javaClass.getAnnotation(For.class);

				if (factories.containsKey(annotation.value())) {
					throw new IllegalStateException("Cannot annotate more than one factory for "
							+ annotation.value().getCanonicalName());
				}

				try {
					factories.put(annotation.value(), (InjectionFactory<?>) factoryClass.javaClass.newInstance());
				} catch (Exception e) {
					throw new UnexpectedException(e);
				}
			}
		}

		return factories;
	}

	private static Object getBean(Class<?> type, Inject inject) throws Exception {
		return getBean(type, inject.singleton(), inject.configuration(), inject.defaultClass());
	}

	public static Object getBean(Class<?> type, boolean singleton, String configurationParameter,
			Class defaultClass) throws Exception {
		if (!singleton) {
			return createInstance(type, singleton, configurationParameter, defaultClass);
		}

		String beanName = getBeanName(type, configurationParameter);

		if (instances.containsKey(beanName)) {
			Logger.debug("[Injector] Loading existing bean: %s", beanName);
			return instances.get(beanName);
		}

		Object bean = createInstance(type, singleton, configurationParameter, defaultClass);
		instances.put(beanName, bean);
		return bean;
	}

	private static Object createInstance(Class<?> type, boolean singleton, String configurationParameter,
			Class defaultClass) throws Exception {
		if (Logger.isDebugEnabled()) {
			Logger.debug("[Injector] Creating bean: %s", getBeanName(type, configurationParameter));
		}

		// if there is a configuration parameter
		if (!StringUtils.isEmpty(configurationParameter)) {
			String className = Play.configuration.getProperty(configurationParameter);
			Class clazz;
			if (!StringUtils.isEmpty(className)) {
				clazz = Class.forName(className);
			} else if (defaultClass != null && defaultClass != NULL.class) {
				clazz = defaultClass;
			} else {
				throw new IllegalStateException("Cannot find class to @Inject to " + type.getCanonicalName());
			}
			return clazz.newInstance();
		}

		// if there is a factory
		if (getFactories().containsKey(type)) {
			return getFactories().get(type).create();
		}

		// otherwise, try instantiating type directly
		try {
			return type.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalStateException("Cannot instanciate " + type.getCanonicalName()
					+ "with default empty constructor");
		} catch (IllegalAccessException e) {
			throw new IllegalStateException("Cannot find factory for @Inject of " + type.getCanonicalName());
		}
	}

	private static String getBeanName(Class<?> type, String configurationParameter) {
		String beanName = type.getName();
		if (StringUtils.isNotEmpty(configurationParameter)) {
			beanName += "#" + configurationParameter;
		}
		return beanName;
	}
}

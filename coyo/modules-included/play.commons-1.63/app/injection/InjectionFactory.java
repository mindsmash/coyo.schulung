package injection;

/**
 * Factory for dependency creation. A factory is annotated with @For to declare
 * class responsibility.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 * @param <T>
 */
public interface InjectionFactory<T> {

	/**
	 * Create a Bean of Type T.
	 * 
	 * @return
	 */
	T create();
}

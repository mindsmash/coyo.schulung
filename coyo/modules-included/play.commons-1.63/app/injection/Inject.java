package injection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark a static attribute to be injected by the play.di module. By default a
 * singleton is injected over all classes. If no {@link InjectionFactory} is
 * available for the given type the module will try to create a new instance of
 * the type by using its empty default constructor. You may also specify a
 * defaultClass attribute to tell play.di which class to inject by default.
 * 
 * Giving a configuration key name to the Annotations configuration property
 * pulls the class name from the play application.conf and creates a new
 * instance by using the empty default constructor.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {

	boolean singleton() default false;

	String configuration() default "";

	Class defaultClass() default NULL.class;

	public static class NULL {
	}
}

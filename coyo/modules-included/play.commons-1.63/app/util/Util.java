package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Validation;

/**
 * Does nice things to help developers get their things done.
 * 
 * @author mindsmash GmbH
 */
public abstract class Util {

	/**
	 * Checks if a string is a number.
	 * 
	 * @param string
	 * @return True if string is a number
	 */
	public static boolean isNumber(String string) {
		try {
			Integer.parseInt(string);
		} catch (NumberFormatException ex) {
			return false;
		}
		return true;
	}

	/**
	 * Adds a parameter correctly to an URL
	 * 
	 * @param url
	 * @param name
	 * @param value
	 * @return New URL
	 */
	public static String addUrlParam(String url, String name, Object value) {
		int qpos = url.indexOf('?');
		int hpos = url.indexOf('#');
		char sep = qpos == -1 ? '?' : '&';
		String seg = sep + name + '=' + value;
		return hpos == -1 ? url + seg : url.substring(0, hpos) + seg + url.substring(hpos);
	}

	/**
	 * Parses a string list of email addresses correctly into a string list.
	 * Invalid email addresses will be left out.
	 * 
	 * Email addresses can be separated by comma, semicolon or break.
	 * 
	 * @param emailList
	 * @return List of email addresses
	 */
	public static List<String> parseEmailList(String emailList) {
		List<String> result = new ArrayList<String>();

		String q = emailList;
		q = q.replaceAll("\\r", ","); // breaks
		q = q.replaceAll(";", ","); // semicolons
		String[] arr = q.split(","); // commas
		for (String email : arr) {
			email = email.trim();
			if (Validation.required("", email).ok && Validation.email("", email).ok) {
				result.add(email);
			}
		}
		return result;
	}

	/**
	 * Parses a comma separated string into a string list.
	 * 
	 * @param emailList
	 * @return List
	 */
	public static List<String> parseList(String list, String separator) {
		List<String> result = new ArrayList<String>();

		if (list != null && separator != null) {
			String[] arr = list.split(separator);
			for (String item : arr) {
				result.add(item.trim());
			}
		}
		return result;
	}

	/**
	 * Implodes ajava list into a separated string list.
	 * 
	 * @param list
	 * @return String
	 */
	public static String implodeList(List<Object> list, String separator) {
		String result = "";

		if (list != null && separator != null) {
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object o = it.next();
				result += o + (it.hasNext() ? "," : "");
			}
		}
		return result;
	}

	/**
	 * Converts two arrays of the same size into a map.
	 * 
	 * @param keys
	 * @param values
	 * @return A mutable map.
	 */
	public static Map arraysToMap(Object[] keys, Object[] values) {
		int keysSize = (keys != null) ? keys.length : 0;
		int valuesSize = (values != null) ? values.length : 0;

		if (keysSize == 0 && valuesSize == 0) {
			// return mutable map
			return new HashMap();
		}

		if (keysSize != valuesSize) {
			throw new IllegalArgumentException("The number of keys doesn't match the number of values.");
		}

		Map map = new HashMap();
		for (int i = 0; i < keysSize; i++) {
			map.put(keys[i], values[i]);
		}

		return map;
	}

	/**
	 * Checks if a text string matches a simple wildcard pattern.
	 * 
	 * @param text, e.g. "My dog poops"
	 * @param pattern, e.g. "*dog*poop*"
	 * @return True if the given text string matches the given pattern
	 */
	public static boolean matches(String text, String pattern) {
		if (pattern == null || text == null || "".equals(pattern)) {
			return false;
		}

		String[] cards = pattern.split("\\*");

		for (String card : cards) {
			int idx = text.indexOf(card);

			if (idx == -1) {
				return false;
			}

			text = text.substring(idx + card.length());
		}
		return true;
	}

	/**
	 * Concatenate the elements of an array into a string.
	 * 
	 * @param ary
	 * @param delim
	 * @return Imploded array
	 */
	public static String implode(Object[] ary, String delim) {
		String out = "";
		if (ary != null) {
			for (int i = 0; i < ary.length; i++) {
				if (i != 0) {
					out += delim;
				}
				out += ary[i];
			}
		}
		return out;
	}

	/**
	 * Concatenate the elements of an array into a string.
	 * 
	 * @param list
	 * @param delim
	 * @return Imploded list
	 */
	public static String implode(List list, String delim) {
		return implode(list.toArray(), delim);
	}

	/**
	 * Uses all available locales to get the map of all countries. Some
	 * available locales are incomplete and those are filtered out.
	 * 
	 * You can use the {@link Locale#getDisplayCountry()} method to get the name
	 * of the country.
	 * 
	 * @return Clean map of available country locales. Key is the country key,
	 *         e.g. "DE".
	 */
	public static Map<String, Locale> getCountries() {
		Map<String, Locale> r = new TreeMap<String, Locale>();

		for (Locale locale : Locale.getAvailableLocales()) {
			if (!StringUtils.isEmpty(locale.toString()) && !StringUtils.isEmpty(locale.getDisplayCountry())) {
				r.put(locale.getCountry(), locale);
			}
		}

		return r;
	}

	/**
	 * Generates a random number within a range
	 * 
	 * @param min
	 * @param max
	 * @return Random long number.
	 */
	public static long rand(long min, long max) {
		return min + (long) (Math.random() * ((max - min) + 1));
	}
}

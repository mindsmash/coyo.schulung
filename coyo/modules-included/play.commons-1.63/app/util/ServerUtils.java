package util;

import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import play.Logger;

public class ServerUtils {

	private static final Pattern MAC_PATTERN = Pattern.compile("((([0-9a-fA-F]){1,2}[-:]){5}([0-9a-fA-F]){1,2})");
	private static String mac;

	private ServerUtils() {
	}

	/**
	 * @return Returns one valid MAC address or null if none found.
	 */
	public static String getMacAddress() {
		if (mac == null) {
			try {
				// try Java 6 method
				Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
				if (interfaces != null) {
					while (interfaces.hasMoreElements()) {
						NetworkInterface ni = (NetworkInterface) interfaces.nextElement();
						if (ni != null) {
							byte[] mac = ni.getHardwareAddress();
							if (mac != null && mac.length > 0) {
								String ma = "";
								for (int i = 0; i < mac.length; i++) {
									ma += String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : "");
								}
								ServerUtils.mac = ma;
								return ma;
							}
						}
					}
				}

				// try windows command
				String windowsMac = getMacAddressFromCommand("getmac /fo csv /nh");
				if (windowsMac != null) {
					mac = windowsMac;
					return mac;
				}

				// try freebsd command
				String freeBsdMac = getMacAddressFromCommand("ifconfig | awk '/ether/ {print $NF}'");
				if (freeBsdMac != null) {
					mac = freeBsdMac;
					return mac;
				}
			} catch (Exception e) {
				Logger.warn(e, "error determining mac address");
			}
		}

		return mac;
	}

	private static String getMacAddressFromCommand(String command) {
		try {
			Process p = Runtime.getRuntime().exec(command);
			java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
			String line = in.readLine();
			if (!StringUtils.isEmpty(line)) {
				List<String> mac = Util.parseList(line, ",");
				for (String ma : mac) {
					if (MAC_PATTERN.matcher(ma).matches()) {
						return ma;
					}
				}
			}
		} catch (IOException ignored) {
			// command does not exist, wrong OS
		}

		return null;
	}
}

package util;

import models.BaseModel;
import play.Play;
import play.cache.Cache;
import play.i18n.Lang;

/**
 * This is a helper for server-side caching of entity-related data (typically
 * HTML code).
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class CacheUtil {

	private CacheUtil() {
	}

	/**
	 * Clears the server side cache for an entity.
	 * 
	 * @param entity
	 */
	public static void clear(BaseModel entity) {
		if (entity != null) {
			// clear for every language (set in pos.tag)
			for (String lang : Util.parseList(Play.configuration.getProperty("application.langs"), ",")) {
				Cache.delete(getKey(entity, lang));
			}
		}
	}

	public static String getKey(BaseModel entity) {
		return getKey(entity, Lang.get());
	}

	private static String getKey(BaseModel entity, String lang) {
		return entity.getClass().getName() + "-" + entity.id + "-" + lang;
	}
}

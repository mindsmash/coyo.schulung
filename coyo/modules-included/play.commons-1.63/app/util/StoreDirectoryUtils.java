package util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import play.Play;

public class StoreDirectoryUtils {

	private StoreDirectoryUtils() {
	}

	/**
	 * Returns the file object for an absolute or play relative path.
	 * 
	 * @param path
	 * @return File or IOException if not found.
	 */
	public static File getStore(String path) {
		File store = new File(path);
		if (!store.isAbsolute()) {
			store = Play.getFile(path);
		}

		if (!store.exists()) {
			store.mkdirs();
		}

		return store;
	}

	/**
	 * Ensure that the store exists.
	 * 
	 * @param store
	 * @return
	 */
	public static File ensureStoreExists(File store) {
		if (!store.exists()) {
			store.mkdirs();
		}
		return store;
	}

	/**
	 * Clean directory if exists.
	 * 
	 * @param directory
	 * @throws IOException
	 */
	public static void cleanDirectoryIfExists(File directory) throws IOException {
		if (directory.exists()) {
			FileUtils.cleanDirectory(directory);
		}
	}
}

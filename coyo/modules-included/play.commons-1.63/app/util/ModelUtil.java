package util;

import java.util.ArrayList;
import java.util.List;

import models.BaseModel;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

/**
 * Util methods for models
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class ModelUtil {

	public static final String SRL_SEPERATOR_CHAR = "-";

	private ModelUtil() {
	}

	/**
	 * Serializes into a string that combines the class name and ID. Format is "models.class.path.ClassName-[ID]"
	 * 
	 * @param model
	 * @return String representation for given model.
	 */
	public static String serialize(GenericModel model) {
		final StringBuffer sb = new StringBuffer();
		final Object id = model._key();

		if (id == null || StringUtils.isEmpty(id.toString())) {
			throw new IllegalArgumentException("Failed create unique name model is missing id value.");
		}

		final String clazzName = model.getClass().getName();

		return sb.append(clazzName).append(SRL_SEPERATOR_CHAR).append(id).toString();
	}

	/**
	 * Loads an object from a serialized string. See {@link #serialize(GenericModel)}
	 * 
	 * @param serialized
	 * @param clazz
	 * @return Loaded model or null if not found
	 */
	public static <T extends GenericModel> T deserialize(String serialized, Class<T> clazz) {
		try {
			return JPA.em().find(clazz, Long.parseLong(serialized.substring(serialized.lastIndexOf(SRL_SEPERATOR_CHAR) + 1)));
		} catch (Exception e) {
			Logger.warn(e, "error loading db object from string: %s", serialized);
		}

		return null;
	}

	/**
	 * Loads an object from a serialized string. See {@link #serialize(GenericModel)}
	 * 
	 * @param serialized
	 * @return Loaded model or null if not found
	 */
	public static GenericModel deserialize(String serialized) {
		try {
			Class clazz = Class.forName(serialized.substring(0, serialized.lastIndexOf(SRL_SEPERATOR_CHAR)));
			return deserialize(serialized, clazz);
		} catch (Exception e) {
			Logger.warn(e, "error loading db object from string: %s", serialized);
		}
		return null;
	}
	
	public static List<Long> getIdList(List<? extends BaseModel> list) {
		List<Long> r = new ArrayList<Long>();
		for (BaseModel o : list) {
			r.add(o.id);
		}
		return r;
	}

	public static String getIds(List<? extends BaseModel> list, String delim) {
		return Util.implode(getIdList(list), delim);
	}
}

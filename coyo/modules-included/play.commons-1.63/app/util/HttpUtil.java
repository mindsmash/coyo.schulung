package util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import play.mvc.Http;
import play.mvc.Http.Header;

public abstract class HttpUtil {

	/**
	 * @return The referrer URL or null
	 */
	public static String getReferrerUrl() {
		Header referrerHeader = Http.Request.current().headers.get("referer");
		if (referrerHeader != null) {
			List<String> referrerList = referrerHeader.values;
			if (referrerList != null && referrerList.size() > 0) {
				return referrerList.get(0);
			}
		}

		return null;
	}

	public static Map<String, String> getQueryMap(String url) {
		Map<String, String> map = new HashMap<String, String>();

		if (url.indexOf("?") >= 0) {
			String[] params = url.split("\\?")[1].split("&");
			for (String param : params) {
				String[] split = param.split("=");
				String name = split[0];
				String value = split.length > 1 ? split[1] : "";
				map.put(name, value);
			}
		}

		return map;
	}

	// int qpos = url.indexOf('?');
	// int hpos = url.indexOf('#');
	// char sep = qpos == -1 ? '?' : '&';
	// String seg = sep + name + '=' + value;
	// return hpos == -1 ? url + seg : url.substring(0, hpos) + seg +
	// url.substring(hpos);

	public static String appendParam(String url, String param, Object value) {
		if (StringUtils.isEmpty(url)) {
			return url;
		}

		// remove first
		if (url.indexOf(param + "=") >= 0) {
			url = removeParam(url, param);
		}
		String fragment = "";
		if (url.indexOf("#") > 0) {
			fragment = url.substring(url.indexOf("#"));
			url = url.substring(0, url.indexOf("#"));
		}

		return url + (url.contains("?") ? "&" : "?") + param + "=" + value + fragment;
	}

	public static String removeParam(String url, String param) {
		Map<String, String> params = getQueryMap(url);
		if(params.size() == 0) {
			return url;
		}
		
		params.remove(param);

		String qs = "";
		for (String key : params.keySet()) {
			qs = qs + "&" + key + "=" + params.get(key);
		}
		if (qs.length() > 0) {
			qs = qs.substring(1);
		}
		url = url.substring(0, url.indexOf("?") + (qs.length() > 0 ? 1 : 0));

		return url + qs;
	}

	public static URL createURL(String url, QueryString qs) throws MalformedURLException {
		return new URL(appendQueryString(url, qs.getQuery()));
	}

	public static String appendQueryString(String url, String qs) throws MalformedURLException {
		String fragment = "";
		if (url.indexOf("#") > 0) {
			fragment = url.substring(url.indexOf("#"));
			url = url.substring(0, url.indexOf("#"));
		}
		return url + (url.contains("?") ? "&" : "?") + qs + fragment;
	}

	public static QueryString createQueryString(String name, String value) {
		return new QueryString(name, value);
	}

	public static class QueryString {

		private String query = "";

		private QueryString(String name, String value) {
			encode(name, value);
		}

		public void add(String name, String value) {
			query += "&";
			encode(name, value);
		}

		private void encode(String name, String value) {
			try {
				query += URLEncoder.encode(name, "UTF-8");
				query += "=";
				query += URLEncoder.encode(value, "UTF-8");
			} catch (UnsupportedEncodingException ex) {
				throw new RuntimeException("VM does not support UTF-8");
			}
		}

		public String getQuery() {
			return query;
		}

		public String toString() {
			return getQuery();
		}

	}
	
	/**
	 * Checks whether the url has a protocol of either http://, https://, ftp://, file:// or just //
	 */
	public static boolean hasValidProtocol(String url) {
		return url.startsWith("//") || url.startsWith("http://") || url.startsWith("https://")
				|| url.startsWith("ftp://") || url.startsWith("file://");
	}

	/**
	 * URL may not contain white spaces and quotes
	 */
	public static boolean isValidUrlString(String url) {
		return !url.contains("\"") && !url.contains(" ");
	}
}

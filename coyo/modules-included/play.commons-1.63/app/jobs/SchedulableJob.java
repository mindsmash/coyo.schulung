package jobs;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import play.Logger;
import play.jobs.Job;
import play.jobs.JobsPlugin;
import play.libs.Expression;
import play.libs.Time.CronExpression;

/**
 * A job that can be rescheduled using {@link JobUtil}.
 * 
 * @deprecated Not currently working. Needs fixing.
 * @author Jan Marquardt, mindsmash GmbH
 */
public abstract class SchedulableJob extends Job {

	public Date nextPlannedExecution = null;
	public String cron = null;

	public void schedule(String cron) {
		try {
			this.cron = cron;
			JobsPlugin.scheduledJobs.add(this);

			Date now = new Date();
			cron = Expression.evaluate(cron, cron).toString();
			CronExpression cronExp = new CronExpression(cron);
			Date nextDate = cronExp.getNextValidTimeAfter(now);
			if (nextDate != null && !nextDate.equals(nextPlannedExecution)) {
				nextPlannedExecution = nextDate;
				JobsPlugin.executor.schedule((Callable<?>) this, nextDate.getTime() - now.getTime(),
						TimeUnit.MILLISECONDS);
				executor = JobsPlugin.executor;
			}
		} catch (Exception e) {
			Logger.error(e, "cannot schedule job %s", this);
		}
	}

	@Override
	public void _finally() {
		// cron is null if we force job execution
		if (cron != null && executor == JobsPlugin.executor) {
			schedule(cron);
		}
	}
}

package jobs;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

import play.Logger;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import util.Util;

/**
 * Runs every day (configurable) to clean the temp folder. Play does not currently do this on it's on. The temp folder
 * is filled by file uploads and other crap.
 *
 * You need to enable this job by setting the following configuration parameter: "mindsmash.autoCleanTemp=true"
 *
 * In order to exclude certain folders, use this configuration parameter: "mindsmash.autoCleanTemp.exclude=ROOT,assets"
 * (ROOT equals the files directly under the temp folder)
 *
 * @author Jan Marquardt
 */
@OnApplicationStart
public class TempCleaner extends Job {

	public static final boolean ACTIVE = Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.autoCleanTemp",
			"false"));

	public static final List<String> EXCLUDE = Util.parseList(
			Play.configuration.getProperty("mindsmash.autoCleanTemp.exclude"), ",");

	public static final String cleaningInterval = Play.configuration.getProperty("mindsmash.autoCleanTemp.interval",
			"1d");

	@Override
	public void doJob() throws Exception {
		if (Play.mode.isProd() && ACTIVE) {
			new CleanerJob().every(cleaningInterval);
		}
	}

	public class CleanerJob extends Job {
		@Override
		public void doJob() throws Exception {
			Logger.debug("[TempCleaner] Start cleaning temporary folders.");
			for (File file : Play.tmpDir.listFiles()) {
				if (file.exists()) {
					if (file.isDirectory()) {
						// if dir, check if excluded
						if (!EXCLUDE.contains(file.getName())) {
							Logger.debug("[TempCleaner] Deleting dir from temp: %s", file.getName());
							FileUtils.deleteDirectory(file);
						}
					} else {
						// if file, check if ROOT is excluded
						if (!EXCLUDE.contains("ROOT")) {
							Logger.debug("[TempCleaner] Deleting file from temp: %s", file.getName());
							file.delete();
						}
					}
				}
			}
			Logger.debug("[TempCleaner] Done cleaning temporary folders.");
		}
	}

}

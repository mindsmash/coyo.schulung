package db.lazy;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;

import org.hibernate.usertype.UserType;

/**
 * Helper to generate a wrapper for a Hibernate {@link UserType} object.
 * 
 * @author Carsten Canow, mindsmash GmbH
 */
public class LazyWrapperProxyHelper {

	public static LazyUserType buildProxy(final Class<? extends LazyUserType> clazz, final Object id) {
		return buildProxyInner(clazz, id);
	}

	public static LazyUserType buildProxy(final Class<? extends LazyUserType> clazz, final String id) {
		return buildProxyInner(clazz, id);
	}

	private static LazyUserType buildProxyInner(final Class<? extends LazyUserType> clazz, final Object id) {
		try {
			Class c = getClass(clazz);
			Object proxy = c.newInstance();
			((ProxyObject) proxy).setHandler(new LazyWrapperProxy(id, clazz));
			return (LazyUserType) proxy;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Map<Class, ProxyFactory> factories = new ConcurrentHashMap<Class, ProxyFactory>();
	private static MethodFilter filter = new MethodFilter() {
		public boolean isHandled(Method m) {
			// do not handle finalize()
			return !m.getName().equals("finalize");
		}
	};

	private static Class getClass(final Class<? extends LazyUserType> clazz) {
		ProxyFactory factory = factories.get(clazz);
		if (null == factory) {
			ProxyFactory f = new ProxyFactory();
			f.setSuperclass(clazz);
			f.setFilter(filter);
			factories.put(clazz, f);
			factory = f;
		}
		return factory.createClass();
	}
}

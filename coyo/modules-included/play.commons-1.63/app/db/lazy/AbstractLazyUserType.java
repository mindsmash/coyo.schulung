package db.lazy;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

/**
 * General implementation for a lazy {@link UserType}.
 * 
 * @author Carsten Canow, mindsmash GmbH
 */
abstract public class AbstractLazyUserType implements LazyUserType {

	abstract protected LazyUserType _byId(Object id);

	abstract public Object lazyDeepCopy(Object value) throws HibernateException;

	@Override
	final public Object deepCopy(Object value) throws HibernateException {
		if (value == null) {
			return null;
		}
		if (!((LazyUserType) value).isLoaded()) {
			return value;
		}
		return lazyDeepCopy(value);
	}

	protected Object lazyNullSafeGet(Object id) {
		LazyUserType proxy = LazyWrapperProxyHelper.buildProxy(getClass(), id);
		if (proxy != null) {
			return proxy;
		}
		return _byId(id);
	}

	@Override
	public boolean isLoaded() {
		return true;
	}

	@Override
	public boolean isWrapper() {
		return false;
	}

	@Override
	public LazyUserType forceLoad() {
		return _byId(getObjectId());
	}

	/**
	 * Equal when objectId and returnedClass are equal.
	 */
	@LazyLoad
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || !(obj instanceof AbstractLazyUserType)) {
			return false;
		}
		AbstractLazyUserType other = (AbstractLazyUserType) obj;
		if (returnedClass() != other.returnedClass()) {
			return false;
		}
		if (getObjectId() == null) {
			if (other.getObjectId() != null) {
				return false;
			}
		} else if (!getObjectId().equals(other.getObjectId())) {
			return false;
		}
		return true;
	}
}

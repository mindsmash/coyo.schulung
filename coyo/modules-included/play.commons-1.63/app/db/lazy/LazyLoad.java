package db.lazy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Define whether a method can be called on wrapper or the real object.
 * 
 * @author Carsten Canow, mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface LazyLoad {
	boolean value() default true;
}

package db.lazy;

import org.hibernate.usertype.UserType;

/**
 * Additional methods needed to load a Hibernate {@link UserType} lazy.
 * 
 * @author Carsten Canow, mindsmash GmbH
 */
public interface LazyUserType extends UserType {

	/**
	 * Provide the foreign identifier for the object.
	 */
	public Object getObjectId();

	/**
	 * Determine whether the distant object is already loaded or not.
	 */
	public boolean isLoaded();

	/**
	 * Determine whether the object is the object is wrapper or the wrapped
	 * instance.
	 */
	public boolean isWrapper();

	/**
	 * Force to load the distant object.
	 */
	public LazyUserType forceLoad();
}

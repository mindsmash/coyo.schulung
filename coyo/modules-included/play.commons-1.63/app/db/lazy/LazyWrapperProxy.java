package db.lazy;

import java.lang.reflect.Method;

import javassist.util.proxy.MethodHandler;

import org.hibernate.usertype.UserType;

import play.Logger;

/**
 * Wrap a Hibernate {@link UserType} object inside a proxy to load on demand
 * only.
 * 
 * @author Carsten Canow, mindsmash GmbH
 */
public class LazyWrapperProxy implements MethodHandler {
	private static final String FORCE_LOAD_METHOD_NAME = "forceLoad";
	private static final String RETURNED_CLASS_METHOD_NAME = "returnedClass";
	private static final String IS_WRAPPER_METHOD_NAME = "isWrapper";
	private static final String IS_LOADED_METHOD_NAME = "isLoaded";
	private static final String GET_ID_METHOD_NAME = "getId";
	private static final String INIT_METHOD_NAME = "_byId";
	private Object instance = null;
	private Object id = null;
	private Class<?> realClazz = null;

	public LazyWrapperProxy(Object id, Class realClazz) {
		this.id = id;
		this.realClazz = realClazz;
	}

	@Override
	public Object invoke(Object self, Method m, Method proceed, Object[] args) throws Throwable {
		if (GET_ID_METHOD_NAME.equals(m.getName())) {
			// id is known; initialization not necessary
			return id;
		}
		if (IS_WRAPPER_METHOD_NAME.equals(m.getName())) {
			// is this object a wrapper (else: method called on real object)
			return true;
		}
		if (IS_LOADED_METHOD_NAME.equals(m.getName())) {
			// has the instance already been initialized
			return instance != null;
		}
		if (RETURNED_CLASS_METHOD_NAME.equals(m.getName())) {
			// real class name is known; initialization not necessary
			return realClazz;
		}
		LazyLoad lazyLoading = m.getAnnotation(LazyLoad.class);
		if (lazyLoading != null && lazyLoading.value() == true) {
			return proceed.invoke(self, args);
		}
		if (FORCE_LOAD_METHOD_NAME.equals(m.getName())) {
			// force initialization
			return forceLoad(self);
		}
		if (INIT_METHOD_NAME.equals(m.getName())) {
			// do initialize instance
			return proceed.invoke(self, id);
		}

		// method call did not trap: initialize and invoke call
		forceLoad(self);
		// method call might be private/protected: override visibility
		m.setAccessible(true);
		// invoke method on instance
		return m.invoke(instance, args);
	}

	private Object forceLoad(Object self) throws Throwable {
		if (instance == null) {
			if (id == null || realClazz == null) {
				throw new IllegalArgumentException("Initialization failed: id and realClazz may not be null!");
			}
			Logger.debug("Object of type %s loaded by id %s", realClazz.getSimpleName(), id.toString());
			// initialize instance
			Method initMethod = realClazz.getDeclaredMethod(INIT_METHOD_NAME, id.getClass());
			initMethod.setAccessible(true);
			instance = initMethod.invoke(self, id);
		}
		return instance;
	}
}

package binding;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

/**
 * Helps you bind IDs to models
 * 
 * @author Jan Marquardt
 */
public class ModelIdBinder {

	private ModelIdBinder() {
	}

	public static <T extends GenericModel> List<T> bind(Class<T> modelClass, List<Long> ids) {
		List<T> r = new ArrayList<T>();

		try {
			if (ids != null) {
				for (Long id : ids) {
					T e = (T) JPA.em().find(modelClass, id);
					if (e != null) {
						r.add(e);
					}
				}
			}
		} catch (Exception e) {
			throw new Error(e);
		}

		return r;
	}
}

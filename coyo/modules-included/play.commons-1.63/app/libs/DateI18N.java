package libs;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.i18n.Lang;

public class DateI18N {

	public static String getNormalFormat() {
		String format = getLocalizedDateFormat("date.format");
		if (format == null) {
			format = "yyyy-MM-dd";
		}
		return format;
	}

	public static String getShortFormat() {
		String format = getLocalizedDateFormat("date.format.short");
		if (format == null) {
			format = "dd/MM";
		}
		return format;
	}

	public static String getTimeFormat() {
		String format = getLocalizedDateFormat("date.format.time");
		if (format == null) {
			format = "h:mm a";
		}
		return format;
	}

	public static String getFullFormat() {
		String format = getLocalizedDateFormat("date.format.full");
		if (format == null) {
			format = "yyyy-MM-dd - h:mm a";
		}
		return format;
	}

	public static String getLocalizedDateFormat(String type) {
		final String localizedDateFormat = Play.configuration.getProperty(type + "." + Lang.get());
		if (!StringUtils.isEmpty(localizedDateFormat)) {
			return localizedDateFormat;
		}
		final String globalDateFormat = Play.configuration.getProperty(type);
		if (!StringUtils.isEmpty(globalDateFormat)) {
			return globalDateFormat;
		}
		return null;
	}
}

package libs;

import org.joda.time.DateTime;

import play.Logger;
import play.cache.Cache;
import play.libs.Time;
import play.mvc.Http.Request;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public abstract class Protection {

	/**
	 * Protects a resource from being called too many times. A typical case is the protection of the authentication
	 * process so that a user can only try the authentication so many times.
	 * 
	 * Simply call this method to increase the count by 1. If maxTrials is reached, the method will return true. Then
	 * you can handle the error the way you prefer.
	 * 
	 * You can define after how many seconds the count of trials will expire.
	 * 
	 * @param max
	 * @param expiration
	 *            e.g. 10s, 1h, 10min
	 * @return True if the maximum is reached.
	 */
	public static boolean max(int max, String expiration) {
		return max(getDefaultRequestId(), max, expiration);
	}

	/**
	 * Same as {@link #max(int, int)} but with a configurable request ID. The max trials will be counted per request ID.
	 * 
	 * @param requestId
	 * @param max
	 * @param expiration
	 *            e.g. 10s, 1h, 10min
	 * @return See {@link #max(int, int)}
	 */
	public static boolean max(String requestId, int max, String expiration) {
		if (requestId == null) {
			Logger.warn("request ID is null. protection not working.");
			return false;
		}

		String key = getKey("max", requestId);
		String expirationKey = key + ":expiration";

		Logger.debug("[Security] Retrieving expiration key for max trials: %s.", Cache.get(expirationKey));

		if (checkExpiration(Cache.get(expirationKey), expiration)) {
			// set counter to 0
			clearMax(requestId);

			// set expiration holder
			Cache.safeSet(expirationKey, new DateTime(), expiration);
			Cache.safeAdd(key, 0, expiration);
		}

		long incrementedValue = Cache.incr(key, 1);
		Logger.debug("[Security] Max count is at %d of %d", incrementedValue, max);
		return incrementedValue >= max;
	}

	public static void clearMax(String requestId) {
		String key = getKey("max", requestId);
		Cache.decr(key, (int) Cache.incr(key, 0));
	}

	public static void clearMax() {
		clearMax(getDefaultRequestId());
	}

	public static boolean maxReached(String requestId, int maxTrials) {
		String key = getKey("max", requestId);
		return Cache.incr(key, 0) >= maxTrials;
	}

	public static boolean maxReached(int maxTrials) {
		return maxReached(getDefaultRequestId(), maxTrials);
	}

	private static String getKey(String type, String requestId) {
		return "protection:" + type + ":" + requestId;
	}

	private static String getDefaultRequestId() {
		if (Request.current() == null) {
			throw new IllegalStateException(
					"protection failed: cannot create request ID because request is null. try supplying a custom request ID.");
		}
		return Request.current().remoteAddress + "@" + Request.current().action;
	}

	/**
	 * This method returns true if the cache entry for checking the max failed attempts has expired.
	 * 
	 * @param object
	 *            The retrieved cache entry object. This should be an instance of DateTime
	 * @param expiration
	 *            String representing the expiration. This must be the same string, that was passed as expiration when
	 *            creating the cache entry e.g. 1min, 20s, ...
	 * @return true if the entry is expired, false otherwise.
	 */
	private static boolean checkExpiration(Object object, String expiration) {
		/* if the expiration cache entry is null, the key is expired */
		if (object == null) {
			return true;
		}

		/* if we can retrieve an entry, we check the stored expiration date */
		if (object instanceof DateTime) {
			DateTime expirationTime = ((DateTime) object).plusSeconds(Time.parseDuration(expiration));
			return expirationTime.isBeforeNow();
		}

		/* if a cache entry exists, but it hasn't expired yet, return false. */
		return false;
	}
}
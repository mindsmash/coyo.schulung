package jobs;

import ldap.LDAPConnector;
import models.AbstractTenant;
import play.Logger;

public class LDAPUserSync extends TenantJob {

	public LDAPUserSync(AbstractTenant tenant) {
		super(tenant);
	}

	@Override
	public void doTenantJob() throws Exception {
		Logger.info("[LDAP] Starting sync of all users");
		LDAPConnector.syncAllUsers(true);
		Logger.info("[LDAP] Finished sync of all users");
	}
}

package jobs;

import ldap.LDAPConnector;
import models.AbstractTenant;
import play.Logger;

public class LDAPGroupSync extends TenantJob {

	public LDAPGroupSync(AbstractTenant tenant) {
		super(tenant);
	}

	@Override
	public void doTenantJob() throws Exception {
		Logger.info("[LDAP] Starting sync of all groups");
		LDAPConnector.syncAllGroups();
		Logger.info("[LDAP] Finished sync of all groups");
	}
}

package ldap;

public class LDAPException extends Exception {

	public LDAPException(String message) {
		super(message);
	}

	public LDAPException(String message, Exception cause) {
		super(message, cause);
	}
}

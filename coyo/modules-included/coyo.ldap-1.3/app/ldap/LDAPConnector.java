package ldap;

import conf.ApplicationSettings;
import injection.Inject;
import injection.InjectionSupport;
import models.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ldap.control.PagedResultsDirContextProcessor;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapEncoder;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.core.support.SingleContextSource;
import play.Logger;
import play.Play;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import play.i18n.Lang;
import play.libs.Crypto;
import util.Util;
import utils.JPAUtils;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import java.util.ArrayList;
import java.util.List;

@InjectionSupport
public class LDAPConnector {

	// auth source
	public static final String SOURCE = "LDAP";

	@Inject(configuration = "coyo.ldap.syncer.class", defaultClass = LDAPSyncer.class)
	protected static LDAPSyncer syncer;

	public static Settings loadLDAPSettings() {
		Settings settings = Settings.findOrCreate("ldap");

		// sync from configuration to DB once
		if (!settings.contains("url") && Play.configuration.containsKey("mindsmash.auth.authenticator.ldap.url")) {
			settings.setProperty("url", Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.url", ""));
			settings.setProperty("additionalUserDn", "");
			settings.setProperty("baseDn",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.baseDn", ""));
			settings.setProperty("username",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userDn", ""));
			settings.setProperty("password",
					Crypto.encryptAES(
							Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.password", "")));
			settings.setProperty(
					"followReferrals",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.referral", "follow").equals(
							"follow")
							+ "");
			settings.setProperty("userObjectClass", Play.configuration.getProperty(
					"mindsmash.auth.authenticator.ldap.userObjectClass", "inetorgperson"));
			settings.setProperty("userObjectFilter",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userObjectFilter", ""));
			settings.setProperty("userUIDAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userUidAttr", "uid"));
			settings.setProperty(
					"userAuthAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userAuthAttr",
							Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userUidAttr", "uid")));
			settings.setProperty("userEmailAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userMailAttr", "mail"));
			settings.setProperty("userFirstNameAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userFirstnameAttr", "givenName"));
			settings.setProperty("userLastNameAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.userLastnameAttr", "sn"));
			settings.setProperty(
					"autocreate",
					Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.autoCreate",
							"true")) + "");
			settings.setProperty(
					"groupSync",
					Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groups",
							"false")) + "");
			settings.setProperty(
					"subgroupSync",
					Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groups",
							"false")) + "");
			settings.setProperty("groupObjectClass", Play.configuration.getProperty(
					"mindsmash.auth.authenticator.ldap.groupObjectClass", "groupOfNames"));
			settings.setProperty("groupObjectFilter",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groupObjectFilter", ""));
			settings.setProperty("additionalGroupDn",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groupDn", ""));
			settings.setProperty("groupNameAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groupNameAttr", "cn"));
			settings.setProperty("groupMemberAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groupMemberAttr", "member"));
			settings.setProperty("groupSubgroupAttr",
					Play.configuration.getProperty("mindsmash.auth.authenticator.ldap.groupMemberGroupAttr", "member"));

			settings.save();
			JPAUtils.makeTransactionWritable();
		}

		return settings;
	}

	/**
	 * Tests if LDAP connection is working.
	 *
	 * @throws Exception
	 */
	public static boolean testConnection(String url, String username, String password) throws Exception {
		try {
			LdapContextSource source = createLdapSource(url, "", username, password, false);
			LdapTemplate template = new LdapTemplate(source);
			template.afterPropertiesSet();
			template.lookup("");

			return true;
		} catch (Exception e) {
			Logger.info(e, "[LDAP] Connection test failed");
			throw e;
		}
	}

	/**
	 * Syncs all users into the internal DB. Orphan users (auto-deletion) are not currently supported.
	 *
	 * @throws LDAPException
	 */
	public static void syncAllUsers(boolean syncProfiles) throws LDAPException {
		final Settings settings = loadLDAPSettings();

		// load all ldap users
		final List<DirContextAdapter> all = search(
				settings.getString("additionalUserDn"),
				"(&(objectclass=" + settings.getString("userObjectClass") + ")"
						+ settings.getString("userObjectFilter") + ")");

		// load all users ids from database which authSource = LDAP
		final boolean deleteOrphanedObjects = settings.getBoolean("deleteOrphanedLDAPObjects");
		final List<Long> orphanedUsers;

		if (deleteOrphanedObjects) {
			orphanedUsers = User.findUsersIdsByAuthSource(SOURCE);
		} else {
			orphanedUsers = new ArrayList<>();
		}

		if (Logger.isDebugEnabled()) {
			Logger.debug("[LDAP] Starting sync of %s users ...", all.size());
		}

		int i = 0;
		for (DirContextAdapter ctx : all) {
			final User user = syncUser(settings, ctx, syncProfiles);

			if (user != null) {
				orphanedUsers.remove(user.id);
			}

			if (i++ % 100 == 0) {
				JPA.em().flush();
				if (Logger.isDebugEnabled()) {
					Logger.debug("[LDAP] Done with %s users ...", i);
				}
			}
		}

		Logger.debug("[LDAP] Found %d orphaned user objects.", orphanedUsers.size());

		if (deleteOrphanedObjects) {
			deleteOrphanedUsers(orphanedUsers);
		}
	}

	/**
	 * Syncs a LDAP user with the internal DB.
	 *
	 * @param username
	 * @param syncProfile True to sync additional profile fields.
	 * @return Created or found user or null if user does not exist in DB.
	 * @throws LDAPException
	 */
	public static User syncUser(String username, boolean syncProfile) throws LDAPException {
		return syncUser(loadLDAPSettings(), loadUser(createUserFilterByAuth(username)), syncProfile);
	}

	/**
	 * Syncs a LDAP user with the internal DB.
	 *
	 * @param settings
	 * @param ctx
	 * @param syncProfile True to sync additional profile fields.
	 * @return Created or found user or null if user does not exist in DB.
	 */
	protected static User syncUser(Settings settings, DirContextAdapter ctx, boolean syncProfile) {
		final Attributes attrs = ctx.getAttributes();
		final String uid = LDAPConnector.getAttributeValue(attrs, settings.getString("userUIDAttr"));

		// try to load user
		User user = getUserByLDAPCtx(ctx);
		boolean created = false;

		// create if not exists
		if (user == null) {
			if (settings.getBoolean("autocreate")) {
				Logger.debug("[LDAP] User [%s] unknown, creating ...", uid);

				created = true;
				user = new User();
				user.authUid = uid;
				user.authSource = LDAPConnector.SOURCE;
				user.password = "";
				user.language = Lang.getLocale();
				user.active = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOACTIVATION);
				user.role = UserRole.findDefaultRole();
			} else {
				Logger.info("[LDAP] User [%s] not found in internal DB. Auto-creation is disabled.", uid);
				return null;
			}
		}

		// newly created users must always be synched
		if (created || syncProfile) {
			// sync
			Logger.debug("[LDAP] LDAP user [%s] known, sync", uid);

			user.email = LDAPConnector.getAttributeValue(attrs, settings.getString("userEmailAttr"));
			user.firstName = LDAPConnector.getAttributeValue(attrs, settings.getString("userFirstNameAttr"));
			user.lastName = LDAPConnector.getAttributeValue(attrs, settings.getString("userLastNameAttr"));

			try {
				if (syncer != null) {
					syncer.syncUserProfile(attrs, user);
				}
			} catch (Exception e) {
				Logger.error(e, "[LDAP] Error synching user profile [%s]", user);
			}

			// save
			if (!user.validateAndSave()) {
				List<String> errors = new ArrayList<>();
				for (play.data.validation.Error error : Validation.errors()) {
					errors.add(error.getKey() + "=" + error.message());
				}
				Logger.error("[LDAP] Could not create LDAP user [%s] because validation failed with errors: %s", user,
						Util.implode(errors, ", "));
				return null;
			}

			// sync groups
			try {
				LDAPConnector.syncGroups(user);
			} catch (Exception e) {
				Logger.error(e, "[LDAP] Error synching LDAP groups for user [%s]", user);
			}

			// default group on create
			if (created) {
				UserGroup defaultGroup = UserGroup.findDefaultGroup();
				if (defaultGroup != null) {
					defaultGroup.addUser(user);
				}
			}
		}

		return user;
	}

	/**
	 * Deletes orphaned groups
	 *
	 * @param orphanedUsers list of orphaned user objects (present in database but not within the ldap)
	 */
	private static void deleteOrphanedUsers(final List<Long> orphanedUsers) {
		for (Long id : orphanedUsers) {
			final User user = User.findById(id);

			if (user == null) {
				Logger.debug("[LDAP] Failed to delete orphaned user object with id: %d.", id);
				continue;

			} else {
				Logger.debug("[LDAP] Deleted orphaned user object with id: %d.", id);
				user.delete();
			}
		}
	}

	/**
	 * tries to load user object using directory DirContextAdapter
	 *
	 * @param ctx DirContextAdapter
	 * @return User
	 */
	private static User getUserByLDAPCtx(final DirContextAdapter ctx) {
		final Settings settings = loadLDAPSettings();

		if (!ctx.attributeExists(settings.getString("userUIDAttr"))) {
			return null;
		}
		// fetch user uid from ldap user obj
		final String userUID = getAttributeValue(ctx.getAttributes(), settings.getString("userUIDAttr"));

		if (StringUtils.isEmpty(userUID)) {
			return null;
		}

		return getUserByUid(userUID);
	}

	/**
	 * Syncs all groups into the internal DB. Orphan groups (auto-deletion) are not currently supported.
	 *
	 * @throws LDAPException
	 */
	public static void syncAllGroups() throws LDAPException {
		Settings settings = loadLDAPSettings();

		if (!settings.getBoolean("groupSync")) {
			Logger.warn("[LDAP] Cannot sync groups because group synching is disabled");
			return;
		}

		List<DirContextAdapter> all = search(
				settings.getString("additionalGroupDn"),
				"(&(objectclass=" + settings.getString("groupObjectClass") + ")"
						+ settings.getString("groupObjectFilter") + ")");


		final boolean deleteOrphanedObjects = settings.getBoolean("deleteOrphanedLDAPObjects");
		final List<Long> orphanedGroups;

		if (deleteOrphanedObjects) {
			orphanedGroups = StaticUserGroup.findGroupIdsBySource(SOURCE);
		} else {
			orphanedGroups = new ArrayList<>();
		}

		if (Logger.isDebugEnabled()) {
			Logger.debug("[LDAP] Starting sync of %s groups ...", all.size());
		}

		int i = 0;
		for (DirContextAdapter ctx : all) {
			syncGroup(settings, ctx, orphanedGroups);

			if (i++ % 100 == 0) {
				if (Logger.isDebugEnabled()) {
					Logger.debug("[LDAP] Done with %s groups ...", i);
				}
				JPA.em().flush();
			}
		}

		if (deleteOrphanedObjects) {
			deleteOrphanedGroups(orphanedGroups);
		}
	}

	protected static void syncGroup(Settings settings, DirContextAdapter ctx, List<Long> orphanedGroups) {
		String groupDn = ctx.getNameInNamespace();

		try {
			final String name = ctx.getAttributes().get(settings.getString("groupNameAttr")).get() + "";

			if (Logger.isDebugEnabled()) {
				Logger.debug("[LDAP] Found group [%s] with dn [%s]", name, groupDn);
			}

			StaticUserGroup group = StaticUserGroup.find("directoryUid = ? AND directorySource = ?", groupDn, SOURCE)
					.first();

			if (group == null) {
				if (Logger.isDebugEnabled()) {
					Logger.debug("[LDAP] Creating LDAP group [%s]", name);
				}

				group = new StaticUserGroup();
				group.directorySource = SOURCE;
				group.directoryUid = groupDn;
			}

			group.name = name;
			group.save();

			orphanedGroups.remove(group.id);

		} catch (Exception e) {
			Logger.warn(e, "[LDAP] Could not sync group [%s]", groupDn);
		}
	}


	/**
	 * Deletes orphaned groups
	 *
	 * @param orphanedGroups list of orphaned group entries that are present in database but within the ldap
	 */
	private static void deleteOrphanedGroups(final List<Long> orphanedGroups) {
		for (Long id : orphanedGroups) {
			final StaticUserGroup group = StaticUserGroup.findById(id);

			if (group == null) {
				Logger.debug("[LDAP] Failed to retrieve orphaned user with id: %d.", id);
				continue;

			} else {
				Logger.debug("[LDAP] Deleted orphaned user with id: %d.", id);
				group.delete();

			}
		}
	}

	/**
	 * Syncs the user into his groups.
	 *
	 * @param user
	 * @throws NamingException
	 */
	protected static void syncGroups(User user) throws NamingException {
		Settings settings = loadLDAPSettings();

		if (!settings.getBoolean("groupSync")) {
			return;
		}

		if (!SOURCE.equals(user.authSource)) {
			Logger.warn("[LDAP] Cannot sync groups because user [%s] is not a LDAP user", user);
			return;
		}

		try {
			DirContextAdapter userCtx = loadUser(createUserFilterByUid(user.authUid));
			String userDn = userCtx.getNameInNamespace();
			Logger.debug("[LDAP] Syncing groups for user [%s], also known with user dn [%s]", user, userDn);

			// lookup all groups of the user
			List<DirContextAdapter> userGroups = search(settings.getString("additionalGroupDn"), "(&(objectclass="
					+ settings.getString("groupObjectClass") + ")(" + settings.getString("groupMemberAttr") + "="
					+ LdapEncoder.nameEncode(userDn) + ")" + settings.getString("groupObjectFilter") + ")");

			// remove user from all existing directory groups first
			for (UserGroup group : user.groups) {
				if (SOURCE.equals(((StaticUserGroup) group).directorySource)) {
					group.users.remove(user);
					group.save();
				}
			}

			// iterate groups and add user
			for (DirContextAdapter ctx : userGroups) {
				// sync this group with user and also sync the entire parent
				// group tree
				syncGroupMembership(ctx, user);
			}
		} catch (Exception e) {
			Logger.error(e, "[LDAP] Error syncing LDAP groups for user [%s]", user);
		}
	}

	protected static void syncGroupMembership(DirContextAdapter groupCtx, User user) {
		String groupDn = groupCtx.getNameInNamespace();
		try {
			Settings settings = loadLDAPSettings();

			String name = groupCtx.getAttributes().get(settings.getString("groupNameAttr")).get() + "";

			if (Logger.isDebugEnabled()) {
				Logger.debug("[LDAP] User is in group [%s] with dn [%s]", name, groupDn);
			}

			// add user to group
			StaticUserGroup group = StaticUserGroup.find("directoryUid = ? AND directorySource = ?", groupDn, SOURCE)
					.first();

			if (group == null) {
				Logger.info(
						"[LDAP] Cannot add user [%s] to group [%s] because group does not exist in DB, yet. Sync groups first?",
						user, groupDn);
				return;
			}

			group.users.add(user);
			group.save();

			// now check if the group is member of other groups
			List<DirContextAdapter> parentGroups = search(settings.getString("additionalGroupDn"), "(&(objectclass="
					+ settings.getString("groupObjectClass") + ")(" + settings.getString("groupSubgroupAttr") + "="
					+ LdapEncoder.nameEncode(groupDn) + ")" + settings.getString("groupObjectFilter") + ")");

			for (DirContextAdapter ctx : parentGroups) {
				syncGroupMembership(ctx, user);
			}
		} catch (Exception e) {
			Logger.warn(e, "[LDAP] Error synching group membership for group [%s] and user [%s]. Skipping group ...",
					groupDn, user);
		}
	}

	/**
	 * Tries to authenticate a user against LDAP.
	 *
	 * @param username
	 * @param password
	 * @return True if successful.
	 * @throws LDAPException
	 */
	public static boolean authenticate(String username, String password) throws LDAPException {
		String filter = createUserFilterByAuth(username);

		if (Logger.isDebugEnabled()) {
			Logger.debug("[LDAP] Trying to authenticate user [%s] with filter [%s]", username, filter);
		}

		return getTemplate().authenticate("", filter, password);
	}

	/**
	 * Loads the LDAP attributes for user with given username (authentication name).
	 *
	 * @param username
	 * @return
	 * @throws LDAPException
	 */
	public static Attributes loadUserAttributes(String username) throws LDAPException {
		return loadUser(createUserFilterByAuth(username)).getAttributes();
	}

	/**
	 * Loads the UID of a user by using the authentication name.
	 *
	 * @param username
	 * @return UID or null if not found.
	 */
	public static String getUserUid(String username) {
		Settings settings = loadLDAPSettings();

		// check if auth and UID are the same, then we can simply use the
		// username
		if (settings.getString("userUIDAttr").equals(settings.getString("userAuthAttr"))) {
			return username;
		}

		try {
			DirContextAdapter ctx = loadUser(createUserFilterByAuth(username));
			Attributes attrs = ctx.getAttributes();

			return getAttributeValue(attrs, settings.getString("userUIDAttr"));
		} catch (Exception e) {
			Logger.error(e, "[LDAP] Error loading LDAP UID for user [%s]", username);
		}

		return null;
	}

	public static User getUserByUid(String authUid) {
		if (authUid == null) {
			return null;
		}
		return User.find("LOWER(authUid) = ? AND authSource = ?", authUid.toLowerCase(), LDAPConnector.SOURCE).first();
	}

	public static User getUserByAuth(String username) {
		return getUserByUid(getUserUid(username));
	}

	/**
	 * Utility method.
	 *
	 * @param attributes
	 * @param key
	 * @return
	 */
	public static String getAttributeValue(Attributes attributes, String key) {
		Attribute attr = attributes.get(key);
		if (attr != null) {
			try {
				return attr.get() + "";
			} catch (NamingException e) {
				Logger.warn(e, "[LDAP] Error loading attribute [%s]", key);
			}
		}
		return null;
	}

	protected static String createUserFilterByAuth(String username) {
		Settings settings = loadLDAPSettings();
		return "(&(objectclass=" + settings.getString("userObjectClass") + ")(" + settings.getString("userAuthAttr")
				+ "=" + username + ")" + settings.getString("userObjectFilter") + ")";
	}

	protected static String createUserFilterByUid(String uid) {
		Settings settings = loadLDAPSettings();
		return "(&(objectclass=" + settings.getString("userObjectClass") + ")(" + settings.getString("userUIDAttr")
				+ "=" + uid + ")" + settings.getString("userObjectFilter") + ")";
	}

	/**
	 * Loads a single LDAP object.
	 *
	 * @param filter
	 * @return
	 * @throws LDAPException
	 */
	protected static DirContextAdapter loadUser(String filter) throws LDAPException {
		Settings settings = loadLDAPSettings();
		List<DirContextAdapter> list = getTemplate().search(settings.getString("additionalUserDn"), filter,
				new SimpleContextMapper());

		if (list.size() == 0) {
			return null;
		} else if (list.size() > 1) {
			throw new LDAPException("Found " + list.size() + " results but expected 1");
		}

		return list.get(0);
	}

	protected static List<DirContextAdapter> search(String base, String filter) throws LDAPException {
		Settings settings = loadLDAPSettings();

		Integer pageSize = 0;
		if (settings.contains("paging")) {
			pageSize = settings.getInteger("paging");
		}

		if (pageSize == 0) {
			// lookup all groups at once
			return getTemplate().search(base, filter, new SimpleContextMapper());

		} else {
			// lookup paged
			LdapTemplate localTemplate = new LdapTemplate(new SingleContextSource(getTemplate().getContextSource()
					.getReadOnlyContext()));

			PagedResultsDirContextProcessor processor = new PagedResultsDirContextProcessor(pageSize);
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			List<DirContextAdapter> all = new ArrayList<>();
			do {
				all.addAll(localTemplate.search(base, filter, searchControls, new SimpleContextMapper(), processor));
				processor = new PagedResultsDirContextProcessor(pageSize, processor.getCookie());
			} while (processor.getCookie().getCookie() != null);

			return all;
		}
	}

	protected static LdapContextSource createLdapSource(String url, String baseDn, String username, String password,
			boolean followReferrals) throws Exception {
		LdapContextSource source = new LdapContextSource();
		source.setUrl(url);
		source.setBase(baseDn);
		source.setUserDn(username);
		source.setPassword(password);
		if (followReferrals) {
			source.setReferral("follow");
		}
		source.afterPropertiesSet();
		return source;
	}

	protected static LdapContextSource createLdapSourceFromSettings() throws Exception {
		Settings settings = loadLDAPSettings();
		return createLdapSource(settings.getString("url"), settings.getString("baseDn"),
				settings.getString("username"), Crypto.decryptAES(settings.getString("password")),
				settings.getBoolean("followReferrals"));
	}

	protected static LdapTemplate getTemplate() throws LDAPException {
		try {
			final LdapTemplate template = new LdapTemplate(createLdapSourceFromSettings());
			template.afterPropertiesSet();

			return template;
		} catch (Exception e) {
			throw new LDAPException("Error creating connection to LDAP", e);
		}
	}

	protected static class SimpleContextMapper implements ContextMapper {

		@Override
		public Object mapFromContext(Object arg0) {
			return arg0;
		}
	}
}

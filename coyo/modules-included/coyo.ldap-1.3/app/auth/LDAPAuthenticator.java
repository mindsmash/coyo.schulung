package auth;

import ldap.LDAPConnector;
import models.User;
import play.Logger;
import play.mvc.Http.Request;
import controllers.Auth;
import exceptions.FlashException;

/**
 * 
 * The {@link Authenticator} to login with LDAP user informations
 * 
 * @author mindsmash GmbH
 * 
 */
public class LDAPAuthenticator extends DefaultAuthenticator implements Authenticator {

	@Override
	public boolean authenticate(String username, String password) throws FlashException {
		// try local user first
		if (super.authenticate(username, password)) {
			return true;
		}

		// try LDAP
		try {
			if (LDAPConnector.authenticate(username, password)) {
				Logger.debug("[LDAP] LDAP authentication successful [%s]", username);

				User user = LDAPConnector.syncUser(username, shouldSyncUser());
				if (user == null) {
					return false;
				}

				return user.isPersistent() && user.active;
			}
		} catch (Exception e) {
			Logger.error(e, "[LDAP] Error performing LDAP search");
		}

		return false;
	}

	protected boolean shouldSyncUser() {
		return Request.current() != null && Request.current().controllerClass == Auth.class;
	}

	@Override
	public boolean canResetPassword(String username) {
		// never allowed for LDAP
		return false;
	}

	@Override
	public void doResetPassword(String username, String passwordResetToken, String password) throws FlashException {
		if (getUser(username).authSource != null) {
			throw new FlashException("auth.passwordResetUnsupported");
		}
		super.doResetPassword(username, passwordResetToken, password);
	}

	@Override
	public void storePasswordResetToken(String username, String passwordResetToken) throws FlashException {
		if (getUser(username).authSource != null) {
			throw new FlashException("auth.passwordResetUnsupported");
		}
		super.storePasswordResetToken(username, passwordResetToken);
	}

	@Override
	public void sendPasswordResetNotification(String username, String passwordResetToken) throws FlashException {
		if (getUser(username).authSource != null) {
			throw new FlashException("auth.passwordResetUnsupported");
		}
		super.sendPasswordResetNotification(username, passwordResetToken);
	}

	@Override
	public boolean isValidPasswordResetToken(String username, String passwordResetToken) throws FlashException {
		if (getUser(username).authSource != null) {
			throw new FlashException("auth.passwordResetUnsupported");
		}
		return super.isValidPasswordResetToken(username, passwordResetToken);
	}

	@Override
	protected User getUser(String username) {
		// try local user first
		User user = super.getUser(username);
		if (user != null) {
			return user;
		}

		// try LDAP
		try {
			String authUid = LDAPConnector.getUserUid(username);
			if (authUid != null) {
				return LDAPConnector.getUserByUid(authUid);
			}
		} catch (Exception e) {
			Logger.warn(e, "[LDAP] Error looking up LDAP user [%s]", username);
		}

		return null;
	}

	@Override
	public Object getId(String username) {
		User user = getUser(username);
		if (user == null) {
			return null;
		}
		return user.id;
	}
}

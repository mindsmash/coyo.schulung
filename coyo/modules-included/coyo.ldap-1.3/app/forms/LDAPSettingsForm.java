package forms;

import java.lang.reflect.Field;

import org.apache.commons.lang.StringUtils;

import models.Settings;
import play.Logger;
import play.data.validation.Required;
import play.libs.Crypto;

public class LDAPSettingsForm {

	public LDAPSettingsForm() {
	}

	public LDAPSettingsForm(Settings settings) {
		for (Field field : LDAPSettingsForm.class.getFields()) {
			try {
				if (settings.contains(field.getName())) {
					if (field.getType().equals(String.class)) {
						field.set(this, settings.getString(field.getName()));
					} else if (field.getType().equals(Integer.class)) {
						field.set(this, settings.getInteger(field.getName()));
					} else if (field.getType().equals(Boolean.class)) {
						field.set(this, new Boolean(settings.getBoolean(field.getName())));
					}
				}
			} catch (Exception e) {
				Logger.error(e, "error loading LDAP settings form. skipping property: %s", field.getName());
			}
		}

		if (!StringUtils.isEmpty(settings.getString("password"))) {
			try {
				password = Crypto.decryptAES(settings.getString("password"));
			} catch (Exception ignored) {
			}
		}
	}

	@Required
	public String url;
	@Required
	public String username;
	@Required
	public String password;

	@Required
	public String baseDn;
	public String additionalUserDn;
	public String additionalGroupDn;

	public Boolean autocreate = true;
	public Boolean groupSync = true;
	public Boolean subgroupSync = true;
	public Boolean followReferrals = true;
	public Boolean deleteOrphanedLDAPObjects = false;

	public Integer paging;

	@Required
	public String userObjectClass = "organizationalPerson";
	public String userObjectFilter;
	@Required
	public String userUIDAttr = "sAMAccountName";
	@Required
	public String userAuthAttr = "sAMAccountName";
	@Required
	public String userEmailAttr = "mail";
	@Required
	public String userFirstNameAttr = "givenName";
	@Required
	public String userLastNameAttr = "sn";
	public String userGroupMemberAttr = "memberOf";

	public String groupObjectClass = "groupOfNames";
	public String groupObjectFilter;
	public String groupNameAttr = "cn";
	public String groupMemberAttr = "member";
	public String groupSubgroupAttr = "member";

	public void store(Settings settings) {
		for (Field field : LDAPSettingsForm.class.getFields()) {
			if ("password".equals(field.getName())) {
				continue;
			}

			try {
				Object o = field.get(this);
				if (o != null) {
					settings.setProperty(field.getName(), o + "");
				}
			} catch (Exception e) {
				Logger.error(e, "error storing LDAP settings form data to settings object. skipping property: %s",
						field.getName());
			}
		}

		if (!StringUtils.isEmpty(password)) {
			settings.setProperty("password", Crypto.encryptAES(password));
		}
	}
}

package controllers.administration;

import jobs.LDAPGroupSync;
import jobs.LDAPUserSync;
import ldap.LDAPConnector;
import models.Settings;
import multitenancy.MTA;
import play.data.validation.Valid;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Util;
import play.templates.TemplateLoader;
import security.Secure;
import acl.Permission;
import auth.LDAPAuthenticator;
import controllers.Auth;
import controllers.WebBaseController;
import forms.LDAPSettingsForm;

/**
 * LDAP settings administration. Requires the LDAP authenticator to be activated
 * in the configuration file.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
@SettingsNav(label = "nav.administration.ldap")
public class LDAP extends WebBaseController {

	/**
	 * Checks if the LDAP authenticator is activated in the configuration.
	 */
	@Before
	static void checkLdapAuthenticator() {
		if (!isLDAPactivated()) {
			flash.error(Messages.get("administration.ldap.authenticatorActivationRequired"));
			flash.keep();
			Users.index();
		}
	}

	public static void index() {
		renderArgs.put("form", new LDAPSettingsForm(LDAPConnector.loadLDAPSettings()));
		render();
	}

	// ajax
	public static void test(String url, String username, String password) {
		boolean success;
		String error = "";

		try {
			success = LDAPConnector.testConnection(url, username, password);
		} catch (Exception e) {
			error = e.getMessage();
			success = false;
		}

		renderJSON("{\"success\":" + success + ",\"error\":\"" + error + "\"}");
	}

	public static void syncGroups() {
		new LDAPGroupSync(MTA.getActiveTenant()).now();
		flash.success(Messages.get("administration.ldap.groupSyncStarted"));
		index();
	}

	public static void syncUsers() {
		new LDAPUserSync(MTA.getActiveTenant()).now();
		flash.success(Messages.get("administration.ldap.userSyncStarted"));
		index();
	}

	@Transactional
	public static void save(@Valid LDAPSettingsForm form) {
		if (validation.hasErrors()) {
			render("@index", form);
		}

		Settings settings = LDAPConnector.loadLDAPSettings();
		form.store(settings);
		settings.save();

		flash.success(Messages.get("administration.ldap.save.success"));
		index();
	}

	@Util
	public static boolean isLDAPactivated() {
		return Auth.getAuthenticator() instanceof LDAPAuthenticator;
	}
}

<!-- Load HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
	<script src="#{resource 'scripts/html5shiv.js' /}"></script>
<![endif]-->

#{loadLess}
	#{addLess 'bootstrap.min' /}
	#{doBody /}
#{/}

<script type="text/javascript" src="#{resource 'scripts/jquery-1.8.3.min.js' /}"></script>
<script type="text/javascript" src="#{resource 'scripts/bootstrap.min.js' /}"></script>
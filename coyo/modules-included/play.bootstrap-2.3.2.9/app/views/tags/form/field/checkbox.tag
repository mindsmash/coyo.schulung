%{
	// custom help position
	_addon = _help;
	_help = '';
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "checked", "labelProperty", "value", "id", "addon", "falseValue", "disabled");
}%
#{form.field}
	<label class="checkbox"><input type="checkbox" name="${_arg}" id="${_id}" value="${_value?.escape()}" ${_checked?.yesno('checked','')}  ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}/> &{_addon}</label>
	#{if _falseValue != null}<input type="hidden" name="${_arg}" value="${_falseValue?.escape()}" />#{/}
	#{doBody /}
#{/}

%{
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "id", "disabled");
}%
#{form.field}
	<input type="password" name="${_arg}" id="${_id}" ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}/>
	#{doBody /}
#{/}

%{
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "checked", "value", "id", "labelProperty", "valueProperty", "disabled");
}%
#{form.field}
	#{doBody /}
	#{list items:_items, as:'item'}
		%{
			value = (_valueProperty && item.hasProperty(_valueProperty))?item[_valueProperty]:item;
			label = (_labelProperty && item.hasProperty(_labelProperty))?item[_labelProperty]:item;
		}%
		<label class="checkbox">
			<input type="checkbox" name="${_arg}" value="${value?.escape()}"#{if _values?.contains(item) || _values?.contains(value)} checked="checked"#{/} ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}>
			${label}
		</label>
	#{/list}	
#{/}

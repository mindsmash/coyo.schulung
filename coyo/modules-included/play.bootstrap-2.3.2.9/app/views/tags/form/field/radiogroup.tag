%{
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "id", "valueProperty", "labelProperty", "value", "disabled");
}%
#{form.field}
	#{doBody /}
	#{list items:_items, as:'item'}
		%{
			value = (_valueProperty && item.hasProperty(_valueProperty))?item[_valueProperty]:item;
			label = (_labelProperty && item.hasProperty(_labelProperty))?item[_labelProperty]:messages.get(item);
		}%
		<label class="radio">
			<input type="radio" name="${_arg}" value="${value?.escape()}"#{if value == _value || item == _value} checked="checked"#{/} ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}>
			${label}
		</label>
	#{/list}	
#{/}

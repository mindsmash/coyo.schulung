%{
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "size", "name", "items", "labelProperty", "value", "valueProperty", "id", "disabled");
}%
#{form.field}
	<select name="${_arg}" id="${_id}" ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}>
		#{doBody /}
		#{list items:_items, as:'item'}
			%{
				value = (_valueProperty && item.hasProperty(_valueProperty))?item[_valueProperty]:item;
				label = (_labelProperty && item.hasProperty(_labelProperty))?item[_labelProperty]:item;
			}%
			<option value="${value?.escape()}"#{if value == _value || _values?.contains(item) || _values?.contains(value)} selected="selected"#{/}>${label?.escape()}</option>
		#{/list}	
	</select>
#{/}

%{
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "id", "value", "disabled");
}%
#{form.field}
	<textarea name="${_arg}" id="${_id}" ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}>${_value?.escape()}</textarea>
	#{doBody /}
#{/}

%{
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "value", "id", "disabled");
}%
#{form.field}
	<input type="file" name="${_arg}" id="${_id}" value="${_value}" ${serializedAttrs} #{if _disabled}disabled="disabled"#{/} #{if _accept}accept="${_accept}"#{/}/>
	#{doBody /}
#{/}

%{
	if(!_maxLength) _maxLength = 255;
	if (!_type) _type = 'text';
	if (!_id) _id = _arg?.slugify();
	serializedAttrs  = play.templates.FastTags.serialize(_attrs, "name", "id", "type", "disabled", "value");
}%
#{form.field}
	<input type="${_type}" name="${_arg}" id="${_id}" value="${_value?.escape()}" ${serializedAttrs}  #{if _disabled}disabled="disabled"#{/}/>
	#{doBody /}
#{/}

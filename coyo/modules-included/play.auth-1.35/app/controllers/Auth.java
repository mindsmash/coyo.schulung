package controllers;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import auth.Authenticator;
import auth.ExpiredException;
import auth.UserIdCookieStore;
import auth.UserIdStore;
import csrf.CheckAuthenticity;
import exceptions.FlashException;
import injection.Inject;
import injection.InjectionSupport;
import libs.Protection;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.exceptions.ActionNotFoundException;
import play.i18n.Messages;
import play.libs.Codec;
import play.mvc.Before;
import play.mvc.Catch;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.Util;
import play.mvc.With;
import play.utils.Utils;
import util.HttpUtil;

/**
 * This is the controller of the mindsmash's auth module. It handles login, logout and the password reset process. Also
 * see {@link auth.Authenticator}.
 * 
 * You can enable additional security features by setting the play configuration properties "mindsmash.auth.maxTrials"
 * and "mindsmash.auth.maxTrialsTimeoutMins".
 * 
 * @author mindsmash GmbH
 */
@InjectionSupport
@With(CSRF.class)
public class Auth extends Controller {

	private static final Logger log = LoggerFactory.getLogger(Auth.class);

	private static final String DESTINATION_SESSION_KEY = "destination";

	private static final int MAX_TRIALS = Integer.parseInt(Play.configuration.getProperty("mindsmash.auth.maxTrials",
			"0"));
	private static final int MAX_TIMEOUT = Integer.parseInt(Play.configuration.getProperty(
			"mindsmash.auth.maxTrialsTimeoutMins", "1"));

	@Inject(configuration = "mindsmash.auth.authenticator.class")
	private static Authenticator authenticator;

	@Inject(configuration = "mindsmash.di.auth.userstore", defaultClass = UserIdCookieStore.class, singleton = true)
	private static UserIdStore idStore;

	@Util
	public static String getCurrentId() {
		try {
			return idStore.getCurrentId(Request.current(), Response.current());
		} catch (ExpiredException e) {
			log.info("[Auth] The current session expired. Performing logout.");
			executeLogout();
			flash.error(Messages.get("auth.sessionExpired"));
			flash.keep();
		} catch (Exception e) {
			log.error("[Auth] An error occurred while getting current user session from cookie.", e);
			executeLogout();
		}
		return null;
	}

	/**
	 * Stores the user id in session scope. This is convenience method for backwards compatibility for
	 * {@link #storeUserIdInSession(Long, boolean)}
	 */
	@Util
	public static void storeUserIdInSession(Long userId) {
		storeUserIdInSession(userId, false);
	}

	/**
	 * Stores the user id in session scope. If the "rememberMe" flag is set, the id is stored throughout the scope of a
	 * browsers session.
	 */
	@Util
	public static void storeUserIdInSession(Long userId, boolean rememberMe) {
		try {
			idStore.storeCurrentUserId(Request.current(), Response.current(), Long.toString(userId, 10), rememberMe);
		} catch (Exception e) {
			log.error("[Auth] An error occurred while setting current user session.", e);
			executeLogout();
			flash.error(Messages.get("auth.sessionExpired"));
			flash.keep();
		}
	}

	/**
	 * Stores the destination that the user should be redirected to after login.
	 * 
	 * @param destination
	 */
	@Util
	public static void storeDestinationInSession(String destination) {
		log.debug("[Auth] Storing destination [{}] in session", destination);
		session.put(DESTINATION_SESSION_KEY, destination);
	}

	/**
	 * If a user calls one of this controller's actions (except for 'logout') but is already logged in, something must
	 * have went wrong, so we redirect him to his home page
	 */
	@Before(unless = { "logout" })
	static void checkAlreadyLoggedIn() {
		if (getCurrentId() != null) {
			redirect(getDestination());
		}
	}

	/**
	 * Not an action. Helper method which first stores the redirect destination and then redirects to the normal login
	 * screen.
	 * 
	 * @param destination
	 */
	@Util
	public static void launchLogin(String destination) {
		storeDestinationInSession(destination);
		login();
	}

	/**
	 * Renders login dialog. You can use {@link #storeDestinationInSession(String)} before calling {@link #login()} to
	 * set the redirect destination.
	 */
	public static void login() {
		render();
	}

	/**
	 * Handles login data
	 * 
	 * @param username
	 *            entered username
	 * @param password
	 *            entered password
	 * @throws Exception
	 */
	@Transactional
	@CheckAuthenticity
	public static void authenticate(@Required String username, @Required String password, boolean rememberMe)
			throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("[Auth] Client[{}] is attempting authentication", request.remoteAddress);
		}

		// brute-force protection
		if (MAX_TRIALS > 0 && Protection.max(request.remoteAddress + "@auth", MAX_TRIALS, MAX_TIMEOUT + "min")) {
			log.info("[Auth] Client [{}] has tried to authenticate too many times - denying login",
					request.remoteAddress);

			params.flash("username");
			flash.error(Messages.get("auth.login.timeout"));
			login();
		}

		// validate submission
		if (Validation.hasErrors()) {
			params.flash("username");
			Validation.keep();
			login();
		}

		// validate password
		if (authenticator.authenticate(username, password)) {
			// success
			flash.success(Messages.get("auth.login.success"));
			succeedAuthentication(username, rememberMe);
		}

		params.flash("username");
		flash.error(Messages.get("auth.login.failed"));

		if (log.isDebugEnabled()) {
			log.debug("[Auth] Client [{}] failed authentication", request.remoteAddress);
		}

		login();
	}

	/**
	 * Logs the current user out
	 * 
	 * @throws Exception
	 * @throws ClassNotFoundException
	 * @throws InvocationTargetException
	 */
	public static void logout() {
		try {
			authenticator.beforeLogout();
		} catch (Exception e) {
			log.error("[Auth] beforeLogout failed", e);
		}

		executeLogout();

		try {
			authenticator.afterLogout();
		} catch (Exception e) {
			log.error("[Auth] afterLogout failed", e);
		}
		flash.success(Messages.get("auth.logout.success"));
		login();
	}

	@Util
	public static void executeLogout() {
		log.debug("[Auth] Executing logout...");

		session.clear();
		params.data.clear();
		params.flash();
		flash.clear();

		Response.current().setHeader("Cache-control", "no-cache, no-store, must-revalidate");
		Response.current().setHeader("Pragma", "no-cache");
		Response.current().setHeader("Expires", Utils.getHttpDateFormatter().format(new Date(0)));

		idStore.clear(Response.current());
	}

	/**
	 * Renders forgot password dialog
	 */
	public static void forgotPassword() {
		render();
	}

	/**
	 * Handles forgot password data
	 * 
	 * @param email
	 *            entered email
	 * @throws Exception
	 */
	@Transactional
	@CheckAuthenticity
	public static void sendPassword(@Required String username) throws Exception {
		// validate submission
		if (Validation.hasErrors()) {
			params.flash();
			Validation.keep();
			forgotPassword();
		}

		// brute-force protection
		if (MAX_TRIALS > 0 && Protection.max(request.remoteAddress + "@sendPassword", MAX_TRIALS, MAX_TIMEOUT + "min")) {
			log.info("[Auth] Client [{}] has tried to send password too many times - denying access",
					request.remoteAddress);

			params.flash();
			flash.error(Messages.get("auth.forgotPassword.timeout"));
			forgotPassword();
		}

		if (authenticator.canResetPassword(username)) {
			enableResetPassword(username, true);
		}
		flash.success(Messages.get("auth.forgotPassword.response"));
		login();
	}

	@Util
	public static void succeedAuthentication(String username, boolean remember) {
		authenticator.beforeLogin(username);

		Object userId = authenticator.getId(username);

		if (userId == null) {
			log.warn("[Auth] Cannot complete authentication because no user id was found for username [{}]", username);
			flash.error(Messages.get("auth.login.failed"));
			redirect("/");
		}

		log.debug("[Auth] Authentication successful. Creating session for user with name [{}] and ID [{}].", username,
				userId);

		storeUserIdInSession(Long.valueOf(userId.toString()), remember);

		authenticator.afterLogin(username);

		try {
			redirect(getDestination());
		} catch (ActionNotFoundException e) {
			redirect("/");
		}
	}

	/**
	 * Refreshes the user session if needed.
	 */
	@Util
	public static void refresh() {
		try {
			idStore.refresh(request, response);
		} catch (Exception e) {
			log.error("[Auth] Error refreshing the current session.");
		}
	}

	@Util
	public static String getDestination() {
		if (session.contains(DESTINATION_SESSION_KEY)) {
			String destination = session.get(DESTINATION_SESSION_KEY);
			session.remove(DESTINATION_SESSION_KEY);
			return destination;
		}
		return "/";
	}

	@Util
	public static String enableResetPassword(String username, boolean mail) throws FlashException {
		// validate username
		if (authenticator.getId(username) == null) {
			redirect("/");
		}

		// generate password reset token
		String passwordResetToken = Codec.UUID().substring(0, 8);
		authenticator.storePasswordResetToken(username, passwordResetToken);

		// send mail
		if (mail) {
			authenticator.sendPasswordResetNotification(username, passwordResetToken);
		}

		return passwordResetToken;
	}

	/**
	 * Renders reset password dialog, where the user can enter a new password in case he forgot it.
	 * 
	 * @param username
	 * @param token
	 *            generated reset token
	 * @throws Exception
	 */
	public static void resetPassword(String username, String token) throws Exception {
		// validate token
		if (username == null || token == null || !authenticator.isValidPasswordResetToken(username, token)) {
			redirect("/");
		}

		render(username, token);
	}

	/**
	 * handles reset password data
	 * 
	 * @param username
	 * @param token
	 *            generated reset token
	 * @param password
	 *            new password
	 * @param passwordRepeat
	 *            repeated new password
	 * @throws Exception
	 */
	@Transactional
	@CheckAuthenticity
	public static void doResetPassword(String username, String token, @Required String password,
			@Required String passwordRepeat) throws Exception {
		// validate token
		if (username == null || token == null || !authenticator.isValidPasswordResetToken(username, token)) {
			redirect("/");
		}

		// validate submission
		validation.equals(passwordRepeat, password);
		if (!authenticator.isPasswordSecure(password)) {
			validation.addError("password", "validation.passwordSecurity");
		}
		if (Validation.hasErrors()) {
			Validation.keep();
			resetPassword(username, token);
		}

		// save new password
		authenticator.doResetPassword(username, token, password);

		flash.success(Messages.get("auth.resetPassword.success"));
		succeedAuthentication(username, false);
	}

	public static Authenticator getAuthenticator() {
		return authenticator;
	}

	/**
	 * Allows your authenticator implementation to display custom error messages, e.g. to inform users why
	 * authentication failed.
	 * 
	 * @param e
	 */
	@Catch(FlashException.class)
	public static void handleFlashExceptions(FlashException e) {
		flash.error(e.getMessage());
		redirect(HttpUtil.getReferrerUrl());
	}
}
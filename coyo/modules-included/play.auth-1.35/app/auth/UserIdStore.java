package auth;

import play.mvc.Http.Request;
import play.mvc.Http.Response;

/**
 * An interface to describe a store to save the id of the current user throughout a session. The id might be stored in a
 * cookie, a session or elsewhere. Stores must take care of serializing, de-serializing, encrypting and de-crypting the
 * user id. In addition the store must implement an expiration strategy.
 * 
 * @see UserIdCookieStore
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public interface UserIdStore {

	/**
	 * Retrieves id of the currently connected user from the store. If no id can be retrieved "null" is returned.
	 * 
	 * @param request
	 *            the current http request. Must not be null.
	 * @param response
	 *            the current http response. Must not be null.
	 * @return the current user id or null.
	 * @throws ExpiredException
	 *             this exception is thrown if the store or the included id is expired. Implementations are needed to
	 *             take care of a suitable expiration strategy.
	 * @throws Exception
	 *             if anything goes wrong with retrieving the id, a general exception must be thrown. In this case the
	 *             calling controller can react accordingly.
	 */
	String getCurrentId(Request request, Response response) throws ExpiredException, Exception;

	/**
	 * Stores the id of the currently connecting user within the store.
	 * 
	 * @param response
	 *            the current http response. Must not be null.
	 * @param userId
	 *            the user id to store
	 * @param remember
	 *            if set to true, the id must be stored beyond session scope. If set to false the id should be cleared
	 *            if the session is cleared - e.g. if the browser window is closed.
	 * 
	 * @throws Exception
	 *             if anything goes wrong while storing the user id an exception must be thrown. In this case the
	 *             calling controller can react accordingly.
	 */
	void storeCurrentUserId(Request request, Response response, String userId, boolean remember) throws Exception;

	/**
	 * Checks and refreshes the user session if needed e.g. if the session is about to expire it should be renewed.
	 * Sessions could also be renewed in a specified interval.
	 * 
	 * @param request
	 *            the current http request. Must not be null.
	 * @param response
	 *            the current http response. Must not be null.
	 * @throws Exception
	 *             if anything goes wron while refreshing the session, an exception is thrown. In this case the calling
	 *             controller can react accordingly.
	 */
	void refresh(Request request, Response response) throws Exception;

	/**
	 * Clear the store. E.g. in case of a logout.
	 * 
	 * @param response
	 *            the current http response. Must not be null.
	 */
	void clear(Response response);
}
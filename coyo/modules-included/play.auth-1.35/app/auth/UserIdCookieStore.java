package auth;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Play;
import play.libs.Crypto;
import play.libs.Time;
import play.mvc.CookieDataCodec;
import play.mvc.Http.Cookie;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.Scope;

import com.google.common.collect.Maps;

/**
 * Implementation of a store for the current user id in a cookie. The id gets AES encrypted and has a dynamic expiration
 * date set within the cookie's value.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class UserIdCookieStore implements UserIdStore {

	/**
	 * Defines how long the stored cookie value is valid. This information is encrypted into the cookie and cannot be
	 * spoofed. If null, the session will be valid forever.
	 */
	private static final String SESSION_EXPIRATION = Play.configuration.getProperty("mindsmash.auth.sessionExpiration");

	/**
	 * Update the session after the set space of time ran off. This only has an effect if
	 * {@link UserIdCookieStore#SESSION_EXPIRATION} is set. If the update interval is not set, the session never gets
	 * refreshed automatically.
	 */
	private static final String SESSION_UPDATE_INTERVAL = Play.configuration
			.getProperty("mindsmash.auth.sessionUpdateInterval");

	private static final String COOKIE_VAL_ID_KEY = "id";
	private static final String COOKIE_VAL_CREATED_KEY = "created";

	/**
	 * Name of the cookie
	 */
	private static final String COOKIE_NAME = Scope.COOKIE_PREFIX + "_USER";;

	/**
	 * Defines how long the cookie is valid
	 */
	private static final String COOKIE_EXPIRATION = Play.configuration.getProperty("mindsmash.auth.cookieExpiration",
			"30d");

	private static final String COOKIE_DOMAIN = Play.configuration.getProperty("mindsmash.auth.cookieDomain");
	private static final String COOKIE_PATH = Play.configuration.getProperty("mindsmash.auth.cookiePath", "/");
	private static final boolean COOKIE_SECURE = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.auth.cookieSecure", "false"));
	private static final boolean COOKIE_HTTP_ONLY = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.auth.cookieHttpOnly", "true"));

	private static final Logger log = LoggerFactory.getLogger(UserIdCookieStore.class);

	@Override
	public String getCurrentId(Request request, Response response) throws ExpiredException, Exception {
		if (request == null || response == null) {
			log.warn("[CookieStore] No user id could be found. No request or response available.");
			return null;
		}

		Cookie userCookie = getUserCookie(request, response);
		Map<String, String> cookieData = getUserCookieData(userCookie);
		String userId = cookieData.get(COOKIE_VAL_ID_KEY);

		if (StringUtils.isEmpty(userId)) {
			log.debug("[CookieStore] No user id could be found in Cookie.");
			return null;
		}

		if (isExpired(cookieData)) {
			log.debug("[CookieStore] Session expired.");
			throw new ExpiredException();
		}

		log.trace("[CookieStore] Session not expired. Returning user id [{}]", userId);
		return userId;
	}

	@Override
	public void storeCurrentUserId(Request request, Response response, String userId, boolean remember)
			throws Exception {

		checkUserSession(request, response, userId);

		Map<String, String> cookieData = new HashMap();
		cookieData.put(COOKIE_VAL_ID_KEY, String.valueOf(userId));
		cookieData.put(COOKIE_VAL_CREATED_KEY, String.valueOf(System.currentTimeMillis()));

		String encodedCookieValue = CookieDataCodec.encode(cookieData);
		String encryptedCookieValue = Crypto.encryptAES(encodedCookieValue);
		Integer maxAge = (remember) ? Time.parseDuration(COOKIE_EXPIRATION) : null;

		Object[] args = { maxAge, userId, encodedCookieValue };
		log.debug("[CookieStore] Setting session cookie with max age [{}] for user with id [{}] with data: {}", args);

		response.setCookie(COOKIE_NAME, encryptedCookieValue, COOKIE_DOMAIN, COOKIE_PATH, maxAge, COOKIE_SECURE,
				COOKIE_HTTP_ONLY);
	}

	@Override
	public void refresh(Request request, Response response) throws Exception {
		/* if no update interval is set, never refresh session */
		if (StringUtils.isEmpty(SESSION_UPDATE_INTERVAL)) {
			return;
		}

		Cookie userCookie = getUserCookie(request, response);
		Map<String, String> cookieData = getUserCookieData(userCookie);

		if (!cookieData.isEmpty() && !isExpired(cookieData)) {
			DateTime creationDate = new DateTime(Long.parseLong(cookieData.get(COOKIE_VAL_CREATED_KEY)));
			long cookieAge = DateTime.now().getMillis() - creationDate.getMillis();
			long intervalTime = Time.parseDuration(SESSION_UPDATE_INTERVAL) * 1000L;
			if (cookieAge > intervalTime) {
				log.debug("[CookieStore] Renewing the session for request [] and action []", request.url,
						request.action);
				String userId = cookieData.get(COOKIE_VAL_ID_KEY);
				storeCurrentUserId(request, response, userId, userCookie.maxAge != null);
			}
		}

	}

	@Override
	public void clear(Response response) {
		response.removeCookie(COOKIE_NAME);
	}

	/**
	 * Check here whether the id changes from request to response. This is a security mechanism to prevent switching
	 * user sessions. If a new user session is about to be created within a valid session of another user, throw an
	 * exception.
	 */
	private void checkUserSession(Request request, Response response, String userId) throws Exception {
		Cookie userCookie = getUserCookie(request, response);
		Map<String, String> cookieData = getUserCookieData(userCookie);
		String currentId = cookieData.get(COOKIE_VAL_ID_KEY);
		if (StringUtils.isNotEmpty(currentId) && !currentId.equals(userId)) {
			Object[] args = { currentId, userId, request.url };
			log.warn(
					"[CookieStore] Session about to be switched from user [{}] to user [{}]. This is an illegal operation. Request: {}",
					args);
			throw new IllegalStateException("Creating a new user session within a valid session is not allowed.");
		}
	}

	private boolean isExpired(Map<String, String> cookieData) {
		if (StringUtils.isEmpty(SESSION_EXPIRATION)) {
			return false;
		} else {
			DateTime now = DateTime.now();
			DateTime creationDate = new DateTime(Long.parseLong(cookieData.get(COOKIE_VAL_CREATED_KEY)));
			DateTime expirationTime = creationDate.plusSeconds(Time.parseDuration(SESSION_EXPIRATION));

			/* if session is expired */
			return (expirationTime.isBefore(now));
		}
	}

	/**
	 * Get data from user cookie. The data is AES coded and must be decoded before. If no cookie could be found and
	 * empty map is returned.
	 */
	private Map<String, String> getUserCookieData(Cookie userCookie) throws UnsupportedEncodingException {
		if (userCookie != null && StringUtils.isNotEmpty(userCookie.value)) {
			Map<String, String> cookieData = new HashMap();
			CookieDataCodec.decode(cookieData, Crypto.decryptAES(userCookie.value));
			return cookieData;
		}

		log.debug("[CookieStore] No user id could be found. Cookie not available.");
		return Maps.newHashMap();
	}

	/**
	 * Tries to read the user cookie. Trying response first as during login it could be that an empty cookie is still
	 * stored within the request but the response contains a valid cookie.
	 *
	 * @param request user cookie
	 * @param response user cookie
	 * @return
	 */
	private Cookie getUserCookie(Request request, Response response) {
		if (response.cookies.containsKey(COOKIE_NAME)) {
			return response.cookies.get(COOKIE_NAME);

		} else 	if (request.cookies.containsKey(COOKIE_NAME)) {
			return request.cookies.get(COOKIE_NAME);
			
		} else {
			return null;
		}
	}

}
package auth;

import controllers.Auth;
import exceptions.FlashException;

/**
 * Implement and configure this class in your application to enable
 * authentication. Set the classpath of your implementation in your play config
 * at "mindsmash.auth.authenticator.class".
 * 
 * @author mindsmash GmbH
 */
public interface Authenticator {

	/**
	 * Authenticates a user.
	 * 
	 * @param username
	 *            A string username, e.g. email or name
	 * @param password
	 *            Not encrypted.
	 * @return True if authentication succeeded
	 */
	public boolean authenticate(String username, String password) throws FlashException;

	/**
	 * Called before the logout is executed.
	 */
	public void beforeLogout();

	/**
	 * Called after a user logs out.
	 */
	public void afterLogout();

	/**
	 * Called before the authentication process is completed. This can be
	 * used to synchronize the user with an external directory or even 
	 * create the user automatically, if it does not exist as a local user model.
	 * 
	 * @param username
	 */
	public void beforeLogin(String username);

	/**
	 * Called after a user successfully logs in.
	 */
	public void afterLogin(String username);

	/**
	 * Get the ID of a user by username. This ID will be stored in the auth
	 * session.
	 * 
	 * @param username
	 * @return ID of the user with given username
	 */
	public Object getId(String username);

	/**
	 * Called when a user sets a new password. If returning false the reset
	 * password action will not succeed. You can check the user's password for
	 * length or other patterns here.
	 * 
	 * @param password
	 * @return True if password is secure
	 */
	public boolean isPasswordSecure(String password);

	/**
	 * Checks if a user is allowed to reset their password.
	 * 
	 * @param username
	 * @return True if allowed.
	 */
	public boolean canResetPassword(String username);

	/**
	 * Sets and saves a new password for the user with given token.
	 * 
	 * @param passwordResetToken
	 *            A validation token for a user
	 * @param password
	 *            not encrypted
	 */
	public void doResetPassword(String username, String passwordResetToken, String password) throws FlashException;

	/**
	 * Sets and saves a new unique token for the user with given username.
	 * 
	 * @param username
	 * @param passwordResetToken
	 *            A validation token for a user
	 */
	public void storePasswordResetToken(String username, String passwordResetToken) throws FlashException;

	/**
	 * Sends a reset password email to the user with given username. That email
	 * should best contain a password reset link for the
	 * {@link Auth#resetPassword(String, String)} action.
	 * 
	 * @param username
	 * @param passwordResetToken
	 *            A validation token for a user
	 */
	public void sendPasswordResetNotification(String username, String passwordResetToken) throws FlashException;

	/**
	 * Valdiates a given password reset token with a username.
	 * 
	 * @param username
	 * @param passwordResetToken
	 * @return True if the token matches the username
	 */
	public boolean isValidPasswordResetToken(String username, String passwordResetToken) throws FlashException;
}
package auth;

/**
 * This exception gets thrown if a user id expired.
 * 
 * @see UserIdStore
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class ExpiredException extends Exception {
	
}
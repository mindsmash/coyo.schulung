package plugins.play;

import fishr.AsyncIndexer;
import fishr.Fishr;
import fishr.util.ConvertionUtils;
import injection.Inject;
import injection.InjectionSupport;

import java.util.HashSet;
import java.util.Set;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.db.jpa.GenericModel;
import play.exceptions.UnexpectedException;
import util.ModelUtil;

/**
 * Integrated to Play's lifecycle, SearchPlugin will trap JPA events and drive the Search service.
 * 
 * @author mindsmash GmbH
 */
@InjectionSupport
public class FishrPlugin extends PlayPlugin {

	public static final boolean INDEX_ASYNC = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.fishr.indexAsync", "true"));

	@Inject(configuration = "mindsmash.fishr.indexAsync.indexer", defaultClass = AsyncIndexer.class)
	private static AsyncIndexer indexer;

	/**
	 * Holds the objects to be indexed for the current invocation / action / transaction. The objects in this queue are
	 * represented by their serialization string (see {@link ModelUtil}). They are indexed at the end of each invocation
	 * to ensure that they are correctly persisted.
	 */
	private static ThreadLocal<Set<String>> invocationIndexQueue = new ThreadLocal<Set<String>>();

	/**
	 * If true, indexing is disabled for the current thread (invocation).
	 */
	private static ThreadLocal<Boolean> indexingDisabled = new ThreadLocal<Boolean>();

	/**
	 * Disables search indexing for all elements that are modified in the current transaction / invocation.
	 */
	public static void disableIndexing(String why) {
		Logger.debug("[Fishr] Disabling indexing for the current invocation (%s)", why);
		indexingDisabled.set(true);
	}

	private static boolean isIndexingDisabled() {
		return indexingDisabled.get() != null && indexingDisabled.get();
	}

	@Override
	public void onApplicationStart() {
		Fishr.init();

		if (INDEX_ASYNC) {
			Logger.debug("[Fishr] Using async indexer: %s", indexer);
			indexer.start();
		}
	}

	@Override
	public void onApplicationStop() {
		try {
			Fishr.shutdown();

			if (INDEX_ASYNC) {
				indexer.stop();
			}
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	@Override
	public void beforeInvocation() {
		if (INDEX_ASYNC) {
			invocationIndexQueue.set(new HashSet<String>());
		}
	}

	@Override
	public void afterInvocation() {
		if (INDEX_ASYNC && !isIndexingDisabled()) {
			if (invocationIndexQueue.get() != null && invocationIndexQueue.get().size() > 0) {
				for (String serialized : invocationIndexQueue.get()) {
					indexer.indexQueue.offer(serialized);
				}
				Logger.debug("[Fishr] offering %s objects to async indexer", invocationIndexQueue.get().size());
				invocationIndexQueue.remove();
			}
		}

		if (isIndexingDisabled()) {
			indexingDisabled.remove();
		}
	}

	@Override
	public void onEvent(final String message, final Object context) {
		if (isIndexingDisabled()) {
			return;
		}

		try {
			if (ConvertionUtils.isIndexable(context)
					&& (message.equals("JPASupport.objectPersisted") || message.equals("JPASupport.objectUpdated"))) {
				if (INDEX_ASYNC) {
					invocationIndexQueue.get().add(ModelUtil.serialize((GenericModel) context));
				} else {
					Fishr.index(context);
				}
			} else if (ConvertionUtils.isUnIndexable(context) && message.equals("JPASupport.objectDeleted")) {
				Fishr.unIndex(context);
			}
		} catch (Exception e) {
			Logger.error(e, "[Fishr] error updating search index for: %s", context);
		}
	}
}
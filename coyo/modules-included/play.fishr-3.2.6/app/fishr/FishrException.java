package fishr;

public class FishrException extends RuntimeException {
	public FishrException(String message, Throwable cause) {
		super(message, cause);
	}

	public FishrException(Throwable cause) {
		super(cause);
	}

	public FishrException(String message) {
		super(message);
	}
}

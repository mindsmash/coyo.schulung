package fishr.solr;

import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;

import fishr.query.QueryTerm;

public class SolrQueryTerm extends QueryTerm {

	public SolrQueryTerm(String field, String value, String addon, boolean phrase) {
		super(field, value, addon, phrase);
	}

	@Override
	public String parse() {
		String escaped = QueryParserUtil.escape(value);
		if (phrase) {
			escaped = "\"" + escaped + "\"";
		}
		return field + ":" + escaped + addon;
	}
}

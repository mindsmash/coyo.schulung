package fishr.solr;

import fishr.query.QueryBlock;
import fishr.query.QueryTerm;

public class SolrQueryBlock extends QueryBlock {

	@Override
	protected QueryBlock createBlock() {
		return new SolrQueryBlock();
	}

	@Override
	protected QueryTerm createTerm(String field, String value, String addon, boolean phrase) {
		return new SolrQueryTerm(field, value, addon, phrase);
	}
}

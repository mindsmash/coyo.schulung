package fishr.solr;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import play.db.jpa.JPABase;
import play.exceptions.UnexpectedException;
import fishr.FishrException;
import fishr.query.Query;
import fishr.query.QueryResult;

/**
 * Solr implementation for the query.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class SolrQuery extends Query {

	private org.apache.solr.client.solrj.SolrQuery query;
	private Class clazz;
	private SolrServer server;
	private String[] order;
	private boolean reverse;

	public <T extends JPABase> SolrQuery(Class<T> clazz, SolrServer server) {
		super(new SolrQueryBlock());
		this.server = server;
		query = new org.apache.solr.client.solrj.SolrQuery();
		query.setFilterQueries("searchClass:" + clazz.getName());
		query.setIncludeScore(true);
		this.clazz = clazz;
	}

	@Override
	public Query page(int offset, int pageSize) {
		query.setStart(offset);
		query.setRows(pageSize);
		return this;
	}

	@Override
	public Query reverse() {
		this.reverse = true;
		return this;
	}

	@Override
	public Query orderBy(String... order) {
		this.order = order;
		return this;
	}

	private void sort() {
		if (order != null) {
			for (String o : order) {
				query.addSortField(o, reverse ? ORDER.desc : ORDER.asc);
			}
		}
	}

	@Override
	public <T extends JPABase> List<T> fetch() throws FishrException {
		List<QueryResult> results = execute();
		List<T> resultList = new ArrayList<T>();

		for (QueryResult result : results) {
			JPABase jpaBase = result.getObject();
			if (jpaBase != null) {
				resultList.add((T) jpaBase);
			}
		}

		return resultList;
	}

	@Override
	public List<Long> fetchIds() throws FishrException {
		query.setFields("id");

		List<QueryResult> results = execute();
		List<Long> ids = new ArrayList(results.size());
		for (QueryResult result : results) {
			ids.add(Long.parseLong(result.getId()));
		}
		return ids;
	}

	private SolrDocumentList getResponse() {
		try {
			query.setQuery(get());
			QueryResponse rp = server.query(query);
			return rp.getResults();
		} catch (SolrServerException e) {
			throw new UnexpectedException("Cannot execute Solr query : " + query);
		}
	}

	@Override
	public long count() throws FishrException {
		return getResponse().size();
	}

	@Override
	public List<QueryResult> execute() throws FishrException {
		sort();
		query.setFields("id", "score");
		SolrDocumentList results = getResponse();
		List<QueryResult> queryResults = new ArrayList<QueryResult>();
		for (SolrDocument doc : results) {
			String entityId = doc.getFieldValue("id").toString().split(":")[1];
			queryResults.add(new QueryResult(entityId, clazz, Float.parseFloat("" + doc.get("score"))));
		}

		return queryResults;
	}
}

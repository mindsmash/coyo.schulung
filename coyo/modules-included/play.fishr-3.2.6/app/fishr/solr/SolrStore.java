package fishr.solr;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import fishr.Indexed;
import fishr.Store;
import fishr.query.Query;
import fishr.util.ConvertionUtils;
import fishr.util.ConvertionUtils.IndexableContent;

public class SolrStore implements Store {

	private static final String SOLR_URL = Play.configuration.getProperty("mindsmash.fishr.solr.url",
			"http://localhost:8983/solr");
	private static final int SOLR_COMMIT_WITHIN = Integer.parseInt(Play.configuration.getProperty(
			"mindsmash.fishr.solr.commitWithin", "10000"));

	private SolrServer server;

	@Override
	public void start() throws Exception {
		server = new HttpSolrServer(SOLR_URL);
	}

	@Override
	public void stop() throws Exception {
		if (null != server) {
			server.shutdown();
		}
	}

	@Override
	public void unIndex(Object object) throws Exception {
		JPABase jpaBase = (JPABase) object;
		String entityId = jpaBase.getClass().getName() + ":" + jpaBase._key();
		server.deleteById(entityId, SOLR_COMMIT_WITHIN);
	}

	@Override
	public void index(Object object) throws Exception {
		JPABase model = (JPABase) object;
		server.deleteById(getEntityId(model));
		SolrInputDocument doc = toDocument(model);
		if (doc != null) {
			server.add(doc, SOLR_COMMIT_WITHIN);
		}
	}

	@Override
	public void rebuildAllIndexes() throws Exception {
		List<ApplicationClass> classes = Play.classes.getAnnotatedClasses(Indexed.class);
		for (ApplicationClass applicationClass : classes) {
			rebuild(applicationClass.javaClass.getName());
		}
		Logger.info("Rebuild index finished");

	}

	@Override
	public void rebuild(String name) throws Exception {
		server.deleteByQuery("searchClass:" + name);
		Class cl = Play.classes.getApplicationClass(name).javaClass;

		List<JPABase> objects = JPA.em().createQuery("select e from " + cl.getCanonicalName() + " as e")
				.getResultList();
		for (JPABase jpaBase : objects) {
			SolrInputDocument doc = toDocument(jpaBase);
			if (doc != null) {
				server.add(doc);
			}
		}
		server.commit();
	}

	private String getEntityId(JPABase model) {
		return model.getClass().getName() + ":" + model._key();
	}

	private SolrInputDocument toDocument(JPABase model) throws Exception {
		if (!ConvertionUtils.isIndexable(model))
			return null;

		SolrInputDocument doc = new SolrInputDocument();

		Indexed indexed = model.getClass().getAnnotation(Indexed.class);
		doc.setDocumentBoost(indexed.boost());

		doc.addField("id", getEntityId(model));
		doc.addField("searchClass", model.getClass().getName());

		List<IndexableContent> indexableContents = ConvertionUtils.getIndexableContents(model);
		for (IndexableContent ic : indexableContents) {
			// solr supports other types than only string
			String suffix = getSolrFieldSuffix(ic.originalType);
			String fieldName = ic.name + suffix;

			Object value;
			if ("_t".equals(suffix)) {
				value = ic.content;
			} else {
				value = ic.originalValue;
			}
			doc.addField(fieldName, value);
		}

		return doc;
	}

	public static String getSolrFieldSuffix(Class type) {
		if (type != null) {
			if (type.equals(long.class) || type.equals(Long.class))
				return "_l";
			if (type.equals(int.class) || type.equals(Integer.class))
				return "_i";
			if (type.equals(double.class) || type.equals(Double.class))
				return "_d";
			if (type.equals(float.class) || type.equals(Float.class))
				return "_f";
		}
		return "_t";
	}

	@Override
	public Query createQuery(Class clazz) {
		return new SolrQuery(clazz, server);
	}

	@Override public Map<String, Long> getHashTags(int size, Set<String> blacklist) {
		return null;
	}
}

package fishr.search;

import play.db.Model;

/**
 * Each model class that should be searchable must implement this interface.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public interface SearchableModel extends Model {

	public Long getId();

	/**
	 * Checks whether a user can list this search result.
	 * 
	 * @param user
	 * @return True if the user is permitted to see/list this object.
	 */
	public boolean checkSearchPermission();

//	/**
//	 * Gives you the possibility to return an alternative result for the current searchable. Usually you will simply
//	 * return the current object but if needed you can supply a different result, e.g. a related entity.
//	 * 
//	 * When searching by category, the category of the returned object will be used and not the category of this object.
//	 * The permissions for the returned object are not checked again.
//	 * 
//	 * @return This or some alternate searchable.
//	 */
//	public SearchableModel getSearchResult();
}

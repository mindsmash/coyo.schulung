package fishr.search;

import java.io.InputStream;

/**
 * Have your model attribute classes implement this interface to allow their complex content to be indexed.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public interface ContentIndexable {

	InputStream get();

	long length();

	String type();
}

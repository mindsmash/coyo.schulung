package fishr.search;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.db.jpa.JPABase;

import com.google.common.collect.Sets;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

import fishr.Fishr;
import fishr.query.Query;
import fishr.query.QueryBlock;
import fishr.query.QueryResult;

/**
 * Use this class to perform searches. This class can be extended for custom logic.
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
public class Searcher {

	private static final float FUZZY_FACTOR = 0.8f;

	private static final int QUERY_MAX_SIZE = 500;

	private static Map<String, List<Class<SearchableModel>>> categories;
	private static Map<Class<SearchableModel>, String> reversedCategories;
	private static Map<Class<? extends SearchableModel>, List<Field>> searchFields = new HashMap<Class<? extends SearchableModel>, List<Field>>();

	protected void beforeSearch(String searchString) {
	}

	protected void afterSearch(Object result) {
	}

	protected boolean checkResult(SearchableModel searchable) {
		return searchable.checkSearchPermission();
	}

	public SearchResult search(String searchString) {
		return search(searchString, null);
	}

	/**
	 * Performs a full search across all available search categories, sorted by score.
	 *
	 * @param searchString
	 * @param size
	 *            Maximum results per category.
	 * @return SearchResult
	 */
	public SearchResult search(String searchString, Integer size) {
		Monitor monitor = MonitorFactory.start("search");

		// prepare
		searchString = searchString.trim();
		beforeSearch(searchString);

		final SearchResult result = new SearchResult();
		for (Class<SearchableModel> searchableModelClass : getReversedCategories().keySet()) {
			injectResults(result, searchByModel(searchString, searchableModelClass, size),
					getCategory(searchableModelClass));
		}

		afterSearch(result);
		monitor.stop();
		Logger.debug("[Fishr] Found %d results for [%s] in %s ms.", result.getCount(), searchString,
				monitor.getLastValue());

		return result;
	}

	/**
	 * Performs a quick ahead search. In comparison to the full search, we don't have to respect categories here. In
	 * order to speed things up, we are simply fetching all results we can find, sort them and add them up. Then we are
	 * starting to filter the results by permissions as soon we have 'max' results. This way we only have to instantiate
	 * and check a couple of items.
	 */
	public SearchResult searchAhead(String searchString, Integer max) {
		Monitor monitor = MonitorFactory.start("searchAhead");

		// prepare
		searchString = searchString.trim();
		beforeSearch(searchString);

		// query for all results of all categories we can find
		Set<QueryResult> queryResults = new TreeSet<>();
		for (Class<SearchableModel> searchableModelClass : getReversedCategories().keySet()) {
			Query query = prepareSearchQuery(searchString, searchableModelClass);
			query.page(0, QUERY_MAX_SIZE);
			queryResults.addAll(query.execute());
		}

		// instantiate and filter results one by one until we have 'max'
		Set<SearchableResult> results = new TreeSet<SearchableResult>();
		filterResultsByPermission(results, queryResults, max);

		// inject the found items to a suitable return value
		final SearchResult result = new SearchResult();
		for (SearchableResult sr : results) {
			injectResults(result, Sets.newHashSet(sr), getCategory(sr.getModel().getClass()));
		}

		afterSearch(result);
		monitor.stop();
		Logger.debug("[Fishr] Found %d results for [%s] in %s ms via ahead search.", result.getCount(), searchString,
				monitor.getLastValue());

		return result;
	}

	public SearchResult searchForHashTag(final String searchString, int size, String term) {
		if (!Fishr.HASHTAGS) {
			Logger.warn("[Fishr] Performing hash tag search even though indexing of hash tags is disabled");
		}

		final SearchResult result = new SearchResult();

		for (Class<SearchableModel> searchableModelClass : getReversedCategories().keySet()) {
			// basic search for hashtag field
			final Query query = Fishr.createQuery(searchableModelClass).and(Fishr.HASH_TAG_FIELD, term);
			injectResults(result, executeQuery(query, searchableModelClass, null), getCategory(searchableModelClass));
		}

		return result;
	}

	/**
	 * Injects the search results for a given category into the overall {@link SearchResult}.
	 */
	protected void injectResults(SearchResult into, Set<SearchableResult> results, String category) {
		if (StringUtils.isNotEmpty(category)) {
			if (!into.results.containsKey(category)) {
				into.results.put(category, new TreeSet<SearchableResult>());
			}
			into.results.get(category).addAll(results);
		}
	}

	public Map<String, List<Class<SearchableModel>>> getCategories() {
		if (categories == null) {
			categories = new LinkedHashMap<String, List<Class<SearchableModel>>>();

			for (ApplicationClass ac : Play.classes.getAnnotatedClasses(Searchable.class)) {
				// determine category
				String category = ac.javaClass.getAnnotation(Searchable.class).category();
				if (category == null) {
					category = ac.name;
				}

				// load list of models in category
				List<Class<SearchableModel>> list = categories.get(category);
				if (list == null) {
					list = new ArrayList<Class<SearchableModel>>();
				}

				// add model
				list.add((Class<SearchableModel>) ac.javaClass);
				categories.put(category, list);
			}
		}

		return categories;
	}

	public Map<Class<SearchableModel>, String> getReversedCategories() {
		if (reversedCategories == null) {
			reversedCategories = new LinkedHashMap<Class<SearchableModel>, String>();
			for (String category : getCategories().keySet()) {
				final List<Class<SearchableModel>> clazzes = getCategories().get(category);
				if (clazzes != null && !clazzes.isEmpty()) {
					for (Class<SearchableModel> clazz : clazzes) {
						reversedCategories.put(clazz, category);
					}
				}
			}
		}

		return reversedCategories;
	}

	public static String getCategory(SearchableModel model) {
		return getCategory(model.getClass());
	}

	protected static String getCategory(Class clazz) {
		Searchable anno = (Searchable) clazz.getAnnotation(Searchable.class);
		if (anno != null) {
			return anno.category();
		}
		return null;
	}

	protected Query prepareSearchQuery(String searchString, Class<? extends SearchableModel> clazz) {
		// prepare search string
		searchString = StringUtils.trim(searchString);

		// execute search query
		Query query = Fishr.createQuery(clazz);

		// split search terms
		QueryBlock block = query.orBlock();
		for (String term : util.Util.parseList(searchString, " ")) {
			block.andBlock().or(Fishr.ALL_FIELD, term, "~" + getFuzzyFactor()).or(Fishr.ALL_FIELD, term, "*~")
					.or(Fishr.RELATED_FIELD, term, "~" + getFuzzyFactor() + "^0.5")
					.or(Fishr.RELATED_FIELD, term, "*~^0.5");
		}

		// always try exact expression search as well
		query.or(Fishr.ALL_FIELD, searchString, true);

		return query;
	}

	protected List<Field> getSearchableFields(Class<? extends SearchableModel> clazz) {
		if (!searchFields.containsKey(clazz)) {
			List<Field> fields = new ArrayList<Field>();
			for (Field field : clazz.getFields()) {
				if (field.isAnnotationPresent(fishr.Field.class)) {
					fields.add(field);
				}
			}
			searchFields.put(clazz, fields);
		}
		return searchFields.get(clazz);
	}

	protected float getFuzzyFactor() {
		return FUZZY_FACTOR;
	}

	/**
	 * Performs a search on a model and checks the read permissions if permission is supplied through
	 * {@link Searchable#permission()}.
	 *
	 * @param searchString
	 * @param clazz
	 * @param max
	 *            Can be null for unlimited results. IF max value is set, this method guarantees, that max or less
	 *            entites are returned.
	 * @return Set of results for given model with search score. Empty Set if no results could be found.
	 */
	public Set<SearchableResult> searchByModel(String searchString, Class<? extends SearchableModel> clazz, Integer max) {
		Query query = prepareSearchQuery(searchString, clazz);
		Set<SearchableResult> result = new TreeSet<SearchableResult>();
		result = executeQuery(query, clazz, max);
		return result;
	}

	/**
	 * Executes a query and takes care that a maximum amount of "max" entities are returned. For performance reasons
	 * this query method uses a max of 500 search results. Fetching a that big amount of results has no performance
	 * impact, since querying the index is very fast. The limit of 'max' entries is then applied when applying the
	 * permission checks.
	 */
	protected Set<SearchableResult> executeQuery(Query query, Class<? extends SearchableModel> clazz, Integer max) {
		Monitor monitor = MonitorFactory.start("executeQuery");
		Set<SearchableResult> results = new TreeSet<SearchableResult>();

		Logger.debug("[Fishr] Searching with query [%s] for class [%s].", query, clazz);
		query.page(0, QUERY_MAX_SIZE);
		List<QueryResult> queryResults = query.execute();
		if (queryResults.size() > 0) {
			Logger.debug("[Fishr] Checking permissions for %d results.", queryResults.size());
			filterResultsByPermission(results, queryResults, max);
		}

		/* return a trimmed subset because we might have more than we need now. */
		monitor.stop();
		Logger.debug("[Fishr] Query yielded %s results in %s ms.", results.size(), monitor.getLastValue());
		return results;
	}

	private void filterResultsByPermission(Set<SearchableResult> results, Collection<QueryResult> queryResults,
			Integer max) {
		Monitor monitor = MonitorFactory.start("filterResultsByPermission");
		for (QueryResult result : queryResults) {
			JPABase object = result.getObject();
			if (object instanceof SearchableModel) {
				SearchableModel searchable = (SearchableModel) object;
				if (searchable != null && checkResult(searchable)) {
					results.add(new SearchableResult(searchable, result.getScore()));
					if (max != null && results.size() >= max) {
						break;
					}
				}
			}
		}
		monitor.stop();
		Logger.debug("[Fishr] Checking permissions took %s ms.", monitor.getLastValue());
	}

	public static class SearchResult implements Serializable {

		private Map<String, Set<SearchableResult>> results = new LinkedHashMap<String, Set<SearchableResult>>();

		public Set<String> getCategories() {
			return results.keySet();
		}

		public Set<SearchableResult> getResults(String category) {
			return results.get(category);
		}

		/**
		 * Cross-category
		 */
		public Set<SearchableResult> getResults() {
			Set<SearchableResult> all = new TreeSet<SearchableResult>();
			for (String category : getCategories()) {
				all.addAll(getResults(category));
			}
			return all;
		}

		public Set<SearchableModel> getModels(String category) {
			return toModels(getResults(category));
		}

		/**
		 * Cross-category
		 */
		public Set<SearchableModel> getModels() {
			return toModels(getResults());
		}

		private Set<SearchableModel> toModels(Set<SearchableResult> results) {
			Set<SearchableModel> r = new LinkedHashSet<SearchableModel>();
			for (SearchableResult result : results) {
				r.add(result.getModel());
			}
			return r;
		}

		public int getCount() {
			return getResults().size();
		}

		public boolean isEmpty() {
			for (Set set : results.values()) {
				if (set.size() > 0) {
					return false;
				}
			}
			return true;
		}
	}
}

package fishr.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Annotate your models with this annotation to make it searchable. Additionally, your models must implement
 * {@link SearchableModel}.
 * </p>
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@Inherited
public @interface Searchable {

	/**
	 * Specifies a category that the search uses to group multiple model results into one category.
	 */
	String category();
}

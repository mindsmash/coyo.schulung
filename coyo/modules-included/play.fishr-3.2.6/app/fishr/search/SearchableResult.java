package fishr.search;

import java.io.Serializable;

import fishr.query.QueryResult;

/**
 * Sortable container for {@link SearchableModel} with score.
 */
public class SearchableResult extends QueryResult implements Serializable {

	private SearchableModel model;

	public SearchableResult(SearchableModel model, Float score) {
		super(model.getId().toString(), model.getClass(), score);
		this.model = model;
	}

	public SearchableModel getModel() {
		return model;
	}
}

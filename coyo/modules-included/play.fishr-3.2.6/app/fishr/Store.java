package fishr;

import fishr.query.Query;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Manages a search backed (indexes, searchers, readers...)
 */
public interface Store {
	public void start() throws Exception;

	public void stop() throws Exception;

	public void unIndex(Object object) throws Exception;

	public void index(Object object) throws Exception;

	public void rebuildAllIndexes() throws Exception;

	public void rebuild(String name) throws Exception;

	public Query createQuery(Class clazz);

	public abstract Map<String, Long> getHashTags(int size, Set<String> blacklist);
}

package fishr.elasticsearch;

import fishr.Fishr;
import fishr.Indexed;
import fishr.Store;
import fishr.query.Query;
import fishr.util.ConvertionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.flush.FlushRequest;
import org.elasticsearch.action.admin.indices.mapping.delete.DeleteMappingRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.ImmutableSettings.Builder;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.indices.IndexMissingException;
import org.elasticsearch.indices.TypeMissingException;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import play.templates.JavaExtensions;
import play.utils.Utils;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

/**
 * ElasticSearch implementation for the store.
 *
 * @author Carsten Canow, mindsmash GmbH
 */
public class ElasticSearchStore implements Store {

	protected Client client;
	protected Node node;

	protected static final TimeValue DEFAULT_TIMEOUT = new TimeValue(Long.parseLong(Play.configuration.getProperty(
			"elasticsearch.timeout", "10")), TimeUnit.SECONDS);

	@Override
	public void start() throws Exception {
		Logger.info("ElasticSearch: start");

		Builder settingsBuilder = ImmutableSettings.settingsBuilder();

		// inject custom elastic search params
		Map<Object, Object> args = Utils.Maps.filterMap(Play.configuration, "^elasticsearch\\..*");
		for (Object o : args.keySet()) {
			settingsBuilder.put((o + "").replaceFirst("elasticsearch.", ""), args.get(o));
		}

		List<InetSocketTransportAddress> hosts = new ArrayList<InetSocketTransportAddress>();
		int index = 0;
		while (true) {
			String host = Play.configuration.getProperty("elasticsearch.host." + index, null);
			int port = Integer.parseInt(Play.configuration.getProperty("elasticsearch.port." + index, "9300"));
			if (host == null || port <= 0) {
				break;
			}
			hosts.add(new InetSocketTransportAddress(host, port));
			index++;
		}

		if (hosts.size() > 0) {
			Logger.info("ElasticSearch: running on %d hosts", hosts.size());
			TransportClient transportClient = new TransportClient(settingsBuilder.build());
			for (InetSocketTransportAddress inetSocketAddress : hosts) {
				transportClient.addTransportAddress(inetSocketAddress);
			}
			client = transportClient;
		} else {
			Logger.info("ElasticSearch: running local");
			settingsBuilder.put("bootstrap.sigar", false);
			settingsBuilder.put("index.number_of_replicas", 0);
			settingsBuilder.put("index.number_of_shards", 1);
			settingsBuilder.put("http.enabled", false);

			node = NodeBuilder.nodeBuilder().settings(settingsBuilder.build()).local(true).node();
			client = node.client();
		}
	}

	@Override
	public void stop() throws Exception {
		Logger.info("ElasticSearch: stop");
		if (null != client) {
			client.close();
		}
		if (null != node) {
			node.close();
		}
	}

	@Override
	public void index(Object object) throws Exception {
		String source = ElasticSearchConversionUtils.getSource(object);
		if (source != null) {
			Class<? extends Object> clazz = object.getClass();
			String index = getIndex(clazz);
			String type = getType(clazz);
			String id = getId(object);
			client.prepareIndex(index, type, id).setTimeout(DEFAULT_TIMEOUT).setSource(source).execute().actionGet();
		}
	}

	@Override
	public void unIndex(Object object) throws Exception {
		Class<? extends Object> clazz = object.getClass();
		String index = getIndex(clazz);
		String type = getType(clazz);
		String id = getId(object);
		client.prepareDelete(index, type, id).setTimeout(DEFAULT_TIMEOUT).execute().actionGet();
	}

	/**
	 * Retrieve the index for the search<br>
	 * ! May not contain upper case characters !
	 *
	 * @param clazz
	 * @return
	 */
	protected String getIndex(Class<? extends Object> clazz) {
		return "default";
	}

	/**
	 * Retrieve the type for the search (e.g. the class name)<br>
	 * ! May not contain points "." !
	 *
	 * @param clazz
	 * @return
	 */
	protected String getType(Class<? extends Object> clazz) {
		return JavaExtensions.slugify(clazz.getName());
	}

	protected String getId(Object object) {
		if (object instanceof JPABase) {
			return String.valueOf(ConvertionUtils.getIdValueFor((JPABase) object));
		}
		return null;
	}

	@Override
	public void rebuildAllIndexes() throws Exception {
		List<ApplicationClass> classes = Play.classes.getAnnotatedClasses(Indexed.class);
		for (ApplicationClass applicationClass : classes) {
			rebuild(applicationClass.javaClass.getName());
		}
		Logger.info("Rebuild index finished");
	}

	@Override
	public void rebuild(String name) throws Exception {
		// retrieve Class by name
		Class clazz = Play.classes.getApplicationClass(name).javaClass;
		String index = getIndex(clazz);
		String type = getType(clazz);

		boolean indexExisted = true;
		try {
			DeleteMappingRequest deleteMapping = new DeleteMappingRequest(index).types(type);
			client.admin().indices().deleteMapping(deleteMapping).actionGet();
		} catch (IndexMissingException e) {
			// ignore silently: no index was present before
			indexExisted = false;
		} catch (TypeMissingException e) {
			// ignore silently: no document of this type was indexed before
		}

		// get all entries
		List<JPABase> objects = JPA.em().createQuery("select e from " + clazz.getCanonicalName() + " as e")
				.getResultList();
		for (JPABase jpaBase : objects) {
			// add index/type
			index(jpaBase);
		}

		if (indexExisted) {
			// flush index
			client.admin().indices().flush(new FlushRequest(index)).actionGet();
		}
	}

	@Override
	public Query createQuery(Class clazz) {
		return new ElasticSearchQuery(clazz, client, getIndex(clazz), getType(clazz));
	}

	public Map<String, Long> getHashTags(int size, Set<String> blacklist) {
		final String index = getIndex(null);
		final String type = null;

		final SearchRequestBuilder builder = client.prepareSearch(index);

		if (!StringUtils.isEmpty(type)) {
			builder.setTypes(type);
		}

		final AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
		for (String blacktag : blacklist) {
			andFilterBuilder.add(FilterBuilders.notFilter(FilterBuilders.termFilter(Fishr.HASH_TAG_FIELD, blacktag)));
		}

		builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder))
				.addAggregation(terms("keys")
						.field(Fishr.HASH_TAG_FIELD)
						.size(size)
						.order(Terms.Order.count(false)));

		final SearchResponse response = builder.execute().actionGet();
		final Terms terms = response.getAggregations().get("keys");

		// fetch aggregation buckets
		final Collection<Terms.Bucket> buckets = terms.getBuckets();
		final Map<String, Long> tags = new LinkedHashMap<>();

		for (Terms.Bucket bucket : buckets) {
			final long count = bucket.getDocCount();
			final String key = bucket.getKeyAsText().string();

			tags.put(key, count);
		}

		return tags;
	}
}

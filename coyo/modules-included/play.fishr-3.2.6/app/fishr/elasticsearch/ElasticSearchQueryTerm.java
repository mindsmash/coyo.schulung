package fishr.elasticsearch;

import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;

import fishr.query.QueryTerm;

public class ElasticSearchQueryTerm extends QueryTerm {

	public ElasticSearchQueryTerm(String field, String value, String addon, boolean phrase) {
		super(field, value, addon, phrase);
	}

	@Override
	public String parse() {
		String escaped = QueryParserUtil.escape(value);
		if (phrase) {
			escaped = "\"" + escaped + "\"";
		}
		String term = escaped + (addon != null ? addon : "");
		return field + ":" + term;
	}
}

package fishr.elasticsearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.indices.IndexMissingException;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;

import play.db.jpa.JPABase;
import play.exceptions.UnexpectedException;
import fishr.FishrException;
import fishr.query.Query;
import fishr.query.QueryResult;

/**
 * ElasticSearch implementation for the query.
 *
 * @author Carsten Canow, mindsmash GmbH
 */
public class ElasticSearchQuery extends Query {
	private Class<JPABase> clazz;
	private Client client;
	private String index;
	private String type;
	private List<SortBuilder> sorts = new ArrayList<SortBuilder>();
	private boolean reverse = false;
	private int from = -1;
	private int size = -1;

	public ElasticSearchQuery(Class clazz, Client client, String index, String type) {
		super(new ElasticSearchQueryBlock());
		this.clazz = clazz;
		this.client = client;
		this.index = index;
		this.type = type;
	}

	@Override
	public ElasticSearchQuery page(int offset, int pageSize) {
		this.from = offset;
		this.size = pageSize;
		return this;
	}

	@Override
	public ElasticSearchQuery reverse() {
		if (!this.reverse) {
			SortOrder sortOrder = getSortOrder(true);
			for (SortBuilder sortBuilder : sorts) {
				sortBuilder.order(sortOrder);
			}
		}
		this.reverse = true;
		return this;
	}

	private SortOrder getSortOrder(boolean reverse) {
		return reverse ? SortOrder.DESC : SortOrder.ASC;
	}

	@Override
	public ElasticSearchQuery orderBy(String... order) {
		sorts.clear();
		SortBuilders sortBuilder = new SortBuilders();
		SortOrder sortOrder = getSortOrder(reverse);
		for (String field : order) {
			sorts.add(sortBuilder.fieldSort(field).order(sortOrder));
		}
		return this;
	}

	@Override
	public <T extends JPABase> List<T> fetch() throws FishrException {
		try {
			List<QueryResult> results = execute();
			List<JPABase> objects = new ArrayList<JPABase>();
			for (QueryResult queryResult : results) {
				JPABase jpaBase = queryResult.getObject();
				if (jpaBase != null) {
					objects.add(jpaBase);
				}
			}
			return (List<T>) objects;
		} catch (IndexMissingException e) {
			// no index found: return empty list
			return Collections.EMPTY_LIST;
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	@Override
	public List<Long> fetchIds() throws FishrException {
		try {
			List<QueryResult> results = execute();
			List<Long> objects = new ArrayList<Long>();
			for (QueryResult queryResult : results) {
				objects.add(Long.parseLong(queryResult.getId()));
			}
			return objects;
		} catch (IndexMissingException e) {
			// ignore silently: no index was present before
			return Collections.EMPTY_LIST;
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	@Override
	public long count() throws FishrException {
		CountResponse response = client.prepareCount().setQuery(QueryBuilders.queryString(get())).execute().actionGet();
		return response.getCount();
	}

	/**
	 * Executes the ElasticSearch query against the index. You get QueryResults.
	 * load the corresponding JPABase objects in the QueryResult Object
	 * @return
	 */
	@Override
	public List<QueryResult> execute() throws FishrException {
		SearchRequestBuilder builder = client.prepareSearch(index).setTypes(type);
		builder.setTimeout(ElasticSearchStore.DEFAULT_TIMEOUT);
		builder.setSearchType(SearchType.QUERY_THEN_FETCH);

		builder.setQuery(QueryBuilders.queryStringQuery(get()));

		// Sorting
		for (SortBuilder sort : sorts) {
			builder.addSort(sort);
		}

		// Paging
		if (from > -1) {
			builder.setFrom(from);
		}
		if (size > -1) {
			builder.setSize(size);
		}

		// Logger.debug("ElasticSearch Query: %s", builder.toString());

		SearchResponse searchResponse = builder.execute().actionGet();
		List<QueryResult> searchResults = new ArrayList<QueryResult>();
		SearchHits hits = searchResponse.getHits();
		for (SearchHit searchHit : hits) {
			searchResults.add(new QueryResult(searchHit.getId(), clazz, searchHit.getScore()));
		}
		return searchResults;
	}
}

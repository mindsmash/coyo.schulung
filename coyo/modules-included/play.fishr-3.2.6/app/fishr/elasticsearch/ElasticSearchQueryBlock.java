package fishr.elasticsearch;

import fishr.query.QueryBlock;
import fishr.query.QueryTerm;

public class ElasticSearchQueryBlock extends QueryBlock {

	@Override
	protected QueryBlock createBlock() {
		return new ElasticSearchQueryBlock();
	}

	@Override
	protected QueryTerm createTerm(String field, String value, String addon, boolean phrase) {
		return new ElasticSearchQueryTerm(field, value, addon, phrase);
	}
}

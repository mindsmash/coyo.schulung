package fishr.elasticsearch;

import java.util.List;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import fishr.Fishr;
import fishr.Indexed;
import fishr.util.ConvertionUtils;
import fishr.util.ConvertionUtils.IndexableContent;
import fishr.util.HashTagged;

public class ElasticSearchConversionUtils {
	public static String getSource(Object object) throws Exception {
		if (!ConvertionUtils.isIndexable(object))
			return null;

		Indexed indexed = object.getClass().getAnnotation(Indexed.class);

		XContentBuilder builder = XContentFactory.jsonBuilder().startObject();

		List<IndexableContent> indexableContents = ConvertionUtils.getIndexableContents(object);

		// TODO: move to ConvertionUtils#getIndexableContents
		if (Fishr.HASHTAGS && object instanceof HashTagged) {
			indexableContents.add(ConvertionUtils.getIndexableHashTags((HashTagged) object));
		}

		for (IndexableContent ic : indexableContents) {
			builder.field(ic.name, ic.content);
		}

		builder.field(Fishr.ALL_FIELD, ConvertionUtils.getAllValue(indexableContents));
		builder.field(Fishr.RELATED_FIELD, ConvertionUtils.getRelatedContent(object));
		builder.field("_boost", indexed.boost());
		return builder.endObject().string();
	}
}

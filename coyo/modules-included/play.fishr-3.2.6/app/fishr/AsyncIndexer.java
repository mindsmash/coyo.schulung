package fishr;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import play.Logger;
import play.db.jpa.JPABase;
import play.db.jpa.JPAPlugin;
import util.ModelUtil;

/**
 * Async indexer that hold all objects in queue that must be written to the index and works through these objects.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class AsyncIndexer implements Runnable {

	/**
	 * Holds all objects that should be indexed now.
	 */
	public BlockingQueue<String> indexQueue = new LinkedBlockingQueue<String>();

	private boolean running = true;

	public void start() {
		running = true;
		new Thread(this, "AsyncIndexer").start();
	}

	public void stop() {
		if (Logger.isDebugEnabled()) {
			Logger.debug("[Fishr] Stopping fishr indexer");
		}
		running = false;
	}

	@Override
	public void run() {
		Logger.info("[Fishr] Starting fishr async indexer");

		JPAPlugin jpaPlugin = new JPAPlugin();

		while (running) {
			try {
				List<String> objectsToIndex = new ArrayList<>();
				objectsToIndex.add(indexQueue.take());
				indexQueue.drainTo(objectsToIndex);

				if (Logger.isDebugEnabled()) {
					Logger.debug("[Fishr] Found %d objects to index", objectsToIndex.size());
				}

				jpaPlugin.startTx(true);
				if (Logger.isDebugEnabled()) {
					Logger.debug("[Fishr] Starting transaction to load these objects");
				}

				for (String serialized : objectsToIndex) {
					try {
						if (Logger.isDebugEnabled()) {
							Logger.debug("[Fishr] Indexing [%s]", serialized);
						}
						doIndex(loadModel(serialized));
					} catch (Exception e) {
						Logger.error(e, "[Fishr] error indexing object [%s]", serialized);
					}
				}
			} catch (Exception e) {
				Logger.error(e, "[Fishr] error in async indexer");
			} finally {
				if (Logger.isDebugEnabled()) {
					Logger.debug("[Fishr] Closing transaction");
				}
				jpaPlugin.closeTx(true);
			}
		}

		Logger.info("[Fishr] Fishr indexer is stopped");
	}

	protected JPABase loadModel(String serialized) {
		return ModelUtil.deserialize(serialized);
	}

	protected void doIndex(Object o) throws Exception {
		Fishr.index(o);
	}
}
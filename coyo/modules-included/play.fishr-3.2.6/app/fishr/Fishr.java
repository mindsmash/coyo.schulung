package fishr;

import fishr.elasticsearch.ElasticSearchStore;
import fishr.query.Query;
import injection.Inject;
import injection.InjectionSupport;

import java.util.*;

import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import play.exceptions.UnexpectedException;

/**
 * Very basic tool to basic search on your JPA objects.
 * <p/>
 * On a JPABase subclass, add the @Indexed annotation on your class, and the @Field annotation on your field members
 * <p/>
 * Each time you save, update or delete your class, the corresponding index is updated
 * <p/>
 * use the search method to query an index.
 * <p/>
 * Samples in samples-and-tests/app/controllers/JPASearch.java
 */
@InjectionSupport
public class Fishr {

	/**
	 * Search field name for the "allfield" - can be used for searching over all fields.
	 */
	public static final String ALL_FIELD = Play.configuration.getProperty("mindsmash.fishr.search.allfield",
			"_allfield");
	/**
	 * Search field name for related content.
	 */
	public static final String RELATED_FIELD = Play.configuration.getProperty("mindsmash.fishr.search.relatedfield",
			"_relatedfield");
	/**
	 * Field containing the documents hashtags
	 */
	public static final String HASH_TAG_FIELD = Play.configuration.getProperty("mindsmash.fishr.search.hashtagfield",
			"hashTags");
	/**
	 * Enable//disbale hashhtag extraction
	 */
	public static final boolean HASHTAGS =
			Boolean.valueOf(Play.configuration.getProperty("mindsmash.fishr.search.hashtags", "false"));

	@Inject(configuration = "mindsmash.fishr.search.store", defaultClass = ElasticSearchStore.class)
	private static Store store;

	public static Store getCurrentStore() {
		return store;
	}

	public static Query createQuery(Class clazz) {
		return store.createQuery(clazz);
	}

	public static void unIndex(Object object) throws Exception {
		if (isIndexed(object)) {
			store.unIndex(object);
		}
	}

	public static void index(Object object) throws Exception {
		if (isIndexed(object)) {
			store.index(object);
		}
	}

	private static boolean isIndexed(Object object) {
		return (object instanceof JPABase) && object.getClass().isAnnotationPresent(Indexed.class)
				&& ((JPABase) object)._key() != null;
	}

	public static void rebuildAllIndexes() throws Exception {
		store.rebuildAllIndexes();
	}

	public static void init() {
		try {
			store.start();
		} catch (Exception e) {
			throw new UnexpectedException("Could not intialize store", e);
		}
	}

	public static void shutdown() throws Exception {
		if (store != null) {
			store.stop();
		}
	}

	public static List<ManagedIndex> listIndexes() {

		List<ManagedIndex> indexes = new ArrayList<ManagedIndex>();
		List<ApplicationClass> classes = Play.classes.getAnnotatedClasses(Indexed.class);
		for (ApplicationClass applicationClass : classes) {
			ManagedIndex index = new ManagedIndex();
			index.name = applicationClass.javaClass.getName();
			index.jpaCount = (Long) JPA.em()
					.createQuery("select count (*) from " + applicationClass.javaClass.getCanonicalName() + ")")
					.getSingleResult();
			indexes.add(index);
		}
		return indexes;
	}


	public static Map<String, Long> getHashTags(int size, final Set<String> blacklist) {
		return store.getHashTags(size, blacklist);
	}
}

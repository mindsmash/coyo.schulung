package fishr.query;

import models.GenericBaseModel;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import fishr.util.ConvertionUtils;

public class QueryResult implements Comparable<QueryResult> {

	private Float score;
	private String id;
	private JPABase object;
	private Class<?> clazz;

	public QueryResult(String id, Class<?> clazz, Float score) {
		this.id = id;
		this.clazz = clazz;
		this.score = score;
	}

	public JPABase getObject() {
		if (null == object) {
			Object objectId = ConvertionUtils.getIdValueFromIndex(clazz, id);
			object = (JPABase) JPA.em().find(clazz, objectId);

			if (null == object) {
				Logger.warn("[Fishr] Cannot find indexed object in database [%s[%s]] - please re-index", clazz, id);
			}
		}
		return object;
	}

	public Float getScore() {
		return score;
	}

	public String getId() {
		return id;
	}

	/**
	 * Compares search results with each other. This must make use of the equals method since TreeSet is using this
	 * method to check for quality. See: http://stackoverflow.com/a/12761627
	 * 
	 * Comparison is done by score descending, modification date of the referenced model descending and id descending.
	 */
	@Override
	public int compareTo(QueryResult o) {
		if (o.equals(this)) {
			return 0;
		}
		if (o.score.compareTo(score) != 0) {
			return o.score.compareTo(score);
		}
		if (this.object instanceof GenericBaseModel && o.object instanceof GenericBaseModel) {
			return ((GenericBaseModel) o.object).modified.compareTo(((GenericBaseModel) this.object).modified);
		}
		return this.id.compareTo(o.id);
	}
}

package fishr.query;

import java.util.List;

import play.db.jpa.JPABase;
import fishr.FishrException;

public abstract class Query {

	protected QueryBlock mainQueryBlock;

	public Query(QueryBlock mainQueryBlock) {
		this.mainQueryBlock = mainQueryBlock;
	}

	abstract public Query page(int offset, int pageSize);

	abstract public Query reverse();

	abstract public Query orderBy(String... order);

	abstract public <T extends JPABase> List<T> fetch() throws FishrException;

	abstract public List<Long> fetchIds() throws FishrException;

	abstract public long count() throws FishrException;

	abstract public List<QueryResult> execute() throws FishrException;

	protected String get() {
		if (mainQueryBlock == null) {
			throw new IllegalStateException("no main query block found");
		}
		return mainQueryBlock.parse();
	}

	public Query and(String field, String value) {
		return and(field, value, "");
	}

	public Query and(String field, String value, String addon) {
		mainQueryBlock.and(field, value, addon);
		return this;
	}

	public Query and(String field, String value, boolean phrase) {
		mainQueryBlock.and(field, value, "", phrase);
		return this;
	}

	public Query and(String field, String value, String addon, boolean phrase) {
		mainQueryBlock.and(field, value, addon, phrase);
		return this;
	}

	public Query or(String field, String value) {
		return or(field, value, "");
	}

	public Query or(String field, String value, String addon) {
		mainQueryBlock.or(field, value, addon);
		return this;
	}

	public Query or(String field, String value, boolean phrase) {
		mainQueryBlock.or(field, value, "", phrase);
		return this;
	}

	public Query or(String field, String value, String addon, boolean phrase) {
		mainQueryBlock.or(field, value, addon, phrase);
		return this;
	}

	public QueryBlock andBlock() {
		return mainQueryBlock.andBlock();
	}

	public QueryBlock orBlock() {
		return mainQueryBlock.orBlock();
	}

	@Override
	public String toString() {
		return getClass() + "[" + get() + "]";
	}
}

package fishr.query;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public abstract class QueryBlock implements QueryPart {

	public Map<QueryPart, Boolean> parts = new LinkedHashMap<QueryPart, Boolean>();

	abstract protected QueryBlock createBlock();

	abstract protected QueryTerm createTerm(String field, String value, String addon, boolean phrase);

	/**
	 * Convenience method
	 * 
	 * @see #and(String, String, String, boolean)
	 */
	public QueryBlock and(String field, String value) {
		return and(field, value, "", false);
	}

	/**
	 * Convenience method
	 * 
	 * @see #and(String, String, String, boolean)
	 */
	public QueryBlock and(String field, String value, String addon) {
		return and(field, value, addon, false);
	}

	/**
	 * Convenience method
	 * 
	 * @see #and(String, String, String, boolean)
	 */
	public QueryBlock and(String field, String value, boolean phrase) {
		return and(field, value, "", phrase);
	}
	
	public QueryBlock and(String field, String value, String addon, boolean phrase) {
		parts.put(createTerm(field, value, addon, phrase), true);
		return this;
	}

	/**
	 * Convenience method.
	 * 
	 * @see #or(String, String, String, boolean)
	 */
	public QueryBlock or(String field, String value) {
		return or(field, value, "", false);
	}

	/**
	 * Convenience method.
	 * 
	 * @see #or(String, String, String, boolean)
	 */
	public QueryBlock or(String field, String value, String addon) {
		return or(field, value, addon, false);
	}

	/**
	 * Convenience method.
	 * 
	 * @see #or(String, String, String, boolean)
	 */
	public QueryBlock or(String field, String value, boolean phrase) {
		return or(field, value, "", phrase);
	}

	public QueryBlock or(String field, String value, String addon, boolean phrase) {
		parts.put(createTerm(field, value, addon, phrase), false);
		return this;
	}

	public QueryBlock andBlock() {
		QueryBlock block = createBlock();
		parts.put(block, true);
		return block;
	}

	public QueryBlock orBlock() {
		QueryBlock block = createBlock();
		parts.put(block, false);
		return block;
	}

	@Override
	public String parse() {
		String s = "";
		for (QueryPart part : parts.keySet()) {
			String partString = part.parse();
			if (!StringUtils.isEmpty(partString)) {
				if (StringUtils.isEmpty(s)) {
					s = partString;
				} else {
					s += (parts.get(part) ? " AND " : " OR ") + partString;
				}
			}
		}
		if (StringUtils.isEmpty(s)) {
			return "";
		}
		return "(" + s + ")";
	}
}

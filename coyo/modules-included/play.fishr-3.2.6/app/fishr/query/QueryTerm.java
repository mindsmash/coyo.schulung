package fishr.query;

public abstract class QueryTerm implements QueryPart {

	public boolean phrase;
	public String field;
	public String value;
	public String addon;

	public QueryTerm(String field, String value, String addon, boolean phrase) {
		this.field = field;
		this.value = value;
		this.addon = addon;
		this.phrase = phrase;
	}

	abstract public String parse();
}

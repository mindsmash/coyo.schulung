package fishr.query;

public interface QueryPart {

	String parse();
}

package fishr.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Id;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;

import play.Logger;
import play.data.binding.Binder;
import play.db.jpa.Blob;
import play.db.jpa.JPABase;
import play.db.jpa.Model;
import play.exceptions.UnexpectedException;
import util.Util;
import fishr.Fishr;
import fishr.Indexed;
import fishr.Related;
import fishr.search.ContentIndexable;

/**
 * Various utils handling object to index and query result to object conversion
 */
public class ConvertionUtils {

	private static Tika tika;

	/**
	 * This is the regular expression defining what is allowed before a hashtag. This is a whitespace, the start of text
	 * / a new line and any XML tag (e.g. '<span>')
	 */
	private static final String HASHTAG_BEFORE = "(^|\\s|\\<\\w+?\\/?\\>)";

	/**
	 * This is the regular expression defining what is allowed after a hashtag. This includes a whitespace, a control
	 * character (e.g. nl) or the end of text / end of a line.
	 * See http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html for POSIX character classes
	 */
	private static final String HASHTAG_AFTER = "(?=\\<|\\p{Punct}|\\p{Space}|\\p{Blank}|$)";

	/**
	 * This is the regular expression matching a hashtag. For a hashtag we allow all letters, encoded umlauts, encoded
	 * 'ß' and unicode characters (like plain, unencoded umlauts).
	 */
	private static final String HASHTAG_BODY = "#((?:(?:&[aouszligm]{4,5};)|[a-z0-9üÀ-ÖØ-öø-ÿ]){3,20})";

	/**
	 * The final hashtag pattern. It combines the two parts for BEFORE and AFTER with the BODY.
	 */
	public static final Pattern HASHTAG_PATTERN = Pattern.compile(HASHTAG_BEFORE + HASHTAG_BODY + HASHTAG_AFTER,
			Pattern.CASE_INSENSITIVE);

	public static final int HASHTAG_MATCHING_GROUP = 2;

	public static class IndexableContent {

		public fishr.Field annotation;
		public String name;
		public String content;

		public Object originalValue;
		public Class originalType;

		public IndexableContent(fishr.Field annotation, String name, String content, Object originalValue,
				Class originalType) {
			this.annotation = annotation;
			this.name = name;
			this.content = content;
			this.originalValue = originalValue;
			this.originalType = originalType;
		}
	}

	/**
	 * Extracts indexable content from a field that can be used for indexing. Supports multiple types (including Blob,
	 * Collection, ContentIndexable).
	 *
	 * @param object
	 * @param field
	 * @return Indexable content object or null.
	 * @throws Exception
	 */
	public static IndexableContent getIndexableContent(Object object, Field field) {
		try {
			Object o = getValue(object, field);
			String content = extractIndexableContent(o);

			if (content != null) {
				return new IndexableContent(field.getAnnotation(fishr.Field.class), field.getName(), content, o,
						field.getType());
			}
		} catch (Exception e) {
			Logger.warn(e, "[Fishr] Error getting indexable content from field [%s] of object [%s]", field, object);
		}
		return null;
	}

	/**
	 * Extracts indexable content from a method that can be used for indexing. Supports multiple types (including Blob,
	 * Collection, ContentIndexable).
	 *
	 * @param object
	 * @param method
	 * @return Indexable content object or null.
	 * @throws Exception
	 */
	public static IndexableContent getIndexableContent(Object object, Method method) {
		try {
			final Object o = method.invoke(object);
			if (o != null) {
				final String indexable = extractIndexableContent(o);
				if (StringUtils.isNotEmpty(indexable)) {
					return new IndexableContent(method.getAnnotation(fishr.Field.class), method.getName(),
							indexable, o, method.getReturnType());
				}
			}
		} catch (Exception e) {
			Logger.warn(e, "[Fishr] Error invoking method [%s] on object [%s] to get indexable content from it",
					method, object);
		}

		return null; // no indexable content found
	}

	/**
	 * Extracts an indexable string value from an object. Supports multiple types (including Blob, Collection,
	 * ContentIndexable).
	 *
	 * @param o
	 * @return String value or null.
	 * @throws Exception
	 */
	public static String extractIndexableContent(Object o) throws IOException {
		if (o != null) {
			if (o instanceof String) {
				return (String) o;
			}

			if (o instanceof Blob) {
				Blob blob = (Blob) o;
				return extractFileContents(blob.get(), blob.length(), blob.type());
			}

			if (o instanceof ContentIndexable) {
				ContentIndexable contentIndexable = (ContentIndexable) o;
				return extractFileContents(contentIndexable.get(), contentIndexable.length(), contentIndexable.type());
			}

			if (o instanceof Collection) {
				String content = "";
				for (Object item : (Collection) o) {
					String itemContent = extractIndexableContent(item);
					if (itemContent != null) {
						content += itemContent + " ";
					}
				}
				if (StringUtils.isNotEmpty(content)) {
					return content;
				}
			}

			return "" + o;
		}
		return null;
	}

	/**
	 * Loads the values of all indexable fields and methods.
	 *
	 * @param object
	 * @return Map with values for fields.
	 */
	public static List<IndexableContent> getIndexableContents(Object object) {
		if (object == null) {
			return null;
		}

		List<IndexableContent> indexableContents = new ArrayList<>();

		for (Field field : object.getClass().getFields()) {
			if (!ConvertionUtils.isIndexable(field))
				continue;
			IndexableContent ic = ConvertionUtils.getIndexableContent(object, field);
			if (ic != null) {
				indexableContents.add(ic);
			}
		}

		for (Method method : object.getClass().getMethods()) {
			if (!ConvertionUtils.isIndexable(method))
				continue;
			IndexableContent ic = ConvertionUtils.getIndexableContent(object, method);
			if (ic != null) {
				indexableContents.add(ic);
			}
		}

		return indexableContents;
	}

	/**
	 * Returns all hashtags found within indexable content
	 * 
	 * @param o
	 * @return List of found hashtags
	 */
	public static Set<String> extractHashtags(Object o) {
		return extractHashTags(getIndexableContents(o));
	}

	/**
	 * Returns all hashtags found within indexable content
	 * 
	 * @param contentList
	 * @return List of found hashtags
	 */
	public static Set<String> extractHashTags(List<IndexableContent> contentList) {
		if (contentList == null) {
			return null;
		}

		final HashSet<String> hashTags = new HashSet<>();

		for (IndexableContent ic : contentList) {
			final Matcher mat = HASHTAG_PATTERN.matcher(ic.content);
			while (mat.find()) {
				hashTags.add(mat.group(HASHTAG_MATCHING_GROUP));
			}
		}

		return hashTags;
	}

	/**
	 * Looks for related content fields and methods, checks if they are indexable and if so, extracts their indexable
	 * content into a single string. This can be used to fill a field for related content and search by that.
	 *
	 * @param object
	 * @return String of related values.
	 * @throws Exception
	 */
	public static String getRelatedContent(Object object) {
		StringBuffer values = new StringBuffer();

		try {
			for (Field field : object.getClass().getFields()) {
				if (!ConvertionUtils.isRelated(field))
					continue;
				String value = ConvertionUtils.extractIndexableContent(getValue(object, field));
				if (value != null) {
					values.append(value).append(' ');
				}
			}

			for (Method method : object.getClass().getMethods()) {
				if (!ConvertionUtils.isRelated(method))
					continue;
				String value = ConvertionUtils.extractIndexableContent(method.invoke(object));
				if (value != null) {
					values.append(value).append(' ');
				}
			}
		} catch (Exception e) {
			Logger.warn(e, "[Fishr] Error getting related content for object [%s]", object);
		}

		return values.toString();
	}

	/**
	 * Can be used to obtain the {@link Fishr#ALL_FIELD} value string for all field values that were loaded with
	 *
	 * @param indexableContents
	 * @return String with all values.
	 */
	public static String getAllValue(List<IndexableContent> indexableContents) {
		StringBuffer allValue = new StringBuffer();
		for (IndexableContent ic : indexableContents) {
			allValue.append(ic.content).append(' ');
		}
		return allValue.toString();
	}

	/**
	 * Can be used to check if an object can be indexed.
	 *
	 * @param object
	 * @return True if object can be indexed.
	 */
	public static boolean isIndexable(Object object) {
		if (object == null)
			return false;
		Indexed indexed = object.getClass().getAnnotation(Indexed.class);
		if (indexed == null)
			return false;
		if (!(object instanceof JPABase))
			return false;
		if (!((JPABase) object).isPersistent())
			return false;
		return true;
	}

	/**
	 * Can be used to check if an object can be unindexed.
	 *
	 * @param object
	 * @return True if object can be unindexed.
	 */
	public static boolean isUnIndexable(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass().getAnnotation(Indexed.class) == null) {
			return false;
		}
		if (!(object instanceof JPABase)) {
			return false;
		}

		return true;
	}

	/**
	 * Determines whether the given field is related and if it is indexable.
	 *
	 * @param field
	 * @return True if related and indexable.
	 * @throws Exception
	 */
	private static boolean isRelated(Field field) throws Exception {
		fishr.Related related = field.getAnnotation(Related.class);
		if (related == null)
			return false;
		return isIndexableType(field.getType());
	}

	/**
	 * Determines whether the given method is related and if it is indexable.
	 *
	 * @param method
	 * @return True if related and indexable.
	 * @throws Exception
	 */
	private static boolean isRelated(Method method) throws Exception {
		fishr.Related related = method.getAnnotation(Related.class);
		if (related == null)
			return false;
		return isIndexableType(method.getReturnType());
	}

	/**
	 * Determines whether given field can be indexed.
	 *
	 * @param field
	 * @return True if field can be indexed.
	 */
	private static boolean isIndexable(Field field) {
		fishr.Field index = field.getAnnotation(fishr.Field.class);
		if (index == null)
			return false;
		return isIndexableType(field.getType());
	}

	/**
	 * Determines whether given method can be indexed.
	 *
	 * @param method
	 * @return True if method can be indexed.
	 */
	private static boolean isIndexable(Method method) {
		fishr.Field index = method.getAnnotation(fishr.Field.class);
		if (index == null)
			return false;
		return isIndexableType(method.getReturnType());
	}

	private static boolean isIndexableType(Class type) {
		if (type.isArray())
			return false;
		return true;
	}

	private static Object getValue(Object object, Field field) throws Exception {
		return PropertyUtils.getProperty(object, field.getName());
	}

	private static Tika getTika() {
		if (tika == null) {
			tika = new Tika();
		}
		return tika;
	}

	private static String extractFileContents(InputStream inputStream, long length, String type) throws IOException {
		if (inputStream != null) {
			try {
				if (Logger.isDebugEnabled()) {
					Logger.debug("Extracted string from [%s]", type);
				}

				return getTika().parseToString(inputStream);
			} catch (Exception e) {
				Logger.info("[Fishr] Cannot extract text from file type [%s]", type);
			} catch (Error e) { // Error needs to be covered for Tika
				Logger.info("[Fishr] Cannot extract text from file type [%s]", type);
			} finally {
				inputStream.close();
			}
		}
		return null;
	}

	/**
	 * Looks for the type of the id fiels on the JPABase target class and use play's binder to retrieve the
	 * corresponding object used to build JPA load query
	 *
	 * @param clazz
	 *            JPABase target class
	 * @param indexValue
	 *            String value of the id, taken from index
	 * @return Object id expected to build query
	 */
	public static Object getIdValueFromIndex(Class<?> clazz, String indexValue) {
		java.lang.reflect.Field field = getIdField(clazz);
		Class<?> parameter = field.getType();
		try {
			return Binder.directBind(indexValue, parameter);
		} catch (Exception e) {
			throw new UnexpectedException("Could not convert the ID from index to corresponding type", e);
		}
	}

	/**
	 * Find a ID field on the JPABase target class
	 *
	 * @param clazz
	 *            JPABase target class
	 * @return corresponding field
	 */
	public static java.lang.reflect.Field getIdField(Class<?> clazz) {
		for (java.lang.reflect.Field field : clazz.getFields()) {
			if (field.getAnnotation(Id.class) != null) {
				return field;
			}
		}
		throw new RuntimeException("Your class " + clazz.getName()
				+ " is annotated with javax.persistence.Id but the field Id was not found");
	}

	/**
	 * Lookups the id field, being a Long id for Model and an annotated field @Id for JPABase and returns the field
	 * value.
	 *
	 * @param jpaBase
	 *            is a Play! Framework that supports JPA
	 * @return the field value (a Long or a String for UUID)
	 */
	public static Object getIdValueFor(JPABase jpaBase) {
		if (jpaBase instanceof Model) {
			return ((Model) jpaBase).id;
		}

		java.lang.reflect.Field field = getIdField(jpaBase.getClass());
		Object val = null;
		try {
			val = field.get(jpaBase);
		} catch (IllegalAccessException e) {
			Logger.error("Unable to read the field value of a field annotated with @Id " + field.getName() + " due to "
					+ e.getMessage(), e);
		}
		return val;
	}

	public static boolean isForcedUntokenized(Class<?> clazz, String fieldName) {
		try {
			java.lang.reflect.Field field = clazz.getField(fieldName);
			fishr.Field index = field.getAnnotation(fishr.Field.class);
			return index.tokenize() && index.sortable();
		} catch (Exception e) {
			Logger.error("%s", e.getCause());
		}
		return false;
	}

	public static IndexableContent getIndexableHashTags(HashTagged hashTaggedObject) {
		if (hashTaggedObject == null) {
			return null;
		}

		final Set<String> hashTags = hashTaggedObject.getHashTags();

		return new IndexableContent(new fishr.Field() {
			@Override
			public boolean stored() {
				return false;
			}

			@Override
			public boolean sortable() {
				return false;
			}

			@Override
			public boolean tokenize() {
				return false;
			}

			@Override
			public boolean equals(Object obj) {
				return false;
			}

			@Override
			public int hashCode() {
				return 0;
			}

			@Override
			public String toString() {
				return null;
			}

			@Override
			public Class<? extends Annotation> annotationType() {
				return null;
			}
		}, Fishr.HASH_TAG_FIELD, Util.implode(hashTags.toArray(), " "), hashTags, List.class);
	}
}

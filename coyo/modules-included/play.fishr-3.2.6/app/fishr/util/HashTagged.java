package fishr.util;

import java.util.Set;

/**
 * Mark an entity as one carrying hashtags
 */
public interface HashTagged {

	/**
	 * @return a List of found hashtags
	 */
	abstract Set<String> getHashTags();
}
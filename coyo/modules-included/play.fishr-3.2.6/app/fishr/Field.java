package fishr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark the fields to be included in the index
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Field {

	/**
	 * True if the value should be stored in the index so that it can be retrieved later.
	 */
	boolean stored() default false;

	boolean sortable() default false;

	/**
	 * Should be true for complex content.
	 */
	boolean tokenize() default true;

	// TODO: needs to be implemented for the different search engines
	// float boost() default 1.0F;
}
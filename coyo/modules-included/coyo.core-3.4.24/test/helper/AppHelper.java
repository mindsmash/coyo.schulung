package helper;

import models.Sender;
import models.app.App;
import models.app.WallApp;

public class AppHelper {

	public static WallApp createWallApp(Sender sender, boolean active) {
		return addApp(new WallApp(), sender, active);
	}

	private static <T extends App> T addApp(T app, Sender sender, boolean active) {
		app.active = active;
		app.sender = sender;
		app.save();
		return app;
	}
}

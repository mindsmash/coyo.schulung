package helper.bookmark;

import helper.RandomHelper;
import models.bookmark.Bookmark;

public class BookmarkHelper {

	public static Bookmark createRandom() {
		return createRandom(null);
	}

	public static Bookmark createRandom(Integer priority) {
		String label = RandomHelper.getRandomString(20);
		String url = RandomHelper.getRandomUrl(20, 3);

		return create(label, url);
	}

	public static Bookmark create(String label, String url) {
		return create(label, url, null);
	}

	public static Bookmark create(String label, String url, Integer priority) {
		Bookmark bookmark = new Bookmark();
		bookmark.label = label;
		bookmark.url = url;
		if (priority != null) {
			bookmark.priority = priority;
		}
		bookmark.save();
		return bookmark;
	}
}

package helper;

import models.comments.Comment;
import models.User;

public class CommentHelper {

	public static Comment createComment(User user) {
		return createComment(user, RandomHelper.getRandomText(50));
	}

	public static Comment createComment(User user, String commentText) {
		Comment comment = new Comment();
		comment.text = commentText;
		comment.author = user;
		comment.save();
		return comment;
	}

}

package helper;

import models.StaticUserGroup;
import models.UserGroup;

public class GroupHelper {

	public static UserGroup createNewGroup() {
		UserGroup group = new StaticUserGroup();
		group.name = RandomHelper.getRandomString(25);
		group.save();
		return group;
	}
}
package helper.messaging;

import helper.RandomHelper;
import models.Sender;
import models.messaging.Conversation;
import models.messaging.ConversationEvent;
import models.wall.post.ConversationPost;

public class ConversationPostHelper {

	public static ConversationPost addConversationPost(Conversation conversation, String message) {
		ConversationPost post = new ConversationPost();
		post.author = RandomHelper.getRandom(conversation.getUsers());
		if (message == null) {
			message = RandomHelper.getRandomText(150);
		}
		post.message = message;
		post.wall = conversation.getDefaultWall();
		post.conversation = conversation;
		post.save();
		return post;
	}

	public static ConversationPost joinConversation(Conversation conversation, Sender author) {
		return conversationEvent(conversation, author, ConversationEvent.JOIN);
	}

	public static ConversationPost leaveConversation(Conversation conversation, Sender author) {
		return conversationEvent(conversation, author, ConversationEvent.LEAVE);
	}

	public static ConversationPost conversationEvent(Conversation conversation, Sender author, ConversationEvent event) {
		ConversationPost post = ConversationPostHelper.addConversationPost(conversation, null);
		post.author = author;
		post.event = event;
		post.save();
		return post;
	}
}

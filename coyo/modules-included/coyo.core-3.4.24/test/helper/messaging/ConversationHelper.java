package helper.messaging;

import models.User;
import models.messaging.Conversation;
import models.messaging.ConversationMember;

public class ConversationHelper {

	public static Conversation createConversation(User... users) {
		Conversation conversation = new Conversation();

		for (User user : users) {
			addConversationMember(conversation, user);
		}

		conversation.save();
		return conversation;
	}

	public static ConversationMember addConversationMember(Conversation conversation, User user) {
		ConversationMember member = new ConversationMember();
		member.user = user;
		member.conversation = conversation;
		conversation.members.add(member);
		return member;
	}
}

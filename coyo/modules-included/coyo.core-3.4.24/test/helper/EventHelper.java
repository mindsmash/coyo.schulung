package helper;

import models.User;
import models.event.Event;
import models.event.EventUser;
import models.event.EventUser.EventResponse;

import org.joda.time.DateTime;

public class EventHelper {

	public static Event createEvent(User user) {
		Event event = new Event(user);
		event.name = RandomHelper.getRandomString(25);
		event.location = RandomHelper.getRandomString(20);
		event.startDate = new DateTime();
		event.save();
		return event;
	}

	public static void inviteUserToEvent(User user, Event event, EventResponse response) {
		event.invite(user);
		for (EventUser eventUser : event.users) {
			if (user.equals(eventUser.user)) {
				eventUser.response = response;
			}
		}
		event.save();
	}
}

package helper.media;

import helper.BlobHelper;
import helper.RandomHelper;
import models.User;
import models.medialibrary.MediaFile;

public class MediaFileHelper {

	public static MediaFile createMediaFile(User user) {
		MediaFile mediaFile = new MediaFile();
		mediaFile.name = RandomHelper.getRandomString(20);
		mediaFile.data = BlobHelper.getDummyBlob();
		mediaFile.library = user.getMediaLibrary();
		mediaFile.save();
		return mediaFile;
	}
}

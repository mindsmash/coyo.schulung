package helper;

import java.util.Date;
import java.util.Random;

import models.Application;
import models.Sender;
import models.User;
import models.wall.Wall;
import models.wall.post.ConversationPost;
import models.wall.post.LikeLinkPost;
import models.wall.post.Post;
import models.wall.post.SimplePost;

import org.apache.commons.lang.time.DateUtils;

public class PostHelper {

	private static void initPost(Post post, Sender sender, Wall wall, boolean randomDate) {
		post.message = RandomHelper.getRandomText(35);
		post.author = sender;
		post.wall = wall;
		Random random = new Random();
		if (randomDate) {
			post.created = DateUtils.addDays(new Date(), -(1 + random.nextInt(364)));
		}
		post.important = random.nextBoolean();
	}

	public static Post simplePost(Sender sender, boolean randomDate) {
		return simplePost(sender, Application.get().getDefaultWall(), randomDate);
	}

	public static Post simplePost(Sender sender, Wall wall, boolean randomDate) {
		SimplePost post = new SimplePost();
		initPost(post, sender, wall, randomDate);
		post.save();
		return post;
	}

	public static Post likeSimplePost(User user, boolean randomDate) {
		return likeSimplePost(user, Application.get().getDefaultWall(), randomDate);
	}

	public static Post likeSimplePost(User user, Wall wall, boolean randomDate) {
		Post post = simplePost(user, wall, randomDate);
		post.like(user);
		return post;
	}

	public static LikeLinkPost likeLinkPost(User user, boolean randomDate) {
		return likeLinkPost(user, Application.get().getDefaultWall(), randomDate);
	}

	public static LikeLinkPost likeLinkPost(User user, Wall wall, boolean randomDate) {
		LikeLinkPost post = new LikeLinkPost();
		initPost(post, user, wall, randomDate);
		post.save();
		return post;
	}
	
	public static Post genericPost(User user, Class<? extends Post> clazz) throws InstantiationException, IllegalAccessException {
		Post post = clazz.newInstance();
		initPost(post, user, user.getDefaultWall(), true);
		return post;
	}
}

package helper;

import java.util.List;

import models.User;
import models.page.Page;

public class PageHelper {

	public static void ensurePages(User user, int amount) {
		List<Page> pages = Page.findAll();
		while (pages.size() < amount) {
			pages.add(createPage(user));
		}
		while (user.getPages(null).size() < amount) {
			Page page = RandomHelper.getRandom(pages);
			if (!page.members.contains(user) && !page.followers.contains(user)) {
				page.members.add(user);
				page.followers.add(user);
				page.save();
			}
		}
	}

	public static Page createOwnPage(User user) {
		Page page = prepare(user);
		page.save();
		return page;
	}

	public static Page createPage(User user) {
		return createPage(user, false, false);
	}

	public static Page createPage(User user, boolean forceAllFollow, boolean follow) {
		Page page = prepare(UserHelper.getAnotherUser(user));
		page.forceAllFollow = forceAllFollow;
		if (follow) {
			page.followers.add(user);
		}
		page.save();
		return page;
	}

	private static Page prepare(User user) {
		Page page = new Page();
		page.name = RandomHelper.getRandomString(35);
		page.creator = user;
		page.members.add(user);
		return page;
	}
}

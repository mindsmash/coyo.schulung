package helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.User;

import org.apache.commons.lang.time.DateUtils;

import util.Util;

public class UserHelper {

	public static User getRandomUser() {
		return getRandomUser(-1);
	}

	public static User getAnotherUser(User user) {
		return getRandomUser(user.id);
	}

	public static User getAnotherUser(User... users) {
		List<Long> ids = new ArrayList<Long>();
		for (User user : users) {
			ids.add(user.id);
		}
		String search = Util.implode(ids, ",");
		List<User> userList = User.find("active = true AND id NOT IN (" + search + ")").fetch();
		return RandomHelper.getRandom(userList);
	}

	private static User getRandomUser(long id) {
		List<User> userList = User.find("active = true AND id != ?", id).fetch();
		return RandomHelper.getRandom(userList);
	}

	public static User createNewUser() {
		User user = new User();
		user.firstName = RandomHelper.getRandomString(25);
		user.lastName = RandomHelper.getRandomString(25);
		user.email = user.firstName + "." + user.lastName + "@" + RandomHelper.getRandomString(15) + "."
				+ RandomHelper.getRandomString(3);
		user.setPassword(RandomHelper.getRandomString(20));
		user.birthdate = DateUtils.addDays(new Date(), -(10 + RandomHelper.random.nextInt(90)));
		user.about = RandomHelper.getRandomText(500);
		user.save();
		return user;
	}
}
package helper;

import java.util.List;
import java.util.Random;

public class RandomHelper {

	private static final String CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String CHARACTERS_FOR_TEXT = CHARACTERS + " \n";
	public static final Random random = new Random();

	public static String getRandomString(int len) {
		return RandomHelper.getRandomString(CHARACTERS, len);
	}

	public static String getRandomText(int len) {
		return RandomHelper.getRandomString(CHARACTERS_FOR_TEXT, len);
	}

	static String getRandomString(String chars, int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(chars.charAt(random.nextInt(chars.length())));
		}
		return sb.toString();
	}

	public static <T> T getRandom(List<T> input) {
		Random random = new Random();
		return input.get(random.nextInt(input.size()));
	}

	public static String getRandomUrl(int urlLength, int domainLength) {
		return "http://" + RandomHelper.getRandomString(urlLength) + "." + RandomHelper.getRandomString(domainLength);
	}

}
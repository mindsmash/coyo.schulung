package helper;

import models.Settings;
import models.User;
import models.workspace.Workspace;
import conf.ApplicationSettings;

public class WorkspaceHelper {

	public static Workspace createWorkspace(User creatorAndAdmin, User... members) {
		Workspace workspace = new Workspace(creatorAndAdmin);
		workspace.name = RandomHelper.getRandomString(50);
		workspace.save();
		for (User user : members) {
			workspace.addMember(user, false);
		}
		return workspace;
	}

	public static void enableWorkspaces(boolean enable) {
		Settings.findApplicationSettings().setProperty(ApplicationSettings.WORKSPACES, String.valueOf(enable));
	}

}

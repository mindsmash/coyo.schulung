package helper.notification;

import helper.EventHelper;
import helper.PageHelper;
import models.User;
import models.notification.EventChangedNotification;
import models.notification.EventNotification;
import models.notification.EventReminderNotification;
import models.notification.EventResponseNotification;
import models.notification.NewEventNotification;
import models.notification.NewPageNotification;
import models.notification.NewPostNotification;
import models.notification.NewUserNotification;
import models.notification.NewWorkspaceNotification;
import models.notification.Notification;
import models.notification.PageNotification;
import models.workspace.Workspace;

public class NotificationHelper {

	public static NewPostNotification addNewPostNotification(User user, Notification.DisplayType type) {
		NewPostNotification notification = new NewPostNotification();
		if (type != null) {
			notification.displayType = type;
		}
		notification.save();
		user.receiveNotification(notification);
		user.refresh();
		return notification;
	}

	public static NewUserNotification addNewUserNotification(User user, Notification.DisplayType type) {
		NewUserNotification notification = new NewUserNotification();
		if (type != null) {
			notification.displayType = type;
		}
		notification.user = user;
		notification.save();
		user.receiveNotification(notification);
		user.refresh();
		return notification;
	}

	public static NewWorkspaceNotification addNewWorkspaceNotification(User user, Notification.DisplayType type, Workspace workspace) {
		NewWorkspaceNotification notification = new NewWorkspaceNotification();
		if (type != null) {
			notification.displayType = type;
		}
		notification.workspace = workspace;
		notification.save();
		user.receiveNotification(notification);
		user.refresh();
		return notification;
	}

	public static EventChangedNotification addEventChangedNotification(User user, Notification.DisplayType type) {
		return addEventNotification(new EventChangedNotification(), user, type);
	}

	public static EventReminderNotification addEventReminderNotification(User user, Notification.DisplayType type) {
		return addEventNotification(new EventReminderNotification(), user, type);
	}

	public static EventResponseNotification addEventResponseNotification(User user, Notification.DisplayType type) {
		return addEventNotification(new EventResponseNotification(), user, type);
	}

	public static NewEventNotification addNewEventNotification(User user, Notification.DisplayType type) {
		return addEventNotification(new NewEventNotification(), user, type);
	}

	private static <T extends EventNotification> T addEventNotification(T notification, User user, Notification.DisplayType type) {
		notification.event = EventHelper.createEvent(user);
		return finish(notification, user, type);
	}

	public static NewPageNotification addNewPageNotification(User user, Notification.DisplayType type) {
		return addPageNotification(new NewPageNotification(), user, type);
	}

	private static <T extends PageNotification> T addPageNotification(T notification, User user, Notification.DisplayType type) {
		notification.page = PageHelper.createPage(user);
		return finish(notification, user, type);
	}

	private static <T extends Notification> T finish(T notification, User user, Notification.DisplayType type) {
		if (type != null) {
			notification.displayType = type;
		}
		notification.save();
		user.receiveNotification(notification);
		user.refresh();
		return notification;
	}
}

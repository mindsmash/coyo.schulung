package helper.embeddables;

import helper.RandomHelper;
import models.embeddables.Link;

public class LinkHelper {

	public static Link createLink() {
		String url = RandomHelper.getRandomUrl(20, 3);

		Link link = new Link();
		link.url = url;
		link.save();

		return link;
	}
}

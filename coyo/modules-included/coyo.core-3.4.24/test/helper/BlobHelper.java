package helper;

import java.io.File;
import java.io.FileInputStream;
import java.net.URISyntaxException;

import play.Play;
import play.exceptions.UnexpectedException;
import play.libs.MimeTypes;
import storage.FlexibleBlob;

public class BlobHelper {

	public static final String SAMPLE_IMAGE = "image.png";

	public static FlexibleBlob getDummyBlob() {
		try {
			FileInputStream fis = new FileInputStream(getSampleImage());
			FlexibleBlob blob = new FlexibleBlob();
			try {
				blob.set(fis, MimeTypes.getContentType(SAMPLE_IMAGE));
			} finally {
				fis.close();
			}
			return blob;
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	public static File getSampleImage() {
		try {
			return new File(Play.classloader.getResource("/" + SAMPLE_IMAGE).toURI());
		} catch (URISyntaxException e) {
			throw new UnexpectedException(e);
		}
	}
}

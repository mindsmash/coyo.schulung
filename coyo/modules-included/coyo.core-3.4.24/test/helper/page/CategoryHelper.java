package helper.page;

import helper.RandomHelper;
import models.page.PageCategory;

public class CategoryHelper {

	public static PageCategory createCategory() {
		return createCategory(null);
	}

	public static PageCategory createCategory(Integer priority) {
		PageCategory category = new PageCategory();
		category.name = RandomHelper.getRandomString(25);
		if (priority != null) {
			category.priority = priority;
		}
		category.save();
		return category;
	}

}

package mta;

import helper.BlobHelper;
import helper.UserHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import models.Application;
import models.Sender;
import models.User;
import multitenancy.MTA;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Test;

import play.Logger;
import play.Play;
import play.libs.Codec;
import play.libs.MimeTypes;
import storage.FlexibleBlob;
import test.FixturesHelper;
import test.UnitTestBase;
import utils.ImageUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import data.TenantDataExporter;
import data.TenantDataImporter;

public class TenantDataExportImportTest extends UnitTestBase {

	@Test
	public void checkExportImport() throws Exception {
		File export = createExport();

		JsonObject packageInfo = null;
		boolean xmlFound = false;
		boolean blobFound = false;

		ZipFile zipFile = new ZipFile(export);
		try {
			ZipEntry packageInfoZe = zipFile.getEntry(TenantDataExporter.EXPORT_INFO_FILE);
			JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(IOUtils.toString(zipFile.getInputStream(packageInfoZe)));
			packageInfo = jsonElement.getAsJsonObject();

			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) entries.nextElement();

				if (!ze.isDirectory()) {
					if (ze.getName().startsWith(TenantDataExporter.EXPORT_METADATA_FOLDER)
							&& ze.getName().endsWith(".xml")) {
						xmlFound = true;
					}
					if (ze.getName().startsWith(TenantDataExporter.EXPORT_BLOBDATA_FOLDER)) {
						blobFound = true;
					}
				}
			}
		} finally {
			zipFile.close();
		}

		assertEquals(Application.getDbSchemaVersion(), packageInfo.get(TenantDataExporter.PKG_INFO_DB_VERSION)
				.getAsString());
		assertEquals(Application.getApplicationVersion(), packageInfo.get(TenantDataExporter.PKG_INFO_APP_VERSION)
				.getAsString());
		DateTime created = new DateTime(packageInfo.get(TenantDataExporter.PKG_INFO_CREATED).getAsLong());
		assertTrue(created.toLocalDate().isEqual(new LocalDate()));
		assertTrue("no xml", xmlFound);
		assertTrue("no blob", blobFound);

		long userCount = User.count();

		FixturesHelper.resetEmptyTenant();
		assertEquals(0L, User.count());
		assertEquals(0, FlexibleBlob.storage.listAll().size());

		TenantDataImporter importer = new TenantDataImporter(export);
		importer.startImport(MTA.getActiveTenant());

		refreshTx();

		assertEquals(userCount, User.count());
		assertTrue(FlexibleBlob.storage.listAll().size() > 0);
	}

	private static File createExport() throws Exception {
		Logger.debug("starting export for tenant: %s", MTA.getActiveTenant());

		// add a blob
		User user = UserHelper.getRandomUser();
		File image = BlobHelper.getSampleImage();
		FileInputStream fis1 = new FileInputStream(image);
		FileInputStream fis2 = new FileInputStream(image);
		try {
			ImageUtils.scaleToWidth(fis1, MimeTypes.getContentType(image.getName()), user.avatar, Sender.AVATAR_WIDTH,
					true);
			ImageUtils.setThumb(fis2, MimeTypes.getContentType(image.getName()), user.thumbnail, Sender.THUMB_WIDTH);
			user.save();
		} finally {
			fis1.close();
			fis2.close();
		}

		File export = new File(Play.tmpDir, "dbexport-" + Codec.UUID());
		export.createNewFile();
		FileOutputStream fos = new FileOutputStream(export);
		try {
			TenantDataExporter.export(MTA.getActiveTenant(), fos);
		} finally {
			fos.close();
		}
		return export;
	}
}

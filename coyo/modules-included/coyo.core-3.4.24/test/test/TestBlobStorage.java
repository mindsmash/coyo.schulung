package test;

import java.io.IOException;

import org.apache.commons.io.FileUtils;

import play.exceptions.UnexpectedException;
import storage.local.LocalFileStorage;

public class TestBlobStorage extends LocalFileStorage {

	public void clear() {
		try {
			FileUtils.cleanDirectory(getStore());
		} catch (IOException e) {
			throw new UnexpectedException(e);
		}
	}
}

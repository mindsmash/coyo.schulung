package test;

import models.Tenant;
import multitenancy.TenantResolver;
import play.mvc.Http.Request;

public class TestTenantResolver implements TenantResolver {

	@Override
	public Tenant getActiveTenant(Request request) {
		return TestCoyoTenant.get();
	}
}

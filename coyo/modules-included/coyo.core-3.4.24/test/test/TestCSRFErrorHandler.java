package test;

import play.Logger;
import controllers.CSRF.CSRFErrorHandler;

public class TestCSRFErrorHandler extends CSRFErrorHandler {

	@Override
	public void handleIncorrectAuthenticityToken() {
		Logger.debug("ignoring bad authenticity token");
	}

	@Override
	public void handleUnavailableSessionCookie() {
		Logger.debug("ignoring unavailable session cookie");
	}
}

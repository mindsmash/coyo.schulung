package test;

import models.AbstractTenant;
import models.Settings;
import models.StaticUserGroup;
import models.User;
import models.UserGroup;
import models.app.CalendarApp;
import models.app.WallApp;
import models.event.Event;
import models.event.EventBase.EventVisibility;
import models.page.Page;
import models.page.Page.PageVisibility;
import multitenancy.MTA;

import org.joda.time.DateTime;

import play.Logger;
import play.cache.Cache;
import play.db.jpa.JPA;
import play.jobs.JobsPlugin;
import play.test.Fixtures;
import plugins.PluginBase;
import plugins.PluginManager;
import utils.JPAUtils;
import apps.AppDescriptor;
import apps.AppManager;
import conf.ApplicationSettings;

public class FixturesHelper {

	private FixturesHelper() {
	}

	public static void resetAndLoadTestStateForSelenium() {
		resetAndLoadTestState();
		resetApplicationSettings();

		// activate all plugins and apps
		Logger.debug("Activations plugins and apps for tenant [%s]", MTA.getActiveTenant());
		for (PluginBase plugin : PluginManager.getPlugins()) {
			PluginManager.activate(plugin);
		}
		for (AppDescriptor app : AppManager.getApps()) {
			AppManager.toggle(app, "page", true);
			AppManager.toggle(app, "workspace", true);
		}

		// deactivate tour plugin
		PluginManager.deactivate(PluginManager.getPlugin("tour"));
	}

	private static void customFixtures() {
		User jan = User.find("email = ?", "janmarq@mac.com").first();
		User daniel = User.find("email = ?", "daniel.busch@mindsmash.com").first();
		User bob = User.find("email = ?", "bob@gmail.com").first();

		UserGroup group1 = StaticUserGroup.find("name = ?", "Testgroup1").first();
		UserGroup group2 = StaticUserGroup.find("name = ?", "Testgroup2").first();
		UserGroup group3 = StaticUserGroup.find("name = ?", "Testgroup3").first();

		Page infoPage1 = new Page();
		infoPage1.name = "Page1";
		infoPage1.about = "About1";
		infoPage1.creator = jan;
		infoPage1.admins.add(bob);
		infoPage1.memberGroups.add(group3);
		infoPage1.visibility = PageVisibility.PRIVATE;
		infoPage1.save();

		CalendarApp calendarApp1 = new CalendarApp();
		calendarApp1.displayName = "Calendar1";
		calendarApp1.sender = infoPage1;
		calendarApp1.save();

		Event event1 = new Event(daniel);
		event1.name = "Event1";
		event1.location = "Hamburg";
		event1.visibility = EventVisibility.PRIVATE;
		event1.startDate = new DateTime();
		event1.endDate = new DateTime().plusHours(1);
		event1.calendar = calendarApp1;
		event1.save();

		Page infoPage2 = new Page();
		infoPage2.name = "Page2";
		infoPage2.about = "About2";
		infoPage2.creator = jan;
		infoPage2.admins.add(daniel);
		infoPage2.visibility = PageVisibility.PRIVATE;
		infoPage2.save();

		WallApp wallApp = new WallApp();
		wallApp.title = "Wall";
		wallApp.sender = infoPage2;
		wallApp.save();

		infoPage1.refresh();
		infoPage2.refresh();
	}

	public static void resetAndLoadTestStateForUnit() {
		resetAndLoadTestState();
		resetApplicationSettings();
	}

	private static void resetApplicationSettings() {
		// settings
		Settings settings = Settings.findApplicationSettings();
		settings.setProperty(ApplicationSettings.DOWNLOADS, "true");
		settings.setProperty(ApplicationSettings.MOBILE_LINK, "true");
		settings.setProperty(ApplicationSettings.REGISTRATION, "true");
		settings.setProperty(ApplicationSettings.ALLOW_ACCOUNT_DELETION, "true");
		settings.setProperty(ApplicationSettings.WORKSPACES, "true");
		settings.setProperty(ApplicationSettings.MESSAGING, "true");
		settings.setProperty(ApplicationSettings.MESSAGING_CHAT, "true");
		settings.setProperty(ApplicationSettings.MESSAGING_WEBRTC, "true");
		settings.setProperty(ApplicationSettings.EMBEDDABLES, "true");
		settings.setProperty(ApplicationSettings.STATISTICS, "true");
		settings.setProperty(ApplicationSettings.APPLICATION_NAME, "Coyo");
		settings.save();
	}

	public static void resetEmptyTenant() {
		// delete current tenant - null check for multi tenant environment
		AbstractTenant tenant = MTA.getActiveTenant();
		if (tenant != null) {
			// make sure tenant is managed
			tenant = JPAUtils.ensureManaged(tenant);

			MTA.activateMultitenancy(tenant);
			tenant.delete();
			JPAUtils.commitAndRefreshTransaction();
		}

		Logger.debug("Clearing database");

		MTA.clearThreadLocals();
		MTA.forceMultitenancyDeactivation();

		Fixtures.deleteDatabase();
		JPA.em().clear();
		Cache.clear();
		new TestBlobStorage().clear();

		MTA.clearThreadLocals();
		MTA.activateMultitenancy();
	}

	private static void resetAndLoadTestState() {
		resetEmptyTenant();

		Logger.debug("Loading fixtures for tenant [%s]", MTA.getActiveTenant());
		Fixtures.loadModels("test-data.yml");
		customFixtures();
	}
}

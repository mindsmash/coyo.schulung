package test;

import models.User;

import org.junit.After;
import org.junit.BeforeClass;

import play.db.jpa.JPAPlugin;
import play.mvc.Http.Request;
import play.test.UnitTest;
import session.UserLoader;

/**
 * Base class for unit tests preparing the test environment.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public abstract class UnitTestBase extends UnitTest {

	@BeforeClass
	public static void loadDataAndForceTenant() {
		FixturesHelper.resetAndLoadTestStateForUnit();
	}

	@After
	public void tearDown() {
		UserLoader.clearThreadLocalUser();
	}

	protected void setActiveUser(User user) {
		if (Request.current() != null) {
			Request.current().args.put("user", user);
		} else {
			UserLoader.setThreadLocalUser(user.id);
		}
	}
	
	protected void refreshTx() {
		JPAPlugin.closeTx(false);
		JPAPlugin.startTx(false);
	}
}

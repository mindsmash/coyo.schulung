package test;

import models.User;
import multitenancy.MTA;

import org.junit.After;
import org.junit.Before;

import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.mvc.Http.Header;
import play.mvc.Http.Request;
import play.test.FunctionalTest;
import controllers.TokenAuthController;

/**
 * Base class for functional tests preparing the test environment.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public abstract class FunctionalTestBase extends FunctionalTest {

	@Before
	public void loadDataAndForceTenant() {
		checkTransaction();

		FixturesHelper.resetAndLoadTestStateForUnit();

		// save data immediately so that the new tenant
		// is available for new requests
		refreshTx();
	}

	protected void beforeTestStateInitialization() {
		// noop
	}

	public void checkTransaction() {
		if (JPA.isEnabled() && !JPA.isInsideTransaction()) {
			JPAPlugin.startTx(false);
		}
	}

	@After
	public void closeTransaction() {
		if (JPA.isEnabled() && JPA.isInsideTransaction()) {
			JPAPlugin.closeTx(false);
		}
	}

	protected void refreshTx() {
		JPAPlugin.closeTx(false);
		JPAPlugin.startTx(false);
		MTA.activateMultitenancy();
	}

	protected Request createRequestWithAuthToken(User user) {
		Request request = newRequest();
		request.headers.put(TokenAuthController.HEADER_NAME, new Header(TokenAuthController.HEADER_NAME,
				TokenAuthController.createToken(user.id)));
		return request;
	}
}

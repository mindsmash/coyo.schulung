package test;

import javax.persistence.Entity;

import models.Tenant;
import play.Logger;
import play.db.jpa.JPA;

@Entity
public class TestCoyoTenant extends Tenant {

	public TestCoyoTenant() {
	}

	public TestCoyoTenant(Long id) {
		this.id = id;
	}

	public static TestCoyoTenant get() {
		if (TestCoyoTenant.count() == 0) {
			TestCoyoTenant t = new TestCoyoTenant().save();
			Logger.debug("Creating test tenant with ID: " + t.id);
			return t;
		}
		return find("").first();
	}

	@Override
	protected void beforeSave() throws Exception {
		if (!isPersistent() && count() >= 1) {
			throw new Error("cannot create more than one test coyo tenant");
		}
		super.beforeSave();
	}
}

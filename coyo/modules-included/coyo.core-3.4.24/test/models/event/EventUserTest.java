package models.event;

import models.event.EventUser.EventResponse;

import org.junit.Test;

import test.UnitTestBase;

public class EventUserTest extends UnitTestBase {

	@Test
	public void checkIcons() {
		assertEquals("icon-ok-sign", EventResponse.ATTENDING.icon);
		assertEquals("icon-question-sign", EventResponse.MAYBE.icon);
		assertEquals("icon-time", EventResponse.WAITING.icon);
		assertEquals("icon-remove-sign", EventResponse.NOT_ATTENDING.icon);
	}

	@Test
	public void checkLabel() {
		EventResponse[] values = EventResponse.values();
		for (EventResponse response : values) {
			assertFalse(response.getLabel().startsWith("event.response"));
		}
	}

	@Test
	public void checkStatus() {
		EventResponse[] values = EventResponse.values();
		for (EventResponse response : values) {
			assertFalse(response.getLabel().startsWith("event.responded"));
		}
	}

	@Test
	public void checkReceiveUpdates() {
		assertTrue(EventResponse.ATTENDING.receiveUpdates());
		assertTrue(EventResponse.MAYBE.receiveUpdates());
		assertFalse(EventResponse.WAITING.receiveUpdates());
		assertFalse(EventResponse.NOT_ATTENDING.receiveUpdates());
	}
}

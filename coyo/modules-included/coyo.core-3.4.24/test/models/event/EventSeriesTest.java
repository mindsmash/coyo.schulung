package models.event;

import helper.UserHelper;
import junit.framework.Assert;
import models.User;
import models.app.CalendarApp;
import models.event.EventBase.EventVisibility;
import models.wall.Wall;

import org.joda.time.DateTime;
import org.junit.Test;

import play.Logger;

import storage.FlexibleBlob;
import test.UnitTestBase;

public class EventSeriesTest extends UnitTestBase {
	@Test
	public void testChildAttributeShadowing() throws Exception {
		// TODO update after class hierarchy change
		User creator = UserHelper.getRandomUser();
		EventSeries series = prepareEventSeries(creator);
		series.calendar = new CalendarApp();

		// assert that values are equal to the masters values
		EventSeriesChild child = new EventSeriesChild(series, DateTime.now(), DateTime.now().plusHours(1));
		Assert.assertEquals(series.admins, child.admins);
		Assert.assertEquals(series.about, child.about);
		Assert.assertEquals(series.allowGuestInvite, child.allowGuestInvite);
		Assert.assertEquals(series.avatar, child.avatar);
		Assert.assertEquals(series.calendar, child.calendar);
		Assert.assertEquals(series.creator, child.creator);
		Assert.assertEquals(series.fulltime, child.fulltime);
		Assert.assertEquals(series.location, child.location);
		Assert.assertEquals(series.name, child.name);
		Assert.assertEquals(series.sendReminder, child.sendReminder);
		Assert.assertEquals(series.thumbnail, child.thumbnail);
		Assert.assertEquals(series.suppressMaybe, child.suppressMaybe);
		Assert.assertNotSame(series.users, child.users);
		Assert.assertEquals(series.users.size(), child.users.size());
		Assert.assertEquals(series.visibility, child.visibility);

		// TODO: only works with persistent objects
		// Assert.assertNotNull(child.getDefaultWall());
		// Assert.assertNotSame(series.getDefaultWall(), child.getDefaultWall());

		// now modify some child attributes and assert that they now differ from the master values
		child.name = "child name";
		child.sendReminder = 23;
		child.visibility = EventVisibility.PUBLIC;
		child.fulltime = !child.fulltime;

		Assert.assertNotSame(series.name, child.name);
		Assert.assertNotSame(series.sendReminder, child.sendReminder);
		Assert.assertNotSame(series.visibility, child.visibility);
		Assert.assertNotSame(series.fulltime, child.fulltime);
	}

//	@Test
//	public void testCreateAndDelete() throws InterruptedException {
//		long seriesCount = EventSeries.count();
//
//		User creator = UserHelper.getRandomUser();
//		EventSeries series = prepareEventSeries(creator);
//		series.save();
//		assertEquals(seriesCount + 1, EventSeries.count());
//
//		series.delete();
//		assertEquals(seriesCount, EventSeries.count());
//	}

	private EventSeries prepareEventSeries(User creator) {
		EventSeries series = new EventSeries(creator, "FREQ:DAILY");
		series.admins.add(creator);
		series.about = "about";
		series.allowGuestInvite = !series.allowGuestInvite;
		series.avatar = new FlexibleBlob("avatar", "type");
		series.fulltime = !series.fulltime;
		series.location = "location";
		series.name = "name";
		series.sendReminder = 1;
		series.suppressMaybe = !series.suppressMaybe;
		series.thumbnail = new FlexibleBlob("thumbnail", "type");
		series.visibility = EventVisibility.CLOSED;
		series.startDate = new DateTime();
		series.users.add(new EventUser(creator, series));
		series.users.add(new EventUser(creator, series));
		return series;
	}
}

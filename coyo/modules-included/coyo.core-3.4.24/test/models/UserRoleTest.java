package models;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class UserRoleTest extends UnitTestBase {
	private UserRole role;

	@Before
	public void before() {
		role = new UserRole();
	}

	@Test
	public void hasGrant() {
		String grant = "test";

		UserRole role = new UserRole();
		assertFalse(role.hasGrant(grant));

		role.grants.add(grant);
		assertTrue(role.hasGrant(grant));
	}

	@Test
	public void checkGroupUsersCount() {
		List<UserRole> roles = UserRole.findAll();
		for (UserRole userRole : roles) {
			assertEquals(userRole.getGroupUsers().size(), userRole.getGroupUserCount());
		}
	}

	@Test
	public void checkGroupUsers() {
		addRandomUsers(5, 10, 15);
		assertEquals(30, role.getGroupUsers().size());
	}

	private void addRandomUsers(int... amounts) {
		for (int amount : amounts) {
			role.groups.add(getTemporaryGroup(amount));
		}
	}

	private UserGroup getTemporaryGroup(int userCount) {
		UserGroup group = new UserGroup() {
		};
		for (int index = 0; index < userCount; index++) {
			group.users.add(new User());
		}
		return group;
	}
}

package models;

import helper.AppHelper;
import helper.EventHelper;
import helper.PageHelper;
import helper.PostHelper;
import helper.UserHelper;
import helper.WorkspaceHelper;

import java.util.List;

import models.User.ActivityStreamSelect.DisplayType;
import models.app.WallApp;
import models.app.forum.ForumPost;
import models.event.Event;
import models.event.EventUser.EventResponse;
import models.page.Page;
import models.wall.ExcludeFromWall;
import models.wall.Wall;
import models.wall.post.ConversationPost;
import models.wall.post.Post;
import models.workspace.Workspace;

import org.junit.Test;

import play.db.jpa.JPA;
import test.UnitTestBase;

public class User_ActivityStreamSelectTest extends UnitTestBase {
	private User user;

	public User_ActivityStreamSelectTest() {
		user = UserHelper.createNewUser();
		setActiveUser(user);
	}

	@Test
	public void checkNewActivityStream() {
		// TODO
	}

	@Test
	public void checkNewUser() {
		assertValidPosts();
	}

	@Test
	public void checkOwnWall() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		// add post to own wall
		PostHelper.simplePost(user, user.getDefaultWall(), true);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkApplicationWall() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		// add post to application wall
		PostHelper.simplePost(user, Application.get().getDefaultWall(), true);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkUserEventAttending() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postUserEvent(EventResponse.ATTENDING);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkUserEventMaybe() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postUserEvent(EventResponse.MAYBE);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkUserEventNotAttending() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postUserEvent(EventResponse.NOT_ATTENDING);
		assertValidPosts();

		// did not add post
		assertEquals(amount, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkUserEventWaiting() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postUserEvent(EventResponse.WAITING);
		assertValidPosts();

		// did not add post
		assertEquals(amount, user.getActivityStream(DisplayType.NATURAL).size());
	}

	private void postUserEvent(EventResponse response) {
		Event event = EventHelper.createEvent(UserHelper.getAnotherUser(user));
		event.invite(user);
		event.respond(user, response);
		event.save();
		PostHelper.simplePost(event.creator, event.getDefaultWall(), true);
	}

	@Test
	public void checkWallAppPageImportantNotFollowed() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postWallAppPage(true, false);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkWallAppPageFollowedNotImportant() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postWallAppPage(false, true);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkWallAppPageImportantAndFollowed() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postWallAppPage(true, true);
		assertValidPosts();

		// added 1 post
		assertEquals(amount + 1, user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void checkWallAppPageNotImportantAndNotFollowed() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		postWallAppPage(false, false);
		assertValidPosts();

		// did not add post
		assertEquals(amount, user.getActivityStream(DisplayType.NATURAL).size());
	}

	private void postWallAppPage(boolean important, boolean follow) {
		Page page = PageHelper.createPage(user, important, follow);
		WallApp app = AppHelper.createWallApp(page, true);
		PostHelper.simplePost(app.sender, app.wall, true);
	}

	@Test
	public void checkWallAppWorkspace() {
		int amount = user.getActivityStream(DisplayType.NATURAL).size();

		Workspace workspace = WorkspaceHelper.createWorkspace(user);
		WallApp app = AppHelper.createWallApp(workspace, true);
		PostHelper.simplePost(app.sender, app.wall, true);

		assertValidPosts();

		// added 1 post
		WorkspaceHelper.enableWorkspaces(true);
		assertEquals("Incorrect amount of posts with active workspace.", amount + 1,
				user.getActivityStream(DisplayType.NATURAL).size());
		// not visible
		WorkspaceHelper.enableWorkspaces(false);
		assertEquals("Incorrect amount of posts with inactive workspace.", amount,
				user.getActivityStream(DisplayType.NATURAL).size());
	}

	@Test
	public void check() {
		// TODO AUTO FOLLOW etc
	}

	@Test
	public void checkConversation() {
		// TODO add post to a Conversation (wall.noActivityStream == true)
	}

	@Test
	public void checkConversationPost() {
		// TODO add a ConversationPost
	}

	private void assertValidPosts() {
		user.refresh();
		List<Post> posts = user.getActivityStream(DisplayType.NATURAL);
		assertTrue("Wall does not contain only valid posts.", containsOnlyValidPosts(posts, user));
	}

	private boolean containsOnlyValidPosts(List<Post> posts, User user) {
		boolean origin;
		for (Post post : posts) {
			Wall wall = post.wall;
			if (wall.noActivityStream == true) {
				return false;
			}
			if (post instanceof ConversationPost || post instanceof ForumPost
					|| post.getClass().isAnnotationPresent(ExcludeFromWall.class)) {
				return false;
			}

			origin = false;
			if (post.author.id == user.id) {
				origin = true;
			} else if (wall.id == user.getDefaultWall().id) {
				origin = true;
			} else if (wall.id == Application.get().getDefaultWall().id) {
				origin = true;
			} else {
				origin = user.following.contains(wall.sender);
			}

			if (origin == false) {
				return false;
			}
		}
		return true;
	}

	@Test
	public void checkSortingNatural() {
		assertTrue(isActivityStreamSorted(DisplayType.NATURAL));
	}

	@Test
	public void checkSortingNotNatural() {
		assertTrue(isActivityStreamSorted(DisplayType.TOP_POSTS));
	}

	private boolean isActivityStreamSorted(DisplayType displayType) {
		List<Post> posts = user.getActivityStream(displayType);
		if (posts.size() <= 1) {
			return true;
		}
		
//		for (Post post : posts) {
//			System.out.println("POST: " + post);
//			System.out.println("CREATED: " + post.created);
//			System.out.println("IMPORTANT: " + post.isImportant(user));
//		}
//		
//		posts = user.getActivityStream(displayType);
//		for (Post post : posts) {
//			System.out.println("POST: " + post);
//			System.out.println("CREATED: " + post.created);
//			System.out.println("IMPORTANT: " + post.isImportant(user));
//		}
		
		Post last = posts.get(0);
		boolean important = last.isImportant(user);
		for (int index = 1; index < posts.size(); index++) {
			Post current = posts.get(index);

			if (!important && current.isImportant(user)) {
				throw new AssertionError("found important post after non-important post");
			} else if (important && !current.isImportant(user)) {
				// importance changed -> no before check
				important = false;
				last = current;
				continue;
			}
			
			if (displayType == DisplayType.NATURAL) {
				if (last.id < current.id) {
					throw new AssertionError("wrong natural sorting. found post with id " + current.id
							+ " with a greater id than the last post with id " + last.id);
				}
			} else {
				// TODO: test sorting by popularity
				// if (current.getPopularityPoints() >
				// last.getPopularityPoints()) {
				// return false;
				// }
			}
			last = current;
		}
		return true;
	}
}
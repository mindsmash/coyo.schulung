package models;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class SettingsTest extends UnitTestBase {

	private static final String KEY = "test";
	private static final String SOME_STRING = "SOME_STRING";
	private Settings settings;

	@Before
	public void before() {
		settings = new Settings();
	}

	@Test
	public void checkContains() {
		settings.setProperty(KEY, SOME_STRING);
		assertTrue(settings.contains(KEY));
	}

	@Test
	public void checkString() {
		settings.setProperty(KEY, SOME_STRING);
		assertEquals(SOME_STRING, settings.getString(KEY));
	}

	@Test
	public void checkBooleanSuccess() {
		settings.setProperty(KEY, String.valueOf(true));
		assertEquals(true, settings.getBoolean(KEY));

		settings.setProperty(KEY, String.valueOf(false));
		assertEquals(false, settings.getBoolean(KEY));

		settings.setProperty(KEY, String.valueOf(SOME_STRING));
		assertEquals(false, settings.getBoolean(KEY));
	}

	@Test
	public void checkIntegerSuccess() {
		int value = Integer.MAX_VALUE;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getInteger(KEY));

		value = -Integer.MAX_VALUE;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getInteger(KEY));

		value = 0;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getInteger(KEY));
	}

	@Test(expected = NumberFormatException.class)
	public void checkIntegerFailure() {
		settings.setProperty(KEY, String.valueOf(SOME_STRING));
		settings.getInteger(KEY);
	}

	@Test
	public void checkLongSuccess() {
		long value = Long.MAX_VALUE;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getLong(KEY));

		value = -Long.MAX_VALUE;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getLong(KEY));

		value = 0;
		settings.setProperty(KEY, String.valueOf(value));
		assertEquals(value, settings.getLong(KEY));
	}

	@Test(expected = NumberFormatException.class)
	public void checkLongFailure() {
		settings.setProperty(KEY, String.valueOf(SOME_STRING));
		settings.getLong(KEY);
	}
}
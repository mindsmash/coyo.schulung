package models.messaging;

import helper.UserHelper;
import helper.messaging.ConversationHelper;

import java.util.List;

import models.User;
import models.wall.Wall;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class ConversationTest extends UnitTestBase {

	private Conversation conversation;
	private User user;
	private User anotherUser;

	@Before
	public void before() {
		user = UserHelper.getRandomUser();
		anotherUser = UserHelper.getAnotherUser(user);
		conversation = ConversationHelper.createConversation(user, anotherUser);
	}
	
	@Test
	public void checkSenderType() {
		assertEquals("conversation", conversation.getSenderType());
	}

	@Test
	public void checkWalls() {
		Wall wall = conversation.getDefaultWall();
		assertTrue(conversation.walls.contains(wall));
	}

	@Test
	public void checkDefaultWalls() {
		assertNotNull(conversation.getDefaultWall());
	}

	@Test
	public void checkAdmins() {
		User user = UserHelper.getRandomUser();
		User anotherUser = UserHelper.getAnotherUser(user);
		ConversationHelper.createConversation(user, anotherUser);

		assertTrue(conversation.getAdmins().size() == 0);
	}

	@Test
	public void checkThumbnailUser() {
		User user = conversation.getThumbnailUser();
		assertNotNull(user);
		assertTrue(user instanceof User);
	}

	@Test
	public void checkUsers() {
		List<User> users = conversation.getUsers();
		assertTrue(users.contains(user));
		assertTrue(users.contains(anotherUser));
	}

	@Test
	public void checkOtherUsers() {
		User user = UserHelper.getRandomUser();
		User anotherUser = UserHelper.getAnotherUser(user);
		conversation = ConversationHelper.createConversation(user, anotherUser);

		List<User> users = conversation.getOtherUsers(user);
		assertFalse(users.contains(user));
		assertTrue(users.contains(anotherUser));
	}

	@Test
	public void checkMemberObject() {
		ConversationMember member = conversation.getMemberObject(user);
		assertNotNull(member);
		assertTrue(member instanceof ConversationMember);
	}

	@Test
	public void checkActiveMemberCount() {
		assertTrue(user.isActive());
		assertTrue(anotherUser.isActive());
		assertEquals(2, conversation.getActiveMemberCount());
	}

	@Test(expected = RuntimeException.class)
	public void checkMemberAmountBeforeSave() {
		Conversation conversation = null;
		try {
			conversation = ConversationHelper.createConversation(user);
		} catch (RuntimeException e) {
			if (conversation != null) {
				for (ConversationMember member : conversation.members) {
					if (member.isPersistent()) {
						member.delete();
					}
				}
				if (conversation.isPersistent()) {
					conversation.delete();
				}
			}
			throw e;
		}
	}

	@Test
	public void checkSaveWithoutDuplicates() {
		int memberSize = conversation.members.size();
		assertTrue(memberSize > 0);

		User alreadyUser = conversation.members.get(0).user;
		ConversationMember newMember = ConversationHelper.addConversationMember(conversation, alreadyUser);
		
		conversation.save();

		assertEquals(memberSize, conversation.members.size());

		newMember.delete();
		conversation.save();

		assertEquals(memberSize, conversation.members.size());
	}

	@Test
	public void checkSearchPermissions() {
		setActiveUser(user);
		assertTrue(conversation.checkSearchPermission());

		User noMember = UserHelper.getAnotherUser(user, anotherUser);
		setActiveUser(noMember);
		assertFalse(conversation.checkSearchPermission());
	}
}

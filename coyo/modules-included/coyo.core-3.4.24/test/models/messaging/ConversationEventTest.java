package models.messaging;

import helper.UserHelper;

import java.util.List;

import models.User;

import org.junit.Test;

import play.test.UnitTest;
import test.UnitTestBase;

public class ConversationEventTest extends UnitTestBase {

	@Test
	public void checkHasLabel() {
		User user = UserHelper.getRandomUser();
		User anotherUser = UserHelper.getAnotherUser(user);
		ConversationEvent[] values = ConversationEvent.values();
		for (ConversationEvent event : values) {
			assertFalse(event.getLabel(user, anotherUser).startsWith("messaging."));
		}
	}
}

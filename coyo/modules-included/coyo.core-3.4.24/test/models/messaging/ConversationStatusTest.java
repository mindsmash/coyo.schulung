package models.messaging;

import java.util.List;

import org.junit.Test;

import test.UnitTestBase;

public class ConversationStatusTest extends UnitTestBase {

	@Test
	public void checkHasLabel() {
		ConversationStatus[] values = ConversationStatus.values();
		for (ConversationStatus status : values) {
			assertFalse(status.getLabel().startsWith("messaging."));
		}
	}

	@Test
	public void checkDefault() {
		List<ConversationStatus> defaultStatus = ConversationStatus.getDefault();
		assertTrue(defaultStatus.contains(ConversationStatus.INBOX));
		assertTrue(defaultStatus.contains(ConversationStatus.ARCHIVED));
	}
}

package models.messaging;

import helper.RandomHelper;
import helper.UserHelper;
import helper.messaging.ConversationHelper;
import helper.messaging.ConversationPostHelper;

import java.util.Date;
import java.util.List;

import models.User;
import models.messaging.ConversationMember.ConversationMemberJsonSerializer;
import models.wall.post.ConversationPost;
import onlinestatus.OnlineStatusService;
import onlinestatus.OnlineStatusServiceFactory;
import onlinestatus.UserOnlineStatus;
import onlinestatus.UserOnlineStatus.OnlineStatus;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import play.db.helper.JpqlSelect;
import test.UnitTestBase;

import com.google.gson.JsonObject;

public class ConversationMemberTest extends UnitTestBase {

	private Conversation conversation;
	private User user;
	private User anotherUser;
	private ConversationMember userMember;

	@Before
	public void before() {
		user = UserHelper.getRandomUser();
		anotherUser = UserHelper.getAnotherUser(user);
		conversation = ConversationHelper.createConversation(user, anotherUser);

		userMember = conversation.getMemberObject(user);
	}

	@Test
	public void checkSubject() {
		User thirdUser = UserHelper.getAnotherUser(user, anotherUser);
		ConversationHelper.addConversationMember(conversation, thirdUser);

		String subject = userMember.getSubject();
		assertFalse(subject.contains(user.getDisplayName()));
		assertTrue(subject.contains(anotherUser.getDisplayName()));
		assertTrue(subject.contains(thirdUser.getDisplayName()));
	}

	@Test
	public void checkArchived() {
		userMember.status = ConversationStatus.ARCHIVED;
		userMember.save();
		userMember.refresh();

		assertTrue(userMember.isArchived());
	}

	@Test
	public void checkUnreadNull() {
		userMember.lastAccess = null;
		userMember.save();
		userMember.refresh();

		assertTrue(userMember.isUnread());
	}

	@Test
	public void checkUnreadBefore() {
		userMember.lastAccess = DateUtils.addDays(conversation.modified, -1);
		userMember.save();
		userMember.refresh();

		assertTrue(userMember.isUnread());
	}

	@Test
	public void checkUnreadAfter() {
		userMember.lastAccess = DateUtils.addDays(conversation.modified, 1);
		userMember.save();
		userMember.refresh();

		assertFalse(userMember.isUnread());
	}

	@Test
	public void checkUpdateActiveBothOnline() {
		setUserOnlineStatus(user, true);
		setUserOnlineStatus(anotherUser, true);
		assertTrue(userMember.updateActive());
	}

	@Test
	public void checkUpdateActiveBothOffline() {
		setUserOnlineStatus(user, false);
		setUserOnlineStatus(anotherUser, false);
		assertFalse(userMember.updateActive());
	}

	@Test
	public void checkUpdateActiveOnlineOffline() {
		setUserOnlineStatus(user, true);
		setUserOnlineStatus(anotherUser, false);
		assertFalse(userMember.updateActive());
	}

	@Test
	public void checkUpdateActiveOfflineOnline() {
		setUserOnlineStatus(user, false);
		setUserOnlineStatus(anotherUser, true);
		assertFalse(userMember.updateActive());
	}

	private void setUserOnlineStatus(User user, boolean online) {
		OnlineStatusService service = OnlineStatusServiceFactory.getOnlineStatusService();
		if (online) {
			service.setOnlineStatus(user, OnlineStatus.ONLINE);
			assertEquals(OnlineStatus.ONLINE, user.getOnlineStatus().status);
		} else {
			service.removeOnlineStatus(user);
			assertEquals(OnlineStatus.OFFLINE, user.getOnlineStatus().status);
		}
	}

	@Test
	public void checkPostsSelectSimple() {
		userMember.showFrom = null;
		userMember.save();
		JpqlSelect select = userMember.getPostsSelect(null, null, null);

		String query = select.toString();
		assertTrue(query.contains("wall = ?"));
		assertFalse(query.contains("modified >= ?"));
		assertFalse(query.contains("id < ?"));
		assertFalse(query.contains("id > ?"));

		assertEquals(1, StringUtils.countMatches(query, "?"));
	}

	@Test
	public void checkPostsSelectComplete() {
		userMember.showFrom = new Date();
		JpqlSelect select = userMember.getPostsSelect(1L, 2L, 3L);

		String query = select.toString();
		assertTrue(query.contains("wall = ?"));
		assertTrue(query.contains("modified >= ?"));
		assertTrue(query.contains("id < ?"));
		assertTrue(query.contains("id > ?"));

		assertEquals(2, StringUtils.countMatches(query, "modified"));
		assertEquals(5, StringUtils.countMatches(query, "?"));
	}

	@Test
	public void checkPostsQueryAll() {
		prepareConversation(conversation, 10);
		assertEquals(10, checkPostQueryAmount(null, null, null).size());
	}

	@Test
	public void checkPostsQueryModified() {
		prepareConversation(conversation, 10);
		List<ConversationPost> allPosts = userMember.getPosts();
		assertEquals(10, allPosts.size());

		randomizeModifiedDate(allPosts, allPosts.size() / 2);

		ConversationPost post = RandomHelper.getRandom(allPosts);
		checkPostQueryAmount(null, null, post.modified.getTime());
	}

	private void randomizeModifiedDate(List<ConversationPost> posts, int offset) {
		for (int index = 0; index < posts.size(); index++) {
			ConversationPost conversationPost = posts.get(index);

			int amount = -offset + index;
			conversationPost.modified = DateUtils.addHours(conversationPost.modified, amount);
			conversationPost._save();
			conversationPost.refresh();
		}
	}

	@Test
	public void checkPostsQueryById() {
		prepareConversation(conversation, 10);
		List<ConversationPost> allPosts = userMember.getPosts();
		assertEquals(10, allPosts.size());

		int before = RandomHelper.random.nextInt(4);
		int after = (9 - before) - RandomHelper.random.nextInt(5 - before);
		Long beforeId = allPosts.get(before).id;
		Long afterId = allPosts.get(after).id;

		assertEquals(after - before - 1, checkPostQueryAmount(beforeId, afterId, null).size());

	}

	private List<ConversationPost> checkPostQueryAmount(Long beforeId, Long afterId, Long modifiedAfter) {
		List<ConversationPost> posts = userMember.getPostsQuery(beforeId, afterId, modifiedAfter).fetch();
		ConversationPost last = posts.get(0);
		for (int index = 1; index < posts.size(); index++) {
			ConversationPost current = posts.get(index);
			assertTrue(last.id > current.id);
			if (beforeId != null) {
				assertTrue(beforeId > last.id);
			}
			if (afterId != null) {
				assertTrue(afterId < current.id);
			}
			last = current;
		}
		return posts;
	}

	@Test
	public void checkUnreadPosts() {
		prepareUnreadPosts();

		List<ConversationPost> allPosts = userMember.getPosts();

		userMember.lastAccess = null;
		assertEquals(allPosts.size(), userMember.getUnreadPosts().size());
		
		userMember.lastAccess = new Date();
		List<ConversationPost> unread = userMember.getUnreadPosts();
		for (ConversationPost post : unread) {
			assertFalse(post.modified.before(userMember.lastAccess));
		}
	}

	@Test
	public void checkUnreadCount() {
		prepareUnreadPosts();
		
		List<ConversationPost> allPosts = userMember.getPosts();

		userMember.lastAccess = null;
		assertEquals(countForeignPosts(allPosts, null), userMember.getUnreadCount().longValue());
		userMember.lastAccess = new Date();
		assertEquals(countForeignPosts(allPosts, userMember.lastAccess), userMember.getUnreadCount().longValue());
	}

	private void prepareUnreadPosts() {
		int amountNoEvents = 10;
		prepareConversation(conversation, amountNoEvents);
		List<ConversationPost> allPosts = userMember.getPosts();
		assertEquals(amountNoEvents, allPosts.size());

		randomizeModifiedDate(allPosts, amountNoEvents / 2);

		ConversationPost join = ConversationPostHelper.joinConversation(conversation, anotherUser);
		join.modified = DateUtils.addHours(new Date(), -10);
		ConversationPost leave = ConversationPostHelper.leaveConversation(conversation, anotherUser);
		leave.modified = DateUtils.addHours(new Date(), 10);

		userMember.showFrom = null;
		assertEquals(amountNoEvents + 2, userMember.getPosts().size());
		userMember.refresh();
	}

	private int countForeignPosts(List<ConversationPost> posts, Date modified) {
		int count = 0;
		for (ConversationPost post : posts) {
			if (post.event == null && !post.author.equals(user)) {
				if (modified == null || !post.modified.before(modified)) {
					count++;
				}
			}
		}
		return count;
	}

	@Test
	public void checkPosts() {
		// no posts
		checkPosts(userMember, conversation);

		// with posts
		prepareConversation(conversation);
		checkPosts(userMember, conversation);

		// multiple conversations with posts
		User user2 = UserHelper.createNewUser();
		User user3 = UserHelper.createNewUser();
		Conversation anotherConversation = ConversationHelper.createConversation(user2, user3);
		prepareConversation(anotherConversation, 5);

		checkPosts(userMember, conversation);
	}

	private void checkPosts(ConversationMember userMember, Conversation conversation) {
		List<ConversationPost> posts = ConversationPost.find("wall = ?", conversation.getDefaultWall()).fetch();
		for (ConversationPost post : userMember.getPosts()) {
			assertTrue(posts.contains(post));
			posts.remove(post);
		}
		assertEquals(0, posts.size());
	}

	private void prepareConversation(Conversation conversation) {
		prepareConversation(conversation, 10);
	}

	private void prepareConversation(Conversation conversation, int amount) {
		for (int index = 0; index < amount; index++) {
			ConversationPostHelper.addConversationPost(conversation, null);
		}
	}

	@Test
	public void checkThumbUrlTwoActiveMembers() {
		User thirdUser = UserHelper.getAnotherUser(user, anotherUser);
		ConversationMember member = ConversationHelper.addConversationMember(conversation, thirdUser);
		conversation.save();
		member.softDelete();

		assertEquals(2, conversation.getActiveMemberCount());

		assertEquals(anotherUser.getThumbURL(), userMember.getThumbURL());
	}

	@Test
	public void checkThumbUrlTwoMembers() {
		conversation.getMemberObject(anotherUser).softDelete();

		assertEquals(1, conversation.getActiveMemberCount());
		assertEquals(2, conversation.members.size());

		assertEquals(anotherUser.getThumbURL(), userMember.getThumbURL());
	}

	@Test
	public void checkThumbUrlMoreMembers() {
		User thirdUser = UserHelper.getAnotherUser(user, anotherUser);
		ConversationHelper.addConversationMember(conversation, thirdUser);
		conversation.save();

		assertTrue(conversation.getActiveMemberCount() != 2);
		assertTrue(conversation.members.size() != 2);

		String thumbUrl = userMember.getThumbURL();
		assertTrue(thumbUrl.startsWith("/public/"));
		assertTrue(thumbUrl.contains("images/group-thumb.png"));
	}

	@Test
	public void checkLastAccess() {
		Date lastAccess = DateUtils.addDays(new Date(), -3);
		userMember.setLastAccess(lastAccess);
		assertTrue(userMember.lastAccessChanged);
		assertEquals(lastAccess, userMember.lastAccess);
	}

	@Test
	public void checkSoftDelete() {
		userMember.softDelete();
		assertTrue(userMember.isDeleted());
	}

	@Test
	public void checkConversationMemberJsonSerializer() {
		JsonObject json = new ConversationMemberJsonSerializer().serialize(userMember, null, null).getAsJsonObject();
		assertTrue(json.has("id"));
		assertTrue(json.has("status"));
		assertTrue(json.has("modified"));
		assertTrue(json.has("unreadCount"));
		assertTrue(json.has("label"));
		assertTrue(json.has("avatar"));
		assertEquals(6, json.entrySet().size());
	}
}

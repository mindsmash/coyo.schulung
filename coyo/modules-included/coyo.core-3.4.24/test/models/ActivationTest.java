package models;

import javax.persistence.PersistenceException;

import org.junit.Test;

import test.TestCoyoTenant;
import test.UnitTestBase;

public class ActivationTest extends UnitTestBase {

	private static final String MAIL_ADDRESS = "mail@mail.de";

	@Test
	public void duplicateEmails() {
		Activation a1 = new Activation(MAIL_ADDRESS);
		Activation a2 = new Activation(MAIL_ADDRESS);

		assertTrue(a1.validateAndSave());
		assertFalse(a2.validateAndSave());
	}

	@Test
	public void toStringContainsMailAndToken() {
		Activation a1 = new Activation(MAIL_ADDRESS);
		String a1String = a1.toString();

		assertTrue(a1String.contains(MAIL_ADDRESS));
		assertTrue(a1String.contains(a1.token));
	}
}

package models.page;

import helper.GroupHelper;
import helper.RandomHelper;
import helper.UserHelper;

import java.util.List;

import models.User;
import models.UserGroup;
import models.app.ContentApp;
import models.page.Page.PageVisibility;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class PageTest extends UnitTestBase {

	private Page page;

	@Before
	public void before() {
		System.out.println("#######BEFORE!!!!!!!");
		User user = UserHelper.createNewUser();
		PageCategory category = new PageCategory();
		category.name = RandomHelper.getRandomString(25);
		category.save();
		page = new Page();
		page.visibility = PageVisibility.PUBLIC;
		page.name = RandomHelper.getRandomString(25);
		page.category = category;
		page.creator = user;
		page.save();
	}

	@Test
	public void checkDefaultApps() {
		assertEquals(0, ContentApp.count("sender = ?", page));

		page.initDefaultApps();

		List<ContentApp> apps = ContentApp.find("sender = ?", page).fetch();
		assertEquals(1, apps.size());

		ContentApp app = apps.get(0);
		assertTrue(page.apps.contains(app));
	}

	@Test
	public void checkForceAllFollow() {
		assertNotSame(page.getFollowerCount(), User.count("active = true"));
		page.forcedFollowers.clear();
		page.forcedFollowerGroups.clear();
		page.forceAllFollow = true;
		page.save();
		assertSame(page.getFollowerCount(), User.count("active = true"));
	}

	@Test
	public void checkForceUserFollows() {
		page.forcedFollowers.clear();
		page.forcedFollowerGroups.clear();
		page.forceAllFollow = false;
		page.save();

		User user = UserHelper.createNewUser();
		assertFalse(page.isFollower(user));

		page.forcedFollowers.add(user);
		page.save();
		assertTrue(page.isFollower(user));
		assertTrue(page.isForcedFollower(user));
	}

	@Test
	public void checkForceGroupFollowers() {
		User user = UserHelper.createNewUser();
		UserGroup group = GroupHelper.createNewGroup();
		group.users.add(user);
		group.save();

		page.forcedFollowers.clear();
		page.forcedFollowerGroups.clear();
		page.forceAllFollow = false;
		page.save();
		assertFalse(page.isFollower(user));

		page.forcedFollowerGroups.add(group);
		page.save();
		assertTrue(page.isFollower(user));
		assertTrue(page.isForcedFollower(user));
	}
}

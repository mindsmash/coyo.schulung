package models.page;

import helper.PageHelper;
import helper.UserHelper;
import helper.page.CategoryHelper;

import java.util.ArrayList;
import java.util.List;

import models.User;
import models.page.Page.PageVisibility;

import org.junit.Test;

import play.cache.Cache;
import test.UnitTestBase;

import com.google.common.base.Joiner;

import controllers.SecureBaseController;

public class PageCategoryTest extends UnitTestBase {
	@Test
	public void checkStoreOrder() {
		List<Long> categories = new ArrayList<Long>();
		for (int index = 0; index < 3; index++) {
			categories.add(CategoryHelper.createCategory(10 - index).id);
		}

		String order = Joiner.on(",").join(categories);
		PageCategory.storeOrder(order);

		PageCategory last = PageCategory.findById(categories.get(0));
		for (int index = 1; index < categories.size(); index++) {
			PageCategory current = PageCategory.findById(categories.get(index));
			assertEquals(last.priority + 1, current.priority);
			last = current;
		}
	}

	@Test
	public void checkPages() {
		User user = UserHelper.createNewUser();
		PageCategory category = CategoryHelper.createCategory();

		List<Page> pages = new ArrayList<Page>();
		for (int index = 0; index < 3; index++) {
			pages.add(PageHelper.createOwnPage(user));
			pages.add(PageHelper.createOwnPage(UserHelper.createNewUser()));
		}
		for (Page page : pages) {
			page.category = category;
			page.visibility = PageVisibility.PRIVATE;
			page.save();
		}
		category.pages.addAll(pages);
		category.save();

		checkPages(user, category, 3);
		checkPages(UserHelper.createNewUser(), category, 0);
	}

	private void checkPages(User user, PageCategory category, int expected) {
		String cacheKey = category.getCacheKey(user);
		Cache.delete(cacheKey);

		setActiveUser(user);
		assertEquals(expected, category.getPages(user).size());

		assertNotNull(Cache.get(cacheKey));
	}

	@Test
	public void checkFindAllSorted() {
		List<Long> ids = new ArrayList<Long>();
		for (int index = 0; index < 5; index++) {
			ids.add(CategoryHelper.createCategory(index).id);
		}

		List<PageCategory> sorted = PageCategory.findAllSorted();
		PageCategory last = sorted.get(0);
		for (int index = 1; index < sorted.size(); index++) {
			PageCategory current = sorted.get(index);
			assertTrue(last.priority <= current.priority);
			last = current;
		}
	}
}

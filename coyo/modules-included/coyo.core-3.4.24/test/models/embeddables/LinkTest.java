package models.embeddables;

import helper.CommentHelper;
import helper.UserHelper;
import helper.embeddables.LinkHelper;
import models.comments.Comment;
import models.User;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class LinkTest extends UnitTestBase {
	private Link link;

	@Before
	public void before() {
		link = LinkHelper.createLink();
	}

	@Test
	public void checkUrl() {
		Link check = Link.findById(link.id);
		assertEquals(link.url, check.url);
	}

	@Test
	public void checkAddComment() {
		User user = UserHelper.createNewUser();
		Comment comment = CommentHelper.createComment(user);

		link.addComment(comment);
		link.save();

		Link saved = Link.findById(link.id);
		assertTrue(saved.getComments().contains(comment));
	}

	@Test
	public void checkRemoveComment() {
		User user = UserHelper.createNewUser();
		Comment comment = CommentHelper.createComment(user);

		link.addComment(comment);
		link.save();

		Link saved = Link.findById(link.id);
		saved.removeComment(comment);

		saved = Link.findById(link.id);
		assertFalse(saved.getComments().contains(comment));
	}

	@Test
	public void checkModel() {
		assertEquals(link, link.getModel());
	}
}

package models.wall;

import helper.PostHelper;
import helper.UserHelper;
import models.User;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class WallTest extends UnitTestBase {
	private User user;
	private Wall wall;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		wall = new Wall(user).save();
	}

	@Test
	public void checkInit() {
		assertEquals(user, wall.sender);

		wall = new Wall(user, true);
		assertEquals(user, wall.sender);
		assertTrue(wall.noActivityStream);

		wall = new Wall(user, false);
		assertEquals(user, wall.sender);
		assertFalse(wall.noActivityStream);
	}

	@Test
	public void checkPosts() {
		User user1 = UserHelper.getAnotherUser(user);
		PostHelper.simplePost(user1, wall, true);
		User user2 = UserHelper.getAnotherUser(user, user1);
		PostHelper.simplePost(user2, wall, true);
		User user3 = UserHelper.getAnotherUser(user, user1, user2);
		PostHelper.simplePost(user3, wall, true);

		wall.refresh();
		assertEquals(3, wall.getPostsQuery(user1).fetch().size());
	}
}

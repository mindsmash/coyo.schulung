package models.wall.post;

import helper.PostHelper;
import helper.UserHelper;
import models.User;

import org.junit.Test;

import test.UnitTestBase;

public class LikeLinkPostTest extends UnitTestBase {

	@Test
	public void checkUnlike() {
		User user1 = UserHelper.getRandomUser();
		User user2 = UserHelper.getAnotherUser(user1);
		LikeLinkPost post = PostHelper.likeLinkPost(user1, false);

		post.like(user1);
		assertEquals(1, post.getLikes().size());

		post.like(user2);
		assertEquals(2, post.getLikes().size());

		post.unlike(user2);
		assertEquals(1, post.getLikes().size());
		assertEquals(1L, LikeLinkPost.count());
	}
}

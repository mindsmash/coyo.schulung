package models.wall.attachments;

import helper.BlobHelper;
import helper.PostHelper;
import helper.RandomHelper;
import helper.UserHelper;
import models.User;
import models.wall.post.Post;

import org.junit.Before;
import org.junit.Test;

import play.exceptions.NoRouteFoundException;
import storage.FlexibleBlob;
import test.UnitTestBase;

public class FileAttachmentTest extends UnitTestBase {
	private FilePostAttachment attachment;
	private FlexibleBlob file;
	private String name;
	private User user;

	@Before
	public void before() {
		file = BlobHelper.getDummyBlob();
		name = RandomHelper.getRandomString(20);
		user = UserHelper.createNewUser();
		Post post = PostHelper.simplePost(user, user.getDefaultWall(), true);
		attachment = new FilePostAttachment();
		attachment.name = name;
		attachment.file = file;
		attachment.post = post;
		attachment.save();
		post.attachments.add(attachment);
		post.save();
	}

	@Test
	public void checkAfterDelete() {
		attachment.delete();
		assertFalse(attachment.file.exists());
	}

	@Test
	public void checkFileName() {
		assertEquals(name, attachment.getFileName());
	}

	@Test
	public void checkFileData() {
		assertNotNull(attachment.getFileData());
	}

	@Test
	public void checkContentType() {
		assertNotNull(attachment.getContentType());
	}

	@Test
	public void checkDisplayName() {
		assertEquals(name, attachment.getDisplayName());
	}

	@Test
	public void checkDownloadUrl() {
		try {
			attachment.getDownloadUrl();
		} catch (NoRouteFoundException e) {
			fail();
		}
	}

	@Test
	public void checkUniqueId() {
		assertTrue(attachment.getUniqueId().contains(String.valueOf(attachment.id)));
	}
}

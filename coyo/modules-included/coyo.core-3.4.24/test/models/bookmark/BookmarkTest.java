package models.bookmark;

import helper.bookmark.BookmarkHelper;

import java.util.List;

import org.junit.Test;

import test.UnitTestBase;

import com.google.common.base.Joiner;

public class BookmarkTest extends UnitTestBase {

	@Test
	public void checkCreate() {
		Bookmark bookmark = BookmarkHelper.createRandom();

		Bookmark check = Bookmark.findById(bookmark.id);
		assertEquals(bookmark.label, check.label);
		assertEquals(bookmark.url, check.url);
	}

	@Test
	public void checkStoreOrder() {
		Bookmark.deleteAll();

		Bookmark bookmark1 = BookmarkHelper.createRandom(100);
		Bookmark bookmark2 = BookmarkHelper.createRandom(200);
		Bookmark bookmark3 = BookmarkHelper.createRandom(300);

		checkOrder(bookmark1.id, bookmark2.id, bookmark3.id);
	}

	private void checkOrder(Long... bookmarks) {
		String order = Joiner.on(",").join(bookmarks);

		Bookmark.storeOrder(order);

		List<Bookmark> allBookmarks = Bookmark.findAll();
		assertEquals(bookmarks.length, allBookmarks.size());

		Bookmark last = Bookmark.findById(bookmarks[0]);
		for (int index = 1; index < bookmarks.length; index++) {
			Bookmark current = Bookmark.findById(bookmarks[index]);
			assertEquals(last.priority + 1, current.priority);
			last = current;
		}
	}
}

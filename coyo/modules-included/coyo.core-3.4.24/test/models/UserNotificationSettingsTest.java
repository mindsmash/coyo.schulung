package models;

import java.util.List;

import models.UserNotificationSettings.MailInterval;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class UserNotificationSettingsTest extends UnitTestBase {

	private UserNotificationSettings settings;

	@Before
	public void before() {
		settings = new UserNotificationSettings();
	}

	@Test
	public void checkMailInterval() {
		assertEquals(MailInterval.DAILY, settings.getMailInterval());

		MailInterval[] values = MailInterval.values();
		for (MailInterval mailInterval : values) {
			settings.mailInterval = mailInterval;
			assertEquals(mailInterval, settings.getMailInterval());
		}

		settings.mailInterval = null;
		assertEquals(MailInterval.DAILY, settings.getMailInterval());
	}

	@Test
	public void checkIsWanted() {
		List<String> mailEvents = settings.mailEvents;

		settings.receiveMails = false;
		for (String event : mailEvents) {
			assertFalse(settings.isWanted(event));
		}

		settings.receiveMails = true;
		for (String event : mailEvents) {
			assertTrue(settings.isWanted(event));
		}
	}
}

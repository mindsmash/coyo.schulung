package models;

import org.junit.Test;

import test.UnitTestBase;

public class StaticUserGroupTest extends UnitTestBase {
	@Test
	public void isDeletable() {
		assertTrue(new StaticUserGroup().isDeletable());
	}
}

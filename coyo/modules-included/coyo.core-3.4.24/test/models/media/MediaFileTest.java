package models.media;

import helper.UserHelper;
import helper.media.MediaFileHelper;
import models.User;
import models.medialibrary.MediaFile;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class MediaFileTest extends UnitTestBase {
	private MediaFile mediaFile;
	private User user;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		mediaFile = MediaFileHelper.createMediaFile(user);
	}

	@Test
	public void checkStore() throws Exception {
		MediaFile check = MediaFile.findById(mediaFile.id);

		assertEquals(mediaFile, check);
		assertEquals(mediaFile.name, check.name);
		assertEquals(mediaFile.data, check.data);
		assertEquals(mediaFile.library, check.library);

		assertEquals(user.getMediaLibrary(), check.library);
	}

	@Test
	public void checkAfterDelete() throws Exception {
		mediaFile.delete();
		assertFalse(mediaFile.data.exists());
	}
}

package models.media;

import helper.UserHelper;
import models.User;
import models.medialibrary.MediaLibrary;

import org.junit.Test;

import test.UnitTestBase;

public class MediaLibraryTest extends UnitTestBase {
	@Test
	public void checkSenderId() throws Exception {
		User user = UserHelper.createNewUser();
		assertEquals(user.id, user.getMediaLibrary().getSenderId());

		MediaLibrary mediaLibrary = new MediaLibrary();
		mediaLibrary.save();
		assertNull(mediaLibrary.getSenderId());
	}
}

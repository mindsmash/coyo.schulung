package models.container;

import helper.UserHelper;
import models.Application;
import models.User;
import models.container.fields.TextField;
import models.container.values.StringItemValue;

import org.junit.After;
import org.junit.Test;
import org.testng.Assert;

import play.db.jpa.JPA;
import test.UnitTestBase;

public class ContainerTest extends UnitTestBase {
	@After
	public void cleanUp() {
		// delete all data manually
		JPA.em().createNativeQuery("delete from container_field_prop").executeUpdate();
		ItemValue.deleteAll();
		Item.deleteAll();
		Field.deleteAll();
		Container.deleteAll();
	}

	@Test
	public void testCreate() throws Exception {
		Container container = new Container(Application.get(), "Test");
		TextField textField = new TextField(container);
		textField.label = "Test text field";
		container.addField(textField);

		User user = UserHelper.createNewUser();
		Item item = new Item(container, user);
		item.addValue(new StringItemValue(textField, "Test value"));
		container.addItem(item);

		container.save();

		Container check = Container.findById(container.id);
		Assert.assertEquals(check.items.size(), 1);
	}
}

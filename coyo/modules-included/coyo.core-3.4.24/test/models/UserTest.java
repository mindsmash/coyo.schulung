package models;

import helper.PageHelper;
import helper.PostHelper;
import helper.RandomHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import json.UserChooserJsonSerializer;
import models.notification.Notification;
import models.page.Page;
import models.wall.post.ConversationPost;
import models.wall.post.Post;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

import com.google.gson.JsonObject;

public class UserTest extends UnitTestBase {

	private User user;

	@Before
	public void before() {
		user = UserHelper.getRandomUser();
	}

	@Test
	public void checkPassword() {
		Date now = new Date();
		String password = RandomHelper.getRandomString(16);
		user.setPassword(password);
		assertFalse(now.after(user.passwordModified));
		assertFalse(user.password.equals(password));
	}

	@Test
	public void checkFirstNameAndSlug() {
		user.setFirstName(randomString());
		assertNull(user.getSlug());
		user.save();
		assertNotNull(user.getSlug());
	}

	@Test
	public void checkLastNameAndSlug() {
		user.setLastName(randomString());
		assertNull(user.getSlug());
		user.save();
		assertNotNull(user.getSlug());
	}

	private String randomString() {
		return new BigInteger(130, new SecureRandom()).toString(32);
	}

	@Test
	public void checkFullname() {
		assertTrue(user.getFullName().contains(user.firstName));
		assertTrue(user.getFullName().contains(user.lastName));
	}

	@Test
	public void checkFullnameWithTitle() {
		if (user.title == null) {
			user.title = RandomHelper.getRandomString(20);
		}
		assertTrue(user.getFullNameWithTitle().contains(user.title));
	}

	@Test
	public void checkRealFollowers() {
		List<User> users = User.findAll();
		for (User user : users) {
			List<User> followers = user.getRealFollowers();
			assertFalse(followers.contains(user));
			for (User follower : followers) {
				assertTrue(follower.isActive());
				assertFalse(follower.external);
			}
		}
	}

	@Test
	public void checkRealFollowing() {
		List<User> users = User.findAll();
		for (User user : users) {
			List<User> followings = user.getRealFollowing();
			assertFalse(followings.contains(user));
			for (User following : followings) {
				assertTrue(following.isActive());
				assertFalse(following.external);
			}
		}
	}

	@Test
	public void checkAuthenticate() {
		int length = 20;

		String password = RandomHelper.getRandomString(length - 1);
		user.setPassword(password);
		assertTrue(user.authenticate(password));

		for (int index = 0; index < 10; index++) {
			password = RandomHelper.getRandomString(length);
			assertFalse(user.authenticate(password));
		}
	}

	@Test
	public void hasGrant() {
		List<User> users = User.findAll();
		for (User user : users) {
			if (user.role != null && user.role.grants.size() > 0) {
				String grant = RandomHelper.getRandom(user.role.grants);
				assertTrue(user.hasGrant(grant));

				grant = RandomHelper.getRandomString(10);
				assertEquals(user.role.hasGrant(grant), user.hasGrant(grant));
			}

			for (UserGroup group : user.groups) {
				if (group.role != null && group.role.grants.size() > 0) {
					String grant = RandomHelper.getRandom(group.role.grants);
					assertTrue(user.hasGrant(grant));

					grant = RandomHelper.getRandomString(10);
					assertEquals(group.hasGrant(grant), user.hasGrant(grant));
				}
			}
		}
	}

	@Test
	public void checkReceiveAndGetNotification() {
		Notification notification = NotificationHelper.addNewPostNotification(user, null);
		assertTrue(contains(user.getNotifications(false, 0), notification));
	}

	@Test
	public void checkEmailNotifications() {
		Notification notification = NotificationHelper.addNewPostNotification(user, Notification.DisplayType.EMAIL);
		Date beforeSave = DateUtils.addMinutes(new Date(), -1);
		assertTrue(contains(user.getEmailNotifications(beforeSave), notification));
	}

	@Test
	public void checkUnreadNotificationsCount() {
		long before = user.getUnreadNotificationsCount();
		NotificationHelper.addNewPostNotification(user, null);
		assertEquals(before + 1, user.getUnreadNotificationsCount());
		user.resetUnreadNotificationCount();
		assertEquals(0, user.getUnreadConversationCount());
	}

	@Test
	public void checkUnreadNotifications() {
		Notification notification = NotificationHelper.addNewPostNotification(user, null);
		assertTrue(contains(user.getUnreadNotifications(), notification));
	}

	private boolean contains(List<UserNotification> notifications, Notification notification) {
		for (UserNotification userNotification : notifications) {
			if (userNotification.notification.equals(notification)) {
				return true;
			}
		}
		return false;
	}

	@Test
	public void checkLastActivityStreamUpdate() {
		Date update = user.getLastActivityStreamUpdate();
		assertNotNull(update);

		setActiveUser(user);

		user.getNewActivityStream(true);
		assertFalse(user.getLastActivityStreamUpdate().before(update));
	}

	@Test
	public void checkPages() {
		PageHelper.ensurePages(user, 2);

		int amount = user.getPages(null).size() / 2;
		assertEquals(amount, user.getPages(amount).size());

		List<Page> pages = user.getPages(null);
		assertEquals(pages.size(), new HashSet<Page>(pages).size());

	}

	@Test
	public void checkAllPosts() throws InstantiationException, IllegalAccessException {
		/* check whether post count is 0 initially */
		assertTrue(user.getPostCount() == 0);

		/* create two different kinds of posts - conversation posts are not counted */
		PostHelper.likeSimplePost(user, false);
		PostHelper.genericPost(user, ConversationPost.class);
		assertTrue(user.getPostCount() == 1l);
	}

	@Test
	public void checkWallPosts() throws InstantiationException, IllegalAccessException {
		/* create two different kinds of posts */
		Post post = PostHelper.likeSimplePost(user, false);
		Post conversationPost = PostHelper.genericPost(user, ConversationPost.class);

		/* get posts and check result - conversation posts are not allowed to show up */
		List<Post> wallPosts = user.getWallPostsQuery().fetch();
		assertFalse("Result is empty.", wallPosts.isEmpty());
		assertFalse("Post is not filtered from result correctly.", wallPosts.contains(conversationPost));
		assertTrue("Result does not contain unfiltered posts.", wallPosts.contains(post));
	}

	@Test
	public void checkUpcomingEvents() {
		assertNotNull(user.getUpcomingEvents());
	}

	@Test
	public void checkWalls() {

	}

	@Test
	public void checkOnlineUsers() {

	}

	@Test
	public void checkOnlineUserCount() {

	}

	@Test
	public void checkIsOnline() {

	}

	@Test
	public void checkBirthday() {

	}

	@Test
	public void checkUpcomingBirthday() {

	}

	@Test
	public void checkDisplayName() {
		assertNotNull(user.getDisplayName());
	}

	@Test
	public void checkDelete() {
		ArrayList<UserGroup> groups = new ArrayList<UserGroup>(user.groups);

		user.delete();
		user.refresh();

		assertTrue(user.groups.size() == 0);
		assertTrue(user.deleted);
		assertFalse(user.isActive());

		user.deleted = false;
		user.active = true;
		for (UserGroup userGroup : groups) {
			userGroup.addUser(user);
		}
		user.save();
	}

	@Test
	public void checkDefaultWall() {
		assertNotNull(user.getDefaultWall());
	}

	@Test
	public void checkActiveWorkspacesQuery() {

	}

	@Test
	public void checkActiveWorkspaces() {

	}

	@Test
	public void checkFieldOptions() {

	}

	@Test
	public void checkFieldOptionsJson() {

	}

	@Test
	public void checkUserChooserSelect() {

	}

	@Test
	public void checkAdmins() {
		assertTrue(user.getAdmins().contains(user));
	}

	@Test
	public void checkSenderType() {
		assertEquals("user", user.getSenderType());
	}

	@Test
	public void checkSharable() {
		assertTrue(user.isSharable());
	}

	@Test
	public void checkPrivateCacheKey() {

	}

	@Test
	public void checkUnreadConversationCount() {

	}

	@Test
	public void checkUnreadConversations() {

	}

	@Test
	public void checkActiveConversations() {

	}

	@Test
	public void checkConversations() {

	}

	@Test
	public void checkUpdatedConversations() {

	}

	@Test
	public void checkConversationCount() {

	}

	@Test
	public void checkUserChooserJsonSerializer() {
		JsonObject json = new UserChooserJsonSerializer().serialize(user, null, null).getAsJsonObject();
		assertTrue(json.has("id"));
		assertTrue(json.has("thumbURL"));
		assertTrue(json.has("fullName"));
		assertTrue(json.has("department"));
		assertTrue(json.has("office"));
		assertTrue(json.has("jobTitle"));
		assertTrue(json.has("company"));
	}

	@Test
	public void checkActivityStreamSelect() {

	}
}

package models;

import helper.CommentHelper;
import helper.PostHelper;
import helper.UserHelper;
import models.comments.Comment;
import models.wall.post.Post;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class CommentTest extends UnitTestBase {

	private static final String COMMENT_TEXT = "My test <h1>comment</h1>";
	private Comment comment;

	@Before
	public void before() {
		User user = UserHelper.getRandomUser();

		comment = CommentHelper.createComment(user, COMMENT_TEXT);
	}

	@Test
	public void addComment() {
		assertSame(Comment.findById(comment.id), comment);
	}

	@Test
	public void deleteComment() {
		comment.delete();
		assertNull(Comment.findById(comment.id));
	}

	@Test
	public void checkLinkifiedText() {
		String text = comment.getLinkifiedText();
		boolean containsHTML = text.matches(".*\\<[^>]+>.*");
		assertFalse(containsHTML);
	}

	@Test
	public void checkLike() {
		User user = UserHelper.getAnotherUser(comment.author);
		assertTrue(comment.getLikes().size() == 0);

		comment.like(user);
		assertTrue(comment.getLikes().contains(user));
	}

	@Test
	public void checkUnLike() {
		User user = UserHelper.getAnotherUser(comment.author);

		comment.like(user);
		assertTrue(comment.getLikes().contains(user));

		comment.unlike(user);
		assertFalse(comment.getLikes().contains(user));
	}
}
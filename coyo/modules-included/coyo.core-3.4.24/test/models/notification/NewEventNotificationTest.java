package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.EventHelper;
import helper.RandomHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.app.CalendarApp;
import models.event.EventBase.EventVisibility;
import models.event.Event;
import models.event.EventUser.EventResponse;
import models.wall.Wall;
import models.wall.post.NotificationPost;

import org.junit.Test;

import test.UnitTestBase;

public class NewEventNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		EventNotification notification = NotificationHelper.addNewEventNotification(user, Notification.DisplayType.WEB);

		User anotherUser = UserHelper.getAnotherUser(user);
		UserNotification un = UserNotification.find("user = ? AND notification = ? ORDER BY created DESC", user, notification).first();
		String label = notification.getText(un);

		assertFalse(label.startsWith("notification.event.new"));

		assertTrue(label.contains(notification.event.creator.getDisplayName()));
		assertTrue(label.contains(notification.event.name));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Event event = EventHelper.createEvent(user);

		User user1 = UserHelper.createNewUser();
		User user2 = UserHelper.createNewUser();
		User user3 = UserHelper.createNewUser();
		User user4 = UserHelper.createNewUser();

		EventHelper.inviteUserToEvent(user1, event, EventResponse.ATTENDING);
		EventHelper.inviteUserToEvent(user2, event, EventResponse.MAYBE);
		EventHelper.inviteUserToEvent(user3, event, EventResponse.NOT_ATTENDING);
		EventHelper.inviteUserToEvent(user4, event, EventResponse.WAITING);
		event.admins.add(user);

		assertTrue(Application.POSTS_ENABLED);

		CalendarApp calendarApp = new CalendarApp();
		calendarApp.displayName = RandomHelper.getRandomString(25);
		calendarApp.sender = user;
		calendarApp.events.add(event);
		calendarApp.save();

		assertNull(checkNotification(event, calendarApp, null, user.getDefaultWall()));

		assertNotNull(checkNotification(event, null, EventVisibility.PUBLIC, Application.get().getDefaultWall()));
		assertNull(checkNotification(event, null, EventVisibility.PRIVATE, Application.get().getDefaultWall()));
		assertNull(checkNotification(event, null, EventVisibility.CLOSED, Application.get().getDefaultWall()));
	}

	private NotificationPost checkNotification(Event event, CalendarApp calendarApp, EventVisibility visibility,
			Wall wall) {
		event.calendar = calendarApp;
		event.visibility = visibility;

		String query = "author = ? AND wall = ?";
		NotificationPost.delete(query, event, wall);


		NewEventNotification.raise(event);

		return NotificationPost.find(query, event, wall).first();
	}
}

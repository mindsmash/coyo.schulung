package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.CommentHelper;
import helper.PostHelper;
import helper.UserHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Application;
import models.comments.Comment;
import models.Sender;
import models.User;
import models.UserNotification;
import models.wall.post.Post;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class PostCommentNotificationTest extends UnitTestBase {

	private User user;
	private User anotherUser;
	private Post post;
	private Comment comment;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		anotherUser = UserHelper.createNewUser();
		post = PostHelper.simplePost(user, false);
		comment = CommentHelper.createComment(anotherUser);
		post.comments.add(comment);
		post.save();
	}

	@Test
	public void checkRaise() {
		List<PostCommentNotification> notifications = getNotifications(post, comment);
		for (PostCommentNotification notification : notifications) {
			assertEquals(post, notification.post);
			assertEquals(comment, notification.postComment);
		}
	}

	@Test
	public void checkLabel() {
		List<PostCommentNotification> notifications = getNotifications(post, comment);
		for (PostCommentNotification notification : notifications) {
			checkInnerLabel(notification, user);
			checkInnerLabel(notification, anotherUser);
			checkInnerLabel(notification, UserHelper.createNewUser());
			post.author = comment.author;
			checkInnerLabel(notification, user);
			checkInnerLabel(notification, anotherUser);

		}
	}

	private void checkInnerLabel(PostCommentNotification notification, User user) {
		String label = notification.getText(user);

		assertFalse(label.startsWith("notifications.post.comment"));

		Post post = notification.post;
		Set<User> commentAuthors = new HashSet<User>();
		for (Comment comment : post.getComments()) {
			commentAuthors.add(comment.author);
		}
		int commentAuthorAmount = commentAuthors.size();

		String postAuthor = post.author.getDisplayName();
		String commentAuthor = comment.author.getDisplayName();

		if (user == post.author && user == comment.author) {
			assertFalse(label.contains(commentAuthor));
			assertFalse(label.contains(postAuthor));
		} else if (user == post.author) {
			assertTrue(label.contains(commentAuthor));
			assertFalse(label.contains(postAuthor));
		} else if (user == comment.author) {
			assertTrue(label.contains(postAuthor));
		} else if (comment.author == post.author) {
			assertTrue(label.contains(commentAuthor));
			assertEquals(commentAuthorAmount >= 2, 2 == StringUtils.countMatches(label, postAuthor));
		} else {
			assertTrue(label.contains(commentAuthor));
			assertTrue(label.contains(postAuthor));
		}
		if (commentAuthorAmount > 2) {
			assertTrue(label.contains(String.valueOf(commentAuthorAmount - 1)));
		}
	}

	@Test
	public void checkThumbnailSender() {
		List<PostCommentNotification> notifications = getNotifications(post, comment);
		for (PostCommentNotification notification : notifications) {
			assertEquals(anotherUser, notification.getThumbnailSender());
		}
	}

	private List<PostCommentNotification> getNotifications(Post post, Comment comment) {
		assertTrue(Application.POSTS_ENABLED);
		int followerCount = 3;
		
		for (int index = 0; index < followerCount; index++) {
			post.follow(UserHelper.createNewUser());
		}
		post.follow(comment.author);
		post.save();

		UserNotification.deleteAll();

		PostCommentNotification.raise(post, comment);
		return PostCommentNotification.find("post = ?", post).fetch();
	}
}

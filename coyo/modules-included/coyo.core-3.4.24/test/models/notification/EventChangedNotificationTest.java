package models.notification;

import helper.EventHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.User;
import models.UserNotification;
import models.event.Event;
import models.event.EventUser.EventResponse;

import java.util.Collection;

import org.junit.Test;
import test.UnitTestBase;

public class EventChangedNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		EventChangedNotification notification = NotificationHelper.addEventChangedNotification(user,
				Notification.DisplayType.WEB);

		User anotherUser = UserHelper.getAnotherUser(user);
		String label = notification.getText(anotherUser);

		notification.event.fulltime = true;
		assertFalse(label.startsWith("notification.event.reminder"));
		notification.event.fulltime = false;
		assertFalse(label.startsWith("notification.event.reminder"));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Event event = EventHelper.createEvent(user);

		User user1 = UserHelper.createNewUser();
		User user2 = UserHelper.createNewUser();
		User user3 = UserHelper.createNewUser();
		User user4 = UserHelper.createNewUser();

		EventHelper.inviteUserToEvent(user1, event, EventResponse.ATTENDING);
		EventHelper.inviteUserToEvent(user2, event, EventResponse.MAYBE);
		EventHelper.inviteUserToEvent(user3, event, EventResponse.NOT_ATTENDING);
		EventHelper.inviteUserToEvent(user4, event, EventResponse.WAITING);
		event.admins.add(user);

		Collection<Long> receivers = EventChangedNotification.raise(event);

		// admin
		assertTrue(receivers.contains(user.id));
		// attending
		assertTrue(receivers.contains(user1.id));
		// maybe
		assertTrue(receivers.contains(user2.id));
		// not attending
		assertFalse(receivers.contains(user3.id));
		// waiting
		assertFalse(receivers.contains(user4.id));
	}
}

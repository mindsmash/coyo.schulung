package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.EventHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.User;
import models.UserNotification;
import models.event.Event;
import models.event.EventUser.EventResponse;

import org.junit.Test;

import test.UnitTestBase;

import java.util.Collection;
import java.util.List;

public class EventResponseNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		EventResponseNotification notification = NotificationHelper.addEventResponseNotification(user,
				Notification.DisplayType.WEB);
		notification.user = user;

		User anotherUser = UserHelper.getAnotherUser(user);
		UserNotification un = UserNotification
				.find("user = ? AND notification = ? ORDER BY created DESC", anotherUser, notification).first();
		String label = notification.getText(un);

		assertFalse(label.startsWith("notification.event.response"));

		assertTrue(label.contains(notification.event.name));

		assertTrue(label.contains(EventResponse.WAITING.getLabel()));

		checkResponse(notification, EventResponse.ATTENDING, user);
		checkResponse(notification, EventResponse.MAYBE, user);
		checkResponse(notification, EventResponse.NOT_ATTENDING, user);
		checkResponse(notification, EventResponse.WAITING, user);
	}

	private void checkResponse(EventResponseNotification notification, EventResponse response, User anotherUser) {
		notification.eventResponse = response;
		UserNotification un = UserNotification
				.find("user = ? AND notification = ? ORDER BY created DESC", anotherUser, notification).first();
		assertTrue(notification.getText(un).contains(response.getLabel()));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Event event = EventHelper.createEvent(user);

		User user1 = UserHelper.createNewUser();
		User user2 = UserHelper.createNewUser();
		User user3 = UserHelper.createNewUser();
		User user4 = UserHelper.createNewUser();

		EventHelper.inviteUserToEvent(user1, event, EventResponse.ATTENDING);
		EventHelper.inviteUserToEvent(user2, event, EventResponse.MAYBE);
		EventHelper.inviteUserToEvent(user3, event, EventResponse.NOT_ATTENDING);
		EventHelper.inviteUserToEvent(user4, event, EventResponse.WAITING);
		event.admins.add(user);

		Collection<Long> receivers = EventResponseNotification.raise(event, user1, EventResponse.ATTENDING);

		// admin
		assertTrue(receivers.contains(user.id));
		// attending
		assertFalse(receivers.contains(user1.id));
		// maybe
		assertFalse(receivers.contains(user2.id));
		// not attending
		assertFalse(receivers.contains(user3.id));
		// waiting
		assertFalse(receivers.contains(user4.id));
	}
}

package models.notification;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import helper.PageHelper;
import helper.UserHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.page.Page;
import test.UnitTestBase;

public class PageFollowNotificationTest extends UnitTestBase {

	private User anotherUser;
	private Page page;
	private User user;

	@Before
	public void before() {
		anotherUser = UserHelper.createNewUser();
		page = PageHelper.createPage(anotherUser);
		user = UserHelper.createNewUser();
	}

	@Test
	public void check() {
		PageFollowNotification notification = getNotification(user, page);

		// assert attributes
		assertEquals(user, notification.user);
		assertEquals(page, notification.page);

		// assert thumb
		assertEquals(user, notification.getThumbnailSender());

		// assert label
		assertTrue(notification.getText(anotherUser).contains(user.getFullName()));
	}

	private PageFollowNotification getNotification(User user, Page page) {
		assertTrue(Application.POSTS_ENABLED);

		page.admins.add(page.creator);
		page.followers.add(user);
		page.save();

		UserNotification.deleteAll();

		PageFollowNotification.raise(page, user);

		return PageFollowNotification.find("page = ?", page).first();
	}
}

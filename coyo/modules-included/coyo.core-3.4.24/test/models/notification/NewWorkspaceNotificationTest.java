package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.UserHelper;
import helper.WorkspaceHelper;
import helper.notification.NotificationHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.wall.Wall;
import models.wall.post.NotificationPost;
import models.workspace.Workspace;

import org.junit.Test;

import test.UnitTestBase;

public class NewWorkspaceNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();

		Workspace workspace = WorkspaceHelper.createWorkspace(user);
		NewWorkspaceNotification notification = NotificationHelper.addNewWorkspaceNotification(user, Notification.DisplayType.WEB,
				workspace);
		UserNotification un = UserNotification.find("user = ? AND notification = ? ORDER BY created DESC", user, notification).first();
		String label = notification.getText(un);
		assertFalse(label.startsWith("notification.workspace.new"));
		assertTrue(label.contains(workspace.creator.getDisplayName()));
		assertTrue(label.contains(workspace.name));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Workspace workspace = WorkspaceHelper.createWorkspace(user);

		assertTrue(Application.POSTS_ENABLED);

		Wall wall = Application.get().getDefaultWall();

		String query = "author = ? AND wall = ?";
		NotificationPost.delete(query, workspace, wall);

		NewWorkspaceNotification.raise(workspace);

		assertNotNull(NotificationPost.find(query, workspace, wall).first());
	}

	@Test
	public void checkThumbnailSender() {
		User user = UserHelper.createNewUser();
		Workspace workspace = WorkspaceHelper.createWorkspace(user);
		NewWorkspaceNotification notification = NotificationHelper.addNewWorkspaceNotification(user, Notification.DisplayType.WEB,
				workspace);

		assertEquals(workspace, notification.workspace);
	}
}

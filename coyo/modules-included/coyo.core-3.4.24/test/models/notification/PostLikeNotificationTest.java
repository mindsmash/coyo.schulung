package models.notification;

import helper.PostHelper;
import helper.UserHelper;
import models.Application;
import models.Sender;
import models.User;
import models.UserNotification;
import models.wall.post.Post;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import test.UnitTestBase;

import java.util.ArrayList;
import java.util.List;

public class PostLikeNotificationTest extends UnitTestBase {

	private User user;
	private Post post;
	private User liker;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		liker = UserHelper.createNewUser();
		post = PostHelper.simplePost(user, false);
		post.follow(UserHelper.createNewUser());
		post.save();
	}

	@Test
	public void checkRaise() {
		List<PostLikeNotification> notifications = getNotifications(post, liker);
		for (PostLikeNotification notification : notifications) {
			assertEquals(post, notification.post);
			assertEquals(liker, notification.user);
		}
	}

	@Test
	public void checkLabel() {
		PostLikeNotification notification = getNotifications(post, liker).get(0);
		for (int index = 0; index < 3; index++) {
			checkInnerLabel(notification, user);
			checkInnerLabel(notification, liker);
			checkInnerLabel(notification, UserHelper.createNewUser());
			Sender temp = post.author;
			post.author = notification.user;
			post.author = temp;

			liker = UserHelper.createNewUser();
			post.like(liker);
		}
	}

	private void checkInnerLabel(PostLikeNotification notification, User user) {
		for (UserNotification un : notification.userNotifications) {
			if (un.notification == notification) {
				String label = notification.getText(un);
				assertFalse(label.startsWith("notifications.post.like"));

				Post post = notification.post;

				int likeAmount = post.getLikes().size();

				String userName = notification.user.getDisplayName();
				String postAuthor = post.author.getDisplayName();

				if (user == post.author && user == notification.user) {
					assertFalse(label.contains(postAuthor));
				} else if (user == post.author) {
					assertTrue(label.contains(userName));
				} else if (user == notification.user) {
					assertTrue(label.contains(postAuthor));
				} else if (notification.user == post.author) {
					assertTrue(label.contains(userName));
					assertEquals(likeAmount >= 2, 2 == StringUtils.countMatches(label, postAuthor));
				} else {
					assertTrue(label.contains(userName));
					assertTrue(label.contains(postAuthor));
				}
				if (likeAmount > 2) {
					assertTrue(label.contains(String.valueOf(likeAmount - 1)));
				}
			}
		}
	}

	@Test
	public void checkThumbnailSender() {
		List<PostLikeNotification> notifications = getNotifications(post, liker);
		for (PostLikeNotification notification : notifications) {
			assertEquals(liker, notification.getThumbnailSender());
		}
	}

	private List<PostLikeNotification> getNotifications(Post post, User liker) {
		assertTrue(Application.POSTS_ENABLED);

		UserNotification.deleteAll();

		post.like(liker);
		return PostLikeNotification.find("post = ?", post).fetch();
	}
}

package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.UserHelper;

import java.util.ArrayList;
import java.util.List;

import models.Application;
import models.User;
import models.UserNotification;

import org.junit.Test;

import play.exceptions.NoRouteFoundException;
import test.UnitTestBase;

public class UserFollowNotificationTest extends UnitTestBase {

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		User follower = UserHelper.createNewUser();

		List<UserFollowNotification> notifications = getNotifications(user, follower);
		for (UserFollowNotification notification : notifications) {
			assertEquals(follower, notification.follower);
			assertEquals(user, notification.user);
		}
	}

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		User follower = UserHelper.createNewUser();

		List<UserFollowNotification> notifications = getNotifications(user, follower);
		for (UserFollowNotification notification : notifications) {
			String label = notification.getText(null);
			assertFalse(label.startsWith("notification.user.follow"));
			assertTrue(label.contains(follower.getFullName()));
		}
	}

	@Test
	public void checkImageUrl() {
		User user = UserHelper.createNewUser();
		User follower = UserHelper.createNewUser();

		try {
			List<UserFollowNotification> notifications = getNotifications(user, follower);
			for (UserFollowNotification notification : notifications) {
				String imageUrl = notification.getImageUrl();
				assertTrue(imageUrl.contains("/resource/sender/thumb"));
				assertTrue(imageUrl.contains(String.valueOf(follower.id)));
			}
		} catch (NoRouteFoundException e) {
			fail();
		}
	}

	@Test
	public void checkThumbnailSender() {
		User user = UserHelper.createNewUser();
		User follower = UserHelper.createNewUser();

		List<UserFollowNotification> notifications = getNotifications(user, follower);
		for (UserFollowNotification notification : notifications) {
			assertEquals(follower, notification.getThumbnailSender());
		}
	}

	private List<UserFollowNotification> getNotifications(User user, User follower) {
		assertTrue(Application.POSTS_ENABLED);

		UserNotification.deleteAll();

		UserFollowNotification.raise(user, follower);

		return UserFollowNotification.find("user = ? AND follower = ?", user, follower).fetch();
	}
}

package models.notification;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import helper.UserHelper;
import helper.WorkspaceHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import test.UnitTestBase;

public class WorkspaceLeaveNotificationTest extends UnitTestBase {

	private User user;
	private User anotherUser;
	private Workspace workspace;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		anotherUser = UserHelper.createNewUser();
		workspace = WorkspaceHelper.createWorkspace(user);
	}

	@Test
	public void checkRaise() {
		List<WorkspaceLeaveNotification> notifications = getNotifications(anotherUser, workspace);
		for (WorkspaceLeaveNotification notification : notifications) {
			assertEquals(workspace, notification.workspace);
			assertEquals(anotherUser, notification.user);
		}
	}

	@Test
	public void checkLabel() {
		List<WorkspaceLeaveNotification> notifications = getNotifications(anotherUser, workspace);
		for (WorkspaceLeaveNotification notification : notifications) {
			UserNotification un = UserNotification.find("notification = ?", notification).first();
			String label = notification.getText(un);
			assertFalse(label.startsWith("notification.workspace.leave"));
			assertTrue(label.contains(anotherUser.getFullName()));
			assertTrue(label.contains(workspace.name));
		}
	}

	@Test
	public void checkThumbnailSender() {
		List<WorkspaceLeaveNotification> notifications = getNotifications(anotherUser, workspace);
		for (WorkspaceLeaveNotification notification : notifications) {
			assertEquals(workspace, notification.getThumbnailSender());
		}
	}

	private List<WorkspaceLeaveNotification> getNotifications(User user, Workspace workspace) {
		assertTrue(Application.POSTS_ENABLED);

		assertEquals(1, workspace.getUserMembers().size());

		UserNotification.deleteAll();

		WorkspaceLeaveNotification.raise(workspace, user);
		return WorkspaceLeaveNotification.find("workspace = ? AND user = ?", workspace, user).fetch();
	}
}

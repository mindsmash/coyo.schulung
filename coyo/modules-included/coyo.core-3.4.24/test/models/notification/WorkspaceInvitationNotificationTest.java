package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.UserHelper;
import helper.WorkspaceHelper;

import java.util.ArrayList;
import java.util.List;

import models.Application;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;

import org.junit.Before;
import org.junit.Test;

import test.UnitTestBase;

public class WorkspaceInvitationNotificationTest extends UnitTestBase {

	private User user;
	private User anotherUser;
	private Workspace workspace;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		anotherUser = UserHelper.createNewUser();
		workspace = WorkspaceHelper.createWorkspace(user);
	}

	@Test
	public void checkRaise() {
		List<WorkspaceInvitationNotification> notifications = getNotifications(anotherUser, workspace);
		for (WorkspaceInvitationNotification notification : notifications) {
			assertEquals(workspace, notification.workspace);
			assertEquals(anotherUser, notification.user);
		}
	}

	@Test
	public void checkThumbnailSender() {
		List<WorkspaceInvitationNotification> notifications = getNotifications(anotherUser, workspace);
		for (WorkspaceInvitationNotification notification : notifications) {
			assertEquals(workspace, notification.getThumbnailSender());
		}
	}

	private List<WorkspaceInvitationNotification> getNotifications(User user, Workspace workspace) {
		assertTrue(Application.POSTS_ENABLED);

		assertEquals(1, workspace.getUserMembers().size());

		UserNotification.deleteAll();

		WorkspaceInvitationNotification.raise(workspace, user);
		return WorkspaceInvitationNotification.find("workspace = ? AND user = ?", workspace, user).fetch();
	}
}

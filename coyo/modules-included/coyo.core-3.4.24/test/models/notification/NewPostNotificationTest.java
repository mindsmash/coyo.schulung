package models.notification;

import org.junit.Test;

import helper.PostHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.wall.Wall;
import models.wall.post.Post;
import test.UnitTestBase;

public class NewPostNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		PostNotification notification = NotificationHelper.addNewPostNotification(user, Notification.DisplayType.WEB);

		User anotherUser = UserHelper.getAnotherUser(user);
		String label;
		label = checkLabel(user, anotherUser, anotherUser.getDefaultWall(), notification);
		assertFalse(label.contains(user.getDisplayName()));

		label = checkLabel(user, user, user.getDefaultWall(), notification);
		assertFalse(label.contains(anotherUser.getDisplayName()));

		label = checkLabel(user, user, anotherUser.getDefaultWall(), notification);
		assertTrue(label.contains(anotherUser.getDisplayName()));

		User thirdUser = UserHelper.createNewUser();
		label = checkLabel(user, anotherUser, thirdUser.getDefaultWall(), notification);
		assertTrue(label.contains(thirdUser.getDisplayName()));
	}

	private String checkLabel(User user, User sender, Wall wall, PostNotification notification) {
		notification.post = PostHelper.simplePost(sender, wall, false);
		UserNotification un = UserNotification
				.find("user = ? AND notification = ? ORDER BY created DESC", user, notification).first();
		String label = notification.getText(un);
		assertFalse(label.startsWith("notification.post.new"));
		assertTrue(label.contains(notification.post.author.getDisplayName()));
		return label;
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		NewPostNotification notification = raiseNotification(user);
		assertNotNull(notification);
		assertTrue(notification instanceof NewPostNotification);
	}

	@Test
	public void checkThumbnailSender() {
		User user = UserHelper.createNewUser();
		NewPostNotification notification = raiseNotification(user);
		assertEquals(user, notification.getThumbnailSender());
	}

	private NewPostNotification raiseNotification(User user) {
		User anotherUser = UserHelper.createNewUser();
		Post post = PostHelper.simplePost(user, anotherUser.getDefaultWall(), false);

		assertTrue(Application.POSTS_ENABLED);

		NewPostNotification.raise(post);

		return NewPostNotification.find("post = ?", post).first();
	}
}

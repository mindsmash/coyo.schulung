package models.notification;

import java.util.List;

import org.junit.Test;

import helper.UserHelper;
import models.Application;
import models.User;
import models.UserNotification;
import test.UnitTestBase;

public class UserRegistrationNotificationTest extends UnitTestBase {

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();

		List<UserRegistrationNotification> notifications = getNotifications(user);
		for (UserRegistrationNotification notification : notifications) {
			assertEquals(user, notification.user);
		}
	}

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();

		List<UserRegistrationNotification> notifications = getNotifications(user);
		for (UserRegistrationNotification notification : notifications) {
			for (UserNotification un : notification.userNotifications) {
				if (un.notification == notification) {
					String label = notification.getText(un);
					assertFalse(label.startsWith("notification.user.registration."));
					assertTrue(label.contains(user.getFullName()));
				}
			}
		}
	}

	@Test
	public void checkThumbnailSender() {
		User user = UserHelper.createNewUser();
		List<UserRegistrationNotification> notifications = getNotifications(user);
		for (UserRegistrationNotification notification : notifications) {
			assertEquals(user, notification.getThumbnailSender());
		}
	}

	private List<UserRegistrationNotification> getNotifications(User user) {
		assertTrue(Application.POSTS_ENABLED);

		List<User> superadmins = User.find("superadmin = true AND active = true").fetch();
		int amount = 3 - superadmins.size();
		while (amount >= 0) {
			User admin = UserHelper.createNewUser();
			admin.superadmin = true;
			admin.save();
			superadmins.add(admin);
			amount--;
		}

		UserNotification.deleteAll();

		UserRegistrationNotification.raise(user);
		return UserRegistrationNotification.find("user = ?", user).fetch();
	}
}

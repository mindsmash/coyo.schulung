package models.notification;

import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.wall.Wall;
import models.wall.post.NotificationPost;

import org.junit.Test;

import test.UnitTestBase;

public class NewUserNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		NewUserNotification notification = NotificationHelper.addNewUserNotification(user, Notification.DisplayType.WEB);
		UserNotification un = UserNotification.find("user = ? AND notification = ? ORDER BY created DESC", user, notification).first();
		String label = notification.getText(un);
		assertFalse(label.startsWith("notification.user.new"));
		assertTrue(label.contains(user.getFullName()));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		assertNotNull(raiseNotification(user));
	}

	@Test
	public void checkThumbnailSender() {
		User user = UserHelper.createNewUser();
		NotificationPost userNotification = raiseNotification(user);
		NewUserNotification notification = (NewUserNotification) userNotification.notification;
		assertEquals(user, notification.getThumbnailSender());
	}

	private NotificationPost raiseNotification(User user) {
		assertTrue(Application.POSTS_ENABLED);

		Wall wall = Application.get().getDefaultWall();

		String query = "author = ? AND wall = ?";
		NotificationPost.delete(query, user, wall);

		NewUserNotification.raise(user);

		return NotificationPost.find(query, user, wall).first();
	}
}

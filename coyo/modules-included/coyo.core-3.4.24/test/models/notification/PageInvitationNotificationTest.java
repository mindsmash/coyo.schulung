package models.notification;

import helper.PageHelper;
import helper.UserHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.page.Page;
import org.junit.Test;
import test.UnitTestBase;

public class PageInvitationNotificationTest extends UnitTestBase {

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Page page = PageHelper.createPage(user, false, false);

		PageInvitationNotification invitation = getInvitation(user, page);
		assertEquals(page, invitation.page);
		assertEquals(user, invitation.user);
	}

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		Page page = PageHelper.createPage(user, false, false);

		PageInvitationNotification notification = getInvitation(user, page);

		for (UserNotification un : notification.userNotifications) {
			if (un.notification == notification) {
				String label = notification.getText(un);
				assertFalse(label.startsWith("notification.page.invite"));
				assertTrue(label.contains(page.name));
			}
		}
	}

	private PageInvitationNotification getInvitation(User user, Page page) {
		assertTrue(Application.POSTS_ENABLED);

		String query = "user = ?";
		UserNotification.delete(query, user);

		PageInvitationNotification.raise(page, user);
		return PageInvitationNotification.find("page = ? AND user = ?", page, user).first();
	}
}

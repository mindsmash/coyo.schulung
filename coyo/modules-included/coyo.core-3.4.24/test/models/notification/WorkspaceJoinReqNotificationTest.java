package models.notification;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import helper.UserHelper;
import helper.WorkspaceHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import test.UnitTestBase;

public class WorkspaceJoinReqNotificationTest extends UnitTestBase {

	private User user;
	private Workspace workspace;

	@Before
	public void before() {
		user = UserHelper.createNewUser();
		workspace = WorkspaceHelper.createWorkspace(user);
	}

	@Test
	public void checkRaise() {
		List<WorkspaceJoinReqNotification> notifications = getNotifications(user, workspace);
		for (WorkspaceJoinReqNotification notification : notifications) {
			assertEquals(workspace, notification.workspace);
			assertEquals(user, notification.user);
		}
	}

	@Test
	public void checkThumbnailSender() {
		List<WorkspaceJoinReqNotification> notifications = getNotifications(user, workspace);
		for (WorkspaceJoinReqNotification notification : notifications) {
			assertEquals(workspace, notification.workspace);
		}
	}

	private List<WorkspaceJoinReqNotification> getNotifications(User user, Workspace workspace) {
		assertTrue(Application.POSTS_ENABLED);

		workspace.addMember(UserHelper.createNewUser(), true);
		workspace.save();

		UserNotification.deleteAll();
		WorkspaceJoinReqNotification.raise(workspace, user);
		return WorkspaceJoinReqNotification.find("workspace = ? AND user = ?", workspace, user).fetch();
	}
}

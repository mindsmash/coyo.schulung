package models.notification;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import helper.PageHelper;
import helper.UserHelper;
import helper.notification.NotificationHelper;
import models.Application;
import models.User;
import models.UserNotification;
import models.page.Page;
import models.wall.Wall;
import models.wall.post.NotificationPost;

import org.junit.Test;

import test.UnitTestBase;

public class NewPageNotificationTest extends UnitTestBase {

	@Test
	public void checkLabel() {
		User user = UserHelper.createNewUser();
		PageNotification notification = NotificationHelper.addNewPageNotification(user, Notification.DisplayType.WEB);

		User anotherUser = UserHelper.getAnotherUser(user);
		UserNotification un = UserNotification.find("user = ? AND notification = ? ORDER BY created DESC", user, notification).first();
		String label = notification.getText(un);

		assertFalse(label.startsWith("notification.page.new"));

		assertTrue(label.contains(notification.page.creator.getDisplayName()));
		assertTrue(label.contains(notification.page.name));
	}

	@Test
	public void checkRaise() {
		User user = UserHelper.createNewUser();
		Page page = PageHelper.createPage(user);

		assertTrue(Application.POSTS_ENABLED);

		assertNotNull(checkNotification(page, Application.get().getDefaultWall()));
	}

	private NotificationPost checkNotification(Page page, Wall wall) {

		String query = "author = ? AND wall = ?";
		NotificationPost.delete(query, page, wall);

		NewPageNotification.raise(page);

		return NotificationPost.find(query, page, wall).first();
	}
}

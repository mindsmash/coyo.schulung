package models;

import helper.AppHelper;
import helper.BlobHelper;
import helper.UserHelper;
import models.app.App;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareSenderPost;

import org.junit.Before;
import org.junit.Test;

import play.templates.JavaExtensions;
import test.UnitTestBase;

public class SenderTest extends UnitTestBase {

	private Sender sender;

	@Before
	public void before() {
		sender = UserHelper.getRandomUser();
	}

	@Test
	public void hasMediaLibrary() {
		assertNotNull(sender.getMediaLibrary());
	}

	@Test
	public void checkActiveApps() {
		for (App app : sender.getActiveApps()) {
			assertTrue(app.active);
		}

		AppHelper.createWallApp(sender, true);
		AppHelper.createWallApp(sender, false);

		for (App app : sender.getActiveApps()) {
			assertTrue(app.active);
		}
	}

	@Test
	public void checkSlugify() {
		assertEquals(sender.getSlug(), JavaExtensions.slugify(sender.getDisplayName()));
	}

	@Test
	public void checkShare() {
		User user = UserHelper.getAnotherUser((User) sender);
		Wall wall = Application.get().getDefaultWall();
		Post post = sender.share(user, wall, "Test message");

		assertEquals(post, ShareSenderPost.findById(post.id));
	}

	@Test
	public void checkAvatarUrl() throws InterruptedException {
		String placeholderUrl = sender.getAvatarURL();
		assertTrue(placeholderUrl.endsWith("-nopic.png"));

		sender.avatar = BlobHelper.getDummyBlob();
		String imageUrl = sender.getAvatarURL();

		assertTrue(imageUrl.contains("avatar"));
		assertTrue(imageUrl.contains(String.valueOf(sender.id)));
		assertTrue(imageUrl.contains(String.valueOf(sender.modified.getTime())));
	}

	@Test
	public void checkThumbUrl() {
		String placeholderUrl = sender.getThumbURL();
		assertTrue(placeholderUrl.endsWith("-nothumb.png"));

		sender.thumbnail = BlobHelper.getDummyBlob();
		String imageUrl = sender.getThumbURL();

		assertTrue(imageUrl.contains("thumb"));
		assertTrue(imageUrl.contains(String.valueOf(sender.id)));
		assertTrue(imageUrl.contains(String.valueOf(sender.modified.getTime())));
	}

	@Test
	public void checkFollowerCount() {
		User user = UserHelper.getAnotherUser((User) sender);
		
		Long followerCount = sender.getFollowerCount();

		sender.followers.add(user);
		sender.save();

		// auto follow self
		assertEquals(followerCount+1, sender.getFollowerCount());
	}
}
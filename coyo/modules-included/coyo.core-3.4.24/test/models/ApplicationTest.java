package models;

import java.util.List;

import org.junit.Test;

import test.UnitTestBase;

public class ApplicationTest extends UnitTestBase {

	@Test
	public void hasDisplayName() {
		assertNotNull(Application.get().getDisplayName());
	}

	@Test
	public void getIsSingleton() {
		Application app1 = Application.get();
		Application app2 = Application.get();
		assertSame(app1, app2);
	}

	@Test
	public void getIsSingletonException() {
		Application.get();

		try {
			Application.get().save();
		} catch (RuntimeException e) {
			fail();
		}

		Application app = null;
		try {
			app = new Application();
			app.save();
			fail();
		} catch (RuntimeException e) {
			if (app != null) {
				app.delete();
			}
		}
	}

	@Test
	public void checkSenderType() {
		assertEquals(Application.get().getSenderType(), "application");
	}

	@Test
	public void checkDefaultWall() {
		assertNotNull(Application.get().getDefaultWall());
	}

	@Test
	public void checkAdmins() {
		List<User> admins = Application.get().getAdmins();
		for (User user : admins) {
			assertTrue(user.isActive());
			assertTrue(user.superadmin);
		}
	}

	@Test
	public void checkChangeAdmin() {
		User user = User.find("active = true AND superadmin = ?", false).first();

		List<User> admins = Application.get().getAdmins();
		assertFalse(admins.contains(user));
		user.superadmin = true;
		user.save();

		admins = Application.get().getAdmins();
		assertTrue(admins.contains(user));
	}

	@Test
	public void checkChangeUser() {
		User user = User.find("active = true AND superadmin = ?", true).first();

		List<User> admins = Application.get().getAdmins();
		assertTrue(admins.contains(user));

		user.superadmin = false;
		user.save();

		admins = Application.get().getAdmins();
		assertFalse(admins.contains(user));
	}
}

package models;

import org.junit.Test;

import test.UnitTestBase;

public class UserGroupTest extends UnitTestBase {
	@Test
	public void hasGrant() {
		String grant = "test";

		UserGroup group = new UserGroup() {
		};

		UserRole role = new UserRole();
		group.role = role;
		assertFalse(group.hasGrant(grant));

		role.grants.add(grant);
		assertTrue(group.hasGrant(grant));
	}

	@Test
	public void isDeletable() {
		assertFalse(new UserGroup() {
		}.isDeletable());
	}

	@Test
	public void isUserManageable() {
		assertFalse(new UserGroup() {
		}.isUserManageable());
	}
}

package models.statistics;

import java.util.Date;

import multitenancy.MTA;

import org.joda.time.DateTime;
import org.junit.Test;
import jobs.*;
import test.UnitTestBase;

public class TrackingRecordTest extends UnitTestBase {

	private static final String TRACK_TYPE = "test";

	@Test
	public void testTracking() throws Exception {
		// make sure tenant is persisted because the statistics persister is running in another thread
		refreshTx();
		MTA.activateMultitenancy();

		Date start = new DateTime().minusDays(7).toDate();
		int days = 10;

		// general tracking
		StatisticsPersisterJob.syncQueueItems();
		assertEquals(0, TrackingRecord.load(start, TRACK_TYPE, days).total.intValue());
		TrackingRecord.track(TRACK_TYPE, "discriminator", 5);
		Thread.sleep(100);
		StatisticsPersisterJob.syncQueueItems();
		assertEquals(5, TrackingRecord.load(start, TRACK_TYPE, days).total.intValue());

		// single tracking
		TrackingRecord.trackOne(TRACK_TYPE);
		Thread.sleep(100);
		StatisticsPersisterJob.syncQueueItems();
		assertEquals(6, TrackingRecord.load(start, TRACK_TYPE, days).total.intValue());
	}
}

package models;

import helper.PageHelper;
import helper.UserHelper;
import models.app.MediaApp;
import models.comments.Comment;
import models.media.MediaCollection;
import models.media.MediaPhoto;
import models.media.MediaRoom;
import models.page.Page;
import multitenancy.MTA;

import org.junit.Test;

import storage.FlexibleBlob;
import test.TestCoyoTenant;
import test.UnitTestBase;

public class TenantTest extends UnitTestBase {

	@Test
	public void delete() {
		MTA.activateMultitenancy(TestCoyoTenant.get());

		// create some more data
		addSomeData();

		assertNotNull(TestCoyoTenant.get());
		assertEquals(MTA.getActiveTenant(), TestCoyoTenant.get());
		assertEquals(1L, Tenant.count());
		Tenant tenant = (Tenant) MTA.getActiveTenant();
		tenant.delete();
		assertEquals(0L, Tenant.count());
		assertEquals(0L, Sender.count());
	}

	private void addSomeData() {
		User user = UserHelper.getRandomUser();
		MediaRoom room = new MediaRoom(user);
		room.save();
		addPhotos(room, user);

		Page page = PageHelper.createPage(user);
		MediaApp app = new MediaApp();
		app.sender = page;
		app.save();
		addPhotos(app.room, user);

		refreshTx();
		MTA.activateMultitenancy(TestCoyoTenant.get());
	}

	private void addPhotos(MediaRoom room, User user) {
		MediaCollection album = new MediaCollection();
		album.room = room;
		album.save();

		MediaPhoto photo1 = new MediaPhoto();
		photo1.collection = album;
		photo1.creator = user;
		photo1.file = new FlexibleBlob();
		photo1.filename = "Photo1";
		photo1.save();

		Comment comment1 = new Comment();
		comment1.author = user;
		comment1.text = "Comment1";
		comment1.save();
		photo1.addComment(comment1);

		Comment comment2 = new Comment();
		comment2.author = UserHelper.getAnotherUser(user);
		comment2.text = "Comment2";
		comment2.save();
		photo1.addComment(comment2);

		MediaPhoto photo2 = new MediaPhoto();
		photo2.collection = album;
		photo2.creator = UserHelper.getAnotherUser(user);
		photo2.file = new FlexibleBlob();
		photo2.filename = "Photo2";
		photo2.save();
	}
}

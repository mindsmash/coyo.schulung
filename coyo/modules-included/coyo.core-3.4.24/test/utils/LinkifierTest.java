package utils;

import static org.junit.Assert.*;
import helper.UserHelper;
import models.User;

import org.junit.Test;

import play.mvc.Router;
import test.UnitTestBase;

public class LinkifierTest extends UnitTestBase {

	@Test
	public void testUserReference() {
		User user = UserHelper.createNewUser();
		String slug = user.getSlug();

		// test user reference
		String messageWithSlug = "@" + slug;
		assertTrue(Linkifier.linkify(messageWithSlug).contains(user.getFullName()));
		assertTrue(Linkifier.linkify(messageWithSlug).contains("href=\"" + Router.reverse("Users.wall").url));

		// test invalid user reference
		String messageWithName = "@" + user.firstName;
		assertFalse(Linkifier.linkify(messageWithName).contains("href"));
	}

	@Test
	public void testEmailLink() {
		// test email address
		String messageWithEmail = "bob@gmail.com";
		assertEquals("<a href=\"mailto:" + messageWithEmail + "\" target=\"blank\">" + messageWithEmail + "</a>", Linkifier.linkify(messageWithEmail));

		// test invalid email
		String invalidEmail = "test@test";
		assertFalse(Linkifier.linkify(invalidEmail).contains("href"));
	}

}

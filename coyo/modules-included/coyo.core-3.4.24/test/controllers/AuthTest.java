package controllers;

import org.junit.Test;

import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.Router;
import test.FunctionalTestBase;

public class AuthTest extends FunctionalTestBase {

	private static final String EMAIL = "janmarq@mac.com";
	private static final String PASSWORD = "secret";

	@Test
	public void testRedirect() {
		// assert redirected
		Response response = GET("/");
		assertStatus(302, response);
		assertTrue(response.getHeader("Location").indexOf(Router.reverse("Auth.login").url) >= 0);
	}

	@Test
	public void testWrongParams() {
		// wrong params
		Request request = newRequest();
		request.params.put("username", "notAnEmail");
		request.params.put("rememberMe", "false");
		Response response = POST(request, Router.reverse("Auth.authenticate"));
		assertStatus(302, response);
		assertTrue(response.getHeader("Location").endsWith(Router.reverse("Auth.login").url));
	}

	@Test
	public void testWrongCredentials() {
		// wrong password
		Request request = newRequest();
		request.params.put("username", EMAIL);
		request.params.put("password", "someWrongPassword");
		request.params.put("rememberMe", "false");
		Response response = POST(request, Router.reverse("Auth.authenticate"));
		assertStatus(302, response);
		assertTrue(response.getHeader("Location").endsWith(Router.reverse("Auth.login").url));
	}

	@Test
	public void testLogin() {
		login();
	}

	public static void login() {
		Request request = newRequest();
		request.params.put("username", EMAIL);
		request.params.put("password", PASSWORD);
		Response response = POST(request, Router.reverse("Auth.authenticate"));
		assertStatus(302, response);
		assertFalse(response.getHeader("Location").endsWith(Router.reverse("Auth.login").url));
	}
}

package controllers;

import java.util.HashMap;
import java.util.Map;

import models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;

import play.Logger;
import play.mvc.Http.Response;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import test.FunctionalTestBase;

public class UserChooserTest extends FunctionalTestBase {

	@Test
	public void testSearch() throws JSONException {
		User user = User.find("active = true").first();
		user.department = "Some Department";
		user.jobTitle = "Chef";
		user.save();
		refreshTx();

		Logger.debug("[UserChooserTest] Testing with user %s", user);

		// search by random string
		Response response = POST(createRequestWithAuthToken(user),
				getChooserUrl(false, true, "ho3g128793t21viduavid12321"));
		assertStatus(200, response);
		assertEquals(0, unmarshal(response).length());

		// search by first name
		response = POST(createRequestWithAuthToken(user), getChooserUrl(false, true, user.firstName));
		assertStatus(200, response);
		assertTrue(unmarshal(response).length() > 0);

		// search by last name
		response = POST(createRequestWithAuthToken(user), getChooserUrl(false, true, user.lastName));
		assertStatus(200, response);
		assertTrue(unmarshal(response).length() > 0);

		// search by department
		response = POST(createRequestWithAuthToken(user), getChooserUrl(false, true, user.department));
		assertStatus(200, response);
		assertTrue(unmarshal(response).length() > 0);

		// search by job title
		response = POST(createRequestWithAuthToken(user), getChooserUrl(false, true, user.jobTitle));
		assertStatus(200, response);
		assertTrue(unmarshal(response).length() > 0);
	}

	private String getChooserUrl(boolean showExternals, boolean showMe, String term) {
		Map<String, Object> params = new HashMap<>();
		params.put("showExternals", showExternals);
		params.put("showMe", showMe);
		params.put("term", term);
		ActionDefinition ad = Router.reverse("Users.chooser", params);
		String url = ad.url;
		Logger.debug("[UserChooserTest] Chooser URL: %s", url);
		return url;
	}

	private JSONArray unmarshal(Response response) throws JSONException {
		String string = getContent(response);
		Logger.debug("[UserChooserTest] Response: %s", string);
		return new JSONArray(string);
	}
}

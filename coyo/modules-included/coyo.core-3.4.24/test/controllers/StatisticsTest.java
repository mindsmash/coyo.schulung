package controllers;

import helper.CommentHelper;
import helper.PostHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jobs.StatisticsPersisterJob;
import models.comments.Comment;
import models.User;
import models.statistics.TrackingRecord;
import models.wall.post.Post;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import play.mvc.Router;
import test.FunctionalTestBase;

@Ignore
public class StatisticsTest extends FunctionalTestBase {

	private static final Date start = new DateTime().minusDays(7).toDate();
	private static final int days = 10;

	@Override
	public void loadDataAndForceTenant() {
		super.loadDataAndForceTenant();
	}
	
	@Before
	public void clearTrackingRecords() {
		TrackingRecord.deleteAll();
		StatisticsPersisterJob.indexQueue.clear();
	}

	@Test
	@Ignore
	public void testLogins() throws InterruptedException {
		int total = TrackingRecord.load(start, "logins", days).total.intValue();
		AuthTest.login();
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(500);
		assertEquals(total + 1, TrackingRecord.load(start, "logins", days).total.intValue());
	}

	@Test
	public void testVisitsAndPageviews() throws InterruptedException {
		User user = User.find("active = true").first();

		int totalVisits = TrackingRecord.load(start, "visits", days).total.intValue();
		int totalPageviews = TrackingRecord.load(start, "pageviews", days).total.intValue();
		// first request
		Thread.sleep(500);
		assertStatus(200, GET(createRequestWithAuthToken(user), Router.reverse("ActivityStream.index").url));
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(500);
		assertEquals(totalVisits + 1, TrackingRecord.load(start, "visits", days).total.intValue());
		assertEquals(totalPageviews + 1, TrackingRecord.load(start, "pageviews", days).total.intValue());

		// another request
		assertStatus(200, GET(createRequestWithAuthToken(user), Router.reverse("ActivityStream.index").url));
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(500);
		assertEquals(totalVisits + 1, TrackingRecord.load(start, "visits", days).total.intValue());
		assertEquals(totalPageviews + 2, TrackingRecord.load(start, "pageviews", days).total.intValue());
	}

	@Test
	public void testPostsCommentsAndLikes() throws InterruptedException {
		User user = User.find("active = true").first();
		// test post
		int totalPosts = TrackingRecord.load(start, "posts", days).total.intValue();
		Post post = PostHelper.simplePost(user, user.getDefaultWall(), false);
		Thread.sleep(100);
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(100);
		assertEquals(totalPosts + 1, TrackingRecord.load(start, "posts", days).total.intValue());

		// test comment
		int totalComments = TrackingRecord.load(start, "comments", days).total.intValue();
		Comment comment = CommentHelper.createComment(user);
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(100);
		assertEquals(totalComments + 1, TrackingRecord.load(start, "comments", days).total.intValue());

		// persist our stuff for next request
		refreshTx();

		// test like
		int totalLikes = TrackingRecord.load(start, "likes", days).total.intValue();

		Map<String, String> params = new HashMap<>();
		params.put("id", post.id + "");
		params.put("clazz", post.getClass().getName());
		params.put(TokenAuthController.HEADER_NAME, TokenAuthController.createToken(user.id));
		POST(Router.reverse("Collaboration.like").url, params);
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(100);
		assertEquals(totalLikes + 1, TrackingRecord.load(start, "likes", days).total.intValue());

		params.put("id", comment.id + "");
		params.put("clazz", comment.getClass().getName());
		POST(Router.reverse("Collaboration.like").url, params);
		StatisticsPersisterJob.syncQueueItems();
		Thread.sleep(100);
		assertEquals(totalLikes + 2, TrackingRecord.load(start, "likes", days).total.intValue());
	}
}

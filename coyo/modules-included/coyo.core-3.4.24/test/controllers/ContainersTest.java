package controllers;

import json.list.ContainerSerializer;
import json.list.FieldSerializer;
import json.list.ItemSerializer;
import json.list.ItemValueSerializer;
import models.Sender;
import models.User;
import models.container.Container;
import models.container.Field;
import models.container.Item;
import models.container.ItemValue;
import models.container.fields.NumberField;
import models.container.fields.TextField;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Http.Response;
import play.mvc.Router;
import test.FunctionalTestBase;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import containers.form.ContainerFormData;
import containers.form.FieldFormData;
import containers.form.ItemFormData;
import containers.form.ItemValueFormData;

public class ContainersTest extends FunctionalTestBase {
	
	private class DateFieldExclusionStrategy implements ExclusionStrategy {
		
		@Override
		public boolean shouldSkipField(FieldAttributes f) {
			return f.getName().equals("created") ||  f.getName().equals("modified");
		}
		
		@Override
		public boolean shouldSkipClass(Class<?> arg0) {
			return false;
		}
	}
	
	private static final String ITEM_VALUE = "Test value";
	private static final String TEXT_FIELD_LABEL = "Test text field";
	private static final String CONTAINER_NAME = "Test container";
	private static final String APPLICATION_JSON = "application/json";
	private static final String NUMBER_FIELD_LABEL = "Test number field";
	
	private User user;

	@After
	public void cleanUp() {
		// delete data manually because otherwise deleting Users would fail
		JPA.em().createNativeQuery("delete from container_field_prop").executeUpdate();
		ItemValue.deleteAll();
		Item.deleteAll();
		Field.deleteAll();
		Container.deleteAll();
	}

	@Before
	public void setUp() {
		user = User.find("active=true").first();
	}

	@Test
	public void testSaveContainer() throws Exception {
		// create container and form datas
		Container container = createContainer(user);
		ContainerFormData formData = createContainerFormData(user, container);
		formData.fields.add(createTextFieldFormData());
		
		// send form data
		Response response = saveContainer(formData);
		assertStatus(Http.StatusCode.CREATED, response);
		assertContentType(APPLICATION_JSON, response);
		
		// retrieve container
		Container newContainer = unmarshalContainer(response);
		assertNotNull(newContainer.id);
		assertEquals(CONTAINER_NAME, newContainer.name);

		// check fields
		assertEquals(1, newContainer.fields.size());
		Field textField = newContainer.fields.get(0);
		assertTrue(textField.required);
		assertFalse(textField.hidden);
		assertEquals(2, textField.properties.size());
	}

	@Test
	public void testCreateItem() throws Exception {
		// create container and form datas
		Container newContainer = createContainer(user);
		ContainerFormData formData = createContainerFormData(user, newContainer);
		formData.fields.add(createTextFieldFormData());
		
		// send form data
		Response response = saveContainer(formData);
		
		// retrieve container
		Container container = unmarshalContainer(response);
		assertNotNull(container);

		// create new item
		Response itemResponse = createItem(container);
		assertStatus(Http.StatusCode.CREATED, itemResponse);

		// retrieve and check item
		Item item = unmarshalItem(itemResponse);
		assertNotNull(item);
		assertEquals(1, item.values.size());
		assertEquals(ITEM_VALUE, item.values.get(0).getValue());
	}

	@Test
	public void testCreateItemWithInvalidData() throws Exception {
		// create container and form data
		Container newContainer = createContainer(user);
		ContainerFormData formData = createContainerFormData(user, newContainer);
		formData.fields.add(createTextFieldFormData());
		formData.fields.add(createNumberFieldFormData());
		
		// send form data
		Response response = saveContainer(formData);
		
		// retrieve container
		Container container = unmarshalContainer(response);
		assertNotNull(container);
		
		// try to create invalid item: missing required value
		Field textField = container.fields.get(0);
		ItemFormData itemFormData = makeValidItemFormData(textField);
		Response itemResponse = createItem(container, itemFormData);
		assertStatus(Http.StatusCode.BAD_REQUEST, itemResponse);
		assertContentMatch(Messages.getMessage(user.language.toString(), "validation.required"), itemResponse);

		// try to create invalid item: value too long
		Field numberField = container.fields.get(1);
		String invalidTextFieldValue = "ValueThatIsReallyWayTooLong";
		itemFormData = new ItemFormData(new ItemValueFormData(textField.id, invalidTextFieldValue), new ItemValueFormData(numberField.id, "42"));
		itemResponse = createItem(container, itemFormData);
		assertStatus(Http.StatusCode.BAD_REQUEST, itemResponse);
		assertContentMatch(
				Messages.getMessage(user.language.toString(), "validation.maxSize", invalidTextFieldValue,
						textField.getProperty("maxLength")), itemResponse);
	}

	@Test
	public void testUpdateItem() throws Exception {
		// create container and form data
		Container newContainer = createContainer(user);
		ContainerFormData formData = createContainerFormData(user, newContainer);
		formData.fields.add(createTextFieldFormData());
		
		// send form data
		Response response = saveContainer(formData);
		
		// retrieve container
		Container container = unmarshalContainer(response);
		assertNotNull(container);

		// create item
		Response itemResponse = createItem(container);
		assertStatus(Http.StatusCode.CREATED, itemResponse);
		Item item = unmarshalItem(itemResponse);

		// update item
		Long textFieldId = container.fields.get(0).id;
		Response updateResponse = POST(createRequestWithAuthToken(user),
				Router.reverse("Containers.saveItem", ImmutableMap.<String, Object> of("containerId", container.id, "itemId", item.id)),
				APPLICATION_JSON, toJson(new ItemFormData(new ItemValueFormData(textFieldId, "New value"))));
		assertStatus(Http.StatusCode.OK, updateResponse);

		// check updated item
		Item updatedItem = unmarshalItem(updateResponse);
		assertEquals("New value", updatedItem.getValueForField(textFieldId));
	}

	@Test
	public void testGetContainerWithoutItems() throws Exception {
		// create container and form data
		Container newContainer = createContainer(user);
		ContainerFormData formData = createContainerFormData(user, newContainer);
		formData.fields.add(createTextFieldFormData());
		
		// send form data
		Response response = saveContainer(formData);
		
		// retrieve container
		Container container = unmarshalContainer(response);
		assertNotNull(container);

		// insert item
		assertStatus(201, createItem(container));

		response = GET(createRequestWithAuthToken(user), Router.reverse("Containers.getContainer",
				ImmutableMap.<String, Object> of("containerId", container.id)).url);
		assertStatus(Http.StatusCode.OK, response);

		Container savedContainer = unmarshalContainer(response);
		assertEquals(container.name, savedContainer.name);

		// make sure that items are *not* contained in the response
		assertTrue(container.items.isEmpty());
	}
	
	private Container createContainer(Sender sender) {
		Container container = new Container(sender);
		container.name = CONTAINER_NAME;
		container.save();
		refreshTx();
		return container;
	}

	private ContainerFormData createContainerFormData(Sender sender, Container container) {
		ContainerFormData containerFormData = new ContainerFormData();
		containerFormData.id = container.id;
		containerFormData.senderId = sender.id;
		containerFormData.name = CONTAINER_NAME;
		containerFormData.fields = Lists.newArrayList();
		return containerFormData;
	}
	
	private FieldFormData createTextFieldFormData() {
		FieldFormData fieldFormData = new FieldFormData();
		fieldFormData.type = TextField.TYPE;
		fieldFormData.priority = 1;
		fieldFormData.label = TEXT_FIELD_LABEL;
		fieldFormData.required = true;
		fieldFormData.hidden = false;
		fieldFormData.properties = ImmutableMap.<String, String> of("multiline", "false", "maxLength", "10");
		return fieldFormData;
	}
	
	private FieldFormData createNumberFieldFormData() {
		FieldFormData fieldFormData = new FieldFormData();
		fieldFormData.type = NumberField.TYPE;
		fieldFormData.priority = 2;
		fieldFormData.label = NUMBER_FIELD_LABEL;
		fieldFormData.required = true;
		fieldFormData.hidden = false;
		return fieldFormData;
	}
	
	private Response saveContainer(ContainerFormData containerFormData) {
		return POST(createRequestWithAuthToken(user), Router.reverse("Containers.saveContainer").url, APPLICATION_JSON, toJson(containerFormData));
	}

	private Response createItem(final Container container) {
		return createItem(container, makeValidItemFormData(container.fields.get(0)));
	}

	private Response createItem(final Container container, final ItemFormData itemFormData) {
		return POST(
				createRequestWithAuthToken(user),
				Router.reverse("Containers.createItem", ImmutableMap.<String, Object> of("containerId", container.id)).url,
				APPLICATION_JSON, toJson(itemFormData));
	}

	private ItemFormData makeValidItemFormData(final Field field) {
		return new ItemFormData(new ItemValueFormData(field.id, ITEM_VALUE));
	}

	private String toJson(Object object) {
		return new Gson().toJson(object);
	}

	private Container unmarshalContainer(Response response) {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Container.class, new ContainerSerializer());
		builder.registerTypeHierarchyAdapter(Field.class, new FieldSerializer());
		return builder.create().fromJson(getContent(response), Container.class);
	}

	private Item unmarshalItem(Response response) {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Item.class, new ItemSerializer());
		builder.registerTypeHierarchyAdapter(ItemValue.class, new ItemValueSerializer());
		builder.setExclusionStrategies(new DateFieldExclusionStrategy());
		return builder.create().fromJson(getContent(response), Item.class);
	}
}

package acl;

import helper.UserHelper;
import helper.WorkspaceHelper;
import models.Settings;
import models.User;
import models.app.CalendarApp;
import models.app.WallApp;
import models.event.Event;
import models.messaging.Conversation;
import models.messaging.ConversationMember;
import models.page.Page;
import models.wall.post.Post;
import models.wall.post.SimplePost;
import models.workspace.Workspace;

import org.junit.Before;
import org.junit.Test;

import play.mvc.Http.Request;
import test.UnitTestBase;
import conf.ApplicationSettings;
import controllers.SecureBaseController;

public class PermissionCheckerTest extends UnitTestBase {

	private PermissionChecker permissionChecker;
	private User bob;
	private User admin;

	@Before
	public void setUp() {
		permissionChecker = new PermissionChecker();

		initUsers();

		assertNotNull(bob);
		assertNotNull(admin);
	}

	private void initUsers() {
		if (bob == null || admin == null) {
			bob = User.find("byEmail", "bob@gmail.com").first();
			admin = User.find("byEmail", "daniel.busch@mindsmash.com").first();
		}
	}

	@Test
	public void testSuperadmin() {
		setActiveUser(bob);
		assertFalse(permissionChecker.superadmin());

		setActiveUser(admin);
		assertTrue(permissionChecker.superadmin());
	}

	@Test
	public void testCreate() {
		setActiveUser(bob);
		assertFalse(permissionChecker.createPage());
		assertFalse(permissionChecker.createEvent());
		assertFalse(permissionChecker.createWorkspace());
		assertFalse(permissionChecker.createPage(Page.PageVisibility.PRIVATE));
		assertFalse(permissionChecker.createEvent(Event.EventVisibility.PRIVATE));
		assertFalse(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.PRIVATE));
		assertFalse(permissionChecker.createPage(Page.PageVisibility.PUBLIC));
		assertFalse(permissionChecker.createEvent(Event.EventVisibility.PUBLIC));
		assertFalse(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.PUBLIC));
		assertFalse(permissionChecker.createEvent(Event.EventVisibility.CLOSED));
		assertFalse(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.CLOSED));

		setActiveUser(admin);
		assertTrue(permissionChecker.createPage());
		assertTrue(permissionChecker.createEvent());
		assertTrue(permissionChecker.createWorkspace());
		assertTrue(permissionChecker.createPage(Page.PageVisibility.PRIVATE));
		assertTrue(permissionChecker.createEvent(Event.EventVisibility.PRIVATE));
		assertTrue(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.PRIVATE));
		assertTrue(permissionChecker.createPage(Page.PageVisibility.PUBLIC));
		assertTrue(permissionChecker.createEvent(Event.EventVisibility.PUBLIC));
		assertTrue(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.PUBLIC));
		assertTrue(permissionChecker.createEvent(Event.EventVisibility.CLOSED));
		assertTrue(permissionChecker.createWorkspace(Workspace.WorkspaceVisibility.CLOSED));

	}

	@Test
	public void testFindUser() {
		bob.active = false;
		bob.save();
		assertFalse(permissionChecker.listUser(bob.id));
		bob.active = true;
		bob.deleted = true;
		bob.save();
		assertFalse(permissionChecker.listUser(bob.id));
		bob.deleted = false;
		bob.save();
		assertTrue(permissionChecker.listUser(bob.id));
	}

	@Test
	public void testAccessUser() throws InterruptedException {
		setActiveUser(bob);
		Thread.sleep(500);
		assertTrue(permissionChecker.accessUser(bob.id));
		assertTrue(permissionChecker.accessUser(admin.id));
		bob.external = true;
		bob.save();
		Thread.sleep(500);
		assertTrue(permissionChecker.accessUser(bob.id));
		assertFalse(permissionChecker.accessUser(admin.id));
		bob.external = false;
		bob.save();
	}

	@Test
	public void testEditUserProfile() {
		setActiveUser(admin);
		assertNotNull(bob);
		assertFalse(permissionChecker.editUserProfile(bob.id));
		setActiveUser(bob);
		assertTrue(permissionChecker.editUserProfile(bob.id));
	}

	@Test
	public void testEditAndDeletePage() {
		setActiveUser(admin);
		Page page = Page.find("Select p from Page p where p.name = ?", "Page1").first();
		assertFalse(permissionChecker.editPage(page.id));
		assertFalse(permissionChecker.editPage(page.id, Page.PageVisibility.PRIVATE));
		assertFalse(permissionChecker.editPage(page.id, Page.PageVisibility.PUBLIC));
		assertFalse(permissionChecker.deletePage(page.id));

		setActiveUser(bob);
		assertTrue(permissionChecker.editPage(page.id));
		assertTrue(permissionChecker.editPage(page.id, Page.PageVisibility.PRIVATE));
		assertFalse(permissionChecker.editPage(page.id, Page.PageVisibility.PUBLIC));
		assertFalse(permissionChecker.deletePage(page.id));

		setActiveUser(admin);
		page = Page.find("Select p from Page p where p.name = ?", "Page2").first();
		assertTrue(permissionChecker.editPage(page.id, Page.PageVisibility.PUBLIC));
		assertTrue(permissionChecker.deletePage(page.id));
	}

	@Test
	public void testListAndAccessPage() {
		setActiveUser(admin);
		Page page = Page.find("Select p from Page p where p.name = ?", "Page1").first();
		assertFalse(permissionChecker.editPage(page.id));
		assertFalse(permissionChecker.accessPage(page.id));
		assertFalse(permissionChecker.listPage(page.id));
		page.visibility = Page.PageVisibility.PUBLIC;
		page.save();
		assertTrue(permissionChecker.accessPage(page.id));
		assertTrue(permissionChecker.listPage(page.id));
		page.visibility = Page.PageVisibility.PRIVATE;
		page.save();
		User member = User.find("select u from User u where u.email = ?", "janmarq@mac.com").first();
		setActiveUser(member);
		assertTrue(permissionChecker.accessPage(page.id));
		assertTrue(permissionChecker.listPage(page.id));
	}

	@Test
	public void testCreateEditAndDeleteEvent() {
		CalendarApp calendarApp = CalendarApp.find("byDisplayName", "Calendar1").first();
		Event event = Event.find("byName", "Event1").first();

		setActiveUser(admin);
		assertFalse(permissionChecker.createEvent(calendarApp.id));
		assertTrue(permissionChecker.editEvent(event));
		assertTrue(permissionChecker.deleteEvent(event.id));
		assertTrue(permissionChecker.editEvent(event.id, Event.EventVisibility.PUBLIC));

		setActiveUser(bob);
		assertFalse(bob.hasGrant(UserGrant.CREATE_PUBLIC_EVENT));
		assertTrue(permissionChecker.createEvent(calendarApp.id));
		assertTrue(permissionChecker.editEvent(event));
		assertTrue(permissionChecker.deleteEvent(event.id));
		assertFalse(permissionChecker.editEvent(event.id, Event.EventVisibility.PUBLIC));

		User daniel = User.find("byEmail", "janmarq@mac.com").first();
		setActiveUser(daniel);
		assertFalse(permissionChecker.editEvent(event));
		assertFalse(permissionChecker.deleteEvent(event.id));

		setActiveUser(bob);
		event.calendar = null;
		event.save();
		assertFalse(permissionChecker.deleteEvent(event.id));
		setActiveUser(admin);
		assertTrue(permissionChecker.deleteEvent(event.id));
	}

	@Test
	public void testWorkspace() {
		setActiveUser(admin);
		Settings.findApplicationSettings().setProperty(ApplicationSettings.WORKSPACES, String.valueOf(false));
		Workspace workspace = new Workspace(admin);
		workspace.name = "Test Workspace";
		workspace.save();
		workspace.addMember(admin, true);
		assertFalse(permissionChecker.listWorkspace(workspace.id));
		assertFalse(permissionChecker.accessWorkspace(workspace.id));
		Settings.findApplicationSettings().setProperty(ApplicationSettings.WORKSPACES, String.valueOf(true));
		assertTrue(permissionChecker.listWorkspace(workspace.id));
		assertTrue(permissionChecker.accessWorkspace(workspace.id));
		assertTrue(permissionChecker.editWorkspace(workspace.id));
		assertTrue(permissionChecker.deleteWorkspace(workspace.id));

		setActiveUser(bob);
		assertTrue(permissionChecker.accessWorkspace(workspace.id));
		assertFalse(permissionChecker.editWorkspace(workspace.id));

		User external = User.find("byEmail", "external@coyo.com").first();
		setActiveUser(external);
		// TODO This part results in an error
		// "play.mvc.results.Forbidden: Access denied". Is this behaviour
		// correct?
		// assertFalse(permissionChecker.listWorkspace(workspace.id));
		// assertFalse(permissionChecker.leaveWorkspace(workspace.id));
		// assertFalse(permissionChecker.accessWorkspace(workspace.id));
		// workspace.addMember(external, false);
		// workspace.save();
		// assertTrue(permissionChecker.listWorkspace(workspace.id));
		// assertTrue(permissionChecker.leaveWorkspace(workspace.id));
		// assertTrue(permissionChecker.accessWorkspace(workspace.id));
		// assertFalse(permissionChecker.editWorkspace(workspace.id));
		//
		setActiveUser(bob);
		workspace.visibility = Workspace.WorkspaceVisibility.PRIVATE;
		workspace.save();
		assertFalse(permissionChecker.listWorkspace(workspace.id));
		assertFalse(permissionChecker.accessWorkspace(workspace.id));
		workspace.save();
		workspace.addMember(bob, true);
		assertFalse(permissionChecker.deleteWorkspace(workspace.id));
	}

	@Test
	public void testInviteExternalUsersToWorkspace() {
		setActiveUser(admin);
		Workspace workspace = WorkspaceHelper.createWorkspace(UserHelper.getAnotherUser(admin));

		admin.role.grants.remove(UserGrant.INVITE_EXTERNALS_TO_WORKSPACE);
		assertFalse(admin.role.hasGrant(UserGrant.INVITE_EXTERNALS_TO_WORKSPACE));
		assertFalse(permissionChecker.inviteExternalsToWorkspace(workspace.id));

		admin.role.grants.add(UserGrant.INVITE_EXTERNALS_TO_WORKSPACE);
		assertTrue(admin.role.hasGrant(UserGrant.INVITE_EXTERNALS_TO_WORKSPACE));
		assertFalse(permissionChecker.inviteExternalsToWorkspace(workspace.id));
		
		workspace.addMember(admin, true);
		assertTrue(permissionChecker.inviteExternalsToWorkspace(workspace.id));
	}

	@Test
	public void testAccessAndDeletePost() {
		setActiveUser(bob);
		WallApp wallApp = WallApp.find("byTitle", "Wall").first();
		Post post = new SimplePost();
		post.wall = wallApp.wall;
		post.author = wallApp.sender;
		post.save();
		assertFalse(permissionChecker.accessPost(post.id));
		assertFalse(permissionChecker.deletePost(post.id));
		setActiveUser(admin);
		assertTrue(permissionChecker.deletePost(post.id));
		assertTrue(permissionChecker.accessPost(post.id));
		User another = User.find("byEmail", "hans@gmail.com").first();
		setActiveUser(another);
		assertFalse(permissionChecker.deletePost(post.id));
	}

	@Test
	public void testSharable() {
		setActiveUser(bob);
		Event event = Event.find("byName", "Event1").first();
		assertFalse(permissionChecker.share(event));
		event.visibility = Event.EventVisibility.PUBLIC;
		event.save();
		assertTrue(permissionChecker.share(event));
		event.visibility = Event.EventVisibility.PRIVATE;
		event.save();
	}

	@Test
	public void testFollow() {
		Event event = Event.find("byName", "Event1").first();
		assertTrue(permissionChecker.follow(event));

		setActiveUser(bob);
		assertFalse(permissionChecker.follow(bob));
		assertTrue(permissionChecker.follow(admin));
	}

	@Test
	public void testSender() {
		setActiveUser(bob);
		Conversation conversation = new Conversation();
		ConversationMember member1 = new ConversationMember();
		member1.conversation = conversation;
		member1.user = bob;
		ConversationMember member2 = new ConversationMember();
		member2.conversation = conversation;
		member2.user = admin;
		conversation.members.add(member1);
		conversation.members.add(member2);
		conversation.save();
		assertFalse(permissionChecker.editSender(conversation.id));
		assertTrue(permissionChecker.accessSender(conversation.id));
		assertTrue(permissionChecker.listSender(conversation.id));
	}

}

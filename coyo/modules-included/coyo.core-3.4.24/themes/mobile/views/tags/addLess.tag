*{
	EXTENDED: trys to load each CSS/LESS file also with name: [originalName].mobile.less	
}*
%{
	if(!_dir) _dir = 'stylesheets';
	_path = _dir + '/' + _arg;
	
	if(less == null) less = [];
	if(!less.contains(_path)) {
		less.add(_path);
		
		// EXTENSION START
		if(theme.Theme.resourceExists(_path + '.mobile.less'))
			less.add(_path + '.mobile');
		// EXTENSION END
	}
}%
#{set less:less /}

// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $comments = $(this);
			
			// expand comments button
			$comments.find('.comments-expand-button').click(function(e) {
				e.preventDefault();
				
				$comments.find('.comment.hide').removeClass('hide');
				$comments.find('.comments-expand').remove();
			});
			
			// enable comment submit button
			$comments.find('form.comment-form .comment-submit').removeClass('disabled');
			
			// create comment form
			$comments.find('form.comment-form').ajaxForm({
				beforeSubmit: function(arr, form, options) {
					if($(form).find("textarea[name='comment.text']").val().trim() == '') {
						return false;
					}
				}, success: function(responseText, statusText, xhr, form) {
					$(form).find("textarea[name='comment.text']").val('');
					$(form).find("textarea[name='comment.text']").trigger('keyup');
					$(form).find(".comment-submit").removeClass('disabled');
					$(form).find(".comment-submit").removeAttr('data-triggered');
					$comments.comments('reload');
				}
			});
			
			// autogrow
			$comments.find("form.comment-form textarea[name='comment.text']").autogrow();
			
			// delete comment
			$comments.find('a.comment-delete').click(function(e) {
				e.preventDefault();
				
				var $a = $(e.currentTarget);
				$.get($a.attr('href'), function() {
					$comments.comments('reload');

					// hook
					$comments.trigger('remove');
				});
			});
		},
		reload : function() {
			$comments = $(this);
			
			$comments.find('.comments-list').load($comments.attr('href'), function(data) {
				$comments.comments('init');
				
				// hook
				$comments.trigger('load');
				
				// init layout tools
				initLayoutTools($comments);
			});
		}
	};

	$.fn.comments = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
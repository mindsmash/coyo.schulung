// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $this = $(this);
			
			// init post form
			$this.wall('initPostForm');
			
			// init posts
			$this.find('.post').each(function(i, el) {
				$this.wall('initPost', $(el));
			});
		},
		initPostForm : function() {
			var $this = $(this);
			var $form = $this.find('.post-new-form');
			
			// check form HTML was loaded
			if($form.size() <= 0) {
				return;
			}
			
			var $message = $form.find('.post-message');
			
			// ajaxify form
			$form.ajaxForm({
				beforeSubmit: function(arr, form, options) {
					if($message.val().trim() == '') {
						$form.find("input[type=submit]").removeClass('disabled');
						$message.focus();
						return false;
					}
					$form.find("input[type=submit]").addClass('disabled');
					$form.find("input[type=submit]").attr('disabled', 'disabled');
				}, success: function(data, status, xhr, form) {
					// add post
					$this.find('.posts').prepend(data);
					$this.wall('initPost', $this.find('.post:first'));
					
					// clear form
					$form.clearForm();
					$form.find('input.file').val('');
					$form.find("input[type=submit]").removeAttr('disabled');
					$form.find("input[type=submit]").removeClass('disabled');
					$this.find('.weblink-clear').trigger('click');
					$form.find('.post-message-wrapper').removeClass('active');
					// reset autogrow
					$form.find('.post-message').css('height', $form.find('.post-message').css('min-height'));
					removedWeblinks = [];
				}
			});
			
			// expand form on first focus
			$message.focus(function() {
				$form.find('.post-message-wrapper').addClass('active');
				
				// make textarea autogrow
				$message.autogrow();
			});
			
			// weblink
			var removedWeblinks = [];
			var $weblink = $form.find('.weblink');
			var $weblinkInfo = $this.find('.weblink-info');
			
			// on each space
			$message.keyup(function(e) {
				if(e.which == 32) {
					preloadWeblinkInfo();
				}
			});
			
			// on paste
			$message.bind('paste', function(e) {
				// wait for text to appear
				setTimeout(function() {
					preloadWeblinkInfo();
				}, 100);
			});
			
			function preloadWeblinkInfo() {
				if($weblink.val().trim() == '') {
					// search for weblink
					// http://...
					var matches = $message.val().match(/(https?:\/\/[a-zA-Z0-9-+&@#\/%?=~_|!:,.;]+)/g);
					if(matches) {
						for(var i in matches) {
							if($.inArray(matches[i], removedWeblinks) < 0) {
								loadWeblinkInfo(matches[i].trim());
								return;
							}
						}
					}
					// www...
					matches = $message.val().match(/(^| )(www\.[a-zA-Z0-9-+&@#\/%?=~_|!:,.;]+)/g);
					if(matches) {
						for(var i in matches) {
							if($.inArray(matches[i], removedWeblinks) < 0) {
								loadWeblinkInfo(matches[i].trim());
								return;
							}
						}
					}
				}
			}
			
			function loadWeblinkInfo(url) {
				// only if link wasn't removed before
				if($.inArray(url, removedWeblinks) < 0) {
					$weblink.val(url);
					$.ajax({
						type: 'GET',
						url: $weblinkInfo.attr('weblinkInfoHref'),
						data: { url: url }, 
						success: function(data) {
							$weblinkInfo.find('.title').html(data.title);
							$weblinkInfo.find('.url').html(data.url);
							$weblinkInfo.find('.description').html(data.description);
							if(data.thumb != null) {
								$weblinkInfo.addClass('thumb');
								$weblinkInfo.find('img').attr('alt', data.title);
								$weblinkInfo.find('img').attr('title', data.title);
								$weblinkInfo.find('img').attr('src', data.thumb);
							}
							$weblink.val(data.url);
						}
					});
				}
			}
			
			$weblinkInfo.find('.weblink-clear').click(function() {
				$weblinkInfo.removeClass('loading');
				$weblinkInfo.hide();
				$weblinkInfo.find('.title').html('');
				$weblinkInfo.find('.url').html('');
				$weblinkInfo.find('.description').html('');
				$weblinkInfo.removeClass('thumb');
				$weblinkInfo.find('img').attr('alt', '');
				$weblinkInfo.find('img').attr('title', '');
				$weblinkInfo.find('img').attr('src', '');
				removedWeblinks.push($weblink.val());
				$weblink.val('');
			});
		},
		addPost : function(data, cssClass) {
			var $this = $(this);
			
			var $newPost = $(data);
			var href = $newPost.attr('data-href');
			
			// if not important, add after existing important posts
			if(!$newPost.hasClass('important-unread') && $this.find('.post.important-unread').size() > 0) {
				$newPost.insertAfter($this.find('.post.important-unread').last());
			} else {
				$this.find('.posts').prepend($newPost);
			}
			
			$this.wall('initPost', $this.find('.post[data-href="'+href+'"]'), cssClass);
		},
		initPost : function(post) {
			var $this = $(this);
			var $post = $(post);

			// comments
			$post.find('.comments').comments();
			
			// like click to update count
			$post.find('.post-options .like').click(function(){
				window.setTimeout(function() { $this.wall('reloadPost', $post); }, 500);
			});
			
			// autoreload
			window.setTimeout(function() { $this.wall('reloadPost', $post); }, 15000);
			
			$post.show();
			
			// init layout tools
			initLayoutTools($post);
		},
		reloadPost : function(post) {
			var $this = $(this);
			var $post = $(post);
			
			$.ajax({
				type: 'GET',
				url: $post.attr('href'),
				success: function(data) {
					var href = $post.attr('href');
					var $newPost = $(data);
					
					// check changed
					if($newPost.attr('data-modified') != $post.attr('data-modified')) {
						$post.replaceWith($newPost);
						$post = $this.find('.post[href="'+href+'"]');
						$this.wall('initPost', $post);
					} else {
						// check again in a few seconds
						window.setTimeout(function() { $this.wall('reloadPost', $post); }, 15000);
					}
				},
				error: function(result) {
					// possibly deleted, so remove it
					if(result.status == 404) {
						$post.remove();
					}
				}
			});
		}
	};

	$.fn.wall = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
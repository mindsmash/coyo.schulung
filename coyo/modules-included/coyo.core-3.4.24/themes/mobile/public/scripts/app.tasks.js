// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $this = $(this),
			$cont = $this.find('.tasks');
			
			// delete button
			$cont.on('click', '.task .task-delete', function(e) {
				e.preventDefault();
				$el = $(e.currentTarget);
				$li = $el.parents('li');
				
				$.ajax({
					url  : $el.attr('href'),
					type : 'DELETE',
					data : {authenticityToken : AUTHENTICITY_TOKEN}
				}).success(function() {
					$li.remove();
				});
			});
			
			// check / uncheck events
			$cont.on('click', '.task-checkbox', function(e) {
				e.preventDefault();
				
				$el = $(e.currentTarget);
				$li = $el.parents('li');
				var completed = !$li.hasClass('task-completed');
				
				$.post($this.data('update-url'), {
					'id': $li.data('task-id'),
					'completed': completed
				});
				
				var $activeList = $this.find('.tasks-list.active');
				var fadeTime = 200;
				if (completed) {
					$activeList.find('.tasks-count').html(parseInt($activeList.find('.tasks-count').html())-1);
					$li.fadeOut(fadeTime, function(){
						$li.addClass('task-completed');
						$cont.find('.tasks-completed').prepend($li);
						$li.fadeIn(fadeTime);
					});
				} else {
					$activeList.find('.tasks-count').html(parseInt($activeList.find('.tasks-count').html())+1);
					$li.fadeOut(fadeTime, function(){
						$li.removeClass('task-completed');
						$cont.find('.tasks-uncompleted').prepend($li);
						$this.tasksApp('sortTasks');
						$li.fadeIn(fadeTime);
					});
				}
			});
		}
	};

	$.fn.tasksApp = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
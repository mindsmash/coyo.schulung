$(document).ready(function(){
	$('.sidebar-switch').click(function(){
		 var height = 50;
	     $(this).children().each(function() {
	         height += $(this).height();
	     });
	     
	     $('body').height(height);
	     $('html').height(height);
	});

	// fix bootstrap bug
	$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
});

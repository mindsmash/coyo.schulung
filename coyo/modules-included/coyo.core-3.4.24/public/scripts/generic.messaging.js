$(document).ready(function() {
	$('.navbar .messaging-toggle').click(function(e) {
		var $messaging = $('.navbar .messaging-items');
		
		$.get($messaging.data('url'), function(data){
			$messaging.find('.injected').remove();
			$messaging.find('.inject-before').before(data);
			initLayoutTools($messaging.find('.injected'));
		});
	});
});
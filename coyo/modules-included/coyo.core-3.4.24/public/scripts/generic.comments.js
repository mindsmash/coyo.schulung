// jQuery plugin
(function($) {
	var timer;
	
	var methods = {
		init : function() {
			var $comments = $(this);
			var $textarea = $comments.find("form.comment-form textarea[name='comment.text']");
			
			// load
			$comments.comments('load');
			
			// create comment form
			$comments.find('form.comment-form').ajaxForm({
				beforeSubmit: function(arr, form, options) {
					if($(form).find("textarea[name='comment.text']").val().trim() == '') {
						return false;
					}
				}, success: function(responseText, statusText, xhr, form) {
					$(form).find("textarea[name='comment.text']").val('').trigger('keydown');
					$comments.attr('data-expanded', 'true');
					$comments.comments('load');
				}
			});
			
			// mention for comments
			var userListHref = $comments.attr('data-userlist');
			if (userListHref) {
				$textarea.autocomplete({
					autoFocus: true,
					minLength: 2,
					source: function(request, response){
						$.get(userListHref, {term: getCurrentUserSearch($textarea.val())}, function(data){
							response(data);
						});
					},
					focus: function() {
						return false;
					},
					select: function( event, ui ) {
						var start = this.value.substr(0, $textarea.getCursorPosition());
						var end = this.value.substr($textarea.getCursorPosition());
						
						var words = start.split(" ");
						words.pop();
						words.push(ui.item.slug)
						
						start = words.join(" ");
						
						this.value = start + end;
						return false;
					},
					search: function(event, ui) {
						var search = getCurrentUserSearch(this.value);
						
						if(!search || search.substring(0,1) != '@') {
							$(this).autocomplete('close');
							return false;
						}
					}
				});
				
				function getCurrentUserSearch(value) {
					value = value.substr(0, $textarea.getCursorPosition());
					
					// old: /@[a-zA-Z0-9-_]*/g
					var words = value.split(" ");
					if(words) {
						return words.pop();
					}
					return false;
				}
			}
			
			// submit on enter
			$textarea.unbind('keypress');
			$textarea.bind('keypress', function(e) {
				if (e.keyCode === 13 && !e.shiftKey && e.target.value !== "") {
					e.preventDefault();
					$(e.currentTarget).parents('form.comment-form').submit();
					resetFileInput();
				} else if (e.keyCode === 13 && !e.shiftKey && e.target.value === "") {
					e.preventDefault();
				}
			});
			
			// autogrow
			$textarea.autogrow();


			// file upload
			var $file = $comments.find("form.comment-form input.file");
			var $fileTrigger = $comments.find("form.comment-form .comment-upload-trigger");

			if ($.browser.msie && $.browser.version < 10) {
				$file.addClass('ie');
				$fileTrigger.addClass('ie');
			}

			$fileTrigger.click(function (e) {
				if ($fileTrigger.hasClass('active')) {
					resetFileInput();
				} else {
					$file.click();
				}
			});

			$file.change(updateFileInput);

			function updateFileInput() {
				if ($file.val().trim() != '') {
					$fileTrigger.addClass('active');
				} else {
					$fileTrigger.removeClass('active');
				}

				$textarea.focus();	
			}
			
			function resetFileInput() {
				$file.val('');
				$file.replaceWith($file = $file.clone(true));
				updateFileInput();
			}


		},
		load : function() {
			var $comments = $(this),
				url = $comments.attr('data-href');

			/**
			 * TODO: Checking for an undefined url here is a quick
			 * fix for the search result page. Otherwise the search
			 * will be executed over and over again. We have to find
			 * out what causes this.
			 */
			if (typeof url !== 'undefined') {
				$.get(url, {expanded: $comments.attr('data-expanded')}, function(data, status, xhr) {
					$comments.find('.comments-list').html(data);
					initLayoutTools($comments.find('.comments-list'), true);
					
					// fuzzy dates
					$comments.find('.date-fuzzy').timeago();
					
					// expand comments button
					$comments.find('.comments-expand-button').click(function(e) {
						e.preventDefault();
						$comments.comments('expand');
					});
					
					// delete comment
					$comments.find('a.comment-delete').click(function(e) {
						e.preventDefault();
						
						var $a = $(e.currentTarget);
						$.ajax({
							url  : $a.attr('href'),
							type : 'DELETE',
							data : {authenticityToken : AUTHENTICITY_TOKEN}
						}).success(function() {
							$a.parents('.comment').remove();
	
							// hook
							$comments.trigger('removed');
						});
					});
					
					// hook
					$comments.trigger('load');
					
					// auto-reload
					clearTimeout(timer);
					timer = window.setTimeout(function(){
						$comments.comments('load');
					}, Coyo.commentsUpdateInterval || 30000);
				});
			}
		},
		expand: function() {
			var $comments = $(this);
			$comments.find('.comment.hide').removeClass('hide');
			$comments.find('.comments-expand').remove();
			$comments.attr('data-expanded', 'true');
		},
		isExpanded: function() {
			var $comments = $(this);
			return $comments.attr('data-expanded') == 'true';
		}
	};

	$.fn.comments = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
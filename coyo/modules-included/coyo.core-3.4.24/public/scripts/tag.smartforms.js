// jQuery plugin
(function($) {
	var onError, onSuccess, afterFormLoad;
	
	var methods = {
		init : function(onErrorCallback, onSuccessCallback, afterFormLoadCallback) {
			var $this = $(this);
			
			onError = onErrorCallback;
			onSuccess = onSuccessCallback;
			afterFormLoad = afterFormLoadCallback;
			
			// load form
			$this.load($this.attr('data-form-url'), function(){
				$this.smartforms('prepareForm');
			});
		},
		prepareForm: function() {
			var $this = $(this);
			var $form = $this.find('form');
			
			$this.removeClass('loading');
			
			initLayoutTools($this, true);
			
			// implement our custom status check
			var callback = function(response, status) {
				// IE workaround: Multipart forms are posted to iframe which
				// returns an mocked xhr object. The xhr mock succeeds always.
				var urlPattern = /^((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)?(:[0-9]{1,5})?((?:\/[\+~%\/.\w_-]*)\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/i;
					isurl =  urlPattern.test(response.responseText);
				
				if((response.status == 200 || status == 'success') && isurl) {
					location.href = response.responseText;
				} else {
					var $response = $(response.responseText);
					
					// check if form is the first element (IE8 handles this differently)
					if('form' == $response.get(0).tagName || 'FORM' == $response.get(0).tagName) {
						$this.html(response.responseText);
						$this.smartforms('prepareForm');
					} else {
						alert('There are validation errors. Please check your form ...');
					}
				}
			}
			
			$form.ajaxForm({
				beforeSerialize:function($form, options){
					if(typeof CKEDITOR !== 'undefined') {
				        for (instance in CKEDITOR.instances) {
				            CKEDITOR.instances[instance].updateElement();
				        }
					}
			        return true; 
			    },
				error: callback,
				success: function(data, status, xhr, form) {
					callback(xhr, status, data);
				}
			});
		}
	};

	$.fn.smartforms = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
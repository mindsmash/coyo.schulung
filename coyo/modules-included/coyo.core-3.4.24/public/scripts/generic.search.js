// ahead search
$(document).ready(function(){
	var search = '';
	var loading = false;
	var timer;
	
	$('#search .search-query').focus(updateSearchTypeahead);
	
	$('#search .search-query').keyup(function(e){
		timer = setTimeout(updateSearchTypeahead, 750);
	});
	
	$('#search .search-query').keydown(function(e){
		// enter
		if(e.keyCode == 13) {
			var $hover = $('#search .search-typeahead .result.hover');
			if($hover.size() > 0) {
				e.preventDefault();
				location.href = $hover.data('href');
			}
		}
		
		// up and down arrows
		if (e.keyCode == 40 || e.keyCode == 38) {
			e.preventDefault();
			
			var total = $('#search .search-typeahead .result').size();
			var current = parseInt($('#search .search-typeahead .result.hover').data('index'));
			if(isNaN(current)) {
				current = 0;
			}
			$('#search .search-typeahead .result').removeClass('hover');
			var next = (e.keyCode == 40) ? (current+1) : (current-1);
			if(next < 1) next = 1;
			if(next > total) next = total;
			$('#search .search-typeahead .result[data-index=' + next + ']').addClass('hover');
			return;
		}
		
		clearTimeout(timer);
	});
	
	$('#search .search-query').blur(function(){
		// delay so that links stay clickable
		window.setTimeout(function() { $('#search').removeClass('open'); }, 300);
	});
	
	// hover
	$('#search .search-typeahead').on('mouseenter', '.result', function(){
		$('#search .search-typeahead .result').removeClass('hover');
		$(this).addClass('hover');
	});
	
	// click
	$('#search .search-typeahead').on('click', '.result', function(e){
		e.preventDefault();
		location.href = $('#search .search-typeahead .result.hover').data('href');
	});
	
	function updateSearchTypeahead() {
		var value = $('#search .search-query').val();

		if(!loading && value != $('#search .search-query').attr('placeholder') && value.length >= 1 && search != $('#search .search-query').val()) {
			search = value;
			loading = true;
			
			$('#search .search-typeahead li:not(.permanent)').remove();
			$('#search .search-typeahead').addClass('loading');
			$('#search').addClass('open');
			
			// load results via ajax
			$.get($('#search').attr('data-ahead'), {searchString:value}, function(data, status){
				if(data.trim() == '') {
					$('#search .search-typeahead').addClass('empty');
					$('#search .search-typeahead').removeClass('loading');
				} else {
					$('#search .search-typeahead').prepend(data);
					$('#search .search-typeahead').removeClass('empty');
					$('#search .search-typeahead').removeClass('loading');
					
				}
				loading = false;
				
				// highlight search results
				$('#search .search-typeahead').highlight(value.split(' '));
			});
		} else if(!loading && value.length == 0) {
			$('#search').removeClass('open');
		} else if(!loading && search == $('#search .search-query').val()) {
			$('#search').addClass('open');
		}
	}
});
function coyoForEach(matchClass,fn) {
	var elems = document.getElementsByTagName('*'), i;
	for (i in elems) {
		if((" " + elems[i].className + " ").indexOf(" " + matchClass + " ") > -1) {
			fn(elems[i]);
		}
	}
}
window.onload = function () {
	// like button
	coyoForEach('coyo-like', function(el){
		var width = el.getAttribute('data-width');
		var height = el.getAttribute('data-height');
		if(width == null) width = '100%';
		if(height == null) height = '24px';
		
		el.innerHTML = '<iframe src="@@{Embeddables.like()}?likelist=' + el.getAttribute('data-likelist') + '&url=' + document.URL + '" scrolling="no" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:' + width + '; height:' + height + ';"></iframe>';
	});
	
	// wall
	coyoForEach('coyo-wall', function(el){
		var width = el.getAttribute('data-width');
		var height = el.getAttribute('data-height');
		if(width == null) width = '100%';
		if(height == null) height = '100%';
		
		el.innerHTML = '<iframe src="@@{Embeddables.wall()}?id=' + el.getAttribute('data-id') + '&url=' + document.URL + '" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:' + width + '; height:' + height + ';"></iframe>';
	});
	
	// comments
	coyoForEach('coyo-comments', function(el){
		var width = el.getAttribute('data-width');
		var height = el.getAttribute('data-height');
		if(width == null) width = '100%';
		if(height == null) height = '100%';
		
		el.innerHTML = '<iframe src="@@{Embeddables.comments()}?url=' + document.URL + '" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:' + width + '; height:' + height + ';"></iframe>';
	});
	
	// follow button
	coyoForEach('coyo-follow', function(el){
		var width = el.getAttribute('data-width');
		var height = el.getAttribute('data-height');
		if(width == null) width = '100%';
		if(height == null) height = '24px';
		
		el.innerHTML = '<iframe src="@@{Embeddables.follow()}?id=' + el.getAttribute('data-id') + '" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:' + width + '; height:' + height + ';"></iframe>';
	});
	
	// share button
	coyoForEach('coyo-share', function(el){
		var width = el.getAttribute('data-width');
		var height = el.getAttribute('data-height');
		if(width == null) width = '100%';
		if(height == null) height = '24px';
		
		el.innerHTML = '<iframe src="@@{Embeddables.share()}?url=' + document.URL + '" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:' + width + '; height:' + height + ';"></iframe>';
	});
};
(function($) {

	function handleMenuItems() {
		var $menuItems = $('.navbar .nav.workspaces>li:not(.dropdown)'),
			$hidden = $('<ul/>'),
			$moreItem = $('.navbar .nav.workspaces li.dropdown'),
			moreItemWidth = $moreItem.outerWidth(),
			width = 0,
			count = 0,
			maxWidth = $('.navbar .container').innerWidth() - ($('.navbar .brand').outerWidth() + $('.navbar .nav.pull-right').outerWidth()) - 20;
		
		$moreItem.hide().find('ul').empty();
		
		$menuItems.each(function() {
			var $item = $(this);
			width += $item.width();
			count++;
			$item.css('visibility', 'visible');
			if (!((count >= $menuItems.size() && width < maxWidth) || (width < (maxWidth - moreItemWidth)))) {
				$hidden.append($item.clone());
				$item.hide();
			} else {
				$item.show();
			}
		});
		
		var $hiddenItems = $('li', $hidden);
		if ($hiddenItems.size() > 0) {
			$moreItem.find('ul').append($hiddenItems.show());
			$moreItem.show();
			if ($moreItem.find('li.active').size() > 0) {
				$moreItem.addClass('active');
			}
		}
	}
	
	$(function() {
		handleMenuItems();
		$(window).on('resize', handleMenuItems);
	});
})(jQuery);
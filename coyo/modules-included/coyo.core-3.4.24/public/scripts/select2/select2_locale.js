/**
 * Select2 translations
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () {return message('select2.noMatches');},
        formatInputTooShort: function (input, min) {var n = min - input.length; return message('select2.inputTooShort', n);},
        formatInputTooLong: function (input, max) {var n = input.length - max; message('select2.inputTooLong', n);},
        formatSelectionTooBig: function (limit) {return message('select2.selectionTooBig', limit);},
        formatLoadMore: function (pageNumber) {return message('select2.loadMore');},
        formatSearching: function () {return message('select2.searching');},
        formatMatches: function (matches) {return (matches > 1) ? message('select2.matches', matches) : message('select2.match');}
    });
})(jQuery);

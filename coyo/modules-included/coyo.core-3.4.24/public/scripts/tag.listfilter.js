//jQuery plugin
(function($) {
	var filtering = false;
	var typingTimer;
	var doneTypingInterval = 250;
	
	var methods = {
		init : function() {
			var $field = $(this);
			var $list = $($field.attr('data-list'));
			var $by = $($field.attr('data-filter-by'));
			
			if($list.length != 1) {
				throw 'list filter found 0 or more than 1 list';
			}
			
			if($by.length > 0) {
				$by.change(function(e){
					$field.listfilter('filter');
				});
			}
			
			$field.keyup(function(e) {
				typingTimer = setTimeout(function() {
					$field.listfilter('filter');
				}, doneTypingInterval);
			});
			
			$field.keydown(function(e) {
				clearTimeout(typingTimer);
			});
		},
		filter: function() {
			// do not filter if already filtering
			if(filtering) {
				return;
			}
			
			filtering = true;
			
			var $field = $(this);
			var $list = $($field.attr('data-list'));
			
			// find the li[attr] to filter by
			var by = $field.attr('data-filter-by');
			
			// check if attribute or selector for select field
			if(by.indexOf('data-') < 0) {
				// get value from the select field
				by = $(by).val();
			}
			
			var text = $field.val();
			
			// check for filter text
			if(text.trim() == '') {
				$list.removeClass('filtered');
				$list.children('li').removeClass('filtered');
				filtering = false;
				$(document).trigger('list-filter');
				return;
			}
			
			$list.addClass('filtered');
			
			// lower case
			text = text.trim().toLowerCase();
			
			// hide all items
			$list.children('li').addClass('filtered');
			
			// unhide
			var re = RegExp(text ,"i"); 
			$list.children('li').filter(function(){
				return re.test($(this).attr(by));
			}).removeClass('filtered');
			
			// trigger filter event
			$(document).trigger('list-filter');
			
			filtering = false;
		}
	}

	$.fn.listfilter = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
function forms_update_h2(formElement, modal){
	var text = modal.find('.forms-label-input').val();
	
	if (text === '') {
		formElement.remove();
	}
	
	formElement.find('h2').text(text);
}

function forms_resetModal_h2(formElement, modal){
	modal.find('.forms-label-input').val(formElement.find('h2').text());
}
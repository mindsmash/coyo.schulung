// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $this = $(this);
			
			// init post form
			$this.wall('initPostForm');
		},
		initPostForm : function() {
			var $this = $(this);
			var $form = $this.find('.post-new-form');
			
			// check form HTML was loaded
			if($form.size() <= 0) {
				return;
			}
			
			var $message = $form.find('.post-message');
			
			// ajaxify form
			$form.ajaxForm({
				beforeSubmit: function(arr, form, options) {
					if($message.val().trim() == '') {
						$form.find("input[type=submit]").removeClass('disabled');
						$message.focus();
						$message.addClass('error-empty');
						return false;
					}
					$message.removeClass('error-empty');
					$form.find("input[type=submit]").addClass('disabled');
					$form.find("input[type=submit]").attr('disabled', 'disabled');
				}, success: function(data, status, xhr, form) {
					// add post
					$this.wall('addPost', data);
					
					// clear form
					$form.clearForm();
					
					// reset file input
					resetFileInput();
					
					$form.find("input[type=submit]").removeAttr('disabled');
					$form.find("input[type=submit]").removeClass('disabled');
					$this.find('.weblink-clear').trigger('click');
					$form.find('.post-message-wrapper').removeClass('active');
					// reset autogrow
					$form.find('.post-message').css('height', $form.find('.post-message').css('min-height'));
					$form.find("input[type=checkbox]").removeAttr('checked');
					removedWeblinks = [];
					updateFileInput();
				}
			});
			
			// expand form on first focus
			$message.focus(function() {
				$form.find('.post-message-wrapper').addClass('active');
				
				// make textarea autogrow
				$message.autogrow();
			});
			
			// submit on CTRL + ENTER
			$message.keydown(function (e) {
				if (e.ctrlKey && e.keyCode == 13) {
					e.preventDefault();
					$form.submit();
				}
			});
			
			// file input
			var $file = $form.find('input.file');
			var $fileTrigger = $form.find('.file-trigger');
			if($.browser.msie && $.browser.version < 10) {
				$file.addClass('ie');
				$fileTrigger.addClass('ie');
			}
			
			$fileTrigger.click(function(e) {
				if($fileTrigger.hasClass('active')) {
					resetFileInput();
				} else {
					$file.click();
				}
			});
			
			function resetFileInput() {
				$form.find('input.file').val('');
				$file.replaceWith( $file = $file.clone(true) );
				updateFileInput();
			}
			
			function updateFileInput() {
				if($file.val().trim() != '') {
					$fileTrigger.addClass('active');
				} else {
					$fileTrigger.removeClass('active');
				}
			}
			
			$file.change(updateFileInput);
			updateFileInput();

			function getCurrentUserSearch(value) {
				value = value.substr(0, $message.getCursorPosition());
				
				// old: /@[a-zA-Z0-9-_]*/g
				var words = value.split(" ");
				if(words) {
					return words.pop();
				}
				return false;
			}
			
			$message.autocomplete({
				autoFocus: true,
				minLength: 2,
				source: function(request, response){
					$.get($this.attr('userListHref'), {term: getCurrentUserSearch($message.val())}, function(data){
						response(data);
					});
				},
				focus: function() {
					return false;
				},
				select: function( event, ui ) {
					var start = this.value.substr(0, $message.getCursorPosition());
					var end = this.value.substr($message.getCursorPosition());
					
					var words = start.split(" ");
					words.pop();
					words.push(ui.item.slug);
					
					start = words.join(" ");
					
					this.value = start + end;
					return false;
				},
				search: function(event, ui) {
					var search = getCurrentUserSearch(this.value);
					
					if(!search || search.substring(0,1) != '@') {
						$(this).autocomplete('close');
						return false;
					}
				}
			});
			
			// weblink
			var removedWeblinks = [];
			var $weblink = $form.find('.weblink');
			var $weblinkInfo = $this.find('.weblink-info');
			
			// on each space
			$message.keyup(function(e) {
				if(e.which == 32) {
					preloadWeblinkInfo();
				}
			});
			
			// on paste
			$message.bind('paste', function(e) {
				// wait for text to appear
				setTimeout(function() {
					preloadWeblinkInfo();
				}, 100);
			});
			
			function preloadWeblinkInfo() {
				if($weblink.val().trim() == '') {
					// search for weblink
					// http://...
					var matches = $message.val().match(/(https?:\/\/[a-zA-Z0-9-+&@#\/%?=~_|!:,.;]+)/g);
					if(matches) {
						for(var i in matches) {
							if($.inArray(matches[i], removedWeblinks) < 0) {
								loadWeblinkInfo(matches[i].trim());
								return;
							}
						}
					}
					// www...
					matches = $message.val().match(/(^| )(www\.[a-zA-Z0-9-+&@#\/%?=~_|!:,.;]+)/g);
					if(matches) {
						for(var i in matches) {
							if($.inArray(matches[i], removedWeblinks) < 0) {
								loadWeblinkInfo(matches[i].trim());
								return;
							}
						}
					}
				}
			}
			
			function loadWeblinkInfo(url) {
				// only if link wasn't removed before
				if($.inArray(url, removedWeblinks) < 0) {
					$weblink.val(url);
					$weblinkInfo.addClass('loading');
					$weblinkInfo.show();
					$.ajax({
						type: 'GET',
						url: $weblinkInfo.attr('weblinkInfoHref'),
						data: { url: url }, 
						success: function(data) {
							$weblinkInfo.find('.title').text(data.title);
							$weblinkInfo.find('.url').text(data.url);
							$weblinkInfo.find('.description').text(data.description);
							if(data.thumb != null) {
								$weblinkInfo.addClass('thumb');
								$weblinkInfo.find('img').attr('alt', data.title);
								$weblinkInfo.find('img').attr('title', data.title);
								$weblinkInfo.find('img').attr('src', data.thumb);
							}
							$weblink.val(data.url);
							$weblinkInfo.removeClass('loading');
						}
					});
				}
			}
			
			$weblinkInfo.find('.weblink-clear').click(function() {
				$weblinkInfo.removeClass('loading');
				$weblinkInfo.hide();
				$weblinkInfo.find('.title').text('');
				$weblinkInfo.find('.url').text('');
				$weblinkInfo.find('.description').text('');
				$weblinkInfo.removeClass('thumb');
				$weblinkInfo.find('img').attr('alt', '');
				$weblinkInfo.find('img').attr('title', '');
				$weblinkInfo.find('img').attr('src', '');
				removedWeblinks.push($weblink.val());
				$weblink.val('');
			});
		},
		addPost : function(data, cssClass) {
			var $this = $(this);
			
			var $newPost = $(data);
			var href = $newPost.attr('data-href');

			// if not important, add after existing important posts
			if(!$newPost.hasClass('important-unread') && $this.find('.post.important-unread').size() > 0 && !$this.is('#userWall')) {
				$newPost.insertAfter($this.find('.post.important-unread').last());
			} else {
				$this.find('.posts').prepend($newPost);
			}
			
			$this.wall('loadPost', $this.find('.post[data-href="'+href+'"]'), cssClass);
		},
		loadPost : function($post, cssClass) {
			var $this = $(this),
				url = $post.attr('data-href');
			
			/* FIXME for some reason this method is called twice sometimes with an empty post.
			 * Therefore we have to check for undefined. */
			if (typeof url !== 'undefined') {
				$.ajax({
					type: 'GET',
					url: url,
					success: function(data) {
						var href = $post.attr('data-href');
						$post.replaceWith($(data));
						$post = $this.find('.post[data-href="' + href + '"]');
						
						$this.wall('initPost', $post, cssClass);
					},
					error: function(result) {
						// possibly deleted, so remove it
						if(result.status == 404) {
							$post.remove();
						}
					}
				});
			}
		},
		initPost : function($post, cssClass) {
			var $this = $(this);

			if(!$post.hasClass('post')) {
				return;
			}
			
			// add custom css class
			if (typeof cssClass != 'undefined') {
				$post.addClass(cssClass);
			}
			
			// bind delete post trigger
			$post.find('a.post-delete').click(function(e) {
				e.preventDefault();
				
				var $a = $(e.currentTarget);
				$.ajax({
					url  : $a.attr('href'),
					type : 'DELETE',
					data : {authenticityToken : AUTHENTICITY_TOKEN}
				}).success(function() {
					$post.remove();
				});
			});
			
			// load fuzzy time
			$post.find('.date-fuzzy').timeago();
			
			// comments
			$post.find('.comments').comments();
			
			// comment link
			function initCommentLink() {
				if($post.find('.comment').length == 0 && $post.find('.comment-form textarea').val() == '') {
					$post.find('.comment-form').hide();
					$post.find('.comment-button').show();
					
					// show form on button click
					$post.find('.comment-button').click(function(e) {
						e.preventDefault();
						$(e.currentTarget).hide();
						$post.find('.comment-form').show();
						$post.find('.comment-form textarea').focus();
					});
				} else {
					$post.find('.comment-form').show();
					$post.find('.comment-button').hide();
					$post.find('.comment-form textarea').unbind('blur');
				}
			}
			initCommentLink();
			
			// update comment link on comment load/remove
			$post.find('.comments').bind('load', initCommentLink);
			$post.find('.comments').bind('removed', initCommentLink);
			
			// toggle sticky
			$post.find('a.post-sticky').click(function(e){
				e.preventDefault();
				
				var $a = $(e.currentTarget);
				$.post($a.attr('href'), function() {
					$this.wall('loadPost', $post);
				});
			});
			
			// toggle follow
			$post.find('a.post-follow-toggle').click(function(e){
				e.preventDefault();
				
				var $a = $(e.currentTarget),
					url = $a.attr('href');
				
				$.post(url, function() {
					$this.wall('loadPost', $post);
				});
			});
			
			// toggle flagged
			$post.find('.post-flagged-toggle').click(function(e){
				e.preventDefault();
				
				var $a = $(e.currentTarget);
				$.post($a.attr('href'), function() {
					$post.toggleClass('flagged');
				});
			});
			
			// toggle read
			$post.find('.post-read-toggle').click(function(e){
				e.preventDefault();
				
				var $a = $(e.currentTarget);
				$.post($a.attr('href'), function() {
					$post.toggleClass('important-unread');
				});
			});
			
			// init layout tools
			initLayoutTools($post);
		}
	};

	$.fn.wall = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);

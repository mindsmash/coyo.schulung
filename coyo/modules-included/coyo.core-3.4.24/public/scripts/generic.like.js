$(document).ready(function () {
	// handle like clicks
	$('a.like:not([data-like-button])').live('click', function (e) {
		e.preventDefault();

		var a = $(e.currentTarget);
		$.post(a.attr('href'), function (data) {
			$('a.like[data-uid="' + a.attr('data-uid') + '"]').each(function (i, el) {
				applyLikeLink($(el), data);
			});
			$('span.likelist[data-uid="' + a.attr('data-uid') + '"]').each(function (i, el) {
				applyLikelist($(el), data);
			});
		});
	});

	function updateLikeLink(el) {
		var $a = $(el);
		$a.addClass('load');
		$.getJSON($a.attr('data-status-href'), function (data) {
			applyLikeLink($a, data);
			$a.removeClass('load');
		});
	}

	function applyLikeLink($a, data) {
		if (data.status == true) {
			$a.find('.text-unlike').show();
			$a.find('.text-like').hide();
		} else {
			$a.find('.text-unlike').hide();
			$a.find('.text-like').show();
		}
	}

	function updateLikelist(el) {
		var $span = $(el);
		$span.addClass('load');
		$.getJSON($span.attr('data-href'), function (data) {
			applyLikelist($span, data);
			$span.removeClass('load');
		});
	}

	function applyLikelist($span, data) {
		$span.html(data.likelist);
		if (data.likelist == '') {
			$span.addClass('empty');
		} else {
			$span.removeClass('empty');
		}

		// tooltip
		$span.find('.tip').tooltip({
			container: 'body'
		});
	}

	window.setTimeout(function () {
		$('span.likelist').each(function (i, el) {
			$eventService.on('LikeEvent', function (event, data) {
				if (new RegExp("id=" + data.id + "&clazz=" + data.clazz + ".*").test(el.getAttribute("data-href"))) {
					updateLikelist(el);
				}

			});
		});

		$('a.like').each(function (i, el) {
			$eventService.on('LikeEvent', function (event, data) {
				if (new RegExp("id=" + data.id + "&clazz=" + data.clazz + ".*").test(el.getAttribute("data-href"))) {
					updateLikeLink(el);
				}
			});
		});
	}, 5000);
});
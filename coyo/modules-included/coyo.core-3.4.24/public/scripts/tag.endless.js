//jQuery plugin
(function($) {
	var methods = {
		init : function(pageSize) {
			var $this = $(this);

			// check if more items in list than page size
			if($this.children('li').length < pageSize) {
				return;
			}
			
			var max = pageSize;
			
			// only show first page
			$this.children('li:not(.filtered):not(.force-hide)').show();
			$this.children('li:not(.filtered):not(.force-hide):gt('+max+')').hide();
			
			function check() {
				if ($(window).scrollTop() >= $(document).height() - $(window).height() - 200) {
					$this.addClass('loading');
					
					max += pageSize;
					$this.endless('update', max);
				}
			}
			
			// trigger more on scroll
			$(window).scroll(check);
			
			// restart scrolling after filtering
			$(document).bind('list-filter', function(){
				if($this.hasClass('filtered')) {
					$this.children('li').css('display','');
				} else {
					// restart endless
					$this.children('li:not(.filtered):not(.force-hide)').show();
					$this.children('li:not(.filtered):not(.force-hide):gt('+max+')').hide();
				}
			});
			
			// trigger once on load
			check();
		},
		update: function(max) {
			var $this = $(this);
			
			if(!$this.hasClass('filtered')) {
				// show until max
				$this.children('li:not(.filtered):not(.force-hide):lt('+max+')').show().trigger('endless-show');
				
				$this.removeClass('loading');
			}
		}
	};

	$.fn.endless = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
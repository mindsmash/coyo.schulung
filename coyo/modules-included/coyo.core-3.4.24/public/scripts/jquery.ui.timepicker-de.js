/* custom by mindsmash */
$.timepicker.regional['de'] = {
	timeOnlyTitle: 'Wähle Zeit',
	timeText: 'Zeit',
	hourText: 'Stunde',
	minuteText: 'Minute',
	secondText: 'Sekunde',
	millisecText: 'Millisekunde',
	currentText: 'Jetzt',
	closeText: 'Fertig',
	ampm: false
};
$.timepicker.setDefaults($.timepicker.regional['de']);
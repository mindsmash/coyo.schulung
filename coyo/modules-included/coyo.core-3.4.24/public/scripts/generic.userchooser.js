//jQuery plugin
(function($) {
	var template;
	var timer;
	
	var methods = {
		init : function() {
			var $this = $(this);
			var $btn = $this.find('.userchooser-btn');
	
			// mustache template
			template = Mustache.compile($('#userchooserTemplate').html());
			
			// init popover
			$btn.popover({
				html: true,
				title: function(){
					return $this.find('.userchooser-popover-title').html();
				},
				content: function(){
					return $this.find('.userchooser-popover-content').html();
				},
				trigger: 'manual',
				animation: false,
				container: 'body'
			});
			
			// bind open
			$btn.click(function(e){
				e.preventDefault();
				if($btn.hasClass('active')) {
					$this.userchooser('closePopover');
				} else {
					$this.userchooser('openPopover');
				}
			});
		},
		getPopover : function(){
			return $('.popover-userchooser[data-id='+$(this).attr('id')+']');
		},
		update : function(){
			var $this = $(this);
			var $popover = $this.userchooser('getPopover');
			var $search = $popover.find('.search-query');
			
			var searchTerm = $search.val();
			var lastSearchTerm = $this.data('last-search');
			
			if(!$popover.hasClass('loading') && searchTerm != $search.attr('placeholder') && (searchTerm.length == 0 || lastSearchTerm != searchTerm)) {
				$this.data('last-search', searchTerm);
				$popover.addClass('loading');
				
				var $list = $popover.find('.userchooser-list').html('');
				
				// load
				$.ajax({
					url: $this.data('load-url'),
					type: 'POST',
					data: {
						length: 2000,
						selected: $this.find('.userchooser-value').val(),
						term: searchTerm
					},
					success: function(data){
						// if empty
						if(data.length == 0) {
							$popover.find('.userchooser-empty').show();
							$list.hide();
						} else {
							$popover.find('.userchooser-empty').hide();
							$list.show();
							$.each(data, function(i, o){
								o.active = $this.userchooser('isActive', o.id);
								$list.append(template(o));
							});
						}
						
						$popover.removeClass('loading');
					},
					error: function(jqXHR, textStatus, errorThrown){
						$popover.find('.userchooser-empty').show();
						$list.hide();
						throw "could not load users for userchooser: " + errorThrown + " ["+textStatus+"]";
					}
				});
			}
		},
		openPopover : function(){
			var $this = $(this);
			var $btn = $this.find('.userchooser-btn');
			
			clearTimeout(timer);
			
			$btn.addClass('active');
			$btn.popover('show');
			
			// add custom css class
			$('.popover').addClass('popover-userchooser').attr('data-id', $this.attr('id'));
			$btn.popover('show'); // readjust
			
			// init popover
			window.setTimeout(function(){
				var $popover = $this.userchooser('getPopover');
				
				// bind close
				$popover.on('click', '.userchooser-popover-close', function(e){
					e.preventDefault();
					$this.userchooser('closePopover');
				});
				
				// click on anything outside the popover
				$(document).click(function(e){
					if($btn.hasClass('active') && !$(e.target).hasClass('userchooser-btn') && !$.contains($popover[0], e.target)) {
						$this.userchooser('closePopover');
					}
				});
				
				// bind user click
				$popover.on('click', '.userchooser-user', function(e){
					e.preventDefault();
					var $a = $(this);
					if($this.userchooser('isActive', $a.data('id'))) {
						$this.userchooser('deactivate', $a.data('id'));
					} else {
						$this.userchooser('activate', $a.data('id'));
					}
					
					// update btn label
					$this.find('.userchooser-count').html($this.find('.userchooser-value option[value!=""]').length);
					$popover.find('.userchooser-count').html($this.find('.userchooser-value option[value!=""]').length);
					
					// trigger jq event
					$this.trigger('userchooserChange');
				});
				
				// bind search clear button
				$popover.on('click', '.search-clear', function(){
					var $search = $('.popover .search-query');
					if($search.val() != '') {
						$search.val('').focus();
						$this.userchooser('update');
					} else {
						$search.focus();
					}
				});
				
				// bind on search term entry
				var $search = $popover.find('.search-query');
				
				// modal fix
				$(document).off('focusin.modal');
				$search.click(function(){
					$search.focus();
				});
				
				$search.keyup(function(e) {
					timer = setTimeout(function() {
						$this.userchooser('update');
					}, 500);
				});
				
				$search.keydown(function(e) {
					// do not submit form on enter
					if (e.keyCode == 13) {
						e.preventDefault();
					}
					clearTimeout(timer);
				});
				
				// focus on open
				$search.focus();
				
				$this.userchooser('update');
			}, 200);
		},
		closePopover : function(){
			var $this = $(this);
			var $btn = $this.find('.userchooser-btn');
			var $popover = $this.userchooser('getPopover');
			
			$btn.removeClass('active');
			
			// remove manually
			$popover.remove();
		},
		isActive : function(id) {
			return $(this).find('.userchooser-value option[value='+id+']').length > 0;
		},
		activate : function(id) {
			var $this = $(this);
			
			// check max first
			var max = parseInt($this.data('max'));
			if(max > 0 && $this.find('.userchooser-value option[value!=""]').length >= max) {
				alert($this.data('max-error'));
				return;
			}
			
			$this.userchooser('getPopover').find('.userchooser-user[data-id='+id+']').addClass('btn-primary');
			$this.find('.userchooser-value').append('<option value="' + id + '" selected>' + id + '</option>');
			$this.find('.userchooser-value option[value=""]').remove();
		},
		deactivate : function(id) {
			var $this = $(this);
			$this.userchooser('getPopover').find('.userchooser-user[data-id='+id+']').removeClass('btn-primary');
			$this.find('.userchooser-value option[value='+id+']').remove();
			if ($this.data('empty-list') && $this.find('.userchooser-value option').size() == 0) {
				// use empty list item to "return" null for empty lists
				$this.find('.userchooser-value').append('<option value="" selected></option>');
			}
		}
	};

	$.fn.userchooser = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
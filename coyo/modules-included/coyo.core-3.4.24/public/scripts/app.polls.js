// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $this = $(this);
			$this.poll('load');
		},
		load : function() {
			var $this = $(this);
			
			$this.addClass('loading');
			$this.load($this.data('load-url'), function() {
				$this.removeClass('loading');
				$this.find('.tip').tooltip();
				
				if($this.data('canvote') === 'false') {
					return;
				}
			
				// hover coloring
				$this.find('.answer-bar.ok').mouseenter(function(e){
					$(e.currentTarget).addClass('progress-success');
				});
				$this.find('.answer-bar.ok').mouseleave(function(e){
					$(e.currentTarget).removeClass('progress-success');
				});
				
				// vote action
				$this.find('.answer-bar.ok').click(function(e){
					$.ajax({
						type: 'POST',
						url: $(e.currentTarget).data('vote-url'),
						success: function() {
							$this.poll('load');
						},
						error: function() {
							// ignored
						}
					});
				});
				
				// unvote action
				$this.find('.answer-bar.answered').click(function(e){
					$.ajax({
						type: 'POST',
						url: $(e.currentTarget).data('unvote-url'),
						success: function() {
							$this.poll('load');
						},
						error: function() {
							// ignored
						}
					});
				});
			});
		}
	};

	$.fn.poll = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
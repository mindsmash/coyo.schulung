/*
Custom jQuery scrollTo plugin based on

Author: David Vogeleer
Site: http://www.individual11.com/
Description: animated scroll to anchor

Version: 0.3
*/

(function($) {
	"use strict";
	$.fn.scrollTo = function(options) {
		
		var settings = {
			offset : 0,       //an integer allowing you to offset the position by a certain number of pixels. Can be negative or positive
			speed  : 'slow',   //speed at which the scroll animates
			easing : null     //easing equation for the animation. Supports easing plugin as well (http://gsgd.co.uk/sandbox/jquery/easing/)
		};

		if (options) {
			$.extend(settings, options);
		}
		
		return this.each(function(i, el){
			$(el).click(function(e){
				var idToLookAt;
				if ($(el).attr('href').match(/#/) !== null) {
					e.preventDefault();
					idToLookAt = $(el).attr('href');
					if ($(idToLookAt).length > 0) {
						$('html,body').stop().animate({scrollTop: $(idToLookAt).offset().top + settings.offset}, settings.speed, settings.easing);
					}
				}
			});
		});
	};
})(jQuery);
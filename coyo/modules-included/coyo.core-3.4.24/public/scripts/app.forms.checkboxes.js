function forms_update_checkboxes(formElement, modal){
	var controls = formElement.find('.render-checkboxes');
	controls.find('label').remove();

	jQuery.fn.reverse = [].reverse;
	modal.find('.forms-label-option').reverse().each(function(){
		controls.prepend('<label class="checkbox"><input type="checkbox" data-id="' + $(this).attr('data-id') + '"/>' + $(this).val() + '</label>');
	});
	controls.append(controls.find('p.help-block'));
}

function forms_addCheckbox(text, before, id){	
	text = text.replace(/\"/g, "&quot;");
	$('<div><input type="text" class="forms-label-option" data-id="' + id + '" value="' + text + '"/><span class="icon-remove"></span></div>').insertBefore(before);
	
	$('.multiple-settings .icon-remove').unbind('click');
	$('.multiple-settings .icon-remove').click(function(){
		$(this).parent().remove();
		
		if($('.multiple-settings .icon-remove').length === 1){
			$('.multiple-settings .icon-remove').remove();
		}
	});
}

function forms_resetModal_checkboxes(formElement, modal){
	var clearfix = modal.find('.clearfix');
	modal.find('.forms-label-option').parent().remove();
	formElement.find('label.checkbox').each(function(){
		forms_addCheckbox($(this).text(), clearfix, $(this).find('input').attr('data-id'));
	});
}

$(document).ready(function(){
	$('.forms-add-checkbox').click(function(){
		forms_addCheckbox('', $(this).parent().find('.clearfix'));
	});
});

function forms_getOptionsData_checkboxes(formElement){
	var options = [];
	formElement.find('.render-checkboxes input[type=checkbox]').each(function(i){
		var checkboxEl = $(this),
			label = checkboxEl.parent().text(),
			idAttr = checkboxEl.attr('data-id'),
			checkboxId = null;
		
		if (typeof idAttr !== 'undefined' && idAttr !== null && idAttr.trim() !== '') {
			checkboxId = parseInt(idAttr);
		}
		
		options.push({id:checkboxId, label: label, fieldOrder: i});
	});
	
	return options;
}
function fixIE() {
	// all IE
	if (!$.browser.msie) {
		return;
	}
	
	$(document).ready(function(){
		$('body').addClass('msie');
	});
	
	// define trim()
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}
	
	// define console
	if (!window.console) window.console = {};
	if (!window.console.log) window.console.log = function () { };
	
	// before IE9
	if(parseInt($.browser.version, 10) >= 9) {
		return;
	}
	
	$(document).ready(function(){
		$('a.btn[data-loading-text]').click(function (e) {
			var url = $(e.currentTarget).attr('href');
			var connector = '?';
			if(url.indexOf('?') >= 0) {
				connector = '&';
			}
			
			top.location.href = url + connector + 'referrer=' + document.location.href;
		});
	});
	
	if(parseInt($.browser.version, 10) == 8) {
		$(document).ready(function(){
			$('body').addClass('msie8');
		});
	}
	
	// before IE8
	if(parseInt($.browser.version, 10) >= 8) {
		return;
	}
	
	if(typeof console === "undefined") {
		console = {
			log: function() { },
			debug: function() { },
			info: function() { },
			warn: function() { },
			error: function() { }
		};
	}
}

fixIE();
$(document).ready(function(){
	// follow buttons
	$(document).on('click', 'a[data-follow]', function(e){
		e.preventDefault();
		
		var $this = $(this);
		
		$.post($this.attr('href'), function(){
			$('a[data-follow=' + $this.attr('data-follow') + ']').toggle();
		});
	});
	$(document).on('mouseover', 'a.unfollow[data-follow]', function(e){
		if($(this).hasClass('btn')) {
			$(this).addClass('btn-danger').removeClass('btn-primary');
		}
		$(this).find('i').removeClass('icon-ok').addClass('icon-remove');
	});
	$(document).on('mouseleave', 'a.unfollow[data-follow]', function(e){
		if($(this).hasClass('btn')) {
			$(this).addClass('btn-primary').removeClass('btn-danger');
		}
		$(this).find('i').removeClass('icon-remove').addClass('icon-ok');
	});
});
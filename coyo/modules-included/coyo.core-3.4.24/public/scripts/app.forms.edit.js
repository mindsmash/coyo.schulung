$(document).ready(function(){
	
	// intercept save to update json object
	$('.forms-editor').parents('form').submit(function(){
		// do not submit if enter was pressed in a modal
		if($('.forms-editor .modal:visible').length > 0) {
			return false;
		}
		
		var formElements = [];
		
		$('.forms-form > .control-group').each(function(i){
			var formElement = $(this);
			var formFieldId = null;
			var type = formElement.attr('data-type');
			
			if (formElement.attr('data-id') !== null && formElement.attr('data-id').trim() !== ""){
				formFieldId = parseInt(formElement.attr('data-id'));
			}
			
			var options = (window['forms_getOptionsData_' + type]) ? window['forms_getOptionsData_' + type](formElement) : [];
			
			formData = {
				id: formFieldId,
				label: formElement.find('.forms-label').text(),
				type: formElement.attr('data-type'),
				placeholder: formElement.find('.forms-input').attr('placeholder'),
				helpText: formElement.find('.help').attr('data-content'),
				fieldOrder: i,
				options: options,
				required: formElement.attr('data-required') === 'true',
				cssClass: formElement.attr('data-class')
			};
			
			formElements.push(formData);
			
			$('#forms_formString').val(JSON.stringify(formElements, null));
		});
	});
	
	// hooks for adding form elements
	function createElement(){
		var type = $(this).attr('data-type');
		
		var copy = $('.forms-templates *[data-type=' + type + ']').clone(true, true);
		var copyNum = $('.forms-form *[data-type=' + type + ']').length;
		var copyId = 'forms-' + type + '-' + copyNum;
		copy.attr('id', copyId);
		
		// init modal
		var modalCopy = copy.find('.modal');
		
		modalCopy.modal({
			backdrop: 'static',
			show: false
		});
		
		$('.forms-form').append(copy);
		
		return false;
	}
	$('.forms-field-types li').click(createElement);
	
	// action hooks for fields
	$('.forms-delete').click(function(){
		$(this).parents('.control-group').remove();
		return false;
	});
	
	$('.forms-edit').click(function(){
		var formElement = $(this).parents('.control-group');
		var modal = formElement.find('.modal');
		
		modal.modal('show');
		onModalShow(formElement, modal);
		
		var modalResetFunction = window['forms_resetModal_' + formElement.attr('data-type')];
		
		if(modalResetFunction){
			modalResetFunction(formElement, modal);
		}
		
		return false;
	});
	
	// init modal save button
	$('.forms-modal-save').click(function(){
		var modal = $(this).parents('.modal');
		var formElement = modal.parents('.control-group');
		
		modal.modal('hide');
		onModalHide(formElement, modal);
		
		var formUpdateFunction = window['forms_update_' + formElement.attr('data-type')];
		
		if(formUpdateFunction){
			formUpdateFunction(formElement, modal);
		}

		return false;
	});
	
	// init sortable
	$('.forms-form').sortable({
		axis: 'y',
		cursor: 'move'
	});
	
	
	function onModalHide(formElement, modal){
		formElement.find('.forms-label').text(modal.find('.forms-label-input').val()); //update label
		formElement.find('.forms-input').attr('placeholder', modal.find('.forms-placeholder-input').val()); //update placeholder
		
		//update helptext
		var helpText = modal.find('.forms-helptext-input').val();
		formElement.children('.controls').find('.help').remove();
		if(helpText !== ''){
			formElement.children('.controls').append('<a href="javascript:void(0)" class="btn btn-mini popover-toggle help" data-content="'+helpText+'"><i class="icon-question-sign"></i></a>');
			formElement.children('.controls').find('.help').popover();
		}
		
		// update required info
		if(modal.find('.forms-required-input').attr('checked')){
			formElement.find('.forms-required').show();
			formElement.attr('data-required', 'true');
		} else {
			formElement.find('.forms-required').hide();
			formElement.attr('data-required', 'false');
		}
		
		// update date info
		if(modal.find('.forms-date-input').attr('checked')){
			formElement.attr('data-class', 'date');
		} else {
			formElement.attr('data-class', '');
		}

	}
	
	function onModalShow(formElement, modal){
		
		// prevent sorting of form elements in background when modal is active
		$('.forms-form').sortable('disable');

		// turn it back on again as soon as the modal is hidden
		modal.on('hide', function(){
			$('.forms-form').sortable('enable');
		});
		
		// enable option sorting
		$('.multiple-settings').sortable({
			axis: 'y',
			cursor: 'move'
		});
		
		modal.find('.forms-label-input').val(formElement.find('.forms-label').text()); // reset label
		modal.find('.forms-placeholder-input').val(formElement.find('.forms-input').attr('placeholder')); // reset placeholder
		modal.find('.forms-helptext-input').val(formElement.find('.help').attr('data-content')); // reset helptext
		
		// reset required info
		if(formElement.attr('data-required') === 'true'){
			modal.find('.forms-required-input').attr('checked', 'checked');
		} else {
			modal.find('.forms-required-input').removeAttr('checked');
		}
		
		// bind date
		if (formElement.attr('data-class') === 'date'){
			modal.find('.forms-date-input').attr('checked', 'checked');
		} else {
			modal.find('.forms-date-input').removeAttr('checked');
		}
		
	}
	
});
// jQuery plugin
(function($) {
	var currentCalendar;
	var currentDate;
	var currentType;
	var currentUrl;
	
	var methods = {
		init : function(url, calendarId) {
			currentUrl = url;
			currentCalendar = calendarId;
			
			var $this = $(this);
			
			// load initial calendar
			$this.calendar('load', false);
			
			// view type switch
			$this.on('click', 'a[data-type]', function(e){
				e.preventDefault();
				currentType = $(e.currentTarget).attr('data-type');
				$this.calendar('load', false);
			});
			
			// start date switch (contained within the different view types)
			$this.on('click', 'a[data-date]', function(e){
				e.preventDefault();
				currentDate = $(e.currentTarget).attr('data-date');
				$this.calendar('load', false);
			});
			
			// enable filter features
			if($this.find('.calendar-filter').length > 0) {
				$(document).on('click', 'a.calendar-toggle-cal', function(e){
					$a = $(e.currentTarget);
					
					if($a.hasClass('ok')) {
						$a.removeClass('ok');
					} else {
						$a.addClass('ok');
					}
					
					$this.calendar('load', true);
				});
				
				// hide / show all
				$(document).on('click', 'a.calendar-toggle-all', function(e){
					$a = $(e.currentTarget);
					
					if($a.attr('data-action') == 'show') {
						$(document).find('a.calendar-toggle-cal').addClass('ok');
					} else {
						$(document).find('a.calendar-toggle-cal').removeClass('ok');
					}
					
					$this.calendar('load', true);
				});
			}
		},
		load : function(doFilter) {
			var $this = $(this);
			var $body = $this.find('.calendar-body');
			var filter;

			if(doFilter) {
				filter = '';
				$this.find('a.calendar-toggle-cal.ok').each(function(i, el){
					filter += $(el).attr('data-id') + ',';
				});
			}
			
			$this.addClass('loading');
			$body.html('');
			
			$this.find('.calendar-type-switch a[data-type]').removeClass('active');
			$this.find('.calendar-type-switch a[data-type="'+currentType+'"]').addClass('active');

			$.get(currentUrl, {
				start : currentDate, 
				type : currentType, 
				calendarId : currentCalendar, 
				filter : filter
			}, function(html){
				$body.html(html);
				$this.removeClass('loading');
				initLayoutTools($body);
				
				// update ical button
				$this.find('a.calendar-ical-subscribe').attr('href', $body.find('.calendar-body-inner').data('ical-url'));
				
				// update current type
				currentType = $body.find('.calendar-body-inner').data('current-type');
				$this.find('.calendar-type-switch a[data-type="'+currentType+'"]').addClass('active');
			});
		}
	};

	$.fn.calendar = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
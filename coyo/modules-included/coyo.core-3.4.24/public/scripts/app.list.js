/* jshint -W117 */
angular.module('app.list', ['coyo.generic', 'app.list.controller', 'coyo.list.services']);

var module = angular.module('app.list.controller', []);

module.controller('ListAppFormController', ['$scope', 'listModel', function($scope, listModel) {
	
	$scope.init = function(containerId) {
		/*
		 * try to get container - if one is available use it and get it's
		 * permission. Otherwise create a new container with default permissions
		 */
		if (angular.isDefined(containerId)) {
			listModel.getContainer(containerId, false, function(container) {
				if (angular.isDefined(container)) {
					$scope.container = container;
					listModel.getPermissions(container.id, function(result) {
						$scope.container.permissions = transformPermissions(result);
					});
				} else {
					$scope.container = createDefaultContainer();
				}
			});
		} else {
			$scope.container = createDefaultContainer();
		}
	};
	
	$scope.isDisabled = function(key) {
		if (angular.isDefined($scope.container) && angular.isDefined($scope.container.permissions)) {
			if (key === 'all') {
				return $scope.container.permissions['Read'] !== 'all';
			}
			if (key === 'own') {
				return $scope.container.permissions['Read'] === 'none';
			}
		}
		return false;
	};
	
	var createDefaultContainer = function() {
		return {
			name : '',
			permissions : {Create : true, Read : 'all', Update : 'own', Delete : 'own'},
			notificationRecipient: 'FOLLOWERS'
		};
	};
	
	var transformPermissions = function(input) {
		var permissions = {};
		permissions['Create'] = input['Create'];
		permissions['Read'] = getPermissionValue('ReadAll', 'ReadOwn', input);
		permissions['Update'] = getPermissionValue('UpdateAll', 'UpdateOwn', input);
		permissions['Delete'] = getPermissionValue('DeleteAll', 'DeleteOwn', input);
		return permissions;
	};
	
	var getPermissionValue = function(keyAll, keyOwn, permissions) {
		if (permissions[keyAll]) {
			return 'all'; 
		} 
		return (permissions[keyOwn]) ? 'own' : 'none'; 
	};
	
}]);
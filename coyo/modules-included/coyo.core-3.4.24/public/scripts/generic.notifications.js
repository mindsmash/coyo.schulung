$(document).ready(function() {
	var $notifications = $('.navbar .notifications-items'),
		$notificationsToggle = $('.navbar .notifications-toggle'),
		$loader = $notifications.find('.loading');
	
	// open & update
	if ($notifications.length > 0) {
		$notificationsToggle.click(function(e) {
			$loader.show();
			$.get($notifications.data('url'), function(data) {
				$loader.hide();
				$('.notification').remove();
				$('.notification-divider').remove();
				$notifications.find('.loading').after(data);
				initLayoutTools($notifications, true);
				$.post($notificationsToggle.data('url'));
			});
		});
		
		$('.navbar .notifications-items a.notifications-readall').click(function(e) {
			// force hide unread count on click
			$('#navbar-item-notifications .unread-count').addClass('ng-hide');
			
			// mark notifications as read
			$.post($(e.currentTarget).data('url'));
		});
	}
});
/* jshint -W117 */
var module = angular.module('coyo.list.services', []);


module.factory('errorHandler', [function() {
	/*
	 * Parses the error message from server and returns an array of
	 * i18n messages for display.
	 */
	var parseErrors = function(container, response, status) {
		var errors = [];
		if (status === 400) {
			angular.forEach(_.flatten(_.values(response)), function(error) {
				var field = _.findWhere(container.fields, {id : +error.key});
				errors.push({field : field.label, message : error.message});
			});
		} else if (status === 500) {
			if (response.indexOf('no values passed') > 0) {
				errors.push({message : message('list.error.noValues')});
			} else if (response.indexOf('Invalid form data') > 0) {
					errors.push({message : message('list.error.invalidFormData')});
			} else {
				errors.push({message : message('list.error.creation')});
			}
		} else if (status === 403) {
			errors.push({message : message('list.error.forbidden')});
		}
		return errors;
	};
	
	return {
		parseErrors : parseErrors
	};
}]);


module.factory('valueUtils', ['$filter', function($filter) {
	/**
	 * Returns the label for any value of any type. For arrays all labels
	 * are joined to a comma separated list, whereas for objects the correct
	 * value is determined. If the flag "preserveType" is not set, the return 
	 * value will always be a string, regardless of it's former type.
	 */
	var getLabel = function(itemValue, preserveType) {
		var referenceValue = itemValue.value;
		if (angular.isObject(itemValue.value)) {
			var displayProperty = itemValue.displayProperty;
			if (angular.isArray(itemValue.value)) {
				/* Array: Join all values to a string */
				referenceValue = _.pluck(itemValue.value, displayProperty).join(", ");
			} else {
				/* Object: Get correct value from object */
				referenceValue = itemValue.value[displayProperty];
			}
		} else {
			if (!preserveType) {
				/* cast to string */
				referenceValue += '';
			} 	
		} 			
		if (_.isString(itemValue.value)) {
			var compare = itemValue.value;
			compare = compare.replace("ä", "ae");
			compare = compare.replace("ö", "oe");
			compare = compare.replace("ü", "ue");
			compare = compare.replace("ß", "ss");
			return compare;
		}			
		return (angular.isDefined(referenceValue)) ? referenceValue : '';
	};
	
	/**
	 * Returns the value for ordering items. Needs the item to order and the 
	 * field's ID which should be used for ordering. If the field ID is not valid,
	 * null is returned. 
	 */
	var getOrderValue = function(item, fieldId) {
		if (fieldId > 0) {
			var itemValue = _.findWhere(item.values, {fieldId : fieldId});
			if (angular.isDefined(itemValue)) {
				if(angular.isDefined(itemValue.value) && angular.isDefined(itemValue.value.date) && angular.isDate(itemValue.value.date)) {
					var formattedDate = $filter("date")(itemValue.value.date, "yyyy-MM-dd");
					if(angular.isDefined(formattedDate)) {
						return formattedDate;
					} else if(angular.isDefined(itemValue.value.formattedDate)) {
						return itemValue.value.formattedDate;
					} else {
						return "";
					}
				} else {
					return getLabel(itemValue, true);
				}
			}
		}
		/* sort items by their modification date by default */
		return item.modified;
	};
	
	/**
	 * Flattens the item values. For example the values of option
	 * fields, can contain objects and arrays of objects. If one
	 * object is passed, it's reduced to it's id. If an array of
	 * objects is passed, the ids are returned as a comma separated
	 * string. If an array of none objects are passed, the values of
	 * the array are returned as a comma separated string. This
	 * conversion is necessary since the server can only store
	 * strings as values.
	 */
	var parseItemValues = function(itemValues) {
		var result = angular.copy(itemValues);
		angular.forEach(result, function(itemValue, key) {
			if (angular.isObject(itemValue)) {
				if (angular.isArray(itemValue)) {
					result[key] = (angular.isObject(itemValue[0])) ? _.pluck(itemValue, 'id').join(",") : itemValue.join(",");
				} else if(angular.isDate(itemValue)) {
					result[key] = $filter("date")(itemValue, Coyo.dateFormats["normal"]);
				} else {
					result[key] = itemValue.id;
				}
			}
		});
		/* clear all empty values */
		_.each(result, function(value, key) {
			if (_.isUndefined(value) || _.isNull(value) || value === '' || value === false) {
				delete result[key];
			}
		});
		return result;
	};
	
	return {
		getLabel        : getLabel,
		getOrderValue   : getOrderValue,
		parseItemValues : parseItemValues
	};
}]);


module.factory('formHelper', ['CoyoUtils', function(CoyoUtils) {
	/**
	 * Helper method to create a placeholder indicating the field
	 * constraints: required, max, min, max length and min length.
	 */
	var _createPlaceholderText = function(required, properties) {
		var placeholder = '';
		if (CoyoUtils.stringToBoolean(required)) {
			placeholder += message('list.field.required');
		}
		angular.forEach(properties, function(property, name) {
			switch (name) {
				case 'min':
				case 'max':
				case 'minLength':
				case 'maxLength':
				case 'date':
					placeholder += (placeholder.length > 0) ? ', ' : '';
					placeholder += message('list.field.constraint.' + name, property);
				break;
			default:
				break;
			}
		});
		return (placeholder.length > 0) ? placeholder : message('list.field.optional');
	};
	
	return {
		createPlaceholderText : _createPlaceholderText
	};
}]);


module.factory('listModel', ['$http', function($http) {
	return {
		getContainer : function(containerId, includeItems, onSuccess, onError) {
			var url = '/containers/' + containerId,
				params = {includeItems : includeItems};
			
			$http({
				method : 'GET',
				url    : url,
				params : params
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status, headers, config);
			});
		},
		saveContainer : function(container, onSuccess) {
			var url = '/containers',
				formData = container;
			
			$http({
				method : 'POST',
				url    : url,
				data   : formData
			}).success(function(response) {
				onSuccess(response);
			});
		},
		createItem : function(containerId, itemValues, onSuccess, onError) {
			var url = '/containers/' + containerId + '/items',
				formData = itemValues;
			
			$http({
				method : 'POST',
				url    : url,
				data   : formData
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status, headers, config);
			});
		},
		saveItem : function(containerId, itemId, itemValues, onSuccess, onError) {
			var url = '/containers/' + containerId + '/items/' + itemId,
				formData = itemValues;
			
			$http({
				method : 'POST',
				url    : url,
				data   : formData
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status, headers, config);
			});
		},
		deleteItem : function(containerId, itemId, onSuccess) {
			var url = '/containers/' + containerId + '/items/' + itemId;
			$http({
				method : 'DELETE',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			});
		},
		getItems : function(containerId, onSuccess) {
			var url = '/containers/' + containerId + '/items';
			$http({
				method : 'GET',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			});
		},
		getFieldTypes : function(onSuccess) {
			var url = '/containers/fieldTypes';
			$http({
				method : 'GET',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			});
		},
		getPermissions : function(containerId, onSuccess) {
			var url = '/containers/' + containerId + '/permissions';
			$http({
				method : 'GET',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			});
		}
	};
}]);
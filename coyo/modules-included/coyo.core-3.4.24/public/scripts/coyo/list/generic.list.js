/* jshint -W117 */
angular.module('coyo.list', ['coyo.generic', 'coyo.list.controllers', 'coyo.list.directives', 'coyo.list.services', 'coyo.list.filters', 'coyo.generic.userchooser', 'coyo.generic.optionchooser', 'coyo.generic.filechooser', 'ngRoute', 'ui.sortable'])

	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		var getQueryParam = function(paramName) {
			var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"),
				results = regex.exec(location.search);
			if (results !== null) {
				return decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			return null;
		};

		$routeProvider
			.when('/', {
				redirectTo : function() {
					var itemId = getQueryParam('itemId');
					if (itemId) {
						return '/overview?itemId=' + itemId;
					} else {
						return '/overview';
					}
				}
			})
			.when('/config', {
				templateUrl: Coyo.scripts + '/views/coyo/list/configuration.html',
				controller: 'ListConfigurationController'
			})
			.when('/overview', {
				templateUrl: Coyo.scripts + '/views/coyo/list/itemlist.html',
				controller: 'ListOverviewController',
				reloadOnSearch: false
			})
			.otherwise({
				templateUrl: Coyo.scripts + '/views/coyo/list/itemlist.html',
				controller: 'ListOverviewController'
			});
		
		}
	]);


var module = angular.module('coyo.list.filters', []);

/**
 * Filter for the query filter input. Returns all array items, which have values
 * that contain the query string. All fields of type boolean are ignored. If the
 * value is an array of objects, their label values will be handled as an comma
 * separated string.
 */
module.filter('itemfilter', ['$filter', 'valueUtils', function($filter, valueUtils) {
	return function(items, filterQuery) {
		// return all items if query string is not set
		if (angular.isUndefined(items) || angular.isUndefined(filterQuery) || filterQuery === '') {
			return items;
		}
			
		var result = [];
		for (var i = 0; i < items.length; i++) {
			for (var j = 0; j < items[i].values.length; j++) {
				var itemValue = items[i].values[j];
				
				/* do not evaluate booleans */
				if (angular.isUndefined(itemValue) || itemValue.type.indexOf('Boolean') > 0) {
					continue;
				}
				
				var valueToFilter;
				/* allow filtering of dates */
				if(angular.isDefined(itemValue.value) && angular.isDefined(itemValue.value.formattedDate)) {
					valueToFilter = itemValue.value.formattedDate;
				} else {
					valueToFilter = valueUtils.getLabel(itemValue).toLowerCase();
				}
				/* add all items that contain the query string. */
				if (valueToFilter.indexOf(filterQuery.toLowerCase()) >= 0) {
					result.push(items[i]);
					break;
				}
			}
		}
		return result;
	};
}]);


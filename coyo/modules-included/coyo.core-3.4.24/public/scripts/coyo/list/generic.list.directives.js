/* jshint -W117 */
var module = angular.module('coyo.list.directives', ['coyo.list.tpls']);

/**
 * Directive for displaying values depending on their type.
 * - Text: simply returns the text, but replaces new line with
 *   <br> tags 
 * - Boolean: replaces 'true or 'false' with according icons
 * - Option: only displays the label. If more than one option 
 *   was selected, the labels are separated by commas.
 * - User: Displays the full name of a user and a link to the 
 *   user's profile. If more than one user was selected, the 
 *   links are separated by comma.
 * - File: Displays the file with a download link. If the file 
 *   is of a known type and the parameter "showPreview" is set 
 *   to "true", a preview is generated (e.g. image, pdf, video) 
 */
module.directive('formatValue', ['$compile', 'CoyoUtils', 'HashtagService', '$filter', function($compile, CoyoUtils, HashtagService, $filter) {
	return {
		restrict : 'A',
		scope    : {
			formatValue : '=',
			type        : '=',
			properties  : '=?',
			showPreview : '=?'
		},
		link : function(scope, element, attrs) {
			scope.showPreview = (angular.isDefined(scope.showPreview) && CoyoUtils.stringToBoolean(scope.showPreview));
			
			var formatInput = function(scope) {
				var html = '';
				
				/* check if input is empty and one of the types, that can't handle empty input */
				if (_.isEmpty(scope.formatValue) && (scope.type === 'OPTIONS' || scope.type === 'DATE' || scope.type === 'USER' || scope.type === 'FILE' || scope.type === 'LINK')) {
					return '';
				}
				
				switch (scope.type) {
					case 'TEXT':
						/* We had to disable Hashtags for list app, since it leads to XSS vulnerabilities */
						// scope.formatValue = HashtagService.hashify(scope.formatValue);
						html = '<span class="show-newline" data-ng-bind="formatValue"></span>';
						return $compile(html)(scope);
					case 'DATE':
						scope.formatValue.date = new Date(scope.formatValue.iMillis);
						scope.formatValue.formattedDate = $filter("date")(scope.formatValue.date, Coyo.dateFormats["normal"]);
						html = '<div format-date="formatValue.date"></div>';
						return $compile(html)(scope);
					case 'BOOLEAN':
						return (scope.formatValue) ? '<span class="label label-success"><i class="icon-ok icon-white"></i></span>' : '<span class="label label-important"><i class="icon-remove icon-white"></i></span>';
					case 'OPTIONS':
						if (angular.isArray(scope.formatValue)) {
							scope.labels = _.pluck(scope.formatValue, 'label').join(', ');
							html = '<span data-ng-bind="labels"></span>';
						} else {
							html = '<span data-ng-bind="formatValue.label"></span>';
						}
						return $compile(html)(scope);
					case 'USER':
						if (angular.isArray(scope.formatValue)) {
							html = angular.element('<span data-ng-repeat="user in formatValue"><user-link data-user-link="user"></user-link><span ng-hide="$last">, </span></span>');
						} else {
							html = angular.element('<span><user-link data-user-link="formatValue"></user-link></span>');
						} 
						return $compile(html)(scope);
					case 'FILE':
						html = angular.element('<div data-wall-post-attachments="formatValue" data-show-preview="{{showPreview}}"></div>');
						return $compile(html)(scope);
					case 'LINK':
						html = angular.element('<a class="btn btn-mini btn-primary" data-ng-href="{{formatValue}}" data-tooltip-placement="bottom" data-tooltip-append-to-body="true" data-tooltip="{{formatValue}}"><i class="icon-share-alt icon-white"></i></a>');
						if (angular.isUndefined(scope.properties) || !CoyoUtils.stringToBoolean(scope.properties.sametab)) {
							html.attr('target', '_blank');
						}
						return $compile(html)(scope);
					default:
						html = '<span data-ng-bind="formatValue"></span>';
						return $compile(html)(scope);
				}
			};
			
			/*
			 * IMPORTANT: Since we are using element.html() here, it is very important, that
			 * the returned value of formatInput is already sanitized and does not contain any
			 * malicious HTML or script tags! In most of the cases we let angular itself take
			 * care of doing this, by using ngBind.
			 */
			var html = formatInput(scope);
			element.html(html);
		}
	};
}]);



/**
 * Displays an item in the detail view.
 */
module.directive('displayItem', ['$compile', '$templateCache', function($compile, $templateCache) {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			field       : '=',
			itemValue   : '=',
			showPreview : '='
		},
		templateUrl : 'template/item.html'
	};
}]);


/**
 * Renders a form field based on its type.
 */
module.directive('dynamicField', ['$compile', '$templateCache', function($compile, $templateCache) {
	return {
		restrict : 'E',
		scope : {
			field   : '=',
			ngModel : '='
		},
		link : function(scope, element, attrs) {
			var type = scope.field.type.toLowerCase(),
				template = $templateCache.get('template/input/' + type + '.html'),
				html = angular.element(template);
			if (type === 'user') {
				scope.options = {
					serviceUrl    : '/users/chooser',
					showMe        : true,
					showExternals : true
				};
			}
			element.replaceWith($compile(html)(scope));
		}
	};
}]);

module.directive('dynamicInput', ['$compile', 'formHelper', function($compile, formHelper) {
	return {
		restrict : 'E',
		replace: true,
		link : function(scope, element, attrs) {
			var type = scope.field.type.toLowerCase(),
				inputElement;
			
			switch (type) {
				case 'text':
					if (scope.field.properties.multiline) {
						inputElement = angular.element('<textarea>');
					} else {
						inputElement = angular.element('<input>');
						inputElement.attr('type', 'text');
						if (scope.field.properties.minLength) {
							inputElement.attr('minlength', scope.field.properties.minLength);
							inputElement.attr('data-ng-minlength', scope.field.properties.minLength);
						}
						if (scope.field.properties.maxLength) {
							inputElement.attr('maxlength', scope.field.properties.maxLength);
							inputElement.attr('data-ng-maxlength', scope.field.properties.maxLength);
						}
					}
					inputElement.attr('placeholder', formHelper.createPlaceholderText(scope.field.required, scope.field.properties));
					break;
				case 'date':
					inputElement = angular.element('<input type="text" class="form-control" data-ng-class="{required: field.required}">');
					inputElement.attr('data-datepicker-popup', Coyo.dateFormats["normal"]);
					inputElement.attr('data-show-button-bar', 'false');
					inputElement.attr('data-is-open', 'opened');
					inputElement.attr('data-datepicker-append-to-body', 'true');
					inputElement.attr('data-datepicker-options', 'dateOptions');
					inputElement.attr('data-date-disabled', 'disabled(date, mode)');
					inputElement.attr('placeholder', formHelper.createPlaceholderText(scope.field.required, scope.field.properties));
					break;
				case 'link':
					inputElement = angular.element('<input>');
					inputElement.attr('type', 'url');
					inputElement.attr('placeholder', formHelper.createPlaceholderText(scope.field.required, scope.field.properties));
					break;
				case 'number':
					inputElement = angular.element('<input>');
					inputElement.attr('type', 'number');
					inputElement.attr('min', scope.field.properties.min);
					inputElement.attr('max', scope.field.properties.max);
					inputElement.attr('placeholder', formHelper.createPlaceholderText(scope.field.required, scope.field.properties));
					break;
				case 'boolean':
					inputElement = angular.element('<input>');
					inputElement.attr('type', 'checkbox');
					if (!scope.ngModel && scope.field.properties.preselected) {
						scope.ngModel = true;
					}
					break;
				case 'options':
					/* options are handled by optionChooser directive */
				case 'user':
					/* user is handled by userChooser directive */
					break;
				case 'file':
					/* file is handled by fileChooser directive */
					break;
				default:
					inputElement = angular.element('<input>');
					break;
			}
			
			inputElement.attr('id', scope.field.id);
			inputElement.attr('name', scope.field.id);
			inputElement.attr('data-ng-model', 'ngModel');
			if (!angular.isUndefined(scope.field.required)) {
				inputElement.attr('data-ng-required', scope.field.required);
			}
			
			var formElement = angular.element('<div data-ng-form name="form" class="input-field-wrapper">');
			formElement.append(inputElement);
			
			element.replaceWith($compile(formElement)(scope));
		}, controller : ['$scope', function($scope) {
			$scope.dateOptions = {
				formatDay: 'dd',
				formatMonth: 'MMMM',
				formatYear: 'yyyy',
				startingDay: 1
			};
			$scope.format = Coyo.dateFormats["normal"];
			$scope.$watch('form.$valid', function(newVal, oldVal){
				$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : $scope.form.$valid});
			}, true);
			$scope.$watch('ngModel', function(newVal, oldVal){
				$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : $scope.form.$valid});
			}, true);
		}] 
	};
}]);


/**
 * Module which holds all templates for different input types.
 */
angular.module('coyo.list.tpls', ['template/input/text.html', 'template/input/date.html', 'template/input/link.html', 'template/input/number.html', 'template/input/boolean.html', 'template/input/options.html', 'template/input/file.html', 'template/input/user.html', 'template/item.html']);

angular.module("template/item.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/item.html", 
		'<div><span class="item-label">{{field.label}}: </span><span class="item-value" data-format-value="itemValue" data-type="field.type" data-properties="field.properties" data-show-preview=showPreview></span></div>'
	);
}]);

angular.module("template/input/text.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/text.html", 
		'<div class="control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<dynamic-input />' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/date.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/date.html", 
		'<div class="control-group input-group date-field">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
				'<div class="input-prepend">' +
					'<span class="add-on"><i class="icon-calendar"></i></span>' +
					'<dynamic-input />' +
				'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/link.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/link.html", 
		'<div class="control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<div class="input-prepend">' +
					'<span class="add-on"><i class="icon-globe"></i></span>' +
					'<dynamic-input />' +
				'</div>' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/options.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/options.html", 
		'<div class="control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<option-chooser data-field="field" data-ng-model="ngModel" />' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/user.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/user.html", 
		'<div class="control-group userfield-control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<user-chooser data-field="field" data-ng-model="ngModel" data-options="options" />' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/number.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/number.html", 
		'<div class="control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<dynamic-input />' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/file.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/file.html", 
		'<div class="control-group">' +
			'<label class="control-label" data-ng-class="{required: field.required}" for="{{field.id}}">{{field.label}}</label>' +
			'<div class="controls">' +
				'<file-chooser data-field="field" data-ng-model="ngModel" />' +
			'</div>' +
		'</div>'
	);
}]);

angular.module("template/input/boolean.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/input/boolean.html", 
		'<div class="control-group">' +
			'<div class="controls">' +
				'<label class="checkbox" data-ng-class="{required: field.required}"> {{field.label}}' +
					'<dynamic-input />' +
				'</label>' +
			'</div>' +
		'</div>'
	);
}]);
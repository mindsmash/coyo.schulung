/* jshint -W117 */
var module = angular.module('coyo.list.controllers', []);

/**
 * General controller to initialize the app. The controller takes a valid
 * container ID as parameter.
 */
module.controller('CoyoListController', ['$scope', 'listModel', function($scope, listModel) {
	$scope.init = function(containerId) {
		$scope.containerId = containerId;
	};
}]);


/**
 * Controller handling the configuration of the list. This controller is used
 * when the configuration page for a list is displayed (url: /config).
 */
module.controller('ListConfigurationController', ['$scope', '$location', '$modal', 'listModel', function($scope, $location, $modal, listModel) {
	if (angular.isDefined($scope.$parent.containerId)) {
		listModel.getContainer($scope.$parent.containerId, false, function(response) {
			$scope.container = response;
		});
	}

	listModel.getFieldTypes(function(response) {
		$scope.fieldTypes = response;
	});
	
	$scope.getProperties = function(field) {
		if (angular.isDefined($scope.fieldTypes) && angular.isDefined(field.type)) {
			var properties = $scope.fieldTypes[field.type].properties;
			_.each(properties, function(property) {
				property.id = property.key;
				property.properties = {};
			});
			return properties;
		}
	};
	
	$scope.removeField = function(index) {
		$scope.container.fields.splice(index, 1);
	};
	
	$scope.createField = function() {
		$scope.currentField = {};
		
		/* create modal */
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/list/createfield.html',
			controller  : 'ListFieldTypeController',
			windowClass : 'modal-large',
			scope       : $scope
		});
		
		modalInstance.result.then(function(response) {
			$scope.container.fields.push(response);
		});
		
	};
	
	$scope.editField = function(field, index) {
		$scope.currentField = field;
		
		/* create modal */
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/list/createfield.html',
			controller  : 'ListFieldTypeController',
			windowClass : 'modal-large',
			scope       : $scope
		});
		
		modalInstance.result.then(function(response) {
			$scope.container.fields[index] = response;
		});
		
	};
	
	$scope.saveContainer = function() {
		// update the positions of the fields before submitting
		angular.forEach($scope.container.fields, function(field, index) {
			field.priority = index;
		});
		
		// save container
		listModel.saveContainer($scope.container, function(result) {
			$location.url('/overview');
		});
	};
	
}]);


/**
 * This controller handles the main view - the overview of all items. The
 * controller is used when calling the URL "/overview" or simply the index page
 * of the app.
 */
module.controller('ListOverviewController', ['$scope', '$modal', '$window', 'listModel', 'valueUtils', 'errorHandler', '$location', function($scope, $modal, $window, listModel, valueUtils, errorHandler, $location) {
	var _ = $window._;
	$scope.errors = [];
	$scope.orderId = -1;
	$scope.orderReverse = true;
	$scope.loading = true;

	if ($scope.$parent.containerId) {
		listModel.getContainer($scope.$parent.containerId, true, function(response) {
			$scope.container = response;
			$scope.loading = false;
			// if itemId routes param is present show item
			if ($location.search().itemId) {
				var itemId = $location.search().itemId;
				var item = _.findWhere($scope.container.items, {'id': Number(itemId)});
				if (item) {
					$scope.showItem(item);
				}
			}
		}, function(response, status) {
			$scope.errors = errorHandler.parseErrors($scope.container, response, status);
			$scope.loading = false;
		});
	}

	/*
	 * Get the value for an item (row) with the index 'index' and
	 * the passed fieldId. Since the information are in two
	 * different lists, we have to do a lookup. Return an empty
	 * string, if no value could be found.
	 */
	$scope.getValue = function(item, fieldId) {
		if (angular.isDefined(item)) {
			var result = _.findWhere(item.values, {fieldId : fieldId});
			if (typeof result !== 'undefined') {
				return result.value;
			}
		}
		return '';
	};
	
	
	/* show options for each item on hover */
	$scope.hover = function(item) {
		item.showOptions = !item.showOptions;
	};
	
	
	/* returns the value of an item to be compared when ordering the list */
	$scope.orderPredicate = function(item) {
		return valueUtils.getOrderValue(item, $scope.orderId);
	};
	
	
	/* set order of table (which field and ASC or DESC) */
	$scope.setOrder = function(fieldId) {
		if ($scope.orderId === fieldId) {
			$scope.orderReverse = !$scope.orderReverse;
		} else {
			$scope.orderId = fieldId;
			$scope.orderReverse = false;
		}
	};
	
	
	/*
	 * Shows a modal dialog for creating a new item. Therefore an
	 * empty itemValues object is added to the scope, which is then
	 * filled by the form. Values are saved and passed to the server
	 * as key, value pairs (field id, value).
	 */
	$scope.createItem = function() {
		$scope.itemValues = {};
		$scope.itemValidation = {};
		
		/* create modal */
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/list/edititem.html',
			controller  : 'ListItemController',
			windowClass : 'modal-scroll',
			scope       : $scope
		});
	
		/* push item to view and delete temporary item model */
		modalInstance.result.then(function(response) {
			$scope.container.items.unshift(response);
			_cleanUp();
		}, _cleanUp);
	};


	/*
	 * Shows a modal dialog for editing an item. Before the modal
	 * can be shown, the item's values must be converted to key,
	 * value pairs (field id, value). The item is updated within the
	 * view automatically.
	 */
	$scope.editItem = function(item) {
		$scope.currentItem = item;
		$scope.itemValues = {};
		$scope.itemValidation = {};
		
		/* convert item values */
		angular.forEach($scope.container.fields, function(field) {
			var itemValue = _.findWhere(item.values, {fieldId : field.id});
			if (angular.isDefined(itemValue)) {
				if(angular.isDefined(itemValue.value) && angular.isDefined(itemValue.value.date) && angular.isDefined(itemValue.value.date)) {
					this[field.id] = itemValue.value.date;
				} else {
					this[field.id] = itemValue.value;
				}
			}
		}, $scope.itemValues);
		
		/* create modal */
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/list/edititem.html',
			controller  : 'ListItemController',
			windowClass : 'modal-scroll',
			scope       : $scope
		});
	

		/* update item in view (or delete it if action was a
		 * deletion) and reset temporary item model */
		modalInstance.result.then(function(response) {
			var item = _.findWhere($scope.container.items, {id : response.id}),
				index = $scope.container.items.indexOf(item);
			if (response.deleted) {
				$scope.container.items.splice(index, 1);
			} else {
				$scope.container.items[index] = response;
			}
			_cleanUp();
		}, _cleanUp);
	};
	
	$scope.showItem = function(item) {
		$scope.currentItem = item;
		// adds deep linking parameter itemId to browser url angularjs routing is suppressed by reloadOnSearch: false in routing rule
		$location.search({itemId: item.id});

		/* create modal */
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/list/itemdetails.html',
			controller  : 'ListItemController',
			windowClass : 'modal-scroll',
			scope       : $scope
		});

		/* reset temporary item model */
		modalInstance.result.then(function() {}, _cleanUp);
	};

	/* cleans up after a modal was closed */
	var _cleanUp = function() {
		$scope.currentItem = null;
		$scope.itemValues = null;
		$location.search('itemId', null); // remove param
	};
}]);


/**
 * This controller handles the management (create, edit, delete) of an item. The
 * view is displayed within an modal dialog.
 */
module.controller('ListItemController', ['$scope', '$modalInstance', 'listModel', 'valueUtils', 'errorHandler', function($scope, $modalInstance, listModel, valueUtils, errorHandler) {
	$scope.errors = [];
	
	/*
	 * If the validity of one field changes an event gets
	 * propagated. This method is evaluates this events and checks
	 * whether the overall form is valid or not. This is used to
	 * enable or disable the submit button.
	 */
	$scope.$on('validityChanged', function(event, data) {
		event.stopPropagation();
		$scope.itemValidation[data.fieldId] = data.valid;
		$scope.formIsValid = _.every($scope.itemValidation, function(value) {
			return value;
		});
	});

	$scope.createOrEdit = function(item, itemValues){
		if (item !== undefined) {
			$scope.editItem(item, itemValues);
		}
		if (item === undefined || item === null) {
			$scope.createItem(itemValues);
		}
	};

	$scope.createItem = function(itemValues) {
		$scope.errors = [];
		listModel.createItem($scope.container.id, valueUtils.parseItemValues(itemValues), function(response) {
			$modalInstance.close(response);
		}, function(response, status) {
			$scope.errors = errorHandler.parseErrors($scope.container, response, status);
		});
	};
	
	$scope.editItem = function(item, itemValues) {
		$scope.errors = [];
		listModel.saveItem($scope.container.id, item.id, valueUtils.parseItemValues(itemValues), function(response) {
			$modalInstance.close(response);
		}, function(response, status) {
			$scope.errors = errorHandler.parseErrors($scope.container, response, status);
		});
	};
	
	$scope.deleteItem = function(item) {
		listModel.deleteItem($scope.container.id, item.id, function(response) {
			$modalInstance.close({id : item.id, deleted : true});
		});
	};
	
	
	// dismiss window
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
}]);


/**
 * This controller handles the creation of a field.
 */
module.controller('ListFieldTypeController', ['$scope', '$modalInstance', function($scope, $modalInstance) {
	
	// add a new field with default values
	$scope.initField = function(type) {
		$scope.currentField = {
			newField   : true,
			type       : type,
			hidden     : false,
			required   : false,
			properties : {},
			options    : []
		};
	};
	
	$scope.addOption = function(field) {
		if (angular.isDefined(field.newoption) && field.newoption.length > 0) {
			field.options.unshift({label: field.newoption});
			field.newoption = '';
		}
	};
	
	$scope.deleteOption = function(field, index) {
		field.options.splice(index, 1);
	};
	
	$scope.reset = function() {
		$scope.currentField = {};
	};
	
	$scope.ok = function(field) {
		$modalInstance.close(field);
	};
	
	$scope.isValid = function(field) {
		if (angular.isUndefined(field.label) || field.label.length < 1) {
			return false;
		}
		if (field.type === 'OPTIONS' && field.options.length < 1) {
			return false;
		}
		return true;
	};
	
	// dismiss window
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
}]);
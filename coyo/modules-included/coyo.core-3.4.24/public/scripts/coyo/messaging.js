(function() {
	/**
	 * The coyo.messaging module.
	 */
	var module = angular.module('coyo.messaging', ['coyo.messaging.resources', 'coyo.wall.directives', 'coyo.messaging.directives', 'coyo.messaging.filters', 'ngSanitize']);
})();

(function() {
	var module = angular.module('coyo.workspace.controllers', []);
	
	module.controller('WorkspaceListController', ['$scope', 'WorkspaceService', function($scope, WorkspaceService) { 
		$scope.filter = {};
		$scope.categories = [];
		$scope.workspaces = [];
		$scope.activeCategory = "my";
		$scope.filter = "";
		$scope.done = false;
		$scope.loading = true;
		
		var getWorkspaces = function() {
			var clearCache = !!$scope.filter[$scope.activeCategory];
			WorkspaceService.getWorkspaces($scope.activeCategory, $scope.filter[$scope.activeCategory], clearCache).then(function(result) {
				$scope.workspaces = result.workspaces;
				$scope.done = result.paging.done;
				$scope.loading = false;
			});
		};
		
		$scope.$watch('filter', _.throttle(function(oldValue, newValue) {
			if (oldValue !== newValue) {
				$scope.workspaces = [];
				$scope.loading = true;
				getWorkspaces();
			}
		}, 800, {leading: false}), true);
		
		/* initial call to get the users workspaces */
		WorkspaceService.getCategories().then(function(result) {
			$scope.categories = result;
			getWorkspaces();
		});
		
		$scope.getNextPage = function() {
			$scope.loading = true;
			WorkspaceService.getNextPage($scope.activeCategory, $scope.filter[$scope.activeCategory]).then(function(result) {
				$scope.workspaces = $scope.workspaces.concat(result.workspaces);
				$scope.done = result.paging.done;
				$scope.loading = false;
			});
		};
		
		/* load new category if user switched tabs */
		$scope.switchCategory = function(id) {
			$scope.activeCategory = id;
			$scope.workspaces = [];
			$scope.loading = true;
			getWorkspaces();
		};
		
	}]);

	module.controller('WorkspaceUserListController', ['$scope', 'WorkspaceService', 'EventService', 'CoyoUtils', 'WORKSPACES', function($scope, WorkspaceService, EventService, CoyoUtils, WORKSPACES) {
		$scope.limit = WORKSPACES.LIMITS.MEMBERS;
		$scope.members = [];
		$scope.chatEnabled = false;
		$scope.audioVideoEnabled = false;
		$scope.loading = false;
		
		$scope.init = function(id, chatEnabled, audioVideoEnabled) {
			$scope.loading = true;
			$scope.chatEnabled = chatEnabled;
			$scope.audioVideoEnabled = audioVideoEnabled;
			WorkspaceService.getMembers(id, $scope.limit).then(function(result) {
				$scope.members = result;
				$scope.loading = false;
			});
		};

		/*
		 * Subscribe to event service to get notified of all changed user status.
		 */
		EventService.subscribe('onlineStatusChanged', function (event) {
			var payload = event.object;

			// update status in colleagues list
			var member = _.find($scope.members.memberList, function (member) {
				return member.user.id === payload.userId;
			});

			if (angular.isDefined(member)) {
				member.user.status = payload.newStatus;
				member.user.isCallable = payload.isCallable;
			}
		});
		
		/*
		 * function for ordering members by status (Online > Busy > Away > Offline)
		 */
		$scope.orderByStatus = function(member) {
			return CoyoUtils.orderByStatus(member.user);
		};
		
	}]);
})();

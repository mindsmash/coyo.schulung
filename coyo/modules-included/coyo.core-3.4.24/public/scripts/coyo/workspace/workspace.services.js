(function() {
	var module = angular.module('coyo.workspace.services', []);

	module.factory('WorkspaceService', ['$http', '$q', 'WORKSPACES', function($http, $q, WORKSPACES) {
		var workspacesByCategory = [];
		
		/*
		 * fetches new workspaces from the database by using the given
		 * category id, paging and filter.
		 */
		var _fetch = function(categoryId, page, filter) {
			var deferred = $q.defer(),
				url = ROUTES.Workspace_list.url(),
				params = {
					limit       : WORKSPACES.LIMITS.LIST,
					categoryId  : categoryId,
					page        : page,
					filterQuery : filter
				};
			
			$http.get(url, {params : params}).success(function(data, status, headers, config) {
				var result = {
						workspaces : data,
						paging : {
							page   : page,
							done   : data.length !== WORKSPACES.LIMITS.LIST
						}
				};
				
				if (_.isUndefined(workspacesByCategory[categoryId])) {
					workspacesByCategory[categoryId] = result;
				} else {
					/* concat new page to existing workspaces */
					workspacesByCategory[categoryId].workspaces = workspacesByCategory[categoryId].workspaces.concat(result.workspaces);
					workspacesByCategory[categoryId].paging = result.paging;
				}
				deferred.resolve(result);
			});
			return deferred.promise;
		};
		
		var WorkspaceService = {
			/*
			 * Get all workspaces by category and filter. If clearCache is set
			 * (which is the case if a filter is set), results are not cached.
			 */
			getWorkspaces : function(categoryId, filter, clearCache) {
				if (clearCache || !workspacesByCategory[categoryId] || workspacesByCategory[categoryId].length < 1) {
					return _fetch(categoryId, 1, filter);
				} else {
					var deferred = $q.defer();
					deferred.resolve(workspacesByCategory[categoryId]);
					return deferred.promise;
				}
			},
			
			/*
			 * Get the next page for the current category and filter. This method
			 * only works properly if a the first page was already fetched.
			 */
			getNextPage : function(categoryId, filter) {
				return _fetch(categoryId, ++workspacesByCategory[categoryId].paging.page, filter);
			},
									
			/*
			 * iterates over all workspaces of all categories and replaces the
			 * workspace to be updated. Note that only the arrays values are
			 * iterated, since we have an associative array and _.each can't
			 * work with mixed keys.
			 */
			updateWorkspace : function(ws) {
				_.each(_.values(workspacesByCategory), function(category) {
					_.each(category.workspaces, function(workspace, index) {
						if (ws.id === workspace.id) {
							category.workspaces[index] = ws;
						}
					});
				});
			},
			
			/*
			 * Retrieves all categories from database
			 */
			getCategories : function() {
				var deferred = $q.defer(),
					url = ROUTES.Workspace_categories.url();
				
				$http.get(url).success(function(data, status, headers, config) {
					deferred.resolve(data.elements);
				});
				return deferred.promise;
			},
			
			/*
			 * Gets all members of the workspace with the given id from
			 * database. The amount of results is limited to 'limit'.
			 */
			getMembers : function(id, limit) {
				var deferred = $q.defer(),
					url = ROUTES.Workspace_members.url({id : id}),
					params = {limit : limit};
				
				$http.get(url, {params : params}).success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
				return deferred.promise;
			},
			
			/*
			 * Method to join or send a join request for the workspace with the given id.
			 */
			join : function(id) {
				var deferred = $q.defer(),
					url = ROUTES.Workspace_join.url({id : id});
				
				$http.post(url).success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
				return deferred.promise;
			}
		};
		return WorkspaceService;
	}]);
})();
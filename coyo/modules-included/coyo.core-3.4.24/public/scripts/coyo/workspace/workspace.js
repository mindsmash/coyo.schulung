(function() {
	'use strict';

	angular
		.module('coyo.workspace', ['coyo.workspace.controllers', 'coyo.workspace.services', 'coyo.workspace.directives', 'infinite-scroll'])
		.constant('WORKSPACES', {
			LIMITS : {
				MEMBERS : 20,
				LIST    : 20
			}
		});
})();

(function() {
	var module = angular.module('coyo.workspace.directives', []);
	

	/*
	 * Creates a dynamic join button for workspaces. If a user joins a
	 * public workspace, he is redirected to this workspace on success. If
	 * the user tries to join a closed workspace, a join request is send and
	 * the workspace updated. In addition the button shows a "loading"
	 * state, when a request is performed.
	 */
	module.directive('workspaceJoinBtn', ['$window', 'WorkspaceService', function($window, WorkspaceService) {
		return {
			templateUrl : Coyo.scripts + '/views/coyo/workspace/join.html',
			restrict    : 'A',
			replace     : true,
			scope : {
				workspace : '=workspaceJoinBtn'
			},
			link : function(scope, element, attrs) {
				scope.joinWorkspace = function(id, joinRequest) {
					scope.inProgress = true;
					WorkspaceService.join(id).then(function(result) {
						if (result) {
							if (joinRequest) {
								scope.inProgress = false;
								WorkspaceService.updateWorkspace(result);
							} else {
								$window.location.href = scope.workspace.link;
							}
						}
					});
				};
			}
		};
	}]);
})();
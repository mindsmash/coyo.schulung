var module = angular.module('coyo.generic.filechooser', ['coyo.generic.filechooser.tpls', 'commons.upload']);

/**
 * Renders the file chooser. This field lets users upload various files. 
 */
module.directive('fileChooser', [function() {
	return {
		restrict    : 'EA',
		replace     : true,
		scope       : {
			ngModel : '=',
			field   : '='
		},
		templateUrl : 'template/filechooser.html',
		controller : ['$scope', function($scope) {
			var multiple = angular.isDefined($scope.field.multiple) ? $scope.field.multiple : true;
			
			$scope.ngModel = _.map($scope.ngModel, function(file) {
				return _.extend({
					uploadId : file.id,
					percent  : 100,
					name     : file.fileName
				}, file);
			});
			
			$scope.uploadOptions = {
				name     : 'item-attachments-' + $scope.field.id,
				browseId : 'upload-trigger-' + $scope.field.id,
				url      : '/uploads',
				multiple : multiple
			};
			
			/* check whether the model is set if the field is required */
			if ($scope.field.required) {
				$scope.$watch('ngModel', function(newVal, oldVal) {
					var valid = (angular.isDefined(newVal) && newVal.length > 0);
					$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : valid});
				}, true);
			}
			
			/* flag this field as invalid when upload is in progress */
			$scope.$on('upload-api:start', function(e, name, uploads) {
				$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : false});
			});
			$scope.$on('upload-api:complete', function(e, name, uploads) {
				$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : true});
			});
		}] 
	};
}]);


/**
 * Module holding all needed templates
 */
angular.module('coyo.generic.filechooser.tpls', ['template/filechooser.html']);

angular.module("template/filechooser.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/filechooser.html", 
		'<div class="file-chooser-container">' +
			'<div data-ng-model="ngModel" data-upload="uploadOptions">' + 
			'</div>' +
			'<a class="btn" href="" title="{{\'messaging.form.attachAFile\' | i18n}}" id="upload-trigger-{{field.id}}" data-upload-trigger="item-attachments-{{field.id}}">' + 
				'<i class="icon-file"></i>&nbsp;{{\'messaging.form.attachAFile\' | i18n}}' + 
			'</a>' +
		'</div>'
	);
}]);
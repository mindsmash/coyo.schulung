var app = angular.module('coyo.pluginhooks.directives', []);

app.directive('userIdeas', ['$filter', '$http', function ($filter, $http) {
	return {
		restrict: 'A',
		replace: false,
		templateUrl: Coyo.scripts + '/views/plugins/inno/userideas.html',
		scope: {},

		link: function (scope, elm, attrs) {
			scope.userIdeas = [];
			scope.itemsToShow = 5;

			scope.getRemainingDays = function (idea) {
				if (idea.status === "NEW" && idea.ttl > 0) {
					var today = moment();
					var expireDate = moment(idea.created).add(idea.ttl, 'days');
					var diff = expireDate.diff(today, 'days');
					if (diff === 0) {
						diff = (expireDate.diff(today) > 0 ) ? 1 : 0;
					}
					return diff;
				} else {
					return 0;
				}
			};
			
			scope.moreIdeas = function(event) {
				event.stopPropagation();
				scope.itemsToShow += 5;
			};


			scope.loadIdeas = function () {
				$http({
					cache: true,
					url: '/app/inno/userideas',
					method: 'GET',
					responseType: 'json'
				}).success(function (resp) {
					scope.userIdeas = resp;
				});
			};
		}
	};
}]);


app.directive('latestMembers', ['$filter', '$http', function ($filter, $http) {
	return {
		restrict: 'A',
		replace: true,
		templateUrl: Coyo.scripts + '/views/coyo/pluginhooks/latestMembers.html',
		scope: {},

		link: function (scope, elm, attrs) {
			scope.latestMembers = [];

			scope.findNewestMembers = function () {
				$http({
					cache: true,
					url: '/users/newMembers',
					method: 'GET',
					responseType: 'json'
				}).success(function (resp) {
					scope.latestMembers = resp;
				});
			};

			scope.findNewestMembers();
		}
	};
}]);
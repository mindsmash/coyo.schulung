var module = angular.module('coyo.likes.services', []);

// TODO: use ROUTES
module.service('LikesService', ['$http', '$q', function ($http, $q) {
	var LikesService = {
		toggle : function (likableID, likableClazz) {
			var deferred = $q.defer(),
				url = '/api/likes/' + likableID + '/' + likableClazz;

			$http.post(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		get : function (likableID, likableClazz) {
			var deferred = $q.defer(),
				url = '/api/likes/' + likableID + '/' + likableClazz;
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};

	return LikesService;
}]);
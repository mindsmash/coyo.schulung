var module = angular.module('coyo.likes.directives', []);

/**
 * Directive for a list of likes. Likable must define the following JSON properties: [id, clazz]
 */
module.directive('likeList', ['$rootScope', 'LikesService', '$filter', function ($rootScope, LikesService, $filter) {
	return {
		templateUrl: Coyo.scripts + '/views/coyo/likes/list.html',
		restrict: 'A',
		replace: true,
		scope: {
			likable : '=likeList'
		},
		link: function (scope, element, attrs) {
			// default value
			scope.max = attrs.max || 3;
			
			function arrange() {
				if (scope.likes.length > scope.max) {
					var names = '';
					for (var i = scope.max; i < scope.likes.length; i++) {
						if (i < scope.likes.length && i > scope.max) {
							names += ', ';
						}
						var user = scope.likes[i];

						if (user.currentUser) {
							names += $filter('i18n')('you.dative');
						} else {
							names += user.fullName;
						}
					}
					scope.others = names;
				}
			}
			
			// load likes
			function load(newValue, oldValue) {
				if(newValue === oldValue) {
					return;
				}
				
				scope.likes = [];
				LikesService.get(scope.likable.id, scope.likable.clazz).then(function(likes) {
					// sort current user to top
					scope.likes = _.sortBy(likes, 'currentUser').reverse();
					arrange();
				});
			}
			load(scope.likable, null);
			
			// register listener
			$rootScope.$on('like', function(e, data){
				if(data.likable.id === scope.likable.id && data.likable.clazz === scope.likable.clazz) {
					scope.likes = data.likes;
					arrange();
				}
			});
			
			// watch for likable change
			scope.$watch('likable', load);
		}
	};
}]);

/**
 * Directive for a like button. Likable must define the following JSON properties: [id, clazz, userLikes(boolean)]
 */
module.directive('likeButton', ['$rootScope', 'LikesService', function ($rootScope, LikesService) {
	return {
		templateUrl: Coyo.scripts + '/views/coyo/likes/button.html',
		restrict: 'A',
		replace: true,
		scope: {
			likable : '=likeButton'
		},
		link: function (scope, element, attrs) {
			scope.toggle = function() {
				scope.likable.userLikes = !scope.likable.userLikes;
				
				LikesService.toggle(scope.likable.id, scope.likable.clazz).then(function(likes){
					$rootScope.$emit('like', {
						likable: scope.likable,
						likes: likes
					});
				});
			};
		}
	};
}]);
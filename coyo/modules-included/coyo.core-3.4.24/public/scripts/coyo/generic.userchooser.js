var module = angular.module('coyo.generic.userchooser', ['coyo.generic.userchooser.tpls', 'ui.select2']);

/**
 * Renders the user chooser. An enhanced dropdown for the selection of one or
 * more users.
 */
module.directive('userChooser', ['$compile', function($compile) {
	return {
		restrict: 'E',
		scope: {
			options : '=',
			field   : '=',
			ngModel : '='
		},
		replace : true,
		templateUrl : 'template/userchooser.html',
		controller : ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			$scope.userChooserOptions = {
				allowClear   : true,
				placeholder     : ($scope.field.required) ? message('list.field.required') : message('list.field.optional'),
				multiple     : $scope.field.properties.multiple,
				ajax : {
					url      : $scope.options.serviceUrl,
					dataType : 'json',
					type     : 'POST',
					data     : function (term, page) {
						return {
							term          : term,
							showExternals : $scope.options.showExternals,
							showMe        : $scope.options.showMe
						};
					},
					results : function (data, page) {
						return {results : data, text : 'fullName'};
					}
				},
				formatResult: function(user, container, query, escapeMarkup) {
					var userResult = '<div class="user-chooser-result"><img src="' + user.thumbURL + '">';
					userResult += (user.department) ? escapeMarkup(user.fullName) + '<br/><span class="department">' + escapeMarkup(user.department) + '</span>' : escapeMarkup(user.fullName);
					userResult += '</div>';
					return userResult;
				},
				formatSelection: function(user, container, escapeMarkup) {
					return escapeMarkup(user.fullName);
				}
			};

			if (angular.isDefined($scope.options.width)) {
				$scope.userChooserOptions.width = $scope.options.width;
			}
			
			/* check whether the model is set if the field is required */
			if ($scope.field.required) {
				$scope.$watch('ngModel', function(newVal, oldVal) {
					var valid = (angular.isDefined(newVal) && !_.isEmpty(newVal));
					$scope.$emit('validityChanged', {fieldId : $scope.field.id, valid : valid});
				}, true);
			}
		}]
	};
}]);


/**
 * Module holding all needed templates
 */
angular.module('coyo.generic.userchooser.tpls', ['template/userchooser.html']);

angular.module("template/userchooser.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/userchooser.html", 
		'<div><div data-ui-select2="userChooserOptions" id="{{field.id}}" name="{{field.name}}" class="user-chooser" data-ng-model="ngModel" data-ng-required="field.required"></div></div>'
	);
}]);
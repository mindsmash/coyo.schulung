/* jshint -W055 */
(function () {
	var module = angular.module('coyo.admin.hashtags', ['coyo.generic', 'coyo.hashtags', 'coyo.generic.filters', 'ngTable']);

	module.controller('BlacklistController', ['$scope', '$filter', '$http', 'HashtagService', 'ngTableParams', function($scope, $filter, $http, HashtagService, ngTableParams) {
		$scope.blackTag = "";
		$scope.allowedHashtags = [];

		$scope.addToBlackList = function(tag) {
			if (HashtagService.isHashtag(tag)) {
				var plainTagName = HashtagService.removeLeadingHash(tag);
	
				HashtagService.addToBlackList(plainTagName, function(blacklist) {
					populateBlacklist(blacklist);
					blackListChanged();
				});
			}
		};

		$scope.removeFromBlackList = function(tag) {
			if (HashtagService.isHashtag(tag)) {
				var plainTagName = HashtagService.removeLeadingHash(tag);
	
				HashtagService.removeFromBlackList(plainTagName, function(blacklist) {
					populateBlacklist(blacklist);
					blackListChanged();
				});
			}
		};

		HashtagService.getList(0, function(hashtags) {
			$scope.hashTagList = convertList(hashtags);
			$scope.allowedHashtags = _.difference($scope.hashTagList, $scope.blacklist);
		}, false);

		HashtagService.getBlacklist(function(hashtags) {
			populateBlacklist(hashtags);

			$scope.allowedHashtags = _.difference($scope.hashTagList, $scope.blacklist);

			$scope.tableParams = new ngTableParams({
					page: 1,            // show first page
					count: 20          // count per page
				}, {
					getData: function ($defer, params) {
						params.total($scope.blacklist.length);
						$defer.resolve($scope.blacklist.slice((params.page() - 1) * params.count(), params.page() * params.count()));
					}
				}
			);
		});
		
		var blackListChanged = function() {
			$scope.allowedHashtags = _.difference($scope.hashTagList, $scope.blacklist);
			$scope.tableParams.reload();
		};

		var populateBlacklist = function(tags) {
			var newList = convertList(tags);
			newList.sort();
			$scope.blacklist = newList;
		};
		
		var convertList = function(tags) {
			var convertedList = [];
			_.each(tags, function(tag) {
				var tagName = (angular.isDefined(tag.text) ? tag.text : tag);
				convertedList.push('#' + tagName);
			});
			return convertedList;
		};

	}]);

})();
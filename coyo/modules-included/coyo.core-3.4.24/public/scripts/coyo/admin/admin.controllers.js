/*jshint -W055 */

var module = angular.module('coyo.admin.controllers', ['ngAnimate']);

/**
 * Controller for admin user panel. This is mainly required to display the list of users.
 */
module.controller('AdminUserController', ['$scope', '$resource', '$http', '$window', 'ngTableParams', function($scope, $resource, $http, $window, ngTableParams) {
	var User, Users;
	
	$scope.init = function(showDeleted) {
		Users = $resource('/admin/users/list', {showDeleted : showDeleted});
		User = $resource('/admin/users/:userId');
	};
	
	$scope.filters = {
		name : ''
	};
	
	$scope.deleteUser = function(userId) {
		User.remove({userId : userId}, function(response) {
			$window.location.reload();
		});
	};
	
	$scope.recoverUser = function(user) {
		$http.post(user.recoverUrl, {userId : user.id})
			.success(function(data, status, headers, config) {
				$window.location.reload();
			}
		);
	};

	$scope.deleteOAuthDevices = function(user) {
		$http['delete'](ROUTES.User_deleteOAuth2Devices.url({id: user.id})).then(function(response) {
			user.userDevices = [];
		});
	};
	
	$scope.tableParams = new ngTableParams({
		page    : 1,
		count   : 20,
		sorting : {name : 'asc'},
		filter  : $scope.filters
	}, {
		total   : 0,
        getData : function($defer, params) {
			Users.get(params.url(), function(data) {
				$scope.currentUser = _.findWhere(data.userlist, {me: true} );
				params.total(data.total);
				$defer.resolve(data.userlist);
			});
        }
	});
	
}]);

module.value("AdminTeaserControllerConstants", {
	defaultInterval: 8000,
	defaultHeight: 320,
	settingsMinInterval: 500,
	settingsMinHeight: 50,
	resetAlertsInterval: 5000,
	templateURLTeaserItem: Coyo.scripts + '/views/coyo/teaser/teaseritem.html',
	templateURLSettings: Coyo.scripts + '/views/coyo/teaser/teasersettings.html'
});

module.controller('AdminTeaserController', ['$scope', '$modal', '$timeout', 'teaserModel', 'ngTableParams', 'AdminTeaserControllerConstants', function($scope, $modal, $timeout, teaserModel, ngTableParams, AdminTeaserControllerConstants) {
	$scope.loading = true;
	$scope.editMode = false;
	$scope.errorFetchData = false;
	$scope.successSaveSettings = false;
	$scope.errorDelete = false;
	$scope.syncingOrder = false;
	$scope.errorSyncOrder = false;
	$scope.teasers = [];
	$scope.settings = {interval: AdminTeaserControllerConstants.defaultInterval, height: AdminTeaserControllerConstants.defaultHeight};
	var resetAlertsTimer = null;
	
	function startTimerResetAlerts (timeout) {
		resetAlertsTimer = $timeout(function() {
			resetAlerts();
		}, timeout);
	}
	
	function stopTimerResetAlerts() {
		if(resetAlertsTimer) {
			$timeout.cancel(resetAlertsTimer);
			resetAlertsTimer = null;
		}
	}
	
	function resetTimeResetAlerts() {
		stopTimerResetAlerts();
		startTimerResetAlerts(AdminTeaserControllerConstants.resetAlertsInterval);
	}
	
	function resetAlerts() {
		$scope.errorFetchData = false;
		$scope.errorDelete = false;
		$scope.successSaveSettings = false;
	}
	
	function sendTeaserOrder() {
		teaserModel.order($scope.teasers, function() {
			$scope.syncingOrder = false;
		}, function(data, status) {
			$scope.syncingOrder = false;
			$scope.errorSyncOrder = true;
		});
	}
	
	function updateTeaser(teaser, currTeaser) {
		if ((teaser != null) && (currTeaser != null)) {
			teaser.title = ((currTeaser.title != null) && (currTeaser.title.trim().length > 0)) ? currTeaser.title : "";
			teaser.description = ((currTeaser.description != null) && (currTeaser.description.trim().length > 0)) ? currTeaser.description : "";
			teaser.link = ((currTeaser.link != null) && (currTeaser.link.trim().length > 0)) ? currTeaser.link : "";
			teaser.externalLink = currTeaser.externalLink ? true : false;
			teaser.image = ROUTES.Teaser_teaserImage.url({id: teaser.id}) + "?" + new Date().getTime();
		}
	}

	resetAlerts();
	
	$scope.tableParams = new ngTableParams({
		page    : 1,
		count   : 10
	}, {
		total   : 0,
		getData: function ($defer, params) {
			teaserModel.getTeasers(params.url(), function(response) {
				params.total(response.total);
				$scope.teasers = response.teaserList;
				$defer.resolve($scope.teasers);
				$scope.updateTeaserImages();
				$scope.loading = false;
			}, function(data, status) {
				$scope.loading = false;
				$scope.errorFetchData = true;
			});
		}
	});
	
	teaserModel.getSettings(function(response) {
		var properties = response.properties;
		$scope.settings = {interval: parseInt(properties.interval), height: parseInt(properties.height)};
		$scope.tmpSettings = angular.copy($scope.settings);
	}, function(data, status) {
		// TODO
	});
	
	$scope.sortableOptions = {
		stop : function(e, ui) {
			$scope.syncingOrder = true;
			$scope.errorSyncOrder = false;
			sendTeaserOrder();
		},
		helper: function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		}
	};
	
	$scope.updateTeaserImages = function() {
		for(var i = 0; i < $scope.teasers.length; ++i) {
			$scope.teasers[i].image = ROUTES.Teaser_teaserImage.url({id: $scope.teasers[i].id});
		}
	};

	$scope.newTeaser = function() {
		resetAlerts();
		
		$scope.currentTeaser = {author: "", title: "", description: "", link: "", externalLink: false, image: ""};
		
		var modalInstance = $modal.open({
			templateUrl : AdminTeaserControllerConstants.templateURLTeaserItem,
			controller  : 'AdminTeaserFormController',
			scope       : $scope
		});
		
		// update list when new teaser was created
		modalInstance.result.then(function(result) {
			if(result) {
				$scope.teasers.push(result);
				$scope.currentTeaser = null;
				$scope.updateTeaserImages();
				sendTeaserOrder();
			}
		});
	};
	
	$scope.deleteTeaser = function(index) {
		resetAlerts();
		
		var teaser = $scope.teasers[index];
		if (teaser != null) {
			teaserModel.deleteTeaser(teaser.id, function() {
				$scope.teasers.splice(index, 1);
			}, function(data, status) {
				$scope.errorDelete = true;
				resetTimeResetAlerts();
			});
		}
	};
	
	$scope.updateTeaser = function(index, active) {
		resetAlerts();
		
		var teaser = $scope.teasers[index];
		if (teaser != null) {
			teaserModel.updateTeaser(teaser.id, active, function(response) {
				teaser.active = active;
			});
		}
	};

	$scope.editTeaser = function(index) {
		resetAlerts();
		
		$scope.editMode = true;
		
		var currentTeaser = $scope.teasers[index];
		
		$scope.currentTeaser = angular.copy(currentTeaser);

		var modalInstance = $modal.open({
			templateUrl : AdminTeaserControllerConstants.templateURLTeaserItem,
			controller  : 'AdminTeaserFormController',
			scope       : $scope
		});
		
		// update teaser when a teaser has been edited
		modalInstance.result.then(function(result) {
			$scope.editMode = false;
			if(result) {
				updateTeaser(currentTeaser, $scope.currentTeaser);
			}
		}, function() {
			$scope.editMode = false;
		});
	};

	$scope.showSettings = function(index) {
		resetAlerts();

		var modalInstance = $modal.open({
			templateUrl : AdminTeaserControllerConstants.templateURLSettings,
			controller  : 'AdminTeaserFormSettingsController',
			scope       : $scope
		});
		
		modalInstance.result.then(function(result) {
			if (result) {
				var properties = result.properties;
				$scope.settings = {interval: parseInt(properties.interval), height: parseInt(properties.height)};
				$scope.tmpSettings = angular.copy($scope.settings);
				$scope.successSaveSettings = true;
				resetTimeResetAlerts();
			}
		});
	};
}]);

module.controller('AdminTeaserFormSettingsController', ['$scope', '$modalInstance', '$window', 'teaserModel', 'AdminTeaserControllerConstants', function($scope, $modalInstance, $window, teaserModel, AdminTeaserControllerConstants) {
	$scope.tmpSettings = angular.copy($scope.settings);
	
	// dismiss window
	$scope.cancel = function() {
		$modalInstance.close();
	};
	
	$scope.saveSettings = function() {
		$scope.settings = angular.copy($scope.tmpSettings);
		teaserModel.saveSettings($scope.settings, function(response) {
			$modalInstance.close(response);
		}, function(data, status) {
			$scope.error = true;
		});
	};
}]);

module.controller('AdminTeaserFormController', ['$scope', '$modalInstance', '$window', 'teaserModel', function($scope, $modalInstance, $window, teaserModel) {
	$scope.imageField = {id : "teaser", multiple : false, required : true};
		
	// create new teaser
	$scope.create = function(teaser) {
		teaserModel.save(teaser, function(response) {
			$modalInstance.close(response);
		}, function(data) {
			$scope.errors = data.errors;
		});
	};
	
	// dismiss window
	$scope.cancel = function() {
		$modalInstance.close();
	};

	// edit teaser
	$scope.edit = function(teaser) {
		teaserModel.save(teaser, function(response) {
			$modalInstance.close(response);
		}, function(data) {
			$scope.errors = data.errors;
		});
	};
}]);

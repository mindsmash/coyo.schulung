var module = angular.module('coyo.account.controllers', []);

module.controller('PushDevicesController', ['$scope', 'PushDeviceModel', function($scope, PushDeviceModel) {
	$scope.devices = [];
	
	// get all devices initially
	PushDeviceModel.getPushDevices().then(function(devices) {
		$scope.devices = devices;
	});
	
	// delete a device
	$scope.remove = function(device) {
		PushDeviceModel.deletePushDevice(device).then(function() {
			$scope.devices = _.without($scope.devices, device);
		});
	};
	
	// toggle active / inactive
	$scope.toggle = function(device) {
		device.active = !device.active; 
		PushDeviceModel.savePushDevice(device).then(function(devices) {
			$scope.devices = devices;
		});
	};
}]);

module.controller('OAuth2DevicesController', ['$scope', 'OAuth2Service', function($scope, OAuth2Service) {
	$scope.devices = [];

	// get all devices initially
	OAuth2Service.getOAuth2Devices().then(function(response) {
		$scope.devices = response.data;
	});

	// delete a device
	$scope.deleteOAuth2Device = function(device) {
		OAuth2Service.deleteOAuth2Device(device).then(function() {
			$scope.devices = _.without($scope.devices, device);
		});
	};
}]);
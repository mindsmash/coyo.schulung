var module = angular.module('coyo.account.services', []);

/*
 * Service to load push devices.
 */
module.factory('PushDeviceModel', ['$http', '$q', function($http, $q) {
	var PushDeviceModel = {
		getPushDevices : function() {
			var deferred = $q.defer(),
				url = ROUTES.User_getPushDevices.url();
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		savePushDevice : function(device) {
			var deferred = $q.defer(),
				url = ROUTES.User_savePushDevice.url(),
				payload = { id : device.id, type : device.type, token : device.token, active: device.active };
			
			$http.post(url, payload).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			
			return deferred.promise;
		},
		deletePushDevice: function(device) {
			var deferred = $q.defer(),
				url = ROUTES.User_deletePushDevice.url({id: device.id});
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
	
	return PushDeviceModel;
}]);


module.factory('OAuth2Service', ['$http', function ($http) {
	return {
		getOAuth2Devices: function () {
			return $http.get(ROUTES.User_getOAuth2Devices.url());
		},
		deleteOAuth2Device: function (device) {
			return $http['delete'](ROUTES.User_deleteOAuth2Device.url({id: device.id}));
		}
	};
}]);
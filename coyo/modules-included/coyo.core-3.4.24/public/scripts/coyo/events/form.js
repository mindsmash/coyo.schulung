(function() {
	var module = angular.module('coyo.events.form',
			[ 'coyo.generic', 'coyo.events.directives', 'localytics.directives' ])

	// controller is needed as glue between server rendered template and the Angular directive
	.controller(
			'EventsFormController',
			[
					'$log',
					'$scope',
					'$window',
					function($log, $scope, $window) {
						$scope.rrule = {};
						var originalRrule, originalStartDate, originalEndDate, originalCalendarId;

						// watch changes to RRULE and trigger event if necessary
						$scope.$watch('rrule', function(newValue, oldValue) {
							if (angular.isUndefined(originalRrule)) {
								originalRrule = newValue.string;
							}

							if (angular.isDefined(newValue) && !angular.equals(newValue, oldValue)) {
								angular.element('body').trigger('rrule-changed', {
									newValue : newValue.string,
									oldValue : oldValue.string,
									originalValue : originalRrule
								});
							}
						}, true);

						// watch other properties to be able to tell if creation of series has to be forced
						$scope.$watch('startDate', function(newValue, oldValue) {
							if (angular.isUndefined(originalStartDate)) {
								originalStartDate = newValue;
							}
						});
						$scope.$watch('endDate', function(newValue, oldValue) {
							if (angular.isUndefined(originalEndDate)) {
								originalEndDate = newValue;
							}
						});
						$scope.$watch('calendarId', function(newValue, oldValue) {
							if (angular.isUndefined(originalCalendarId)) {
								originalCalendarId = newValue;
							}
						});

						$scope.creationOfNewSeriesRequired = function() {
							return (!angular.equals(originalRrule, $scope.rrule.string))
									|| (!angular.equals(originalStartDate, $scope.startDate))
									|| (!angular.equals(originalEndDate, $scope.endDate))
									|| (!angular.equals(originalCalendarId, $scope.calendarId));
						};
						$scope.submit = function(updateType) {
							if ('ONLY_THIS' === updateType && $scope.creationOfNewSeriesRequired()) {
								// this is not allowed
								return;
							}

							// WORKAROUND - we need to submit the form in a traditional way but triggered by Angular
							$scope.updateType = updateType;
							$scope.$broadcast('submitForm');
						};

						// WORKAROUND - we need to bind the global calendar array to our scope
						$scope.calendars = $window.eventCalendars;
					} ]);
})();

(function() {
	var module = angular.module('coyo.event.services', []);

	module.factory('CalendarService', ['$http', '$q', function($http, $q) {
		var CalendarService = {
			getUsersByResponse : function(id) {
				var deferred = $q.defer(),
					url = ROUTES.Event_users.url({id : id});
				
				$http.get(url).success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
				return deferred.promise;
			}
		};
		return CalendarService;
	}]);
})();
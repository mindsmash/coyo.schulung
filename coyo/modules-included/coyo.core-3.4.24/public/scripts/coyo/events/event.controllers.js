(function() {
	var module = angular.module('coyo.event.controllers', []);

	module.controller('EventUserListController', ['$scope', 'CalendarService', 'EventService', 'CoyoUtils', function($scope, CalendarService, EventService, CoyoUtils) {
		$scope.usersPerReponse = {};
		$scope.chatEnabled = false;
		$scope.audioVideoEnabled = false;
		$scope.loading = false;
		
		$scope.init = function(id, chatEnabled, audioVideoEnabled) {
			$scope.loading = true;
			$scope.chatEnabled = chatEnabled;
			$scope.audioVideoEnabled = audioVideoEnabled;
			CalendarService.getUsersByResponse(id).then(function(result) {
				$scope.usersPerReponse = result;
				$scope.loading = false;
			});
		};

		/*
		 * Subscribe to event service to get notified of all changed user status.
		 */
		EventService.subscribe('onlineStatusChanged', function (event) {
			var payload = event.object;

			// update status in colleagues list
			var user = _.find($scope.usersPerReponse.ATTENDING.userList, function (user) {
				return user.id === payload.userId;
			});
			if (!angular.isDefined(user)) {
				user = _.find($scope.usersPerReponse.MAYBE.userList, function (user) {
					return user.id === payload.userId;
				});
			}
			if (!angular.isDefined(user)) {
				user = _.find($scope.usersPerReponse.NOT_ATTENDING.userList, function (user) {
					return user.id === payload.userId;
				});
			}
			if (!angular.isDefined(user)) {
				user = _.find($scope.usersPerReponse.WAITING.userList, function (user) {
					return user.id === payload.userId;
				});
			}

			if (angular.isDefined(user)) {
				user.status = payload.newStatus;
				user.isCallable = payload.isCallable;
			}
		});
		
		/*
		 * function for ordering members by status (Online > Busy > Away > Offline)
		 */
		$scope.orderByStatus = function(user) {
			return CoyoUtils.orderByStatus(user);
		};
		
	}]);
})();
(function() {
	'use strict';

	angular.module('coyo.event', ['coyo.event.controllers', 'coyo.event.services']);
})();
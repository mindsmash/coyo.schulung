(function() {
	var module = angular
			.module('coyo.events.directives', [])

			.filter(
					'descriptiveWeekdayList',
					[
							'$filter',
							'$window',
							function($filter, $window) {
								return function(weekdays) {
									var days = String(weekdays), _ = $window._, sortedWeekdays = [ 'MO', 'TU', 'WE',
											'TH', 'FR', 'SA', 'SU' ];
									var allWeekdaysSelected = (days.indexOf('MO') !== -1 && days.indexOf('TU') !== -1
											&& days.indexOf('WE') !== -1 && days.indexOf('TH') !== -1 && days
											.indexOf('FR') !== -1), weekendSelected = (days.indexOf('SA') !== -1 || days
											.indexOf('SU') !== -1), allWeekendSelected = (days.indexOf('SA') !== -1 && days
											.indexOf('SU') !== -1);
									if (allWeekdaysSelected && !weekendSelected) {
										return $filter('i18n')('event.series.rrule.weekdays.everyWeekday');
									} else if (allWeekdaysSelected && allWeekendSelected) {
										return $filter('i18n')('event.series.rrule.weekdays.everyDay');
									}

									weekdays = _.sortBy(weekdays, function(day) {
										return _.indexOf(sortedWeekdays, day);
									});
									var formatted = _.map(weekdays, function(day) {
										return $filter('i18n')('event.series.rrule.weekdays.' + day);
									}).join(', '), lastIndexOfSeperator = formatted.lastIndexOf(',');
									if (lastIndexOfSeperator >= 0) {
										formatted = formatted.substr(0, lastIndexOfSeperator) + ' '
												+ $filter('i18n')('and') + formatted.substr(lastIndexOfSeperator + 1);
									}
									return formatted;
								};
							} ])

			.directive('submitOn', [ '$timeout', function($timeout) {
				return {
					resrict : 'A',
					require : '^form',
					link : function(scope, element, attrs) {
						scope.$on(attrs.submitOn, function() {
							$timeout(function() {
								element.trigger('submit');
							});
						});
					}
				};
			} ])

			// Recurrence Rule UI widget
			.directive(
					'recurrenceRule',
					[
							'$filter',
							'$window',
							function($filter, $window) {
								return {
									restrict : 'A',
									scope : {
										'model' : '=',
										'locale' : '@',
										'dateFormatPicker' : '@',
										'dateFormatIso' : '@',
										'startDate' : '=',
										'startDateInvalid' : '='
									},
									templateUrl : Coyo.scripts + '/views/coyo/events/recurrence-rule.html',
									replace : true,
									link : function(scope, element) {
										var moment = $window.moment, _ = $window._, RRule = $window.RRule, fromRRule = function(
												rrule) {
											return {
												freq : RRule.FREQUENCIES[rrule.options.freq],
												interval : !!rrule.options.interval ? rrule.options.interval : 1,
												end : !!rrule.options.until ? 'ON_DATE' : 'NEVER',
												until : !!rrule.options.until ? moment.utc(rrule.options.until).format(
														scope.dateFormatIso) : null,
												weekdays : (function(weekdays) {
													var weekdayMap = {};
													weekdayMap[RRule.MO.weekday] = 'MO';
													weekdayMap[RRule.TU.weekday] = 'TU';
													weekdayMap[RRule.WE.weekday] = 'WE';
													weekdayMap[RRule.TH.weekday] = 'TH';
													weekdayMap[RRule.FR.weekday] = 'FR';
													weekdayMap[RRule.SA.weekday] = 'SA';
													weekdayMap[RRule.SU.weekday] = 'SU';
													return _.map(weekdays, function(weekday) {
														return weekdayMap[weekday];
													});
												}(rrule.options.byweekday))
											};
										}, toRRule = function(rrule) {
											return new RRule({
												freq : (function(freq) {
													switch (freq) {
													case 'DAILY':
														return RRule.DAILY;
													case 'WEEKLY':
														return RRule.WEEKLY;
													case 'MONTHLY':
														return RRule.MONTHLY;
													case 'YEARLY':
														return RRule.YEARLY;
													}
												}(rrule.freq)),
												interval : angular.isNumber(rrule.interval) ? rrule.interval : null,
												byweekday : (function(weekdays) {
													var weekdayMap = {
														'MO' : RRule.MO,
														'TU' : RRule.TU,
														'WE' : RRule.WE,
														'TH' : RRule.TH,
														'FR' : RRule.FR,
														'SA' : RRule.SA,
														'SU' : RRule.SU
													};
													return _.map(weekdays, function(weekday) {
														return weekdayMap[weekday];
													});
												}(rrule.weekdays)),
												until : angular.isUndefined(rrule.until) ? null : moment.utc(
														rrule.until, scope.dateFormatIso)
											});
										};

										// init RRULE from existing model or create default
										scope.rrule = scope.model ? fromRRule(RRule.fromString(scope.model)) : {
											'freq' : 'NONE',
											'interval' : 1,
											'end' : 'NEVER',
											'weekdays' : []
										};

										scope.freqValues = [ {
											'value' : 'NONE',
											'label' : $filter('i18n')('event.series.rrule.freq.NONE.label')
										}, {
											'value' : 'DAILY',
											'label' : $filter('i18n')('event.series.rrule.freq.DAILY.label')
										}, {
											'value' : 'WEEKLY',
											'label' : $filter('i18n')('event.series.rrule.freq.WEEKLY.label')
										}, {
											'value' : 'MONTHLY',
											'label' : $filter('i18n')('event.series.rrule.freq.MONTHLY.label')
										}, {
											'value' : 'YEARLY',
											'label' : $filter('i18n')('event.series.rrule.freq.YEARLY.label')
										} ];
										scope.endValues = [ {
											'value' : 'NEVER',
											'label' : $filter('i18n')('event.series.rrule.end.NEVER.label')
										}, {
											'value' : 'ON_DATE',
											'label' : $filter('i18n')('event.series.rrule.end.ON_DATE.label')
										} ];

										scope.intervalShown = function() {
											return scope.endShown();
										};
										scope.endShown = function() {
											return angular.isDefined(scope.rrule.freq)
													&& !angular.equals(null, scope.rrule.freq)
													&& !angular.equals('', scope.rrule.freq)
													&& !angular.equals('NONE', scope.rrule.freq);
										};
										scope.endDateShown = function() {
											return scope.endShown() && angular.equals('ON_DATE', scope.rrule.end);
										};
										scope.weekdaySelectionShown = function() {
											return angular.equals('DAILY', scope.rrule.freq)
													|| angular.equals('WEEKLY', scope.rrule.freq);
										};

										scope.weekdayIsSelected = function(wd) {
											return _.contains(scope.rrule.weekdays, wd);
										};
										scope.toggleWeekdaySelection = function(weekday) {
											if (_.contains(scope.rrule.weekdays, weekday)) {
												scope.rrule.weekdays = _.without(scope.rrule.weekdays, weekday);
												return;
											}
											scope.rrule.weekdays.push(weekday);
										};


										scope.$watch('startDate', function(newValue) {
											if (angular.isUndefined(newValue) || angular.isUndefined(scope.rrule.until)) {
												return;
											}

											scope.startDateInvalid = newValue > scope.rrule.until;

											if (scope.startDateInvalid) {
												angular.element('.control-group.start-date').addClass('error');
											} else {
												angular.element('.control-group.start-date').removeClass('error');
											}

											if (scope.startDateInvalid) {
												scope.startDate = undefined;
												return;
											}
										}, true);

										scope.$watch('rrule', function(newValue, oldValue) {
											if (angular.isUndefined(newValue) || angular.equals(newValue, oldValue)) {
												return;
											}

											scope.eventSeriesInvalid = scope.startDate > newValue.until;
											
											if (scope.eventSeriesInvalid) {
												newValue.until = undefined;
												return;
											}
											
											// reset dependent values if necessary
											if (!scope.endShown()) {
												scope.model = null;
												return;
											}

											if (!scope.endDateShown()) {
												newValue.end = 'NEVER';
												delete newValue.until;
											}

											if (!scope.weekdaySelectionShown()) {
												scope.rrule.weekdays = [];
											}

											// create RRULE object and write to model
											var rruleObject = toRRule(scope.rrule);
											if (rruleObject !== null) {
												scope.model = rruleObject.toString();
											}
										}, true);

										// update dependent values of frequency
										scope.updateFrequencyDependentValues = function(newFreq) {
											if (newFreq === 'DAILY') {
												scope.rrule.weekdays = [ 'MO', 'TU', 'WE', 'TH', 'FR' ];
											} else if (newFreq === 'WEEKLY') {
												var referenceDate = !!scope.startDate ? scope.startDate : moment()
														.utc();
												// set to weekday of selected start date or today
												var isoWeekdays = [ '', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU' ];
												scope.rrule.weekdays = [ isoWeekdays[moment.utc(referenceDate,
														scope.dateFormatIso).isoWeekday()] ];
											}
										};
									}
								};
							} ]);
})();

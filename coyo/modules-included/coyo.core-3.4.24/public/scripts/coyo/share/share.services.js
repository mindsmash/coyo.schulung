var module = angular.module('coyo.share.services', []);

module.service('ShareService', ['$http', '$q', '$log', function ($http, $q, $log) {
	var ShareService = {
		share : function (id, clazz, wallId, message) {
			var deferred = $q.defer(),
				url = ROUTES.Share_share.url(),
				data = {
					id      : id,
					clazz   : clazz,
					wall    : wallId,
					message : message
				};

			$http.post(url, data).success(function(data, status, headers, config) {
				deferred.resolve(data);
			}).error(function(error) {
				$log.error('Entity with id [' + id + '] could not be shared.', error);
			});
			return deferred.promise;
		},
		getSender : function () {
			var deferred = $q.defer(),
				url = ROUTES.Share_senderList.url();
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data.members);
			}).error(function(error) {
				$log.error('Could not fetch list of senders.', error);
			});
			return deferred.promise;
		}
	};

	return ShareService;
}]);
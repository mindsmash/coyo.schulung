var module = angular.module('coyo.share.directives', ['ui.select2']);

/**
 * Renders the wall chooser. An enhanced dropdown for the selection of one wall
 * to share something to.
 * 
 * This select element provides selection of all workspaces, pages, events and
 * other users, where the current user can possibly share content on the wall.
 * The attached ngModel then contains the selected wall id.
 */
module.directive('wallChooser', ['$compile', 'ShareService', function($compile, ShareService) {
	return {
		restrict: 'EA',
		scope: {
			ngModel : '=',
			options : '=wallChooser'
		},
		replace : true,
		templateUrl : Coyo.scripts + '/views/coyo/wall/wallchooser.html',
		controller : ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			ShareService.getSender().then(function(senderList) {
				$scope.senderList = senderList;
			});
		}]
	};
}]);
/* jshint -W055  */
var module = angular.module('coyo.calls.controllers', []);

module.controller('CallsController', ['$scope', '$modal', 'ngTableParams', 'CallsModel', function($scope, $modal, ngTableParams, CallsModel) {
	$scope.total = 0;
	
	/* configure table to show all calls with pagination */
	$scope.tableParams = new ngTableParams({
		page    : 1,
		count   : 20,
		sorting : {date : 'desc'}
	}, {
		total   : 0,
		getData : function($defer, params) {
			CallsModel.getCalls(params.url()).then(function(calls) {
				params.total(calls.total);
				$defer.resolve(calls.list);
				$scope.total = calls.total;
			});
        }
	});

	$scope.showSummary = function(call) {
		return call.summary && !_.isEmpty(call.summary.trim());
	};

	$scope.showCall = function(call) {
		if($scope.showSummary(call)) {
			$scope.currentCall = call;
			$scope.date = new Date(call.created);

			/* create modal */
			var modalInstance = $modal.open({
				templateUrl : Coyo.scripts + '/views/coyo/webrtc/calldetails.html',
				controller  : 'CallsController',
				windowClass : 'modal-scroll',
				scope       : $scope
			});

			/* reset temporary item model */
			modalInstance.result.then(function() {}, _cleanUp);
		}
	};

	/* cleans up after a modal was closed */
	var _cleanUp = function() {
		$scope.currentCall = null;
		$scope.date = null;
		$scope.summaryEmpty = false;
	};
}]);

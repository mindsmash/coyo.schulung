var module = angular.module('coyo.calls.services', []);

module.factory('CallsModel', ['$http', '$q', function($http, $q) {
	var CallsModel = {
		getCalls : function(params) {
			var deferred = $q.defer(),
				url = ROUTES.Calls_getCalls.url();
		
			$http.get(url, {params : params})
				.success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
			return deferred.promise;
		},
		addCall : function(status, participants) {
			var deferred = $q.defer(),
				url = ROUTES.Calls_addCall.url();
			
			$http.post(url, {status : status, participants : participants})
				.success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
			return deferred.promise;
			
		},
		save : function(call, context) {
			var deferred = $q.defer();
			var url = ROUTES.Calls_save.url({
				'id'   : (context !== null) ? context.id : null,
				'clazz': (context !== null) ? context.clazz : null
			});

			$http.post(url, call)
				.success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
			return deferred.promise;
		}
	};
	
	return CallsModel;
}]);

var module = angular.module('coyo.online.service', []);

module.service('OnlineService', ['$http', '$window', '$log', function ($http, $window, $log) {
	var BUSY_KEY = 'BUSY';

	var basilOptions = {
		namespace: 'mindsmash.webrtc.activetab',
		storages: ['local', 'cookie', 'session', 'memory'],
		expireDays: 365
	};
	var basil = new window.Basil(basilOptions);

	var service = {

		getStatus: function (onSuccess, onError) {
			$http({
				method: 'GET',
				url: $window.ROUTES.User_getStatus.url()
			}).success(function (data, status, headers, config) {
				if(_.isFunction(onSuccess)) {
					onSuccess(data, status, headers, config);
				}
			}).error(function (data, status, headers, config) {
				if(_.isFunction(onError)) {
					onError(data, status, headers, config);
				}
			});
		},

		getStatusOf: function (id, onSuccess, onError) {
			$http({
				method: 'GET',
				url: $window.ROUTES.User_getStatusOf.url({id: id})
			}).success(function (data, status, headers, config) {
				if(_.isFunction(onSuccess)) {
					onSuccess(data, status, headers, config);
				}
			}).error(function (data, status, headers, config) {
				if(_.isFunction(onError)) {
					onError(data, status, headers, config);
				}
			});
		},

		setStatus: function (status, onSuccess, onError) {
			$http({
				method: 'POST',
				url: $window.ROUTES.User_setStatus.url(),
				data: status
			}).success(function (response) {
				if(_.isFunction(onSuccess)) {
					onSuccess(response);
				}
			}).error(function (data, status, headers, config) {
				if(_.isFunction(onError)) {
					onError(data, status, headers, config);
				}
			});
		},

		/**
		 * Reads the users current online status and stores it in the local storage.
		 * Afterwards the users status is set to busy.
		 */
		setBusy: function () {
			var that = this;
			that.getStatus(function (data, status) {
				that.setStatus('BUSY', function () {
					basil.set(BUSY_KEY, data.status);
					$log.debug('[WebRTC] Successfully set busy state');
				}, function () {
					$log.error('[WebRTC] Failed to set busy state');
				});
			});
		},

		/**
		 * Reads the previous user online status from the clients local storage.
		 * If found it will restore the users online status and deletes the entry from the local storage.
		 */
		releaseBusy: function () {
			var previousUserOnlineStatus = basil.get(BUSY_KEY);
			if (previousUserOnlineStatus) {
				this.setStatus(previousUserOnlineStatus, function () {
					basil.remove(BUSY_KEY);
					$log.debug('[WebRTC] Successfully restored previous online status: ' + previousUserOnlineStatus);
				}, function () {
					$log.error('[WebRTC] Failed to restore previous online status: ' + previousUserOnlineStatus);
				});
			} else {
				$log.debug("[WebRTC] Failed to read previous user online status during release of busy state");
			}
		},

		/**
		 * Test whether an entry of the users previous online status is present within the clients local storage.
		 */
		testBusy: function () {
			return basil.get(BUSY_KEY) ? true : false;
		}
	};

	return service;
}]);
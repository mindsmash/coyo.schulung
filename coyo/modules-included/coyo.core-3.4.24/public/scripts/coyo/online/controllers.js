(function () {
	var module = angular.module('coyo.online.controllers', []);

	module.controller('OnlineUserListController', ['$scope', '$http', '$window', 'EventService', 'CoyoUtils', 'WEBRTC_CONSTANTS', function ($scope, $http, $window, EventService, CoyoUtils, WEBRTC_CONSTANTS) {
		var _ = $window._;

		/*
		 * initialize controller
		 */
		$scope.init = function (userCount, chatEnabled, audioVideoEnabled) {
			$scope.onlineUserCount = userCount;
			$scope.chatEnabled = chatEnabled;
			$scope.audioVideoEnabled = audioVideoEnabled;
		};

		/*
		 * Get list of all colleagues of current user
		 */
		$scope.loading = true;
		$http({
			method: 'GET',
			url: $window.ROUTES.User_colleagues.url()
		}).success(function (response) {
			$scope.colleagues = response;
			$scope.loading = false;
		});

		/*
		 * Subscribe to event service to get notified of all changed user status.
		 */
		EventService.subscribe(WEBRTC_CONSTANTS.EVENTS.STATUS.ONLINECHANGED, function (event) {
			var payload = event.object;

			// update online user count
			$scope.onlineUserCount = payload.onlineUserCount;

			// update status in colleagues list
			var colleague = _.find($scope.colleagues, function (colleague) {
				return colleague.id === payload.userId;
			});
			if (angular.isDefined(colleague)) {
				colleague.status = payload.newStatus;
				colleague.isCallable = payload.isCallable;
			}
		});

		/**
		 * Limit number of colleagues and show all when requested
		 */
		$scope.quantity = 10;
		$scope.showAll = function () {
			$scope.quantity = $scope.colleagues.length;
		};

		/*
		 * function for ordering colleagues by status (Online > Away > Offline)
		 */
		$scope.orderByStatus = function (colleague) {
			return CoyoUtils.orderByStatus(colleague);
		};
	}]);

	module.controller('StatusCtrl', ['$scope', '$http', '$window', '$log', 'EventService', 'OnlineService', 'WEBRTC_CONSTANTS', function ($scope, $http, $window, $log, EventService, OnlineService, WEBRTC_CONSTANTS) {
		$scope.status = [
			WEBRTC_CONSTANTS.USER.STATUS.ONLINE,
			WEBRTC_CONSTANTS.USER.STATUS.BUSY,
			WEBRTC_CONSTANTS.USER.STATUS.AWAY,
			WEBRTC_CONSTANTS.USER.STATUS.OFFLINE
		];
		var defaultStatus = $scope.status[3];
		$scope.currentStatus = null;
		$scope.currentProfileId = -1;

		/*
		 * Subscribe to event service to get notified of all changed user status.
		 */
		EventService.subscribe(WEBRTC_CONSTANTS.EVENTS.STATUS.ONLINECHANGED, function (event) {
			/* Make sure the event profile ID equals the current profile ID */
			if(event.object && event.object.userId && (event.object.userId === $scope.currentProfileId)) {
				if(event.object.newStatus) {
					$scope.currentStatus.status = event.object.newStatus;
				} else {
					$scope.getStatusOf(event.object.userId);
				}
			}
		});

		$scope.getStatus = function () {
			OnlineService.getStatus(function (data) {
				$scope.currentStatus = data;
			}, function (data, status, headers, config) {
				$scope.currentStatus = defaultStatus;
			});
		};

		$scope.getStatusOf = function (id) {
			$scope.currentProfileId = id;
			OnlineService.getStatusOf(id, function (data) {
				$scope.currentStatus = data;
			}, function (data, status, headers, config) {
				$scope.currentStatus = defaultStatus;
			});
		};

		$scope.setStatus = function (s) {
			OnlineService.setStatus(s, function (data) {
				$scope.currentStatus = data;
			});
		};

		/**
		 * if a previous online status is present in the local storage we assume, we did not restore the user online status and attempt to  do so now...
		 */
		if (OnlineService.testBusy()) {
			$log.warn("[OnlineUserListController] Trying to restore the users online status");
			OnlineService.releaseBusy();
			$scope.getStatus();
		}
	}]);
})();

(function() {
	'use strict';

	angular.module('coyo.online', [ 'coyo.online.controllers', 'coyo.online.service']);
})();
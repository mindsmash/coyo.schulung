(function() {
	/**
	 * The coyo.messaging.directives.
	 */
	var module = angular.module('coyo.messaging.directives', []);

	// adjust stream on scroll up
	module.directive('streamAdjust', function() {
		return function(scope, element, attrs) {
				// height before collection change
			var _oldHeight = 0,
				// height to hold during attachment loads
				_holdHeight = 0;
			
			
			// check if scrolling is needed and update element.
			function adjust() {
				if(element.height() < element.parent().height()) {
					element.css('position', 'absolute');
				} else {
					element.css('position', 'static');
				}
			}
			
			// loaded old posts -> top change -> hold scroll pos
			function top(remember) {
				_.defer(function() {
					var elementHeight = element.height();
					
					adjust();
					
					// use _holdHeight on subsequent element height updates
					if (remember) {
						// use _old height 
						_holdHeight = _oldHeight;
					}
					_oldHeight = elementHeight;
					element.parent().scrollTop(elementHeight - _holdHeight);
				});
			}
			
			// new post -> bottom change -> scroll to bottom
			function bottom() {
				_.defer(function() {
					adjust();					
					// scroll to bottom
					element.parent().scrollTop(element.height());
					
					// capture height of scroll pane
					_oldHeight = element.height();
				});
			}
			
			var _reset = false,
				// latch function -> unlock after 2 calls
				_unlock = angular.noop,
				// adjust function for current scroll direction 
				_direction = angular.noop;
			
			scope.$watch(attrs.streamAdjust, function() {
				// new post collection loaded - reset everything
				_oldHeight = 0;
				_direction = bottom;
				bottom();
				_reset = true;
				_unlock = _.after(2, function() {
					_reset = false;
				});
				

				countTop = 0;
				countBottom = 0;
			});
			
			scope.$watch(attrs.streamAdjust + '[' + attrs.streamAdjust + '.length -1]', function() {
				// new post
				if (!_reset) {
					_direction = bottom;
					bottom();
				}
				_unlock();
			});
			
			scope.$watch(attrs.streamAdjust + '[0]', function() {
				// old posts loaded
				if (!_reset) {
					_direction = top;
					top(true);
				}
				_unlock();
			});
			
			scope.$on('wall-post-attachments:loaded', function() {
				_direction();
			});
		};
	});

	// trigger scope function when scroll up
	module.directive('endlessScrollTop', function() {
		return function(scope, element, attrs) {
			element.on('scroll', _.throttle(function() {
				if ((element.scrollTop()) <= 200 && (!attrs.enabled || (attrs.enabled && scope.$eval(attrs.enabled)))) {
					scope.$eval(attrs.endlessScrollTop);
				}
			}, 200));
		};
	});
	
	// delete control for conversations
	module.directive('conversationDelete', ['$rootScope', function($rootScope) {
		return {
			scope : {
				conversation : '=conversationDelete'
			},
			link : function(scope, element, attrs) {
				element.on('click', function(e) {
					scope.conversation['delete']().then(function() {
						$rootScope.$broadcast('coyo.messaging.conversation-deleted', scope.conversation);
					});

					e.preventDefault();
					e.stopPropagation();
				});
			}
		};
	}]);

	// move control for conversations
	module.directive('conversationMove', ['$rootScope', function($rootScope) {
		return {
			scope : {
				conversation : '=conversationMove',
				status : '@'
			},
			link : function(scope, element, attrs) {
				element.on('click', function(e) {
					scope.conversation[scope.status]().then(function() {
						$rootScope.$broadcast('coyo.messaging.conversation-moved', scope.conversation);
					});

					e.preventDefault();
					e.stopPropagation();
				});
			}
		};
	}]);

	// invitation control for conversations
	module.directive('conversationInvite', function() {
		return {
			scope : {
				conversation : '=conversationInvite'
			},
			link : function(scope, element, attrs) {
				element.on('click', function(e) {
					// TODO replace by angular invitation modal.
					launchModal(ROUTES.Messaging_invite.url({id : scope.conversation.id}));
					
					e.preventDefault();
					e.stopPropagation();
				});
			}
		};
	});
	
	// unread counter for conversation
	module.directive('conversationUnreadCount', ['EventService', function(EventService) {
		return {
			scope : {
				conversation : '=conversationUnreadCount'
			},
			template : '<span class="label label-important unread-count pull-right">{{conversation.unreadCount}}</span>', 
			link : function(scope, element, attrs) {
				EventService.subscribe(['updatedConversation', 'newConversationPost'], function(e) {
					var data = e.object;
					if (data && +data.id === +scope.conversation.id && scope.conversation.modified <= data.modified) {
						_.defer(function(){
							scope.$apply(function(scope) {
								scope.conversation.modified = data.modified;
								scope.conversation.unreadCount = data.unreadCount;
							});
						});
					}
				});

			}
		};
	}]);
	
	module.directive('openConversation', ['$http', '$rootScope', 'EventService', function($http,$rootScope,EventService){
		return {
			restrict: 'A',
			scope : {
				link : '@openConversation'
			},
			priority: 1000,
			link : function(scope, element, attrs) {
				element.bind('click', function(e){
					e.preventDefault();
					$http.get(scope.link).success(function(data){
						EventService.publish('updatedConversation',data);
						$rootScope.$broadcast('coyo.messaging.chat.open', data);
					});
				});
			}
		};
	}]);
})();
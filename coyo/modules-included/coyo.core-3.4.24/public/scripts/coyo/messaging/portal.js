(function() {
	/**
	 * The coyo.chat module.
	 */
	var module = angular.module('coyo.messaging.portal', ['coyo.generic', 'ui.router']);
	
	module.config(['$stateProvider', '$urlRouterProvider', '$routeProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $routeProvider, $locationProvider) {
		
		var getQueryParam = function (paramName) {
			var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"), results = regex.exec(location.search);

			if (results !== null) {
				return decodeURIComponent(results[1].replace(/\+/g, " "));
			}

			return null;
		};

		$routeProvider.when('/', {
			redirectTo: function () {
				var id = getQueryParam('id');
				var status = getQueryParam('status');
				var between = status && id ? '&' : '';
				return '/messaging?' + (id ? 'id=' + id : '') + between + (status ? 'status=' + status : '');
			}
		});

		$stateProvider.state('conversation', {
			url : '/messaging?id&status',
			onEnter : ['$stateParams', '$state', function($stateParams, $state) {
				if (!$stateParams.status) {
					// goto inbox if no state was set
					$state.go('conversation', {status : 'INBOX', id : $stateParams.id});
					return false;
				}
			}]
		});
		
	}]);
})();
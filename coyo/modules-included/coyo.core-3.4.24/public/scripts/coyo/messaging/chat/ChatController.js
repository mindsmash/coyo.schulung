(function() {
	/**
	 * The coyo.messaging.ChatController
	 */
	var module = angular.module('coyo.messaging.chat');

	module.controller('ChatController', ['$scope', '$rootScope', '$timeout', 'ConversationMember', 'EventService', function($scope, $rootScope, $timeout, ConversationMember, EventService) {
		$scope.conversations = [];
		$scope.loading = false;
		
		ConversationMember.findActive().then(function(cms) {
			$scope.conversations = cms;
			$scope.loading = false;
		});
		
		function getConversationId(preferredId) {
			if (preferredId && _getConversationFromList(preferredId)) {
				return preferredId;
			}
			if ($scope.conversations.length > 0) {
				return $scope.conversations[0].id;
			}
			return undefined;
		}
		
		$rootScope.$on('coyo.messaging.chat.open', function(e, cm) {
			$timeout(function(){						
				angular.element('#chat'+cm.id).scope().openConversation(cm);
			});
		});
		
		EventService.subscribe(['updatedConversation', 'newConversationPost'], function(e) {
			var data = e.object;
			var loadedConversation = _getConversationFromList(data.id);
			if (angular.isDefined(loadedConversation)) {
				loadedConversation.unreadCount = data.unreadCount;
			} else {
				var cm = new ConversationMember(data);
				$scope.conversations.push(cm);
			}
		});
		
		function _getConversationFromList(id) {
			return _.find($scope.conversations, function(conversation) {
				return +conversation.id === +id;
			});
		}
		
		function _removeFromList(conversation) {
			for (var i = 0; i < $scope.conversations.length; i++) {
				if (+$scope.conversations[i].id === +conversation.id) {
					$scope.conversations.splice(i, 1);
					break;
				}
			}
		}
		
		$scope.removeConversation = function(conversation){
			conversation.remove = true;
			conversation.close().then(function(){
				$timeout(function() {
					_removeFromList(conversation);	
				}, 1000);
			});
		};
		
		//TODO: routing
		$scope.openPortal = function(conversation){
			window.location.href	="/messaging?id="+conversation.id;
		};

		/*
		 * Subscribe to event service to get notified of all changed user status. 
		 */
		EventService.subscribe('onlineStatusChanged', function(event) {
			var payload = event.object;

			var c = _.filter($scope.conversations, function(conversation) {
				if(+payload.userId !== +conversation.connectedUser && conversation.users.length === 2) {
					return _.find(conversation.users, function(user) {
						if (+payload.userId === +user.id) {
							return conversation;
						}
					});
				}
			});
			
			if (angular.isDefined(c)) {
				_.each(c, function(conversation) {
					conversation.conversationStatus = payload.newStatus;					
				});
			}
		});
	}]);
})();
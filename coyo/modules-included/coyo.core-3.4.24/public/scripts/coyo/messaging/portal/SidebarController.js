/* jshint -W003 */
(function() {
	/**
	 * The coyo.messaging.SidebarController
	 */
	var module = angular.module('coyo.messaging.portal');

	module.controller('SidebarController', ['$scope', '$state', '$rootScope', 'ConversationMember', 'EventService', function($scope, $state, $rootScope, ConversationMember, EventService) {
		
		$scope.conversations = [];
		$scope.loading = true;
		
		function getConversationId(preferredId) {
			if (preferredId && _getConversationFromList(preferredId)) {
				return preferredId;
			}
			if ($scope.conversations.length > 0) {
				return $scope.conversations[0].id;
			}
			return undefined;
		}
		
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			if (!toParams.status) {
				return; 
			}
			function openConversation(conversationId) {
				var conversation = _getConversationFromList(conversationId);
				if (angular.isDefined(conversation)) {
					$rootScope.$broadcast('coyo.messaging.conversation-opened', conversation);
					$rootScope.$emit('setTitle', conversation.subject);
				} else {
					$rootScope.$broadcast('coyo.messaging.conversation-closed');
					$rootScope.$emit('resetTitle');
				}
			}

			if (toParams.status !== fromParams.status || (toParams.id && !_getConversationFromList(toParams.id))) {
				ConversationMember.findByStatus(toParams.status).then(function(cms) {
					$scope.conversations = cms;
					$scope.loading = false;
					var conversationId = getConversationId(toParams.id);
					if (+conversationId === +toParams.id) {
						return openConversation(conversationId);
					} else {
						$rootScope.$broadcast('coyo.messaging.conversation-none');
					}
					$state.go('conversation', {id : conversationId});
				});
				return;
			}
			
			openConversation(toParams.id);
		});
		
		$rootScope.$on('coyo.messaging.conversation-moved', _removeFromList);
		$rootScope.$on('coyo.messaging.conversation-deleted', _removeFromList);
		
		EventService.subscribe(['updatedConversation', 'newConversationPost'], function(e) {
			var data = e.object;
			var loadedConversation = _getConversationFromList(data.id);
			// if not in loaded list - refresh all
			if (angular.isUndefined(loadedConversation)) {
				$state.go('conversation', {status : data.status, id : data.id}, {reload : true});
			}
		});

		/*
		 * Subscribe to event service to get notified of all changed user status. 
		 */
		EventService.subscribe('onlineStatusChanged', function(event) {
			var payload = event.object;
			
			var c = _.find($scope.conversations, function(conversation) {
				if (conversation.users.length === 2) {
					return _.find(conversation.users, function(user) {
						return user.id === payload.userId;
					});
				}
			});

			if (angular.isDefined(c)) {
				c.conversationStatus = payload.newStatus;
			}
		});
		
		function _getConversationFromList(id) {
			return _.find($scope.conversations, function(conversation) {
				return +conversation.id === +id;
			});
		}
		
		function _removeFromList(e, conversation) {
			for (var i = 0; i < $scope.conversations.length; i++) {
				if (+$scope.conversations[i].id === +conversation.id) {
					$scope.conversations.splice(i, 1);
					break;
				}
			}
			if (+$state.params.id === +conversation.id) {
				$state.go('conversation', {id : getConversationId()});
			}
		}
	}]);
})();
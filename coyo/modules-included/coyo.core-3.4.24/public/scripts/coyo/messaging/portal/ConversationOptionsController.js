(function() {
	/**
	 * The coyo.messaging.ConversationOptionsController
	 */
	var module = angular.module('coyo.messaging.portal');

	module.controller('ConversationOptionsController', ['$scope', '$rootScope', function($scope, $rootScope) {
		$scope.conversation = null;
		
		$rootScope.$on('coyo.messaging.conversation-closed', function(e) {
			$scope.conversation = null;
		});
		
		$rootScope.$on('coyo.messaging.conversation-opened', function(e, conversation) {
			$scope.conversation = conversation;
		});
	}]);
})();
/* jshint -W030 */
(function() {
	/**
	 * The coyo.messaging.resources.
	 */
	var module = angular.module('coyo.messaging.resources', ['play']);

	// The conversation member resource.
	module.factory('ConversationMember', ['playResourceFactory', '$http', function(playResourceFactory, $http) {
		
		var ConversationMember = playResourceFactory({
			url: '/api/messaging/conversations',
			name: 'conversationMember'
		});
		
		// object members
		angular.extend(ConversationMember.prototype, {
			archive : function() {
				return $http.put('/api/messaging/conversations/' + this.id + '/status', 'ARCHIVED');
			},
			
			reopen : function() {
				return $http.put('/api/messaging/conversations/' + this.id + '/status', 'INBOX');
			},
			
			open : function() {
				return $http.put('/api/messaging/conversations/' + this.id + '/active', 'true');
			},
			
			close : function() {
				return $http.put('/api/messaging/conversations/' + this.id + '/active', 'false');
			}
		});
			
		// class members
		angular.extend(ConversationMember, {
			findByStatus : function(status) {
				return this.query({status : status});
			},
			findActive: function(){
				return this.query({active : true});
			}
		});
		return ConversationMember;
	}]);
	
	// The conversation posts resource.
	module.factory('ConversationPostCollection', ['$q', '$http', function($q, $http) {
		
		var ConversationPostCollection = function(conversationId) {
			this.posts = [];
			this.conversationId = conversationId;
			this.conversationModified = 0;
			this.loading = false;
		};
		
		// object members
		angular.extend(ConversationPostCollection.prototype, {
			load : function(args, finishWhenEmpty, fn) {
				// fn - the append function for new posts.
				fn || (fn = angular.bind(this, function(post) {
					this.posts.unshift(post);
				}));
				finishWhenEmpty = !!finishWhenEmpty;

				var deferred = $q.defer();

				if (this.loading) {
					// already loading - reject
					deferred.reject();
					return deferred.promise;
				}
				
				this.loading = true;
				
				
				args = angular.extend({
					page: 1,
					length: 20
				}, args);
			
				$http.get('/api/messaging/conversations/' + this.conversationId + '/posts', {params: args})
					.success(angular.bind(this, function(posts, status, headers) {
					
					if (posts.length > 0) {
						for (var i = 0; i < posts.length; i++) {
							// append loaded posts - one by one
							fn(posts[i]);
						}
					} else {
						if (finishWhenEmpty) {
							this.allLoaded = true;
						}
					}
					
					// memorize last modified date.
					this.conversationModified = Number(headers()['conversation-modified']);
					this.loading = false;
					deferred.resolve(this.posts);
				}));
				
				return deferred.promise;
			},
			
			hasPreviousPages : function() {
				return !this.allLoaded;
			},
			
			nextPage : function() {
				if (!this.hasPreviousPages()) {
					// return resolved promise - no more items.
					var deferred = $q.defer();
					deferred.resolve(this);
					return deferred.promise;
				}
				
				if (this.posts.length > 0) {
					return this.load({beforeId : this.posts[0].id}, true);
				}
				return this.load();
			},
			
			newPosts : function() {
				if (this.posts.length > 0) {
					// insert function for new posts
					var _appendRemoveUnconfirmed = angular.bind(this, function(insert) {
						for (var i = this.posts.length - 1; i >= 0; i--) {
							var current = this.posts[i];
							if (current.id === insert.id) {
								// post already in -> mark confirmed 
								this.posts[i].unconfirmed = false;
								return;
							} else if(current.id < insert.id) {
								if ((i + 1) < this.posts.length) {
									// post must be inserted
									this.posts.splice(i + 1, 0, insert);
								} else {
									// post must be appended
									this.posts.push(insert);
								}
								return;
							}
						}
						// post must be prepended
						this.posts.unshift(insert);
					});
					// load all after last confirmed post
					return this.load({afterId : this.getLastPostId()}, false, _appendRemoveUnconfirmed);
				}
				return this.load();
			},
			
			getLastPostId : function() {
				for (var i = this.posts.length - 1; i > 0; i--) {
					var post = this.posts[i];
					// check for last confirmed post id
					if (!post.unconfirmed) {
						return post.id;
					}
				}
				return 0;
			},
			
			addPost : function(post) {
				return $http.post('/api/messaging/conversations/' + this.conversationId + '/posts', post).success(angular.bind(this, function(post) {
					post.unconfirmed = true; 
					// push directly to posts but mark unconfirmed
					this.posts.push(post);
					return post;
				}));
			}
		});
		
		// class members
		angular.extend(ConversationPostCollection, {
			findByConversation : function(conversation) {
				var deferred = $q.defer(),
					conversationId = angular.isObject(conversation) ? conversation.id : conversation;
				
				var collection = new ConversationPostCollection(conversationId);
				
				collection.load().then(function() {
					deferred.resolve(collection);
				});
				return deferred.promise;
			}
		});
		
		return ConversationPostCollection;
	}]);
})();
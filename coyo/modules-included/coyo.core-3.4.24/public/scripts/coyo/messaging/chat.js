(function() {
	/**
	 * The coyo.chat module.
	 */
	var module = angular.module('coyo.messaging.chat', ['coyo.messaging','commons.upload']);
})();
(function() {
	/**
	 * The coyo.messaging.ConversationController
	 */
	var module = angular.module('coyo.messaging');

	module.controller('ConversationController', ['$scope', '$rootScope', 'ConversationPostCollection', 'EventService', '$timeout', function($scope, $rootScope, ConversationPostCollection, EventService, $timeout) {
		$scope.postCollection = null;
		$scope.loading = false;

		$scope.init = function(loading) {
			$scope.loading = loading;
		};

		$rootScope.$on('coyo.messaging.conversation-closed', function(e) {
			$scope.closeConversation();
		});
		
		$rootScope.$on('coyo.messaging.conversation-opened', function(e, conversation) {
			$scope.openConversation(conversation);
		});

		$rootScope.$on('coyo.messaging.conversation-none', function(e) {
			$scope.loading = false;
		});


		$scope.openConversation = function(conversation){
			$scope.loading = true;
			$scope.postCollection = null;
			ConversationPostCollection.findByConversation(conversation.id).then(function(c) {
				$scope.postCollection = c;
				_.defer(function() {
					$scope.$apply(function() {
						// defer loading to re-enabled endless scrolling after $digest
						$scope.loading = false;
						$scope.enabled = true;
					});
				});
			});
		};
		
		$scope.closeConversation = function(){
			$scope.enabled = false;
		};
		
		$scope.closeAndRemoveConversation = function(conversation){
			$scope.closeConversation(conversation);
			$scope.removeConversation(conversation);
		};
		
		$scope.triggerConversation = function(conversation){
			if($scope.enabled){
				$scope.closeConversation();
			}else{
				$scope.openConversation(conversation);
			}
		};
		
		$scope.loadMorePosts = function() {
			$scope.loading = true;
			$scope.postCollection.nextPage().then(function() {
				$scope.loading = false;
			});
		};
		
		EventService.subscribe('newConversationPost', function(e) {
			var conversation = e.object;
			if ($scope.postCollection && +$scope.postCollection.conversationId === +conversation.id && $scope.enabled) {
				$scope.loading = true;
				$scope.postCollection.newPosts().then(function() {
					$scope.loading = false;
				});
			}
		});
	}]);
})();
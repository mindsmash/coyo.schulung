(function() {
	/**
	 * The coyo.messaging.filters.
	 */
	var module = angular.module('coyo.messaging.filters', []);

	module.filter('authorcompressed', function() {
		return function(input) {
			if (!angular.isArray(input)) {
				return input;
			}
			for (var i = 0; i < input.length; i++) {
				var current = input[i];
				if (i > 0 && current.attachments.length === 0 && !current.event) {
					var lastPost = input[i-1];
					if (lastPost.attachments.length === 0 && +lastPost.author.id === +current.author.id && (current.created - lastPost.created) < (1000 * 60 * 5)) {
						lastPost.message += '\n' + current.message;
						lastPost.linkifiedMessage += '\n' + current.linkifiedMessage;
						lastPost.created = current.created;
						input.splice(i, 1);
						continue;
					}
				}
			}
			return input;
		};
	});
})();
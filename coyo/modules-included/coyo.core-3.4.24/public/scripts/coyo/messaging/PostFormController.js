(function() {
	/**
	 * The coyo.messaging.PostFormController
	 * 
	 * Needs postCollection in parent scope.
	 */
	var module = angular.module('coyo.messaging');

	module.controller('PostFormController', ['$scope', '$rootScope', function($scope, $rootScope) {
		$scope.loading = false;
		$scope.enabled = false;
		
		$scope.getUploadOptions = function(cm) {
			return {
				name      : (cm) ? 'attachments-' + cm.id : 'attachments',
				dropId    : (cm) ? 'conversation-form-' + cm.id : 'conversation-form',
				browseId  : (cm) ? 'upload-trigger-' + cm.id : 'upload-trigger',
				url       : '/uploads'
			};
		};

		$scope.$watch('$parent.enabled', function() {
			if(!$scope.loading){
				$scope.enabled = $scope.$parent.enabled;
				if (!$scope.enabled) {
					$scope.resetForm();
				}
			}
		});	
		
		$scope.change = function() {
			$scope.postForm.trigger('change');
		};
		
		$scope.addPost = function(post) {
			if ($scope.postForm.$pristine || angular.isUndefined(post.message)) {
				return;
			}
			
			$scope.loading = true;
			if ($scope.postForm.$valid) {
				$scope.postCollection.addPost(post)['finally'](function() {
					$scope.resetForm();
				});
			}
		};
		
		$scope.resetForm = function() {
			$scope.post = {};
			$scope.loading = false;
			$scope.postForm.$setPristine();
		};
	}]);
})();

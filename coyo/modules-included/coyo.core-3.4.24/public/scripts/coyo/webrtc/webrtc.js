/* jshint -W117 */
var module = angular.module('coyo.webrtc', ['coyo.webrtc.controllers', 'coyo.webrtc.directives', 'coyo.webrtc.services', 'coyo.calls.services', 'user.service', 'ui.router']);

module.constant('WEBRTC_CONSTANTS', {
	'ACTIVE_TAB_ID' : 'currentActiveTab', // the key for the currently active tab in basil
	'BASIL_OPTIONS' : {
		namespace  : 'mindsmash.webrtc.activetab',
		storages   : [
			'local',
			'cookie',
			'session',
			'memory'
		],
		expireDays : 365
	},
	'SOUND' : {
		'INCOMING' : {
			'NAME' : 'RINGTONE',
			'SRC'  : Coyo.scripts + '/sounds/incoming_call.mp3'
		},
		'OUTGOING' : {
			'NAME' : 'RINGTONE',
			'SRC'  : Coyo.scripts + '/sounds/incoming_call.mp3'
		}
	},
	'TIMEOUT' : {
		'LAST_MOVEMENT'        :  2000, // timeout in ms for checking whether the user is still typing
		'KEYUP'                :   750, // timeout in ms for sending data after pressing a key
		'SENDDATA'             :   250, // timeout in ms for sending data via the data channel
		'NO_PERMISSION'        :  1500, // timeout in ms for sending the no permissions signal
		'CALLING'              : 60000, // timeout in ms after a call gets cancelled automatically, if the callee does not answer
		'GRANT_PERMISSION'     : 55000  // timeout in ms for the waiting time of any participant to activate his a/v
	},
	'MODAL' : {
		'STATE' : {
			'MODAL'   : 'modal',
			'PREINIT' : 'preinit',
			'INIT'    : 'init',
			'WAITING' : 'waiting',
			'MESSAGE' : 'message',
			'VIDEO'   : 'video',
			'SUMMARY' : 'summary'
		}
	},
	'USER' : {
		'STATUS' : {
			'ONLINE'  : 'ONLINE',
			'BUSY'    : 'BUSY',
			'AWAY'    : 'AWAY',
			'OFFLINE' : 'OFFLINE'
		}
	},
	'RESPONSE' : {
		'ACCEPTED'     : 'ACCEPTED',
		'DECLINED'     : 'DECLINED',
		'OK'           : 'OK',
		'TIMEOUT'      : 'TIMEOUT',
		'CANCEL'       : 'CANCEL',
		'NOTSUPPORTED' : 'NOTSUPPORTED',
		'NOTAVAILABLE' : 'NOTAVAILABLE',
		'UNKNOWN'      : 'UNKNOWN'
	},
	'SIGNAL' : {
		'NOPERMISSIONS' : 'nopermissions',
		'DISCONNECT'    : 'disconnect',
		'ENABLEVIDEO'   : 'enableVideo',
		'SETUPDONE'     : 'setupDone'
	},
	'EVENTS' : {
		'RTC' : {
			'CALL'    : 'rtcCallEvent',
			'RESPOND' : 'rtcRespondEvent',
			'DEFAULT' : 'rtcEvent'
		},
		'STATUS' : {
			'ONLINECHANGED' : 'onlineStatusChanged'
		},
		'CALL' : {
			'RECEIVED'      : 'callResponseReceived',
			'CANCELTIMEOUT' : 'cancelTimeout'
		}
	},
	'ICECONNECTION' : {
		'CLOSED'      : 'closed',
		'DISCONNECTED': 'disconnected'
	},
	'CALL' : {
		'DECLINED' : 'DECLINED',
		'CANCELED' : 'CANCELED',
		'OK'       : 'OK'
	},
	'STATE' : {
		'CHECKING'     : 'checking',
		'CONNECTED'    : 'connected',
		'COMPLETED'    : 'completed',
		'DISCONNECTED' : 'disconnected',
		'CLOSED'       : 'closed'
	},
	'DATACHANNEL' : {
		'NAME'          : 'webRTCDataChannel',
		'SIGNAL'        : {
			'LOCK'      : 'lock',
			'CHANGESET' : 'changeset',
			'CLEAR'     : 'clear'
		},
		'STATE'         : {
			'TYPING'    : 'typing',
			'DONE'      : 'done'
		},
		'READYSTATE'    : {
			'OPEN'      : 'open',
			'CLOSED'    : 'closed'
		},
		'LOCK'          : {
			'OTHER'     : 'otherIsSending',
			'SELF'      : 'selfSending'
		},
		'CHUNKSIZE'     : 12000, // the max. chunk size of a single line sent in the data channel in bytes (~no. of characters). Be conservative since not every browser may have the same constraints,
		'MAXTRIES'      :     8  // the max. number of tries to send a message
	},
	'CONNECTION_STATE' : {
		'FREE'   : 'free',
		'INCALL' : 'incall',
		'ERROR'  : 'error'
	}
});

module.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'WEBRTC_CONSTANTS', function($stateProvider, $urlRouterProvider, $locationProvider, WEBRTC_CONSTANTS) {

	$stateProvider.state(WEBRTC_CONSTANTS.MODAL.STATE.MODAL, {
		abstract: true,
		onEnter: ['$modal', '$state', function($modal, $state) {
			var modalInstance = $modal.open({
				templateUrl : Coyo.scripts + '/views/coyo/webrtc/videochat.html',
				controller  : 'WebRTCVideoChatController',
				windowClass : 'modal-large',
				backdrop    : 'static',
				keyboard    : false
			});

			modalInstance.result.then(function(response) {});
		}],
		params : {
			user : {}, 
			call : {},
			isCaller : false
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.PREINIT, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.waiting.html',
				controller: 'WebRTCVideoChatPreInitController'
			}
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.INIT, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.init.html',
				controller: 'WebRTCVideoChatInitController'
			}
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.WAITING, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.waiting.html',
				controller: 'WebRTCVideoChatInitWaitController'
			}
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.VIDEO, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.video.html',
				controller: 'WebRTCVideoChatVideoController'
			}
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.message.html',
				controller: 'WebRTCVideoChatMessageController'
			}
		},
		params : {
			message : '', 
			error : ''
		}
	}).state(WEBRTC_CONSTANTS.MODAL.STATE.SUMMARY, {
		parent: WEBRTC_CONSTANTS.MODAL.STATE.MODAL,
		views: {
			'modal@': {
				templateUrl: Coyo.scripts + '/views/coyo/webrtc/videochat.summary.html',
				controller: 'WebRTCVideoChatSummaryController'
			}
		}
	});
	
}]);

/* jshint -W117 */
var module = angular.module('coyo.webrtc.directives', []);

module
	.directive('openVideoChat', ['$modal', '$rootScope', '$log', '$timeout', '$filter', '$state', 'SoundManager', 'WebRTCModel', 'CallsModel', 'VideoChatWindow', 'EventService', 'OnlineService', 'WEBRTC_CONSTANTS', function($modal, $rootScope, $log, $timeout, $filter, $state, SoundManager, WebRTCModel, CallsModel, VideoChatWindow, EventService, OnlineService, WEBRTC_CONSTANTS) {
	return {
		restrict: 'A',
		scope : {
			user         : '=openVideoChat',
			contextClass : '@',
			contextId    : '@',
			contextName  : '@',
			audioOnly    : '@'
		},
		link : function(scope, element, attrs) {
			scope.isAudioOnly = scope.audioOnly === 'true';

			element.bind('click', function(e) {
				e.preventDefault();

				WebRTCModel.setContext(scope.contextId, scope.contextClass, scope.contextName);

				if(!window.RTCPeerConnection || !navigator.getUserMedia) {
					$log.error("[WebRTC] WebRTC is not supported by this browser");
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: scope.user, error: message('webrtc.error.notsupported')});
					$state.reload();
					return;
				}
				
				/* don't do anything if user is not online */
				if(scope.user.status !== WEBRTC_CONSTANTS.USER.STATUS.ONLINE) {
					return;
				}
				scope.response = false;
				OnlineService.setBusy();

				var timer = null;

				scope.declinedCallDate = new Date();

				scope.modalHeader = message('webrtc.call.modal.title.chooser');
				
				/* create modal */
				var modalInstance = $modal.open({
					templateUrl : Coyo.scripts + '/views/coyo/webrtc/calling.html',
					windowClass : 'modal-small',
					backdrop    : 'static',
					keyboard    : false,
					scope       : scope
				});

				/* 
				 * Handle callbacks when user is closing the modal.
				 * - CANCEL is triggered when the caller cancels the call
				 * - DECLINED is triggered when the caller clicks "OK" after the callee declined the call
				 */
				modalInstance.result.then(function(response) {
					$timeout.cancel(timer);

					SoundManager.stop(WEBRTC_CONSTANTS.SOUND.OUTGOING.NAME);

					switch(response) {
						case WEBRTC_CONSTANTS.RESPONSE.CANCEL:
							WebRTCModel.respond(scope.user.id, response).then(function() {
								$log.debug('[WebRTC] Aborting call. Cancelled by caller.');
								CallsModel.addCall(WEBRTC_CONSTANTS.CALL.CANCELED, [scope.user.id]);
								OnlineService.releaseBusy();
								WebRTCModel.setIsFree();
							}, function() {
								$log.error('[WebRTC] Failed to abort call.');
								OnlineService.releaseBusy();
								WebRTCModel.setIsFree();
							});
						/* jshint ignore:start */
							break;
						case WEBRTC_CONSTANTS.RESPONSE.TIMEOUT:
							response = WEBRTC_CONSTANTS.RESPONSE.TIMEOUTSELF;
						case WEBRTC_CONSTANTS.RESPONSE.DECLINED:
						case WEBRTC_CONSTANTS.RESPONSE.NOTSUPPORTED:
						case WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE:
							$log.debug('[WebRTC] Got a negative response:', response);
							OnlineService.releaseBusy();
							WebRTCModel.setIsFree();
							modalInstance.close();
							break;
						/* jshint ignore:end */
					}
				});
				
				/*
				 * Handle callbacks when a response is received from the callee
				 * - ACCEPTED is send if the callee accepted the call
				 * - CANCELED is send if the callee declined the call
				 */
				var unregister = $rootScope.$on(WEBRTC_CONSTANTS.EVENTS.CALL.RECEIVED, function(event, response, callee) {
					$timeout.cancel(timer);

					SoundManager.stop(WEBRTC_CONSTANTS.SOUND.OUTGOING.NAME);

					if(response === WEBRTC_CONSTANTS.RESPONSE.ACCEPTED) {
						CallsModel.addCall(WEBRTC_CONSTANTS.CALL.OK, [callee.id]).then(function(call) {
							VideoChatWindow.open(callee, true, call);
						});
						modalInstance.close();
					} else {
						CallsModel.addCall(WEBRTC_CONSTANTS.CALL.DECLINED, [callee.id]);
						scope.response = response;
					}

					/* unregister binding after is was called once */
					unregister();
				});

				WebRTCModel.setCallType(scope.isAudioOnly);

				scope.modalHeader = message(scope.isAudioOnly ? 'webrtc.call.modal.title.calling.audio' : 'webrtc.call.modal.title.calling.video');

				scope.declinedCallDate = new Date();

				/* set timer and abort call after a defined amount of time */
				timer = $timeout(function() {
					SoundManager.stop(WEBRTC_CONSTANTS.SOUND.OUTGOING.NAME);
					WebRTCModel.setIsError();
					WebRTCModel.respond(scope.user.id, WEBRTC_CONSTANTS.RESPONSE.TIMEOUT).then(function() {
						$log.debug('[WebRTC] Aborting call. Timeout.');
						CallsModel.addCall(WEBRTC_CONSTANTS.CALL.CANCELED, [scope.user.id]);
						scope.response = WEBRTC_CONSTANTS.RESPONSE.TIMEOUTSELF;
					}, function() {
						$log.error('[WebRTC] Failed to abort call.');
						scope.response = WEBRTC_CONSTANTS.RESPONSE.TIMEOUT;
					});
				}, WEBRTC_CONSTANTS.TIMEOUT.CALLING);

				WebRTCModel.call(scope.user.id, scope.isAudioOnly).then(function(response) {
					$log.debug('[WebRTC] Called user [' + scope.user.id + ']. Waiting for response.');
					SoundManager.loop(WEBRTC_CONSTANTS.SOUND.OUTGOING.NAME, WEBRTC_CONSTANTS.SOUND.OUTGOING.SRC);
					WebRTCModel.setIsInCall();
				}, function(response) {
					$log.debug('[WebRTC] User seems to be offline. Cancelling.');
					scope.response = WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE;
				});
			});
		}
	};
}]);

module.directive('videoElement', ['$sce', function($sce) {
	return {
		template: function(element, attrs) {
			return (angular.isDefined(attrs.videoMuted)) ? '<div><video muted ng-src="{{trustSrc()}}" autoplay></video></div>' : '<div><video ng-src="{{trustSrc()}}" autoplay></video></div>';
		},
		restrict: 'EA',
		replace: true,
		scope: {
			videoSrc   : '=',
			videoMuted : '@'
		},
		link: function(scope) {
			scope.trustSrc = function() {
				return (scope.videoSrc) ? $sce.trustAsResourceUrl(scope.videoSrc) : undefined;
			};
		}
	};
}]);

module.directive('videoChatButton', [function() {
	return {
		templateUrl : Coyo.scripts + '/views/coyo/webrtc/videochatbutton.html',
		restrict: 'EA',
		replace: true,
		scope : {
			userList                : '=videoChatButton',
			btnMini                 : '@',
			showDisabledPlaceholder : '@'
		},
		link : function(scope) {
			scope.buttonDisabled = scope.showDisabledPlaceholder === 'true';
			scope.showButton = false;
			scope.$watch('userList', function(newList) {
				scope.showButton = _.some(newList, function(user) {
					return '' + user.isCallable === 'true';
				});
			});
		}
	};
}]);

module.directive('videoChatHeader', [function() {
	return {
		templateUrl : Coyo.scripts + '/views/coyo/webrtc/videochatheader.html',
		restrict: 'EA',
		replace: true,
		scope : {
			user  : '=videoChatHeader'
		}
	};
}]);

module.directive('contextChooser', [function() {
	return {
		templateUrl : Coyo.scripts + '/views/coyo/webrtc/contextchooser.html',
		restrict: 'EA',
		replace : true,
		scope: {
			ngModel     : '=',
			options     : '=contextChooser',
			contextList : '@'
		},
		link : function(scope) {
			scope.list = JSON.parse(scope.contextList);
		}
	};
}]);

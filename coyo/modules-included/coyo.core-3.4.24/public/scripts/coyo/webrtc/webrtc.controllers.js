/* jshint -W117 */
/* jshint -W083 */
var module = angular.module('coyo.webrtc.controllers', []);

module.controller('WebRTCCallController', ['$scope', '$rootScope', '$log', '$timeout', '$state', '$modal', 'CoyoUtils', 'WebRTCModel', 'VideoChatWindow', 'EventService', 'SoundManager', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $rootScope, $log, $timeout, $state, $modal, CoyoUtils, WebRTCModel, VideoChatWindow, EventService, SoundManager, OnlineService, WEBRTC_CONSTANTS) {
	var basilOptions = WEBRTC_CONSTANTS.BASIL_OPTIONS;
	var basil = new window.Basil(basilOptions);

	var lastMovement = new Date();

	var timeoutChecker = null; // checker for the caller to answer (calling/ringing)

	var tabId = CoyoUtils.generateGUID();
	basil.set(WEBRTC_CONSTANTS.ACTIVE_TAB_ID, tabId);

	$('body').mousemove(function(event) {
		if(new Date().getTime() > (lastMovement.getTime() + WEBRTC_CONSTANTS.TIMEOUT.LAST_MOVEMENT)) {
			lastMovement = new Date();
			basil.set(WEBRTC_CONSTANTS.ACTIVE_TAB_ID, tabId);
		}
	});

	function cancelTimeout() {
		if(timeoutChecker != null) {
			$log.debug("[WebRTC] Canceling response timeout");
			$timeout.cancel(timeoutChecker);
			timeoutChecker = null;
		}
	}

	var cancelTimeoutUnregister = $rootScope.$on(WEBRTC_CONSTANTS.EVENTS.CALL.CANCELTIMEOUT, function() {
		cancelTimeout();
		cancelTimeoutUnregister();
	});

	/* Incoming call opening modal */
	EventService.subscribe(WEBRTC_CONSTANTS.EVENTS.RTC.CALL, function(e) {
		$scope.isAudioOnly = e.object.audioOnly;
		$scope.caller = e.object.sender;

		if(!window.RTCPeerConnection || !navigator.getUserMedia) {
			$log.warn("[WebRTC] " + $scope.caller.fullName + " tried to call you, but:");
			$log.error("[WebRTC] WebRTC is not supported by this browser");
			WebRTCModel.respond($scope.caller.id, WEBRTC_CONSTANTS.RESPONSE.NOTSUPPORTED);
			return;
		}

		if(!WebRTCModel.isFree()) {
			$log.warn("[WebRTC] User " + $scope.caller.fullName + " (ID: " + $scope.caller.id + ") tried to call, but you are not available.");
			WebRTCModel.respond($scope.caller.id, WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE);
			return;
		} else {
			$log.debug("[WebRTC] Available for call");
			WebRTCModel.setIsInCall();
		}

		if(basil.get(WEBRTC_CONSTANTS.ACTIVE_TAB_ID) === tabId) {
			$scope.response = false;

			SoundManager.loop(WEBRTC_CONSTANTS.SOUND.INCOMING.NAME, WEBRTC_CONSTANTS.SOUND.INCOMING.SRC);
			if(!OnlineService.testBusy()) {
				OnlineService.setBusy();
			}

			$scope.cancelledCallDate = new Date();
			$scope.modalTitle = message($scope.isAudioOnly ? 'webrtc.respond.modal.title.audio' : 'webrtc.respond.modal.title.video');

			/* create modal */
			var modalInstance = $modal.open({
				templateUrl : Coyo.scripts + '/views/coyo/webrtc/respond.html',
				windowClass : 'modal-small',
				backdrop    : 'static',
				keyboard    : false,
				scope       : $scope
			});

			cancelTimeout();
			timeoutChecker = $timeout(function(){
				$log.debug("[WebRTC] Response timed out");
				SoundManager.stop(WEBRTC_CONSTANTS.SOUND.INCOMING.NAME);
				WebRTCModel.respond($scope.caller.id, WEBRTC_CONSTANTS.RESPONSE.TIMEOUT);
				if(!WebRTCModel.isError()) {
					WebRTCModel.setIsError();
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.caller, message: message('webrtc.call.timeout', $scope.caller.fullName)});
				} else {
					OnlineService.releaseBusy();
					WebRTCModel.setIsFree();
				}
				modalInstance.close();
			}, WEBRTC_CONSTANTS.TIMEOUT.CALLING);

			/* 
			 * Handle callbacks when user is closing the modal.
			 * - ACCEPTED is triggered when the callee accepts the call
			 * - DECLINED is triggered when the callee declines the call
			 * - OK is triggered, if the caller cancelled the call and the callee clicks 'OK'
			 */
			modalInstance.result.then(function(response) {
				SoundManager.stop(WEBRTC_CONSTANTS.SOUND.INCOMING.NAME);
				if(response && !_.isUndefined(response)) {
					switch(response) {
						case WEBRTC_CONSTANTS.RESPONSE.ACCEPTED:
							cancelTimeout();
							WebRTCModel.setCallType($scope.isAudioOnly);
							WebRTCModel.respond($scope.caller.id, response).then(function() {
								VideoChatWindow.open($scope.caller, false);
							});
							break;
						case WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE:
						case WEBRTC_CONSTANTS.RESPONSE.DECLINED:
							cancelTimeout();
							OnlineService.releaseBusy();
							WebRTCModel.setIsFree();
							WebRTCModel.respond($scope.caller.id, response);
							break;
						case WEBRTC_CONSTANTS.RESPONSE.OK:
							cancelTimeout();
							OnlineService.releaseBusy();
							WebRTCModel.setIsFree();
							break;
						default:
							break;
					}
				}
			});

			/*
			 * Handle response callbacks
			 */
			var unregister = $rootScope.$on(WEBRTC_CONSTANTS.EVENTS.CALL.RECEIVED, function(event, response, sender) {
				$log.debug("[WebRTC] Got a caller response:", response, new Date());
				SoundManager.stop(WEBRTC_CONSTANTS.SOUND.INCOMING.NAME);

				cancelTimeout();
				switch (response) {
					case WEBRTC_CONSTANTS.RESPONSE.TIMEOUT:
						if(!WebRTCModel.isError()) {
							WebRTCModel.setIsError();
							$scope.response = WEBRTC_CONSTANTS.RESPONSE.TIMEOUT;
						}
						break;
					case WEBRTC_CONSTANTS.RESPONSE.NOTSUPPORTED:
						if(!WebRTCModel.isError()) {
							WebRTCModel.setIsError();
							$scope.response = WEBRTC_CONSTANTS.RESPONSE.NOTSUPPORTED;
						}
						break;
					case WEBRTC_CONSTANTS.RESPONSE.CANCEL:
						if(!WebRTCModel.isError()) {
							WebRTCModel.setIsError();
							$scope.response = WEBRTC_CONSTANTS.RESPONSE.CANCEL;
						}
						break;
					case WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE:
						if(!WebRTCModel.isError()) {
							WebRTCModel.setIsError();
							$scope.response = WEBRTC_CONSTANTS.RESPONSE.NOTAVAILABLE;
						}
						break;
					case WEBRTC_CONSTANTS.RESPONSE.ACCEPTED:
					case WEBRTC_CONSTANTS.RESPONSE.DECLINED:
					case WEBRTC_CONSTANTS.RESPONSE.OK:
						break;
					default:
						$log.error("[WebRTC] Unknown response: ", response);
						if(!WebRTCModel.isError()) {
							WebRTCModel.setIsError();
							$scope.response = WEBRTC_CONSTANTS.RESPONSE.UNKNOWN;
						}
						break;
				}

				/* unregister binding after is was called once */
				unregister();
			});
		}
	}, true);

	EventService.subscribe(WEBRTC_CONSTANTS.EVENTS.RTC.RESPOND, function(e) {
		var response = e.object.response;
		var sender = e.object.sender;

		$rootScope.$emit(WEBRTC_CONSTANTS.EVENTS.CALL.RECEIVED, response, sender);
	}, true);

}]);

module.controller('WebRTCVideoChatController', ['$scope', '$log', '$rootScope', '$timeout', '$state', '$stateParams', 'EventService', 'WebRTC', 'VideoStream', 'WebRTCModel', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $log, $rootScope, $timeout, $state, $stateParams, EventService, WebRTC, VideoStream, WebRTCModel, OnlineService, WEBRTC_CONSTANTS) {
	$scope.call = $stateParams.call;
	$scope.user = $stateParams.user;
	$scope.isCaller = $stateParams.isCaller;
	$scope.streams = {local : false, remote : false};
	var callAvailableChecker = null; // checker if the caller/callee is still available (stream activation)

	function cancelTimeout() {
		$rootScope.$emit(WEBRTC_CONSTANTS.EVENTS.CALL.CANCELTIMEOUT);
		if(callAvailableChecker) {
			$log.debug("[WebRTC] Canceling call timeout");
			$timeout.cancel(callAvailableChecker);
			callAvailableChecker = null;
		}
	}

	$scope.forceDisconnect = function() {
		$log.debug("[WebRTC] Forcing a disconnect");
		if(VideoStream) {
			VideoStream.stop();
			VideoStream = null;
		}
		WebRTCModel.setLocalStreamSetup(false);
		WebRTCModel.setRemoteStreamSetup(false);
		$scope.streams.local.stream = null;
		$scope.streams.local.url = null;
		$scope.streams.local.disabled = true;
		$scope.streams.local = false;
		$scope.streams.remote.stream = null;
		$scope.streams.remote.url = null;
		$scope.streams.remote.disabled = true;
		$scope.streams.remote = false;
		WebRTCModel.setWebRTCObj(null);
		cancelTimeout();
	};

	cancelTimeout();
	callAvailableChecker = $timeout(function(){
		$log.debug("[WebRTC] Call timed out");
		$scope.forceDisconnect();
		WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
			angular.noop,
			function(){
				$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
			});
		if(WebRTCModel.isInCall()) {
			WebRTCModel.setIsError();
			$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.call.timeout', $scope.user.fullName)});
		}
		$rootScope.$emit(WEBRTC_CONSTANTS.EVENTS.CALL.CANCELTIMEOUT);
	}, WEBRTC_CONSTANTS.TIMEOUT.GRANT_PERMISSION);

	$scope.initWebRTC = function() {
		return new WebRTC($scope.isCaller, $scope.user, {
			onRemoteStreamReceived : function(stream) {
				cancelTimeout();
				$log.debug("[WebRTC] Received remote stream");
				$scope.streams.remote = {stream : stream, url : URL.createObjectURL(stream), disabled: WebRTCModel.isAudioCall()};
				WebRTCModel.setRemoteStreamSetup(true);
				if(WebRTCModel.localStreamIsSetUp() && ($state.current.name !== WEBRTC_CONSTANTS.MODAL.STATE.VIDEO)) {
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.VIDEO);
				}
			},
			onStateChanged : function(status) {
				cancelTimeout();
				if((status === WEBRTC_CONSTANTS.STATE.CONNECTED) || (status === WEBRTC_CONSTANTS.STATE.COMPLETED)) {
					$log.debug("[WebRTC] Streaming set up");
				} else if(status === WEBRTC_CONSTANTS.STATE.DISCONNECTED) {
					$log.debug("[WebRTC] The other user disconnected");
					$scope.forceDisconnect();
					if(!WebRTCModel.isError()) {
						WebRTCModel.setIsError();
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.message.hangup', $scope.user.fullName)});
					}
				}
			},
			onInit: function() {
				if(WebRTCModel.localStreamIsSetUp() && WebRTCModel.remoteStreamIsSetUp() && ($state.current.name !== WEBRTC_CONSTANTS.MODAL.STATE.VIDEO)) {
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.VIDEO);
				}
			}
		});
	};

	WebRTCModel.setWebRTCObj($scope.initWebRTC());
}]);

module.controller('WebRTCVideoChatPreInitController', ['$scope', '$log', '$state', 'WebRTCModel', 'EventService', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $log, $state, WebRTCModel, EventService, OnlineService, WEBRTC_CONSTANTS) {
	$scope.cancel = function() {
		WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
			angular.noop,
			function(){
				$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
			});
		OnlineService.releaseBusy();
		WebRTCModel.setIsFree();
		$scope.$close();
		$scope.forceDisconnect();
	};

	EventService.subscribeOneTime(WEBRTC_CONSTANTS.EVENTS.RTC.DEFAULT, function(payload) {
		var event = JSON.parse(payload.object);
		$log.debug('[WebRTC] Received event', event);
		if(event.custom) {
			switch(event.custom.type) {
				case WEBRTC_CONSTANTS.SIGNAL.DISCONNECT:
					if(WebRTCModel.isInCall()) {
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.message.hangup', $scope.user.fullName)});
					}
					$scope.forceDisconnect();
					break;
				case WEBRTC_CONSTANTS.SIGNAL.ENABLEVIDEO:
					$scope.streams.remote.disabled = event.custom.value;
					break;
				case WEBRTC_CONSTANTS.SIGNAL.NOPERMISSIONS:
					if(!WebRTCModel.isError()) {
						WebRTCModel.setIsError();
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.error.nopermissions.caller', $scope.user.fullName)});
					}
					$scope.forceDisconnect();
					break;
				case WEBRTC_CONSTANTS.SIGNAL.SETUPDONE:
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.INIT);
					break;
				case WEBRTC_CONSTANTS.SIGNAL.NOPERMISSIONS:
					if(!WebRTCModel.isError()) {
						WebRTCModel.setIsError();
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.error.nopermissions.caller', $scope.user.fullName)});
					}
					$scope.forceDisconnect();
					break;
				default:
					$log.warn("[WebRTC] Received unknown custom event in WebRTCVideoChatPreInitController.", event);
			}
		}
	});
}]);

module.controller('WebRTCVideoChatInitController', ['$scope', '$log', '$state', '$stateParams', '$timeout', 'VideoStream', 'WebRTCModel', 'EventService', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $log, $state, $stateParams, $timeout, VideoStream, WebRTCModel, EventService, OnlineService, WEBRTC_CONSTANTS) {
	$scope.user = $stateParams.user;

	$scope.cancel = function() {
		WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
			angular.noop,
			function(){
				$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
			});
		OnlineService.releaseBusy();
		WebRTCModel.setIsFree();
		$scope.$close();
		$scope.forceDisconnect();
	};

	function initVideoStream() {
		$log.debug("[WebRTC] Initializing video stream");
		if(!OnlineService.testBusy()) {
			OnlineService.setBusy();
		}
		if(VideoStream) {
			var stream = VideoStream.get();
			if(stream) {
				stream.then(function(stream) {
					VideoStream.enableVideo(stream, !WebRTCModel.isAudioCall());
					if(WebRTCModel.getWebRTCObj()) {
						$log.debug("[WebRTC] Successfully initialized video stream");
						$scope.streams.local = {stream: stream, url: URL.createObjectURL(stream), disabled: WebRTCModel.isAudioCall()};
						WebRTCModel.getWebRTCObj().setLocalStream(stream);
						WebRTCModel.setLocalStreamSetup(true);

						if(WebRTCModel.remoteStreamIsSetUp() && ($state.current.name !== WEBRTC_CONSTANTS.MODAL.STATE.VIDEO)) {
							$state.go(WEBRTC_CONSTANTS.MODAL.STATE.VIDEO);
						} else {
							$state.go(WEBRTC_CONSTANTS.MODAL.STATE.WAITING);
						}

						WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.SETUPDONE}})).then(
							angular.noop,
							function(){
								$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.SETUPDONE);
							});
					} else {
						$log.error("[WebRTC] Could not initialize WebRTC connection");
						$scope.forceDisconnect();
					}
				}, function() {
					$log.debug("[WebRTC] Error initializing video stream");
					if(!WebRTCModel.isError()) {
						WebRTCModel.setIsError();
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, error: message('webrtc.error.nopermissions')});
					}
					$scope.forceDisconnect();
					/* wrapped into a timeout because the browsers take some time to initialize the grant permission request */
					$timeout(function() {
						WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.NOPERMISSIONS}})).then(
							angular.noop,
							function(){
								$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.NOPERMISSIONS);
							});
					}, WEBRTC_CONSTANTS.TIMEOUT.NO_PERMISSION);
				});
			} else {
				$log.debug("[WebRTC] Error getting the video stream");
				if(!WebRTCModel.isError()) {
					WebRTCModel.setIsError();
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, error: message('webrtc.error.unknown')});
				}
				WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
					angular.noop,
					function(){
						$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
					});
			}
		} else {
			$log.debug("[WebRTC] Error accessing the video stream");
			if(!WebRTCModel.isError()) {
				WebRTCModel.setIsError();
				$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, error: message('webrtc.error.unknown')});
			}
			$scope.forceDisconnect();
		}
	}

	initVideoStream();

	EventService.subscribe(WEBRTC_CONSTANTS.EVENTS.RTC.DEFAULT, function(payload) {
		var event = JSON.parse(payload.object);
		$log.debug('[WebRTC] Received event', event);
		if(event.signal) {
			if(WebRTCModel.getWebRTCObj()) {
				WebRTCModel.getWebRTCObj().handleMessage(event.signal);
			} else {
				$log.error("[WebRTC.subscribe] Could not initialize WebRTC connection");
			}
		} else if(event.custom) {
			switch(event.custom.type) {
				case WEBRTC_CONSTANTS.SIGNAL.DISCONNECT:
					if(WebRTCModel.isInCall()) {
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.message.hangup', $scope.user.fullName)});
					}
					$scope.forceDisconnect();
					break;
				case WEBRTC_CONSTANTS.SIGNAL.ENABLEVIDEO:
					$scope.streams.remote.disabled = event.custom.value;
					break;
				case WEBRTC_CONSTANTS.SIGNAL.NOPERMISSIONS:
					if(!WebRTCModel.isError()) {
						WebRTCModel.setIsError();
						$state.go(WEBRTC_CONSTANTS.MODAL.STATE.MESSAGE, {user: $scope.user, message: message('webrtc.error.nopermissions.caller', $scope.user.fullName)});
					}
					$scope.forceDisconnect();
					break;
				default:
					$log.warn("[WebRTC] Received unknown custom event.", event);
			}
		}
	}, true);

}]);

module.controller('WebRTCVideoChatInitWaitController', ['$scope', '$log', '$stateParams', 'WebRTCModel', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $log, $stateParams, WebRTCModel, OnlineService, WEBRTC_CONSTANTS) {
	$scope.user = $stateParams.user;

	$scope.cancel = function() {
		WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
			angular.noop,
			function(){
				$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
			});
		OnlineService.releaseBusy();
		WebRTCModel.setIsFree();
		$scope.$close();
		$scope.forceDisconnect();
	};
}]);

module.controller('WebRTCVideoChatVideoController', ['$scope', '$rootScope', '$state', '$window', '$timeout', '$log', 'VideoStream', 'WebRTCModel', 'UserService', 'EventService', 'OnlineService', 'WEBRTC_CONSTANTS', function($scope, $rootScope, $state, $window, $timeout, $log, VideoStream, WebRTCModel, UserService, EventService, OnlineService, WEBRTC_CONSTANTS) {
	$scope.videoOff = WebRTCModel.isAudioCall();
	$scope.muted = false;
	/* use two different textarea checks, one for enabling it (textareaEnabled for ngDisabled) and one for shortly disabling it when the other user is typing (textAreaReadOnly for ngReadOnly) */
	$scope.textareaEnabled = false;
	$scope.textAreaReadOnly = true;
	$scope.summaryNotes = "";
	$scope.typingStr = "calls.typing.initializing";
	$scope.userTyping = "";
	$scope.lockedStatus = 0; // 0: Unlocked, 1: Locked, 2: Typing yourself
	$scope.img = {
		audio: {
			muted: Coyo.scripts + "/../images/audio-muted.png"
		}
	};

	var currentChangeset = "";
	var typing = false;
	var keyUpPromise = null;
	var lastSendingDate = null;

	/**
	 * Sets the "Is typing..." string
	 */
	function setTypingStr(type) {
		switch(type) {
			case WEBRTC_CONSTANTS.DATACHANNEL.LOCK.OTHER:
				$scope.typingStr = "calls.typing";
				break;
			case WEBRTC_CONSTANTS.DATACHANNEL.LOCK.SELF:
				$scope.typingStr = "calls.typing.you";
				break;
		}
	}

	WebRTCModel.setCallInitialized(true);

	/**
	 * Resets the "Is typing" string
	 */
	function resetTyping() {
		$scope.typingStr = " ";
	}

	/**
	 * Callback for whenever a data channel update has been received
	 * @param dataChannel The data channel
	 * @param msgType The msg type, "lock" or "changeset"
	 */
	function dataReceived(dataChannel, msgType) {
		switch(msgType) {
			case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.LOCK:
				$scope.lockedStatus = WebRTCModel.isLocked() ? 1 : 0;
				$scope.userTyping = dataChannel.users.typing.name;
				$scope.textAreaReadOnly = WebRTCModel.isLocked();
				if(WebRTCModel.isLocked()) {
					$log.debug("[WebRTC] Received a lock");
					setTypingStr(WEBRTC_CONSTANTS.DATACHANNEL.LOCK.OTHER);
				} else {
					$log.debug("[WebRTC] Received a free lock");
					resetTyping();
				}
				break;
			case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CHANGESET:
				$log.debug("[WebRTC] Received a changeset");
				currentChangeset = dataChannel.data.lines.join('\n');
				$scope.summaryNotes = currentChangeset;
				break;
			case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CLEAR:
				$log.debug("[WebRTC] Received a clear");
				$scope.summaryNotes = "";
				break;
			default:
				$log.debug("[WebRTC] Received a message with the unknown message type '" + msgType + "':", dataChannel);
				break;
		}
		$scope.$digest(); // we need to manually start the digest here to display the current changes in the textarea
	}

	if(WebRTCModel.dataChannelIsOpen()) {
		$log.debug("[WebRTC] Data channel initialized");
		WebRTCModel.registerDataReceivedCallback(dataReceived);
		$scope.textareaEnabled = true;
		$scope.textAreaReadOnly = false;
		resetTyping();
	} else {
		$log.debug("[WebRTC] Data channel not initialized, yet.");
		WebRTCModel.registerDataChannelSetupCallback(function() {
			$log.debug("[WebRTC] Data channel initialized");
			$scope.textareaEnabled = true;
			$scope.textAreaReadOnly = false;
			resetTyping();
			$scope.$digest();
		});
	}

	WebRTCModel.registerDataReceivedCallback(dataReceived);

	UserService.getConnectedUser(WebRTCModel.setUserData);

	$scope.enableVideo = function(enabled) {
		$log.debug("[WebRTC] Toggling video");
		$scope.videoOff = !enabled;
		if(VideoStream) {
			VideoStream.enableVideo($scope.streams.local.stream, !$scope.videoOff);
			// VideoStream.toggleVideo($scope.streams.local.stream, $scope.videoOff);
			WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.ENABLEVIDEO, value : $scope.videoOff}})).then(
				angular.noop,
				function(){
					$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.ENABLEVIDEO);
				});
		}
	};

	$scope.toggleVideo = function() {
		$log.debug("[WebRTC] Toggling video");
		$scope.enableVideo($scope.videoOff ? true : false);		
	};

	$scope.mute = function() {
		$log.debug("[WebRTC] Toggling mute");
		$scope.muted = !$scope.muted;

		if(VideoStream) {
			VideoStream.toggleMute($scope.streams.local.stream, $scope.muted);
		}
	};

	$scope.browse = function() {
		$window.open(document.location.href, '_blank');
	};

	$scope.hangUp = function() {
		$log.debug("[WebRTC] Hanging up");
		WebRTCModel.signal($scope.user.id, JSON.stringify({custom : {type : WEBRTC_CONSTANTS.SIGNAL.DISCONNECT}})).then(
			angular.noop,
			function(){
				$log.error("[WebRTC] Failed to signal with signal", WEBRTC_CONSTANTS.SIGNAL.DISCONNECT);
			});
		if($scope.isCaller && WebRTCModel.callInitialized()) {
			$state.go(WEBRTC_CONSTANTS.MODAL.STATE.SUMMARY);
		} else {
			OnlineService.releaseBusy();
			WebRTCModel.setIsFree();
			$scope.$close();
		}
		$scope.forceDisconnect();
	};

	/**
	 * Recursive function to send a chunked line via the data channel
	 *
	 * @param lineNo The line number
	 * @param splitLine The line split in chunks (Array)
	 * @param lineChunk The current chunk to be sent
	 * @param success Success function to be called after all chunks have successfully been sent
	 */
	function sendLine(lineNo, splitLine, lineChunk, success) {
		if (lineChunk < splitLine.length) {
			$log.debug("[WebRTC] Sending chunked line " + lineNo + ", chunk " + lineChunk);
			var x = function (lineNo, splitLine, chunkNo, success) {
				WebRTCModel.sendData(lineNo, splitLine[chunkNo], chunkNo > 0, function () {
					sendLine(lineNo, splitLine, ++chunkNo, success);
				}, function () {
					$log.error("[WebRTC] Retry from chunk " + lineChunk);
					sendLine(lineNo, splitLine, chunkNo, success);
				}, false);
			}.bind(this, lineNo, splitLine, lineChunk, success)();
		} else if (_.isFunction(success)) {
			success();
		}
	}

	/**
	 * Recursive function to send all the lines via the data channel
	 * Sends line for line (changeset), splits lines that are above a certain chunk size
	 *
	 * @param startAt Line position
	 * @param success Success function to be called after all lines have successfully been sent
	 * @param error Error function to be called if not all chunks could be sent
	 */
	function sendData_helper(startAt, tries, success, error) {
		var allDone = true;
		var currLines = WebRTCModel.getCurrentLines();
		var summaryNotesList = $scope.summaryNotes.split('\n');
		var chunkSizeRegexp = new RegExp('.{1,' + WEBRTC_CONSTANTS.DATACHANNEL.CHUNKSIZE + '}', 'g');
		var maxTries = tries;
		var i;

		/* send changed lines */
		for(i = startAt; (i < Math.max(currLines.length, summaryNotesList.length)) && (maxTries > 0); ++i) {
			if(currLines[i] !== summaryNotesList[i]) {
				$log.debug("[WebRTC] Line " + i + " changed");
				var splitLine = [];
				var lineToSend = "";
				if(summaryNotesList[i] !== undefined) {
					splitLine = summaryNotesList[i].match(chunkSizeRegexp);
					splitLine = (splitLine !== null) ? splitLine : [];
					lineToSend = summaryNotesList[i];
				}
				if(splitLine.length > 1) {
					$log.debug("[WebRTC] Chunking line " + i);
					sendLine(i, splitLine, 0, function() {
						sendData_helper(++startAt, WEBRTC_CONSTANTS.DATACHANNEL.MAXTRIES, success, error);
					});
				} else {
					var x = function(lineNo, line, success, retry) {
						WebRTCModel.sendData(lineNo, line, false, function() {
							sendData_helper(++startAt, WEBRTC_CONSTANTS.DATACHANNEL.MAXTRIES, success, error);
						}, function() {
							if(retry) {
								$log.error("[WebRTC] Retry from line " + startAt);
								sendData_helper(startAt, --maxTries, success, error);
							}
						}, false);
					}.bind(this, i, lineToSend, success, maxTries >= 0)();
				}
				allDone = false;
				break;
			}
		}
		if((maxTries >= 0) && allDone) {
			if(_.isFunction(success)) {
				success();
			}
		} else if(maxTries < 0) {
			if(_.isFunction(error)) {
				error();
			}
		}
	}

	/**
	 * Send data via the data channel
	 * Calls internal helper functions that actually do the work
	 *
	 * @param success Success function to be called after the data has successfully been sent
	 * @param error Error function to be called if not all chunks could be sent
	 */
	function sendData(success, error) {
		sendData_helper(0, WEBRTC_CONSTANTS.DATACHANNEL.MAXTRIES, success, error);
	}

	// Use keyUp instead of watch to prohibit the initial "Is typing..."
	$scope.keyUp = function(keyEvent) {
		if(!WebRTCModel.isLocked()) {
			if(!typing) {
				typing = true;
				WebRTCModel.requestTypingLock();
				setTypingStr(WEBRTC_CONSTANTS.DATACHANNEL.LOCK.SELF);
				$scope.lockedStatus = 2;
				lastSendingDate = new Date();
			}

			if(!WebRTCModel.isSending()) {
				if(keyUpPromise !== null) {
					$timeout.cancel(keyUpPromise);
					keyUpPromise = null;
				}
				keyUpPromise = $timeout(function(){
					if(!WebRTCModel.isSending()) {
						WebRTCModel.setSending(true);
						sendData(function() {
							lastSendingDate = null;
							WebRTCModel.setSending(false);
							typing = false;
							$scope.lockedStatus = 0;
							resetTyping();
							WebRTCModel.freeTypingLock();
						}, function() {
							$log.error("[WebRTC] Max. number of tries of sending the message exceeded. Canceling...");
                            function callback() {
                                $scope.summaryNotes = "";
                                lastSendingDate = null;
                                typing = false;
                                WebRTCModel.setSending(false);
                                $scope.lockedStatus = 0;
                                resetTyping();
                                WebRTCModel.freeTypingLock();
                            }
							WebRTCModel.sendClearData(callback, callback, true);
						});
					}
				}, WEBRTC_CONSTANTS.TIMEOUT.KEYUP);

				if(lastSendingDate && ((new Date().getTime() - lastSendingDate.getTime()) > WEBRTC_CONSTANTS.TIMEOUT.SENDDATA)) {
					WebRTCModel.setSending(true);
					lastSendingDate = new Date();
					sendData(function() {
						WebRTCModel.setSending(false);
					}, function() {
						$log.error("[WebRTC] Max. number of tries of sending the message exceeded. Canceling...");
                        function callback() {
                            $scope.summaryNotes = "";
                            WebRTCModel.setSending(false);
                        }
						WebRTCModel.sendClearData(callback, callback, true);
					});
				}
			} else {
				$scope.summaryNotes = currentChangeset;
			}
		} else {
			$scope.summaryNotes = currentChangeset;
		}
	};
}]);

module.controller('WebRTCVideoChatMessageController', ['$scope', '$log', '$state', '$stateParams', 'OnlineService', 'WebRTCModel', 'WEBRTC_CONSTANTS', function($scope, $log, $state, $stateParams, OnlineService, WebRTCModel, WEBRTC_CONSTANTS) {
	$scope.error = $stateParams.error;
	$scope.message = $stateParams.message;
	$scope.endedCallDate = new Date();

	$scope.ok = function() {
		if($scope.isCaller && WebRTCModel.callInitialized()) {
			$state.go(WEBRTC_CONSTANTS.MODAL.STATE.SUMMARY);
		} else {
			OnlineService.releaseBusy();
			WebRTCModel.setIsFree();
			$scope.$close();
		}
	};
}]);

module.controller('WebRTCVideoChatSummaryController', ['$scope', '$log', '$window', 'CallsModel', 'ShareService', 'WebRTCModel', 'OnlineService', function($scope, $log, $window, CallsModel, ShareService, WebRTCModel, OnlineService) {
	function contextAvailable(ctx) {
		return ctx &&
			!_.isUndefined(ctx.clazz) && !_.isNull(ctx.clazz) && !_.isEmpty(ctx.clazz.trim()) &&
			!_.isUndefined(ctx.id) && !_.isNull(ctx.id) && !_.isEmpty(ctx.id.trim());
	}

	/* Add the previously stored summary notes to the summary controller */
	var lines = WebRTCModel.getCurrentLines();
	$scope.call.summary = angular.isArray(lines) ? lines.join('\n') : lines;
	$scope.saveButtonMsg = message('webrtc.save.nosharing');
	$scope.context = WebRTCModel.getContext();
	$scope.showContextChooser = contextAvailable($scope.context);
	/* generate a unique context UID (as in ModelUtils.serialize) */
	$scope.context.uid = $scope.context.clazz + "-" + $scope.context.id;
	/* the context UID returned by the context chooser */
	$scope.contextUid = $scope.context.uid;
	/* a list for the context chooser dropdown */
	$scope.contextList = !$scope.showContextChooser ? [] : [$scope.context];

	/* the currently selected context */
	var selectedContext = null;

	/* allow to remove the chosen values in the context chooser */
	$scope.select2OptionsContextChooser = {
		allowClear:true
	};
	/* allow to remove the chosen values in the wall chooser */
	$scope.select2OptionsWallChooser = {
		allowClear:true
	};

	/* watch for a context change */
	$scope.$watch('contextUid', function() {
		var contextSelected = false;
		for (var i = 0; i < $scope.contextList.length; ++i) {
			if($scope.contextList[i].uid === $scope.contextUid) {
				$log.debug("[WebRTC] Setting a new context:", $scope.contextUid);
				selectedContext = $scope.contextList[i];
				contextSelected = true;
				break;
			}
		}
		if(!contextSelected) {
			$log.debug("[WebRTC] Removed the context");
			selectedContext = null;
		}
	});

	/* watch for a wall change */
	$scope.$watch('wall', function() {
		if($scope.wall) {
			$log.debug("[WebRTC] Sharing on a wall");
			$scope.saveButtonMsg = message('webrtc.save.sharing');
		} else {
			$log.debug("[WebRTC] Not sharing on a wall");
			$scope.saveButtonMsg = message('webrtc.save.nosharing');
		}
	});

	$scope.save = function() {
		if(angular.isDefined($scope.call)) {
			CallsModel.save($scope.call, selectedContext).then(function() {
				if($scope.wall) {
					$log.debug("[WebRTC] Sharing the call on wall", $scope.wall);
					ShareService.share($scope.call.id, 'models.Call', $scope.wall).then(function() {
						$window.location.href = ROUTES.Activitystream_index.url();
					});
				} else {
					$log.debug("[WebRTC] Not sharing the call");
				}
				OnlineService.releaseBusy();
				WebRTCModel.setIsFree();
				$scope.$close();
			});
		} else {
			$log.error("[WebRTC] Could not save call");
			OnlineService.releaseBusy();
			WebRTCModel.setIsFree();
			$scope.$close();
		}
	};
}]);

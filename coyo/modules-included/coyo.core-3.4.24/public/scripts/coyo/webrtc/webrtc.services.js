/* jshint -W117 */
var module = angular.module('coyo.webrtc.services', []);

module.factory('WebRTCModel', ['$http', '$q', '$log', 'WEBRTC_CONSTANTS', function($http, $q, $log, WEBRTC_CONSTANTS) {
	var dataChannel = null;
	var dataChannelSetupCallback = null;
	var dataChannelCallback = null;
	var dataInternalChannelCallback = null;
	var callInitialized = false;
	var connectionState = WEBRTC_CONSTANTS.CONNECTION_STATE.FREE;
	var localStreamSetup = false;
	var remoteStreamSetup = false;
	var conEstablished = false;
	var dcOpen = false;
	var audioOnly = false;
	var sending = false;
	var webRTCObj = null;
	var context = {
		isSet : false,
		id    : null,
		name  : null,
		clazz : null
	};

	function userDataSet() {
		return (dataChannel.users.user.id !== "") && (dataChannel.users.user.name !== "");
	}

	/**
	 * Removes unused data lines
	 */
	function sanitizeData() {
		var times = 0;
		var splitTo = dataChannel.data.lines.length;
		for(var i = dataChannel.data.lines.length - 1; i >= 0; --i) {
			if(dataChannel.data.lines[i] === "") {
				++times;
			} else {
				break;
			}
		}
		if(times > 0) {
			arr = [];
			for(var j = 0; j < dataChannel.data.lines.length - times; ++j) {
				var line = dataChannel.data.lines[j];
				arr[j] = line ? line : "";
			}
			dataChannel.data.lines = arr;
		}
	}

	/**
	 * Resets the channel data. Call this method every time you need a clean WebRTCModel
	 */
	function resetChannelData() {
		dataChannel = null;
		dataChannel = {
			valid : false,
			locked: false,
			users : {
				user: {
					id  : "",
					name: ""
				},
				typing: {
					id  : "",
					name: ""
				}
			},
			data  : {
				lines     : [],
				appendLine: false,
				lineNo    : 0,
				line      : ""
			}
		};
		dcOpen = false;
	}
	resetChannelData();

	var WebRTCModel = {
		setWebRTCObj: function(obj) {
			if(!obj && (webRTCObj !== null)) {
				webRTCObj.disconnect();
				webRTCObj = null;
			} else {
				webRTCObj = obj;
			}
		},
		getWebRTCObj: function(obj) {
			return webRTCObj;
		},
		setSending: function(isSending) {
			sending = isSending;
		},
		isSending: function() {
			return sending;
		},
		setContext: function(id, clazz, name) {
			context.id = id;
			context.clazz = clazz;
			context.name = name;
			context.isSet = true;
		},
		getContext: function() {
			return context;
		},
		call: function(userId, audioCall) {
			var deferred = $q.defer();
			$http.post(ROUTES.WebRTC_call.url({'id' : userId, 'audioOnly': audioCall}), {}).
				success(function(data, status, headers, config) {
					deferred.resolve(data.elements);
				}).error(function(data, status, headers, config) {
					deferred.reject();
				});
			return deferred.promise;
		},
		respond: function(userId, response) {
			var deferred = $q.defer();
			$http.post(ROUTES.WebRTC_respond.url({
				'id': userId,
				'response': response
			}), {}).
				success(function (data, status, headers, config) {
					deferred.resolve(data.elements);
				}).error(function(data, status, headers, config) {
					deferred.reject();
				});
			return deferred.promise;
		},
		signal: function(userId, jsonData) {
			var deferred = $q.defer();
			var url = ROUTES.WebRTC_signal.url({'id' : userId});
			$http.post(url, {data : jsonData}).
				success(function(data, status, headers, config) {
					deferred.resolve(data.elements);
				}).error(function(data, status, headers, config) {
					deferred.reject();
				});
			return deferred.promise;
		},
		getStunServer: function() {
			var deferred = $q.defer(),
			url = ROUTES.WebRTC_stun.url();
			$http.get(url).
				success(function(data, status, headers, config) {
					deferred.resolve(data);
				}).error(function(data, status, headers, config) {
					deferred.reject();
				});
			return deferred.promise;
		},
		setUserData: function(data) {
			if(!_.isUndefined(data.id) && !_.isUndefined(data.fullName)) {
				$log.debug("[WebRTC] Setting user data: '" + data.fullName + "' (" + data.id + ")");
				resetChannelData();
				dataChannel.users.user.id = data.id.trim();
				dataChannel.users.user.name = data.fullName.trim();
			} else {
				$log.error("[WebRTC] Some or all of the user data is undefined");
			}
		},
		isLocked: function() {
			return dataChannel.locked;
		},
		createTypingLockMsg: function(isTyping) {
			if(!userDataSet()) {
				$log.error("[WebRTC] (createTypingLockMsg) User data is not set");
				return "";
			}
			var obj = {
				msg: {
					type: WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.LOCK
				},
				control: {
					state: isTyping ? WEBRTC_CONSTANTS.DATACHANNEL.STATE.TYPING : WEBRTC_CONSTANTS.DATACHANNEL.STATE.DONE,
					user: {
						id: dataChannel.users.user.id,
						name: dataChannel.users.user.name
					}
				},
				date: {
					current: new Date()
				}
			};
			return JSON.stringify(obj);
		},
		createClearDataMsg: function() {
			if(!userDataSet()) {
				$log.error("[WebRTC] (createClearDataMsg) User data is not set");
				return "";
			}
			var obj = {
				msg: {
					type: WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CLEAR
				},
				control: {
					user: {
						id: dataChannel.users.user.id,
						name: dataChannel.users.user.name
					}
				}
			};
			return JSON.stringify(obj);
		},
		createChangesetMsg: function(lineNo, line, append, lastChange, lastSave) {
			if(!userDataSet()) {
				$log.error("[WebRTC] (createChangesetMsg) User data is not set");
				return "";
			}
			var obj = {
				msg: {
					type: WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CHANGESET
				},
				control: {
					user: {
						id: dataChannel.users.user.id,
						name: dataChannel.users.user.name
					}
				},
				date: {
					current: new Date(),
					lastChange: lastChange,
					lastSave: lastSave
				},
				data: {
					lineNo    : lineNo,
					line      : line,
					appendLine: append
				}
			};
			return JSON.stringify(obj);
		},
		parseChangesetMsg: function(changeset) {
			if(!userDataSet()) {
				$log.error("[WebRTC] (parseChangesetMsg) User data is not set");
				return false;
			}
			if(!_.isFunction(dataChannelCallback)) {
				$log.error("[WebRTC] No data channel callback set");
				return false;
			}
			var data = JSON.parse(changeset);
			dataChannel.valid = false;
			$log.debug("[WebRTC] Message type: " + data.msg.type, data);
			switch(data.msg.type) {
				case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.LOCK:
					dataChannel.locked = data.control.state !== WEBRTC_CONSTANTS.DATACHANNEL.STATE.DONE;
					if(dataChannel.locked) {
						dataChannel.users.typing.id = data.control.user.id;
						dataChannel.users.typing.name = data.control.user.name;
					} else {
						dataChannel.users.typing.id = "";
						dataChannel.users.typing.name = "";
					}
					dataChannel.valid = true;
					break;
				case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CHANGESET:
					if(dataChannel.users.typing.id === data.control.user.id) {
						if(data.data.appendLine) {
							dataChannel.data.lines[data.data.lineNo] += data.data.line;
						} else {
							dataChannel.data.lines[data.data.lineNo] = data.data.line;
						}
						dataChannel.data.lineNo = data.data.lineNo;
						dataChannel.data.line = data.data.line;
						dataChannel.valid = true;
						sanitizeData();
					}
					break;
				case WEBRTC_CONSTANTS.DATACHANNEL.SIGNAL.CLEAR:
					if(dataChannel.users.typing.id === data.control.user.id) {
						dataChannel.data.lines = [];
						dataChannel.data.lineNo = 0;
						dataChannel.data.line = "";
						dataChannel.valid = true;
					}
					break;
				default:
					$log.error("[WebRTC] Unknown message type", data.msg.type);
					break;
			}
			if(!dataChannel.valid) {
				$log.error("[WebRTC] Data channel is not valid");
				return false;
			}

			dataChannelCallback(dataChannel, data.msg.type);
			return true;
		},
		requestTypingLock: function() {
			if(!userDataSet()) {
				$log.error("[WebRTC] (requestTypingLock) User data is not set");
				return;
			}
			if(dataChannel.locked) {
				$log.error("[WebRTC] Data channel is locked");
				return;
			}
			if(!_.isFunction(dataInternalChannelCallback)) {
				$log.error("[WebRTC] No internal callback set");
				return;
			}
			dataChannel.users.typing.id = dataChannel.users.user.id;
			dataChannel.users.typing.name = dataChannel.users.user.name;
			dataInternalChannelCallback(WebRTCModel.createTypingLockMsg(true));
		},
		freeTypingLock: function() {
			if(!userDataSet()) {
				$log.error("[WebRTC] (freeTypingLock) User data is not set");
				return;
			}
			if(dataChannel.locked) {
				$log.error("[WebRTC] Data channel is locked");
				return;
			}
			if(!_.isFunction(dataInternalChannelCallback)) {
				$log.error("[WebRTC] No internal callback set");
				return;
			}
			dataChannel.users.typing.id = "";
			dataChannel.users.typing.name = "";
			dataInternalChannelCallback(WebRTCModel.createTypingLockMsg(false));
		},
		sendClearData: function(success, error, callErrorIfNoDataIsAvailable) {
			if(!userDataSet()) {
				$log.error("[WebRTC] (sendClearData) User data is not set");
				if(callErrorIfNoDataIsAvailable && _.isFunction(error)) {
					error();
				}
				return;
			}
			if(!_.isFunction(dataInternalChannelCallback)) {
				$log.error("[WebRTC] No internal callback set");
				if(callErrorIfNoDataIsAvailable && _.isFunction(error)) {
					error();
				}
				return;
			}
			dataChannel.data.lines = [];
			dataChannel.data.lineNo = 0;
			dataChannel.data.line = "";
			try {
				dataInternalChannelCallback(WebRTCModel.createClearDataMsg());
				if(_.isFunction(success)) {
					success();
				}
			} catch(e) {
				/* Error sending message in data channel */
				if(_.isFunction(error)) {
					error();
				}
			}
		},
		sendData: function(lineNo, line, append, success, error, callErrorIfNoDataIsAvailable) {
			if(!userDataSet()) {
				$log.error("[WebRTC] (sendData) User data is not set");
				if(callErrorIfNoDataIsAvailable && _.isFunction(error)) {
					error();
				}
				return;
			}
			if(!_.isFunction(dataInternalChannelCallback)) {
				$log.error("[WebRTC] No internal callback set");
				if(callErrorIfNoDataIsAvailable && _.isFunction(error)) {
					error();
				}
				return;
			}
			try {
				dataInternalChannelCallback(WebRTCModel.createChangesetMsg(lineNo, line, append));
				if(append) {
					dataChannel.data.lines[lineNo] += line;
				} else {
					dataChannel.data.lines[lineNo] = line;
				}
				sanitizeData();
				if(_.isFunction(success)) {
					success();
				}
			} catch(e) {
				/* Error sending message in data channel */
				if(_.isFunction(error)) {
					error();
				}
			}
		},
		registerDataChannelSetupCallback: function(callback) {
			if(_.isFunction(callback)) {
				dataChannelSetupCallback = callback;
			}
		},
		registerDataReceivedCallback: function(callback) {
			if(_.isFunction(callback)) {
				dataChannelCallback = callback;
			}
		},
		registerInternalDataReceivedCallback: function(callback) {
			if(_.isFunction(callback)) {
				dataInternalChannelCallback = callback;
			}
		},
		getCurrentLines: function() {
			return dataChannel.data.lines;
		},
		setCallInitialized: function(init) {
			callInitialized = init;
		},
		callInitialized: function() {
			return callInitialized;
		},
		setIsFree: function() {
			connectionState = WEBRTC_CONSTANTS.CONNECTION_STATE.FREE;
			callInitialized = false;
		},
		isFree: function() {
			return connectionState === WEBRTC_CONSTANTS.CONNECTION_STATE.FREE;
		},
		setIsInCall: function() {
			connectionState = WEBRTC_CONSTANTS.CONNECTION_STATE.INCALL;
		},
		isInCall: function() {
			return connectionState === WEBRTC_CONSTANTS.CONNECTION_STATE.INCALL;
		},
		setIsError: function() {
			connectionState = WEBRTC_CONSTANTS.CONNECTION_STATE.ERROR;
		},
		isError: function() {
			return connectionState === WEBRTC_CONSTANTS.CONNECTION_STATE.ERROR;
		},
		setLocalStreamSetup: function(setup) {
			localStreamSetup = setup;
		},
		setRemoteStreamSetup: function(setup) {
			remoteStreamSetup = setup;
		},
		localStreamIsSetUp: function() {
			return localStreamSetup;
		},
		remoteStreamIsSetUp: function() {
			return remoteStreamSetup;
		},
		connectionEstablished: function(c) {
			conEstablished = c;
		},
		connectionIsEstablished: function() {
			return conEstablished;
		},
		dataChannelOpened: function(s) {
			dcOpen = s;
			if(dcOpen && _.isFunction(dataChannelSetupCallback)) {
				dataChannelSetupCallback();
			}
		},
		dataChannelIsOpen : function() {
			return dcOpen;
		},
		setCallType : function(type) {
			audioOnly = type;
		},
		isAudioCall : function() {
			return audioOnly;
		}
	};

	return WebRTCModel;
}]);

module.factory('VideoChatWindow', ['$state', 'WebRTCModel', 'WEBRTC_CONSTANTS', function($state, WebRTCModel, WEBRTC_CONSTANTS) {
	var VideoChatWindow = {
			open : function(user, isCaller, call) {
				if(isCaller) {
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.INIT, {user : user, isCaller : isCaller, call : call}, {reload : true});
				} else {
					$state.go(WEBRTC_CONSTANTS.MODAL.STATE.PREINIT, {user : user, isCaller : isCaller, call : call}, {reload : true});
				}
			}
	};
	return VideoChatWindow;
}]);

module.factory('WebRTC', ['$log', '$q', '$timeout', 'WebRTCModel', 'WEBRTC_CONSTANTS', function($log, $q, $timeout, WebRTCModel, WEBRTC_CONSTANTS) {
	var serverConfiguration = {iceServers : []};
	/* Optional parameters: DtlsSrtpKeyAgreement, RtpDataChannels: true} */
	var pcConstraint = null;
	/* Optional parameters: ordered, reliable */
	var dataChannelParams = null;
	var mediaConstraints = null;

	var WebRTC = function(isCaller, otherUser, customOptions) {
		if(!WebRTCModel.connectionIsEstablished()) {
			WebRTCModel.connectionIsEstablished(true);

			$log.debug("[WebRTC] Start establishing connection with user.", otherUser);

			this.options = customOptions;
			this.pc = null;
			this.dataChannel = null;

			this.pcLoaded = $q.defer();
			this.localStreamSet = $q.defer();
			this.offerReceived = $q.defer();

			var pcLoadedPromise = this.pcLoaded.promise;
			var localStreamSetPromise = this.localStreamSet.promise;
			var offerReceived = this.offerReceived.promise;

			var self = this;

			$q.all({pc : pcLoadedPromise, stream : localStreamSetPromise}).then(function(result) {
				$log.debug('[WebRTC] Adding local stream to peer connection.', result.stream, result.pc);
				self.pc.addStream(result.stream);
				if(isCaller) {
					self.offer(otherUser);
				}
			});

			$q.all({offer : offerReceived, stream : localStreamSetPromise}).then(function(result) {
				self.answer(otherUser);
			});

			WebRTCModel.getStunServer().then(function(servers) {
				if(_.isUndefined(servers) || _.isEmpty(servers)) {
					$log.error('[WebRTC] Could not find STUN servers in server configuration.');
					self.pc = new RTCPeerConnection(null, pcConstraint);
				} else {
					$log.debug('[WebRTC] Found server configuration of STUN servers.', servers);
					angular.forEach(servers, function(server) {
						serverConfiguration.iceServers.push({url : server});
					});
					self.pc = new RTCPeerConnection(serverConfiguration, pcConstraint);
				}

				/**
				 * data channel event handling
				 */
				function dc_onopen() {
					$log.debug("[WebRTC] Data channel opened");
					WebRTCModel.dataChannelOpened(true);
					WebRTCModel.registerInternalDataReceivedCallback(function(processedData) {
						/* Sending data... */
						if(self.dataChannel) {
							if(self.dataChannel.readyState === WEBRTC_CONSTANTS.DATACHANNEL.READYSTATE.OPEN) {
								self.dataChannel.send(processedData);
							} else {
								$log.error("[WebRTC] Data channel is not open. Current status: " + self.dataChannel.readyState);
							}
						} else {
							$log.error("[WebRTC] Data channel is not open.");
						}
					});

					self.options.onInit();
				}

				function dc_onclose(event) {
					$log.debug("[WebRTC] Data channel closed", event);
					WebRTCModel.dataChannelOpened(false);
					WebRTCModel.connectionIsEstablished(false);
				}

				function dc_onerror(error) {
					$log.error("[WebRTC] Data channel error:", error);
				}

				function dc_onmessage(event) {
					$log.debug("[WebRTC] Received a data channel message");
					WebRTCModel.parseChangesetMsg(event.data);
				}

				function dc_ondatachannel(event) {
					$log.debug("[WebRTC] Data channel received", event);
					self.dataChannel = event.channel;
					self.dataChannel.binaryType = 'arraybuffer';
					self.dataChannel.onopen = dc_onopen;
					self.dataChannel.onclose = dc_onclose;
					self.dataChannel.onerror = dc_onerror;
					self.dataChannel.onmessage = dc_onmessage;
				}

				/**
				 * peer connection event handling
				 * send any ice candidates to the other peer
				 */
				self.pc.onicecandidate = function(event) {
					if(event.candidate) {
						WebRTCModel.signal(otherUser.id, JSON.stringify({signal : {'candidate' : event.candidate}}));
					} else {
						$log.debug('[WebRTC] Done sending ICE canditates.');
					}
				};

				// once remote stream arrives, show it in the remote video element
				self.pc.onaddstream = function(event) {
					$log.debug('[WebRTC] Received remote stream.');
					self.options.onRemoteStreamReceived(event.stream);
				};

				self.pc.oniceconnectionstatechange = function() {
					if(self.pc) {
						$log.debug('[WebRTC] Connection state changed to ' + self.pc.iceConnectionState);
						self.options.onStateChanged(self.pc.iceConnectionState);
					} else {
						$log.debug("[WebRTC] Peer connection is not established");
					}
				};

				if(isCaller) {
					var dcName = WEBRTC_CONSTANTS.DATACHANNEL.NAME + "-" + Math.floor(Math.random() * (1000 - 1) + 1);
					self.dataChannel = self.pc.createDataChannel(dcName, dataChannelParams);
					self.dataChannel.binaryType = 'arraybuffer';

					$log.debug("[WebRTC] Data channel created");

					self.dataChannel.onopen = dc_onopen;
					self.dataChannel.onclose = dc_onclose;
					self.dataChannel.onerror = dc_onerror;
					self.dataChannel.onmessage = dc_onmessage;
				}

				self.pc.ondatachannel = dc_ondatachannel;

				self.pcLoaded.resolve(self.pc);
				$log.debug("[WebRTC] Peer connection resolved");
			}, function() {
				$log.error('[WebRTC] Getting the stun server failed.');
				WebRTCModel.connectionIsEstablished(false);
			});
		} else {
			$log.warn("[WebRTC] Connection already established");
		}
	};

	WebRTC.prototype.setLocalStream = function(stream) {
		$log.debug('[WebRTC] Local stream set by peer.');
		this.localStreamSet.resolve(stream);
	};
	
	WebRTC.prototype.offer = function(otherUser) {
		$log.debug('[WebRTC] Creating offer for [' + otherUser.id + ']');
		
		var self = this;
		
		self.pc.createOffer(function(offer) {
			$log.debug('[WebRTC] Creating offer.');
			self.pc.setLocalDescription(offer, function() {
				WebRTCModel.signal(otherUser.id, JSON.stringify({signal : {"offer": offer}}));
			}, function(error) {
				$log.error("[WebRTC] Error creating offer.", error);
			});
		}, function(error) {
			$log.error("[WebRTC] Error creating offer.", error);
		}, mediaConstraints);
	};
	
	WebRTC.prototype.answer = function(otherUser) {
		$log.debug('[WebRTC] Creating answer for [' + otherUser.id + ']');
		
		var self = this;
		
		self.pc.createAnswer(function(answer) {
			$log.debug('[WebRTC] Creating answer.');
			self.pc.setLocalDescription(answer, function() {
				WebRTCModel.signal(otherUser.id, JSON.stringify({signal : {"answer": answer}}));
			}, function(error) {
				$log.error("[WebRTC] Error creating answer.", error);
			});
		}, function(error) {
			$log.error("[WebRTC] Error creating answer.", error);
		}, mediaConstraints);
	};
	
	WebRTC.prototype.handleMessage = function(signal) {
		if(this.pc !== null) {
			if(signal.offer) {
				$log.debug('[WebRTC] Received offer with session description.', signal.offer);
				this.pc.setRemoteDescription(new RTCSessionDescription(signal.offer), function() {
					$log.debug('[WebRTC] Successfully set remote description.');
				}, function() {
					$log.error('[WebRTC] Failed to set remote description.');
				});
				this.offerReceived.resolve(signal.offer);
			} else if(signal.answer) {
				$log.debug('[WebRTC] Received answer with session description.', signal.answer);
				this.pc.setRemoteDescription(new RTCSessionDescription(signal.answer), function() {
					$log.debug('[WebRTC] Successfully set remote description.');
				}, function() {
					$log.error('[WebRTC] Failed to set remote description.');
				});
			} else if(signal.candidate) {
				$log.debug('[WebRTC] Received ICE candidate.', signal.candidate);
				try {
					this.pc.addIceCandidate(new RTCIceCandidate(signal.candidate));
				} catch(e) {
					$log.error("[WebRTC] Error adding ICE candidate.");
				}
			} else {
				$log.warn('[WebRTC] Received invalid signal.', signal);
			}
		} else {
			$log.error("[WebRTC] Peer connection could not be established");
		}
	};

	WebRTC.prototype.disconnect = function() {
		$log.debug('[WebRTC] Disconnecting.');
		if(this.dataChannel && (this.dataChannel.readyState !== WEBRTC_CONSTANTS.DATACHANNEL.READYSTATE.CLOSED)) {
			$log.debug("[WebRTC] Closing datachannel");
			this.dataChannel.close();
			this.dataChannel = null;
		}
		if(this.pc && (this.pc.signalingState !== WEBRTC_CONSTANTS.ICECONNECTION.CLOSED)) {
			$log.debug("[WebRTC] Closing peer connection");
			this.pc.close();
			this.pc = null;
		}
		WebRTCModel.connectionIsEstablished(false);
	};
	
	return WebRTC;
}]);

module.factory('VideoStream', ['$q', '$log', function($q, $log) {
	var stream = null;
	var initialized = false;
	return {
		streamUp: function() {
			return initialized;
		},
		get: function() {
			if(stream !== null) {
				return $q.when(stream);
			} else {
				if(!initialized) {
					initialized = true;
					var d = $q.defer();
					navigator.getUserMedia({video: true, audio: true}, function(s) {
						stream = s;
						d.resolve(stream);
					}, function(e) {
						initialized = false;
						d.reject(e);
					});
					return d.promise;
				}
			}
		},
		/*
		 * This function closes the MediaStream and all open audio and video tracks.
		  * See:
		  * https://developer.mozilla.org/en-US/docs/Web/API/MediaStream
		  * https://developers.google.com/web/updates/2015/07/mediastream-deprecations
		 */
		stop: function () {
			if (stream !== null) {
				// firefox specific code
				try {
					stream.stop();
				} catch (e) {
					// can be ignore happens when browsing with chrome
				}

				// new style supported by chrome. Throws no error in firefox but the stream is not cleaned up properly
				_.forEach(stream.getTracks(), function (track) {
					track.stop();
				});

				stream = null;
			}
			initialized = false;
		},
		toggleMute: function(stream, mute) {
			var audioTracks = stream.getAudioTracks();
			for(var i = 0, l = audioTracks.length; i < l; i++) {
				audioTracks[i].enabled = !mute;
				if(mute) {
					$log.debug('[WebRTC] Audio track muted.', audioTracks[i]);
				}
			}
		},
		enableVideo: function(stream, enabled) {
			if(stream) {
				var videoTracks = stream.getVideoTracks();
				for(var i = 0, l = videoTracks.length; i < l; i++) {
					videoTracks[i].enabled = enabled;
					if(!enabled) {
						$log.debug('[WebRTC] Video stream disabled.', videoTracks[i]);
					}
				}
			}
		}
	};
}]);

(function () {
    var module = angular.module('coyo.generic.longpolling', ['ngResource']);
    
    
	module.service('LongPoller', ['$q', '$resource', function ($q, $resource) {
		
		var httpTimeout = Coyo.longpollingTimeout + 30000; // add 30s to configured timeout
		
		this.resource = $resource('/api/update', {}, {'query': {method: 'GET', isArray: false, timeout : httpTimeout}});
		this.action = 'query';
		this.stopped = false;
	
		this.start = function (params) {
			var self = this;
			if (!this.deferred) {
				this.deferred = $q.defer();
			}
			this.resource[this.action](params, function (data) {
				self.deferred.notify(data);
			}, function (e) {
				// if 403 (forbidden) received to redirect to login page
				if (e && e.status === 403) {
					location.href = "/login";
				}
				self.deferred.notify('error');
			});
			this.promise = this.deferred.promise;
			return this.promise;
		};
		
		this.restart = function(lastId){
			this.start({lastEventId:lastId});
		};
		
		this.stop = function(){
			this.stopped = true;
		};
		
		return this;
	}]);
	
	module.run(['$rootScope', '$timeout', 'LongPoller', 'EventService', function ($rootScope, $timeout, LongPoller, EventService) {
		var errorCount = 0;
		var lastEventId;
		
		$timeout(function() {
			LongPoller.start().then(angular.noop, angular.noop, function(update) {
				if (LongPoller.stopped) {
					return;
				}
				if (update !== 'error') {
					errorCount = 0;
					if (update.events != null) {
						angular.forEach(update.events, function(event){
							EventService.publish(event.type,event.data);
						});
					}
					EventService.publish('counterUpdate', {
						messages : update.unreadMessages,
						notifications : update.unreadNotifications,
						posts : update.unreadPosts
					});
					lastEventId = update.lastEventId;
					LongPoller.restart(update.lastEventId);
				} else {
					if (errorCount < 5) {
						errorCount++;
						LongPoller.restart(lastEventId);
					} else {
						throw 'update service received too many errors. is the server offline?';
					}
				}
			});
		}, (Modernizr.touch ? 500 : 0));
	}]);
})();
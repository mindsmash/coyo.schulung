(function() {
	/**
	 * The coyo.filters.
	 */
	var module = angular.module('coyo.generic.filters', []);
	
	// translate message
	module.filter('i18n', function() {
		return function() {
			// path thru play i18n
			return message.apply(this, arguments);
		};
	});

	// marks content as trusted html - always remember to escape content in the backend when used
	module.filter('trusthtml', ["$sce", function($sce) {
		return function (val) {
			return $sce.trustAsHtml(val);
		};
	}]);

	module.filter('ellipsify', function () {
		return function (input, lenght) {
			if (input && input.length > lenght) {
				// Replace this with the real implementation
				return input.substring(0, lenght) + '...';
			} else {
				return input;
			}
		};
	});
})();
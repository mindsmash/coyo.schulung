(function() {
	/**
	 * The coyo.directives.
	 */
	var module = angular.module('coyo.generic.directives', ['ui.bootstrap']);

	// page header title
	module.directive('coyoTitle', function() {
		return function(scope, element) {
			var originalTitle = element.text();
			
			scope.$on('setTitle', function(event, title){
				element.text(title);
			});
			
			scope.$on('resetTitle', function(event, title){
				element.text(originalTitle);
			});
		};
	});
	
	// autogrow textarea
	module.directive('autogrow', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs) {
				scope.$watch(attrs.ngModel, function(){
					element.trigger('change');
				});
				element.autogrow();
			}
		};
	});
	
	// focus on attribute content change / load
	module.directive('focus', function() {
		return function(scope, element, attrs) {
			if (attrs.focus) {
				scope.$watch(attrs.focus, function() {
					element.focus();
				});
			} else {
				element.focus();
			}
		};
	});

	/**
	 * This directive creates an a tag which can be used to send a POST or
	 * DELETE instead of an GET request. If an confirmation is required,
	 * there are two different options for this. You can either pass a
	 * message for "confirmation", which is then shown in a confirmation
	 * dialog. You can otherwise pass a message with "confirmation-really"
	 * which transforms the label of a button to the passed message, before
	 * the option can be applied.
	 * 
	 * After the passed URL was called successfully, the page is reloaded.
	 */
	module.directive('restLink', ['ConfirmationDialog', function(ConfirmationDialog) {
		return {
			scope : {
				url                : '@',
				type               : '@?',
				redirect           : '@?',
				confirmation       : '@?',
				confirmationReally : '@?'
			},
			restrict : 'EA',
			transclude : true,
			replace    : true,
			template   : function(element, attrs) {
				if (attrs.confirmation) {
					return '<a href="" ng-transclude></a>';
				} else if (attrs.confirmationReally) {
					return '<a href="" data-really-click="send()" data-really-message={{confirmationReally}} ng-transclude></a>';
				}
				return '<a href="" data-ng-click="send()" ng-transclude></a>';
			},
			link: function(scope, element, attrs) {
				/* add confirmation dialog if message is set */
				if (attrs.confirmation) {
					element.bind('click', function(e) {
						e.preventDefault();
						e.stopPropagation();
						ConfirmationDialog.show(scope, scope.confirmation, scope.send);
					});
				}
			},
			controller : ['$scope', '$http', '$window', function($scope, $http, $window) {
				$scope.clickable = true;
				if (!$scope.type) {
					$scope.type = 'POST';
				}
				$scope.send = function() {
					// aboid double click
					if ($scope.clickable) {
						$scope.clickable = false;
						$http({
							method : $scope.type,
							url    : $scope.url
						}).error(function() {
							$scope.clickable = true;
						}).success(function(data, status, headers, config) {
							if ($scope.redirect) {
								$window.location.href = $scope.redirect;
							} else {
								$window.location.reload();
							}
							$scope.clickable = true;
						});
					}
				};
			}]
		};
	}]);
	
	/**
	 * This directive renders a group of pages, workspaces, events or
	 * bookmarks in the default sidebar. The directive itself takes the
	 * group title as an argument. In addition pass an url to fetch the
	 * items, an icon to be displayed with each item and optionally a limit.
	 * If more items exist then the limit specifies, a "Show all" button is
	 * displayed. If no limit is specified, a default value of 5 is used.
	 * Note that due to the complex sorting of workspaces, all items are
	 * fetched on initialization.
	 */
	module.directive('sidebarGroup', ['SidebarModel', function(SidebarModel) {
		return {
			scope : {
				sidebarGroup       : '@',
				url                : '@',
				icon               : '@',
				limit              : '@?',
				linkTarget         : '@'
			},
			restrict : 'EA',
			replace    : true,
			transclude : true,
			templateUrl   : Coyo.scripts + '/views/coyo/generic/sidebar-group.html',
			controller : ['$scope', '$http', '$q', function($scope, $http, $q) {
				if (angular.isUndefined($scope.limit)) {
					$scope.limit = 5;
				}
				
				$scope.loading = true;
				SidebarModel.getItems($scope.url, -1).then(function(items) {
					$scope.loading = false;
					$scope.items = items;
				});
				
				$scope.showAll = function() {
					$scope.limit = $scope.items.length;
				};
			}]
		};
	}]);
	
	// exec attribute content on keydown enter
	module.directive('onEnter', function() {
		return function(scope, element, attrs) {
			element.on('keydown', function(e) {
				if (e.which === 13 && (!e.altKey && !e.shiftKey && !e.ctrlKey)) {
					e.preventDefault();
					e.stopPropagation();
					scope.$apply(function (){
						scope.$eval(attrs.onEnter);
					});
				}
			});
		};
	});
	
	// show nice tooltip
	module.directive('tip', function() {
		return function(scope, element, attrs) {
			_.defer(function() {
				element.tooltip({
					container: 'body',
					title: scope.$eval(attrs.tip)
				});
			});
		};
	});
	
	// init audio player
	module.directive('audioPlayer', function() {
		return function(scope, element, attrs) {
			_.defer(function() {
				element.addClass('inline-playable')
				.inlinePlayer();
			});
		};
	});
	
	// init video player
	module.directive('videoPlayer', function() {
		return function(scope, element, attrs) {
			var options = scope.$eval('videoPlayer') || {};
			_.defer(function() {
				videojs(element.attr('id'), options);
			});
		};
	});
	
	// image preview
	module.directive('imagePreview', function() {
		return function(scope, element, attrs) {
			var imageUrl = scope.$eval(attrs.imagePreview),
				url = ROUTES.Resource_ajaxImage.url({url : imageUrl, downloadUrl: imageUrl});
			
			element.on('click', function(e) {
				e.preventDefault();
				launchModal(url, 'large');
			});
		};
	});
	
	// show as fuzzy date
	module.directive('fuzzyDate', ['$filter', function($filter) {
		return {
			scope : {
				date : '=fuzzyDate'
			},
			link: function(scope, element, attrs) {
				var set = function(val, fn) {
					var formatted = $filter('date')(val, 'yyyy-MM-dd HH:mm:ss');
					element.attr('title', formatted).html(formatted).timeago(fn);
				};
				set(scope.date, 'init');
				
				scope.$watch('date', function(val) {
					set(val, 'updateFromDOM');
				});
			}
		};
	}]);
	
	module.directive('formatDate', ['$filter', function($filter) {
		return {
			scope : {
				date : '=formatDate'
			},
			link: function(scope, element, attrs) {
				scope.format = attrs.format;
				if (typeof scope.format === 'undefined') { 
					scope.format = 'normal';
				}
				
				var set = function(val, fn) {
					var formatted = $filter('date')(val, Coyo.dateFormats[scope.format]);
					element.text(formatted);
				};
				set(scope.date, 'init');
				
				scope.$watch('date', function(val) {
					set(val, 'updateFromDOM');
				});
			}
		};
	}]);
	
	module.directive('formatTime', ['$filter', function($filter) {
		return {
			scope : {
				date : '=formatTime'
			},
			link: function(scope, element, attrs) {
				scope.format = attrs.format;
				if (typeof scope.format === 'undefined') {
					scope.format = 'normal';
				}
				
				var set = function(val, fn) {
					var formatted = $filter('date')(val, Coyo.timeFormats[scope.format]);
					element.text(formatted);
				};
				set(scope.date, 'init');
				
				scope.$watch('date', function(val) {
					set(val, 'updateFromDOM');
				});
			}
		};
	}]);

	/**
	 * Displays the users avatar along with his or her online status.
	 */
	module.directive('userAvatarStatus', ['$compile', function($compile) {
		return {
			templateUrl : Coyo.scripts + '/views/coyo/generic/user-avatar-status.html',
			scope : {
				user : '=userAvatarStatus'
			}
		};
	}]);

	/**
	 * Displays the message icons (messaging and audio video chat) for the passed user.
	 */
	module.directive('userMessageIcons', ['$compile', function($compile) {
		return {
			templateUrl : Coyo.scripts + '/views/coyo/generic/user-message-icons.html',
			scope : {
				user              : '=userMessageIcons',
				chatEnabled       : '=chatEnabled',
				audioVideoEnabled : '=audioVideoEnabled',
				contextClass      : '@',
				contextId         : '@',
				contextName       : '@'
			}
		};
	}]);

	// display link to the given user
	module.directive('userLink', ['$compile', function($compile) {
		return {
			scope : {
				user : '=userLink'
			},
			link : function(scope, element, attrs) {
				scope.length = attrs.length || 255;

				var template = '{{user.fullName | limitTo: length}}';
			
				if (angular.isObject(scope.user) && scope.user.accessUser) {
					template = '<a href="{{user.wallUrl}}" target="_self" title="{{user.fullName}}">' + template + '</a>';
				} else {
					template = '<span class="muted">' + template + '</span>';
				}
				var html = angular.element(template);
				element.replaceWith($compile(html)(scope));
			}
		};
	}]);
	
	// display thumb of the given user
	module.directive('userThumb', ['$compile', function($compile) {
		return {
			scope : {
				user : '=userThumb'
			},
			link : function(scope, element, attrs) {

				var template = '<img ng-src="{{user.thumbUrl}}" class="user-avatar thumb" alt="{{user.fullName}}" title="{{user.fullName}}">';
			
				if (angular.isObject(scope.user) && scope.user.accessUser) {
					template = '<a href="{{user.wallUrl}}" target="_self" class="thumb-link">' + template + '</a>';
				} else {
					template = '<span class="thumb-link">' + template + '</span>';
				}
				var html = angular.element(template);
				element.replaceWith($compile(html)(scope));
			}
		};
	}]);
	
	// unread counter for eventService
	module.directive('unreadCount', ['EventService', function(EventService) {
		return {
			scope : {
				type : '@unreadCount'
			},
			template : '<span class="label label-important unread-count pull-right" data-ng-show="unreadCounter > 0">{{unreadCounter}}</span>', 
			link : function(scope, element, attrs) {
				EventService.subscribe('counterUpdate', function(e) {
					var data = e.object;
					_.defer(function(){
						scope.$apply(function(scope) {
							scope.unreadCounter = data[scope.type];
						});
					});
				});
			}
		};
	}]);

	module.directive('backgroundImg', function() {
		return function(scope, element, attrs) {
			attrs.$observe('backgroundImg', function(value) {
				element.css({
					'background-image' : 'url(' + value + ')',
					'background-size' : 'cover'
				});
			});
		};
	});

	module.directive('focusMe', [ '$timeout', '$parse',
			function($timeout, $parse) {
				return {
					link : function(scope, element, attrs) {
						var cursorFocus = function(elem) {
							var x = window.scrollX, y = window.scrollY;
							elem.focus();
							window.scrollTo(x, y);
						};
						var model = $parse(attrs.focusMe);
						scope.$watch(model, function(value) {
							if (value === true) {
								$timeout(function() {
									cursorFocus(element[0]);
								});
							}
						});
					}
				};
			} ]);

	/**
	 * Shows an bootstrap modal dialog for confirmation before an action is
	 * invoked.
	 * 
	 * Invoked action is wrapped into a $timeout service for proper execution
	 * within queue. See: http://stackoverflow.com/a/18996042
	 */
	module.directive('confirmDialog', ['ConfirmationDialog', function(ConfirmationDialog) {
		return {
			restrict : 'A',
			scope : {
				message : '@confirmMessage',
				actionFn : '&confirmDialog',
				ngDisabled : '=?',
				propagation: '@'
			},
			link : function(scope, element, attrs) {
				element.bind('click', function(e) {
					e.preventDefault();
					if (!scope.propagation) {
						e.stopPropagation();
					}
					if (!scope.ngDisabled) {
						ConfirmationDialog.show(scope, scope.message, scope.actionFn);
					}
				});
			}
		};
	}]);

	/**
	 * A confirmation for risky actions like delete. 
	 * Usage: Add attributes: really-message="Really?" and really-click="takeAction()"
	 * 
	 * Note: This is the angular version of the jQuery implementation in generic.layout.main.js
	 */
	module.directive('reallyClick', ['$timeout', function($timeout) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var timer;
				element.bind('click', function(e) {
					if (element.hasClass("asked")) {
						element.removeClass("asked");
						scope.$apply(attrs.reallyClick);
					} else {
						var label = element.html();
//						timer = $timeout(function() {
//							element.html(label);
//							element.removeClass("asked");
//						}, 3000);
						element.addClass('asked');
						element.html(attrs.reallyMessage);
					}
				});
				
				scope.$on("$destroy", function(event) {
					$timeout.cancel(timer);
				});
			}
		};
	} ]);

	// based on http://www.abequar.net/posts/jquery-ui-datepicker-with-angularjs
	module.directive('datePicker', function() {
		return {
			restrict : 'A',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModelCtrl) {
				$(function() {
					element.datepicker({
						dateFormat : attrs.dateFormat ? attrs.dateFormat : 'yy-mm-dd',
						onSelect : function(date) {
							scope.$apply(function() {
								ngModelCtrl.$setViewValue(date);
							});
						}
					});
				});
			}
		};
	});

	/**
	 * ellipsify text
	 *
	 * @text = text to ellipsify
	 * @max = max length of text
	 * @ellipsis = signs to add to shortend text
	 */
	module.directive('ellipsifyText', ['$filter', function ($filter) {

		return {
			restrict: 'A',
			template: '<span title="{{tooltipTxt}}">{{ellipsified}}</span>',
			scope: {
				textData: '=',
				maxSize:  '@', // 255   : default
				ellipsis: '@',  // '...' : default
				noToolTip:  '@'
			},

			link: function (scope, element, attrs) {
				scope.ellipsis = scope.ellipsis ? scope.ellipsis : '...';
				scope.maxSize = scope.maxSize ? scope.maxSize : 255;

				if (scope.textData.length > scope.maxSize) {
					scope.ellipsified = $filter('limitTo')(scope.textData, scope.maxSize);
					scope.tooltipTxt = scope.noToolTip ? null : scope.textData;
					scope.ellipsified = scope.ellipsified + scope.ellipsis;
				} else {
					scope.ellipsified = scope.textData;
				}
			}
		};
	}]);

	/**
	 *  For pagination usage
	 */
	module.filter('startFrom', function() {
		return function(input, start) {
			start = +start; //parse to int
			return input.slice(start);
		};
	});


	// date dropdown selection
	module.directive('dateDropdowns', ['$window', function($window) {
		return {
			restrict : 'A',
			require : 'ngModel',
			replace : true,
			scope : {
				'model' : '=ngModel',
				'name' : '@',
				'dateFormat' : '@',
				'locale' : '@'
			},
			templateUrl : Coyo.scripts + '/views/coyo/generic/date-dropdowns.html',
			controller : ['$scope', '$window', function($scope, $window) {
				var _ = $window._, moment = $window.moment, locale = $scope.locale;
				moment.locale(locale);

				$scope.days = _.range(1, 32);
				$scope.months = _.map(moment.monthsShort(), function(month, index) {
					return {
						value : index + 1, // we assume month to be zero based
						label : month
					};
				});
				$scope.years = _.range(1900, moment().get('year') + 1).reverse();

				$scope.dateComponents = {};

				// initial parsing of model
				$scope.$watch('model', function(newValue) {
					if (angular.isUndefined(newValue) || newValue === null || !_.isEmpty($scope.dateComponents)) {
						return;
					}

					var date = moment(newValue, "YYYY-MM-DD");
					$scope.dateComponents.day = date.date();
					$scope.dateComponents.month = date.month() + 1; // month is zero based!
					$scope.dateComponents.year = date.year() === 1896 ? null : date.year();
				});

				$scope.getMoment = function() {
					// if no year is selected, the special value 1896 will be used
					return moment([
							!angular.isNumber($scope.dateComponents.year) ? 1896
									: $scope.dateComponents.year, $scope.dateComponents.month - 1,
							$scope.dateComponents.day ]);
				};

				$scope.validateDate = function() {
					if (!$scope.getMoment().isValid()) {
						var date = $scope.dateComponents;
						// try to adjust date to make it valid
						if (date.day > 28) {
							date.day--;
							$scope.validateDate();
						}
					}
				};

				$scope.dateIsValid = function() {
					return angular.isNumber($scope.dateComponents.day)
							& angular.isNumber($scope.dateComponents.month);
				};
			} ],
			link : function(scope, element, attrs, ctrl) {
				var moment = $window.moment, _ = $window._;

				scope.$watch('dateComponents', function(newValue) {
					if (angular.isUndefined(newValue)) {
						return;
					}
					var date = scope.dateComponents, isValid = scope.dateIsValid();
					if (isValid) {
						// month is zero based!
						scope.model = scope.getMoment().format(scope.dateFormat);
					} else {
						scope.model = null;
					}

					ctrl.$setValidity('dateDropdowns', isValid);
				}, true);

				scope.showInvalidMessage = function() {
					var d = scope.dateComponents;
					return !_.isEmpty(d) && d.day !== null && d.month !== null && !scope.dateIsValid();
				};
			}
		};
	}]);
	
	module.directive('ckEditor', [function () {
		return {
			restrict: 'A',
			scope: {
				height:  '@',
				mediaLibraryId:  '@'
			},
			require : 'ngModel',
			link: function (scope, element, attrs, ngModelCtrl) {
				if(!element.attr('id')) {
					console.log('Error loading CKEditor. Your textarea DOM element requires an ID.');
					return;
				}
				
				var config = CKEDITOR_BASECONFIG;
				
				if(scope.height) {
					config.height = scope.height;
				}
				
				if(scope.mediaLibraryId) {
					var renderRoute = ROUTES.MediaLibraries_render.url({ id : scope.mediaLibraryId });
					var uploadRoute = ROUTES.MediaLibraries_upload.url({ id : scope.mediaLibraryId }) + '&authenticityToken=' + AUTHENTICITY_TOKEN;
					
					config.filebrowserWindowWidth = '800';
					config.filebrowserWindowHeight = '600';
					config.filebrowserImageBrowseUrl = renderRoute;
					config.filebrowserImageUrl = uploadRoute;
					config.filebrowserBrowseUrl = renderRoute;
					config.filebrowserImageUploadUrl = uploadRoute;
					config.filebrowserUploadUrl = uploadRoute;
					config.extraPlugins = 'oembed,simpleuploads,tableresize';
				}
				
				var instance = CKEDITOR.replace(element.attr('id'), config);
				
				// bind content to ngModel
				function updateNgModel() {
					scope.$evalAsync(function(){
						if(instance) {
							ngModelCtrl.$setViewValue(instance.getData());
						}
					});
				}
				
				instance.on('change', updateNgModel);
				instance.on('blur', updateNgModel);
			}
		};
	}]);
})();

(function() {
	/**
	 * The coyo.generic.services.
	 */
	var module = angular.module('coyo.generic.utils', []);

	// Utilities for coyo generic.
	module.service('Utils', function() {
		return {
			UUID : function() {
				var S4 = function() {
					return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
				};
				return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
			}
		};
	});
})();
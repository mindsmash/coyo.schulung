/* jshint -W117 */
var module = angular.module('coyo.generic.controllers', []);

/*
 * Controller setting the title. It adds the number of unread posts,
 * notifications and messages to the title by using the TitleManager.
 */
module.controller('CoyoTitleController', ['$scope', 'EventService', 'TitleManager', function($scope, EventService, TitleManager) {
	
	$scope.init = function(title) {
		$scope.originalTitle = title;
		$scope.title = title;
	};
	
	EventService.subscribe('counterUpdate', function(e) {
		var data = e.object;
		_.defer(function() {
			$scope.$apply(function(scope) {
				$scope.title = TitleManager.updateTitle($scope.originalTitle, data);
			});
		});
	});
}]);
/**
 * Module for all generic angular templates.
 */
angular.module('coyo.generic.tpls', ['template/table/pager.html']);

/**
 * We need to supply a custom template for the pager, since ngTable already works with bootstrap 3.x.
 */
angular.module("template/table/pager.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/table/pager.html", '<div class="pagination"><div class="ng-cloak ng-table-pager"><ul class="ng-table-pagination"><li ng-class="{\'disabled\': !page.active}" ng-repeat="page in pages" ng-switch="page.type"><a ng-switch-when="prev" ng-click="params.page(page.number)" href="">&laquo; {{\'previous\' | i18n}}</a><a ng-switch-when="first" ng-click="params.page(page.number)" href=""><span ng-bind="page.number"></span></a><a ng-switch-when="page" ng-click="params.page(page.number)" href=""><span ng-bind="page.number"></span></a><a ng-switch-when="more" ng-click="params.page(page.number)" href="">&#8230;</a><a ng-switch-when="last" ng-click="params.page(page.number)" href=""><span ng-bind="page.number"></span></a><a ng-switch-when="next" ng-click="params.page(page.number)" href="">{{\'next\' | i18n}} &raquo;</a></li></ul></div></div>');
}]);
(function() {
	/*
	 * The coyo.generic.services.
	 */
	var module = angular.module('coyo.generic.services', []);

	/*
	 * The global event service for Coyo.
	 */
	module.service('EventService', ['$q', '$rootScope', function($q, $rootScope) {
		var Event = function(type, object) {
			this.type = type;
			this.object = object;
		};

		var subscriptions = {};
		
		var EventService = {
			subscribe : function(eventName, callback, unsubscribeIfAlreadyRegistered) {
				if(unsubscribeIfAlreadyRegistered) {
					EventService.unsubscribe(eventName);
				}
				subscriptions[eventName] = $rootScope.$on('coyo.eventService', function(e, event){
					if(_.isString(eventName)) {
						if(event.type === eventName || '*' === eventName) {
							callback(event);
						}
					} else if(_.isArray(eventName) && _.contains(eventName, event.type)){
						callback(event);
					}
				});
			},
			unsubscribe: function(eventName) {
				if(subscriptions[eventName]) {
					subscriptions[eventName]();
				}
			},
			unsubscribeAll: function() {
				for(var eventName in subscriptions) {
					subscriptions[eventName]();
				}
			},
			subscribeOneTime : function(eventName, callback) {
				var unsubscribe = $rootScope.$on('coyo.eventService', function(e, event){
					if(_.isString(eventName)) {
						if (event.type === eventName || '*' === eventName) {
							callback(event);
						}
					} else if(_.isArray(eventName) && _.contains(eventName, event.type)){
						callback(event);
					}
					unsubscribe();
				});
			},
			publish : function(eventName,event) {
				$rootScope.$broadcast('coyo.eventService', new Event(eventName, event));
			}
		};
		
		return EventService;
	}]);
	
	
	/*
	 * Service to show a confirmation dialog. This service is used by
	 * various directives. Pass the current scope, a message and a
	 * controller function to call after the confirmation dialog was
	 * confirmed.
	 */
	module.service('ConfirmationDialog', ['$modal', '$timeout', function($modal, $timeout) {
		var ConfirmationDialog = {
			show : function(scope, message, actionFn) {
				scope.message = message;
				var modalInstance = $modal.open({
					templateUrl : Coyo.scripts + '/views/coyo/generic/confirm.html',
					scope : scope
				});
				modalInstance.result.then(function(result) {
					$timeout(function() {
						scope.$apply(actionFn);
					});
				});
			}
		};
		return ConfirmationDialog;
	}]);
	

	/*
	 * Service to load senders for the sidebar.
	 */
	module.factory('SidebarModel', ['$http', '$q', function($http, $q) {
		var SidebarModel = {
			getItems : function(url, limit) {
				var deferred = $q.defer();
				
				$http.get(url, {
					params : {limit : limit}
				}).success(function(data, status, headers, config) {
					deferred.resolve(data);
				});
				
				return deferred.promise;
			}
		};
		
		return SidebarModel;
	}]);


	/*
	 * Service to update the title. It adds the number of unread posts,
	 * notifications and messages to the title.
	 */
	module.service('TitleManager', [function() {
		var TitleManager = {
			updateTitle : function(originalTitle, unreadCounts) {
				var total = 0;
				_.each(unreadCounts, function(count) {
					if (_.isNumber(count) && !_.isNaN(count)) {
						total += count;
					}
				});
				return (total > 0) ? "(" + total + ") " + originalTitle : originalTitle;
			}
		};
		
		return TitleManager;
	}]);
	

	/*
	 * Service that wraps the Soundmanager2 and provides convenience methods
	 * to play, loop and stop sounds.
	 */
	module.service('SoundManager', ['$window', function($window) {
		var soundManager = $window.soundManager;
		
		var _getOrCreateSound = function(id, url) {
			var sound = soundManager.getSoundById(id);
			if ((sound !== undefined) && (sound != null)) {
				return sound;
			}
            sound = soundManager.createSound({
                id : id,
                url : url
            });
            sound = !sound ? null : sound;

			return sound;
		};
		
		var _loop = function(sound) {
            if(sound != null) {
                sound.play({
                    onfinish: function() {
                        _loop(sound);
                    }
                });
            }
		};
		
		var SoundManager = {
			play : function(id, url) {
				var sound = _getOrCreateSound(id, url);
                if(sound != null) {
                    sound.play();
                }
			},
			loop : function(id, url) {
                var sound = _getOrCreateSound(id, url);
                if(sound != null) {
                    _loop(sound);
                }
			},
			stop : function(id) {
				var sound = soundManager.getSoundById(id);
				if (sound != null) {
					sound.stop();
				}
			}
		};
		
		return SoundManager;
	}]);

	
	/*
	 * General utility services used by Coyo
	 */
	module.service('CoyoUtils', [function() {
		var CoyoUtils = {
			
			/* Converts a string to boolean. See: http://stackoverflow.com/a/1414175 */
			stringToBoolean : function(string) {
				if (angular.isUndefined(string)) {
					return false;
				}
				if (_.isBoolean(string)) {
					return string;
				}
				switch (string.toLowerCase()) {
					case 'true': case 'yes': case '1': case 'on': return true;
					case 'false': case 'no': case '0': return false;
					default: return string !== ''; // same as Boolean(string)
				}
			},
		
			/* 
			 * Generates a GUID
			 * see: http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
			 */
			generateGUID : function() {
				var gen = function() {
					return Math.floor((1 + Math.random()) * 0x10000)
						.toString(16)
						.substring(1);
				};
				return gen() + gen() + '-' + gen() + '-' + gen() + '-' + gen() + '-' + gen() + gen() + gen();
			},
			
			/*
			 * Compares the user status of users and can be used by orderBy 
			 * statements in iterations via ngRepeat. It orders by 
			 * ONLINE > BUSY > AWAY > OFFLINE.
			 */
			orderByStatus : function(user) {
				if (user.status === 'ONLINE') {
					return 0;
				} else if (user.status === 'BUSY') {
					return 1;
				} else if (user.status === 'AWAY') {
					return 2;
				}
				return 3;
			}
		};
		
		return CoyoUtils;
	}]);
	
})();
(function() {
	/**
	 * The coyo.wall.directives.
	 */
	var module = angular.module('coyo.wall.directives', []);
	
	// load asynchronously and display link attachment in wall
	module.directive('linkAttachment', ['$http', function($http) {
		return {
			templateUrl : Coyo.scripts + '/views/coyo/wall/wall-post-attachment-link.html',
			replace     : true,
			scope       : {
				url : '@linkAttachment'
			},
			link : function(scope, element, attrs) {
				var url = ROUTES.Posts_weblinkInfo.url(),
					params = {url : scope.url};
				
				scope.loading = true;
				$http.get(url, {params : params}).success(function(response) {
					scope.info = response;
					scope.loading = false;
				}).error(function() {
					scope.loading = false;
				});
			}
		};
	}]);

	// display attachments
	module.directive('wallPostAttachments', [function() {
		return {
			template : '<div><ul class="unstyled attachments loading"><li class="attachment" ng-repeat="attachment in attachments" data-wall-post-attachment="attachment" data-show-preview="{{preview}}"></li></ul><div class="clearfix"></div></div>',
			replace : true,
			scope : {
				attachments : '=wallPostAttachments',
				preview     : '@?showPreview'
			},
			link : function(scope, element, attrs) {
				// if show-preview is not defined set true as default value 
				scope.preview = (angular.isDefined(scope.preview)) ? scope.preview : true;
				
				// wait for all attachments being loaded and inform upper scope.
				var informScope = _.after(scope.attachments.length, function() {
					element.find('ul').first().removeClass('loading');
					scope.$emit('wall-post-attachments:loaded');
				});
				scope.$on('wall-post-attachment:loaded', informScope);
			}
		};
	}]);
	
	// display single attachment
	module.directive('wallPostAttachment', ['$http', '$templateCache', '$compile', 'CoyoUtils', function($http, $templateCache, $compile, CoyoUtils) {
		return {
			scope : {
				attachment : '=wallPostAttachment',
				preview    : '@?showPreview'
			}, 
			link : function(scope, element) {
				var type = 'download',
					showPreview = (angular.isDefined(scope.preview)) ? CoyoUtils.stringToBoolean(scope.preview) : true;
				if (showPreview && scope.attachment.preview && scope.attachment.preview.type) {
					// attachment has preview
					type = scope.attachment.preview.type;
				}

				// determine template to load and do it
				var src = Coyo.scripts + '/views/coyo/wall/wall-post-attachment-' + type + '.html';
				$http.get(src, {cache: $templateCache}).success(function(template) {
					element.html(template);
					$compile(element.contents())(scope);
					var total = element.find('img').length;
					if (total === 0) {
						scope.$emit('wall-post-attachment:loaded');
					} else {
						// inform parent scopes about newly loaded images 
						var loaded = 0;
						element.find('img').load(function(){
							if(++loaded >= total) {
								scope.$emit('wall-post-attachment:loaded');
							}
						});
					}
				});
			}
		};
	}]);
})();
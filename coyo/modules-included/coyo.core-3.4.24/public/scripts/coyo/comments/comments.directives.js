var module = angular.module('coyo.comments.directives', ['coyo.comments.tpls']);

module.directive('comments', ['CommentService', '$filter', function (CommentService, $filter) {
	return {
		templateUrl: Coyo.scripts + '/views/coyo/comments/comments.html',
		restrict: 'A',
		replace: true,
		scope: {
			commentable: '=',
			uniqueId: '='
		},
		controller: ['$scope', '$timeout', '$window', function ($scope, $timeout, $window) {
			// textarea id
			var textareaId = 'comment-textarea' + $scope.uniqueId;
			// comment text
			$scope.text = '';
			// comment attachments list of unique ids
			$scope.attachments = [];
			// uploading yes/no
			$scope.uploading = false;
			// show all comments?
			$scope.showAll = false;
			// list of comments
			$scope.comments = [];
			// list of user to mention
			$scope.userToMention = [];

			// used by data-upload
			$scope.getUploadOptions = function (commentable) {
				return {
					name: 'comment-attachments-' + commentable.id,
					dropId: 'attachments-dropzone-' + commentable.id,
					browseId: 'comment-upload-trigger-' + commentable.id,
					url: '/uploads'
				};
			};

			// upload started
			$scope.$on('upload-api:start', function (e, name, uploads) {
				$scope.uploading = true;
			});

			// upload complete
			$scope.$on('upload-api:complete', function (e, name, uploads) {
				$scope.uploading = false;
				$timeout(function () {
					var element = $window.document.getElementById(textareaId);
					if (element) {
						element.focus();
					}
				});
			});

			// create new comment
			$scope.create = function ($event) {
				if ($event.which === 13 && !$event.shiftKey) {
					$event.preventDefault();
					var text = $scope.text;
					var attachments = $scope.attachments;
					$scope.text = '';
					$scope.attachments = [];

					CommentService.create($scope.commentable.id, $scope.commentable.clazz, text, attachments).then(function (comment) {
						$scope.comments.push(comment);
						$scope.showAll = true;
					});
				}
			};

			// delete on comment
			$scope.remove = function (comment) {
				CommentService.remove($scope.commentable.id, $scope.commentable.clazz, comment.id).then(function () {
					$scope.comments = _.without($scope.comments, comment);
				});
			};

			/* get all users that can be mentioned in this comment.
			 * At least 2 letters are needed to start a search */
			$scope.getUserToMention = function (term) {
				if (term.length < 2) {
					$scope.userToMention = [];
					return;
				}
				CommentService.userList($scope.commentable.senderId, '@' + term).then(function (users) {
					$scope.userToMention = users;
				});
			};

			/* When an user is selected to be mentioned in the comment,
			 * paste it's slug into the textarea, not the name. */
			$scope.getUserSlug = function (item) {
				return item.slug;
			};

			function load(newValue, oldValue) {
				if (newValue === oldValue) {
					return;
				}

				$scope.comments = [];
				CommentService.get($scope.commentable.id, $scope.commentable.clazz).then(function (comments) {
					$scope.comments = comments;
				});
			}

			load($scope.commentable);

			// watch for commentable change
			$scope.$watch('commentable', load);
		}]
	};
}]);

/*
 * Templates for comments directive. We use a custom template for the menu, 
 * that shows all users, which can be mentioned in a comment.
 */
angular.module('coyo.comments.tpls', ['template/mention.html']);

angular.module('template/mention.html', []).run(['$templateCache', function ($templateCache) {
	$templateCache.put('template/mention.html',
		'<ul class="dropdown-menu" style="display:block"><li mentio-menu-item="item" ng-repeat="item in items track by $index"><a class="text-primary" ng-bind-html="item.value | mentioHighlight:triggerText:\'menu-highlighted\' | unsafe"></a></li></ul>'
	);
}]);
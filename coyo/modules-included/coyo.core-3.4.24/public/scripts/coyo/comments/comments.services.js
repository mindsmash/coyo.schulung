var module = angular.module('coyo.comments.services', []);

// TODO: use ROUTES
module.service('CommentService', ['$http', '$q', function ($http, $q) {
	var CommentService = {
		create : function (commentableID, commentableClazz, text, attachments) {
			var deferred = $q.defer(),
				url = '/api/comments/' + commentableID + '/' + commentableClazz;

			$http.post(url, {
				text: text,
				attachments: attachments
			}).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		remove : function (commentableID, commentableClazz, commentId) {
			var deferred = $q.defer(),
				url = '/api/comments/' + commentableID + '/' + commentableClazz + '/' + commentId;
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		get : function (commentableID, commentableClazz) {
			var deferred = $q.defer(),
				url = '/api/comments/' + commentableID + '/' + commentableClazz;
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		userList : function (senderId, term) {
			var deferred = $q.defer(),
				url = ROUTES.Posts_userList.url(),
				params = {id : senderId, term : term};
			
			$http.get(url, {params : params}).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};

	return CommentService;
}]);
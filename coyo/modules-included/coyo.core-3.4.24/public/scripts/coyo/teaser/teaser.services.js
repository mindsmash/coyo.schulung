var module = angular.module('coyo.teaser.services', []);

module.factory('teaserModel', ['$http', function($http) {
    return {
        getTeasers: function(params, onSuccess, onError) {
			var url = ROUTES.Teaser_list.url();
			
			$http({
				method : 'GET',
				url    : url,
				params : params
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
		},
		getActive: function(onSuccess, onError) {
			var url = ROUTES.Teaser_listActive.url();

			$http({
				method : 'GET',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
		},
        getTeaser: function(teaserId, onSuccess, onError) {
			var url = ROUTES.Teaser_teaser.url({id: teaserId});
			
			$http({
				method : 'GET',
				url    : url
			}).success(function() {
				onSuccess();
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
        },
        getSettings: function(onSuccess, onError) {
			var url = ROUTES.Teaser_getSettings.url();

			$http({
				method : 'GET',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
        },
        saveSettings : function(settings, onSuccess, onError) {
			var url = ROUTES.Teaser_saveSettings.url();
			
			$http({
				method : 'POST',
				url    : url,
				data   : settings
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
        },
        save : function(teaser, onSuccess, onError) {
			var url = ROUTES.Teaser_save.url();
			
			delete teaser.image;
			$http({
				method : 'POST',
				url    : url,
				data   : teaser
			}).success(function(response) {
				if(response.error) {
					onError(response);
				} else {
					onSuccess(response);
				}
			}).error(function(data, status, headers, config) {
				onError(data);
			});
        },
        updateTeaser: function(teaserId, active, onSuccess, onError) {
			var url = ROUTES.Teaser_toggle.url({id : teaserId, active : active});
					
			$http({
				method : 'POST',
				url    : url
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
		},
		order : function(teasers, onSuccess, onError) {
			var url = ROUTES.Teaser_order.url(),
				order = _.pluck(teasers, 'id');
			
			$http({
				method : 'POST',
				url    : url,
				data   : order
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
		},
        deleteTeaser: function(teaserId, onSuccess, onError) {
			var url = ROUTES.Teaser_delete.url({id: teaserId});
			
			$http({
				method : 'DELETE',
				url    : url
			}).success(function() {
				onSuccess();
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
        }
    };
}]);
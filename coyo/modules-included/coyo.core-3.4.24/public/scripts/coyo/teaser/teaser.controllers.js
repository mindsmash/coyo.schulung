/*jshint -W055 */

var module = angular.module('coyo.teaser.controllers', []);

module.value("TeaserControllerConstants", {
	defaultInterval: 8000,
	defaultHeight: 320
});

module.controller('TeaserController', ['$scope', '$timeout', 'teaserModel', 'TeaserControllerConstants', function($scope, $timeout, teaserModel, TeaserControllerConstants) {
		$scope.teasers = [];
		$scope.loading = true;
		$scope.loadingObj = {active: true, settings: true};
		$scope.settings = {interval: TeaserControllerConstants.defaultInterval, height: TeaserControllerConstants.defaultHeight};

		// get list of teasers
		teaserModel.getActive(function(response) {
			$scope.teasers = response;
			$scope.loadingObj.active = false;
			$scope.loading = $scope.loadingObj.active || $scope.loadingObj.settings;
			for(var i = 0; i < $scope.teasers.length; ++i) {
				$scope.teasers[i].imageUrl = ROUTES.Teaser_teaserImage.url({id: $scope.teasers[i].id});
			}
			$timeout(function() {
				$('.carousel').show();
			});
		}, function(data, status) {
			// do nothing
		});
		
		var teaserHeightTries = 25;
		var teaserHeightInterval = 500;
		var teaserHeightTimeout = null;
		
		function clearTeaserHeightTimeout() {
			if(teaserHeightTimeout != null) {
				window.clearTimeout(teaserHeightTimeout);
				teaserHeightTimeout = null;
			}
		}
		
		function setTeaserHeight() {
			if(teaserHeightTries > 0) {
				--teaserHeightTries;
				if($(".carousel-inner > .item").css("height") === undefined) {
					clearTeaserHeightTimeout();
					window.setTimeout(setTeaserHeight, teaserHeightInterval);
				} else {
					clearTeaserHeightTimeout();
					$(".carousel-inner > .item").css("height", $scope.settings.height + "px");
				}
			} else {
				$(".carousel-inner > .item").css("height", $scope.settings.height + "px");
			}
		}
		
		// dynamically change the height of the carousel
		$scope.$watch("settings.height", function() {
			setTeaserHeight();
		});
		$scope.settings.height = TeaserControllerConstants.defaultHeight;

		// dynamically change the interval of the carousel
		$scope.$watch("settings.interval", function() {
			$('#teaserCarousel').carousel({
				interval: $scope.settings.interval
			});
		});
		$scope.settings.interval = TeaserControllerConstants.defaultInterval;
		
		// get teaser settings
		teaserModel.getSettings(function(response) {
			$scope.settings = response.properties;
			$scope.loadingObj.settings = false;
			$scope.loading = $scope.loadingObj.active || $scope.loadingObj.settings;
		}, function(data, status) {
			// do nothing
		});
	}
]);

var module = angular.module('coyo.media.directives', []);

module.directive('mediaLink', ['MediaModel', '$modal', function(MediaModel, $modal) {
	return {
		templateUrl: Coyo.scripts + '/views/coyo/media/mediaLink.html',
		restrict: 'A',
		replace: true,
		scope: {
			mediaId : '=mediaLink'
		},
		link: function (scope, element, attrs) {
			MediaModel.getMedia(scope.mediaId).then(function(media) {
				scope.media = media;
			});
			
			scope.open = function(media) {
				var modalInstance = $modal.open({
					templateUrl : Coyo.scripts + '/views/coyo/media/media.html',
					controller  : 'MediaDetailController',
					windowClass : 'large',
					resolve     : {
						media : function(){
							return media;
						}
					}
				});
			};
		}
	};
}]);

module.directive('mediaPreview', ['$compile', '$templateCache', '$http', '$timeout', function($compile, $templateCache, $http, $timeout) {
	return {
		restrict: 'A',
		replace: true,
		scope: {
			media : '=mediaPreview'
		},
		link: function (scope, element, attrs) {
			scope.$watch('media', function(media, old){
				$http.get(Coyo.scripts + '/views/coyo/media/media-' + media.typeKey + '.html', {cache: $templateCache}).success(function(template) {
					element.html(template);
					$compile(element.contents())(scope);
					
					// init player for videos
					if(media.id > 0 && media.typeKey === 'video') {
						$timeout(function(){
							new MediaElementPlayer('#media-video-player');
						});
					}
				});
			});
		}
	};
}]);
angular.module('coyo.media', ['coyo.media.controllers', 'coyo.media.directives', 'coyo.media.services', 'ngRoute', 'ui.sortable'])
	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	
		var getQueryParam = function(paramName) {
			var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"),
			results = regex.exec(location.search);
			if (results !== null) {
				return decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			return null;
		};
		
		$routeProvider
			.when('/media', {
				redirectTo : function() {
					var collectionId = getQueryParam('collectionId');
					if (collectionId !== null) {
						var mediaId = getQueryParam('mediaId');
						return (mediaId !== null) ? '/media/collection/' + collectionId + '/' + mediaId : '/media/collection/' + collectionId;
					}
					return '/media/index';
				}
			})
			.when('/media/index', {
				templateUrl: Coyo.scripts + '/views/coyo/media/room.html',
				controller: 'MediaRoomController'
			})
			.when('/media/collection/:collectionId', {
				templateUrl: Coyo.scripts + '/views/coyo/media/collection.html',
				controller: 'MediaCollectionController'
			})
			.when('/media/collection/:collectionId/:mediaId', {
				templateUrl: Coyo.scripts + '/views/coyo/media/collection.html',
				controller: 'MediaCollectionController'
			});
		
	}
]).run(['$http', '$templateCache', function($http, $templateCache){
	// preload partials to avoid additional loading time
	$http.get(Coyo.scripts + '/views/coyo/media/media-photo.html', {cache:$templateCache});
	$http.get(Coyo.scripts + '/views/coyo/media/media-video.html', {cache:$templateCache});
}]);
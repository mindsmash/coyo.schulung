var module = angular.module('coyo.media.services', []);

module.factory('MediaModel', ['$http', '$q', function($http, $q) {
	var MediaModel = {
		getRoom : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Media_room.url({ 'id' : id });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		getCollection : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Media_collection.url({ 'id' : id });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		getMediaItems : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Media_mediaItems.url({ 'id' : id });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		getMedia : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Media_media.url({ 'id' : id });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		saveCollection : function(collection) {
			var deferred = $q.defer(),
				url = ROUTES.Media_saveCollection.url();
			
			$http.post(url, collection).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			
			return deferred.promise;
		},
		sortCollections : function(room) {
			var deferred = $q.defer(),
				url = ROUTES.Media_sortCollections.url({id: room.id});
			
			var ids = _.pluck(room.collections, 'id');
			
			$http.post(url, ids).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		deleteCollection : function(collection) {
			var deferred = $q.defer(),
				url = ROUTES.Media_deleteCollection.url({id: collection.id});
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		upload : function(collection, uploads) {
			var deferred = $q.defer(),
				url = ROUTES.Media_upload.url({id: collection.id});
		
			$http.post(url, uploads).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		sortMedia : function(collection, media) {
			var deferred = $q.defer(),
				url = ROUTES.Media_sortMedia.url({id: collection.id});
			
			var ids = _.pluck(media, 'id');
			
			$http.post(url, ids).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		deleteMedia : function(media) {
			var deferred = $q.defer(),
				url = ROUTES.Media_deleteMedia.url({id: media.id});
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		setCover : function(media) {
			var deferred = $q.defer(),
				url = ROUTES.Media_setCover.url({id: media.id});
			
			$http.post(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		saveMedia : function(media) {
			var deferred = $q.defer(),
				url = ROUTES.Media_saveMedia.url();
			
			$http.post(url, media).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
	
	return MediaModel;
}]);
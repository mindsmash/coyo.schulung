var module = angular.module('coyo.media.controllers', []);

/**
 * Parent controller which is called by the Play! tag
 */
module.controller('MediaParentController', ['$scope', 'MediaModel', '$modal', '$location', function($scope, MediaModel, $modal, $location) {
	$scope.init = function(roomId){
		$scope.roomId = roomId;
		
		/*
		 * When the app is initially called, play does this with
		 * no specific route. We therefore can't handle the
		 * routing via routeProvider, since other angular apps
		 * might have the route "/" already handled. So for the
		 * initial call we will redirect to a "/media" sub path.
		 * From there the routeProvider will be able to work
		 * correctly.
		 */
		if ($location.path().indexOf('/media') !== 0) {
			$location.path('/media');
		}
	};
	
	$scope.collectionDialog = function(collection, callback) {
		var modal = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/media/collectionForm.html',
			controller  : 'MediaCollectionFormController',
			resolve     : {
				collection : function(){return collection;}
			}
		});
		
		if(callback) {
			modal.result.then(callback);
		}
	};
}]);

/**
 * List of collections
 */
module.controller('MediaRoomController', ['$scope', 'MediaModel', '$modal', '$location', function($scope, MediaModel, $modal, $location) {
	MediaModel.getRoom($scope.roomId).then(function(data) {
		$scope.room = data;
		$scope.sortableOptions.disabled = !$scope.room.canEdit;
	});
	
	$scope.sortableOptions = {
		disabled: true,
		stop: function(e, ui) {
			MediaModel.sortCollections($scope.room);
		}
	};
	
	$scope.createCollectionDialog = function(){
		$scope.collectionDialog({ room : $scope.room }, function(collection){
			$scope.room.collections.push(collection);
			$location.path('/media/collection/' + collection.id);
		});
	};
}]);

/**
 * List of media
 */
module.controller('MediaCollectionController', ['$scope', 'MediaModel', '$routeParams', '$timeout', '$modal', '$location', '$route', 'UploadApi', function($scope, MediaModel, $routeParams, $timeout, $modal, $location, $route, UploadApi) {
	$scope.loading = false;
	
	MediaModel.getCollection($routeParams.collectionId).then(function(data) {
		$scope.collection = data;
		$scope.sortableOptions.disabled = !$scope.collection.canEdit;
	});
	
	function loadMedia() {
		$scope.loading = true;
		MediaModel.getMediaItems($routeParams.collectionId).then(function(data) {
			$scope.mediaItems = data;
			checkDirty($scope.mediaItems);
			
			// if media id is set open modal
			if ($routeParams.mediaId) {
				var media = _.findWhere(data, {id : +$routeParams.mediaId});
				$scope.open(media);
			}
			
			$scope.loading = false;
		});
	}
	loadMedia();
	
	// updates a single media item when it changes (e.g. in modal)
	$scope.$on('mediaChanged', function(e, media){
		angular.copy(media, _.findWhere($scope.mediaItems, { id: media.id }));
	});
	
	$scope.sortableOptions = {
		disabled: true,
		stop: function(e, ui) {
			MediaModel.sortMedia($scope.collection, $scope.mediaItems);
		}
	};
	
	$scope.uploads = [];
	$scope.getUploadOptions = function(collection) {
		return {
			name      : 'collection-' + collection.id,
			dropId    : 'collection-' + collection.id + '-dropzone',
			browseId  : 'collection-' + collection.id + '-trigger',
			url       : '/uploads'
		};
	};

	/**
	 * clean up mediaItems by removing all other existing dummies
	 */
	function cleanDummyFiles() {
		$scope.mediaItems = _.filter($scope.mediaItems, function(item){
			return !item.dummy;
		});
	}
	
	// watch for new uploads
	$scope.$on('upload-api:start', function(e, name, files) {

		cleanDummyFiles();

		if (name === 'collection-' + $scope.collection.id) {
			// add dummies to indicate loader
			_.each(files, function(element, i){
				$scope.mediaItems.push({
					uploading: true,
					title: '',
					dummy: true
				});
			});
		}
	});
	
	// watch for completed uploads
	$scope.$on('upload-api:complete', function(e, name, files) {
		if (name === 'collection-' + $scope.collection.id) {
			MediaModel.upload($scope.collection, files).then(function(result) {
				// replace dummies
				_.each(result.created, function(media, i){
					angular.copy(media, _.findWhere($scope.mediaItems, { dummy: true }));
				});

				cleanDummyFiles();
				
				// display errors
				$scope.errors = result.errors;
				
				// update count
				$scope.collection.mediaCount = $scope.mediaItems.length;
				
				// clear uploads
				UploadApi.get('collection-' + $scope.collection.id).files = [];
				
				checkDirty($scope.mediaItems);
			});
		}
	});
	
	/**
	 * Checks a list of media items for missing thumbs and reloads them after some time.
	 */
	function checkDirty(mediaItems) {
		var delay = 2500;
		
		function reloadMedia(media) {
			$timeout(function(){
				MediaModel.getMedia(media.id).then(function(newMediaData) {
					if(!newMediaData.generatingVariants) {
						angular.copy(newMediaData, media);
						
						// possibly set new cover
						if(media.isCover) {
							$scope.collection.coverUrl = media.thumbUrl;
						}
					} else {
						reloadMedia(media);
					}
				});
			}, delay);
		}
		
		_.each(mediaItems, function(media){
			if(media.generatingVariants && media.id > 0) {
				reloadMedia(media);
			}
		});
	}
	
	$scope.open = function(media) {
		if(media.id > 0 && !media.generatingVariants) {
			var modalInstance = $modal.open({
				templateUrl : Coyo.scripts + '/views/coyo/media/media.html',
				controller  : 'MediaDetailController',
				windowClass : 'large',
				resolve     : {
					media : function(){
						return media;
					}
				},
				scope: $scope.$new()
			}).result.then(loadMedia);
		}
	};
	
	$scope.edit = function(){
		$scope.collectionDialog($scope.collection);
	};
	
	$scope.remove = function() {
		MediaModel.deleteCollection($scope.collection).then(function() {
			$location.path('/media/index');
		});
	};
	
	$scope.dismissError = function(error) {
		$scope.errors = _.without($scope.errors, error);
	};
}]);

module.controller('MediaCollectionFormController', ['$scope', 'MediaModel', '$modalInstance', 'collection', function($scope, MediaModel, $modalInstance, collection) {
	$scope.master = angular.copy(collection);
	$scope.collection = collection;
	
	$scope.save = function(){
		MediaModel.saveCollection($scope.collection).then(function(collection) {
			$modalInstance.close(collection);
		});
	};
	
	$scope.dismiss = function(){
		// reset
		angular.copy($scope.master, $scope.collection);
		$modalInstance.dismiss();
	};
}]);

module.controller('MediaDetailController', ['$scope', '$modal', '$location', '$routeParams', 'MediaModel', '$modalInstance', '$timeout', 'media', function($scope, $modal, $location, $routeParams, MediaModel, $modalInstance, $timeout, media) {
	$scope.media = media;
	
	$scope.setCover = function() {
		MediaModel.setCover($scope.media).then(function(media) {
			angular.copy(media, $scope.media);
		});
	};
	
	$scope.remove = function() {
		MediaModel.deleteMedia($scope.media).then(function() {
			$modalInstance.close();
		});
	};
	
	$scope.next = function() {
		MediaModel.getMedia($scope.media.next).then(function(media) {
			$scope.media = media;
		});
	};
	
	$scope.previous = function() {
		MediaModel.getMedia($scope.media.previous).then(function(media) {
			$scope.media = media;
		});
	};
	
	$scope.dismiss = function() {
		$modalInstance.dismiss();
	};
	
	$scope.save = function() {
		MediaModel.saveMedia($scope.media).then(function(media){
			$scope.$emit('mediaChanged', media);
		});
	};
}]);
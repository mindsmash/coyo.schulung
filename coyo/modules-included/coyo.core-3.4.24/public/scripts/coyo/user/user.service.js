/* jshint -W117 */
var module = angular.module('user.service', []);

module.factory('UserService', ['$http', function($http) {
    var UserService = {
        getConnectedUser : function(callback) {
            if(_.isFunction(callback)) {
                $http.get(ROUTES.User_current.url()).success(function (data, status, headers, config) {
                    callback(data);
                });
            }
        }
    };

    return UserService;
}]);

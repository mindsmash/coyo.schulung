var module = angular.module('coyo.generic.optionchooser', ['coyo.generic.optionchooser.tpls', 'ui.select2']);

module.directive('optionChooser', ['$compile', function($compile) {
	return {
		restrict    : 'E',
		scope       : {
			field   : '=',
			ngModel : '='
		},
		replace     : true,
		templateUrl : 'template/optionchooser.html',
		controller  : ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			/* define options for select 2 */
			$scope.selectOptions = {
				allowClear      : true,
				placeholder     : ($scope.field.required) ? message('list.field.required') : message('list.field.optional'),
				multiple        : $scope.field.properties.multiple,
				data            : {results : $scope.field.options, text : 'label'},
				formatResult    : function(option, container, query, escapeMarkup) {
					return '<p>' + escapeMarkup(option.label) + '</p>';
				},
				formatSelection : function(option, container, escapeMarkup) {
					return escapeMarkup(option.label);
				}
			};

			/*enable the save button when an options is chosen */
			$scope.$watch('ngModel', function(newVal, oldVal) {
				var valid = $scope.field.required ? (angular.isDefined(newVal) && !_.isEmpty(newVal)) : true;
				$scope.$emit('validityChanged', {fieldId: $scope.field.id, valid: valid});
			}, true);
		}]
	};
}]);


/**
 * Module holding all needed templates
 */
angular.module('coyo.generic.optionchooser.tpls', ['template/optionchooser.html']);

angular.module("template/optionchooser.html", []).run(['$templateCache', function($templateCache) {
	$templateCache.put("template/optionchooser.html", 
		'<div><div data-ui-select2="selectOptions" id="{{field.id}}" name="{{field.name}}" class="options-chooser" data-ng-model="ngModel" data-ng-required="field.required"></div></div>'
	);
}]);
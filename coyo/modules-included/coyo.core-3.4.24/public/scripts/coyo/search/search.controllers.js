var module = angular.module('coyo.search.controllers', []);

module.controller('SearchFullController', ['$scope', 'SearchModel', function($scope, SearchModel) {
	$scope.init = function(searchString){
		$scope.searchString = searchString;
		$scope.update();
	};

	$scope.update = function() {
		$scope.loading = true;

		if ($scope.searchString) {
			SearchModel.search($scope.searchString).then(function (data) {
				$scope.loading = false;
				$scope.data = data;
				$scope.empty = _.isEmpty(data);
			});
		} else {
			$scope.loading = false;
			$scope.empty = true;
		}
	};
}]);
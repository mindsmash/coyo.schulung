var module = angular.module('coyo.search.services', []);

module.factory('SearchModel', ['$http', '$q', function($http, $q) {
	var SearchModel = {
		search : function(searchString) {
			var deferred = $q.defer(),
				url = ROUTES.Search_search.url({ 'searchString' : encodeURIComponent(searchString) });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
	
	return SearchModel;
}]);
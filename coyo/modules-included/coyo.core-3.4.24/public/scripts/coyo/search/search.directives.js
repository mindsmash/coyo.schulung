var module = angular.module('coyo.search.directives', []);

module.directive('highlight', [function() {
	return {
		restrict: 'A',
		scope: {
			string : '=highlight'
		},
		link: function (scope, element, attrs) {
			scope.$watch('string', function(){
				$(element).highlight(scope.string.split(' '));
			});
		}
	};
}]);
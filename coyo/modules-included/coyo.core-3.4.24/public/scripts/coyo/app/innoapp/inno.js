angular.module('coyo.inno', ['coyo.generic', 'coyo.inno.controllers', 'coyo.inno.services', 'coyo.inno.directives', 'app.list.controller', 'coyo.list.controllers', 'coyo.list.directives',
	'coyo.list.services', 'coyo.generic.userchooser', 'coyo.generic.optionchooser', 'coyo.generic.filechooser', 'coyo.comments', 'ui.sortable', 'ngRoute', 'ngAnimate'])

	.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

		var getQueryParam = function (paramName) {
			var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"), results = regex.exec(location.search);

			if (results !== null) {
				return decodeURIComponent(results[1].replace(/\+/g, " "));
			}

			return null;
		};

		$routeProvider
			.when('/', {
				redirectTo: function () {
					var idea = getQueryParam('idea');
					if (idea) {
						return '/index/' + idea;
					} else {
						return '/index';
					}
				}
			})
			.when('/index', {
				templateUrl: Coyo.scripts + '/views/apps/inno/index.html',
				controller: 'InnoAppOverviewController'
			})
			.when('/index/:idea', {
				templateUrl: Coyo.scripts + '/views/apps/inno/index.html',
				controller: 'InnoAppOverviewController'
			})
			.otherwise({
				templateUrl: Coyo.scripts + '/views/apps/inno/index.html',
				controller: 'InnoAppOverviewController'
			});
	}]);

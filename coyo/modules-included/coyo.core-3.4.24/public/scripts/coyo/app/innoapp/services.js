/**
 * @author Gerrit Dongus
 */
var module = angular.module('coyo.inno.services', []);

module.service('InnoAppUtils', ['$http', function ($http) {
	var InnoAppUtils = {
		getRemainingDays : function(idea, ttl) {
			if (idea.status === "NEW") {
				var today = moment();
				var expireDate = moment(idea.created).add(ttl, 'days');
				var remainingDays = expireDate.diff(today, 'days');

				if (remainingDays < 1) {
					remainingDays = 1;
				}
				return remainingDays;
			} else {
				return 0;
			}
		}
	};
	return InnoAppUtils;
}]);

module.service('InnoAppService', ['$http', function ($http) {

	var _getModerators = function (id, onSuccess) {
		$http({
			cache: true,
			url: '/app/inno/moderators?id=' + id,
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	var _getIdea = function (id, onSuccess) {
		$http({
			url: '/app/inno/idea/' + id,
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	/*
	 * Try to find similar ideas using backend fuzzy on title or description
	 */
	var _getSimilarIdeas = function (idea, app, onSuccess) {
		var url = '/app/inno/idea/similar/' + app.id;

		$http.post(url, idea).success(function (response) {
			onSuccess(response);
		}).error(function () {
			onSuccess();
		});
	};

	/*
	 * used by plugin - find new ideas of other users
	 */
	var _findNewIdeas = function (onSuccess) {
		$http({
			url: '/app/inno/ideasofotherusers',
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	// get all possible status enums
	var _getStatusList = function (onSuccess) {
		$http({
			url: '/app/inno/status',
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	// get app
	var _getApp = function (id, cached, onSuccess) {
		$http({
			cache: true,
			url: '/app/inno?id=' + id + '&cached=' + cached,
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};


	var _getIdeasFiltered = function (appId, filterParams, onSuccess) {
		var url = '/app/inno/ideas/' + appId;

		$http.post(url, filterParams)
			.success(function (response) {
				onSuccess(response);
			}).error(function () {
				onSuccess(false);
			});
	};



	// create a new idea
	var _createIdea = function (containerId, idea, itemValues, onSuccess, onError) {
		var url = '/app/inno/idea/create/' + containerId + '/' + idea.category.id;
		_createOrUpdateIdea(url, idea, itemValues, onSuccess, onError);
	};

	// update a new idea
	var _updateIdea = function (idea, itemValues, onSuccess, onError) {
		var url = '/app/inno/idea/update/' + idea.id + '/' + idea.category.id;
		_createOrUpdateIdea(url, idea, itemValues, onSuccess, onError);
	};

	var _createOrUpdateIdea = function(url, idea, itemValues, onSuccess, onError) {
		var data = {
				title        : idea.title,
				description  : idea.description,
				isAnon       : idea.isAnon,
				hiddenField  : idea.hiddenField,
				categoryId   : idea.category.id,
				formData     : itemValues
		};
		$http.post(url, data).success(function (response) {
			onSuccess(response);
		}).error(function (data, status, headers, config) {
			onError(data, status, headers, config);
		});
	};

	// ...
	var _rateIdea = function (ideaId, rating, onSuccess, onError) {
		var url = '/app/inno/idea/rate/' + ideaId + '/' + rating;
		$http.post(url)
			.success(function (item) {
				onSuccess(item);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	// ...
	var _follow = function (ideaId, onSuccess, onError) {
		var url = '/app/inno/idea/follow/' + ideaId;
		$http.post(url)
			.success(function (item) {
				onSuccess(item);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	// ...
	var _unfollow = function (ideaId, onSuccess, onError) {
		var url = '/app/inno/idea/unfollow/' + ideaId;
		$http.post(url)
			.success(function (item) {
				onSuccess(item);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	var _deleteIdea = function (containerId, itemId, onSuccess, onError) {
		var url = '/app/inno/idea/delete/' + containerId + '/' + itemId;

		$http['delete'](url)
			.success(function (response) {
				onSuccess(response);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	var _setIdeaStatus = function (itemId, status, comment, onSuccess, onError) {
		var url = '/app/inno/idea/status/' + itemId + '/' + status;

		$http.post(url, comment)
			.success(function (response) {
				onSuccess(response);

			})
			.error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	var _linkIdea = function (itemId, linkedIdea, onSuccess, onError) {
		var url = '/app/inno/idea/link/' + itemId + '/' + linkedIdea.id;

		$http.post(url)
			.success(function (response) {
				onSuccess(response);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	var _delinkIdea = function (itemId, linkedItem, onSuccess, onError) {
		var url = '/app/inno/idea/delink/' + linkedItem + '/' + itemId;
		$http.post(url)
			.success(function (response) {
				onSuccess(response);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	var _isModerator = function (appId, onSuccess, onError) {
		var url = '/app/inno/user/ismoderator/' + appId;

		$http.get(url)
			.success(function (response) {
				onSuccess(response);
			}).error(function (data, status, headers, config) {
				onError(data, status, headers, config);
			});
	};

	return {
		getIdeasFiltered: _getIdeasFiltered,
		getModerators: _getModerators,
		getApp: _getApp,
		createIdea: _createIdea,
		updateIdea: _updateIdea,
		deleteIdea: _deleteIdea,
		getIdea: _getIdea,
		rateIdea: _rateIdea,
		getStatusList: _getStatusList,
		setIdeaStatus: _setIdeaStatus,
		linkIdea: _linkIdea,
		delinkIdea: _delinkIdea,
		findNewIdeas: _findNewIdeas,
		getSimilarIdeas: _getSimilarIdeas,
		follow: _follow,
		unfollow: _unfollow,
		isModerator: _isModerator
	};
}]);
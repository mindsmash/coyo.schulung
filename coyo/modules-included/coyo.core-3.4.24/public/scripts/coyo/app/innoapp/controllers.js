/* jshint -W117 */
var app = angular.module('coyo.inno.controllers', []);

var userchooserOptions = {
	field: {
		id: 'userChooser',
		required: false,
		properties: {
			multiple: false
		}
	},
	options: {
		serviceUrl: '/users/chooser',
		showMe: true,
		showExternals: true,
		width: 180
	}
};

// Root controller for the inno app holds static information
app.controller('InnoAppRootController', ['$scope', 'InnoAppService', 'listModel', function ($scope, InnoAppService, listModel) {
	// initialize

	$scope.init = function (appId) {
		$scope.appId = appId;
		$scope.pageSize = 10;
	};
}]);


// Controls the configuration page of the inno app
app.controller('InnoAppConfigController', ['$scope', '$controller', '$modal', 'listModel', 'InnoAppService', '$filter',
	function ($scope, $controller, $modal, listModel, InnoAppService, $filter) {

		$controller('ListAppFormController', {$scope: $scope});
		$controller('ListConfigurationController', {$scope: $scope});

		$scope['new'] = {
			category: ''
		};


		$scope.init = function (appId) {
			if (angular.isDefined(appId)) {
				InnoAppService.getApp(appId, false, function (response) {
					$scope.app = response;
					// needed for generic.list.controller
					$scope.container = $scope.app.container;
					// check if categories present
					$scope.saveform.$setValidity('reason', $scope.app.categories.length > 0);
					// remove item values as not needed and deserialized correctly in backend!
					$scope.app.container.items = [];
				});
			} else {
				$scope.app = {
					categories: [],
					ttl: 0,
					allowSearchByAuthor: true,
					canBeRevoked: true,
					commentsAllowed: true,
					ideasHiddenField: false,
					container: _createDefaultContainer(),
					allowDeanonymization: true,
					visibility: "PUBLIC",
					title: $filter('i18n')('app.inno')
				};
				// needed for generic.list.controller
				$scope.container = $scope.app.container;
			}

			$scope.userchooser = userchooserOptions;
			$scope.userchooser.field.properties.multiple = true;
			$scope.userchooser.field.required = false;
			$scope.userchooser.options.width = 'resolve';//778;
			$scope.fieldTypes = $scope.$parent.fieldTypes;
		};

		var _createDefaultContainer = function () {
			return {
				name: '',
				permissions: {Create: true, Read: 'all', Update: 'own', Delete: 'own'},
				fields: []
			};
		};

		$scope.remove = function (index) {
			$scope.app.categories.splice(index, 1);
			$scope.saveform.categoryInput.$setValidity('reason', $scope.app.categories.length > 0);
		};

		$scope.addCategory = function (keyEvent) {
			if (!keyEvent || keyEvent.which === 13) {
				if ($scope['new'].category && $scope['new'].category.length < 255 && _.findWhere($scope.app.categories, {'name': $scope['new'].category}) === undefined) {
					$scope.app.categories.push({name: $scope['new'].category});
					$scope['new'].category = "";
					$scope.saveform.categoryInput.$setValidity('reason', $scope.app.categories.length > 0);

				}
			}
		};
	}
]);


// Controls the detail page of the inno app
app.controller('InnoAppOverviewController', ['$scope', '$controller', '$modal', '$http', 'InnoAppService', '$routeParams', 'errorHandler', '$timeout',
	function ($scope, $controller, $modal, $http, InnoAppService, $routeParams, errorHandler, $timeout) {

		/**
		 * Initializes the app. Loads the app and ideas from the backend and defines filters and params.
		 */
		$scope.init = function() {
			// get app id from InnoAppRootController
			$scope.appId = $scope.$parent.appId;
			$scope.isModerator = false;

			if (!$scope.appId) {
				return;
			}

			// retrieve field types to provide selection options
			InnoAppService.getStatusList(function (response) {
				$scope.statusList = response;
			});

			$controller('ListOverviewController', {$scope: $scope});

			$scope.userchooser = userchooserOptions;
			$scope.userchooser.field.properties.multiple = false;
			$scope.userchooser.options.width = 280;

			$scope.filterParams = {
				page: 1,
				sensitiveInformation: null
			};

			$scope.dateFormat = Coyo.dateFormats.normal;
			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

			InnoAppService.getApp($scope.appId, true, function (response) {
				$scope.app = response;
				// needed for generic.list.controller
				$scope.container = $scope.app.container;
			});

			InnoAppService.isModerator($scope.appId, function(response) {
				$scope.isModerator = response === 'true';
			}, function() {
				$scope.isModerator = false;
			});
		};


		var updateIdeas = function () {
			$scope.loading = true;
			if (!$scope.app || !$scope.app.container) {
				return;
			}

			var filterParams = createFilterParams($scope.pageSize);

			InnoAppService.getIdeasFiltered($scope.appId, filterParams, function (results) {
				$scope.app.container.items = results.ideas;
				$scope.ideasCount = results.count;
				$scope.loading = false;
			});
		};

		var createFilterParams = function(pageSize) {
			var filterParams = _.clone($scope.filterParams);
			filterParams.containerId = $scope.app.container.id;

			// remove timezone information from datepicker date to solve: https://github.com/angular-ui/bootstrap/issues/2072
			if ($scope.filterParams.from) {
				filterParams.from = moment($scope.filterParams.from).format("YYYY-MM-DD");
			}
			if ($scope.filterParams.till) {
				filterParams.till = moment($scope.filterParams.till).format("YYYY-MM-DD");
			}

			filterParams.firstResult = ($scope.filterParams.page - 1) * $scope.pageSize;
			filterParams.maxResults = pageSize;

			filterParams.sensitiveInformation = ($scope.filterParams.sensitiveInformation === 'true' ||
			filterParams.sensitiveInformation === 'false') ? $scope.filterParams.sensitiveInformation : null;

			return filterParams;
		};

		$scope.$watch('filterParams', _.throttle(function () {
				updateIdeas();
		}, 900, {leading: false}), true);

		$scope.open = function ($event, opened) {
			$event.preventDefault();
			$event.stopPropagation();
			$scope[opened] = true;
		};

		$scope.csvExport = function () {
			var url = '/ideas/csv';
			$http.post(url, createFilterParams(0))
				.success(function (response) {
					var blob = new Blob([response], {type: 'text/comma-separated-values'});
					var filename = $scope.app.senderDisplayName + '-' + $scope.app.container.name + '.csv';
					saveAs(blob, filename); // FileSave.js
				});
		};

		// open item creation modal dialog
		$scope.createIdea = function () {
			$scope.currentIdea = {};
			$scope.itemValues = {};
			$scope.itemValidation = {};

			var modalInstance = $modal.open({
				backdrop: 'static',
				templateUrl: Coyo.scripts + '/views/apps/inno/editIdea.html',
				controller: 'InnoItemModalController',
				scope: $scope,
				windowClass: 'modal modal-large'
			});

			modalInstance.result.then(function (response) {
				$scope.itemValues = {};
				$scope.itemValidation = {};
				$scope.currentIdea = {};

				updateIdeas();
			});
		};

		$scope.editIdea = function (idea) {
			// load idea with all values via rpc
			InnoAppService.getIdea(idea.id, function (idea) {
				$scope.currentIdea = idea;
				$scope.itemValues = {};
				$scope.itemValidation = {};

				/* convert item values */
				angular.forEach($scope.container.fields, function (field) {
					var itemValue = _.findWhere($scope.currentIdea.values, {fieldId: field.id});
					if (angular.isDefined(itemValue)) {
						this[field.id] = itemValue.value;
					}
				}, $scope.itemValues);

				var modalInstance = $modal.open({
					templateUrl: Coyo.scripts + '/views/apps/inno/editIdea.html',
					controller: 'InnoItemModalController',
					scope: $scope,
					windowClass: 'modal modal-large'
				});

				/**
				 * Update item in view (or delete it if action was a deletion) and reset temporary item model
				 */
				modalInstance.result.then(function (response) {
					updateIdeas();
				});
			});
		};

		$scope.followIdea = function (idea) {
			InnoAppService.follow(idea.id, function (success) {
				idea.following = true;
			}, function (response, status) {
				$scope.errors = errorHandler.parseErrors($scope.container, response, status);
			});
		};

		$scope.unfollowIdea = function (idea) {
			InnoAppService.unfollow(idea.id, function (success) {
				idea.following = false;
			}, function (response, status) {
				$scope.errors = errorHandler.parseErrors($scope.container, response, status);
			});
		};

		// open status configuration modal dialog
		$scope.openStatusDialog = function (idea) {
			$scope.currentIdea = idea;
			$scope.currentIdea.newStatus = idea.status;

			var modalInstance = $modal.open({
				templateUrl: Coyo.scripts + '/views/apps/inno/statusdialog.html',
				controller: 'InnoItemModalController',
				scope: $scope,
				windowClass: 'modal'
			});

			modalInstance.result.then(function (response) {
				updateIdeas();
			});
		};

		// open item detail view modal
		$scope.showDetails = function (idea) {
			InnoAppService.getIdea(idea.id, function (idea) {
				$scope.currentIdea = idea;

				var modalInstance = $modal.open({
					templateUrl: Coyo.scripts + '/views/apps/inno/details.html',
					controller: 'InnoItemModalController',
					scope: $scope,
					windowClass: 'modal large'
				});

				modalInstance.result.then(function () {
					updateIdeas();
				}, function () {
					updateIdeas();
				});
			});
		};

		// show status (read only)
		$scope.showStatus = function (idea) {
			$scope.currentIdea = idea;
			var modalInstance = $modal.open({
				templateUrl: Coyo.scripts + '/views/apps/inno/showStatus.html',
				controller: 'InnoItemModalController',
				scope: $scope,
				windowClass: 'modal'
			});
		};

		// show status (read only)
		$scope.showRating = function (idea) {
			$scope.currentIdea = idea;
			var modalInstance = $modal.open({
				templateUrl: Coyo.scripts + '/views/apps/inno/showRatings.html',
				controller: 'InnoItemModalController',
				scope: $scope,
				windowClass: 'modal'
			});
		};

		$scope.deanonymizeIdea = function (idea) {
			idea.deanonymized = idea.deanonymized ? false : true;
		};

		// delete idea
		$scope.deleteIdea = function (container, idea) {
			InnoAppService.deleteIdea(container.id, idea.id, function (response) {
				container.items.splice(_.indexOf(container.items, idea), 1);
				_.find(container.items, function (_item, index) {
					// find and remove deleted idea links
					_.find(_item.linkedIdeas, function (linkedIdea, linkedIndex) {
						if (idea.id === linkedIdea.id) {
							_item.linkedIdeas.splice(index, 1);
							return true;
						}
					});
					// find and remove deleted idea in app container
					if (idea.id === _item.id) {
						container.items.splice(index, 1);
						return true;
					}
				});
			}, function (response, status) {
				var modalInstance = $modal.open({
					templateUrl: Coyo.scripts + '/views/apps/inno/deleteError.html',
					controller: 'InnoItemModalController',
					scope: $scope,
					windowClass: 'modal'
				});
			});
		};

		$scope.init();

		if ($routeParams.idea) {
			InnoAppService.getIdea($routeParams.idea, function (idea) {
				$scope.showDetails(idea);
			});
		}
	}
]);


/**
 * Controller for creation and modification of ideas via modal dialog
 */
app.controller('InnoItemModalController', ['$scope', '$modalInstance', '$controller', 'listModel', 'valueUtils', 'errorHandler', 'InnoAppService', function ($scope, $modalInstance, $controller, listModel, valueUtils, errorHandler, InnoAppService) {
	$controller('ListOverviewController', {$scope: $scope, $modalInstance: $modalInstance});
	$controller('ListItemController', {$scope: $scope, $modalInstance: $modalInstance});

	$scope.itemMeta = {};
	$scope.showSimilar = false;
	$scope.similarIdeas = [];

	// if no fields defined we avoid list app validation
	if ($scope.$parent.app && $scope.$parent.app.container && $scope.$parent.app.container.fields.length < 1) {
		$scope.formIsValid = true;
	}

	$scope.saveIdea = function (idea, itemValues) {
		$scope.saveIdeaDisabled = true;
		
		$scope.createNewIdea($scope.container.id, idea, valueUtils.parseItemValues(itemValues), function (response) {
			$modalInstance.close(response);
			$scope.saveIdeaDisabled = false;
		}, function (response, status) {
			$scope.errors = errorHandler.parseErrors($scope.container, response, status);
			$scope.saveIdeaDisabled = false;
		});
	};

	$scope.createNewIdea = function (containerId, idea, itemValues, onSuccess, onFail) {
		InnoAppService.createIdea($scope.app.container.id, idea, valueUtils.parseItemValues(itemValues), onSuccess, onFail);
	};

	/**
	 * Finds similar ideas and returns true if the user submits modal
	 * If the user cancels the modal false will be returned
	 * @param idea
	 * @param onSuccess
	 */
	$scope.findSimilarIdeas = function (idea, itemValues) {
		InnoAppService.getSimilarIdeas(idea, $scope.$parent.app, function (similarIdeas) {
			if (similarIdeas && similarIdeas.length > 0) {
				$scope.showSimilar = true;
				$scope.similarIdeas = similarIdeas;
			} else {
				$scope.showSimilar = false;
				$scope.similarIdeas = [];
				$scope.saveIdea(idea, itemValues);
			}
		});
	};

	$scope.dismissSimilar = function () {
		$scope.showSimilar = false;
		$scope.similarIdeas = [];
	};

	$scope.updateIdea = function (currentIdea, itemValues) {
		$scope.errors = [];
		InnoAppService.updateIdea(currentIdea, valueUtils.parseItemValues(itemValues), function (response) {
			currentIdea.id = response.id;
			$modalInstance.close(response);
		}, function (response, status) {
			$scope.errors = errorHandler.parseErrors($scope.container, response, status);
		});
	};

	$scope.setStatus = function (newStatus) {
		InnoAppService.setIdeaStatus($scope.currentIdea.id, newStatus,
			$scope.currentIdea.statusComment ? $scope.currentIdea.statusComment : '', function (response) {
				$scope.currentIdea.status = newStatus;
				$modalInstance.close(response);
			}, function (response, status) {
				$scope.errors = errorHandler.parseErrors($scope.container, response, status);
			});
	};


	$scope.closeModal = function (response) {
		$modalInstance.close($scope.currentIdea);
	};
}]);


/**
 * Controller for creation and modification of ideas via modal dialog
 */
app.controller('IdeaRatingController', ['$scope', '$controller', 'listModel', 'valueUtils', 'errorHandler', 'InnoAppService', function ($scope, $controller, listModel, valueUtils, errorHandler, InnoAppService) {
	$scope.rate = 0;
	$scope.max = 5;
	$scope.isReadonly = false;

	$scope.hoveringOver = function (value) {
		$scope.overStar = value;
		$scope.percent = 100 * (value / $scope.max);
	};

	$scope.ratingStates = [
		{stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
		{stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
		{stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
		{stateOn: 'glyphicon-heart'},
		{stateOff: 'glyphicon-off'}
	];
}]);



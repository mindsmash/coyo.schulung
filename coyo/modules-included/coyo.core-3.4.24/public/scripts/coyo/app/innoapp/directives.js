var app = angular.module('coyo.inno.directives', []);

app.directive('custom', function () {
	return {
		require: 'ngModel',
		link: function (scope, elm, attrs, ngModelCtrl) {
			scope.$watch(attrs.ngModel, function () {
				if (scope.app && scope.app.categories && scope.app.categories.length < 1) {
					ngModelCtrl.$setValidity('categories', false);
				} else if (scope.app && scope.app.categories && scope.app.categories.length > 0) {
					ngModelCtrl.$setValidity('categories', true);
				}
			}, true);
		}
	};
});

app.directive('ideasPluginList', ['InnoAppService', function (InnoAppService) {
	return {
		restrict: 'A',
		replace: true,
		templateUrl: Coyo.scripts + '/views/plugins/inno/ideas.html',
		scope: {
			pageSize: '='
		},

		link: function (scope, elm, attrs) {
			scope.currentPage = 0;
			scope.ideas = [];

			InnoAppService.findNewIdeas(function (ideas) {
				scope.ideas = ideas;
				scope.numberOfPages = function () {
					return Math.ceil(scope.ideas.length / scope.pageSize);
				};
			});
		}
	};
}]);

/**
 * Renders 5 stars for the user to rate an idea. After the users rating is 
 * successfully send, the idea is updated.
 */
app.directive('rateIdea', ['$log', '$http', 'InnoAppService', function($log, $http, InnoAppService) {
	return {
		restrict    : 'A',
		templateUrl : Coyo.scripts + '/views/apps/inno/rating.html',
		scope: {
			id   : '@',
			idea : '=rateIdea'
		},
		link: function (scope, elm, attrs) {
			scope.rateIdea = function(rating) {
				InnoAppService.rateIdea(scope.idea.id, rating, function(idea) {
					scope.idea = idea;
				}, function(error) {
					$log.error(error);
				});
			};

			scope.withdrawRating = function () {
				var url = '/app/inno/rating/' + scope.idea.id;
				$http.delete(url)
					.success(function (idea) {
						scope.idea = idea;
					}).error(function (error) {
						$log.error(error);
					});
			};
		}
	};
}]);

/**
 * Show the passed rating with up to five stars. The shown rating is watched in
 * order to change the rating dynamically, if it changes (e.g. when a user is 
 * rating this idea.)
 */
app.directive('ideaRatingOverview', ['InnoAppService', function (InnoAppService) {
	return {
		restrict: 'A',
		replace: false,
		templateUrl: Coyo.scripts + '/views/apps/inno/rating-overview.html',
		scope: {
			rating : '=ideaRatingOverview'
		},
		link : function(scope) {
			scope.$watch('rating', function () {
				if (_.isNumber(scope.rating)) {
					scope.width = 20 * scope.rating + "%";
				} else {
					scope.width = 20 * scope.rating.replace(',', '.') + '%';
				}
			});
		}
	};
}]);

app.directive('linkedIdeas', ['InnoAppService', function (InnoAppService) {
	return {
		templateUrl: Coyo.scripts + '/views/apps/inno/linkedIdeas.html',
		restrict: 'A',
		scope: {
			idea: '=',
			clazz: '@'
		},
		controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
			$scope.newLink = '';
			$scope.idea.url = $scope.idea.url + '?f=' + Date.now();
			$scope.address = {};

			$scope.selectIdeaOptions = {
				ajax: {
					url: '/app/inno/idea/linkable/' + $scope.idea.id,
					dataType: 'json',
					type: 'POST',
					data: function (term, page) {
						return {
							term: term
						};
					},
					results: function (ideas, page) {
						var results = [];
						for (var i = 0; i < ideas.length; i++) {
							if (ideas[i].id === $scope.idea.id) {
								continue;
							}

							var beenLinked = false;
							for (var ii = 0; ii < $scope.idea.linkedIdeas.length; ii++) {
								if ($scope.idea.linkedIdeas[ii].id === ideas[i].id) {
									beenLinked = true;
									break;
								}
							}
							if (beenLinked === false) {
								results.push(ideas[i]);
							}
						}
						return {results: results};
					}
				},
				formatResult: function (idea, container, query) {
					return idea.title;
				},
				formatSelection: function (object, container, escapeMarkup) {
					return escapeMarkup((_.isEmpty(object) || _.isUndefined(object)) ? '' : object.title);
				}
			};

			$scope.delinkItem = function (idea) {
				InnoAppService.delinkIdea($scope.idea.id, idea, function (response) {
					$scope.idea.linkedIdeas = response.linkedIdeas;
					$scope.newLink = '';
				});
			};

			$scope.linkIdea = function () {
				if ($scope.newLink && $scope.idea && $scope.idea.id) {
					InnoAppService.linkIdea($scope.idea.id, $scope.newLink, function (response) {
						$scope.idea.linkedIdeas = response.linkedIdeas;
						$scope.newLink = '';
					});
				}
			};

			// toggle to edit mode or back
			$scope.toggleEdit = function() {
				$scope.editMode = !$scope.editMode;
			};
		}]
	};
}]);


/**
 * Renders one idea on the overview list. This directive does not use 
 * an isolated scope and depends on scope variables and methods defined 
 * in the parent controller.
 */
app.directive('renderIdea', [function () {
	return {
		templateUrl: Coyo.scripts + '/views/apps/inno/renderIdea.html',
		restrict: 'A',
		replace: true
	};
}]);


/**
 * Renders the remaining days ribbon in the top right corner.
 */
app.directive('remainingDays', ['InnoAppUtils', function(InnoAppUtils) {
	return {
		templateUrl: Coyo.scripts + '/views/apps/inno/remainingDays.html',
		restrict: 'A',
		scope: {
			idea : '=remainingDays',
			ttl  : '@'
		},
		link : function(scope) {
			if (scope.ttl > 0) {
				scope.days = InnoAppUtils.getRemainingDays(scope.idea, scope.ttl);
			}
		}
	};
}]);
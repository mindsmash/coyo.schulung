var module = angular.module('coyo.wikiapp.controller', []);

module.controller('WikiAppCtrl', ['$scope', 'WikiAppService', function ($scope, WikiAppService) {
	$scope.loading = true;

	$scope.options =
	{
		dropped: function (event) {
			var dest = event.dest;
			if (!_.isNull(dest) && !_.isUndefined(dest)) {
				var index = dest.index,
					destId = null;

				if (!_.isNull(dest.nodesScope.$parent.$modelValue) && !_.isUndefined(dest.nodesScope.$parent.$modelValue)) {
					destId = dest.nodesScope.$parent.$modelValue.id;
				}

				var source = event.source;
				if (!_.isNull(source) && !_.isUndefined(source)) {
					var articleID = source.nodeScope.$modelValue.id;

					if (!_.isNull(index) && !_.isUndefined(index) && !_.isUndefined(destId) && !_.isNull(articleID) && !_.isUndefined(articleID)) {
						WikiAppService.moveArticle(articleID, destId, index, _.noop);
					}
				}
			}
		}
	};
	
	$scope.init = function(appId, mobile) {
	
		WikiAppService.getRootNodes(appId, function(nodes) {
			$scope.loading = false;
			$scope.tree = nodes;
		});
	
		WikiAppService.isEditable(appId, function(result) {
			$scope.isEditable = (mobile === true) ? false : result;
		});
	};

	$scope.createUrl = function(id) {
		return ROUTES.WikiApp_article.url({id: id});
	};

	$scope.toggleNode = function(scope) {
		scope.toggle();
	};

	// unused
	$scope.collapseAll = function() {
		var scope = getRootNodesScope();
		scope.collapseAll();
	};

	$scope.expandAll = function() {
		var scope = getRootNodesScope();
		scope.expandAll();
	};
}]);

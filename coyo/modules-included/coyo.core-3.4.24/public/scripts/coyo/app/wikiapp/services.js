var module = angular.module('coyo.wikiapp.services', []);

module.service('WikiAppService', ['$http', function ($http) {
	
	/* fetch child articles of one wiki app article */
	var _getRootNodes = function (id, onSuccess) {
		$http({
			url: ROUTES.WikiApp_getRootNodes.url({id: id}),
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	/* move article to another node */
	var _moveArticle = function (id, destId, index, onSuccess) {
		$http.post(ROUTES.WikiApp_moveArticle.url(), {
				id: id,
				destId: destId,
				destIndex: index
		}).success(function (response) {
			onSuccess(response);
		});
	};

	/* check whether tree should be editable or not */
	var _isEditable = function (id, onSuccess) {
		$http({
			url: ROUTES.WikiApp_isEditable.url({id: id}),
			method: 'GET',
			responseType: 'json'
		}).success(function (response) {
			onSuccess(response);
		});
	};

	return {
		getRootNodes : _getRootNodes,
		moveArticle   : _moveArticle,
		isEditable   : _isEditable
	};
}]);
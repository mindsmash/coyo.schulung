/* jshint -W117 */
angular.module('coyo.forum.services', [])
	.factory('forumModel', ['$http', function($http) {
		return {
			deleteTopic : function(topicId, onSuccess) {
				var url = '/forum/topic/' + topicId;
				
				$http({
					method : 'DELETE',
					url    : url
				}).success(function() {
					onSuccess();
				});
			}, 
			createTopic : function(appId, title, content, attachments, onSuccess) {
				var url = '/forum/topic/' + appId,
					formData = {title : title, content : content, attachments : attachments};
						
				$http({
					method : 'POST',
					url    : url,
					data   : formData
				}).success(function(response) {
					onSuccess(response);
				});
			},
			getTopics : function(appId, params, onSuccess) {
				var url = '/forum/topics/' + appId;
				
				$http({
					method : 'GET',
					url    : url,
					params : params
				}).success(function(response) {
					onSuccess(response);
				});
			},
			deletePost : function(postId, onSuccess) {
				var url = '/forum/post/' + postId;
					
				$http({
					method : 'DELETE',
					url    : url
				}).success(function() {
					onSuccess();
				});
			},
			addPost : function(topicId, content, attachments, onSuccess) {
				var url = '/forum/addPost/' + topicId,
					formData = {content : content, attachments : attachments};
			
				$http({
					method : 'POST',
					url    : url,
					data   : formData
				}).success(function(response) {
					onSuccess(response);
				});
			},
			getTopic : function(topicId, onSuccess) {
				var url = '/forum/topic/' + topicId;
				$http({
					method : 'GET',
					url    : url
				}).success(function(response) {
					onSuccess(response);
				});
			},
			getTopicByPost : function(postId, onSuccess) {
				var url = '/forum/topic/post/' + postId;
				$http({
					method : 'GET',
					url    : url
				}).success(function(response) {
					onSuccess(response);
				});
			},
			getPosts : function(topicId, params, onSuccess) {
				var url = '/forum/posts/' + topicId;
				$http({
					method : 'GET',
					url    : url,
					params : params
				}).success(function(response) {
					onSuccess(response);
				});
			},
			setTopicClosed : function(topicId, closed, onSuccess) {
				var url = '/forum/topic/' + topicId + "/close/" + closed;				
				$http({
					method : 'POST',
					url    : url
				}).success(function(response) {
					onSuccess(response);
				});
			}
		};
	}]);
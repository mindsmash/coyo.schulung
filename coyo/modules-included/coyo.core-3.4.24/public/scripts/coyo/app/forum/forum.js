/* jshint -W117 */
angular.module('coyo.forum', ['coyo.generic', 'ngRoute', 'coyo.forum.controllers', 'coyo.forum.services', 'infinite-scroll', 'coyo.generic.filechooser'])
	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		
		var getQueryParam = function(paramName) {
			var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"),
			results = regex.exec(location.search);
			if (results !== null) {
				return decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			return null;
		};
		
		$routeProvider
			.when('/', {
				redirectTo : function() {
					var postId = getQueryParam('postId');
					if (postId !== null) {
						return '/post/' + postId;
					}
					var topicId = getQueryParam('topicId');
					if (topicId !== null) {
						return '/topic/' + topicId;
					}
					return '/index';
				}
			})
			.when('/index', {
				templateUrl: Coyo.scripts + '/views/apps/forum/topiclist.html',
				controller: 'CoyoForumListCtrl'
			})
			.when('/topic/:topicId', {
				templateUrl: Coyo.scripts + '/views/apps/forum/topicdetail.html',
				controller: 'CoyoForumDetailCtrl'
			})
			.when('/post/:postId', {
				templateUrl: Coyo.scripts + '/views/apps/forum/topicdetail.html',
				controller: 'CoyoForumDetailCtrl'
			})
			.otherwise({ 
				templateUrl: Coyo.scripts + '/views/apps/forum/topiclist.html',
				controller: 'CoyoForumListCtrl'
			});
		}
	]);
/* jshint -W117 */
var module = angular.module('coyo.forum.controllers', []);

module.controller('CoyoForumCtrl', ['$scope', function($scope) {
	// initialize
	$scope.init = function(appId, createTopicPermission) {
		$scope.appId = appId;
		$scope.createTopicPermission = createTopicPermission;
	};
}]);


module.controller('CoyoForumListCtrl', ['$scope', '$modal', '$location', 'forumModel', function($scope, $modal, $location, forumModel) {
	var requestParams = {page : 1, length: 25};
	
	$scope.appId = $scope.$parent.appId;
	$scope.topics = [];
	$scope.loading = {busy : false, done : false};
	
	// get list of topics with endless scrolling
	$scope.getTopics = function() {
		$scope.loading.busy = true;
		forumModel.getTopics($scope.appId, requestParams, function(response) {
			$scope.topics = $scope.topics.concat(response);
			requestParams.page++;
			if (response.length < requestParams.length) {
				$scope.loading.done = true;
			}
			$scope.loading.busy = false;
		});
	};
	$scope.getTopics();  // initial request
	
	// show options for each topic on hover
	$scope.hover = function(topic) {
		topic.showOptions = !topic.showOptions;
	};
	
	// show modal
	$scope.showForm = function() {
		var modalInstance = $modal.open({
			templateUrl : Coyo.scripts + '/views/apps/forum/newtopic.html',
			controller  : 'CoyoForumFormCtrl',
			scope       : $scope
		});
	
		// update list when new topic was created
		modalInstance.result.then(function(result) {
			$location.url('topic/' + result.id);
		});
		
	};
	
	// delete given topic
	$scope.deleteTopic = function(index) {
		var topic = $scope.topics[index];
		forumModel.deleteTopic(topic.id, function() {
			$scope.topics.splice(index, 1);
		});
	};
	
	// close or reopen the given topic
	$scope.setTopicClosed = function(index, closed) {
		var topic = $scope.topics[index];
		forumModel.setTopicClosed(topic.id, closed, function(response) {
			$scope.topics[index] = response;
		});
	};
	
}]);


module.controller('CoyoForumDetailCtrl', ['$scope', '$routeParams', '$location', 'forumModel', function($scope, $routeParams, $location, forumModel) {
	$scope.fileField = {id : "forum", required : false};
	$scope.fileAttachments = [];
	$scope.paging = {page : 1, length: 10, total : 0, maxPages : 10};
	$scope.loading = false;

	var renderTopic = function(response) {
		$scope.loading = false;
		$scope.topic = response;
		$scope.paging.total = response.postCount;
		// initially trigger pageChanged to get the first page of posts loaded
		$scope.paging.page = (typeof response.page !== 'undefined') ? response.page : 1;
		$scope.pageChanged($scope.paging.page);
	};
	
	// get topic
	$scope.loading = true;
	if (typeof $routeParams.topicId !== 'undefined') {
		forumModel.getTopic($routeParams.topicId, renderTopic);
	} else if ($routeParams.postId !== 'undefined') {
		$scope.highlight = $routeParams.postId;
		forumModel.getTopicByPost($routeParams.postId, renderTopic);
	}
	
	// get posts to display on page change
	$scope.pageChanged = function(page) {
		$scope.loading = true;
		var params = {page : page, length : $scope.paging.length};
		forumModel.getPosts($scope.topic.id, params, function(response) {
			$scope.loading = false;
			$scope.topic.posts = response;
		});
	};
	
	// show options for each post on hover
	$scope.hover = function(post) {
		post.showOptions = !post.showOptions;
	};
	
	// add a new post to a topic
	$scope.addPost = function() {
		forumModel.addPost($scope.topic.id, $scope.content, $scope.fileAttachments, function(response) {
			$scope.content = '';
			$scope.fileAttachments = [];
			$scope.paging.page = $scope.numPages;
			$scope.pageChanged($scope.numPages);
			$scope.topic.posts.push(response);
			$scope.paging.total++;
		});
	};
	
	// delete a post
	$scope.deletePost = function(index) {
		var post = $scope.topic.posts[index];
		forumModel.deletePost(post.id, function() {
			$scope.topic.posts.splice(index, 1);
			$scope.paging.total--;
		});
	};
	
	// delete the current topic and link to the index page
	$scope.deleteTopic = function() {
		forumModel.deleteTopic($scope.topic.id, function() {
			$location.url('index');
		});
	};
	
	// close or reopen the given topic
	$scope.setTopicClosed = function(closed) {
		forumModel.setTopicClosed($scope.topic.id, closed, renderTopic);
	};
	
}]);


module.controller('CoyoForumFormCtrl', ['$scope', '$modalInstance', '$window', 'forumModel', function($scope, $modalInstance, $window, forumModel) {
	$scope.fileField = {id : "forum", required : false};
	
	// create new topic
	$scope.create = function(topic) {
		forumModel.createTopic($scope.appId, topic.title, topic.content, topic.attachments, function(response) {
			$scope.topic = '';
			$modalInstance.close(response);
		});
	};
	
	// dismiss window
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
}]);
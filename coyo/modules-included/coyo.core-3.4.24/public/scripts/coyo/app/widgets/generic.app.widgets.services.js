var module = angular.module('coyo.app.widgets.services', []);

module.factory('WidgetModel', ['$http', '$q', function($http, $q) {
	var WidgetModel = {
		getWidgets : function(appId) {
			var deferred = $q.defer();
			
			$http.get(ROUTES.AppWidgets_widgets.url(), {
				params : {id : appId}
			}).success(function(data, status, headers, config) {
				deferred.resolve(data.elements);
			});
			
			return deferred.promise;
		},
		createWidget : function(targetAppId, app, widget) {
			var deferred = $q.defer();
			
			$http.post(ROUTES.AppWidgets_create.url(), {}, {
				params : {
					targetAppId : targetAppId,
					sourceAppId : app.id,
					widgetKey   : widget.key,
					title       : widget.title
				}
			}).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			
			return deferred.promise;
		},
		deleteWidget: function(widgetId) {
			var deferred = $q.defer();
			
			$http({
				method: 'DELETE',
				url: ROUTES.AppWidgets_delete.url(),
				params : { id : widgetId }
			}).success(function(data, status, headers, config) {
				deferred.resolve(widgetId);
			});
			
			return deferred.promise;
		},
		sortWidgets: function(appId, sortedIds) {
			$http.post(ROUTES.AppWidgets_sort.url(), {}, {
				params : {
					appId : appId,
					sortOrder : sortedIds
				}
			});
		},
		getCatalog : function(appId) {
			var deferred = $q.defer();
			
			$http.get(ROUTES.AppWidgets_catalog.url(), {
				params : {appId : appId}
			}).success(function(data, status, headers, config) {
				deferred.resolve(data.elements);
			});
			
			return deferred.promise;
		}
	};
	
	return WidgetModel;
}]);
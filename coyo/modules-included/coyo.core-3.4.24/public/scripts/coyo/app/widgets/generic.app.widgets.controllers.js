/*jshint -W055 */

/**
 * Controller for app widgets
 */
var module = angular.module('coyo.app.widgets.controllers', []);

module.controller('AppWidgetsController', ['$scope', '$http', '$rootScope', '$modal', 'WidgetModel', function($scope, $http, $rootScope, $modal, WidgetModel) {
	
	$scope.init = function(appId) {
		$scope.appId = appId; 
		
		WidgetModel.getWidgets(appId).then(function(widgets) {
			$scope.widgets = widgets;
		});
	};
	
	$scope.sortableOptions = {
		axis: 'y',
		handle: '.widget-sort',
		stop: function(e, ui) {
			var ids = "";
			_.each($scope.widgets, function(widget, i){
				if(i > 0) {
					ids += ",";
				}
				ids += widget.id;
			});
			
			WidgetModel.sortWidgets($scope.appId, ids);
		}
	};
	
	$rootScope.$on('changedAppWidgetsEditMode', function(event, editMode) {
		$scope.editMode = editMode;
	});
	
	$scope.addWidget = function() {
		// show modal
		$scope.addWidgetModal = $modal.open({
			templateUrl : Coyo.scripts + '/views/coyo/app/widgets/addwidget.html',
			controller  : 'NewAppWidgetController',
			scope       : $scope,
			windowClass : 'large'
		});
	
		// update list when new topic was created
		$scope.addWidgetModal.result.then(function(result) {
			if (result != null) {
				$scope.widgets.push(result);
			}
		});
			
	};
	
	$scope.deleteWidget = function(widgetId) {
		WidgetModel.deleteWidget(widgetId).then(function(widgetId) {
			$scope.widgets = _.without($scope.widgets, _.findWhere($scope.widgets, { id : widgetId }));
		});
	};
	
	$scope.getTemplateUrl = function(widget) {
		return Coyo.scripts + '/views/apps/widgets/' + widget.sourceAppKey + '-' + widget.key + '.html';
	};
	
	$scope.finish = function() {
		$rootScope.editMode = false;
		$rootScope.$emit('changedAppWidgetsEditMode', false);
	};
}]);

module.controller('AppWidgetsSwitchController', ['$scope', '$rootScope', function($scope, $rootScope) {
	$rootScope.editMode = false;
	
	$scope.switchMode = function() {
		$rootScope.editMode = !$rootScope.editMode;
		$rootScope.$emit('changedAppWidgetsEditMode', $rootScope.editMode);
	};
}]);

module.controller('NewAppWidgetController', ['$scope', '$http', 'WidgetModel', function($scope, $http, WidgetModel) {
	$scope.newWidget = {};
	
	$scope.init = function() {
		$scope.imagePath = Coyo.images;
		WidgetModel.getCatalog($scope.appId).then(function(widgets) {
			$scope.catalog = widgets;
		});
	};
	
	$scope.close = function(){
		$scope.addWidgetModal.close();
	};
	
	$scope.getWidgetIconUrl = function(appKey, widgetKey) {
		return Coyo.images + '/app-' + appKey + '-widget-' + widgetKey + '.png';
	};
	
	$scope.selectWidget = function(app, widget) {
		if (!widget.selectable) {
			return;
		}
		widget.title = "";
		
		$scope.newWidget = {
			app    : app,
			widget : widget
		};
	};
	
	$scope.ok = function() {
		if (!$scope.newWidget.widget.selectable) {
			return;
		}
		WidgetModel.createWidget($scope.appId, $scope.newWidget.app, $scope.newWidget.widget).then(function(widget) {
			$scope.addWidgetModal.close(widget);
		});
	};
	
	$scope.reset = function() {
		$scope.newWidget = {};
	};
}]);
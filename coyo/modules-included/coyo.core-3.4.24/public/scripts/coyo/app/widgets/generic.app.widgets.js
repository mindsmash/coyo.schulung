/**
 * Widgets module for creating, sorting and displaying app widgets.
 */
angular.module('coyo.app.widgets', ['coyo.app.widgets.controllers', 'coyo.app.widgets.services', 'ui.sortable']);
var module = angular.module('coyo.socialLinklist.socialLinklist.directives', []);

module.directive('focus', ['$timeout', function ($timeout) {
    return {
      restrict: 'A',
      link: function ($scope, $element) {
        $timeout(function () {
        $element[0].focus();
        });
      }
    };
  }
]);

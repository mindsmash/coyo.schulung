
angular.module('coyo.socialLinklist.socialLinklist.services', [])
.factory('linklistModel', ['$http', function($http){
	return {
		saveLink : function(appId, link, onSuccess){			
			var url = ROUTES.SocialLinklist_save.url({'id' : appId});
			$http({
				method	: 'POST',
				url		: url,
				data	: link
			}).success(function(response) {
				var result = angular.isDefined(response.members) ? response.members : response;
				onSuccess(result);
			});
		},

		getLinks : function(appId, onSuccess){		
			var url = ROUTES.SocialLinklist_getLinks.url({'id' : appId});
			$http({
				method	: 'GET',
				url		: url
			}).success(function(response) {
				onSuccess(response);
			});
		},
		
		deleteLink : function(linkId, onSuccess){
			var url = ROUTES.SocialLinklist_deleteLink.url({'id' : linkId});
			$http({
				method	: 'DELETE',
				url		: url
			}).success(function() {
				onSuccess();
			});
		},
		
		getMetaOfUrl : function(link, onSuccess){
			var url = ROUTES.SocialLinklist_getMetaOfUrl.url();
			$http({
				method : 'POST',
				url    : url,
				data   : link
			}).success(function(response) {
				var result = angular.isDefined(response.members) ? response.members : response;
				onSuccess(result);
			});
		},
		order : function(appId, links, onSuccess, onError) {
			var url = ROUTES.SocialLinklist_order.url({'id': appId }),
				order = _.pluck(links, 'id');		
			$http({
				method : 'POST',
				url    : url,
				data   : order
			}).success(function(response) {
				onSuccess(response);
			}).error(function(data, status, headers, config) {
				onError(data, status);
			});
		}
	};
}]);
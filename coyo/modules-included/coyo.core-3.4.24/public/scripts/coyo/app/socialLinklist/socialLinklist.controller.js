
var module = angular.module('coyo.socialLinklist.socialLinklist.controller', []);

module.controller('CoyoLinklistShowListCtrl', ['$scope','$modal', '$location', '$routeParams', 'linklistModel', function($scope, $modal, $location, $routeParams, linklistModel){
	$scope.loading = {busy : false};
	$scope.links = [];
	$scope.syncingOrder = false;
	$scope.errorSyncOrder = false;
	$scope.addAllowed = false;
	$scope.editAllowed = false;
	
	$scope.init = function(appId) {
		$scope.appId = appId;
		$scope.getLinks();
	};
	
	$scope.showForm = function() {
		$scope.currData = {
				title: "",
				linkUrl: "",
				description: "",
				enterTitelAndDescription: false,
				edit: false
		};
		var modalInstance = $modal.open({
			templateUrl	: Coyo.scripts + '/views/apps/socialLinklist/newLink.html',
			controller	: 'CoyoLinklistFormCtrl',
			scope		: $scope
		});
		
		modalInstance.result.then(function(response) {
			$scope.links.push(response);
		});
	};
	
	$scope.hoverShowOptions = function(link) {
		link.showOptions = true;
	};
	
	$scope.hoverHideOptions = function(link) {
		link.showOptions = false;
	};
	
	$scope.getLinks = function(){
		$scope.loading.busy = true;
		linklistModel.getLinks($scope.appId, function(response) {
			$scope.links = $scope.links.concat(response.linklist);
			$scope.editAllowed = response.usersCanEditLinks;
			$scope.addAllowed = response.usersCanAddLinks;
			$scope.admin = response.admin;
			$scope.loading.busy = false;
		});
	};
	
	$scope.deleteLink = function(index) {
		var link = $scope.links[index];
		linklistModel.deleteLink(link.id, function(){
			$scope.links.splice(index, 1);
		});
	};	
	
	$scope.editLink = function(index) {
		$scope.currData = {
				title :			$scope.links[index].title,
				faviconUrl :	$scope.links[index].faviconUrl,
				id :			$scope.links[index].id,
				linkUrl :		$scope.links[index].linkUrl,
				description :	$scope.links[index].description,
				showOptions :	$scope.links[index].showOptions,
				index :			index
		};
		$scope.currData.edit = true;

		var modalInstance = $modal.open({
			templateUrl	: Coyo.scripts + '/views/apps/socialLinklist/newLink.html',
			controller	: 'CoyoLinklistFormCtrl',
			scope		: $scope
			});

		modalInstance.result.then(function (response) {
			$scope.loading.busy = true;
			linklistModel.getLinks($scope.appId, function(response) {
				$scope.links = response.linklist;
				$scope.loading.busy = false;
			});
		});
	};

	
	$scope.sortableOptions = {
		cancel : ".unsortable",
		items : "li:not(.unsortable)",
		stop : function(e, ui) {
			$scope.syncingOrder = true;
			$scope.errorSyncOrder = false;
			sendLinkOrder();
		},
		helper: function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		}
	};
		
	function sendLinkOrder() {
		linklistModel.order($scope.appId, $scope.links, function() {
			$scope.syncingOrder = false;
		}, function(data, status) {
			$scope.syncingOrder = false;
			$scope.errorSyncOrder = true;
		});
	}
	
}]);

module.controller('CoyoLinklistFormCtrl', ['$scope', '$modalInstance', '$window', 'linklistModel', function($scope, $modalInstance, $window, linklistModel){
	$scope.fileField = {id : "link", required: false};
	$scope.link = {};
	$scope.loading.busy = false;
	$scope.inputUrlInvalid = false;
	var loadMeta = false;

	$scope.url_regexp = new RegExp("(^[\/][\S]*)|([a-zA-Z0-9][\S]*[a-zA-Z0-9]+[\.][a-zA-Z0-9]{2,}[\S]*)", "i"); // jshint ignore:line
	
	$scope.enter = function(event, link, valid) {
		if (valid && event.keyCode === 13) {
			if (link.title !== '' && $scope.linkUrl === link.linkUrl) {
				$scope.save(link);
			} else {
				$scope.getMetaOfUrl(link);
				$scope.linkUrl = link.linkUrl;
			}
		}
	};
		
	$scope.getMetaOfUrl = function(link) {
		$scope.inputUrlInvalid = false;
		if (link.linkUrl !== "") {
			$scope.loading.busy = true;
			linklistModel.getMetaOfUrl(link, function(response) {
				if (!response.error) {
					$scope.currData = response;
					$scope.newLink = response;
					$scope.currData.enterTitelAndDescription = true;
				} else {
					$scope.inputUrlInvalid = true;
				}
				$scope.loading.busy = false;
				var element = $window.document.getElementById('title');
				if (element) {
					element.focus();
				}
			});
		}
	};
	
	$scope.save = function(link) {
		$scope.inputUrlInvalid = false;
		linklistModel.saveLink($scope.appId, link, function(response) {
			$scope.links[link.index] = link;
			if (!response.error) {
				$modalInstance.close(response); 
			} else {
				$scope.inputUrlInvalid = true;
			}
		});
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
}]);
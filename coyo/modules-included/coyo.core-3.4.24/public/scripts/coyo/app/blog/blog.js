angular.module('coyo.app.blog', ['coyo.app.blog.controllers', 'coyo.app.blog.directives', 'coyo.app.blog.services', 'ngRoute'])
	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

		$routeProvider
			.when('/blog/:appId/list', {
				templateUrl: Coyo.scripts + '/views/coyo/app/blog/list.html',
				controller: 'BlogListController'
			}).when('/blog/:appId/article/create', {
				templateUrl: Coyo.scripts + '/views/coyo/app/blog/form.html',
				controller: 'BlogArticleFormController'
			}).when('/blog/:appId/article/edit/:articleId', {
				templateUrl: Coyo.scripts + '/views/coyo/app/blog/form.html',
				controller: 'BlogArticleFormController'
			}).when('/blog/:appId/article/:articleId', {
				templateUrl: Coyo.scripts + '/views/coyo/app/blog/article.html',
				controller: 'BlogArticleController'
			}).when('/blog/:appId/article/:articleId/:anchorId', {
				templateUrl: Coyo.scripts + '/views/coyo/app/blog/article.html',
				controller: 'BlogArticleController'
			});
		
	}
]);
var module = angular.module('coyo.app.blog.services', []);

module.factory('BlogModel', ['$http', '$q', function($http, $q) {
	var BlogModel = {
		info : function(appId) {
			var deferred = $q.defer(),
				url = ROUTES.Blog_info.url({ 'id' : appId });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},

		articles : function(appId, year, month) {
			var deferred = $q.defer(),
				url = ROUTES.Blog_articles.url({ 'id' : appId, 'year' : year, 'month' : month });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},

		articlesPaged: function (appId, firstResult, maxResults, year, month) {
			var url = '/api/app/blog/' + appId + '/articlesPaged/' + firstResult + '/' +
				maxResults + '/' + year + '/' + month;
			return $http.get(url);
		},

		article : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Blog_article.url({ 'id' : id });
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},

		saveArticle : function(appId, article) {
			var deferred = $q.defer(),
				url = ROUTES.Blog_save.url({ 'id' : appId });
			
			// the author is handled via backend and we shouldn't transfer it
			delete article.author;
			
			$http.post(url, article).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			
			return deferred.promise;
		},

		deleteArticle : function(id) {
			var deferred = $q.defer(),
				url = ROUTES.Blog_delete.url({ 'id' : id });
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
	
	return BlogModel;
}]);
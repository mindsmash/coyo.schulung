var module = angular.module('coyo.app.blog.controllers', []);

/**
 * Parent controller which is called by the Play! template
 */
module.controller('BlogAppController', ['$scope', 'BlogModel', '$location', function ($scope, BlogModel, $location) {
	var getQueryParam = function (paramName) {
		var regex = new RegExp("[\\?&]" + paramName + "=([^&#]*)"), results = regex.exec(location.search);
		if (results !== null) {
			return decodeURIComponent(results[1].replace(/\+/g, " "));
		}
		return null;
	};

	$scope.templateBasePath = Coyo.scripts;
	$scope.currentState = 'parent';

	$scope.init = function (appId) {
		$scope.appId = appId;

		if ($location.path().indexOf('/blog') !== 0) {
			var articleId = getQueryParam('articleId');
			var url = (articleId) ? "/blog/" + appId + "/article/" + articleId : '/blog/' + $scope.appId + '/list';
			$location.path(url);
		}
	};
}]);

/**
 * Blog overview
 */
module.controller('BlogListController', ['$scope', 'BlogModel', '$location', '$routeParams', function ($scope, BlogModel, $location, $routeParams) {
	$scope.$parent.currentState = 'list';
	$scope.articles = [];
	$scope.count = 0;
	$scope.currentMonth = {};
	$scope.currentMonth.year = null;
	$scope.currentMonth.month = null;

	$scope.pagination = {
		page: 1,
		pageSize: 10,
		firstResult: function () {
			return ($scope.pagination.page - 1) * $scope.pagination.pageSize;
		},
		maxResults: function () {
			return $scope.pagination.pageSize;
		}
	};

	BlogModel.info($routeParams.appId).then(function (data) {
		$scope.info = data;

		$scope.info.articleCount.sort(function (o1, o2) {
			if (_.has(o1, 'year') && _.has(o2, 'year')) {
				return o1.year < o2.year;
			}
			return false;
		});

		$scope.latest();
	});

	$scope.$watch('pagination', _.throttle(function () {
		$scope.fetchArticles();
	}, 150, {leading: false}), true);



	$scope.initParams = function(monthObject) {
		$scope.currentMonth = monthObject;		

		if ($scope.pagination.page == 1) {
			$scope.fetchArticles();
		}else{
			$scope.pagination.page = 1;
		}
	};

	$scope.loadMonth = function (monthObject) {
		$scope.initParams(monthObject);
	};

	$scope.latest = function() {
		$scope.initParams();
	};

	$scope.fetchArticles = function () {
		var year = null;
		var month = null;

		if ($scope.currentMonth) {
			year = $scope.currentMonth.year;
			month = $scope.currentMonth.month;
		}

		BlogModel.articlesPaged($routeParams.appId, $scope.pagination.firstResult(), $scope.pagination.maxResults(),
			year, month).then(function (response) {

			$scope.articles = response.data.articles;
			$scope.count = response.data.count;
		});
	};
}]);

/**
 * Blog article details
 */
module.controller('BlogArticleController', ['$scope', 'BlogModel', '$location', '$routeParams', '$anchorScroll', '$window', '$timeout', function ($scope, BlogModel, $location, $routeParams, $anchorScroll, $window, $timeout) {
	$scope.$parent.currentState = 'article';
	$scope.$parent.articleId = $routeParams.articleId;


	$scope.goToAnchor = function (anchor) {
		var anchorId = (anchor) ? anchor : $routeParams.anchorId;

		if (angular.isUndefined(anchorId) || !anchorId) {
			return;
		}

		var destination = $location.url();
		var anchorStart = destination.indexOf('?anchorId=');

		if (anchorStart > 0) {
			destination = destination.replace(destination.substring(anchorStart, destination.length), '?anchorId=' + anchorId);
		} else {
			destination = destination + '?anchorId=' + anchorId;
		}

		// change url in broswer, but don´t redirect or trigger angular to redirect
		$location.url(destination);
		$location.replace();
		$window.history.pushState(null, 'any', $location.absUrl());
		// racing condition with the angular rendering of the article
		$timeout(
			function () {
				$('html,body').animate(
					{scrollTop: $("a[name='" + anchorId + "']").offset().top - 40}
					, 'slow'
				)
			}
			, 300);
	}


	BlogModel.article($routeParams.articleId).then(function (data) {
		var idStr = '<a href="#';
		var changed = false;

		// Parse the HTML and replace the CKEditor anchors with JQuery-Anchors
		// Reason: AngularJS-Routing wants to interpret the CKEditors
		while (data.outputText.indexOf(idStr) > 0) {
			var hrefStart = data.outputText.indexOf(idStr) + idStr.length - 1;
			var strToCut = data.outputText.substr(hrefStart,
				data.outputText.indexOf('"', hrefStart) - hrefStart);
			var fullHrefStmt = data.outputText.substring(data.outputText.indexOf(idStr),
				data.outputText.indexOf('</a>', data.outputText.indexOf(idStr)) + '</a>'.length);
			var tagContent = fullHrefStmt.substr(fullHrefStmt.indexOf(">") + 1, fullHrefStmt.length - fullHrefStmt.indexOf(">") - 5);
			var replacementStr = '<a onclick="goToAnchor(\'' + strToCut.substr(1, strToCut.length) + '\')">' + tagContent + '</a>';

			changed = true;
			data.outputText = data.outputText.replace(fullHrefStmt, replacementStr);
		}

		if (changed) {
			data.outputText = data.outputText.concat(
				"<script type='text/javascript'>" +
				"function goToAnchor(anchorId) { " +
				" var appElement = document.querySelector('#blogArticleController'); " +
				" var $scope = angular.element(appElement).scope(); " +
				" $scope.goToAnchor(anchorId); " +
				"}; goToAnchor();" +
				"</script>"
			);
		}

		$scope.article = data;
	});
}]);

/**
 * Blog article form
 *
 * Please note that handling the publish date is quite difficult because of JavaScript's native timezone support.
 * We always want to user to work with the timezone that he configured in his profile settings instead of the
 * timezone of his computer. So when we receive the publish date from the server, we use the user's configured backend
 * timezone using momentjs.
 */
module.controller('BlogArticleFormController', ['$scope', 'BlogModel', '$location', '$routeParams', '$timeout', function ($scope, BlogModel, $location, $routeParams, $timeout) {
	$scope.$parent.currentState = 'form';
	$scope.publishDate = new Date();

	function pad(num, size) {
		var s = num + "";
		while (s.length < size) {
			s = "0" + s;
		}
		return s;
	}

	// resets the publish time from the publish date
	function resetPublishTime() {
		$scope.publishTime = pad($scope.article.publishDate.hours(), 2) + ":" + pad($scope.article.publishDate.minutes(), 2);
	}

	// init
	BlogModel.info($routeParams.appId).then(function (data) {
		$scope.info = data;

		function initPublishDateFields() {
			$scope.publishDate = $scope.article.publishDate.toDate();
			resetPublishTime();
		}

		if (!$routeParams.articleId) {
			$scope.article = {};
			$scope.article.publishDate = moment().tz(Coyo.connectedUserTimeZone);
			initPublishDateFields();
		} else {
			BlogModel.article($routeParams.articleId).then(function (data) {
				$scope.article = data;

				if (data.publishDate) {
					$scope.article.publishDate = moment(data.publishDate).tz(Coyo.connectedUserTimeZone);
				} else {
					$scope.article.publishDate = moment().tz(Coyo.connectedUserTimeZone);
				}
				initPublishDateFields();
			});
		}
	});

	// applies selected date
	$scope.updatePublishDate = function (value) {
		if (value instanceof Date) {
			$scope.article.publishDate.year(value.getFullYear());
			$scope.article.publishDate.month(value.getMonth());
			$scope.article.publishDate.date(value.getDate());
		}
	};

	// applies selected time
	$scope.updatePublishTime = function (value) {
		var regex = /([01]\d|2[0-3]):([0-5]\d)/;

		if (regex.test(value)) {
			$scope.article.publishDate.hours(value.substring(0, 2));
			$scope.article.publishDate.minutes(value.substring(3, 5));
		} else {
			resetPublishTime();
		}
	};

	$scope.getUserTimeZone = function () {
		return Coyo.connectedUserTimeZone;
	};

	/*
	 Checks if the article would get unpublished. Which means he is already published and the new publish date lies in
	 the future.
	 */
	$scope.couldUnpublishArticle = function (article) {
		return article.isPublished && $scope.article.publishDate.valueOf() > moment().valueOf();
	};

	$scope.getTeaserImageUploadOptions = function (article) {
		return {
			name: 'article-teaserimage',
			dropId: 'article-teaserimage-dropzone',
			browseId: 'article-teaserimage-trigger',
			url: '/uploads',
			multiple: false
		};
	};

	// watch for completed uploads
	$scope.$on('upload-api:complete', function (e, name, files) {
		if (name === 'article-teaserimage' && files.length > 0 && files[0]) {
			var latestUpload = files.pop();
			$scope.article.newTeaserImageUpload = latestUpload;
			$scope.article.teaserImageUrl = ROUTES.Blog_teaserImageUpload.url({id: latestUpload});
		}
	});

	$scope.deleteTeaserImage = function () {
		$scope.article.newTeaserImageUpload = 'delete';
		$scope.article.teaserImageUrl = null;
		$scope.article.teaserImage = null;
	};

	$scope.save = function (article) {
		// wait for CKEditor
		$timeout(function () {
			BlogModel.saveArticle($scope.appId, article).then(function (data) {
				$location.path('/blog/' + $scope.appId + '/article/' + data.id);
			}, function (error) {
				// TODO: use FlashService when available
				console.log('Error saving blog article', error);
			});
		}, 500);
	};

	$scope.remove = function (article) {
		BlogModel.deleteArticle(article.id).then(function (data) {
			$location.path('/blog/' + $scope.appId + '/list');
		}, function (error) {
			// TODO: use FlashService when available
			console.log('Error deleting blog article', error);
		});
	};
}]);

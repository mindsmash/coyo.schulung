var app = angular.module('coyo.localfilesapp.controller', []);

app.controller('LatestFilesController', ['$scope', '$http', function ($scope, $http) {
	$scope.files = [];
	$http({
		cache: true,
		url: '/app/files/latest',
		method: 'GET',
		responseType: 'json'
	}).success(function (files) {
		$scope.files = files;
	});
}]);

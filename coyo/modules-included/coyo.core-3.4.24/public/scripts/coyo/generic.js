// used to send and receive angulars EventService events outside of angular context
$eventService = jQuery({});

(function() {
	/**
	 * The coyo.generic module.
	 */
	var module = angular.module('coyo.generic',	[
        'coyo.generic.controllers',
        'coyo.generic.services',
        'coyo.generic.directives',
        'coyo.generic.filters',
        'coyo.generic.longpolling',
        'coyo.generic.tpls',
        'coyo.messaging.chat',
        'coyo.online',
        'coyo.search',
        'coyo.hashtags',
        'coyo.teaser',
        'coyo.pluginhooks.directives',
        'coyo.bookmarks',
        'coyo.media',
        'coyo.calls',
        'coyo.comments',
        'coyo.likes',
        'coyo.share',
        'coyo.account',
        'coyo.event',
        'coyo.webrtc',
        'coyo.workspace',
        'coyo.app.widgets',
        'coyo.app.blog',
        'placeholderShim'
    ]);

	module.config(['$locationProvider', '$logProvider', function($locationProvider, $logProvider) {
		$locationProvider.html5Mode(false).hashPrefix('!');
		
		/* set the debug level according to mode set in Play! */
		$logProvider.debugEnabled(Coyo.isDevMode);
	}]);

	// TODO: Remove when events not consumed from outside of angular anymore!
	module.run(['EventService', function(EventService) {

		EventService.subscribe('*', function (e) {
			$eventService.trigger(e.type, e.object);
		});
	}]);
	
})();
(function () {
	var module = angular.module('coyo.hashtags', []);

	module.controller('HashtagController', ['$scope', '$http', 'HashtagService', function ($scope, $http, HashtagService) {
		HashtagService.getList(23, function (hashtags) {
			$scope.hashTagList = HashtagService.calculateWeight(hashtags);
		});

	}]);

})();
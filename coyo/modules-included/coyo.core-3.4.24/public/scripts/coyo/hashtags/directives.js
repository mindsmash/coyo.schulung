/* 
 * Convert hashtag strings to html links 
 * TODO Is this actually being used?
 */
module.directive('hashify', ['HashtagService', function (HashtagService) {
	return {
		restrict: 'A',
		replace: true,
		scope: {},
		link: function (scope, element) {
			if (Coyo.hashtags) {
				var html = HashtagService.hashify(element.text());
				element.html(html);
			}
		}
	};
}]);

/* users follow/unfollow hashtags */
module.directive('followUnfollowHashtag', ['HashtagService', function(HashtagService) {
	return {
		templateUrl: Coyo.scripts + '/views/coyo/hashtags/follow.html',
		restrict: 'A',
		replace: true,
		scope: {
			tag: "@followUnfollowHashtag"
		},

		link: function(scope, element, attrs) {
			if (!HashtagService.isHashtag(scope.tag)) {
				scope.isHashtag = false;
				return;
			}
			
			scope.isHashtag = true;
			scope.tagName = HashtagService.removeLeadingHash(scope.tag);
			scope.followText = message('hashtag.follow', scope.tag);
			scope.unfollowText = message('hashtag.unfollow', scope.tag);
			
			HashtagService.getUserHashtags(function(hashtags) {
				scope.followsHashTag = _.contains(hashtags, scope.tagName);
			});

			scope.follow = function() {
				HashtagService.followHashtag(scope.tagName, function(newList) {
					scope.followsHashTag = _.contains(newList, scope.tagName);
				});
			};

			scope.unfollow = function() {
				HashtagService.unFollowHashtag(scope.tagName, function(newList) {
					scope.followsHashTag = _.contains(newList, scope.tagName);
				});
			};
		}
	};
}]);
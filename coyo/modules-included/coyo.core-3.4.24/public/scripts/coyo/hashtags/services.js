module.service('HashtagService', ['$http', '$sanitize', function ($http, $sanitize) {
		var hashtagRegex = new RegExp(Coyo.hashtagPattern, "ig");

		/* hashifies text */
		var _hashify = function (text) {
			return text.replace(hashtagRegex, function (match, $1, $2, offset, original) {
				var hashTagSearchURL = ROUTES.HashTag_search.url({searchString: "%23" + $2});
				return $1 + '<a href="' + hashTagSearchURL + '">#' + $2 + '</a>';
			});
		};

		/* fetch the hashtaglist from server */
		var _getHashtagList = function (max, onSuccess, filtered) {
			filtered = (angular.isDefined(filtered)) ? filtered : true;
			max = (angular.isDefined(max)) ? max : 0;

			$http({
				url: ROUTES.HashTag_hashtags.url(),
				method: 'GET',
				params: {max : max, filtered : filtered},
				responseType: 'json'
			}).success(function (response) {
				var hashtags = [];

				_.each(response, function (value, key, list) {
					hashtags.push({
						text: key,
						count: value,
						searchURL: ROUTES.HashTag_search.url({searchString: "%23" + key})
					});
				});

				onSuccess(hashtags);
			});
		};

		/* removes tag from users hashtags */
		var _unFollowHashTag = function (hashtag, onSuccess) {
			if (isValid(hashtag)) {
				$http.post(ROUTES.HashTag_unfollow.url({hashtag : hashtag}))
					.success(function (response) {
						onSuccess(response);
					}
				);
			}
		};

		/* adds tag to users hashtags */
		var _followHashTag = function (hashtag, onSuccess) {
			if (isValid(hashtag)) {
				$http.post(ROUTES.HashTag_follow.url({hashtag : hashtag}))
					.success(function (response) {
						onSuccess(response);
					}
				);
			}
		};

		/* gets user hashtags */
		var _getUserHashtags = function (onSuccess) {
			$http({
				url: ROUTES.HashTag_userHashtags.url(),
				method: 'GET',
				responseType: 'json'
			}).success(function (response) {
				onSuccess(response);
			});
		};

		/* returns blacklisted hashtags */
		var _getBlacklist = function (onSuccess) {
			$http({
				url: ROUTES.HashTag_blacklist.url(),
				method: 'GET',
				responseType: 'json'
			}).success(function (response) {
				onSuccess(response);
			});
		};

		/* adds tag to users hashtags */
		var _addToBlackList = function (hashtag, onSuccess) {
			if (isValid(hashtag)) {
				$http.post(ROUTES.HashTag_addToBlacklist.url(), {hashtag: hashtag})
					.success(function (response) {
						onSuccess(response);
					}
				);
			}
		};

		/* remove tag from blacklist */
		var _removeFromBlackList = function (hashtag, onSuccess) {
			if (isValid(hashtag)) {
				$http.post(ROUTES.HashTag_removeFromBlacklist.url(), {hashtag: hashtag})
					.success(function (response) {
						onSuccess(response);
					}
				);
			}
		};
		
		/* 
		 * Remove the leading hash from hashtag. This is needed because we store hashtags without '#' in the db.
		 * Returns the name of the hashtag without '#'
		 */
		var _removeLeadingHash = function(hashtag) {
			return hashtag.replace(hashtagRegex, function (match, $1, $2, offset, original) {
				return $2;
			});
		};
		
		/* Checks whether passed string is a hashtag using regular expressions */
		var _isHashtag = function(hashtag) {
			return hashtagRegex.test(hashtag);
		};

		/* sort and calculate the weight of every tag. return the new list. */
		var _calculateWeight = function (hashtags) {
			// sort tags by count
			hashtags = _.sortBy(hashtags, function (o) {
				return o.count;
			});

			hashtags.reverse();

			var classCnt = 10,
				tagCount = hashtags.length;

			// assign css classes
			_.each(hashtags, function (o) {
				o.className = "s" + Math.round(classCnt);
				classCnt = classCnt - classCnt / tagCount;
			});

			return hashtags;
		};
		
		/* 
		 * Checks whether a hashtag parameter is valid or not. A hashtag parameter
		 * is valid if it is not 'undefined' and not 'null'. 
		 */
		var isValid = function(hashtag) {
			return (!_.isUndefined(hashtag) && !_.isNull(hashtag));
		};

		return {
			getList: _getHashtagList,
			calculateWeight: _calculateWeight,
			hashify: _hashify,
			isHashtag : _isHashtag,
			getUserHashtags: _getUserHashtags,
			followHashtag: _followHashTag,
			unFollowHashtag: _unFollowHashTag,
			getBlacklist: _getBlacklist,
			addToBlackList: _addToBlackList,
			removeFromBlackList: _removeFromBlackList,
			removeLeadingHash : _removeLeadingHash
		};
	}]);
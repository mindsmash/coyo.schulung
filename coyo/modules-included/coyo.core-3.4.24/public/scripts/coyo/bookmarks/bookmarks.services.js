var module = angular.module('coyo.bookmarks.services', []);

/*
 * Service to load bookmarks.
 */
module.factory('BookmarksModel', ['$http', '$q', function($http, $q) {
	var BookmarksModel = {
		getBookmarks : function() {
			var deferred = $q.defer(),
				url = ROUTES.User_bookmarks.url();
			
			$http.get(url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		},
		addBookmark : function(bookmark) {
			var deferred = $q.defer(),
				url = ROUTES.User_addBookmark.url(),
				payload = {label : bookmark.label, url : bookmark.url};
			
			$http.post(url, payload).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			
			return deferred.promise;
		},
		deleteBookmark : function(bookmark) {
			var deferred = $q.defer(),
				url = ROUTES.User_deleteBookmark.url({id: bookmark.id});
			
			$http['delete'](url).success(function(data, status, headers, config) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
	
	return BookmarksModel;
}]);
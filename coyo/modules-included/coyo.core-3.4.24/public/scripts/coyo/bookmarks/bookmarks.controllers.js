var module = angular.module('coyo.bookmarks.controllers', []);

module.controller('BookmarksController', ['$scope', 'BookmarksModel', function($scope, BookmarksModel) {
	$scope.loading = true;
	$scope.editMode = false;
	$scope.newBookmark = {};
	$scope.bookmarks = [];
	
	// get all bookmarks initially
	BookmarksModel.getBookmarks().then(function(bookmarks) {
		$scope.loading = false;
		$scope.bookmarks = bookmarks;
	});
	
	// toggle to edit mode or back
	$scope.toggleEdit = function() {
		$scope.editMode = !$scope.editMode;
	};
	
	// delete a bookmark
	$scope.deleteBookmark = function(index) {
		var bookmark = $scope.bookmarks[index];
		BookmarksModel.deleteBookmark(bookmark).then(function() {
			$scope.bookmarks.splice(index, 1);
		});
	};
	
	// add a new bookmark
	$scope.addBookmark = function() {
		if ($scope.bookmarkform.$invalid) {
			return;
		}
		BookmarksModel.addBookmark($scope.newBookmark).then(function(bookmarks) {
			$scope.bookmarks = bookmarks;
			$scope.newBookmark = {};
		});
	};
	
}]);
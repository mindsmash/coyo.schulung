/*jshint -W055 */

/**
 * Module for displaying, creating and editing bookmarks in sidebar.
 */
angular.module('coyo.bookmarks', ['coyo.bookmarks.controllers', 'coyo.bookmarks.services']);
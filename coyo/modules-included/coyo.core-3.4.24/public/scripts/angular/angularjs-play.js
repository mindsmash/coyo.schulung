angular.module('play', ['rails'])

.config(['RailsResourceProvider', 'railsSerializerProvider', function (RailsResourceProvider, railsSerializerProvider) {
	RailsResourceProvider.rootWrapping(false);
	railsSerializerProvider.underscore(function(v) { return v; });
	railsSerializerProvider.camelize(function(v) { return v; });
}])

.factory('EntityCollection', ['railsResourceFactory', function(railsResourceFactory) {
	function EntityCollection(Resource) {
		this.$resource = Resource;
		this.queryParams = {};
		this.cache = null;
	}
	
	EntityCollection.prototype.page = function(page) {
		if (angular.isDefined(page) && angular.isNumber(page) && !isNaN(page)) {
			this.queryParams._page = page;
			this.cache = null;
		}
		return this;
	}
	
	EntityCollection.prototype.pageSize = function(pageSize) {
		if (angular.isDefined(pageSize) && angular.isNumber(pageSize) && !isNaN(pageSize)) {
			this.queryParams._pageSize = pageSize;
			this.cache = null;
		}
		return this;
	}
	
	EntityCollection.prototype.sort = function(sorting) {
		if (angular.isObject(sorting) && angular.isDefined(sorting.column)) {
			this.queryParams._orderBy = sorting.column;
			this.cache = null;
		}
		if (angular.isObject(sorting) && angular.isDefined(sorting.direction)) {
			if (sorting.direction == "DESC") {
				this.queryParams._orderBy = "!".concat(this.queryParams._orderBy); 
			}
			this.cache = null;
		}
		return this;
	}
	
	EntityCollection.prototype.params = function(params) {
		if (angular.isObject(params)) {
			for (p in params) {
				if(!angular.isDefined(params[p])) {
					delete params[p];
				}
			}
			angular.extend(this.queryParams, params);
			this.cache = null;
		}
		return this;
	}
	
	EntityCollection.prototype.fetch = function() {
		var response = (this.cache === null) ? this.$resource.query(this.queryParams) : this.cache;
		this.cache = response;
		return this.cache;
	}
	
	return EntityCollection;
}])

.factory('metaDataInterceptor', ['$q', function($q) {
	var ResourcePromise = function(promise) {
		this.promise = promise;
		if(angular.isDefined(promise.response)) {
			this.response = promise.response;
		}
		if(angular.isDefined(promise.context)) {
			this.context = promise.context;
		}
	};
	
	angular.extend(ResourcePromise.prototype, {
		'then' : function(successCallback, errorCallback, notifyCallback, metaCallback) {
			return new ResourcePromise(this.promise['then'](function(data) {
				if(data && angular.isFunction(data.headers)) {
					this.$$headers = data.headers();
				}
				if(angular.isDefined(metaCallback && angular.isDefined(this.$$headers))) {
					metaCallback(this.$$headers);
				}
				return angular.isFunction(successCallback) ? successCallback(data) : data;
			}, errorCallback, notifyCallback))
		},
		'catch' : function(callback) {
			return new ResourcePromise(this.promise['catch'](callback));
		},
		'finally' : function(callback) {
			return new ResourcePromise(this.promise['finally'](callback));
		},
		'meta' : function(metaCallback) {
			return this.then(null, null, null, metaCallback);
		}
	});
	
	return function (promise) {
		return new ResourcePromise(promise);
	}
}])

.factory('playResourceFactory', ['railsResourceFactory', 'EntityCollection', function(
		railsResourceFactory, EntityCollection) {
	
	return function (config) {
		angular.extend(config, {
			responseInterceptors: ['metaDataInterceptor']
		});
		var Resource = railsResourceFactory(config);
		
		angular.extend(Resource.prototype, {
			get : function(customUrl) {
				return Resource.$get(this.$url(customUrl));
			}
		});
		
		angular.extend(Resource, {
			collect : function() {
				return new EntityCollection(Resource);
			}
		});
		
		return Resource;
	}
}])

.factory('playSerializer', ['railsSerializer', function(railsSerializer) {
	return railsSerializer;
}]);

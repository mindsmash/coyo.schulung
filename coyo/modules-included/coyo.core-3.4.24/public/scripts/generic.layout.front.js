$(document).ready(function() {
	// init body
	initLayoutTools($('body'));
});

function initLayoutTools($root) {
	$root.find('form.disableSubmit').submit(function() {
		$(this).find('input[type="submit"]').prop('disabled', true);
	});
}
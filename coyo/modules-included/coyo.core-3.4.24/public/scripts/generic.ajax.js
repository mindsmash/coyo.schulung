$(document).ready(function() {
	// ajax links
	$('body').delegate('a.ajax', 'click', function(event) {
		event.preventDefault();
		
		var $el = $(event.currentTarget);
		launchModal($el.attr('href'), $el.data('ajax-class'));
	});
});

function launchModal(url, ajaxClass) {
	// close any active modal
	if($('#modal').size() > 0) {
		$('#modal').bind('hidden', function() {
			launchModal(url, ajaxClass);
		});
		$('#modal').modal('hide');
		return;
	}
	
	// create modal
	$('body').append('<div id="modal" class="modal fade hide" tabindex="-1"></div>');
	$modal = $('#modal');
	
	// check for inline css class definition, e.g. modal size
	if(typeof ajaxClass !== 'undefined') {
		$modal.addClass(ajaxClass);
	}
	
	// loading status
	$('body').addClass('loading');
	
	// load and launch
	$modal.load(url, function(responseText, textStatus) {
		$('body').removeClass('loading');
		
		function showError(responseText) {
			$modal.html('');
			$modal.append('<div class="modal-header"><a href="#" class="close" data-dismiss="modal">×</a><h4>Error</h4></div>');
			if (responseText) {
				$modal.append('<div class="modal-body"><p>' + responseText + '</p></div>');
			} else {
				$modal.append('<div class="modal-body"><p>The requested site could not be loaded. Please try again later.</p></div>');
			}

		}
		
		if(textStatus == 'error') {
			showError();
		} else {
			initLayoutTools($modal);
		}
		
		$modal.modal({
			show: true,
			backdrop: true,
			keyboard: true
		});
		
		$modal.bind('hidden', function() {
			$('body').trigger('modalHidden');
			$modal.html('');
			$modal.remove();
		});
		
		// load chosen
		$modal.find('select.chosen').chosen();
		
		// load ajax form
		$modal.find('form.ajaxForm').ajaxForm({
			error: function(e){
				$('body').trigger('modalSubmitError', e);
				showError(e.responseText);
			},
			success: function(response, status, xhr, form){
				$('body').trigger('modalSubmitSuccess', response, status, xhr, form);
				$('#modal').modal('hide');
			}
		});
	});
}
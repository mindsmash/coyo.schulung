//jQuery plugin
(function($) {
	var methods = {
		init : function(settings) {
			var $this = $(this);
			
			$this.data('settings', settings);
			
			// if filter, setup
			if (settings.filter) {
				var $filter = $(settings.filter);
				function filter(e) {
					if (e) e.preventDefault();
					$this.endlessAjax('load', 1, true);
				}
				
				$filter.on('submit', filter);
				$filter.on('change', 'select', filter);
				
				var throttle = null;
				$filter.on('keyup', 'input', function(e) {
					if (throttle) {
						clearTimeout(throttle);
						throttle = null;
					}
					throttle = setTimeout(function() {
						filter();
					}, 300);
				});
			}
			
			// trigger more on scroll
			if(settings.scrollContainer) {
				$scrollContainer = $(settings.scrollContainer);

				$scrollContainer.scroll(function() {
					if ($scrollContainer.scrollTop() >= $scrollContainer[0].scrollHeight - $scrollContainer.height() - 200) {
						$this.endlessAjax('extend');
					}
				});
			} else {
				$(window).scroll(function() {
					if ($(window).scrollTop() >= $(document).height() - $(window).height() - 200) {
						$this.endlessAjax('extend');
					}
				});
			}

			// trigger once on load
			$this.endlessAjax('load', 1);
		},
		load: function(page, flush) {
			var $this = $(this);
			
			if(!$this.hasClass('loading') && (flush || !$this.hasClass('all-loaded'))) {
				settings = $this.data('settings'),
				params = {};
				
				if (flush) {
					$this.removeClass('all-loaded');
					$this.empty();
				}
				
				$this.addClass('loading');
				
				params['page'] = page;
				params['length'] = settings.length;
				
				if (settings.filter) {
					var $filter = $(settings.filter);
					$filter.find('[name!=""]').each(function() {
						if ($(this).attr('name') && $(this).val() != $(this).attr('placeholder')) {
							params[$(this).attr('name')] = $(this).val();
						}
					});
				}
				
				$.ajax({
					url: settings.url,
					data: params,
					success: function(html) {
						var $items = $(html);

						$this.removeClass('loading');
						
						if($items.size() == 0) {
							$this.addClass('all-loaded');
							return;
						}
						
						$this.data('currentPage', page);
						$this.append($items);
						initLayoutTools($items);
						
						// trigger event
						$items.trigger('endless-init');
						
						/*
						 * This helps loading partial pages
						 * (frontend-based permission checks)
						 */
						if($items.filter('.endless-load-more').size() > 0 &&
							($items.size() == 1 || $this.children().size() < settings.length)) {
							
							$this.endlessAjax('load', parseInt($this.data('currentPage')) + 1);
						}
						
						/*
						 * This prevents posts from being shown twice. This can happen when a new post is added to top 
						 * and then a new page is queried.
						 */
						var posts = [];
						$.each($('.wall .post'), function(i, val) {
							var href = $(val).attr('data-href') || $(val).attr('href');
							if(!_.isUndefined(posts[href]) && !$(val).hasClass('post-share') && !$(val).is('div')) {
								$(val).remove();
							} else {
								posts[href] = val;
							}
						});
					}
				});
			}
		},
		extend: function() {
			var $this = $(this),
			settings = $this.data('settings');
			$this.endlessAjax('load', parseInt($this.data('currentPage')) + 1);
		}
	};

	$.fn.endlessAjax = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
// jQuery plugin
(function($) {
	var playerClass = 'inline-player';
	
	var methods = {
		init : function() {
			var $playable = $(this);
			
			soundManager.onready(function() {
				$playable.addClass(playerClass);
				// init sound
				soundManager.createSound({
					id: $playable.attr('id'),
					url: $playable.attr('href'),
					autoLoad: false,
					autoPlay: false,
					volume: 75
				});
				
				// delegate click event
				$playable.click(function(e) {
					$playable.inlinePlayer('click', e);
				});
			});
		},
		click: function(e) {
			var $playable = $(this);
			
			e.preventDefault();
			
			// check playing
			if($playable.hasClass('playing')) {
				// pause
				$playable.inlinePlayer('pause');
			} else if($playable.hasClass('paused')) {
				// resume
				$playable.inlinePlayer('start');
			} else {
				// stop any other song
				if($('.' + playerClass + '.playing').size() > 0) {
					$('.' + playerClass + '.playing').inlinePlayer('stop');
				}
				if($('.' + playerClass + '.paused').size() > 0) {
					$('.' + playerClass + '.paused').inlinePlayer('stop');
				}
				
				// start
				$playable.inlinePlayer('start');
			}
		},
		pause: function() {
			var $playable = $(this);
			$playable.removeClass('playing');
			$playable.addClass('paused');
			soundManager.pause($playable.attr('id'));
		},
		stop: function() {
			var $playable = $(this);
			$playable.removeClass('playing');
			$playable.removeClass('paused');
			soundManager.stop($playable.attr('id'));
		},
		start: function() {
			var $playable = $(this);
			$playable.addClass('playing');
			$playable.removeClass('paused');
			soundManager.play($playable.attr('id'));
		}
	};

	$.fn.inlinePlayer = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
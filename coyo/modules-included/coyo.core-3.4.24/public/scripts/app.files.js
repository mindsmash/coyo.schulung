(function() {

	var sortFiles = function(a, b, direction) {
		var $a = $(a),
			$b = $(b);
	
		if (($a.hasClass('folder') && $b.hasClass('folder')) || (!$a.hasClass('folder') && !$b.hasClass('folder'))) {
			if (direction === 'asc') {
				return $a.html().toLowerCase().localeCompare($b.html().toLowerCase());
			}
			return $b.html().toLowerCase().localeCompare($a.html().toLowerCase());
		}
		return ($a.hasClass('folder') ? 0 : 1);
	};


	jQuery.extend(jQuery.fn.dataTableExt.oSort, {
		"filename-asc" : function(a, b) {
			return sortFiles(a, b, 'asc');
		},
		"filename-desc" : function(a, b) {
			return sortFiles(a, b, 'desc');
		},
		"dataAttr-pre" : function(span) {
			return $(span).data('sort-value');
		}
	});
}());

// file browser
$(document).ready(function(){
	var interval;
	var $fileBrowser;
	var supportsActiveX = (Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(window, 'ActiveXObject')) || ('ActiveXObject' in window);
	
	var fileBrowser = {
		init : function(){
			$fileBrowser = $('#fileBrowser');

			// for non-IE, try to find sharepoint plugin and load it
			if(!$.browser.msie && navigator.plugins) {
				(function loadSharePointPlugin(){
					for(var i = 0; i < navigator.plugins.length; i++) {
						for(var m = 0; m < navigator.plugins[i].length; m++) {
							if ('application/x-sharepoint' === navigator.plugins[i][m].type) {
								try {
								$('body').append('<object id="sharePointPlugin" type="application/x-sharepoint" style="width: 0; height: 0; position: absolute; left: -99999px;"></object>');
								} catch(e) {
									alert(e);
								}
								return;
							}
						}
					}
				}());
			}
			
			$('body').on('modalHidden', function(){
				fileBrowser.reload();
			});
			
			// bind ajax links
			$fileBrowser.on('click', '*[data-file-uid]', function(e){
				e.preventDefault();
				fileBrowser.load({ uid : $(this).data('file-uid'), view : $(this).data('view') });
			});
			
			// click on anything outside the popover closes it
			$(document).click(function(e){
				$('.preview-toggle').each(function () {
					if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
						$(this).popover('hide');
					}
				});
			});
			
			fileBrowser.reload();
			
		},
		reload : function(){
			// load initial view
			fileBrowser.load({ uid : $fileBrowser.data('file-uid') });
		},
		load : function(args) {
			// force all tips to hide
			$('.tooltip').remove();

			// loading status
			$fileBrowser.find('.files-nav .btn').addClass('disabled');
			
			// load
			$.get($fileBrowser.data('browse-url'), args, function(data){
				var $data = $(data);
				$fileBrowser.html(data);
				$fileBrowser.data('file-uid', $data.data('current-file-uid'));
				initLayoutTools($fileBrowser, true);
				
				// remove browser back button listener
				clearInterval(interval);
				
				// office links
				var SPPlugin = $('#sharePointPlugin').get(0);
				$fileBrowser.find('a.file-edit-office').click(function(e){
					e.preventDefault();
					
					var url = $(this).attr('href');
					
					try {
						if (supportsActiveX) {
							new ActiveXObject('SharePoint.OpenDocuments.2').EditDocument(url);
							return;
						}
						if (SPPlugin && typeof SPPlugin !== 'undefined' && typeof SPPlugin.EditDocument !== 'undefined') {
							SPPlugin.EditDocument(url);
							return;
						}
					} catch (exception) {
						alert(message('app.files.officeIntegration.error') + ': ' + exception.message);
					}
					
					alert(message('app.files.officeIntegration.error'));
				});
				
				// init previews
				$fileBrowser.find('.preview-toggle').popover({
					trigger: 'click',
					placement: 'bottom',
					html: true,
					content: '<div class="preview-popover-wrapper loading"></div>'
				});
				$fileBrowser.find('.preview-toggle').bind('shown', function(){
					$.get($(this).data('preview-url'), function(data){
						$('.preview-popover-wrapper').html(data).removeClass('loading');
					});
				});
			});
		}
	};
	
	fileBrowser.init();
});
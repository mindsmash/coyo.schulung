// jQuery plugin
(function($) {
	var methods = {
		init : function() {
			var $this = $(this);
			
			$this.css('min-height', $this.parent().height());
			
			// new task form
			var $form = $this.find('form#task-new');
            var $text = $("#task-new-name");

            $text.keyup(function() {
                $this.tasksApp('toggleError');
            });
            $text.on('paste', function() {
                setTimeout(function () {
                    $this.tasksApp('toggleError');
                }, 100);

            });

			$this.find('#task-new-name').keypress(function(e){
				if(e.which === 13) {
					e.preventDefault();
					
					// check empty
					var $name = $this.find('#task-new-name'); 
					if($name.val().trim() === '') {
						$name.focus();
						return false;
					}
					
					// set list id
					$form.find('#task-new-list-id').val($this.find('.tasks-list.active').data('list-id'));
					
					// submit
					$form.ajaxSubmit({
                        beforeSubmit: function(arr, form, options) {
                            return !$this.tasksApp('toggleError');
                        },
						success: function(data, status, xhr, form) {
							// clear form
							$form.clearForm();
							$form.find('#task-new-list-id').val('');
							
							// reload after new task successfully created
							$this.tasksApp('loadTasks', $this.find('.tasks-list.active'));
						}
					});
				}
			});
			
			// list sorting
			$this.find('.tasks-lists').sortable({
				items: '.tasks-list-sortable',
				opacity: 0.75,
				revert: 200,
				update: function(event, ui) {
					var order = '';
					$this.find('.tasks-list-sortable').each(function(i, el) {
						order = order + $(el).data('list-id') + ',';
					});
					$.post($this.find('.tasks-lists').data('sort-url'), {order: order});
				}
			});
			
			// make list droppable to tasks
			$this.find('.tasks-list').droppable({
				accept: '.task',
				hoverClass: 'dragover',
				tolerance: 'pointer',
				drop: function(event, ui) {
					$list = $(this);
					$activeList = $this.find('.tasks-list.active');
					
					if ($list.data('list-id') === $activeList.data('list-id')) {
						return;
					}
					
					$task = $(ui.draggable);
					$task.hide();
					
					$.post($this.data('update-url'), {
						'id': $task.data('task-id'),
						'listId': $list.data('list-id')
					}, function() {
						$task.remove();
					});
					
					// update tasks count
					if (!$task.hasClass('task-completed')) {
						$activeList.find('.tasks-count').html(parseInt($activeList.find('.tasks-count').html())-1);
						$list.find('.tasks-count').html(parseInt($list.find('.tasks-count').html())+1);
					}
				}
			});
			
			// attach click events
			$this.find('.tasks-list a').click(function(e){
				e.preventDefault();
				$this.tasksApp('loadTasks', $(e.currentTarget).parents('.tasks-list'));
			});
			
			// load first list by default
			if($this.find('.tasks-list').length > 0) {
				$this.tasksApp('loadTasks', $this.find('.tasks-list')[0]);
			}
			
			// bind task edit form submit
			$('body').on('submit', '#task-edit-form', function(e){
				e.preventDefault();
				$('#task-edit-form').ajaxSubmit(function() { 
					// reload
					$this.tasksApp('loadTasks', $this.find('.tasks-list.active'));
					
					// close modal
					$('#modal').modal('hide');
				}); 
			});
			
			// bind task delete button
			$('body').on('click', '#task-edit-form-delete', function(e) {
				e.preventDefault();
				
				$.ajax({
					url  : $('#task-edit-form-delete').attr('href'),
					type : 'DELETE',
					data : {authenticityToken : AUTHENTICITY_TOKEN}
				}).success(function() {
					// reload
					$this.tasksApp('loadTasks', $this.find('.tasks-list.active'));
					
					// close modal
					$('#modal').modal('hide');
				});
			});
		},
		loadTasks : function(list) {
			var $this = $(this);
			var $list = $(list);
			var $cont = $this.find('.tasks-container');
			
			// load
			$cont.addClass('loading');
			$.get($list.find('a').attr('href'), function(data){
				$cont.html(data);
				$cont.removeClass('loading');
				$this.find('.tasks-list').removeClass('active');
				$list.addClass('active');

				// update count in tasks list
				$list.find('.tasks-count').html($cont.find('.tasks-uncompleted .task').length);
				
				$this.tasksApp('initTasks');
			});
		},
		sortTasks : function() {
			var $this = $(this);
			var $cont = $this.find('.tasks-container');
			var $activeList = $this.find('.tasks-list.active');

			// update count in task list after check/uncheck events
			$activeList.find('.tasks-count').html($(".tasks .task-uncompleted").length);

			
			$cont.find('.tasks-uncompleted .task').sort(function (a, b) {
				return $(a).data('task-priority') - $(b).data('task-priority');
			}).each(function (_, container) {
				$(container).parent().append(container);
			});
		},
		initTasks : function() {
			var $this = $(this);
			var $cont = $this.find('.tasks-container');
			
			// delete button
			$cont.find('.task .task-delete').click(function(e){
				e.preventDefault();
				$el = $(e.currentTarget);
				$li = $el.parents('li');
				
				$.ajax({
					url  : $el.attr('href'),
					type : 'DELETE',
					data : {authenticityToken : AUTHENTICITY_TOKEN}
				}).success(function() {
					$li.remove();
				});
			});
			
			// check/uncheck events
			$cont.find('.task .task-checkbox').click(function(e){
				e.preventDefault();
				$el = $(e.currentTarget);
				$li = $el.parents('li');
				var completed = !$li.hasClass('task-completed');
				
				$.post($this.data('update-url'), {
					'id': $li.data('task-id'),
					'completed': completed
				});
				
				var $activeList = $this.find('.tasks-list.active');
				var fadeTime = 200;
				if(completed) {
					$li.fadeOut(fadeTime, function(){
						$li.addClass('task-completed');
						$li.removeClass('task-uncompleted');
						$cont.find('.tasks-completed').prepend($li);
						$this.tasksApp('sortTasks');
						$li.fadeIn(fadeTime);
					});
				} else {
					$li.fadeOut(fadeTime, function(){
						$li.removeClass('task-completed');
						$li.addClass('task-uncompleted');
						$this.tasksApp('sortTasks');
						$li.fadeIn(fadeTime);
					});
				}
			});

			// clear done button
			$cont.find('.task-list-cleardone').click(function(e){
				e.preventDefault();
				$.post($(this).attr('href'), function(){
					$cont.find('.tasks-completed .task').remove();
				});
			});
			
			// task sorting
			$this.find('.tasks-uncompleted').sortable({
				items: '.task',
				opacity: 0.75,
				revert: 200,
				update: function(event, ui) {
					var prio = 1;
					var order = '';
					$this.find('.tasks-uncompleted .task').each(function(i, el) {
						var $task = $(el);
						order = order + $task.data('task-id') + ',';
						$task.attr('data-task-priority', prio++);
					});
					$.post($this.data('sort-url'), {order: order});
				},
				stop: function() {
					$this.find('.task').css('opacity', '');
				}
			});
			
			// allow completed tasks to be dragged to other list
			$this.find('.task-completed').draggable({
				revert: true,
				revertDuration: 200
			});
		},

        toggleError: function() {
            var $text = $("#task-new-name");
            var $nameDiv = $("#task-name-control");
            if($text.val().length > 255) {
                $nameDiv.toggleClass('control-group error', true);
                return true;
            } else {
                $nameDiv.toggleClass('control-group error', false);
                return false;
            }
        }
	};

	$.fn.tasksApp = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else {
			return methods.init.apply(this, arguments);
		}
	};
})(jQuery);
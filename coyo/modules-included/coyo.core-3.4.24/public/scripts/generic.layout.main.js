$(document).ready(function() {
	// init body
	initLayoutTools($('body'));
	
	// auto-hide success messages
	$('.alert.alert-success').delay(3000).fadeOut();
	
	// auto-add authenticity token for ajax requests
	$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
	    // only add for POST requests
		if(originalOptions.type == 'POST' || originalOptions.type == 'post') {
	    	var data = originalOptions.data;
	    	if (data !== undefined && $.type(data) === 'string' && data.length > 0) {
	    		if(data.indexOf('authenticityToken') < 0) {
	    			data += '&authenticityToken=' + AUTHENTICITY_TOKEN;
	    		}
	    	} else if(data !== undefined && $.type(data) === 'object') {
	    		data.authenticityToken = AUTHENTICITY_TOKEN;
	    		data = $.param(data);
	    	} else {
	    		data = 'authenticityToken=' + AUTHENTICITY_TOKEN;
	    	}
	    	options.data = data;
		}
	});
});

// can be called for ajax content, too
function initLayoutTools(root, force) {
	var $root = $(root);
	
	if(!force && $root.data('layout-initiated') == 'true') {
		return;
	}
	$root.data('layout-initiated', 'true');
	
	// init chosen
	$root.find('select.chosen').each(function(i, el){
		$(el).chosen({
			allow_single_deselect: ($(el).attr('data-allow-deselect') == 'true')
		});
	});

	// init datepicker
	$root.find('input.date').each(function(i, el) {
		var format = $(el).attr('data-format');
		if(format == null || format == '') {
			format = 'yy-mm-dd';
		}
		
		$(el).datepicker({
			dateFormat: format
		});
	});
	
	// init datetimepicker
	$root.find('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'HH:mm',
		stepMinute: 15
	});
	
	// init timepicker
	$root.find('input.time').timepicker({
		timeFormat: 'HH:mm',
		stepMinute: 15
	});
	
	// init btn loading text
	$root.on('click', '.btn[data-loading-text]', function (e) {
		$el = $(e.currentTarget);
		// $el.button('loading'); // not working at the moment in Bootstrap 2.0
		$el.addClass('disabled');
		
		// setting the disabled attribute in IE prevents the original click
		// action from being performed
		if (!$.browser.msie) {
			$el.attr('disabled', 'disabled');
		}
		
		$el.attr('value', $el.attr('data-loading-text'));
		$el.html($el.attr('data-loading-text'));
	});
	
	// disable submit buttons on click
	$root.find('input[type=submit]:not(.prevent), button[type=submit]:not(.prevent)').live('click', function(e) {
		$(this).addClass('disabled');
	});
	
	// double-click confirm buttons ('Really?')
	$root.find('a[really]').live('click', function(event){
		$el = $(event.target);
		
		if(!$el.hasClass('asked')) {
			event.preventDefault();
			
			$el.html($el.attr('really'));
			$el.addClass('asked');
		}
	});
	$root.find('a[data-really]').live('click', function(event){
		$el = $(event.target);
		
		if(!$el.hasClass('asked')) {
			event.preventDefault();
			
			window.setTimeout("$el.html('"+$el.html()+"'); $el.removeClass('asked');", 3000);
			
			$el.html($el.data('really'));
			$el.addClass('asked');
		}
	});
	
	// tooltip
	$root.find('.tip').tooltip({
		container: 'body'
	});
	
	// popover
	$root.find('.popover-toggle').popover({
		container: 'body',
		content: function(){
			// custom feature that allows a child .popover-content element
			// to be used to host the popover HTML content
			var $this = $(this);
			if($this.find('.popover-content').length > 0) {
				return $this.find('.popover-content').html();
			}
			return $this.data('content');
		}
	});
	
	// custom confirm
	$root.find('a[data-confirm]').live('click', function(event){
		event.preventDefault();
		$el = $(event.target);
		
		// clear existing modal
		$('#modal').html('');
		
		// create modal
		$('body').append('<div id="modal" class="modal fade hide" tabindex="-1"></div>');
		$modal = $('#modal');
		$modal.append('<div class="modal-header"><a href="#" class="close" data-dismiss="modal">×</a><h4>' + message('confirm.modal.title') + '</h4></div>');
		$modal.append('<div class="modal-body"><p>'+$el.attr('data-confirm')+'</p></div>');
		$modal.append('<div class="modal-footer"><a href="#" class="btn no">' + message('no') + '</a><a href="'+$el.attr('href')+'" class="btn btn-primary">' + message('yes') + '</a></div>');
		
		$modal.find('.no').click(function() {
			$modal.modal('hide');
		});
		
		$modal.modal({
			show: true,
			backdrop: true,
			keyboard: true
		});
		
		$modal.bind('hidden', function() {
			$modal.html('');
			$modal.remove();
		});
	});
	
	// sidebar more button
	$root.on('click', '.more', function(e){
		e.preventDefault();
		e.stopPropagation();
		
		$(e.currentTarget).parents('ul').find('li').show();
		$(e.currentTarget).hide();
		
		return false;
	});
	
	// save button
	$root.find('a.form-save').click(function(e){
		e.preventDefault();
		
		var $a = $(e.currentTarget);
		if($a.data('triggered') != 'true') {
			$a.addClass('disabled');
			$a.data('triggered', 'true');
			$a.parents('form').submit();
		}
	});
	
	// highlight tabs with errors
	$root.find('.tab-content .control-group.error').each(function(i, el){
		// find tab element
		$tab = $('ul.nav-tabs a[href="#' + $(el).parents('.tab-pane').attr('id') + '"]');
		if(!$tab.hasClass('error')) {
			$tab.addClass('error');
			$tab.html('<i class="icon-exclamation-sign"></i> ' + $tab.html());
		}
	});
	
	// fuzzy dates
	$root.find('.date-fuzzy').timeago();
	
	// fix bootstrap bug
	$root.on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });

	// truncate texts
	$root.find('.js-text-expander').expander({
		'slicePoint' : 400,
		'preserveWords': true,
		'expandText' : message('post.showMore'),
		'expandPrevix' : '',
		'expandEffect' : 'show',
		'expandSpeed' : 100,
		'userCollapse' : false
	});
}

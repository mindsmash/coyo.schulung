function forms_update_select(formElement, modal){
	var select = formElement.find('select');
	select.find('option').remove();
	
	modal.find('.forms-label-option').each(function(){
		select.append('<option data-id="' + $(this).attr('data-id') + '">' + $(this).val() + '</option>');
	});
}

function forms_addSelectOption(text, before, id){
	text = text.replace(/\"/g, "&quot;");
	$('<div><input type="text" class="forms-label-option" data-id="' + id + '" value="' + text + '"/><span class="icon-remove"></span></div>').insertBefore(before);
	
	$('.multiple-settings .icon-remove').unbind('click');
	$('.multiple-settings .icon-remove').click(function(){
		$(this).parent().remove();
		
		if($('.multiple-settings .icon-remove').length === 1){
			$('.multiple-settings .icon-remove').remove();
		}
	});
}

function forms_resetModal_select(formElement, modal){
	var clearfix = modal.find('.clearfix');
	modal.find('.forms-label-option').parent().remove();
	formElement.find('option:not([disabled])').each(function(){	
		forms_addSelectOption($(this).html(), clearfix, $(this).attr('data-id'));
	});
}

$(document).ready(function(){
	$('.forms-add-option').click(function(e) {
		e.preventDefault();
		forms_addSelectOption('', $(this).parent().find('.clearfix'));
	});
});

function forms_getOptionsData_select(formElement){
	var options = [];
	formElement.find('option:not([disabled])').each(function(i){
		var optionEl = $(this),
			label = optionEl.text(),
			idAttr = optionEl.attr('data-id'),
			optionId = null;
		
		if (typeof idAttr !== 'undefined' && idAttr !== null && idAttr.trim() !== ''){
			optionId = parseInt(idAttr);
		}
		
		options.push({id:optionId, label: label, fieldOrder: i});
	});
	

	
	return options;
}
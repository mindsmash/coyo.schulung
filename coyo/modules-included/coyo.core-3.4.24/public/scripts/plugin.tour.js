$(document).ready(function(){
	var tourIndex = 0;
	
	function nextTourBubble() {
		var data = tourPluginData[tourIndex++];
		var $bubble = $('#tour-bubble');
		$bubble.removeAttr('style').css({
			'bottom':data.bottom,
			'top':data.top,
			'left':data.left,
			'right':data.right
		});
		$bubble.removeClass().addClass('popover').addClass(data.placement);
		$bubble.find('.tour-bubble-body').html(data.html);
		$bubble.find('.popover-title').html(data.title);
		
		if(tourPluginData.length <= tourIndex) {
			$bubble.find('.tour-bubble-button-next').hide();
		}
		
		$bubble.fadeIn(200);
	}
	
	$('.tour-bubble-button-next').click(function(e){
		$('#tour-bubble').fadeOut(200, nextTourBubble);
	});
	$('.tour-bubble-button-close').click(function(e){
		$('#tour-bubble').fadeOut(200);
		$('#tour-overlay').fadeOut(200);
	});
	
	window.setTimeout(function(){
		$('#tour-overlay').css('filter', 'alpha(opacity=50)'); // IE8 hack to display overlay with opacity
		$('#tour-overlay').fadeIn(200);
		nextTourBubble();
	}, 300);
});
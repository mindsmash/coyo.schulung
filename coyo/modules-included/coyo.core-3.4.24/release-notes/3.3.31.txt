
Release Notes - Coyo - Version 3.3.31

** Bug
    * [COYO-4156] - iOS app does not load avatars (in native features)
    * [COYO-4157] - Sending private message in iOS app not working
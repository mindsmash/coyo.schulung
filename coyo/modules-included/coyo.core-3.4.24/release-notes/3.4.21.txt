Release Notes - Coyo3 - Version 3.4.21

** Bug
    * [COYO-4754] - InnoApp: tenant deletion fails due to constrain violation of linked ideas
    * [COYO-4759] - Innoapp: delete button dysfunctional when configured as default app
    * [COYO-4763] - Switching users on saas

** Story
    * [COYO-4609] - An user is logged out automatically when his session expires
    * [COYO-4750] - An user is able to understand what he can do with the OAuth2 device management interface

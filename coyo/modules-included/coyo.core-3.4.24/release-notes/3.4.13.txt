
Release Notes - Coyo3 - Version 3.4.13


** Bug
    * [COYO-4107] - Upcoming events counter seems to include (unviewable) private events
    * [COYO-4113] - EventReminderNotification seems to only get distributed to full-day-events
    * [COYO-4317] - Functional/Laptop/Win8.1/Chrome-Dynamic List: Cannot add long lists
    * [COYO-4334] - User sees wiki articles from closed workspace in search results after leaving workspace
    * [COYO-4446] - [Functional/Laptop/Win7/FF] Changing a link needs a reload of page
    * [COYO-4475] - DocumentApp: dropdown shows empty menu point
    * [COYO-4493] - Chat bubbles are showing users with online status, who are not online.
    * [COYO-4507] - Difference between blog publishing date and blog-app notification 
    * [COYO-4516] - Dynamic List - Cursor jumps to end of line if you type something
    * [COYO-4557] - Dynamic list sorting is wrong for German umlauts
    * [COYO-4598] - Umlauts in dynamic list are not sorted correctly after alphabetical sortation
    * [COYO-4599] - Only 50 blog articles are shown in the blog app
    * [COYO-4600] - Video call cannot be deleted due to foreign key constraint
    * [COYO-4608] - Events in widget upcoming event and calendar are not equal














** Technical Task
    * [COYO-4592] - Performance optimization




** Todo
    * [COYO-4593] - Files-app: Each navigation step is loading the entire file tree
    * [COYO-4594] - Improve PushNotificationJob performance
    * [COYO-4595] - Improve PNSClientWatchConnectionJob performance



package models.page;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.TenantModel;
import models.User;
import play.cache.Cache;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import util.Util;
import acl.Permission;
import controllers.Security;

/**
 * 
 * Represents a list of {@link Page}s, to order them.Ç‚
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "page_category")
public class PageCategory extends TenantModel {

	@Required
	@MaxSize(25)
	@Unique
	@Column(nullable = false)
	public String name;

	public int priority = 9999;

	@OneToMany(mappedBy = "category")
	public List<Page> pages = new ArrayList<Page>();

	/**
	 * 
	 * Updates the priority of the page categories by a String in the format
	 * categoryid,categoryid,...,categoryid.
	 * 
	 * @param orderString
	 */
	public static void storeOrder(String orderString) {
		List<String> order = Util.parseList(orderString, ",");
		int priority = 0;
		for (String id : order) {
			PageCategory category = PageCategory.findById(Long.valueOf(id));
			if (category != null) {
				category.priority = priority++;
				category.save();
			}
		}
	}

	public List<Page> getPages(User user) {
		String cacheKey = getCacheKey(user);
		List<Page> r = Cache.get(cacheKey, List.class);

		if (r == null) {
			r = new ArrayList<Page>();
			for (Page page : pages) {
				if (Security.check(Permission.LIST_PAGE, page.id)) {
					r.add(page);
				}
			}

			Cache.set(cacheKey, r, "5min");
		}
		return r;
	}

	public String getCacheKey(User user) {
		return "pageCategory-" + id + "-pages-" + user.id;
	}

	public static List<PageCategory> findAllSorted() {
		return find("ORDER BY priority ASC").fetch();
	}
}

package models.page;

import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.Sender;
import models.SenderType;
import models.User;
import models.UserGroup;
import models.app.App;
import models.app.ContentApp;
import models.app.WallApp;
import models.dao.PageDao;
import models.event.Event;
import models.helper.ClearManyToMany;
import models.notification.PageNotification;
import models.wall.Wall;
import models.wall.post.Post;
import play.data.binding.NoBinding;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.JavaExtensions;
import plugins.PluginEvents;
import plugins.PluginManager;
import session.UserLoader;
import checks.PageNameCheck;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

/**
 * Represents a page.
 * 
 * @author Jan Marquardt
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Indexed(boost = 4F)
@Table(name = "page")
@SenderType("page")
@Searchable(category = "pages")
@InjectionSupport
public class Page extends Sender implements Comparable<Sender> {

	public static final String MEMBER_GROUP_TABLE = "page_member_group";
	public static final String FORCE_FOLLOWER_GROUP_TABLE = "page_force_group";

	public enum PageVisibility {
		PUBLIC("globe"), PRIVATE("eye-close");

		public String icon;

		private PageVisibility(String icon) {
			this.icon = icon;
		}

		public String getLabel() {
			return Messages.get("page.visibility." + this + ".label");
		}

		public String getDescription() {
			return Messages.get("page.visibility." + this + ".description");
		}

		public boolean hasMembers() {
			return this == PRIVATE;
		}
	}

	@Inject(configuration = "coyo.di.dao.page", defaultClass = PageDao.class, singleton = true)
	private static PageDao pageDao;

	@Required
	@CheckWith(PageNameCheck.class)
	@Field
	@MaxSize(50)
	@Unique
	@Column(nullable = false)
	public String name;

	@ManyToOne
	public PageCategory category;

	// sticky / force follow
	// ------------------------------------------------------------------------------------------------
	@NoBinding
	public boolean forceAllFollow = false;

	@NoBinding
	@ManyToMany
	@JoinTable(name = "page_force_user")
	@ClearManyToMany(appliesFor = User.class)
	public List<User> forcedFollowers = new ArrayList<User>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = FORCE_FOLLOWER_GROUP_TABLE)
	public List<UserGroup> forcedFollowerGroups = new ArrayList<UserGroup>();
	// ------------------------------------------------------------------------------------------------

	@NoBinding
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public User creator;

	@Required
	@Column(nullable = false)
	public PageVisibility visibility = PageVisibility.PUBLIC;

	@NoBinding
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "page")
	public List<PageNotification> notifications = new ArrayList<PageNotification>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = "page_member")
	@ClearManyToMany(appliesFor = User.class)
	public List<User> members = new ArrayList<User>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = MEMBER_GROUP_TABLE)
	public List<UserGroup> memberGroups = new ArrayList<UserGroup>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = "page_admin")
	@Required
	@ClearManyToMany(appliesFor = User.class)
	public List<User> admins = new ArrayList<User>();

	public boolean hasForcedFollowers() {
		return forceAllFollow || !forcedFollowers.isEmpty() || !forcedFollowerGroups.isEmpty();
	}

	public boolean isForcedFollower(User user) {
		return pageDao.getForceFollowUserIds(this).contains(user.id);
	}

	public List<User> getAllMembers() {
		Set<User> r = new HashSet<User>(members);
		r.addAll(admins);
		for (UserGroup group : memberGroups) {
			r.addAll(group.users);
		}
		return new ArrayList<User>(r);
	}

	public boolean isMember(User user) {
		return getAllMembers().contains(user);
	}

	public boolean isAdmin(User user) {
		return admins.contains(user);
	}

	@Override
	public Wall getDefaultWall() {
		for (App app : apps) {
			if (app instanceof WallApp) {
				return ((WallApp) app).wall;
			}
		}
		return null;
	}

	public void initDefaultApps() {
		ContentApp app = new ContentApp();
		app.sender = this;
		app.title = app.getAppDescriptor().getName();
		apps.add(app);
		save();
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public boolean isSharable() {
		return visibility != PageVisibility.PRIVATE;
	}

	@Override
	public List<User> getAdmins() {
		return admins;
	}

	public List<Event> getEvents() {
		return Event.find("reference = ?", this).fetch();
	}

	@Override
	public void _save() {
		// plugins
		PluginManager.raiseEvent(PluginEvents.PAGE_SAVE, this);

		super._save();
	}
	
	@Override
	protected void updateFollowers() {
		User user;

		// handle forced followers
		if (hasForcedFollowers()) {			
			final Set<User> users = new HashSet<>();		
			for (Long id : pageDao.getForceFollowUserIds(this)) {				
				user = User.findById(id);
				users.add(user);
			}	
			users.addAll(this.forcedFollowers);
			for (UserGroup group : this.forcedFollowerGroups) {
				users.addAll(group.users);
			}
			followers.addAll(users);
		}

		// only members can be followers so we remove
		// non-members from followers again
		if (visibility.hasMembers()) {
			for (User userToRemove : new ArrayList<User>(followers)) {
				if (!isMember(userToRemove)) {
					followers.remove(userToRemove);
				}
			}
		}

		// get the connected user to mark all sticky posts as read
		user = UserLoader.getConnectedUser(false);
		if (user != null && !this.isFollower(user)) {
			markAllWallPostsAsRead(this, user);
		}
	}

	@Override
	public String getSearchInfoText() {
		long followerCount = getFollowerCount();
		return followerCount + " " + Messages.get("follower" + JavaExtensions.pluralize(followerCount));
	}

	@Override
	public boolean canHaveApps() {
		return true;
	}

	@Override
	public String getVisibilityIcon() {
		return visibility.icon;
	}

	@Override
	public String getVisibilityText() {
		return visibility.getLabel();
	}

	@Override
	public String getURL(Long appId) {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("slug", getSlug());

		if (appId != null) {
			App app = App.findById(appId);
			if (app != null && app.sender == this) {
				args.put("appId", appId);
				args.put("appSlug", app.getSlug());
			}
		}

		return Router.reverse("Pages.show", args).url;
	}

	/**
	 * Marks all sticky posts as read for a new page follower
	 */
	@Transactional
	private static void markAllWallPostsAsRead(Page page,  User user) {
		for (Wall wall : page.walls) {
			List<Post> postList = wall.getPostsQuery(user).fetch();
			for (Post post : postList) {
				if (post.isImportant(user) && !post.hasRead(user)) {
					post.toggleRead(user);
				}
			}
		}
	}
}

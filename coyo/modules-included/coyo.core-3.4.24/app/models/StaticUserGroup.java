package models;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import net.fortuna.ical4j.model.ConstraintViolationException;
import play.data.validation.Unique;
import play.db.jpa.JPA;

import java.util.List;

@Entity
@Table(name = "usergroup_type_static")
public class StaticUserGroup extends UserGroup {

	@Unique
	public String directoryUid;
	public String directorySource;

	public boolean isLocal() {
		return directorySource == null;
	}

	@Override
	public boolean isUserManageable() {
		return isLocal();
	}

	@Override
	public boolean isDeletable() {
		return true;
	}

	@Override
	protected void beforeSave() throws Exception {
		// directoryUid unique constraint
		if (directoryUid != null) {
			if (StaticUserGroup.count("directoryUid = ? AND id != ?", directoryUid, (id == null ? 0L : id)) > 0) {
				throw new ConstraintViolationException("directoryUid [" + directoryUid + "] already in use");
			}
		}
	}

	public static List<Long> findGroupIdsBySource(final String directorySource) {
		// load all ldap synced group objects from database
		final TypedQuery<Long> query = JPA.em().createQuery(
				"SELECT g.id FROM StaticUserGroup g WHERE directorySource like :directorySource", Long.class);

		query.setParameter("directorySource", directorySource);
		final List<Long> groupIds = query.getResultList();

		return groupIds;
	}
}

package models;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.validation.Required;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Model for holding translations.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Table(name = "translation")
public class Translation extends TenantModel {

	/**
	 * The sender that this translation is related to. Used for permission checks when translations are loaded.
	 */
	@ManyToOne(optional = false)
	public Sender sender;

	@Required
	@Column(nullable = false)
	public Locale language;

	@Required
	@Column(nullable = false)
	public String uid;

	@Lob
	public String text;

	public static List<Translation> listTranslations(Long senderId, String uid) {
		return find("sender.id = ? AND uid = ?", senderId, uid).fetch();
	}

	public static Translation findTranslation(Long senderId, String uid, String language) {
		return findTranslation(senderId, uid, new Locale(language));
	}
	
	public static Translation findTranslation(Long senderId, String uid, Locale language) {
		return find("sender.id = ? AND uid = ? AND language = ?", senderId, uid, language).first();
	}

	public static void storeTranslation(Long senderId, String uid, Locale language, String text) {
		Translation translation = find("uid = ? AND language = ?", uid, language).first();
		if (translation == null) {
			translation = new Translation();
			translation.uid = uid;
			translation.language = language;

			Sender sender = Sender.findById(senderId);
			if (sender == null) {
				throw new IllegalArgumentException("Cannot create translation because sender with id [" + senderId
						+ "] could not be found");
			}
			translation.sender = sender;
		}
		translation.text = text;
		translation.save();
	}

	public static class TranslationJsonSerializer implements JsonSerializer<Translation> {

		private boolean withText = true;

		public TranslationJsonSerializer(boolean withText) {
			this.withText = withText;
		}

		public JsonElement serialize(Translation translation, Type type, JsonSerializationContext context) {
			JsonObject o = new JsonObject();
			o.addProperty("senderId", translation.sender.id);
			o.addProperty("uid", translation.uid);
			o.addProperty("language", translation.language.getLanguage());
			if (withText) {
				o.addProperty("text", translation.text);
			}
			return o;
		}
	}
}

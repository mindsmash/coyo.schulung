package models;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app_forms_result_value")
public class FormResultValue extends TenantModel {

	@ManyToOne(optional = false)
	public FormResult result;

	@ManyToOne(optional = false)
	public FormField field;

	@Lob
	public String value;
}

package models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Serves as a blacklist for hashtags
 */
@Entity
@Table(name = "hashtagblacklist")
public class HashtagBlackList extends TenantModel {

	// Blacklisted hashtags
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "hashtagblacklist_hashtag")
	private Set<String> hashtags = new HashSet<>();

	private static final String SQL = "SELECT h FROM HashtagBlackList h";

	/**
	 * Adds one hashtag to the hashtag blacklist
	 *
	 * @param hashtag String
	 */
	public static Set<String> addHashtag(final String hashtag) {
		final TypedQuery<HashtagBlackList> q = em().createQuery(SQL, HashtagBlackList.class);
		try {
			final HashtagBlackList blackList = q.getSingleResult();
			blackList.hashtags.add(hashtag);
			blackList.save();

			return blackList.hashtags;
		} catch (NoResultException e) {
			final HashtagBlackList blackList = new HashtagBlackList();
			blackList.hashtags.add(hashtag);
			blackList.save();

			return blackList.hashtags;
		}
	}

	/**
	 * Removes one hashtag to the hashtag blacklist
	 *
	 * @param hashtag String
	 */
	public static Set<String> removeHashtag(final String hashtag) {
		final TypedQuery<HashtagBlackList> q = em().createQuery(SQL, HashtagBlackList.class);
		try {
			final HashtagBlackList blackList = q.getSingleResult();
			blackList.hashtags.remove(hashtag);
			blackList.save();

			return blackList.hashtags;
		} catch (NoResultException e) {
			return new HashSet<>();
		}
	}

	/**
	 * Returns the hashtag blacklist
	 */
	public static Set<String> getHashtags() {
		final TypedQuery<HashtagBlackList> q = em().createQuery(SQL, HashtagBlackList.class);
		try {
			final HashtagBlackList blackList = q.getSingleResult();

			return blackList.hashtags;
		} catch (NoResultException e) {
			return Collections.emptySet();
		}
	}
}

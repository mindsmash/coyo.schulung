package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.app.FilesApp;
import models.app.files.File;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_sharefile")
public class ShareFilePost extends Post {

	@ManyToOne(optional = false)
	public FilesApp app;
	public String fileUid;

	public File getFile() {
		return app.getFile(fileUid);
	}

	public File getTargetFile() {
		File file = app.getFile(fileUid);
		if (file != null && !file.isFolder()) {
			return file.getParent();
		}
		return file;
	}
}
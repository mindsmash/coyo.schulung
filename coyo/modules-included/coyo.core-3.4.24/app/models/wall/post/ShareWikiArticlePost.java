package models.wall.post;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.comments.Comment;
import models.app.WikiAppArticle;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_sharewiki")
public class ShareWikiArticlePost extends Post {

	@ManyToOne(optional = false)
	public WikiAppArticle article;

	@Override
	public List<Comment> getComments() {
		if (article.app.usersCanComment) {
			return article.getComments();
		} else {
			return super.getComments();
		}
	}

	@Override
	public void addComment(Comment comment) {
		if (article.app.usersCanComment) {
			article.addComment(comment);
		} else {
			super.addComment(comment);
		}
	}

	@Override
	public void removeComment(Comment comment) {
		if (article.app.usersCanComment) {
			article.removeComment(comment);
		} else {
			super.removeComment(comment);
		}
	}
}
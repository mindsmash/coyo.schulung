package models.wall.post;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.comments.Comment;
import models.User;
import models.app.BlogAppArticle;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_shareblog")
public class ShareBlogArticlePost extends Post {

	@ManyToOne(optional = false)
	public BlogAppArticle article;

	@Override
	public List<Comment> getComments() {
		return article.getComments();
	}

	@Override
	public void addComment(Comment comment) {
		article.addComment(comment);
	}

	@Override
	public void removeComment(Comment comment) {
		article.removeComment(comment);
	}

	@Override
	public List<User> getLikes() {
		return article.getLikes();
	}

	@Override
	public void like(User user) {
		article.like(user);
		this.save();
	}

	@Override
	public void unlike(User user) {
		article.unlike(user);
		this.save();
	}

	@Override
	public boolean likes(User user) {
		return article.likes(user);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return article.checkDeleteCommentPermission(comment, user);
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return article.checkCreateCommentPermission(user);
	}
}

package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.Table;

import models.User;
import fishr.Indexed;

/**
 * LikeLinkPosts are displayed on the wall of the first liker.
 * 
 * @author mindsmash GmbH
 */
@Entity
@Indexed
@Table(name = "post_type_likelink")
public class LikeLinkPost extends Post {

	public String url;

	public void unlike(User user) {
		super.unlike(user);

		if (getLikes().size() == 0) {
			refresh();
			delete();
		}
	}
}
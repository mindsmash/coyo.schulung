package models.wall.post;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import acl.Permission;
import controllers.Security;
import models.comments.Comment;
import models.User;
import models.wall.Wall;
import utils.HibernateUtils;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_sharepost")
public class SharePostPost extends Post {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public Post post;

	@Override
	public Post share(User user, Wall wall, String message) {
		SharePostPost post = new SharePostPost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.post = this.post;
		post.save();
		return post;
	}

	@Override
	public List<Comment> getComments() {
		return post.getComments();
	}

	@Override
	public void addComment(Comment comment) {
		post.addComment(comment);
	}

	@Override
	public void removeComment(Comment comment) {
		post.removeComment(comment);
	}

	@Override
	public List<User> getLikes() {
		return post.getLikes();
	}

	@Override
	public void like(User user) {
		post.like(user);
	}

	@Override
	public void unlike(User user) {
		post.unlike(user);
	}

	@Override
	public boolean likes(User user) {
		return post.likes(user);
	}

	public Post getPost() {
		/*
		 * We need to manually unproxy the post child here, because we are
		 * lazy-loading the post by default. Lazy loading should, however, not
		 * be deactivated because it leads to very long JOIN query to the DB.
		 */
		post = HibernateUtils.unproxy(post);
		return post;
	}
	
	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		// allow post owner to delete comment
		return Security.check(Permission.DELETE_POST, post.id);
	}
}
package models.wall.post;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import libs.DateI18N;
import models.Sender;
import models.User;
import models.messaging.Conversation;
import models.messaging.ConversationEvent;
import models.wall.ExcludeFromWall;
import models.wall.Wall;
import play.i18n.Messages;
import utils.HibernateUtils;
import utils.JPAUtils;
import extensions.CommonExtensions;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "messaging")
@ExcludeFromWall
@Table(name = "post_type_conversation")
public class ConversationPost extends Post {

	@ManyToOne(optional = false)
	public Conversation conversation;

	public ConversationEvent event;

	public Sender getAuthor() {
		// fix lazy loading
		return HibernateUtils.unproxy(author);
	}

	public String getEventMessage(User user) {
		return event.getLabel(user, author);
	}

	@Override
	public boolean checkSearchPermission() {
		return conversation.checkSearchPermission();
	}

	// void conversation posts (chat messages) to be scanned for hashtags
	@Override
	public Set<String> getHashTags() {
		return new HashSet<>();
	}

	@Override
	public String getSearchInfoText() {
		return CommonExtensions.format(created, DateI18N.getFullFormat()) + " "
				+ Messages.get("byName", author.getDisplayName()) + " ›› " + conversation.getDisplayName();
	}

	/**
	 * Can be deleted in 3.4. Just for backwards compatibility. Was inserted instead of a db evolution.
	 */
	@Override
	protected void afterSelect() throws Exception {
		super.afterSelect();
		
		if (!conversation.getDefaultWall().restrictCollaboration && conversation.getDefaultWall().isPersistent()) {
			Wall wall = conversation.getDefaultWall();
			wall.restrictCollaboration = true;
			wall.save();
			JPAUtils.makeTransactionWritable();
		}
	}
}
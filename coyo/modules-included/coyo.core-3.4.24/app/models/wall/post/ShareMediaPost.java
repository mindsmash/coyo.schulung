package models.wall.post;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.comments.Comment;
import models.User;
import models.media.Media;
import fishr.Indexed;

@Entity
@Indexed
@Table(name = "post_type_sharemedia")
public class ShareMediaPost extends Post {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public Media media;
	
	@Override
	public List<Comment> getComments() {
		return media.getComments();
	}

	@Override
	public void addComment(Comment comment) {
		media.addComment(comment);
	}

	@Override
	public void removeComment(Comment comment) {
		media.removeComment(comment);
	}

	@Override
	public List<User> getLikes() {
		return media.getLikes();
	}

	@Override
	public void like(User user) {
		media.like(user);
	}

	@Override
	public void unlike(User user) {
		media.unlike(user);
	}

	@Override
	public boolean likes(User user) {
		return media.likes(user);
	}
}
package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fishr.Indexed;
import models.Sender;
import models.notification.Notification;
import utils.HibernateUtils;

@Entity
@Indexed(boost = 0.3F)
@Table(name = NotificationPost.POST_TYPE_NOTIFICATION_TABLE)
public class NotificationPost extends Post {

	public static final String POST_TYPE_NOTIFICATION_TABLE = "post_type_notification";

	@ManyToOne(optional = false)
	public Notification notification;

	public Sender getAuthor() {
		// fix lazy loading
		return HibernateUtils.unproxy(author);
	}
}
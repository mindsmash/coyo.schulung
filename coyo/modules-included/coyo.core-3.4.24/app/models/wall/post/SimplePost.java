package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.Table;

import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_simple")
public class SimplePost extends Post {

}
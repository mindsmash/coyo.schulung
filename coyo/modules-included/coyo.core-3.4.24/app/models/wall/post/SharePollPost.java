package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.app.PollApp;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_sharepoll")
public class SharePollPost extends Post {

	@ManyToOne(optional = false)
	public PollApp app;
}
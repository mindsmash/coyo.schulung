package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Call;
import fishr.Indexed;

@Entity
@Indexed
@Table(name = "post_type_sharecall")
public class ShareCallPost extends Post {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public Call call;
	
}
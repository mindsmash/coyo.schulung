package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.User;
import fishr.Indexed;

/**
 * Relation between posts and users.
 * 
 * @author mindsmash GmbH
 */
@Entity
@Indexed
@Table(name = "post_user")
public class PostUser extends TenantModel {

	@ManyToOne(optional = false)
	public Post post;

	@ManyToOne(optional = false)
	public User user;

	public boolean likes;
	public boolean following;
	public boolean flagged;
	public boolean isRead;

	public static PostUser findOrCreate(Post post, User user) {
		if (post == null || !post.isPersistent()) {
			throw new IllegalStateException("cannot load post-user relation for non-existent post [" + post
					+ "]. has the post been saved?");
		}

		PostUser postUser = find("post = ? AND user = ?", post, user).first();
		if (postUser == null) {
			postUser = new PostUser();
			postUser.post = post;
			postUser.user = user;
			postUser.save();
		}
		return postUser;
	}
	
	@Override
	public void _save() {
		super._save();
		
		// update modified date to release HTTP caches
		post.save();
	}
}

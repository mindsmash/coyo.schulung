package models.wall.post;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import libs.DateI18N;
import models.BaseModel;
import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Likable;
import models.Sender;
import models.Sharable;
import models.User;
import models.UserNotification;
import models.comments.Comment;
import models.comments.Commentable;
import models.notification.PostCommentNotification;
import models.notification.PostLikeNotification;
import models.notification.PostNotification;
import models.statistics.TrackingRecord;
import models.wall.ExcludeFromWall;
import models.wall.Wall;
import models.wall.attachments.FilePostAttachment;
import models.wall.attachments.LinkPostAttachment;
import models.wall.attachments.PostAttachment;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import play.cache.Cache;
import play.data.Upload;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.utils.HTML;
import session.UserLoader;
import storage.FlexibleBlob;
import util.CacheUtil;
import utils.Hashifier;
import utils.Linkifier;
import utils.Smilifier;
import acl.Permission;
import controllers.Posts;
import controllers.Security;
import extensions.CommonExtensions;
import fishr.Field;
import fishr.Fishr;
import fishr.Related;

/**
 * Model for posts on a wall.
 *
 * @author Marko Ilic, Jan Marquardt
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "post")
@FilterDef(name = "oldPosts", defaultCondition = "created BETWEEN :latest AND NOW()", parameters = @ParamDef(type = "date", name = "latest"))
@Filter(name = "oldPosts")
public abstract class Post extends ExtendedTenantModel implements Likable, Commentable, Comparable<Post>,
		ExtendedSearchableModel, Sharable {

	public static final String POST_COMMENT_TABLE = "post_comment";

	@Lob
	@Field
	public String message;

	@ManyToOne(optional = false)
	public Sender author;

	// sticky
	public boolean important = false;

	@ManyToOne(optional = false)
	public Wall wall;

	@OneToMany(cascade = CascadeType.REMOVE)
	@OrderBy("created ASC")
	@JoinTable(name = POST_COMMENT_TABLE)
	public List<Comment> comments = new ArrayList<Comment>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
	public List<PostAttachment> attachments = new ArrayList<PostAttachment>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
	public List<PostNotification> notifications = new ArrayList<PostNotification>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
	public List<SharePostPost> shares = new ArrayList<SharePostPost>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post", fetch = FetchType.LAZY)
	private List<PostUser> userRelations = new ArrayList<>();

	public String getLinkifiedMessage() {
		String escapedHtml = HTML.htmlEscape(message);
		escapedHtml = Linkifier.linkify(escapedHtml);
		escapedHtml = Smilifier.smilify(escapedHtml);
		if (Fishr.HASHTAGS) {
			escapedHtml = Hashifier.hashify(escapedHtml);
		}

		return escapedHtml;
	}

	@Override
	public Collection<User> getInterestedUsers() {
		return wall.sender.followers;
	}

	public void like(User user) {
		if (!likes(user)) {
			PostUser postUser = PostUser.findOrCreate(this, user);
			postUser.likes = true;
			postUser.following = true;
			postUser.save();

			PostLikeNotification.raise(this, user);
		}
	}

	public void unlike(User user) {
		if (likes(user)) {
			PostUser postUser = PostUser.findOrCreate(this, user);
			postUser.likes = false;
			postUser.save();
			PostLikeNotification.cleanup(this);
		}
	}

	public boolean likes(User user) {
		return PostUser.count("post = ? AND user = ? AND likes = true", this, user) > 0;
	}

	public boolean toggleFollowing(User user) {
		PostUser postUser = PostUser.findOrCreate(this, user);
		postUser.following = !postUser.following;
		postUser.save();
		return postUser.following;
	}

	public void follow(User user) {
		PostUser postUser = PostUser.findOrCreate(this, user);
		postUser.following = true;
		postUser.save();
	}

	public boolean isFollowing(User user) {
		final PostUser postUser = PostUser.find("post = ? AND user = ?", this, user).first();
		return postUser != null ? postUser.following : false;
	}

	public boolean toggleFlagged(User user) {
		PostUser postUser = PostUser.findOrCreate(this, user);

		// invalidate flagged post count cached value
		play.cache.Cache.delete(user.id + User.ActivityStreamSelect.FLAGGED_POST_COUNT_CACHE_KEY);

		// auto-follow when flagged
		if (!postUser.flagged) {
			postUser.following = true;
		}

		postUser.flagged = !postUser.flagged;
		postUser.save();
		return postUser.flagged;
	}

	public boolean isFlagged(User user) {
		return PostUser.findOrCreate(this, user).flagged;
	}

	public boolean toggleRead(User user) {
		PostUser postUser = PostUser.findOrCreate(this, user);
		postUser.isRead = !postUser.isRead;
		postUser.save();
		return postUser.isRead;
	}

	public boolean hasRead(User user) {
		return PostUser.findOrCreate(this, user).isRead;
	}

	public List<User> getLikes() {
		return PostUser.find("SELECT pu.user FROM PostUser pu WHERE pu.post = ? AND pu.likes = true", this).fetch();
	}

	public List<User> getFollowers() {
		return PostUser.find("SELECT pu.user FROM PostUser pu WHERE pu.post = ? AND pu.following = true", this).fetch();
	}

	public List<User> getFlagged() {
		return PostUser.find("SELECT pu.user FROM PostUser pu WHERE pu.post = ? AND pu.flagged = true", this).fetch();
	}

	public List<User> getRead() {
		return PostUser.find("SELECT pu.user FROM PostUser pu WHERE pu.post = ? AND pu.isRead = true", this).fetch();
	}

	public Long getReadCount() {
		return PostUser.count("post = ? AND isRead = true", this);
	}

	@Override
	public int compareTo(Post o) {
		if (important && !o.important) {
			return -1;
		} else if (!important && o.important) {
			return 1;
		}

		return (int) (o.id - id);
	}

	@Override
	protected void beforeSave() throws Exception {
		// TODO: can be removed?
		Cache.delete("post-" + id + "-popularity");

		CacheUtil.clear(this);
		super.beforeSave();
	}

	@Override
	protected void afterInsert() throws Exception {
		super.afterInsert();
		if (!this.getClass().isAnnotationPresent(ExcludeFromWall.class)) {
			TrackingRecord.trackOne("posts");
		}
	}

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();

		PostUser postUser = PostUser.findOrCreate(this, comment.author);
		postUser.following = true;
		postUser.save();

		// raise user notifications
		PostCommentNotification.raise(this, comment);
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		save();

		// cleanup user notifications
		PostCommentNotification.cleanup(this);
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Looks at the user's notifications to check if this post is in an unread state.
	 *
	 * @param user
	 * @return True if this post is in an unread state for given user
	 */
	public boolean hasNewNotifications(User user) {
		return UserNotification
				.count("SELECT COUNT(un) FROM UserNotification un JOIN un.notification n WHERE un.user = ? AND n.post = ? AND un.isRead = false",
						user, this) > 0;
	}

	public void markNotificationsRead(User user) {
		List<UserNotification> notifications = UserNotification
				.find("SELECT un FROM UserNotification un JOIN un.notification n WHERE un.user = ? AND n.post = ? AND un.isRead = false",
						user, this).fetch();

		if (notifications.size() > 0) {
			for (UserNotification un : notifications) {
				un.isRead = true;
				un.save();
			}

			save();
		}
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.READ_POST, this);
	}

	@Override
	public boolean isSharable() {
		return wall.sender.isSharable() || wall.sender == UserLoader.getConnectedUser(false);
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		SharePostPost post = new SharePostPost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.post = this;
		post.save();
		return post;
	}

	/**
	 * Returns whether this post is important for the passed user. This is the case if the post is flagged as important,
	 * the user has not read it yet and the post was created after the user existed.
	 */
	public boolean isImportant(User user) {
		return important && !hasRead(user) && this.created.after(user.created);
	}

	public List<FilePostAttachment> getFileAttachments() {
		return FilePostAttachment.find("post = ?", this).fetch();
	}

	public List<LinkPostAttachment> getLinkAttachments() {
		return LinkPostAttachment.find("post = ?", this).fetch();
	}

	@Override
	public String getDisplayName() {
		return CommonExtensions.shorten(message, 100, "...");
	}

	public boolean createAttachments(final List<Upload> uploads) {
		boolean attachmentAdded = false;
		if (uploads != null) {
			for (Object o : uploads) {
				if (o != null && o instanceof Upload) {
					Upload upload = (Upload) o;
					if (upload.getSize().longValue() > 0L) {
						FilePostAttachment fa = new FilePostAttachment();
						FlexibleBlob blob = new FlexibleBlob();
						blob.set(upload.asStream(), upload.getContentType(), upload.getSize());
						fa.file = blob;
						fa.name = upload.getFileName();
						fa.post = this;
						fa.save();

						attachmentAdded = true;
					}
				}
			}
		}
		return attachmentAdded;
	}

	public static void enableDateFilter(int days) {
		// limits the posts displayed
		org.hibernate.Filter filter = ((org.hibernate.Session) JPA.em().getDelegate()).enableFilter("oldPosts");
		filter.setParameter("latest", DateUtils.addDays(new Date(), days * -1));
	}

	public static void disableDateFilter() {
		((org.hibernate.Session) JPA.em().getDelegate()).disableFilter("oldPosts");
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	@Override
	public Sender getSender() {
		return this.wall.sender;
	}

	@Override
	public void redirectToResult() {
		Posts.show(id);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		// allow post owner to delete comment
		return Security.check(Permission.DELETE_POST, id);
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return Security.check(Permission.COLLABORATE_POST, id);
	}

	@Override
	public boolean checkLikePermission(User user) {
		return Security.check(Permission.COLLABORATE_POST, id);
	}

	/**
	 * Injects the order clause for sorting by important posts first.
	 */
	public static JpqlSelect addImportantOrderBy(JpqlSelect select, User user) {
		select.orderBy(
				"CASE WHEN (p.important = true AND p.created > ? AND NOT EXISTS (SELECT pu FROM PostUser pu WHERE pu.post = p AND pu.user = ? AND pu.isRead = true)) THEN 0 ELSE 1 END")
				.param(user.created).param(user);
		return select;
	}

	/**
	 * Injects the default ordering for wall posts (important posts first, then by creation)
	 */
	public static JpqlSelect addDefaultOrderBy(JpqlSelect select, User user) {
		addImportantOrderBy(select, user);
		select.orderBy("p.id DESC");
		return select;
	}

	@Override
	public Sender getSearchThumbSender() {
		return author;
	}

	@Override
	public String getSearchInfoText() {
		String by = CommonExtensions.format(created, DateI18N.getFullFormat()) + " "
				+ Messages.get("byName", author.getDisplayName());
		if (author != wall.sender) {
			return by + " ›› " + wall.sender.getDisplayName();
		}
		return by;
	}

	@Related
	public String getRelatedSearchInfo() {
		return author.getDisplayName() + " " + wall.sender.getDisplayName();
	}

	@Field
	public List<String> getRelatedSearchComments() {
		return Comment.getSearchInfo(comments);
	}

	@Related
	public List<PostAttachment> getRelatedSearchAttachments() {
		return attachments;
	}

	@Related
	public List<String> getRelatedSearchAttachmentNames() {
		List<String> r = new ArrayList<>();
		for (PostAttachment attachment : attachments) {
			r.add(attachment.getDisplayName());
		}
		return r;
	}
}

package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.media.MediaCollection;
import fishr.Indexed;

@Entity
@Indexed
@Table(name = "post_type_sharemediacoll")
public class ShareMediaCollectionPost extends Post {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public MediaCollection mediaCollection;
}
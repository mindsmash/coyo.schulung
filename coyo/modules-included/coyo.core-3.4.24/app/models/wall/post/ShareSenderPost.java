package models.wall.post;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Sender;
import utils.HibernateUtils;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "posts")
@Table(name = "post_type_sharesender")
public class ShareSenderPost extends Post {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public Sender sender;

	public Sender getSender() {
		return HibernateUtils.unproxy(sender);
	}
}
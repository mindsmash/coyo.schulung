package models.wall.attachments;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;

import models.FileAttachment;
import models.wall.post.Post;
import play.mvc.Router;
import storage.FlexibleBlob;
import fishr.Related;

/**
 * 
 * Represents a File attached to a {@link Post}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
public class FilePostAttachment extends PostAttachment implements FileAttachment {

	public String name;

	public FlexibleBlob file;

	@Override
	protected void afterDelete() throws Exception {
		super.afterDelete();

		// delete attachment
		file.delete();
	}

	@Override
	public String getFileName() {
		return name;
	}

	@Override
	public FlexibleBlob getFileData() {
		return file;
	}

	@Override
	public String getContentType() {
		return file.type();
	}

	@Override
	public Long getParentSenderId() {
		return post.getSender().getId();
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public String getDownloadUrl() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", id);
		return Router.reverse("Posts.fileAttachment", args).url;
	}

	@Override
	public String getUniqueId() {
		return "postFileAttachment-" + id;
	}

	@Override
	public Long getSize() {
		return file.length();
	}

	@Override
	public InputStream get() {
		return file.get();
	}

	@Override
	public long length() {
		return file.length();
	}

	@Override
	public String type() {
		return file.type();
	}
}

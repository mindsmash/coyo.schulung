package models.wall.attachments;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.wall.post.Post;
import fishr.Related;
import fishr.search.Searchable;

/**
 * 
 * Represents an attachment to a specific {@link Post}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "post_attachment")
public abstract class PostAttachment extends TenantModel {

	@ManyToOne(optional = false)
	public Post post;
	
	abstract public String getDisplayName();
}

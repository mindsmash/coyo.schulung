package models.wall.attachments;

import javax.persistence.Entity;
import javax.persistence.Lob;

import models.wall.post.Post;

/**
 * 
 * Represents a Link attached to a {@link Post}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
public class LinkPostAttachment extends PostAttachment {

	@Lob
	public String url;

	@Override
	public String getDisplayName() {
		return url;
	}
}

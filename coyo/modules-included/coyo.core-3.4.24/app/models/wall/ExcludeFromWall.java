package models.wall;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import models.wall.post.ConversationPost;
import models.wall.post.Post;

/**
 * This annotation is used to mark {@link Post} classes. Objects of annotated
 * classes are not shown on a user's wall.
 * 
 * @see {@link ConversationPost}
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcludeFromWall {

}

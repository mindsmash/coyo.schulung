package models.wall;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.Sender;
import models.TenantModel;
import models.User;
import models.wall.post.Post;

import org.hibernate.annotations.Filter;

import play.db.helper.JpqlSelect;
import utils.JPAUtils;

/**
 * 
 * Represents a List of {@link Post}s, that is assigned to a {@link Sender}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = Wall.WALL_TABLE)
@Inheritance(strategy = InheritanceType.JOINED)
public class Wall extends TenantModel {

	public static final String WALL_TABLE = "wall";

	/*
	 * Posts should not be accessed directly because the direct access does not consider the complex sorting. Instead,
	 * the wall has custom getters that consider the sorting.
	 */
	@OneToMany(mappedBy = "wall", cascade = CascadeType.ALL)
	@Filter(name = "oldPosts")
	protected List<Post> posts = new ArrayList<Post>();

	/**
	 * private, only admins can post
	 */
	public boolean restrictPosts = false;

	/**
	 * private, only admins can like and comment
	 */
	public boolean restrictCollaboration = false;

	/**
	 * true to exclude from activity stream
	 */
	public boolean noActivityStream;

	@ManyToOne
	public Sender sender;

	public Wall(Sender sender) {
		this.sender = sender;
	}

	public Wall(Sender sender, boolean noActivityStream) {
		this.sender = sender;
		this.noActivityStream = noActivityStream;
	}

	// /**
	// * Fetches all posts that are marked important and that the given user has
	// not seen yet.
	// *
	// * @param user
	// * @return Ready to execute JPA query
	// */
	// public JPAQuery getImportantPostsQuery(User user) {
	// return Post
	// .find("SELECT p FROM Post p WHERE p.wall = ? AND (p.important = true AND ? NOT MEMBER OF p.read) ORDER BY p.id DESC",
	// this, user);
	// }
	//
	// public List<Post> getImportantPosts(User user) {
	// return getImportantPostsQuery(user).fetch();
	// }
	//
	// /**
	// * Fetches all posts that are not sticky/important for the given user.
	// *
	// * @param user
	// * @return Ready to execute JPA query
	// */
	// public JPAQuery getPostsQuery(User user) {
	// return Post
	// .find("SELECT p FROM Post p WHERE p.wall = ? AND (p.important = false OR ? MEMBER OF p.read) ORDER BY p.id DESC",
	// this, user);
	// }

	public JPAQuery getPostsQuery(User user) {
		JpqlSelect select = new JpqlSelect();
		select.from("Post p");
		select.where("p.wall = ?").param(this);
		Post.addDefaultOrderBy(select, user);
		return Post.find(select.toString(), select.getParams().toArray());
	}

	/**
	 * Injects a new persistent wall into a sender
	 * 
	 * @param sender
	 */
	public static Wall inject(Sender sender) {
		Wall wall = new Wall(sender);
		wall.save();
		sender.refresh();
		JPAUtils.makeTransactionWritable();
		return wall;
	}
}

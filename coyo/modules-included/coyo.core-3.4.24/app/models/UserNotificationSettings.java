package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import jobs.UpdateMailer;
import models.notification.Notification;
import play.db.jpa.JPA;
import play.i18n.Messages;

/**
 * This class is used to save the user's account setting about how to receive
 * notifications. Currently, only e-mail settings are saved.
 * 
 * @author Drews Clausen, Jan Marquardt, mindsmash GmbH
 */
@Entity
@Table(name = "user_notificationsettings")
public class UserNotificationSettings extends TenantModel {

	public static final String USER_NOTIFICATION_SETTINGS_EVENT_TABLE = "user_notificationsettings_event";

	public enum MailInterval {
		QUARTER_HOURLY("0 0/15 * * * ?", 15), HOURLY("0 0 * * * ?", 60), FOUR_HOURLY("0 0 0/4 * * ?", 240), DAILY(
				"0 0 10 * * ?", 1440), WEEKLY("0 0 10 ? * MON", 10080);

		public final String cron;
		public final int intervalMinutes;

		private MailInterval(String cron, int intervalMinutes) {
			this.cron = cron;
			this.intervalMinutes = intervalMinutes;
		}

		public String getLabel() {
			return Messages.get("notifications.mail.interval." + this + ".label");
		}
	}

	@ElementCollection
	@JoinTable(name = USER_NOTIFICATION_SETTINGS_EVENT_TABLE)
	public List<String> mailEvents = new ArrayList<String>();

	public MailInterval mailInterval = MailInterval.DAILY;
	public boolean receiveMails = true;

	public MailInterval getMailInterval() {
		if (mailInterval == null) {
			return MailInterval.DAILY;
		}
		return mailInterval;
	}

	public boolean isWanted(Notification notification) {
		return receiveMails && (mailEvents.contains(notification.getClass().getSimpleName()));
	}

	public boolean isWanted(String mailEvent) {
		return receiveMails && (mailEvents.contains(mailEvent));
	}

	public UserNotificationSettings() {
		this.mailEvents = getAllMailEvents();
	}

	public static List<String> getAllMailEvents() {
		List<String> r = new ArrayList<String>();
		r.addAll(Notification.getConfigurableNotificationTypes());
		r.add(UpdateMailer.NEW_POSTS_MAIL_EVENT);
		r.add(UpdateMailer.NEW_MESSAGES_MAIL_EVENT);
		return r;
	}

	@Override
	protected void beforeDelete() throws Exception {
		JPA.em()
				.createNativeQuery(
						"DELETE FROM " + USER_NOTIFICATION_SETTINGS_EVENT_TABLE
								+ " WHERE usernotificationsettings_id = " + id).executeUpdate();
		super.beforeDelete();
	}

	@Override
	public void beforeSave() throws Exception {
		// adopt default notification settings from application
		if (!isPersistent()) {
			if (Application.count() != 0 && Application.get().defaultNotificationSettings != null) {
				UserNotificationSettings defaultNotificationSettings = Application.get().defaultNotificationSettings;
				this.mailEvents = new ArrayList<>(defaultNotificationSettings.mailEvents);
				this.mailInterval = defaultNotificationSettings.mailInterval;
				this.receiveMails = defaultNotificationSettings.receiveMails;
			}
		}
	}
}

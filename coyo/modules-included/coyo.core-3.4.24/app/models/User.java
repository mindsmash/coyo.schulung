package models;
import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;

import models.app.CalendarApp;
import models.app.WallApp;
import models.comments.Comment;
import models.dao.PageDao;
import models.dao.UserDao;
import models.dao.WorkspaceDao;
import models.event.Event;
import models.event.EventSeriesChild;
import models.event.EventUser;
import models.helper.ClearManyToManyHelper;
import models.media.Media;
import models.media.MediaCollection;
import models.media.MediaRoom;
import models.messaging.ConversationMember;
import models.messaging.ConversationStatus;
import models.notification.EventNotification;
import models.notification.Notification;
import models.notification.Notification.DisplayType;
import models.page.Page;
import models.wall.ExcludeFromWall;
import models.wall.Wall;
import models.wall.post.ConversationPost;
import models.wall.post.Post;
import models.wall.post.PostUser;
import models.workspace.Workspace;
import models.workspace.Workspace.WorkspaceActivitySorter;
import models.workspace.WorkspaceMember;
import onlinestatus.OnlineStatusService;
import onlinestatus.OnlineStatusServiceFactory;
import onlinestatus.UserOnlineStatus;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.data.binding.NoBinding;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.InPast;
import play.data.validation.Match;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.helper.JpqlSelect;
import play.db.helper.SqlSelect.Where;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.Crypto;
import play.templates.JavaExtensions;
import plugins.PluginEvents;
import plugins.PluginManager;
import plugins.play.PushNotificationPlugin;
import session.UserLoader;
import util.Util;
import utils.JPAUtils;
import utils.LanguageUtils;
import validation.SimpleURL;
import binding.ModelIdBinder;
import checks.EmailRequiredCheck;
import checks.UserPasswordCheck;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import conf.ApplicationSettings;
import di.user.DefaultUserNameBuilder;
import di.user.UserNameBuilder;
import events.type.DummyEvent;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;
import fishr.search.SearchableModel;

/**
 * Represents a User.
 *
 * @author Marko Ilic, Jan Marquardt, Drews Clausen
 */
@Entity
@Indexed(boost = 10F)
@Searchable(category = "users")
@Table(name = "usr")
@SenderType("user")
@InjectionSupport
public class User extends Sender implements SearchableModel {

	@Inject(configuration = "coyo.di.dao.page", defaultClass = PageDao.class, singleton = true)
	private static PageDao pageDao;

	@Inject(configuration = "coyo.di.user.namebuilder", defaultClass = DefaultUserNameBuilder.class, singleton = true)
	private static UserNameBuilder nameBuilder;

	/**
	 * This enum defines the user field types, which can be queried dynamically from database
	 */
	private enum UserFieldOption {
		JOBTITLE("jobTitle"), COMPANY("company"), OFFICE("office"), DEPARTMENT("department");

		private String fieldName;

		private UserFieldOption(String fieldName) {
			this.fieldName = fieldName;
		}
	}

	public static final int ONLINE_MINUTES = 5;
	private static final int BIRTHDATE_YEAR_NOT_SET = 1896;

	private static final Integer POST_MONTH_THRESHOLD = Integer.parseInt(
			Play.configuration.getProperty("mindsmash.activitystream.threshold", "0"));

	private static final Integer LAST_ACTIVITY_STREAM_UPDATE_DAYS_MAX = Integer.parseInt(
			Play.configuration.getProperty("coyo.lastActivityStreamUpdate.max.days", "30"));

	@Unique
	@CheckWith(EmailRequiredCheck.class)
	@Email
	@Field
	@NoBinding("profile")
	@MaxSize(255)
	public String email;

	/**
	 * The authUid is the primary authentication id. For local users, this is always the email address. For users with
	 * an alternative authSource, the authUid can be anything else (ID in LDAP directory, OpenID etc.)
	 */
	@Unique
	@NoBinding("profile")
	@MaxSize(255)
	public String authUid;

	/**
	 * A secondary authUid which gets checked on login after the primary authUid. This can be used in customizing
	 * projects to give users an alternative id to login with (e.g. personal id, username, etc.).
	 */
	@Unique
	@NoBinding("profile")
	@MaxSize(255)
	public String authUid2;

	@NoBinding("profile")
	@MaxSize(255)
	public String authSource;

	/**
	 * A real cross-tenant unique ID of a user.
	 */
	@Column(unique = true, nullable = false)
	private String uid;

	@CheckWith(UserPasswordCheck.class)
	@NoBinding
	@MaxSize(255)
	public String password;

	@NoBinding("profile")
	public boolean superadmin = false;

	@NoBinding("profile")
	public boolean superadminMode = false;

	@NoBinding("profile")
	public boolean active = true;

	@Column(nullable = false)
	public Locale language;

	@ManyToOne
	@NoBinding("profile")
	public UserRole role;

	@ManyToMany(mappedBy = "users")
	@OrderBy("name")
	@NoBinding("profile")
	public List<UserGroup> groups = new ArrayList<UserGroup>();

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "user")
	public List<UserDevice> userDevices;

	@Transient
	public List<Long> groupIds;

	@NoBinding("profile")
	@MaxSize(255)
	public String slug;

	// TODO: extract to custom embedded class
	// Basic Information
	// -----------------
	@MaxSize(200)
	public String title;
	@Field
	@Required
	@MaxSize(200)
	@Match(value = "^[^\"]+$", message = "validation.charsNotAllowed")
	public String firstName;
	@Field
	@Required
	@MaxSize(200)
	@Match(value = "^[^\"]+$", message = "validation.charsNotAllowed")
	public String lastName;
	@MaxSize(200)
	public String nickname;

	@InPast
	public Date birthdate;

	@Field
	@MaxSize(200)
	public String hometown;
	@Field
	@MaxSize(200)
	public String currentLocation;
	@Field
	@MaxSize(200)
	public String languages;
	@Field
	@Lob
	public String expertise;
	@Field
	@Lob
	public String currentProjects;
	@Field
	@Lob
	public String interests;

	public String timezone;

	// Work and Education
	// -----------------
	@Field
	@MaxSize(200)
	public String jobTitle;
	@Field
	@MaxSize(200)
	public String company;
	@Field
	@MaxSize(200)
	public String department;
	@Field
	@MaxSize(200)
	public String office;
	@Field
	@MaxSize(200)
	public String college;
	@MaxSize(200)
	public String highschool;

	// @ManyToOne
	// public User boss;

	public Long bossId;

	// Contact
	// -----------------
	@MaxSize(200)
	public String street;
	@MaxSize(200)
	public String streetNumber;

	@MaxSize(200)
	public String zipCode;
	@MaxSize(200)
	public String city;
	@MaxSize(200)
	public String country;

	@Field
	@MaxSize(200)
	public String workPhone;
	@Field
	@MaxSize(200)
	public String workFax;
	@Field
	@MaxSize(200)
	public String mobilePhone;
	@MaxSize(200)
	public String skype;
	@MaxSize(200)
	public String linkedIn;
	@MaxSize(200)
	public String xing;
	@MaxSize(200)
	public String twitter;
	@MaxSize(200)
	public String facebook;

	@Field
	@SimpleURL
	@MaxSize(200)
	public String website;

	// Activities
	// -----------------
	@NoBinding
	@OneToMany(mappedBy = "user")
	public List<ConversationMember> conversations = new ArrayList<ConversationMember>();

	@OneToMany(mappedBy = "host")
	public List<Call> callHost = new ArrayList<Call>();

	@ManyToMany(mappedBy = "participants")
	public List<Call> callParticipant = new ArrayList<Call>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<PostUser> postRelations = new ArrayList<>();

	@NoBinding
	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
	public UserNotificationSettings notSettings = new UserNotificationSettings();

	@NoBinding
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	public List<EventUser> events = new ArrayList<EventUser>();

	@NoBinding
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	@OrderBy("modified DESC")
	public List<UserNotification> notifications = new ArrayList<UserNotification>();

	@NoBinding
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	public List<WorkspaceMember> workspaces = new ArrayList<WorkspaceMember>();

	@NoBinding
	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public List<Comment> comments = new ArrayList<>();

	@NoBinding
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@OrderBy("id")
	public List<UserPushDevice> pushDevices = new ArrayList<>();

	@Transient
	public List<Event> upcomingEvents;

	@Transient
	public List<Event> myEvents;

	// @Transient
	// public List<Event> upcomingSimpleEvents;
	// @Transient
	// public List<EventSeriesChild> eventSeriesChilds;

	@NoBinding
	@ManyToMany(mappedBy = "followers", fetch = FetchType.LAZY)
	public Set<Sender> following = new HashSet<Sender>();

	@ElementCollection
	@CollectionTable(name = "user_hashtag")
	public Set<String> hashtags = new HashSet<>();

	// Technical Information
	// -----------------
	@NoBinding
	public Date passwordModified;
	@NoBinding
	public String passwordResetToken;
	@NoBinding
	public boolean resetPassword = false; // forces user to reset password
	@NoBinding
	public int loginCount = 0;
	@NoBinding
	public Date lastLogin;
	@NoBinding
	public Date lastActivityStreamUpdate = new Date();
	@NoBinding
	public Long lastPushUnreadCount;
	@NoBinding
	public Date termsOfUseAccepted;
	@NoBinding
	public Date lastNotificationRequest = new Date();

	public boolean isLocal() {
		return authSource == null;
	}

	public DateTime getLocalDate(Date date) {
		return new DateTime(date).withZone(getDateTimeZone());
	}

	public DateTimeZone getDateTimeZone() {
		if (!StringUtils.isEmpty(timezone)) {
			try {
				return DateTimeZone.forID(timezone);
			} catch (IllegalArgumentException ignored) {
			}
		}
		return DateTimeZone.UTC;
	}

	@Override
	public String getSlug() {
		return slug;
	}

	public User getBoss() {
		if (bossId != null) {
			return User.findById(bossId);
		}
		return null;
	}

	/**
	 * Encrypts and sets the password of the user to the given <code>password</code>
	 *
	 * @param password
	 *            new plain text password
	 */
	public final void setPassword(String password) {
		this.passwordModified = new Date();
		this.password = Crypto.passwordHash(password);
	}

	public void setFirstName(String firstName) {
		if (!StringUtils.equalsIgnoreCase(this.firstName, firstName)) {
			this.slug = null; // generate new on save
		}
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		if (!StringUtils.equalsIgnoreCase(this.lastName, lastName)) {
			this.slug = null; // generate new on save
		}
		this.lastName = lastName;
	}

	/**
	 * @return full name of the user
	 */
	public final String getFullName() {
		return nameBuilder.getFullName(this);
	}

	/**
	 * @return full name of the user with title
	 */
	public final String getFullNameWithTitle() {
		return nameBuilder.getFullNameWithTitle(this);
	}

	@Override
	public boolean isActive() {
		return this.active;
	}

	/**
	 * @return Real users that follow me (active, not external)
	 */
	public List<User> getRealFollowers() {
		return getRealFollowersQuery().fetch();
	}

	public JPAQuery getRealFollowersQuery() {
		JpqlSelect select = getRealFollowersSelect();
		select.select("f");
		select.orderBy("f.lastName, f.firstName");
		return User.find(select.toString(), select.getParams().toArray());
	}

	public long getRealFollowersCount() {
		JpqlSelect select = getRealFollowersSelect();
		return User.count(select.toString(), select.getParams().toArray());
	}

	private JpqlSelect getRealFollowersSelect() {
		JpqlSelect select = new JpqlSelect();
		select.from("User u JOIN u.followers f");
		select.where("u.id = ? AND f.id != ? AND f.active = true AND f.external = false").params(id, id);
		return select;
	}

	/**
	 * @return Real users I am following (active, not external)
	 */
	public List<User> getRealFollowing() {
		return getRealFollowingQuery().fetch();
	}

	public JPAQuery getRealFollowingQuery() {
		JpqlSelect select = getRealFollowingSelect();
		select.select("f");
		select.orderBy("f.lastName, f.firstName");
		return User.find(select.toString(), select.getParams().toArray());
	}

	public long getRealFollowingCount() {
		JpqlSelect select = getRealFollowingSelect();
		return User.count(select.toString(), select.getParams().toArray());
	}

	private JpqlSelect getRealFollowingSelect() {
		JpqlSelect select = new JpqlSelect();
		select.from("Sender s JOIN s.following f");
		select.where("s.id = ? AND TYPE(f) = User AND f.id != ? AND f.active = true AND f.external = false").params(id,
				id);
		return select;
	}

	/**
	 * Fetches the default media room for this user. Creates one, if non exist.
	 */
	public MediaRoom getMediaRoom() {
		if (mediaRooms.size() == 0) {
			MediaRoom room = new MediaRoom(this);
			room.save();
			refresh();
			JPAUtils.makeTransactionWritable();
		}
		return mediaRooms.get(0);
	}

	public MediaCollection getProfilePictureCollection(boolean create) {
		MediaCollection collection = getMediaRoom().getCollectionByUid("profilePictures");
		if (collection == null && create) {
			collection = new MediaCollection();
			collection.room = getMediaRoom();
			collection.uid = "profilePictures";
			collection.name = "Profile pictures";
			collection.save();
		}
		return collection;
	}

	/**
	 * @param password
	 *            plain text password
	 * @return true if and only if the given <code>password</code> is correct for this user
	 */
	public final boolean authenticate(String password) {
		if (this.password == null) {
			return false;
		}
		return this.password.equals(Crypto.passwordHash(password));
	}

	public boolean hasGrant(String grant) {
		if (role != null && role.hasGrant(grant)) {
			return true;
		}

		for (UserGroup group : groups) {
			if (group.hasGrant(grant)) {
				return true;
			}
		}

		return false;
	}

	@Override
	protected void updateFollowers() {
		// auto-follow
		if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
			// add all users to my followers
			List<User> all = User.find("active = true AND external = false").fetch();
			followers.clear();
			followers.addAll(all);
		}

		followers.add(this);
	}

	/**
	 * This method makes sure that this user is following everything that he should be following due to an automation
	 * setting (e.g. sticky pages).
	 */
	private void updateAutoFollowing() {
		Logger.debug("[User] Updating the auto-followings for user: %s", this);

		// auto-follow
		if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
			// add all users
			List<User> all = User.find("active = true").fetch();
			for (User user : all) {
				user.save();
			}
		}

		// add me to all my pages
		for (Page page : getPages(null)) {
			page.save();
		}

		// add me to all pages where I am a forced follower
		for (Page page : pageDao.getForcedFollowPages(this)) {
			page.save();
		}
	}

	@Override
	protected void beforeSave() throws Exception {
		((Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);

		// set a unique slug
		if (slug == null) {
			slug = JavaExtensions.slugify(getFullName());

			// avoid duplicate slugs
			String tmpSlug = slug;
			int i = 2;
			while (User.count("slug = ? AND id != ?", tmpSlug, (id == null ? 0L : id)) > 0) {
				tmpSlug = slug + i++;
			}

			slug = tmpSlug;
		}

		if (language == null) {
			language = LanguageUtils.getDefaultLocale();
		}

		if (authSource == null) {
			// email NULL constraint if from local authSource
			if (email == null) {
				throw new IllegalStateException("email field must not be null for local users");
			}

			authUid = email;
		}

		// authUid unique constraint
		if (User.count("authUid = ? AND id != ?", authUid, (id == null ? 0L : id)) > 0) {
			throw new IllegalStateException("authUid [" + authUid + "] already in use");
		}

		((Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);

		// set real unique id
		uid = tenant.id + "-" + authUid;

		super.beforeSave();
	}

	@Override
	public void _save() {
		boolean created = !isPersistent();

		// plugins
		PluginManager.raiseEvent(PluginEvents.USER_SAVE, this);

		super._save();

		if (created) {
			updateAutoFollowing();
		}
	}

	@Override
	public final String toString() {
		return "User[id=" + id + ", email=" + email + "]";
	}

	public void receiveNotification(Notification notification) {
		if (notification != null) {
			UserNotification un = null;

			if (Notification.isReusableNotification(notification.getClass())) {
				un = UserNotification.find("user = ? AND notification = ? ORDER BY created DESC", this, notification)
						.first();
			}


			// dont distribute notifications to regular users for deactivated apps
			if (notification instanceof EventNotification) {
				CalendarApp app = ((EventNotification) notification).event.calendar;
				
				if (app != null && !app.active && !app.sender.getAdmins().contains(this)) {
					return;	
				}
			}

			// create new if not reusable
			if (un == null) {
				un = new UserNotification(this, notification);
			}

			// mark as unread
			un.isRead = false;
			un.setEventDate(new Date());
			un.save();

			// publish dummy event to trigger event service to resolve update service futures (long polling)
			new DummyEvent().raise(this);
			// push
			PushNotificationPlugin.push(un);
		}
	}

	/**
	 * Find user ids of users owning a specific authSource
	 *
	 * @param authSource
	 *            String
	 * @return List<Long>
	 */
	public static List<Long> findUsersIdsByAuthSource(final String authSource) {
		// load all ldap synced users objects from database
		final TypedQuery<Long> query = em().createQuery("SELECT u.id FROM User u WHERE authSource like :authSource",
				Long.class);
		query.setParameter("authSource", authSource);

		return query.getResultList();
	}

	public List<UserNotification> getNotifications(boolean webOnly, int limit) {
		JPAQuery q;
		if (webOnly) {
			q = UserNotification.find("user = ? AND (notification.displayType = ? OR notification.displayType = ?) "
					+ "ORDER BY modified DESC", this, DisplayType.WEB, DisplayType.BOTH);
		} else {
			q = UserNotification.find("user = ? ORDER BY notification.modified DESC", this);
		}

		if (limit > 0) {
			return q.fetch(limit);
		}
		return q.fetch();
	}

	public List<UserNotification> getEmailNotifications(Date oldestPossibleDate) {
		List<UserNotification> result = new ArrayList<UserNotification>();

		List<UserNotification> notifications = UserNotification
				.find("isRead = false AND user = ? AND (notification.displayType = ? OR notification.displayType = ?) AND created > ? ORDER BY notification.modified DESC",
						this, DisplayType.EMAIL, DisplayType.BOTH, oldestPossibleDate).fetch();

		// filter by email settings
		for (UserNotification un : notifications) {
			if (notSettings.isWanted(un.notification)) {
				result.add(un);
			}
		}

		return result;
	}

	/**
	 * Returns the amount of unread notifications since the last time the user requested those. A request is triggered,
	 * when the user opens the dropdown in the navbar. The returned count notifications is shown as a badge in the main
	 * navigation bar.
	 * <p/>
	 * If the user never requested the notifications before, the request can be null and in this case we return all
	 * unread notifications.
	 */
	public long getUnreadNotificationsCount() {
		if (lastNotificationRequest == null) {
			return UserNotification.count("user = ? AND isRead = false", this);
		}
		return UserNotification.count("user = ? AND isRead = false AND modified > ?", this, lastNotificationRequest);
	}

	public List<UserNotification> getUnreadNotifications() {
		return UserNotification.find("user = ? AND isRead = false ORDER BY notification.modified DESC", this).fetch();
	}

	public void resetUnreadNotificationCount() {
		lastNotificationRequest = new Date();
		save();
	}

	public Date getLastActivityStreamUpdate() {
		// for existing installations
		if (lastActivityStreamUpdate == null) {
			lastActivityStreamUpdate = new Date();
		}

		return lastActivityStreamUpdate;
	}

	/**
	 * @return Pages that the user is interested in and that the user can see (list).
	 */
	public List<Page> getPages(Integer max) {
		if (max != null) {
			return getPages(0, max);
		}
		return getPages(null, null);
	}

	/**
	 * @return Pages that the user is interested in and that the user can see (list).
	 */
	public List<Page> getPages(Integer page, Integer length) {
		return pageDao.fetchSortedUserPages(this, page, length, null, null);
	}

	/**
	 * Returns the count of all posts of this user. All post classes with the annotation {@link ExcludeFromWall} are
	 * excluded.
	 */
	public long getPostCount() {
		JpqlSelect select = getPostsSelect(false);
		return Post.count(select.toString(), select.getParams().toArray());
	}

	/**
	 * Returns all posts of this user as JPAQuery, including the posts on the user's wall. All post classes with the
	 * annotation {@link ExcludeFromWall} are excluded. Posts are ordered by id descending (creation). Posts with the
	 * "important" flag set are located at the beginning of the list.
	 */
	public JPAQuery getWallPostsQuery() {
		JpqlSelect select = getPostsSelect(true);
		select.orderBy("p.id DESC");
		return Post.find(select.toString(), select.getParams().toArray());
	}

	/**
	 * Returns a JpqlSelect which can be used to retrieve all posts of this user. If no posts could be found an empty
	 * list is returned. In addition to all posts where the current user is set as the author, posts on the user's wall
	 * can be included as well by setting includeWallPosts to 'true'. All post classes with the annotation
	 * {@link ExcludeFromWall} are excluded.
	 *
	 * @param includeWallPosts
	 *            Determines whether posts on the users wall should be included.
	 * @return A list of the user's posts or an empty list.
	 */
	private JpqlSelect getPostsSelect(boolean includeWallPosts) {
		JpqlSelect select = new JpqlSelect();
		select.from("Post p");

		/*
		 * create new WHERE statement to filter for posts of this user and include wall posts if requested
		 */
		Where where = select.where();
		where.where("p.author = ?").param(this);
		if (includeWallPosts) {
			where.orWhere("p.wall = ?").param(getDefaultWall());
		}
		select.where(where);

		// TODO: remove this and instead, add a new boolean flag to the Wall object: noWall
		/* get all classes annotated with @ExcludeFromWall */
		List<ApplicationClass> annotatedClasses = Play.classes.getAnnotatedClasses(ExcludeFromWall.class);

		/* filter all passed classes using a new WHERE statement */
		if (annotatedClasses.size() > 0) {
			Where whereType = select.where();
			for (ApplicationClass ac : annotatedClasses) {
				Logger.debug("User posts requested. Filtering post type: " + ac.javaClass.getCanonicalName());
				/*
				 * it would be desirable to pass the class as a parameter, but unfortunately due to a bug in Hibernate
				 * this is not working. Therefore we have to modify the JPQL string as such.
				 * 
				 * select.andWhere("TYPE(p) != ?").param(postClass);
				 */
				whereType.andWhere("TYPE(p) != " + ac.javaClass.getSimpleName());
			}
			select.andWhere(whereType);
		}

		return select;
	}

	public List<Event> getUpcomingEvents() {
		if (upcomingEvents == null) {
			upcomingEvents = Event.getUpcomingEventsForUser(this, 30);
		}
		return upcomingEvents;
	}

	/**
	 * Uses all upcoming events and groups them by their event series so that for each event series, only one event
	 * occurrence is returned.
	 *
	 * @return Upcoming events, grouped by event series.
	 */
	public List<Event> getMyEvents() {
		if (myEvents == null) {
			myEvents = new ArrayList<>();
			List<Long> eventSeries = new ArrayList<>();

			for (Event event : getUpcomingEvents()) {
				if (event instanceof EventSeriesChild) {
					EventSeriesChild esc = (EventSeriesChild) event;
					if (eventSeries.contains(esc.series.id)) {
						continue;
					} else {
						eventSeries.add(esc.series.id);
					}
				}

				myEvents.add(event);
			}
		}
		return myEvents;
	}

	/**
	 * Gathers all the user's relevant posts into a single activity stream list.
	 *
	 * @return Gathered list of unique posts sorted by date.
	 */
	public List<Post> getActivityStream(models.User.ActivityStreamSelect.DisplayType displayType) {
		return getActivityStream(displayType, null, null);
	}

	public List<Post> getActivityStream(models.User.ActivityStreamSelect.DisplayType displayType, Integer page,
			Integer length) {
		ActivityStreamSelect activityStream = new ActivityStreamSelect(this, displayType);
		activityStream.before = lastActivityStreamUpdate;

		if (page != null && length != null && POST_MONTH_THRESHOLD != null && POST_MONTH_THRESHOLD > 0) {
			if (activityStream.after == null) {
				activityStream.after = new DateTime().minusMonths(POST_MONTH_THRESHOLD).toDate();
			}
		}
		return activityStream.fetch(page, length);
	}

	public List<Post> getActivityStream(Date after) {
		ActivityStreamSelect activityStream = new ActivityStreamSelect(this);
		activityStream.after = after;
		return activityStream.fetch();
	}

	public Long getActivityStreamCount(models.User.ActivityStreamSelect.DisplayType displayType) {
		return new ActivityStreamSelect(this, displayType).count();
	}

	/**
	 * Get the list of new posts on the user's activity stream. Using LAST_ACTIVITY_STREAM_UPDATE_DAYS_MAX as threshold
	 * when fetching new posts.
	 *
	 * @param update
	 *            True to update the activity stream and clear the list of new posts
	 * @return List of new posts
	 */
	public List<Post> getNewActivityStream(boolean update) {
		final DateTime dt = new DateTime().minusDays(LAST_ACTIVITY_STREAM_UPDATE_DAYS_MAX);
		final Date afterThreshold;

		if (new DateTime(lastActivityStreamUpdate).isBefore(dt)) {
			afterThreshold = dt.toDate();
		} else {
			afterThreshold = lastActivityStreamUpdate;
		}

		// update AFTER query to use old value for time threshold!
		if (update) {
			// add some seconds to prevent simultaneous posts/updates
			lastActivityStreamUpdate = DateUtils.addSeconds(new Date(), 5);
			save();
		}

		return getActivityStream(afterThreshold);
	}

	public long getNewActivityStreamCount() {
		ActivityStreamSelect activityStream = new ActivityStreamSelect(this);
		activityStream.after = lastActivityStreamUpdate;
		return activityStream.count();
	}

	/**
	 * Fetches the currently online users sorted by the followed users first.
	 *
	 * @param max
	 *            Null for unlimited.
	 * @return List of currently online users.
	 * @deprecated See {@link OnlineStatusService}
	 */
	@Deprecated
	public List<User> getOnlineUsers(Integer max) {
		return ModelIdBinder.bind(User.class,
				OnlineStatusServiceFactory.getOnlineStatusService(tenant).getOnlineUserIds());
	}

	/**
	 * @deprecated See {@link OnlineStatusService}
	 */
	@Deprecated
	public static long getOnlineUserCount() {
		return OnlineStatusServiceFactory.getOnlineStatusService().getOnlineUserCount();
	}

	public UserOnlineStatus getOnlineStatus() {
		return OnlineStatusServiceFactory.getOnlineStatusService(tenant).getOnlineStatus(this);
	}

	public boolean isOnline() {
		// handing over tenant is essential here for EventCleanJob to work
		return OnlineStatusServiceFactory.getOnlineStatusService(tenant).getOnlineStatus(this).isOnline();
	}

	public Date getBirthday() {
		int year = new DateTime().getYear();

		DateTime d = new DateTime(birthdate);
		d = d.withYear(year).withHourOfDay(23).withMinuteOfHour(59);
		if (d.isAfterNow()) {
			return d.toDate();
		}
		return d.withYear(year + 1).toDate();
	}

	public String getFormattedBirthdate() {
		DateTime date = new DateTime(birthdate);
		if (date.year().get() == BIRTHDATE_YEAR_NOT_SET) {
			return date.toString(libs.DateI18N.getLocalizedDateFormat("date.format.short"));
		}
		return date.toString(libs.DateI18N.getLocalizedDateFormat("date.format"));
	}

	public static Collection<User> getUpcomingBirthdays(int dayRange, int max) {
		List<DateTime> possibleDays = new ArrayList<DateTime>();
		for (int i = 0; i <= dayRange; i++) {
			DateTime date = new DateTime().plusDays(i);
			possibleDays.add(date);
		}

		JpqlSelect select = new JpqlSelect();
		select.from("User u");
		select.where("u.active = true AND u.birthdate IS NOT NULL");

		Where where = select.where();
		for (DateTime day : possibleDays) {
			where.orWhere("(MONTH(u.birthdate) = ? AND DAY(u.birthdate) = ?)").param(day.getMonthOfYear())
					.param(day.getDayOfMonth());
		}

		select.andWhere(where);

		final List<User> users = find(select.toString(), select.getParams().toArray()).fetch(max);

		Collections.sort(users, new Comparator<User>() {
			final Calendar nowCal = Calendar.getInstance();
			final int currentYear = nowCal.get(Calendar.YEAR);

			@Override
			public int compare(User user1, User user2) {
				final Calendar cal1 = new GregorianCalendar();
				cal1.setTime(user1.birthdate);
				cal1.set(Calendar.YEAR, currentYear);
				if  (cal1.before(nowCal)) {
					cal1.set(Calendar.YEAR, currentYear +1);
				}

				final Calendar cal2 = new GregorianCalendar();
				cal2.setTime(user2.birthdate);
				cal2.set(Calendar.YEAR, currentYear);
				if  (cal2.before(nowCal)) {
					cal2.set(Calendar.YEAR, currentYear +1);
				}

				if (cal1.before(cal2)) {
					return -1;
				} else if (cal1.after(cal2)) {
					return 1;
				} else {
					return 0;
				}
			}
		});

		return users;
	}

	@Override
	public String getDisplayName() {
		return nameBuilder.getFullName(this);
	}

	@Override
	public <T extends JPABase> T delete() {
		leaveAllGroups();
		deleted = true;
		active = false;
		return save();
	}

	@Override
	public void _delete() {
		leaveAllGroups();

		// clear all single-sided many-to-many relations
		ClearManyToManyHelper.clearAll(this);

		super._delete();
	}

	private void leaveAllGroups() {
		// clear groups
		for (UserGroup group : groups) {
			group.users.remove(this);
			group.save();
		}
	}

	@Override
	public Wall getDefaultWall() {
		// create default wall
		if (walls.size() == 0) {
			Wall.inject(this);
		}
		return walls.get(0);
	}

	// TODO This should not be public! Check whether this is used by any adaptation projects.
	public JpqlSelect getActiveWorkspacesQuery() {
		JpqlSelect select = new JpqlSelect();
		select.from("Workspace w")
				.where("archived = false AND EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace.id = w.id AND wm.user.id = ?)")
				.param(id);
		return select;
	}

	public long getActiveWorkspaceCount() {
		JpqlSelect select = getActiveWorkspacesQuery();
		return Workspace.count(select.toString(), select.getParams().toArray());
	}

	/**
	 * @deprecated Use {@linkplain WorkspaceDao#getWorkspacesWhereUserIsMember(int, int, User, String, boolean)}
	 *             instead.
	 */
	@Deprecated
	public List<Workspace> getActiveWorkspaces(Integer max) {
		JpqlSelect select = getActiveWorkspacesQuery();
		JPAQuery query = Workspace.find(select.toString(), select.getParams().toArray());

		List<Workspace> workspaces;
		if (max != null) {
			workspaces = query.fetch(max);
		} else {
			workspaces = query.fetch();
		}

		// sort
		if (WorkspaceActivitySorter.isValidState()) {
			Collections.sort(workspaces, new WorkspaceActivitySorter());
		} else {
			Collections.sort(workspaces);
		}

		return workspaces;
	}

	/**
	 * @return IDs of the groups this user is member of.
	 */
	public List<Long> getGroupIds() {
		if (groupIds == null) {
			groupIds = new ArrayList<Long>();
			for (UserGroup group : groups) {
				groupIds.add(group.id);
			}
		}

		return groupIds;
	}

	/**
	 * Returns a list of existing values of the given field. Note that the passed field name must be valid and is
	 * compared to list of predefined field names. This method is designed to fail silently if an illegal argument is
	 * passed.
	 *
	 * @param field
	 *            Can be one of the following: "JOBTITLE", "OFFICE", "COMPANY", "DEPARTMENT"
	 * @return a list of existing values or an empty list, if no values exist or if the provided field is invalid.
	 */
	public static List<String> getFieldOptions(String field) {
		List<String> result = new ArrayList<>();
		UserFieldOption fieldOption;
		try {
			fieldOption = UserFieldOption.valueOf(field.toUpperCase());
		} catch (IllegalArgumentException e) {
			return result;
		}
		result = User.find(
				"SELECT DISTINCT u." + fieldOption.fieldName + " FROM User u WHERE u." + fieldOption.fieldName
						+ " IS NOT NULL ORDER BY u." + fieldOption.fieldName + " ASC").fetch();
		return result;
	}

	/**
	 * Returns all available values for the given field as JSON array. This is used to provide the user with a list of
	 * suggestions (basically values that other users did save). Note that the given options are returned HTML escaped.
	 */
	public static String getFieldOptionsJson(String field) {
		List<String> fieldOptions = getFieldOptions(field);
		List<String> result = Lists.transform(fieldOptions, new Function<String, String>() {
			@Override
			public String apply(String option) {
				return JavaExtensions.escapeHtml(option).toString();
			}
		});
		return new Gson().toJson(result);
	}

	/* TODO This should not be public! */
	public JpqlSelect getUserChooserSelect(boolean showExternals, boolean showMe, List<Long> selected, String searchTerm) {
		JpqlSelect select = new JpqlSelect();
		select.orderBy("lastName,firstName");
		select.where("active = true");

		if (!showExternals) {
			select.andWhere("external = false");
		}

		if (!showMe) {
			select.andWhere("id != ?").param(id);
		}

		// clean selected
		if (selected != null) {
			selected.remove(null);
		}

		// if external, only show workspace members
		if (external) {
			List<Long> allowedUsers = new ArrayList<Long>();
			for (WorkspaceMember wm : workspaces) {
				for (User u : wm.workspace.getUserMembers()) {
					allowedUsers.add(u.id);
				}
			}
			select.andWhere("id IN (" + Util.implode(allowedUsers, ",") + ")");
		}

		if (!StringUtils.isEmpty(searchTerm)) {
			// all name terms must match
			List<String> terms = Util.parseList(searchTerm, " ");
			for (String string : terms) {
				String part = "%" + string.toLowerCase() + "%";
				select.andWhere(
						"(LOWER(lastName) LIKE ? OR LOWER(firstName) LIKE ? OR LOWER(department) LIKE ? OR LOWER(jobTitle) LIKE ?)")
						.params(part, part, part, part);
			}
		} else {
			// show both followed and selected people, or none instead of all
			List<String> orParts = new ArrayList<String>();

			// followers
			if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
				orParts.add("id != -1");
			} else {
				List<Long> followed = JPA.em().createQuery("SELECT u.id FROM User u WHERE ? MEMBER OF u.followers")
						.setParameter(1, this).getResultList();
				if (showMe) {
					followed.add(id);
				}
				if (followed != null && followed.size() > 0) {
					orParts.add("id IN (" + Util.implode(followed, ",") + ")");
				}
			}

			if (selected != null && selected.size() > 0) {
				// selected users
				orParts.add("id IN (" + Util.implode(selected, ",") + ")");
			}

			if (orParts.size() > 0) {
				select.andWhere("(" + Util.implode(orParts, " OR ") + ")");
			} else {
				// show no users
				select.andWhere("true = false");
			}
		}

		return select;
	}

	@Override
	public List<User> getAdmins() {
		return Arrays.asList(this);
	}

	@Override
	public boolean isSharable() {
		return true;
	}

	public long getMediaCount() {
		return Media.count("collection.room.sender = ?", this);
	}

	public String getPrivateCacheKey() {
		return Codec.hexMD5(id + ":" + language + ":" + superadminMode);
	}

	public long getUnreadConversationCount() {
		return ConversationMember
				.count("SELECT COUNT(cm) FROM ConversationMember cm WHERE cm.user = ? AND cm.status = ? AND (cm.lastAccess < cm.conversation.lastPost OR cm.lastAccess IS NULL)",
						this, ConversationStatus.INBOX);
	}

	public List<ConversationMember> getUnreadConversations(Date since) {
		return ConversationMember
				.find("SELECT cm FROM ConversationMember cm WHERE cm.user = ? AND cm.status = ? AND (cm.lastAccess < cm.conversation.lastPost OR cm.lastAccess IS NULL) AND cm.conversation.lastPost > ?",
						this, ConversationStatus.INBOX, since).fetch();
	}

	public List<ConversationMember> getActiveConversations() {
		return ConversationMember.find("active = true AND user = ? AND status = ? ORDER BY conversation.lastPost DESC",
				this, ConversationStatus.INBOX).fetch();
	}

	public List<ConversationMember> getConversations(ConversationStatus status) {
		return getConversations(status, null);
	}

	public List<ConversationMember> getConversations(ConversationStatus status, Integer max) {
		final User user = UserLoader.getConnectedUser();
		final String sql = "status = ? AND user = ? AND status != ? ORDER BY conversation.lastPost DESC";
		final JPAQuery query = ConversationMember.find(sql, status, user, ConversationStatus.DELETED);
		if (max != null) {
			return query.fetch(max);
		}
		return query.fetch();
	}

	public List<Long> getActiveConversationsUserIds() {
		return em()
				.createQuery(
						"SELECT DISTINCT(user.id) FROM ConversationMember WHERE user != :user AND (active = true OR status = :status) AND conversation IN (SELECT conversation.id FROM ConversationMember WHERE user = :user)")
				.setParameter("user", this).setParameter("status", ConversationStatus.INBOX).getResultList();
	}

	public List<ConversationMember> getUpdatedConversations(Date since) {
		return ConversationMember
				.find("SELECT cm FROM ConversationMember cm WHERE cm.conversation.lastPost > ? AND cm.user = ? AND status != ?",
						since, this, ConversationStatus.INVISIBLE).fetch();
	}

	public long getConversationCount(ConversationStatus status) {
		return ConversationMember.count("status = ? AND user = ?", status, this);
	}

	public boolean isInSameWorkspaceWith(User otherUser) {
		String sql2 = "SELECT COUNT(*) FROM Workspace ws " + "WHERE EXISTS"
				+ "(SELECT wm1 FROM WorkspaceMember wm1 WHERE wm1.user = :user1 AND wm1 MEMBER OF ws.members) "
				+ "AND EXISTS "
				+ "(SELECT wm2 FROM WorkspaceMember wm2 WHERE wm2.user = :user2 AND wm2 MEMBER OF ws.members)";

		TypedQuery<Long> q = em().createQuery(sql2, Long.class);
		q.setParameter("user1", this);
		q.setParameter("user2", otherUser);

		return q.getSingleResult() > 0; // Long returned as this is a typed query
	}

	public static User findByEmail(final String email) {
		boolean isExternalFilterActive = ((org.hibernate.Session) JPA.em().getDelegate())
				.getEnabledFilter(Sender.EXTERNAL) != null;
		boolean isSoftDeleteFilterActive = ((org.hibernate.Session) JPA.em().getDelegate())
				.getEnabledFilter(Sender.SOFT_DELETE) != null;

		try {
			if (isExternalFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			}
			if (isSoftDeleteFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
			}

			String sql2 = "SELECT u FROM User u WHERE u.email LIKE :eMail";
			TypedQuery<User> q = em().createQuery(sql2, User.class);
			q.setParameter("eMail", email);

			// Long returned as this is a typed query
			return q.getSingleResult();

		} catch (NoResultException ex) {
			return null;

		} finally {
			if (isExternalFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.EXTERNAL);
			}
			if (isSoftDeleteFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);
			}
		}
	}

	public static List<User> findSuperadmins() {
		return User.find("superadmin = true AND active = true").fetch();
	}

	public static class ActivityStreamSelect {
		public static String FLAGGED_POST_COUNT_CACHE_KEY = "FLAGGED_POST_CNT";

		public enum DisplayType {
			NATURAL("icon-chevron-down"), TOP_POSTS("icon-star"), FLAGGED_POSTS("icon-flag", false, true, false);

			public String icon;
			public boolean showForm = true;
			public boolean showCount = false;
			public boolean showActivities = true;

			private DisplayType(String icon) {
				this.icon = icon;
			}

			private DisplayType(String icon, boolean showForm, boolean showCount, boolean showActivities) {
				this.icon = icon;
				this.showForm = showForm;
				this.showCount = showCount;
				this.showActivities = showActivities;
			}

			public String getLabel() {
				return Messages.get("activitystream.sorting." + toString().toLowerCase());
			}

			public Long getCount(User user) {
				return user.getActivityStreamCount(this);
			}

			/**
			 * Those display types that are sticky and remembered in the session.
			 */
			public static List<DisplayType> sticky() {
				return Arrays.asList(NATURAL, TOP_POSTS);
			}

			/**
			 * Those display types that are only displayed a single time without stickyness.
			 */
			public static List<DisplayType> extra() {
				return Arrays.asList(FLAGGED_POSTS);
			}
		}

		private User user;
		private DisplayType displayType = DisplayType.NATURAL;

		public Date after;
		public Date before;

		/**
		 * Prepare select<br>
		 * - list of senderIds<br>
		 * -> of important pages<br>
		 * -> of followed pages<br>
		 * -> of workspaces the user is active in<br>
		 * - list of {@link Wall}<br>
		 * -> own wall<br>
		 * -> application wall<br>
		 * -> all walls of events where user is {@link EventUser} and he wants to attend (or at least maybe)<br>
		 * -> all {@link WallApp} of all senderIds<br>
		 * -> all walls of followed users<br>
		 * <br>
		 * - only walls that are noActivityStream = false<br>
		 * - only posts that are not {@link ConversationPost}
		 */
		public ActivityStreamSelect(User user) {
			this(user, DisplayType.NATURAL);
		}

		public ActivityStreamSelect(User user, DisplayType displayType) {
			this.user = user;
			this.displayType = displayType;
		}

		/**
		 * Fetch all {@link Post}<br>
		 * - created <= before if set<br>
		 * - created > after if set<br>
		 * - filter by permissions to access posts<br>
		 * - sort by comparator if set
		 *
		 * @return
		 */
		public List<Post> fetch() {
			return fetch(null, null);
		}

		public List<Post> fetch(Integer page, Integer length) {
			return UserDao.fetchPosts(user, displayType, before, after, page, length);
		}

		public Long count() {
			if (displayType == DisplayType.FLAGGED_POSTS) {
				Long count = (Long) play.cache.Cache.get(user.id + FLAGGED_POST_COUNT_CACHE_KEY);
				if (count != null) {
					return count;
				}
				count = UserDao.countFlaggedPosts(user);
				play.cache.Cache.set(user.id + FLAGGED_POST_COUNT_CACHE_KEY, count);
				return count;
			}
			return UserDao.countPosts(user, before, after);
		}
	}

	public void setUserNotificationSettings(UserNotificationSettings defaultNotificationSettings) {
		this.notSettings.mailEvents = new ArrayList<>(defaultNotificationSettings.mailEvents);
		this.notSettings.receiveMails = defaultNotificationSettings.receiveMails;
		this.notSettings.mailInterval = defaultNotificationSettings.mailInterval;
	}

	@Override
	public boolean checkSearchPermission() {
		return super.checkSearchPermission() && !external;
	}

	@Override
	public String getSearchInfoText() {
		String r = department;
		if (external) {
			r += " (" + Messages.get("user.external") + ")";
		}
		return r;
	}

	/**
	 * The total number of unread / unseen items. This currently includes notifications, conversations and posts.
	 */
	public long getTotalUnreadCount() {
		return getUnreadConversationCount() + getUnreadNotificationsCount() + getNewActivityStreamCount();
	}

	@Override
	public String getVisibilityIcon() {
		return "globe";
	}
}

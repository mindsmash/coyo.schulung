package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import models.notification.CallNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareCallPost;
import play.i18n.Messages;

@Entity
@Table(name = Call.CALL_TABLE_NAME)
public class Call extends TenantModel implements Sharable {

	public enum Status {
		DECLINED, CANCELED, OK
	}
	
	public static final String CALL_TABLE_NAME = "videocall";
	private static final String PARTICIPANT_USER_TABLE_NAME = "videocall_participant";
	
	public Status status;
	
	@ManyToOne(optional = false)
	public User host;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "call")
	public List<CallNotification> notifications = new ArrayList<CallNotification>();
	
	@ManyToMany
	@JoinTable(name = PARTICIPANT_USER_TABLE_NAME)
	public Set<User> participants;
	
	@Lob
	public String summary;
	
	public String contextUid;
	
	public boolean isOutgoing(User user) {
		return (user.id == host.id);
	}
	
	public boolean isIncoming(User user) {
		return participants.contains(user);
	}

	public List<User> getOtherUser(User connectedUser) {
		if (connectedUser.id == host.id) {
			return new ArrayList<User>(participants);
		}
		List<User> result = new ArrayList<User>();
		result.add(host);
		for (User participant : participants) {
			if (participant.id != connectedUser.id) {
				result.add(participant);
			}
		}
		return result;
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareCallPost post = new ShareCallPost();
		post.author = user;
		post.wall = wall;
		post.call = this;
		post.save();
		return post;
	}

	@Override
	public String getDisplayName() {
		return Messages.get("calls.displayname");
	}

	@Override
	public boolean isSharable() {
		return true;
	}
	
}
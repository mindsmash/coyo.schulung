package models.media;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Table;

import models.Sender;
import models.User;
import play.i18n.Messages;
import play.mvc.Router;
import storage.FlexibleBlob;
import theme.Theme;
import utils.VideoUtils;
import utils.VideoUtils.VideoType;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Table(name = "media_video")
@Searchable(category = "mediaItem")
public class MediaVideo extends Media {

	public static MediaVideo createVideo(MediaCollection collection, User creator, String filename, FlexibleBlob data) {
		if (!VideoUtils.isVideo(data.type())) {
			throw new IllegalArgumentException("You are trying to create a MediaVideo with an unsupported video file: "
					+ data.type());
		}

		MediaVideo media = new MediaVideo();
		media.collection = collection;
		media.filename = filename;
		media.creator = creator;
		media.file = data;
		media.save();
		return media;
	}

	@Override
	public void generateVariants() {
		// TODO: generate thumb using ffmpeg
		// TODO: generate other video formats using ffmpeg
	}

	@Override
	public String getThumbUrl() {
		return Theme.getResourceUrl("images/media-video-nothumb.png");
	}

	@Override
	public void jsonSerializeCustomFields(JsonObject o, User connectedUser, JsonSerializationContext context) {
		// required for mediaelementjs
		o.addProperty("mediaElementFlashFallback", Theme.getResourceUrl("libs/mediaelementjs/flashmediaelement.swf"));
		
		o.addProperty("posterUrl", Theme.getResourceUrl("images/media-video-noposter.png"));
		
		JsonArray sources = new JsonArray();
		for(VideoType vt : VideoType.values()) {
			if (getVariant(vt) != null) {
				JsonObject source = new JsonObject();
				source.addProperty("type", vt.getMimeType());
				source.addProperty("src", getVariantUrl(vt));
				sources.add(source);
			}		
		}
		o.add("sources", sources);
	}

	/**
	 * Generates an URL to a specific video variant/format. This method does not check if the variant exists.
	 * 
	 * @param type
	 * @return URL string.
	 */
	private String getVariantUrl(VideoType type) {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("type", type.getFileSuffix());
		return Router.reverse("api.MediaAPI.video", args).url;
	}

	public FlexibleBlob getVariant(VideoType type) {
		if (type.getMimeType().equals(file.type())) {
			return file;
		}
		return null;
	}

	@Override
	public String getTypeKey() {
		return "video";
	}

	@Override
	public String getTypeName() {
		return Messages.get("media.video");
	}

	@Override
	public Sender getSender() {
		return this.collection.room.sender;
	}
}

package models.media;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Sender;
import models.Sharable;
import models.User;
import models.app.MediaApp;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareMediaCollectionPost;
import play.data.binding.NoBinding;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import security.CheckCaller;
import theme.Theme;
import acl.Permission;
import controllers.Users;
import controllers.WebBaseController;
import fishr.Field;
import fishr.Indexed;
import fishr.Related;
import fishr.search.Searchable;

/**
 * Represents a collection of {@link Media}.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Indexed
@Table(name = "media_collection")
@Searchable(category = "mediaCollections")
public class MediaCollection extends ExtendedTenantModel implements Sharable, ExtendedSearchableModel {

	@ManyToOne(optional = false)
	public MediaRoom room;

	@MaxSize(255)
	public String uid;

	@Required
	@MaxSize(50)
	@Field
	public String name;

	@OrderBy("priority")
	@OneToMany(mappedBy = "collection", cascade = CascadeType.ALL)
	public List<Media> media = new ArrayList<Media>();

	@NoBinding
	@OneToMany(mappedBy = "mediaCollection", cascade = CascadeType.ALL)
	public List<ShareMediaCollectionPost> posts = new ArrayList<ShareMediaCollectionPost>();

	@Transient
	private Media cover;

	public int priority = 9999;

	/**
	 * @return The cover, if none is set the first {@link Media} in the collection.
	 */
	public Media getCover() {
		if (cover == null) {
			cover = Media.find("collection = ? AND cover = true", this).first();

			if (cover == null && media.size() > 0) {
				cover = media.get(0);
			}
		}
		return cover;
	}

	public String getCoverUrl() {
		Media cover = getCover();
		if (cover == null) {
			return Theme.getResourceUrl("images/media-collection-nocover.png");
		}
		return cover.getThumbUrl();
	}

	@Override
	@PreRemove
	protected void beforeDelete() throws Exception {
		// fix cover foreign key constraint
		this.cover = null;
		willBeSaved = true;

		super.beforeDelete();
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareMediaCollectionPost post = new ShareMediaCollectionPost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.mediaCollection = this;
		post.save();
		return post;
	}

	@Override
	public void redirectToResult() {
		if (room.sender instanceof User) {
			Users.media(room.sender.id, room.sender.getSlug());
		}

		MediaApp app = MediaApp.find("room = ?", room).first();
		Long appId = (app != null) ? app.id : null;

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("collectionId", id);

		WebBaseController.redirectToSender(room.sender, appId, args);
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public Sender getSearchThumbSender() {
		return room.sender;
	}

	@Override
	public String getSearchInfoText() {
		return null;
	}

	@Override
	public boolean isSharable() {
		return room.sender.isSharable();
	}

	@Override
	public boolean checkSearchPermission() {
		return CheckCaller.check(Permission.ACCESS_MEDIA_ROOM, room);
	}

	public Long getMediaCount() {
		return Media.count("collection = ?", this);
	}

	@Related
	public String getRelatedSearchInfo() {
		return room.sender.getDisplayName();
	}
}

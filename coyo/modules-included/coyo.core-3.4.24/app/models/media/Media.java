package models.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import jobs.GenerateMediaVariants;
import json.media.MediaSerializer;
import models.BaseModel;
import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Likable;
import models.Sender;
import models.Sharable;
import models.User;
import models.app.MediaApp;
import models.comments.Comment;
import models.comments.Commentable;
import models.helper.ClearManyToMany;
import models.notification.MediaCommentNotification;
import models.notification.MediaNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareMediaPost;
import multitenancy.MTA;

import org.apache.commons.lang.StringUtils;

import play.cache.Cache;
import play.data.validation.MaxSize;
import play.db.jpa.JPA;
import play.mvc.Router;
import plugins.play.JobExecutorPlugin;
import security.CheckCaller;
import storage.FlexibleBlob;
import theme.Theme;
import acl.Permission;

import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import controllers.Users;
import controllers.WebBaseController;
import fishr.Field;
import fishr.Related;

/**
 * Represents a media item.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "media")
public abstract class Media extends ExtendedTenantModel implements Commentable, Likable, Sharable,
		ExtendedSearchableModel {

	public static final int THUMB_WIDTH = 140;

	private static final String MEDIA_LIKE_TABLE = "media_like";
	public static final String MEDIA_COMMENT_TABLE = "media_comment";

	protected static final String NO_THUMB_IMG = "images/media-nothumb.png";

	@ManyToOne(optional = false)
	public MediaCollection collection;

	@ManyToOne(optional = false)
	public User creator;

	public FlexibleBlob file;
	public FlexibleBlob thumbnail;

	@Field
	@MaxSize(255)
	public String filename;

	@Field
	@MaxSize(255)
	public String title;

	public boolean cover;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = MEDIA_COMMENT_TABLE)
	public List<Comment> comments = new ArrayList<Comment>();

	@ManyToMany
	@JoinTable(name = MEDIA_LIKE_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> likes = new ArrayList<User>();

	@OneToMany(mappedBy = "media", cascade = CascadeType.ALL)
	public List<ShareMediaPost> posts = new ArrayList<ShareMediaPost>();

	@OneToMany(mappedBy = "media", cascade = CascadeType.ALL)
	public List<MediaNotification> notifications = new ArrayList<>();

	public int priority = 9999;

	public String getThumbUrl() {
		if (thumbnail != null && thumbnail.exists()) {
			Map<String, Object> args = new HashMap<>();
			args.put("id", id);
			return Router.reverse("api.MediaAPI.thumbnail", args).url;
		}
		return Theme.getResourceUrl(NO_THUMB_IMG);
	}

	public String getDownloadUrl() {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		return Router.reverse("api.MediaAPI.download", args).url;
	}

	/**
	 * Indicates if a variants generation process is ongoing.
	 * 
	 * @return True if process is ongoing.
	 */
	public boolean isGeneratingVariants() {
		Boolean r = Cache.get("media-" + id + "-generatingVariants", Boolean.class);
		return (r != null && r);
	}

	/**
	 * Sets the status of the variants generation process. Any active status is retained for 5 minutes.
	 * 
	 * @param active
	 */
	public void markGeneratingVariants(boolean active) {
		Cache.set("media-" + id + "-generatingVariants", active, "5min");
	}

	/**
	 * @return Returns the, if existing, next media item in the collection, other ways the first in the collection.
	 */
	public Media getNext() {
		try {
			return collection.media.get(collection.media.indexOf(this) + 1);
		} catch (IndexOutOfBoundsException ignored) {
			return collection.media.get(0);
		}
	}

	/**
	 * @return Returns the, if existing, previous media item in the collection, other ways the last in the collection.
	 */
	public Media getPrevious() {
		try {
			return collection.media.get(collection.media.indexOf(this) - 1);
		} catch (IndexOutOfBoundsException ignored) {
			return collection.media.get(collection.media.size() - 1);
		}
	}

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();

		// create user notifications
		MediaCommentNotification.raise(this, comment);
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		save();

		// cleanup user notifications
		MediaCommentNotification.cleanup(this);
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public void like(User user) {
		likes.add(user);
		save();
	}

	@Override
	public void unlike(User user) {
		likes.remove(user);
		save();
	}

	@Override
	public boolean likes(User user) {
		return likes.contains(user);
	}

	@Override
	public List<User> getLikes() {
		return likes;
	}

	@Override
	public void _save() {
		super._save();

		// make sure only one media item is the cover
		if (cover) {
			JPA.em().createQuery("UPDATE Media m SET m.cover = false WHERE m.id != ? AND m.collection = ?")
					.setParameter(1, id).setParameter(2, collection).executeUpdate();
		}
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + MEDIA_COMMENT_TABLE + " WHERE media_id = " + id).executeUpdate();
		JPA.em().createNativeQuery("DELETE FROM " + MEDIA_LIKE_TABLE + " WHERE media_id = " + id).executeUpdate();

		super.beforeDelete();
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareMediaPost post = new ShareMediaPost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.media = this;
		post.save();
		return post;
	}

	@Override
	public void redirectToResult() {
		MediaRoom room = this.collection.room;

		if (room.sender instanceof User) {
			Users.media(room.sender.id, room.sender.getSlug());
		}

		MediaApp app = MediaApp.find("room = ?", room).first();
		Long appId = (app != null) ? app.id : null;

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("collectionId", collection.id);
		args.put("mediaId", id);

		WebBaseController.redirectToSender(room.sender, appId, args);

	}

	@Override
	public String getDisplayName() {
		if (StringUtils.isNotEmpty(title)) {
			return title;
		}
		return filename;
	}

	@Override
	public Sender getSearchThumbSender() {
		return collection.room.sender;
	}

	@Override
	public String getSearchInfoText() {
		return null;
	}

	@Override
	public boolean isSharable() {
		return collection.room.sender.isSharable();
	}

	@Override
	protected void afterDelete() throws Exception {
		super.afterDelete();

		// delete attachment
		thumbnail.delete();
		file.delete();
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	@Override
	public boolean checkLikePermission(User user) {
		return CheckCaller.check(Permission.LIKE_MEDIA, this);
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return CheckCaller.check(Permission.COMMENT_MEDIA, this);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return CheckCaller.check(Permission.EDIT_MEDIA_ROOM, this.collection.room.id);
	}

	@Override
	public Collection<User> getInterestedUsers() {
		return collection.room.sender.followers;
	}

	@Override
	public boolean checkSearchPermission() {
		return CheckCaller.check(Permission.ACCESS_MEDIA_ROOM, this.collection.room);
	}

	@Related
	public String getRelatedSearchInfo() {
		return collection.room.sender.getDisplayName();
	}

	@Field
	public List<String> getRelatedSearchComments() {
		return Comment.getSearchInfo(comments);
	}

	@Override
	protected void afterInsert() throws Exception {
		// mark the generating thumbs job as active when inserting so that the
		// UI waits for thumbs and such to be generated
		markGeneratingVariants(true);

		// do the thumb generation in a job with a short delay to allow JPA
		// to commit
		JobExecutorPlugin.addJob(new GenerateMediaVariants(MTA.getActiveTenant(), id));

		super.afterInsert();
	}

	/**
	 * Generates the thumbnail (width=THUMB_WIDTH) as well as any other additional variants of this media (e.g. other
	 * image or video formats).
	 */
	abstract public void generateVariants();

	/**
	 * Add class specific JSON data to the base JSON generated by {@link MediaSerializer}.
	 * 
	 * @param o
	 */
	abstract public void jsonSerializeCustomFields(JsonObject o, User connectedUser, JsonSerializationContext context);

	/**
	 * @return Key for this type
	 */
	abstract public String getTypeKey();

	/**
	 * @return Display name for this type
	 */
	abstract public String getTypeName();
}

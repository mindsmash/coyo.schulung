package models.media;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.Sender;
import models.TenantModel;

@Entity
@Table(name = "media_room")
public class MediaRoom extends TenantModel {

	@ManyToOne(optional = false)
	public Sender sender;

	public boolean allowGuestUpload;

	@OrderBy("priority")
	@OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
	public List<MediaCollection> collections = new ArrayList<MediaCollection>();

	public MediaRoom(Sender sender) {
		this.sender = sender;
	}

	public MediaCollection getCollection(String id) {
		return MediaCollection.find("id = ? AND room = ?", Long.valueOf(id), this).first();
	}

	public MediaCollection getCollectionByUid(String uid) {
		return MediaCollection.find("uid = ? AND room = ?", uid, this).first();
	}
}

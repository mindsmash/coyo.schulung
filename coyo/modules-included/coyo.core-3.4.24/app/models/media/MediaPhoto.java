package models.media;

import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import fishr.Indexed;
import fishr.search.Searchable;
import models.Sender;
import models.User;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Router;
import storage.FlexibleBlob;
import utils.ImageUtils;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Entity
@Indexed
@Table(name = "media_photo")
@Searchable(category = "mediaItem")
public class MediaPhoto extends Media {

	public static final int PREVIEW_WIDTH = 500;

	public static enum Orientation {
		PORTRAIT, LANDSCAPE;
	}

	@Enumerated(EnumType.STRING)
	public Orientation orientation = Orientation.PORTRAIT;

	public FlexibleBlob preview;

	public String getPreviewUrl() {
		if (preview != null && preview.exists()) {
			return getRenderUrl(true);
		}
		return getPhotoUrl();
	}

	public String getPhotoUrl() {
		return getRenderUrl(false);
	}

	private String getRenderUrl(boolean preview) {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("preview", preview);
		return Router.reverse("api.MediaAPI.photo", args).url;
	}

	public Orientation getOrientation() {
		// backwards null-safe compatibility
		if (orientation == null) {
			orientation = Orientation.PORTRAIT;
		}
		return orientation;
	}

	@Override
	protected void beforeSave() throws Exception {
		if (file != null && file.exists()) {
			try {
				BufferedImage img = ImageUtils.read(file.get(), file.type());
				if (img != null) {
					orientation = (img.getWidth() > img.getHeight()) ? Orientation.LANDSCAPE : Orientation.PORTRAIT;
				}
			} catch (Exception e) {
				Logger.debug(e, "[MediaPhoto] Error determining orientation for photo %s", this);
				Logger.warn("[MediaPhoto] Error determining orientation for photo %s", this);
			}
		}

		super.beforeSave();
	}

	public static MediaPhoto createPhoto(MediaCollection collection, User creator, String filename, FlexibleBlob data) {
		// prepare
		MediaPhoto media = new MediaPhoto();
		media.collection = collection;
		media.filename = filename;
		media.creator = creator;
		media.file = data;
		media.save();
		return media;
	}

	@Override
	public void generateVariants() {
		try {
			final BufferedImage rotatedImage = ImageUtils.rotateImage(file.asFile());
			thumbnail = new FlexibleBlob();
			preview = new FlexibleBlob();

			if (rotatedImage != null) {
				try (final InputStream is = ImageUtils.createInputStreamFromBufferedImage(rotatedImage, file.type())) {
					file.set(is, file.type(), file.length());
					is.reset();
					ImageUtils.setThumb(is, file.type(), thumbnail, Media.THUMB_WIDTH);
					is.reset();
					ImageUtils.scaleToWidth(is, file.type(), preview, PREVIEW_WIDTH, false);

					save();
					return;
				}
			}

			try (InputStream is = file.get()) {
				ImageUtils.setThumb(is, file.type(), thumbnail, Media.THUMB_WIDTH);
			}
			try (InputStream is = file.get()) {
				ImageUtils.scaleToWidth(is, file.type(), preview, PREVIEW_WIDTH, false);
			}
			save();
		} catch (Exception e) {
			Logger.error("[MediaPhoto] Failed to create image thumb: " + e.getLocalizedMessage());
		}
	}

	@Override
	public void jsonSerializeCustomFields(JsonObject o, User connectedUser, JsonSerializationContext context) {
		o.addProperty("orientation", orientation.toString().toLowerCase());
		o.addProperty("previewUrl", getPreviewUrl());
		o.addProperty("photoUrl", getPhotoUrl());
	}

	@Override
	public String getTypeKey() {
		return "photo";
	}

	@Override
	public String getTypeName() {
		return Messages.get("media.photo");
	}

	@Override
	public Sender getSender() {
		return this.collection.room.sender;
	}
}

package models;

/**
 * Interface for call contexts
 *
 * @author Denis Meyer, mindsmash GmbH
 */
public interface CallContext {

	public String getURL();

	public String getTitle();

}

package models;

import fishr.search.SearchableModel;

public interface ExtendedSearchableModel extends SearchableModel {

	/**
	 * Redirects the user to the found search result.
	 */
	public void redirectToResult();

	/**
	 * @return Name which will be displayed for this search result.
	 */
	public String getDisplayName();

	/**
	 * @return Sender for which the thumb will be shown next to search results.
	 */
	public Sender getSearchThumbSender();

	/**
	 * @return One-line text displayed below search results.
	 */
	public String getSearchInfoText();
}
package models.notification;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.event.Event;
import models.event.EventBase.EventVisibility;
import play.i18n.Messages;
import session.UserLoader;

/**
 * Sends a notification when an {@link Event} is created.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class NewEventNotification extends EventNotification {

	public static void raise(Event event) {
		final NewEventNotification notification = Notification.createOrReuseNotification(NewEventNotification.class,
				ImmutableMap.of("event", event));

		notification.event = event;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		try {
			// post to page or workspace wall
			if (event.calendar != null) {
				final ArrayList<User> list = new ArrayList<User>();
				list.add(event.creator);
				User connectedUser = UserLoader.getConnectedUser(false);
				if (connectedUser != null) {
					list.add(connectedUser);
				}

				return event.calendar.sender.distributeNotification(this, list);
			} else {
				return new ArrayList<>();
			}
		} finally {
			// global and public
			if (event.calendar == null && event.visibility == EventVisibility.PUBLIC) {
				postNotification(this, event);
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		return Messages.get(isEventSeriesChild() ? "notification.event.series.new" : "notification.event.new",
				event.creator.getDisplayName(), event.name);
	}
}

package models.notification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;

import controllers.ActivityStream;
import controllers.app.Files;
import models.BoxFileVersion;
import models.Sender;
import models.User;
import models.UserNotification;
import models.app.FilesApp;
import models.app.files.File;
import models.app.files.FileVersion;
import play.i18n.Messages;
import session.UserLoader;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class FileNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	public User creator;

	public String fileUid;
	public int fileVersionNo;

	protected FileNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(User creator, FilesApp app, File file, FileVersion version) {
		final FileNotification notification = Notification.createOrReuseNotification(FileNotification.class,
				ImmutableMap.of("fileUid", file.getUid()));

		notification.app = app;
		notification.creator = creator;
		notification.fileUid = file.getUid();
		notification.fileVersionNo = version.getVersionNumber();
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (!app.active) {
			return new ArrayList<>();
		}
		return app.sender.distributeNotification(this, Arrays.asList(creator));
	}

	@Override
	public String getText(UserNotification un) {
		FilesApp filesApp = (FilesApp) app;
		File file = filesApp.getFile(fileUid);
		String name = "";

		if (file == null) {
			return Messages.get("notfound");
		}

		if (creator.equals(UserLoader.getConnectedUser(false))) {
			name = Messages.get("you");
		} else {
			name = creator.getFullName();
		}

		StringBuffer sb = new StringBuffer("notifications.file.");
		final Set<User> others = new HashSet<>();
		if (fileVersionNo == 1) {
			sb.append("uploaded");
		} else {
			sb.append("updated");

			final List<BoxFileVersion> versions = BoxFileVersion.find("file_id = ?", fileUid).fetch();
			for (BoxFileVersion version : versions) {
				if (creator != version.creator) {
					others.add(version.creator);
				}
			}

			if (others.size() == 2) {
				sb.append(".other");
			} else if (others.size() > 2) {
				sb.append(".others");
			}
		}

		return Messages.get(sb.toString(), name, file.getName(), app.sender.getDisplayName(), others.size());
	}

	@Override
	public void redirectToSource() {
		FilesApp filesApp = (FilesApp) app;
		File file = filesApp.getFile(fileUid);
		if (file == null) {
			ActivityStream.index(null, null);
		}
		if (!file.isFolder()) {
			file = file.getParent();
		}
		Files.show(filesApp, file);
	}

	@Override
	public Sender getThumbnailSender() {
		return creator;
	}
}

package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.TypedQuery;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import models.workspace.WorkspaceJoinRequest;
import play.i18n.Messages;

/**
 * Informs the {@link Workspace} admins when an {@link User} requestest to join the {@link Workspace}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WorkspaceJoinReqNotification extends WorkspaceNotification {

	public WorkspaceJoinReqNotification() {
	}

	public static void raise(Workspace workspace, User user) {
		final WorkspaceJoinReqNotification notification = Notification
				.createOrReuseNotification(WorkspaceJoinReqNotification.class, ImmutableMap.of("workspace", workspace));

		notification.workspace = workspace;
		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		for (User admin : workspace.getAdmins()) {
			receivers.add(admin.id);
		}

		return distributeAsync(receivers);
	}

	@Override
	public String getText(UserNotification un) {
		final StringBuffer sb = new StringBuffer("notification.workspace.joinRequest");
		final Integer joinRequests;

		final String sql = "SELECT ws FROM WorkspaceJoinRequest ws WHERE ws.workspace.id = :id";
		final TypedQuery<WorkspaceJoinRequest> q = em().createQuery(sql, WorkspaceJoinRequest.class);
		q.setParameter("id", workspace.id);
		final List<WorkspaceJoinRequest> workspaceJoinRequests = q.getResultList();

		if (workspaceJoinRequests.size() == 2) {
			sb.append(".two");
			joinRequests = 2;
		} else if (workspaceJoinRequests.size() > 2) {
			sb.append(".many");
			joinRequests = workspace.joinRequests.size() - 1;
		} else {
			sb.append(".one");
			joinRequests = 1;
		}
		return Messages.get(sb.toString(), user.getFullName(), workspace.name, joinRequests);
	}
}

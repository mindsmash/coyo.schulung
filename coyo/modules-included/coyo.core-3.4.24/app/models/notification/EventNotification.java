package models.notification;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.Sender;
import models.User;
import models.event.Event;
import models.event.EventSeriesChild;
import controllers.Events;

/**
 * Implementation of {@link EventNotification} for {@link Event}s. Should be
 * fired when inviting someone to an event or if someone responses to an event
 * invitation.
 * 
 */
@Entity
public abstract class EventNotification extends Notification {

	@ManyToOne
	public Event event;

	@Override
	public Sender getThumbnailSender() {
		return event;
	}

	protected boolean isEventSeriesChild() {
		return event instanceof EventSeriesChild;
	}

	@Override
	public void redirectToSource() {
		Events.show(event.id, event.getSlug());
	}

	public void distributeNotifications(User user) {
		this.event.distributeNotification(this, Arrays.asList(user));
	}
}

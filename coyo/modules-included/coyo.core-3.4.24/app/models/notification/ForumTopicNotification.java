package models.notification;

import com.google.common.collect.ImmutableMap;
import controllers.WebBaseController;
import models.Sender;
import models.UserNotification;
import models.app.forum.ForumAppTopic;
import play.i18n.Messages;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This notification is raised when a new topic is created. All interested users
 * (followers or members of the workspace or page) except for the author himself
 * / herself are notified. The notification links to the newly added topic.
 *
 * @author Sven Hoffmann, mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class ForumTopicNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	public ForumAppTopic topic;

	protected ForumTopicNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(ForumAppTopic topic) {
		final ForumTopicNotification notification = Notification
				.createOrReuseNotification(ForumTopicNotification.class, ImmutableMap.of("topic", topic));

		notification.app = topic.app;
		notification.topic = topic;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (!topic.app.active) {
			return new ArrayList<>();
		}
		return topic.app.sender.distributeNotification(this, Arrays.asList(topic.author));
	}

	@Override
	public void redirectToSource() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("topicId", topic.id);
		WebBaseController.redirectToSender(topic.sender, app.id, args);
	}

	@Override
	public String getText(UserNotification un) {
		return Messages
				.get("notification.forum.topic", topic.author.getFullName(), topic.title, app.sender.getDisplayName());
	}

	@Override
	public Sender getThumbnailSender() {
		return topic.author;
	}
}

package models.notification.innoapp;

import controllers.WebBaseController;
import models.Sender;
import models.User;
import models.app.inno.Idea;
import models.notification.AppNotification;
import models.notification.DistributeType;
import models.notification.NotificationType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Informs a user about creation of a new idea
 */
@Entity
public abstract class IdeaNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	protected Idea idea;

	@Override 
	public void redirectToSource() {
		final Map<String, Object> args = new HashMap<String, Object>();
		args.put("idea", idea.id);
		WebBaseController.redirectToSender(app.sender, app.id, args);
	}

	@Override 
	public Sender getThumbnailSender() {
		return app.sender;
	}
}

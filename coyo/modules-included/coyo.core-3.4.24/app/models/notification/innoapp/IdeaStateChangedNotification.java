package models.notification.innoapp;

import com.google.common.collect.ImmutableMap;
import models.UserNotification;
import models.app.inno.Idea;
import models.notification.DistributeType;
import models.notification.Notification;
import models.notification.NotificationType;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;

/**
 * Informs a user that a idea status has changed
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class IdeaStateChangedNotification extends IdeaNotification {

	public static void raise(final Idea idea) {
		final IdeaStateChangedNotification notification = Notification
				.createOrReuseNotification(IdeaStateChangedNotification.class, ImmutableMap.of("app", idea.getApp()));

		notification.app = idea.getApp();
		notification.idea = idea;
		notification.save();
		idea.stateChangedNotification = notification;
		idea.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(idea.creator);
	}
	
	@Override
	public String getText(UserNotification un) {
		return Messages.get("app.inno.notify.status", idea.title);
	}

}
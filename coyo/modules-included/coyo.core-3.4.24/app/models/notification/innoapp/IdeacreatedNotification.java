package models.notification.innoapp;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.app.inno.Idea;
import models.notification.Notification;
import models.notification.NotificationType;
import play.i18n.Messages;
import models.notification.DistributeType;
import javax.persistence.Entity;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Informs a user about creation of a new idea
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
// TODO: fix naming typo (requires DB evolution)
public class IdeacreatedNotification extends IdeaNotification {

	public static void raise(Idea idea) {
		final IdeacreatedNotification notification = Notification
				.createOrReuseNotification(IdeacreatedNotification.class, ImmutableMap.of("app", idea.getApp()));

		notification.idea = idea;
		notification.app = idea.getApp();
		notification.save();
		idea.createdNotification = notification;
		idea.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(idea.creator);
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("app.inno.notify.new", idea.title);
	}
}

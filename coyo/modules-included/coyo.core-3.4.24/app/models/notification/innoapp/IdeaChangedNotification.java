package models.notification.innoapp;

import com.google.common.collect.ImmutableMap;
import models.UserNotification;
import models.app.inno.Idea;
import models.notification.DistributeType;
import models.notification.Notification;
import models.notification.NotificationType;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;

/**
 * Informs a user about change of an idea
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class IdeaChangedNotification extends IdeaNotification {

	public static void raise(Idea idea) {
		final IdeaChangedNotification notification = Notification
				.createOrReuseNotification(IdeaChangedNotification.class, ImmutableMap.of("app", idea.getApp()));

		notification.idea = idea;
		notification.app = idea.getApp();
		notification.save();
		idea.changedNotification = notification;
		idea.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(idea.creator);
	}


	@Override
	public String getText(UserNotification un) {
		return Messages.get("app.inno.notify.changed", idea.title);
	}

}


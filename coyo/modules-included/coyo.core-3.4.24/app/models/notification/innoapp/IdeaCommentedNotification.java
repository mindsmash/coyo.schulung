package models.notification.innoapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.app.inno.Idea;
import models.comments.Comment;
import models.notification.DistributeType;
import models.notification.Notification;
import models.notification.NotificationType;
import play.i18n.Messages;
import session.UserLoader;
import utils.Linkifier;

/**
 * Informs a user that a idea comment has been committed
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class IdeaCommentedNotification extends IdeaNotification {

	@Transient
	private Comment comment;

	public static void raise(Idea idea, final Comment comment) {
		final IdeaCommentedNotification notification = Notification
				.createOrReuseNotification(IdeaCommentedNotification.class, ImmutableMap.of("app", idea.getApp()));

		notification.idea = idea;
		notification.app = idea.getApp();
		notification.comment = comment;
		notification.user = comment.author;
		notification.save();

		idea.commentedNotification = notification;
		idea.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		for (User user : idea.getAllFollowers()) {
			User connectedUser = UserLoader.getConnectedUser(false);
			if (user != null && !user.equals(connectedUser)) {
				receivers.add(user.id);
			}
		}

		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(comment.text)) {
			receivers.add(user.id);
		}

		return distributeAsync(receivers);
	}

	/**
	 * Remove user notification if no comments are present
	 *
	 * @param idea
	 */
	public static void cleanup(Idea idea) {
		if (idea.comments.isEmpty()) {
			final IdeaCommentedNotification notification = IdeaCommentedNotification.find("idea = ?", idea).first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> authors = new ArrayList<>();

		final User lastCommentUser = idea.comments.size() >= 1 ?
				idea.comments.get(idea.comments.size() - 1).author : user;
		boolean recipientCommented = (un != null && un.user == lastCommentUser) ? true : false;

		for (Comment comment : idea.comments) {
			if (!authors.contains(comment.author) && (un != null && comment.author != un.user)) {
				authors.add(comment.author);
			}
		}

		String msg = "app.inno.notify.comment";

		final Integer size;
		if (recipientCommented) {
			size = authors.size();
		} else {
			size = authors.size() - 1;
		}

		final String name;
		if (recipientCommented) {
			name = Messages.get("you");
		} else {
			name = lastCommentUser.getFullName();
		}

		if (authors.size() > 1 || recipientCommented && authors.size() == 1) {
			msg = msg + ".multi";
		} else if (recipientCommented) {
			msg = msg + ".you";
		}

		return Messages.get(msg, name, idea.title, size);
	}
}

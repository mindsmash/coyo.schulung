package models.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;

import controllers.WebBaseController;
import models.Sender;
import models.TasksAppTask;
import models.User;
import models.UserNotification;
import play.i18n.Messages;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class TaskNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	public TasksAppTask task;

	protected TaskNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(TasksAppTask task, User user) {
		final TaskNotification notification = Notification.createOrReuseNotification(TaskNotification.class,
				ImmutableMap.of("task", task));

		notification.task = task;
		notification.app = task.app;
		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (!task.app.active) {
			return new ArrayList<>();
		}

		Set<Long> receivers = new HashSet<>();

		// inform assigned users
		for (User assigned : task.users) {
			if (assigned != user) {
				receivers.add(assigned.id);
			}
		}

		return distributeAsync(receivers);
	}

	@Override
	public void redirectToSource() {
		WebBaseController.redirectToSender(app.sender, app.id);
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notification.task", this.user.getFullName(), task.name);
	}
}

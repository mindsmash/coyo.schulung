package models.notification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;

import models.Sender;
import models.UserNotification;
import models.app.BlogAppArticle;
import play.i18n.Messages;
import play.mvc.results.Redirect;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class BlogArticleNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	public BlogAppArticle blogArticle;

	public BlogArticleNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(BlogAppArticle article) {
		final BlogArticleNotification notification = Notification
				.createOrReuseNotification(BlogArticleNotification.class,
						ImmutableMap.of("blogArticle", article, "app", article.app));

		notification.app = article.app;
		notification.blogArticle = article;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (!blogArticle.app.active) {
			return new ArrayList<>();
		} else {
			return blogArticle.app.sender.distributeNotification(this, Arrays.asList(blogArticle.author));
		}
	}

	@Override
	public void redirectToSource() {
		throw new Redirect(blogArticle.getURL());
	}

	@Override
	public Sender getThumbnailSender() {
		return blogArticle.app.sender;
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notification.blog.article", blogArticle.author.getFullName(), blogArticle.title,
				blogArticle.app.sender.getDisplayName());
	}
}

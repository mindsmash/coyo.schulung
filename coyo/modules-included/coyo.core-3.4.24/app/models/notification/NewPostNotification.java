package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.wall.Wall;
import models.wall.post.Post;
import play.i18n.Messages;
import utils.Linkifier;

/**
 * Sends a notification to an {@link User} to inform him about a new {@link Post} on his {@link Wall}, also informs all
 * mentioned {@link User}s.
 *
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class NewPostNotification extends PostNotification {

	public static void raise(Post post) {
		final NewPostNotification notification = Notification.createOrReuseNotification(NewPostNotification.class,
				ImmutableMap.of("post", post));

		notification.post = post;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		if (post.wall.sender != post.author && post.wall.sender instanceof User) {
			receivers.add(post.wall.sender.id);
		}

		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(post.message)) {
			receivers.add(user.id);
		}

		return distributeAsync(receivers);
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		final StringBuffer sb = new StringBuffer("notifications.post.new");
		if (post.author.equals(post.wall.sender)) {
			sb.append(".own");
		} else if (u != null && post.wall.sender.equals(u)) {
			sb.append(".your");
		}

		return Messages.get(sb.toString(), post.author.getDisplayName(), post.wall.sender.getDisplayName());
	}
}

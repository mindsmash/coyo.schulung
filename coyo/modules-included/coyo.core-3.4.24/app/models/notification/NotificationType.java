package models.notification;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import models.notification.Notification.DisplayType;

/**
 * Sets the {@link DisplayType} of a notification.
 * 
 * @author mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface NotificationType {

	DisplayType displayType() default DisplayType.BOTH;
	
	DistributeType distributionType();
	
	boolean configurable() default false;
}

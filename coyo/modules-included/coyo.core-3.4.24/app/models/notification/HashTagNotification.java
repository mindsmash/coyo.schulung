package models.notification;

import com.google.common.collect.ImmutableMap;
import models.ExtendedTenantModel;
import models.Sender;
import models.User;
import models.UserNotification;
import play.Logger;
import play.i18n.Messages;
import session.UserLoader;
import util.ModelUtil;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.Collection;
import java.util.List;

/**
 * Informs a user about new content that contains a hashtag the user is following
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class HashTagNotification extends UserBaseNotification {

	private String hashTag;
	private String entityUId;

	@Transient
	private List<User> users;

	public static void raise(List<User> users, String hashtag, final String modelUId) {
		final HashTagNotification notification = Notification.createOrReuseNotification(HashTagNotification.class,
				ImmutableMap.of("hashTag", hashtag, "entityUId", modelUId));

		if (users.size() < 1) {
			return;
		}

		notification.users = users;
		notification.user = users.get(0);
		notification.hashTag = hashtag;
		notification.entityUId = modelUId;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		final Object o = ModelUtil.deserialize(entityUId);

		if (!(o instanceof ExtendedTenantModel)) {
			Logger.error("[Hashtags] Failed to read ExtendendTenantModel: " + entityUId);
			return null;
		}

		final ExtendedTenantModel extendedTenantModel = (ExtendedTenantModel) o;
		try {
			for (User user : users) {
				UserLoader.setThreadLocalUser(user.id);
				if (extendedTenantModel.checkSearchPermission()) {
					Logger.debug("[Hashtags] Informing user [%s] about hashtag [%s] usage", user, hashTag);
					user.receiveNotification(this);
				}
			}
		} finally {
			UserLoader.clearThreadLocalUser();
		}

		return null;
	}

	@Override
	public void redirectToSource() {
		final Object o = ModelUtil.deserialize(entityUId);
		if (!(o instanceof ExtendedTenantModel)) {
			Logger.error("[PublishHashtagUsageJob] Failed to read ExtendendTenantModel: " + entityUId);
			return;
		}

		final ExtendedTenantModel extendedTenantModel = (ExtendedTenantModel) o;
		extendedTenantModel.redirectToResult();
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notifications.hashtag", hashTag);
	}

	@Override
	public Sender getThumbnailSender() {
		final User connectedUser = UserLoader.getConnectedUser(false);
		return connectedUser != null ? connectedUser : user;
	}
}

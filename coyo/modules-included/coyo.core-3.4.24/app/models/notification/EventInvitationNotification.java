package models.notification;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.event.Event;
import play.i18n.Messages;

/**
 * 
 * Sends a notification when an {@link User} is invited to an {@link Event}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class EventInvitationNotification extends EventNotification {

	public EventInvitationNotification() {
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(user);
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get(isEventSeriesChild() ? "notifications.event.series.invite" : "notifications.event.invite",
				event.name);
	}

	public static void raise(Event event, User user) {
		final EventInvitationNotification notification =
				Notification.createOrReuseNotification(EventInvitationNotification.class,
						ImmutableMap.of("event", event));
		notification.user = user;
		notification.event = event;
		notification.save();

		notification.distribute();
	}
}

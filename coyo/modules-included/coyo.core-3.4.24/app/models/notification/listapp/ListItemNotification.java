package models.notification.listapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;

import controllers.WebBaseController;
import models.Sender;
import models.User;
import models.app.App;
import models.app.ListApp;
import models.container.Item;
import models.notification.AppNotification;
import models.notification.Notification;
import session.UserLoader;

/**
 * Informs a user about creation of a new idea
 */
@Entity
public abstract class ListItemNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	protected Item item;

	public static <T extends ListItemNotification> void raise(Class<T> clazz, App app, Item item) {
		T notification = Notification.createOrReuseNotification(clazz, ImmutableMap.of("app", app));

		notification.app = app;
		notification.item = item;
		notification.save();
		item.itemNotifications.add(notification);
		item.save();
		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (((ListApp) item.getApp()).notificationRecipient == ListApp.NotificationRecipient.NONE) {
			return new ArrayList<>();
		}

		User connectedUser = UserLoader.getConnectedUser(false);
		if (((ListApp) this.app).notificationRecipient == ListApp.NotificationRecipient.ADMINS) {
			Set<Long> receivers = new HashSet<>();

			for (User user : this.app.sender.getAdmins()) {
				if (connectedUser == null || !user.equals(connectedUser)) {
					receivers.add(user.id);
				}
			}

			return distributeAsync(receivers);
		}

		final List<User> excludes = new ArrayList<>();
		if (connectedUser != null) {
			excludes.add(connectedUser);
		}
		return this.app.sender.distributeNotification(this, excludes);
	}

	@Override
	public void redirectToSource() {
		final Map<String, Object> args = new HashMap<String, Object>();
		args.put("itemId", item.id);
		WebBaseController.redirectToSender(app.sender, app.id, args);
	}

	@Override
	public Sender getThumbnailSender() {
		return app.sender;
	}
}

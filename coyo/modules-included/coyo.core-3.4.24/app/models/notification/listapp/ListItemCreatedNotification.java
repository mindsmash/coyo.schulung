package models.notification.listapp;

import models.User;
import models.UserNotification;
import models.app.App;
import models.app.ListApp;
import models.app.inno.Idea;
import models.container.Item;
import models.notification.DistributeType;
import models.notification.NotificationType;
import models.notification.innoapp.IdeaNotification;
import play.i18n.Messages;
import session.UserLoader;

import javax.persistence.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Informs an user about the creation of a new idea
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class ListItemCreatedNotification extends ListItemNotification {

	@Override
	public String getText(UserNotification un) {
		return Messages.get("app.list.notify.new", item.container.name);
	}
}

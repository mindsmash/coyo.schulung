package models.notification.listapp;

import models.User;
import models.UserNotification;
import models.app.ListApp;
import models.container.Item;
import models.notification.Notification;
import models.notification.NotificationType;
import play.i18n.Messages;
import session.UserLoader;
import models.notification.DistributeType;
import javax.persistence.Entity;

import org.apache.commons.lang.ClassUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Informs a user that one item has been changed
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class ListItemChangedNotification extends ListItemNotification {

	@Override
	public String getText(UserNotification un) {
		return Messages.get("app.list.notify.changed", item.container.name);
	}
}

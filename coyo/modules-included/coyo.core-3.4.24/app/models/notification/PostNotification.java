package models.notification;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import models.Sender;
import models.User;
import models.UserNotification;
import models.wall.post.Post;
import utils.HibernateUtils;
import controllers.Posts;

/**
 * This class is used to notify followers if something happens with a already existing post. For notifications about new
 * posts
 * 
 */
@Entity
public abstract class PostNotification extends Notification {

	@ManyToOne(fetch = FetchType.LAZY)
	public Post post;


	public Post getPost() {
		return HibernateUtils.unproxy(post);
	}

	public String getText(User u) {
		return u.getFullName();
	}

	@Override
	public void redirectToSource() {
		Posts.show(post.id);
	}

	@Override
	public Sender getThumbnailSender() {
		return post.author;
	}
}

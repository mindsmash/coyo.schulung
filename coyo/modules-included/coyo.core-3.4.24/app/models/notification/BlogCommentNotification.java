package models.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.google.common.collect.ImmutableMap;

import jobs.NotificationDistributor;
import models.Sender;
import models.User;
import models.UserNotification;
import models.app.BlogAppArticle;
import models.comments.Comment;
import play.i18n.Messages;
import play.mvc.results.Redirect;
import plugins.play.JobExecutorPlugin;
import utils.Linkifier;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class BlogCommentNotification extends BlogArticleNotification {

	@Transient
	public Comment blogComment;

	public BlogCommentNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(BlogAppArticle article, Comment newComment) {
		final BlogCommentNotification notification = Notification
				.createOrReuseNotification(BlogCommentNotification.class,
						ImmutableMap.of("blogArticle", article, "app", article.app));

		notification.app = article.app;
		notification.blogArticle = article;
		notification.blogComment = newComment;
		notification.user = newComment.author;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		
		// inform all other comment authors
		for (Comment comment : blogArticle.comments) {
			if (comment.author != user) {
				receivers.add(comment.author.id);
			}
		}
		// inform all other likers
		for (User user : blogArticle.likes) {
			if (this.user != user) {
				receivers.add(user.id);
			}
		}
		// inform article author
		if (blogArticle.author != user) {
			receivers.add(blogArticle.author.id);
		}
		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(blogComment.text)) {
			receivers.add(user.id);
		}
		
		return distributeAsync(receivers);
	}

	/**
	 * Remove user notification if no comments are present
	 *
	 * @param article
	 */
	public static void cleanup(BlogAppArticle article) {
		if (article.comments.isEmpty()) {
			final BlogCommentNotification notification = BlogCommentNotification.find("blogArticle = ?", article)
					.first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public void redirectToSource() {
		throw new Redirect(blogArticle.getURL());
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}

	@Override
	public String getText(UserNotification un) {
		final User lastCommentUser = blogArticle.comments.get(blogArticle.comments.size() - 1).author;
		boolean recipientCommented = (un != null && un.user == lastCommentUser) ? true : false;

		final List<User> authors = new ArrayList<>();
		for (Comment comment : blogArticle.comments) {
			if (!authors.contains(comment.author) && (un != null && comment.author != un.user)) {
				authors.add(comment.author);
			}
		}

		final StringBuffer sb = new StringBuffer("notifications.blog.comment");
		final Integer size;

		if (recipientCommented) {
			size = authors.size();
			sb.append(".you");
		} else {
			size = authors.size() - 1;
		}

		if (recipientCommented && authors.size() == 1 || authors.size() == 2) {
			sb.append(".multi");
		} else if (authors.size() > 2) {
			sb.append(".multis");
		}

		return Messages.get(sb.toString(), lastCommentUser.getDisplayName(), blogArticle.title, size);
	}
}

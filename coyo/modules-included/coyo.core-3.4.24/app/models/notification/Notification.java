package models.notification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;

import jobs.NotificationDistributor;
import models.Application;
import models.BaseModel;
import models.Sender;
import models.TenantModel;
import models.User;
import models.UserNotification;
import models.wall.Wall;
import models.wall.post.NotificationPost;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Play;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import plugins.play.JobExecutorPlugin;

/**
 * General class for notifications. All types of notifications should extend
 * this class.
 *
 * @author mindsmash GmbH
 */
@Entity
@javax.persistence.Table(name = Notification.TABLE)
@Table(appliesTo = Notification.TABLE, indexes = { 
		@Index(columnNames = { "DTYPE" }, name = "idx_dtype") })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DTYPE", length = 100)
public abstract class Notification extends TenantModel {

	public enum DisplayType {
		WEB, EMAIL, BOTH
	}

	private static final Logger LOG = LoggerFactory.getLogger(Notification.class);

	public static final String TABLE = "notification";

	private static List<String> configurableNotificationTypes;

	public DisplayType displayType = DisplayType.BOTH;

	@ManyToOne
	public User user;

	@OneToMany(mappedBy = "notification", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<UserNotification> userNotifications = new ArrayList<>();

	@OneToMany(mappedBy = "notification", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<NotificationPost> posts = new ArrayList<NotificationPost>();

	protected Notification() {
		// set display type
		NotificationType type = (NotificationType) getClass().getAnnotation(NotificationType.class);
		if (type != null) {
			displayType = type.displayType();
		} else {
			LOG.info("[Notification] Creating a notification [%s] that is not annotated with @NotificationType",
					getClass().getName());
		}
	}

	/**
	 * Handles user notification distribution
	 * 
	 * @return The IDs of the users that received this notification.
	 */
	public abstract Collection<Long> distribute();

	/**
	 * Distributes this notification to given user IDs asynchronously.
	 * 
	 * @param userIds
	 */
	protected Collection<Long> distributeAsync(Collection<Long> userIds) {
		JobExecutorPlugin.addJob(new NotificationDistributor(tenant, id, userIds));
		return userIds;
	}

	/**
	 * Distributes this notification to given users.
	 * 
	 * @param users
	 */
	protected Collection<Long> distributeAsync(User... users) {
		Set<Long> userIds = new HashSet<>();
		for (User user : users) {
			userIds.add(user.id);
		}
		return distributeAsync(userIds);
	}

	public abstract Sender getThumbnailSender();

	public abstract void redirectToSource();

	public abstract String getText(UserNotification un);

	public String getImageUrl() {
		ActionDefinition ad = Router.reverse("Resource.senderThumb", new HashMap<String, Object>() {
			{
				put("senderId", getThumbnailSender().id);
			}
		});
		ad.absolute();
		return ad.url;
	}

	/*
	 * Static methods below
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Finds and reuses or creates a new notification and triggers its
	 * distribution
	 *
	 * @param clazz
	 *            Notification type
	 * @param searchParams
	 * @param <T>
	 * @param <V>
	 * @return
	 */
	public static <T extends Notification, V> T createOrReuseNotification(Class<T> clazz, Map<String, V> searchParams) {
		T notification;

		final StringBuffer sb = new StringBuffer();
		sb.append("SELECT t1 FROM " + clazz.getSimpleName() + " t1 WHERE ");

		// add matching clause per field entry
		final Iterator<Map.Entry<String, V>> iterator = searchParams.entrySet().iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, V> entry = iterator.next();
			sb.append(" (" + entry.getKey() + " = :" + entry.getKey() + ") ");
			if (iterator.hasNext()) {
				sb.append(" AND ");
			}
		}

		// create query
		final TypedQuery<T> q = BaseModel.em().createQuery(sb.toString(), clazz);

		// add field entry parameters
		for (Map.Entry<String, V> entry : searchParams.entrySet()) {
			q.setParameter(entry.getKey(), entry.getValue());
		}

		try {
			notification = q.getSingleResult();
		} catch (NoResultException ex) {
			notification = null;
		}

		if (notification == null) {
			try {
				notification = clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				LOG.error("[Notification] Error creating new notification instance of type " + clazz, e);
				throw new IllegalStateException(e);
			}
		}

		return notification;
	}

	public static List<String> getConfigurableNotificationTypes() {
		if (configurableNotificationTypes == null) {
			configurableNotificationTypes = new ArrayList<>();

			for (Class clazz : Play.classloader.getAnnotatedClasses(NotificationType.class)) {
				NotificationType type = (NotificationType) clazz.getAnnotation(NotificationType.class);
				if (type.configurable()) {
					configurableNotificationTypes.add(clazz.getSimpleName());
				}
			}

			Collections.sort(configurableNotificationTypes);
		}

		return configurableNotificationTypes;
	}

	/**
	 * Posts a notification to the application's wall and thereby to every
	 * user's activity stream.
	 *
	 * @param notification
	 * @param author
	 */
	protected static void postNotification(Notification notification, Sender author) {
		if (notificationEnabled(notification.getClass().getSimpleName())) {
			postNotification(notification, author, Application.get().getDefaultWall());
		}
	}

	/**
	 * Posts a notification to a wall and thereby to the user's activity stream.
	 *
	 * @param notification
	 * @param author
	 * @param wall
	 */
	protected static void postNotification(Notification notification, Sender author, Wall wall) {
		NotificationPost np = new NotificationPost();
		np.author = author;
		np.notification = notification;
		np.wall = wall;
		np.save();
	}

	protected static boolean notificationEnabled(String simpleName) {
		if (!Application.POSTS_ENABLED) {
			return false;
		} else {
			if (Arrays.asList(Application.NOTIFICATIONS_DISABLED.split(",")).contains(simpleName)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Tests if a notification class is reusable
	 *
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public static <T extends Notification> boolean isReusableNotification(Class<T> clazz) {
		final DistributeType distributeType = clazz.getAnnotation(NotificationType.class).distributionType();
		if (distributeType == DistributeType.REUSE) {
			return true;
		} else {
			return false;
		}
	}
}

package models.notification;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import controllers.Workspaces;
import models.Sender;
import models.workspace.Workspace;

@Entity
public abstract class WorkspaceNotification extends Notification {
	
	@ManyToOne
	public Workspace workspace;
	
	@Override
	public void redirectToSource() {
		Workspaces.show(workspace.id, null, workspace.getSlug(), null);
	}

	@Override
	public Sender getThumbnailSender() {
		return workspace;
	}
}

package models.notification;

import com.google.common.collect.ImmutableMap;
import models.Sender;
import models.User;
import models.UserNotification;
import models.comments.Comment;
import models.wall.post.Post;
import play.i18n.Messages;
import utils.Linkifier;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Sends a notification to all {@link User}s that follow a {@link Post}, when a new comment is written.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class PostCommentNotification extends PostNotification {

	@Transient
	public Comment postComment;

	public PostCommentNotification() {
	}

	public static void raise(Post post, Comment comment) {
		final PostCommentNotification notification = Notification.createOrReuseNotification(
				PostCommentNotification.class, ImmutableMap.of("post", post));

		notification.post = post;
		notification.postComment = comment;
		notification.user = comment.author;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		// inform users who are following the post
		for (User user : post.getFollowers()) {
			if (user != postComment.author) {
				receivers.add(user.id);
			}
		}
		// inform users who have flagged the post
		for (User user : post.getFlagged()) {
			if (user != postComment.author) {
				receivers.add(user.id);
			}
		}
		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(postComment.text)) {
			receivers.add(user.id);
		}

		return distributeAsync(receivers);
	}

	/**
	 * Remove user notification if no comments are present
	 *
	 * @param post
	 */
	public static void cleanup(Post post) {
		if (post.comments.isEmpty()) {
			final PostCommentNotification notification = PostCommentNotification.find("post = ?", post).first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		final List<User> authors = new ArrayList<>();
		final User lastCommentUser = getLastCommentingUser();
		boolean recipientCommented = (u != null && u == lastCommentUser) ? true : false;

		for (Comment comment : post.comments) {
			if (!authors.contains(comment.author) && (u != null && comment.author != u)) {
				authors.add(comment.author);
			}
		}

		final StringBuffer sb = new StringBuffer("notifications.post.comment");
		boolean multi = false;
		if (recipientCommented && (u != null && post.author == u)) {
			sb.append(".you.your");
			multi = true;
		} else if (u != null && u == post.author) {
			sb.append(".your");
		} else if (recipientCommented) {
			sb.append(".you");
		} else if (user == post.author) {
			sb.append(".their");
		}

		if ((authors.size() == 2 && !multi) || (authors.size() == 1 && multi)) {
			sb.append(".multi");
		} else if (authors.size() > 2 || multi) {
			sb.append(".multis");
		}

		final Integer size;
		if (recipientCommented || multi) {
			size = authors.size();
		} else {
			size = authors.size() - 1;
		}

		return Messages.get(sb.toString(), lastCommentUser.getFullName(), post.author.getDisplayName(), size);

	}

	@Override
	public Sender getThumbnailSender() {
		return getLastCommentingUser();
	}
	
	/**
	 * Retrieve the last user who wrote a comment.
	 */
	private User getLastCommentingUser() {
		if (post.comments.size() >= 1) {
			return post.comments.get(post.comments.size() - 1).author;
		} else {
			return user;
		}
	}
}

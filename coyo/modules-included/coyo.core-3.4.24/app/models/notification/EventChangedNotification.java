package models.notification;

import java.util.Collection;

import javax.persistence.Entity;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.collect.ImmutableMap;

import libs.DateI18N;
import models.User;
import models.UserNotification;
import models.event.Event;
import play.i18n.Messages;

/**
 * Sends a notification when an {@link Event} is changed.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class EventChangedNotification extends EventNotification {

	public static Collection<Long> raise(Event event) {
		final EventChangedNotification notification = Notification
				.createOrReuseNotification(EventChangedNotification.class, ImmutableMap.of("event", event));

		notification.event = event;
		notification.save();

		return notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return event.distributeNotification(this, null);
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		DateTimeZone timezone = (u != null) ? u.getDateTimeZone() : user.getDateTimeZone();
		DateTime start = event.getStartDate(timezone);
		return Messages.get("notifications.event.changed" + (event.fulltime ? ".fulltime" : ""), event.name,
				formatDateTime(start, DateI18N.getShortFormat()), formatDateTime(start, DateI18N.getTimeFormat()));
	}

	private static String formatDateTime(DateTime dateTime, String pattern) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
		return fmt.print(dateTime);
	}
}

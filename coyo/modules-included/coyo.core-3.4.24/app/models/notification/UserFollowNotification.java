package models.notification;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;
import models.Sender;
import models.User;
import models.UserNotification;
import play.i18n.Messages;
import controllers.Users;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * 
 * Informs a {@link User} when an {@link User} starts following him.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class UserFollowNotification extends UserBaseNotification {

	@ManyToOne
	public User follower;

	public UserFollowNotification() {
	}

	public static void raise(User user, User follower) {
		UserFollowNotification notification = Notification.createOrReuseNotification(UserFollowNotification.class,
				ImmutableMap.of("user", user, "follower", follower));

		notification.user = user;
		notification.follower = follower;
		notification.save();
		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(user);
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notification.user.follow", follower.getFullName());
	}

	@Override
	public void redirectToSource() {
		Users.wall(follower.id, follower.getSlug());
	}


	@Override
	public Sender getThumbnailSender() {
		return follower;
	}
}

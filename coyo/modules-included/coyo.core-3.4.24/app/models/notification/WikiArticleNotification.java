package models.notification;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.app.WikiAppArticle;
import models.app.WikiAppArticleVersion;
import play.i18n.Messages;

import javax.persistence.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WikiArticleNotification extends WikiNotification {

	public static void raise(final WikiAppArticle article, final User user) {
		final WikiArticleNotification notification = Notification
				.createOrReuseNotification(WikiArticleNotification.class, ImmutableMap.of("wikiArticle", article));

		notification.wikiArticle = article;
		notification.app = article.app;
		notification.user = user;

		notification.save();
		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		if (!wikiArticle.app.active) {
			return new ArrayList<>();
		} else {
			return wikiArticle.app.sender.distributeNotification(this, Arrays.asList(user));
		}
	}

	@Override
	public String getText(UserNotification un) {
		final StringBuffer sb = new StringBuffer();
		if (wikiArticle.versions.size() > 1) {
			sb.append("notification.wiki.edit");
		} else {
			sb.append("notification.wiki.new");
		}

		final WikiAppArticleVersion articleVersion = wikiArticle.getCurrentVersion();

		if (un != null && articleVersion.author == un.user) {
			sb.append(".you");
		}

		return Messages.get(sb.toString(), articleVersion.author.getFullName(), wikiArticle.title,
					wikiArticle.app.sender.getDisplayName());
	}
}

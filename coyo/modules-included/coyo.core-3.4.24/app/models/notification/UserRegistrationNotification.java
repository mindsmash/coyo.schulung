package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import play.i18n.Messages;
import controllers.administration.Users;

/**
 * 
 * Informs all superadmins when an {@link User} tries to register.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class UserRegistrationNotification extends UserBaseNotification {

	public static void raise(User user) {
		final UserRegistrationNotification notification =
				Notification.createOrReuseNotification(UserRegistrationNotification.class, ImmutableMap.of("user", user));

		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public String getText(UserNotification un) {
		if (this.user.active) {
			return Messages.get("notification.user.registration.active", user.getFullName());
		} else {
			return Messages.get("notification.user.registration.inactive", user.getFullName());
		}
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		
		// notify superadmins
		List<User> superadmins = User.find("superadmin = true AND active = true").fetch();
		for (User superadmin : superadmins) {
			receivers.add(superadmin.id);
		}
		
		return distributeAsync(receivers);
	}



	@Override
	public void redirectToSource() {
		Users.edit(this.user.id);
	}
}
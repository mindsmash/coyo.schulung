package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.google.common.collect.ImmutableMap;

import controllers.Calls;
import models.Call;
import models.Sender;
import models.User;
import models.UserNotification;
import play.i18n.Messages;

/**
 * Implementation of {@link CallNotification} for {@link Call}s. Should be fired when someone missed a call
 *
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class CallNotification extends Notification {

	@ManyToOne(fetch = FetchType.LAZY)
	public Call call;

	public CallNotification() {
		this.displayType = DisplayType.BOTH;
	}

	@Override
	public Sender getThumbnailSender() {
		return call.host;
	}

	@Override
	public void redirectToSource() {
		Calls.index();
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("webrtc.call.missed", call.host.getFullName());
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		for (User user : call.participants) {
			if (user.id != call.host.id) {
				receivers.add(user.id);
			}
		}
		return distributeAsync(receivers);
	}

	public static void raise(Call call) {
		// final CallNotification notification = new CallNotification();

		final CallNotification notification = Notification.createOrReuseNotification(CallNotification.class,
				ImmutableMap.of("call", call));

		notification.call = call;
		notification.save();

		notification.distribute();
	}
}

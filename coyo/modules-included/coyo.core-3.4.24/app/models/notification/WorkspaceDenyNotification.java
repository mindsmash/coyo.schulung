package models.notification;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import play.i18n.Messages;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;

/**
 * Informs all {@link models.workspace.Workspace} members when a new {@link models.User} joins the
 * {@link models.workspace.Workspace}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WorkspaceDenyNotification extends WorkspaceNotification {

	public WorkspaceDenyNotification() {
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notification.workspace.deny", workspace.name);
	}

	public static void raise(Workspace workspace, User user) {
		final WorkspaceDenyNotification notification = Notification
				.createOrReuseNotification(WorkspaceDenyNotification.class, ImmutableMap.of("workspace", workspace));

		notification.user = user;
		notification.workspace = workspace;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(user);
	}
}

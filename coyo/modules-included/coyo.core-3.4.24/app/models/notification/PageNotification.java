package models.notification;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.Sender;
import models.page.Page;
import controllers.Pages;

/**
 * This class is used to notify if something happens with a {@link Page}.
 * 
 * @author mindsmash GmnbH
 */
@Entity
public abstract class PageNotification extends Notification {

	@ManyToOne
	public Page page;

	@Override
	public void redirectToSource() {
		Pages.show(page.id, null, page.getSlug(), null);
	}

	@Override
	public Sender getThumbnailSender() {
		return page;
	}
}

package models.notification;

import com.google.common.collect.ImmutableMap;
import controllers.Posts;
import models.Sender;
import models.User;
import models.UserNotification;
import models.wall.post.Post;
import play.i18n.Messages;
import session.UserLoader;
import utils.HibernateUtils;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Sends a notification to all {@link User}s that follow a {@link Post}, when
 * the {@link Post} is liked by a new {@link User}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class PostLikeNotification extends PostNotification {

	public static void raise(Post post, User liker) {
		final PostLikeNotification notification = Notification
				.createOrReuseNotification(PostLikeNotification.class, ImmutableMap.of("post", post));

		notification.user = liker;
		notification.post = post;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		
		for (User user : post.getFollowers()) {
			if (user != this.user) {
				receivers.add(user.id);
			}
		}
		
		for (User user : post.getFlagged()) {
			if (user != this.user) {
				receivers.add(user.id);
			}
		}
		
		return distributeAsync(receivers);
	}

	public static void cleanup(Post post) {
		if (post.getLikes().size() < 1) {
			final PostLikeNotification notification = PostLikeNotification.find("post = ?", post).first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> likes = post.getLikes();
		final List<User> filteredLikes = new ArrayList<>();
		final StringBuffer sb = new StringBuffer("notifications.post.like");
		User lastLiked = null;

		int append = 0;
		if(un != null) {
			for (User liker : likes) {
				if (!filteredLikes.contains(liker) && un.user != liker && liker != post.author) {
					filteredLikes.add(liker);
				}
			}

			if (un.user == post.author) {
				sb.append(".your");
				if(filteredLikes.size() > 1) {
					sb.append(".multi");
				} else if(filteredLikes.size() == 0) {
					sb.append(".you");
				}
			} else {
				sb.append(".you");
				if(likes.contains(post.author)) {
					append = 1;
				}
				if((filteredLikes.size() + append) == 0) {
					sb.append(".you");
				} else if((filteredLikes.size() + append) > 1) {
					sb.append(".multi");
				}
			}

			if(((likes.contains(un.user) && filteredLikes.size() > 0) ||
					(likes.contains(un.user) && filteredLikes.size() == 0 && likes.contains(post.author))) &&
					((filteredLikes.size() + append) > 0)) {
				sb.append(".also");
			}

			if(filteredLikes.size() > 1) {
				lastLiked = filteredLikes.get(filteredLikes.size() - 1);
				if((filteredLikes.size() + append - 1) > 1) {
					append -= 1;
					sb.append(".multi");
				}
			} else if(filteredLikes.size() == 1) {
				lastLiked = filteredLikes.get(0);
			}
		} else {
			if(filteredLikes.size() > 1) {
				sb.append(".multi");
			}
		}

		return Messages.get(
				sb.toString(),
				lastLiked != null ? lastLiked.getDisplayName() : ((lastLiked == null && append > 0) ? post.author
						.getDisplayName() : ""),
				post.author.getDisplayName(),
				filteredLikes.size() + append);
	}

	@Override
	public Sender getThumbnailSender() {
		/* does not work with just returning user; if the user dislikes, the image of it is still displayed */
		List<User> likes = post.getLikes();
		User lastLiked = null;
		User currUser = UserLoader.getConnectedUser(false);

		if(currUser != null) {
			if(likes.size() >= 1) {
				lastLiked = likes.get(likes.size() - 1);
				if(lastLiked == currUser && likes.size() >= 2) {
					lastLiked = likes.get(likes.size() - 2);
				}
			} else if(likes.size() == 1) {
				lastLiked = likes.get(0);
			}
			if (lastLiked != null) {
				return lastLiked;
			}
		}
		if (!likes.contains(user) && likes.size() >= 1) {
			return likes.get(likes.size() - 1);
		}
		return user;
	}
}

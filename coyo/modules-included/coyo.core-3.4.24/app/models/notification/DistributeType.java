package models.notification;

/**
 * Distinguishes if a notification can be reused or not
 *
 */
public enum DistributeType {
	SINGLE, REUSE
}

package models.notification;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.page.Page;
import play.i18n.Messages;

/**
 * 
 * Sends a notification when an {@link User} is invited to a {@link Page}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class PageInvitationNotification extends PageNotification {

	public PageInvitationNotification() {
	}

	public static void raise(Page page, User user) {
		final PageInvitationNotification notification = Notification.createOrReuseNotification(
				PageInvitationNotification.class, ImmutableMap.of("page", page, "user", user));

		notification.page = page;
		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(user);
	}

	@Override
	public String getText(UserNotification un) {
		if(un != null && page.isAdmin(un.user)) {
			return Messages.get("notifications.page.invite.admin", page.name);
		}
		return Messages.get("notifications.page.invite", page.name);
	}
}

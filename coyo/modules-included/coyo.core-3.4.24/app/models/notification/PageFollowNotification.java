package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import controllers.Pages;
import models.Sender;
import models.User;
import models.UserNotification;
import models.page.Page;
import play.i18n.Messages;

/**
 * Informs all admins of a page when an {@link User} starts following the {@link Page}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class PageFollowNotification extends PageNotification {

	public PageFollowNotification() {
	}

	public static void raise(Page page, User follower) {
		PageFollowNotification notification = Notification.createOrReuseNotification(PageFollowNotification.class,
				ImmutableMap.of("page", page));
		notification.page = page;
		notification.user = follower;
		notification.save();
		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		// inform page admins
		for (User admin : page.admins) {
			if (admin != user) {
				receivers.add(admin.id);
			}
		}

		return distributeAsync(receivers);
	}

	public static void cleanup(Page page) {
		// a page always has one follower
		if (page.followers.size() < 2) {
			PageFollowNotification notification = PageFollowNotification.find("page = ?", page).first();

			if (notification != null) {
				final Iterator<UserNotification> iterator = notification.userNotifications.iterator();
				while (iterator.hasNext()) {
					final UserNotification un = iterator.next();
					iterator.remove();
					un.delete();
				}
				notification.save();
			}
		}
	}

	@Override
	public void redirectToSource() {
		Pages.followers(page.id);
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		Long count = page.getFollowerCount();
		if(u != null && page.followers.contains(u)) {
			--count;
		}

		// handle illegal state
		if (count == 0) {
			return Messages.get("notification.notfound");
		}

		final StringBuffer sb = new StringBuffer("notification.page.follow");
		if (!page.isFollower(user) || !user.active) {
			sb.append(".unknown");
			if (count == 1) {
				sb.append(".single");
			}
		} else {
			if (u != null && user == u) {
				sb.setLength(0);
				sb.append("notification.page.follow.you");
			}
			if (count == 2) {
				sb.append(".two");
			} else if(count > 2) {
				sb.append(".many");
			}
			count--;
		}

		return Messages.get(sb.toString(), user.getFullName(), page.name, count);
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}
}

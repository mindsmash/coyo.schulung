package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import play.i18n.Messages;

/**
 * Informs all {@link Workspace} members if a member leaves.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WorkspaceLeaveNotification extends WorkspaceNotification {

	public static void raise(Workspace workspace, User user) {
		final WorkspaceLeaveNotification notification = Notification
				.createOrReuseNotification(WorkspaceLeaveNotification.class, ImmutableMap.of("workspace", workspace));

		notification.workspace = workspace;
		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		for (User admin : workspace.getAdmins()) {
			receivers.add(admin.id);
		}

		return distributeAsync(receivers);
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notification.workspace.leave", user.getFullName(), workspace.name);
	}
}

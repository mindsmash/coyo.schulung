package models.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import controllers.WebBaseController;
import models.Sender;
import models.User;
import models.UserNotification;
import models.app.forum.ForumAppTopic;
import models.wall.post.Post;
import play.i18n.Messages;

/**
 * This notification is raised, when a new post is added to a topic. All users
 * who contributed to the topic will get this notification, except for the
 * author of the new post. The notification links to the topic with the new post
 * highlighted.
 *
 * @author Sven Hoffmann, mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class ForumPostNotification extends PostNotification {

	@ManyToOne
	public ForumAppTopic forumTopic;

	protected ForumPostNotification() {
		this.displayType = DisplayType.BOTH;
	}

	public static void raise(ForumAppTopic topic, Post post) {
		if (!topic.app.active) {
			return;
		}
		final ForumPostNotification notification = Notification.createOrReuseNotification(ForumPostNotification.class,
				ImmutableMap.of("forumTopic", topic));
		notification.post = post;
		notification.forumTopic = topic;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		// send this notification to all users who contributed in the topic,
		// except for the author of the post
		for (User user : getRecipients(forumTopic)) {
			if (!user.equals(post.author)) {
				receivers.add(user.id);
			}
		}
		return distributeAsync(receivers);
	}

	@Override
	public void redirectToSource() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("postId", post.id);
		WebBaseController.redirectToSender(forumTopic.sender, forumTopic.app.id, args);
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> users = new ArrayList<>();

		// Skip first post because it is part of the creation process of the
		// forum app
		for (Post post : forumTopic.getPosts()) {
			if ((un != null && un.user != post.author) && !users.contains(post.author)) {
				users.add((User) post.author);
			}
		}

		final boolean recipientCommented = (un != null && post.author == un.user) ? true : false;

		String name;
		if (recipientCommented) {
			name = Messages.get("you");
		} else {
			name = post.author.getDisplayName();
		}

		int authorsCount = recipientCommented ? users.size() : users.size() - 1;

		StringBuffer sb = new StringBuffer("notification.forum.post");
		if (users.size() == 2 || users.size() == 1 && recipientCommented) {
			sb.append(".other");
		} else if (users.size() > 2) {
			sb.append(".others");
		}
		return Messages.get(sb.toString(), name, forumTopic.title, authorsCount);
	}

	@Override
	public Sender getThumbnailSender() {
		return post.author;
	}

	/**
	 * Helper method to return all different users, who contributed to a thread.
	 * Contributing means, that they posted at least one post to the topic.
	 */
	private static Set<User> getRecipients(ForumAppTopic topic) {
		List<User> userList = Lists.transform(topic.getPosts(), new Function<Post, User>() {
			@Override
			public User apply(Post post) {
				return (User) post.author;
			}
		});
		return new HashSet(userList);
	}
}

package models.notification;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import play.i18n.Messages;
import play.templates.JavaExtensions;

/**
 * 
 * Sends a notification when a new {@link User} joins the network.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class NewUserNotification extends UserBaseNotification {

	public static void raise(User user) {
		final NewUserNotification notification =
				Notification.createOrReuseNotification(NewUserNotification.class, ImmutableMap.of("user", user));
		notification.user = user;
		notification.save();
		notification.distribute();
	}


	@Override
	public Collection<Long> distribute() {
		postNotification(this, user);
		return new ArrayList<>();
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		return Messages.get("notification.user.new", JavaExtensions.escape(user.getFullName()));
	}
}

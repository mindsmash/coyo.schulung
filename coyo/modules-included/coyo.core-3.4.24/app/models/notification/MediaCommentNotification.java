package models.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.comments.Comment;
import models.media.Media;
import play.i18n.Messages;
import utils.Linkifier;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class MediaCommentNotification extends MediaNotification {

	@Transient
	public Comment comment;

	public static void raise(Media media, Comment newComment) {
		/* create notification */
		final MediaCommentNotification notification = Notification
				.createOrReuseNotification(MediaCommentNotification.class, ImmutableMap.of("media", media));

		notification.user = newComment.author;
		notification.comment = newComment;
		notification.media = media;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		/* inform the author */
		if (media.creator != comment.author) {
			receivers.add(media.creator.id);
		}

		/* inform all other comment authors */
		for (Comment comment : media.comments) {
			if (comment.author != this.comment.author) {
				receivers.add(comment.author.id);
			}
		}

		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(comment.text)) {
			receivers.add(user.id);
		}

		return distributeAsync(receivers);
	}

	/**
	 * Remove user notification if no comments are present
	 * 
	 * @param media
	 */
	public static void cleanup(Media media) {
		if (media.comments.isEmpty()) {
			final MediaCommentNotification notification = MediaCommentNotification.find("media = ?", media).first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> authors = new ArrayList<>();
		final User lastCommentUser = media.comments.size() >= 1 ?
				media.comments.get(media.comments.size() - 1).author : user;

		boolean recipientCommented = (un != null && un.user == lastCommentUser) ? true : false;

		for (Comment comment : media.comments) {
			if (!authors.contains(comment.author) && (un != null && comment.author != un.user)) {
				authors.add(comment.author);
			}
		}

		final StringBuffer sb = new StringBuffer("notifications.media.comment");

		final Integer size;
		if (recipientCommented) {
			size = authors.size();
		} else {
			size = authors.size() - 1;
		}

		final String name;
		if (recipientCommented) {
			name = Messages.get("you");
		} else {
			name = lastCommentUser.getFullName();
		}

		if (authors.size() == 2 || recipientCommented && authors.size() == 1) {
			sb.append(".multi");
		} else if (authors.size() > 2 || recipientCommented && authors.size() == 2) {
			sb.append(".multis");
		} else if (recipientCommented) {
			sb.append(".you");
		}

		final String title = (media.title != null) ? media.title : media.filename;

		return Messages.get(sb.toString(), name, title, size);
	}

}

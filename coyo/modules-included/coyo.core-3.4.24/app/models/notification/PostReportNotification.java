package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Lob;

import org.apache.commons.lang3.StringUtils;

import conf.ApplicationSettings;
import models.Sender;
import models.Settings;
import models.User;
import models.UserNotification;
import models.wall.post.Post;
import notifiers.PostReportMailer;
import play.i18n.Messages;
import util.Util;

/**
 * Sends a notification to all {@link User}s that are admins of the wall that the post is on.
 * 
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class PostReportNotification extends PostNotification {

	@Lob
	public String postReportMessage;

	public PostReportNotification() {
	}

	public static void raise(Post post, String message, User reporter) {
		PostReportNotification notification = new PostReportNotification();
		notification.postReportMessage = message;
		notification.post = post;
		notification.user = reporter;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		try {
			Set<Long> receivers = new HashSet<>();

			// trigger for wall admins
			for (User user : post.wall.sender.getAdmins()) {
				if (user != this.user) {
					receivers.add(user.id);
				}
			}

			// trigger for super admins
			for (User user : User.findSuperadmins()) {
				if (user != this.user) {
					receivers.add(user.id);
				}
			}

			return distributeAsync(receivers);
		} finally {
			// try to send post report notification
			Settings sett = Settings.findApplicationSettings();
			String mailList = "";
			if (sett != null) {
				mailList = sett.getString(ApplicationSettings.REPORTED_POST_MAIL);
			}

			if (StringUtils.isNotEmpty(mailList)) {
				for (String recipient : Util.parseEmailList(
						Settings.findApplicationSettings().getString(ApplicationSettings.REPORTED_POST_MAIL))) {
					if (StringUtils.isNotEmpty(recipient)) {
						PostReportMailer.reportPost(post, postReportMessage, recipient);
					}
				}
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		return Messages.get("notifications.post.report", post.author.getDisplayName(),
				post.wall.sender.getDisplayName(), postReportMessage);
	}

	@Override
	public Sender getThumbnailSender() {
		return post.wall.sender;
	}
}

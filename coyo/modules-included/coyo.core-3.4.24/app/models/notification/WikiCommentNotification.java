package models.notification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.google.common.collect.ImmutableMap;

import models.User;
import models.UserNotification;
import models.app.WikiAppArticle;
import models.app.WikiAppArticleVersion;
import models.comments.Comment;
import play.i18n.Messages;
import utils.Linkifier;

@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WikiCommentNotification extends WikiNotification {

	@Transient
	public Comment wikiComment;

	public static void raise(WikiAppArticle article, Comment newComment) {
		/* create notification */
		final WikiCommentNotification notification = Notification
				.createOrReuseNotification(WikiCommentNotification.class, ImmutableMap.of("wikiArticle", article));

		notification.wikiArticle = article;
		notification.app = article.app;
		notification.user = newComment.author;
		notification.wikiComment = newComment;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		// do not raise notification if the app is inactive
		if (!wikiArticle.app.active) {
			return new ArrayList<>();
		}

		Set<Long> receivers = new HashSet<>();

		/* inform all other comment authors */
		for (Comment comment : wikiArticle.comments) {
			if (comment.author != user) {
				receivers.add(comment.author.id);
			}
		}
		/* inform the last 10 authors */
		for (WikiAppArticleVersion version : wikiArticle.getVersions(10)) {
			if (version.author != user) {
				receivers.add(version.author.id);
			}
		}
		// inform users who were mentioned
		for (User user : Linkifier.getReferencedUsers(wikiComment.text)) {
			receivers.add(user.id);
		}

		return distributeAsync(receivers);
	}

	/**
	 * Remove user notification if no comments are present
	 *
	 * @param article
	 */
	public static void cleanup(WikiAppArticle article) {
		if (article.comments.isEmpty()) {
			final WikiCommentNotification notification =
					WikiCommentNotification.find("wikiArticle = ?", article).first();
			if (notification != null) {
				notification.delete();
			}
		}
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> authors = new ArrayList<>();

		final User lastCommentUser = wikiArticle.comments.get(wikiArticle.comments.size() - 1).author;
		boolean recipientCommented = (un != null && un.user == lastCommentUser) ? true : false;

		for (Comment comment : wikiArticle.comments) {
			if (!authors.contains(comment.author) && (un != null && comment.author != un.user)) {
				authors.add(comment.author);
			}
		}

		final String name;
		if (recipientCommented) {
			name = Messages.get("you");
		} else {
			name = lastCommentUser.getFullName();
		}

		final StringBuffer sb = new StringBuffer("notifications.wiki.comment");
		if (authors.size() == 2 || recipientCommented && authors.size() == 1) {
			sb.append(".multi");
		} else if (authors.size() > 2 || recipientCommented && authors.size() == 2) {
			sb.append(".multis");
		} else if (recipientCommented) {
			sb.append(".you");
		}

		final Integer size;
		if (recipientCommented) {
			size = authors.size();
		} else {
			size = authors.size() - 1;
		}

		return Messages.get(sb.toString(), name, wikiArticle.title, size);
	}
}

package models.notification;

import com.google.common.collect.ImmutableMap;
import extensions.JodaJavaExtensions;
import libs.DateI18N;
import models.User;
import models.UserNotification;
import models.event.Event;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import play.i18n.Messages;

import java.util.Collection;

import javax.persistence.Entity;

/**
 * Send a notification to an {@link User} to remind him about a upcoming
 * {@link Event}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.SINGLE)
public class EventReminderNotification extends EventNotification {

	public static Collection<Long> raise(Event event) {
		final EventReminderNotification notification = Notification
				.createOrReuseNotification(EventReminderNotification.class, ImmutableMap.of("event", event));
		notification.event = event;
		notification.save();

		return notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return event.distributeNotification(this, null);
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		DateTimeZone timezone = (u != null) ? u.getDateTimeZone() : user.getDateTimeZone();
		DateTime start = event.getStartDate(timezone);
		return Messages.get("notifications.event.reminder" + (event.fulltime ? ".fulltime" : ""), event.name,
				JodaJavaExtensions.format(start, DateI18N.getShortFormat()),
				JodaJavaExtensions.format(start, DateI18N.getTimeFormat()));
	}
}
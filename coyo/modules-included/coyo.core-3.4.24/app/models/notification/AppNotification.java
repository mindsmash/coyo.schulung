package models.notification;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.app.App;

/**
 * Notification to extend when an App specific Notification should be thrown.
 * 
 * @author mindsmash GmbH
 */
@Entity
public abstract class AppNotification extends Notification {

	@ManyToOne
	public App app;

	@Override
	public AppNotification save() {
		if (app != null && app.active) {
			return super.save();
		}
		return this;
	}

	@Override
	protected void beforeSave() throws Exception {
		if (!app.active) {
			throw new IllegalStateException("creating notifications for deactivated app [" + app.id
					+ "] is not allowed");
		}

		super.beforeSave();
	}
}

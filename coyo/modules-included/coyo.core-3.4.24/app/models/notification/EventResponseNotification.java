package models.notification;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;

import models.Sender;
import models.User;
import models.UserNotification;
import models.event.Event;
import models.event.EventUser.EventResponse;
import play.i18n.Messages;

/**
 * Sends a notification to the admins of an {@link Event} to inform them about a response by an invited {@link User}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class EventResponseNotification extends EventNotification {

	public EventResponse eventResponse;

	public static Collection<Long> raise(Event event, User user, EventResponse response) {
		final EventResponseNotification notification = Notification.createOrReuseNotification(
				EventResponseNotification.class, ImmutableMap.of("event", event, "eventResponse", response));

		notification.event = event;
		notification.eventResponse = response;
		notification.user = user;
		notification.save();

		return notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();

		// inform event admins
		for (User admin : event.admins) {
			if (admin != user) {
				receivers.add(admin.id);
			}
		}

		return distributeAsync(receivers);
	}

	@Override
	public String getText(UserNotification un) {
		// TODO: Hotfix
		if (eventResponse == null) {
			eventResponse = EventResponse.WAITING;
		}

		final List<User> responder = event.getUsers(eventResponse);
		int count = responder.size() - 1;

		final StringBuffer sb = new StringBuffer("notifications.event.response");

		if (responder.size() < 1 || responder.size() == 1 && (un != null && responder.get(0) == un.user)) {
			sb.append(".none");

		} else if (responder.size() == 2) {
			sb.append(".multi");

		} else if (responder.size() > 2) {
			sb.append(".multis");

		} else if (!responder.contains(user) && responder.size() > 1) {
			sb.append(".unknown");
			count = responder.size();
		}

		return Messages.get(sb.toString(), user.getFullName(), event.name, eventResponse.getLabel(), count);
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}
}

package models.notification;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.Sender;
import models.media.Media;

@Entity
public abstract class MediaNotification extends Notification {
	
	@ManyToOne
	public Media media;

	public MediaNotification() {
		this.displayType = DisplayType.BOTH;
	}

	/*
	 * TODO Redirect to media item itself
	 * 
	 * (non-Javadoc)
	 * @see models.notification.Notification#redirectToSource()
	 */
	@Override
	public void redirectToSource() {
		media.redirectToResult();
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}
}
package models.notification;

import controllers.WikiApp;
import models.Sender;
import models.User;
import models.app.WikiAppArticle;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public abstract class WikiNotification extends AppNotification {

	@ManyToOne(fetch = FetchType.LAZY)
	public WikiAppArticle wikiArticle;

	protected WikiNotification() {
		this.displayType = DisplayType.BOTH;
	}

	@Override
	public void redirectToSource() {
		WikiApp.article(wikiArticle.id);
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}
}

package models.notification;

import javax.persistence.Entity;

import models.Sender;
import models.User;
import controllers.Users;

/**
 * 
 * This notification is used to notify an {@link User} when something happens to
 * his wall.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
public abstract class UserBaseNotification extends Notification {

	@Override
	public void redirectToSource() {
		Users.wall(user.id, user.getSlug());
	}

	@Override
	public Sender getThumbnailSender() {
		return user;
	}
}

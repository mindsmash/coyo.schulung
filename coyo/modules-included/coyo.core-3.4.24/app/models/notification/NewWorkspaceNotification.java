package models.notification;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import play.i18n.Messages;
import play.templates.JavaExtensions;

import javax.persistence.Entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Sends a notification when a new {@link Workspace} is created.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class NewWorkspaceNotification extends WorkspaceNotification {

	public static void raise(Workspace workspace) {
		final NewWorkspaceNotification notification = Notification
				.createOrReuseNotification(NewWorkspaceNotification.class, ImmutableMap.of("workspace", workspace));

		notification.workspace = workspace;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		postNotification(this, workspace);
		return new ArrayList<>();
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		return Messages.get("notification.workspace.new", JavaExtensions.escape(workspace.creator.getDisplayName()),
				JavaExtensions.escape(workspace.name));
	}
}

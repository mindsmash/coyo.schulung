package models.notification;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.workspace.Workspace;
import play.i18n.Messages;

import javax.persistence.Entity;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Informs an {@link User} when he is invited to join a workspace.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
public class WorkspaceInvitationNotification extends WorkspaceNotification {

	public WorkspaceInvitationNotification() {
	}

	public static void raise(Workspace workspace, User user) {
		final WorkspaceInvitationNotification notification =
				Notification.createOrReuseNotification(WorkspaceInvitationNotification.class, ImmutableMap
						.of("workspace", workspace, "user", user));

		notification.workspace = workspace;
		notification.user = user;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		return distributeAsync(user);
	}

	@Override
	public String getText(UserNotification un) {
		if(workspace.isAdmin(un.user)) {
			return Messages.get("notification.workspace.invitation.admin", workspace.name);
		}
		return Messages.get("notification.workspace.invitation", workspace.name);
	}
}

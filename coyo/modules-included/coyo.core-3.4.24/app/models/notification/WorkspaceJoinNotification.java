package models.notification;

import com.google.common.collect.ImmutableMap;
import injection.Inject;
import injection.InjectionSupport;
import models.User;
import models.UserNotification;
import models.dao.WorkspaceDao;
import models.workspace.Workspace;
import models.workspace.WorkspaceMember;
import play.i18n.Messages;
import session.UserLoader;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Informs all {@link Workspace} members when a new {@link User} joins the
 * {@link Workspace}.
 *
 * @author mindsmash GmbH
 */
@Entity
@NotificationType(configurable = true, distributionType = DistributeType.REUSE)
@InjectionSupport
public class WorkspaceJoinNotification extends WorkspaceNotification {

	@Inject(configuration="coyo.di.dao.workspace", defaultClass = WorkspaceDao.class, singleton = true)
	private static WorkspaceDao workspaceDao;

	public static void raise(Workspace workspace, User user) {
		final WorkspaceJoinNotification notification = Notification
				.createOrReuseNotification(WorkspaceJoinNotification.class, ImmutableMap.of("workspace", workspace));

		notification.user = user;
		notification.workspace = workspace;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		Set<Long> receivers = new HashSet<>();
		
		for (User admin : workspace.getAdmins()) {
			receivers.add(admin.id);
		}
		
		return distributeAsync(receivers);
	}

	@Override
	public String getText(UserNotification un) {
		final List<User> joined = new ArrayList<>();
		List<User> users = workspaceDao.getActiveWorkspaceMembers(this.workspace.id);
		final User connectedUser = UserLoader.getConnectedUser(false);

		for (User user : users) {
			if (!this.workspace.creator.equals(user) && connectedUser != user) {
				joined.add(user);
			}
		}

		if (joined.isEmpty()) {
			if (!this.userNotifications.isEmpty()) {
				while (this.userNotifications.size() != 0) {
					UserNotification toDel = this.userNotifications.get(0);
					this.userNotifications.remove(toDel);
					toDel.delete();
				}
			}
			return Messages.get("notification.workspace.greetings");
		}

		final StringBuffer sb = new StringBuffer("notification.workspace.join");
		int numberOfJoins = joined.size();
		String names = "";

		if (numberOfJoins == 0) {
			return Messages.get("notification.notfound");

		} else if (numberOfJoins == 1) {
			sb.append(".single");
			names = joined.get(0).getFullName();

		} else if (numberOfJoins <= 3) {
			sb.append(".few");
			if (numberOfJoins == 2) {
				names = joined.get(0).getFullName() +
						" " + Messages.get("and") +
						" " + joined.get(1).getFullName();
				numberOfJoins -= 2;
			} else if (numberOfJoins > 2) {
				names = joined.get(0).getFullName() +
						", " + joined.get(1).getFullName() +
						" " + Messages.get("and") +
						" " + joined.get(2).getFullName();
				numberOfJoins -= 3;
			}

		} else {
			sb.append(".many");
			names = joined.get(0).getFullName() +
					", " + joined.get(1).getFullName() +
					", " + joined.get(2).getFullName();
			numberOfJoins -= 3;
			if(numberOfJoins > 1) {
				sb.append("s");
			}
		}

		return Messages.get(sb.toString(), this.workspace.getDisplayName(), names, numberOfJoins);
	}
}

package models.notification;

import javax.persistence.Entity;

import com.google.common.collect.ImmutableMap;
import models.User;
import models.UserNotification;
import models.page.Page;
import play.i18n.Messages;
import play.templates.JavaExtensions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * Sends a notification when an {@link Page} is created.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@NotificationType(distributionType = DistributeType.SINGLE)
public class NewPageNotification extends PageNotification {

	public static void raise(Page page) {
		final NewPageNotification notification = new NewPageNotification();

		notification.page = page;
		notification.save();

		notification.distribute();
	}

	@Override
	public Collection<Long> distribute() {
		postNotification(this, page);
		return new ArrayList<>();
	}

	@Override
	public String getText(UserNotification un) {
		return getText(un != null ? un.user : null);
	}

	public String getText(User u) {
		return Messages.get("notification.page.new", JavaExtensions.escape(page.creator.getDisplayName()),
				JavaExtensions.escape(page.name));
	}
}

package models.event;

import java.text.ParseException;
import java.util.Date;

import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.property.RRule;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.Logger;
import play.i18n.Messages;
import utils.DateUtils;
import extensions.JodaJavaExtensions;

/**
 * @author Benjamin Hintz, mindsmash GmbH
 */
public class RecurrenceRuleInfo {

	public String frequency;
	public String interval;
	public String until;
	public Date untilDate;
	public String weekdayList;

	public RecurrenceRuleInfo(String recurrenceRule, DateTimeZone dateTimeZone) {
		try {
			Recur recur = new RRule(recurrenceRule).getRecur();
			frequency = Messages.get("event.series.rrule.freq." + recur.getFrequency() + ".label");
			interval = Messages.get(String.format("event.series.rrule.freq.%s.%s.help", recur.getFrequency(),
					recur.getInterval() > 1 ? "multiple" : 1), recur.getInterval());
			until = recur.getUntil() == null ? Messages.get("event.series.rrule.end.NEVER.label") : Messages
					.get("until") + " " + JodaJavaExtensions.format(new DateTime(recur.getUntil(), dateTimeZone));
			weekdayList = DateUtils.getDescriptiveWeekdayList(recur.getDayList());
			untilDate = recur.getUntil();
		} catch (ParseException e) {
			Logger.warn(e, "could not extract recurrence rule information from recurrence string [%s]", recurrenceRule);
		}

	}
}

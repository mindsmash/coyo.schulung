package models.event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.User;
import play.i18n.Messages;

/**
 * Maps users to events.
 * 
 * @author Jan Marquardt
 */
@Entity
@Table(name = "event_user")
public class EventUser extends TenantModel {

	public enum EventResponse {
		ATTENDING("icon-ok-sign", true), MAYBE("icon-question-sign", true), WAITING("icon-time", false), NOT_ATTENDING(
				"icon-remove-sign", false);

		public String icon;
		public boolean following;

		private EventResponse(String icon, boolean following) {
			this.icon = icon;
			this.following = following;
		}

		public String getLabel() {
			return Messages.get("event.response." + this);
		}

		public String getStatus() {
			return Messages.get("event.responded." + this);
		}

		/**
		 * Checks if this response status should receive mails and event updates
		 */
		public boolean receiveUpdates() {
			return following;
		}
	}

	@ManyToOne(optional = false)
	public User user;

	@ManyToOne(optional = false)
	public EventBase event;

	@Column(nullable = false)
	public EventResponse response = EventResponse.WAITING;

	public EventUser(User user, EventBase event) {
		this.user = user;
		this.event = event;
	}

	@Override
	public String toString() {
		return "EventUser[user=" + user.id + ", response=" + response + "]";
	}
}

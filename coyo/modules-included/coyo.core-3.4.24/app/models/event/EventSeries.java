package models.event;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fishr.Indexed;
import models.User;
import models.event.EventUser.EventResponse;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import play.data.binding.NoBinding;
import play.data.validation.Required;
import storage.FlexibleBlob;
import utils.DateUtils;

/**
 * Represents the "template" for an event series (or recurring event) which is
 * used to create the series' child instances.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
@Entity
@Indexed
@Table(name = "event_series")
public class EventSeries extends EventBase {
	/*
	 * Constructs a new event series based on an existing event series child. It
	 * also resets the child and adds it to the series as first child.
	 */
	public static EventSeries create(final EventSeriesChild templateEvent, final String recurrenceRule) {
		final EventSeries series = new EventSeries(templateEvent.creator, recurrenceRule);
		series.fulltime = templateEvent.fulltime;
		series.startDate = templateEvent.startDate;
		series.endDate = templateEvent.endDate;

		series.about = templateEvent.about;
		series.admins = new ArrayList<User>(templateEvent.admins);
		series.allowGuestInvite = templateEvent.allowGuestInvite;

		series.avatar = new FlexibleBlob();
		series.thumbnail = new FlexibleBlob();

		series.calendar = templateEvent.calendar;
		series.creator = templateEvent.creator;
		series.location = templateEvent.location;
		series.name = templateEvent.name;
		series.sendReminder = templateEvent.sendReminder;
		series.suppressMaybe = templateEvent.suppressMaybe;
		series.users = EventBase.copyUsers(series, templateEvent);
		series.visibility = templateEvent.visibility;

		// reset child and move it to the newly created series
		templateEvent.reset();
		templateEvent.series = series;
		series.children.add(templateEvent);

		return series;
	}

	public static List<Event> createChildEvents(final EventSeries master, final Interval interval) {
		final List<Interval> recurrenceSet = DateUtils.getRecurrenceSet(master.recurrenceRule, master.startDate,
				master.endDate, master.getCreationTimeZone(), interval);
		final List<Event> children = new ArrayList<Event>(recurrenceSet.size());
		for (final Interval recurringEventInterval : recurrenceSet) {
			children.add(master.createChildEvent(recurringEventInterval));
		}
		return children;
	}

	@NoBinding
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "series", fetch = FetchType.LAZY)
	public List<EventSeriesChild> children = new ArrayList<EventSeriesChild>();

	/**
	 * A RFC 2445 (iCalendar) recurrence rule (RRULE).
	 */
	@Column(nullable = false)
	@Required
	public String recurrenceRule;

	/**
	 * The date until which the master's children were generated (inclusively).
	 */
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime childrenGeneratedUntil;

	/**
	 * Indicates whether the event series has been cancelled.
	 */
	@Column(nullable = false)
	public boolean isCancelled;

	public EventSeries(final User creator, final String recurrenceRule) {
		super(creator);
		this.recurrenceRule = recurrenceRule;
	}

	public EventSeriesChild createChildEvent() {
		return createChildEvent(null);
	}

	public EventSeriesChild createChildEvent(final Interval interval) {
		EventSeriesChild childEvent = null;
		if (interval != null) {
			childEvent = new EventSeriesChild(this, interval.getStart(), interval.getEnd());
		} else {
			childEvent = new EventSeriesChild(this);
		}
		// auto-attend creator
		childEvent.invite(childEvent.creator);
		childEvent.respond(childEvent.creator, EventResponse.ATTENDING);
		return childEvent;
	}

	public void removeChildrenStartingWith(final EventSeriesChild firstChildToRemove) {
		for (final Iterator<EventSeriesChild> iterator = children.iterator(); iterator.hasNext();) {
			final EventSeriesChild child = iterator.next();
			if (child == firstChildToRemove || child.startDate.isAfter(firstChildToRemove.startDate)) {
				// NOTE: its not sufficient to remove children from the list here - we have to delete them explicitly!
				iterator.remove();
				child.delete();
			}
		}
	}

	public void respond(final User user, final EventResponse response, final EventSeriesChild firstChildToRespond) {
		// save response to event series and the given children
		inviteAndRespond(user, response);

		for (final EventSeriesChild eventSeriesChild : EventSeriesChild.find("series = ? AND startDate >= ?", this,
				firstChildToRespond.startDate).<EventSeriesChild> fetch()) {
			eventSeriesChild.inviteAndRespond(user, response);
		}
	}

	public long getChildrenCount() {
		return EventSeriesChild.count("series = ?", this);
	}

	public boolean childrenAreNotGeneratedYet() {
		return childrenGeneratedUntil == null;
	}

	@Override
	protected void updateFollowers() {
		// only the children have followers
	}

	/**
	 * When generating children for an event series, we only want to create
	 * instances up to a certain point in time in the future in order to limit
	 * the number of events to create.
	 * 
	 * This method returns the amount of years to go in the future depending on
	 * the given recurrence rule or rather its frequency:
	 * 
	 * <ul>
	 * <li>1 year for DAILY and WEEKLY events,</li>
	 * <li>3 years for MONTHLY events</li>
	 * <li>and 10 years for YEARLY events.</li>
	 * </ul>
	 */
	public static int getNumberOfYearsForChildGeneration(String recurrenceRule) {
		int plusYears = 1;
		switch (DateUtils.getRecurrenceFrequency(recurrenceRule)) {
		case YEARLY:
			plusYears = 10;
			break;

		case MONTHLY:
			plusYears = 3;
			break;
		}
		return plusYears;
	}

	@Override
	public void _delete() {
		for (Event event : children) {
			event._delete();
		}
		children.clear();

		super._delete();
	}
}

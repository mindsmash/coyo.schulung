package models.event;

import javax.persistence.Entity;
import javax.persistence.Table;

import models.CallContext;
import models.User;
import models.event.EventUser.EventResponse;
import play.mvc.Router;
import plugins.PluginEvents;
import plugins.PluginManager;
import acl.Permission;
import controllers.Security;
import fishr.Indexed;
import fishr.search.Searchable;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a single event.
 */
@Entity
@Indexed
@Searchable(category = "events")
@Table(name = "event")
public class Event extends EventBase implements CallContext {

	public Event() {
	}

	public Event(final User creator) {
		this.creator = creator;
		// add creator as attending user to event
		final EventUser eu = new EventUser(creator, this);
		eu.response = EventResponse.ATTENDING;
		this.users.add(eu);
		this.admins.add(creator);
	}

	/**
	 * Constructs a new event based on the given template event by adopting all relevant fields.
	 */
	public Event(final User creator, final Event templateEvent) {
		this(creator);
		this.about = templateEvent.about;
		this.admins = null; // will be populated before save
		this.allowGuestInvite = templateEvent.allowGuestInvite;
		this.calendar = templateEvent.calendar != null
				&& Security.check(Permission.CREATE_EVENT, templateEvent.calendar.id) ? templateEvent.calendar : null;
		this.endDate = templateEvent.endDate;
		this.external = templateEvent.external;
		this.fulltime = templateEvent.fulltime;
		this.location = templateEvent.location;
		this.name = templateEvent.name;
		this.sendReminder = templateEvent.sendReminder;
		this.startDate = templateEvent.startDate;
		this.suppressMaybe = templateEvent.suppressMaybe;
		this.users = EventBase.copyUsers(this, templateEvent);
		this.visibility = templateEvent.visibility;
	}

	@Override
	protected void updateFollowers() {
		followers.clear();

		for (EventResponse responseType : EventResponse.values()) {
			if (responseType.following) {
				if (isPersistent()) {
					followers.addAll(getUsers(responseType));
				} else {
					for (EventUser eu : users) {
						if (eu.response == responseType) {
							followers.add(eu.user);
						}
					}
				}
			}
		}
		followers.addAll(getAdmins());
		if (calendar != null) {
			followers.addAll(calendar.sender.followers);
		}
	}

	@Override
	public void _save() {
		// plugins
		PluginManager.raiseEvent(PluginEvents.EVENT_SAVE, this);

		super._save();
	}

	@Override public String getURL() {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("slug", getSlug());

		return Router.reverse("Events.show", args).url;
	}

	@Override public String getTitle() {
		return getDisplayName();
	}

}

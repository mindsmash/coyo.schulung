package models.event;

import extensions.JodaJavaExtensions;
import fishr.Field;
import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import libs.DateI18N;
import models.Sender;
import models.User;
import models.app.CalendarApp;
import models.dao.EventDao;
import models.event.EventUser.EventResponse;
import models.helper.ClearManyToMany;
import models.notification.EventNotification;
import models.wall.Wall;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import play.Logger;
import play.data.binding.NoBinding;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import play.i18n.Messages;
import session.UserLoader;
import utils.DateUtils;
import acl.Permission;
import checks.EventEndDateCheck;
import controllers.Security;
import controllers.administration.ThemeSettings;

@Entity
@Table(name = "event_base")
@InjectionSupport
public abstract class EventBase extends Sender {
	
	public static final String EVENT_ADMIN_TABLE = "event_admin";

	public enum EventVisibility {
		PUBLIC("globe"), CLOSED("lock"), PRIVATE("eye-close");

		public String icon;

		private EventVisibility(String icon) {
			this.icon = icon;
		}
		
		public String getLabel() {
			return Messages.get("event.visibility." + this + ".label");
		}

		public String getDescription() {
			return Messages.get("event.visibility." + this + ".description");
		}
	}

	@Column(nullable = false)
	@Required
	public EventVisibility visibility = EventVisibility.PUBLIC;

	@NoBinding
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public User creator;

	private String creationTimezone;
	
	@Inject(configuration="coyo.di.dao.event", defaultClass = EventDao.class, singleton = true)
	private static EventDao eventDao;

	@ManyToOne(fetch = FetchType.LAZY)
	public CalendarApp calendar;

	@Required
	@Column(nullable = false)
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime startDate;

	@Required
	@CheckWith(EventEndDateCheck.class)
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime endDate;

	public boolean fulltime = false;

	@Field
	@MaxSize(50)
	public String name;

	@Field
	@MaxSize(255)
	public String location;

	public Boolean suppressMaybe = false;

	public Boolean allowGuestInvite = true;

	public Integer sendReminder;

	@NoBinding
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
	public List<EventUser> users = new ArrayList<EventUser>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = EVENT_ADMIN_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> admins = new ArrayList<User>();

	@NoBinding
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
	public List<EventNotification> notifications = new ArrayList<EventNotification>();

	/**
	 * @return The time zone that the creator was presuming during creation. If no timezone is available, the current
	 *         creator's timezone will be used.
	 */
	public DateTimeZone getCreationTimeZone() {
		if (!StringUtils.isEmpty(creationTimezone)) {
			try {
				return DateTimeZone.forID(creationTimezone);
			} catch (IllegalArgumentException ignored) {
			}
		}
		if (creator != null) {
			DateTimeZone dtz = creator.getDateTimeZone();
			if (dtz != null) {
				return dtz;
			}
		}
		return DateTimeZone.UTC;
	}

	protected static List<EventUser> copyUsers(final EventBase target, final EventBase source,
			final boolean copyResponse) {
		final ArrayList<EventUser> copied = new ArrayList<EventUser>(source.users.size());
		for (final EventUser eventUser : source.users) {
			final EventUser eventUserCopy = new EventUser(eventUser.user, target);
			if (copyResponse) {
				eventUserCopy.response = eventUser.response;
			}
			copied.add(eventUserCopy);
		}
		return copied;
	}

	protected static List<EventUser> copyUsers(final EventBase target, final EventBase source) {
		return EventBase.copyUsers(target, source, false);
	}

	public DateTime getStartDate() {
		// hibernate doesn't use UTC automatically
		return getStartDate(DateTimeZone.UTC);
	}

	public DateTime getEndDate() {
		// hibernate doesn't use UTC automatically
		return getEndDate(DateTimeZone.UTC);
	}

	public DateTime getStartDate(final DateTimeZone zone) {
		if (startDate == null) {
			return null;
		}
		if (fulltime) {
			return startDate.withZone(DateTimeZone.UTC).withZoneRetainFields(zone);
		}
		return startDate.withZone(zone);
	}

	public DateTime getEndDate(final DateTimeZone zone) {
		if (endDate == null) {
			return null;
		}
		if (fulltime) {
			return endDate.withZone(DateTimeZone.UTC).withZoneRetainFields(zone);
		}
		return endDate.withZone(zone);
	}

	/**
	 * @param day
	 * @param zone
	 * @return True if this event ends on given day.
	 */
	public boolean ends(DateTime day, final DateTimeZone zone) {
		final DateTime endDate = getEndDate(zone);
		if (endDate != null) {
			return endDate.getYear() == day.getYear() && endDate.getDayOfYear() == day.getDayOfYear();
		}
		return false;
	}

	public boolean starts(DateTime day, final DateTimeZone zone) {
		final DateTime startDate = getStartDate(zone);
		if (startDate != null) {
			return startDate.getYear() == day.getYear() && startDate.getDayOfYear() == day.getDayOfYear();
		}
		return false;
	}

	public boolean inFuture() {
		// find date to check
		DateTime date = endDate;
		if (date == null) {
			date = startDate;
		}

		return date.isAfterNow();
	}

	public boolean inPast() {
		// find date to check
		DateTime date = endDate;
		if (date == null) {
			date = startDate;
		}
		if (fulltime) {
			date = date.plusDays(1).withMillisOfDay(0);
		}
		return !date.isAfterNow();
	}

	public List<User> getInvitedUsers() {
		return eventDao.getInvitedUsers(this);
	}

	public List<User> getUsers(EventResponse response) {
		return eventDao.getUsers(this, response);
	}

	public long getUserCount(EventResponse response) {
		return eventDao.getUserCount(this, response);
	}

	/**
	 * @return True if the user has been invited, false if the user was already invited.
	 */
	public boolean invite(User user) {
		if (!isInvited(user)) {
			EventUser eu = new EventUser(user, this);
			eu.save();
			refresh();
			return true;
		}
		return false;
	}

	public void respond(User user, EventResponse response) {
		EventUser eu = getEventUser(user);
		if (eu != null) {
			eu.response = response;

			// TODO: try to remove the persistency check
			if (isPersistent()) {
				eu.save();
			}
		}
	}

	protected EventUser getEventUser(User user) {
		// TODO: try to remove
		if (!isPersistent()) {
			for (EventUser eu : users) {
				if (eu.user == user) {
					return eu;
				}
			}
			return null;
		}

		return EventUser.find("event = ? AND user = ?", this, user).first();
	}

	protected void inviteAndRespond(User user, EventResponse response) {
		invite(user);
		respond(user, response);
	}

	public EventResponse getResponse(User user) {
		EventUser eu = getEventUser(user);
		if (eu != null) {
			return eu.response;
		}
		return null;
	}

	public boolean isInvited(User user) {
		return getEventUser(user) != null;
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public Wall getDefaultWall() {
		// create default wall
		if (walls.size() == 0) {
			Wall.inject(this);
		}
		return walls.get(0);
	}

	/**
	 * Returns the number of upcoming events. Upcoming in this sense means, that the event is either starting or ending
	 * after the start of today (yesterday midnight).
	 * 
	 * @param timeZone
	 *            The timezone to use for all dates.
	 * @param filter
	 *            Supply if you want to apply a filter.
	 * @return number of upcoming events or '0' if no events could be found.
	 */
	public static long getUpcomingEventsCount(DateTimeZone timeZone, CalendarFilter filter) {
		JpqlSelect select = new JpqlSelect();
		select.from("Event e");
		applyWhereFutureEvent(select, timeZone);
		if (filter != null) {
			filter.apply(select);
		}
		return Event.count(select.toString(), select.getParams().toArray());
	}

	/**
	 * Returns all upcoming events for the given sender (e.g. page or workspace). Upcoming in this sense means, that the
	 * event is either starting or ending after the start of today (yesterday midnight).
	 * 
	 * @param timeZone
	 *            The timezone to use for all dates.
	 * @param sender
	 *            The sender the events should be fetched for.
	 * @param limit
	 *            Defines how many events should be found. This parameter is not optional.
	 * @return All upcoming events for the given sender
	 */
	public static List<Event> getUpcomingEventsForSender(DateTimeZone timeZone, Sender sender, int limit) {
		JpqlSelect select = new JpqlSelect();
		select.from("Event e");
		applyWhereFutureEvent(select, timeZone);
		select.andWhere("e.calendar.sender = ?").param(sender);
		select.andWhere("e.calendar.active = true");
		select.orderBy("e.startDate,e.endDate");

		JPAQuery query = Event.find(select.toString(), select.getParams().toArray());

		if (limit > -1) {
			return query.fetch(limit);
		}
		return query.fetch();
	}

	/**
	 * Returns all upcoming events for the given user. Upcoming in this sense means, that the event is either starting
	 * or ending after the start of today (yesterday midnight). All events are set to use the user's timezone
	 * implicitly.
	 * 
	 * @param user
	 *            The user the events should be fetched for.
	 * @param limit
	 *            Defines how many events should be found. This parameter is not optional.
	 * @return All upcoming events for the given user.
	 */
	public static List<Event> getUpcomingEventsForUser(User user, int limit) {
		JpqlSelect select = new JpqlSelect();
		select.select("e FROM Event e LEFT OUTER JOIN e.calendar calendar");
		applyWhereFutureEvent(select, user.getDateTimeZone());
		select.andWhere(
				"(EXISTS (SELECT eu FROM EventUser eu WHERE eu.event.id = e.id AND eu.user.id = ? AND eu.response = ?))")
				.params(user.id, EventResponse.ATTENDING);
		select.andWhere("(calendar.active = true)");
		select.orderBy("e.startDate,e.endDate");
		return Event.find(select.toString(), select.getParams().toArray()).fetch(limit);
	}

	/**
	 * Fetches upcoming events and checks list permissions. Upcoming in this sense means, that the event is either
	 * starting or ending after the start of today (yesterday midnight). This method guarantees that the set amount of
	 * valid events is returned if this amount exists.
	 * 
	 * @param timeZone
	 * @param interestedUser
	 *            Supply this if you want to only get events, that the given user is interested in.
	 * @param filter
	 *            Supply if you want to apply a filter.
	 * @param limit
	 *            Defines how many events should be found. This parameter is not optional, since it would be a big
	 *            performance drain to look for all events.
	 * @return A set amount of upcoming events
	 */
	public static List<Event> getUpcomingEvents(final DateTimeZone timeZone, final User interestedUser,
			CalendarFilter filter, int limit) {
		JpqlSelect select = new JpqlSelect();
		select.select("e FROM Event e LEFT OUTER JOIN e.calendar calendar");
		applyWhereFutureEvent(select, timeZone);
		if (interestedUser != null) {
			select.andWhere("(calendar IS NULL OR (? MEMBER OF calendar.sender.followers))").param(interestedUser);
		}

		select.andWhere("(calendar.active = true)");
		select.andWhere("e.startDate >= now()");

		select.orderBy("e.startDate,e.endDate");

		if (filter != null) {
			filter.apply(select);
		}

		final List<Event> result = new ArrayList<Event>();
		List<Event> batchResult = new ArrayList<Event>();

		/*
		 * Fetch the events in batches depending on the limit size. We have to do since we only can check the complex
		 * permissions on fetched entities. If we have to throw out some events due to insufficient permissions, we will
		 * query more events dynamically. This way we can guarantee an amount of "limit" events.
		 */
		int batch = (int) (limit + (limit / 2));
		int page = 0;

		do {
			page++;
			batchResult = Event.find(select.toString(), select.getParams().toArray()).fetch(page, batch);
			for (final Event event : batchResult) {
				if (Security.check(Permission.LIST_EVENT, event.id)) {
					result.add(event);
					if (result.size() == limit) {
						break; // early break if we have enough events
					}
				}
			}
		} while (result.size() < limit && !batchResult.isEmpty());

		Logger.debug("Found '%d' events in '%d' iterations with a batch size of '%d' and a set limit of '%d'.",
				result.size(), page, batch, limit);

		return result;
	}

	/**
	 * @see #getUpcomingEvents(DateTimeZone, User, CalendarFilter, int)
	 */
	public static List<Event> getUpcomingEvents(final DateTimeZone timeZone, final User interestedUser, int limit) {
		return getUpcomingEvents(timeZone, interestedUser, null, limit);
	}

	/**
	 * @see #getUpcomingEvents(DateTimeZone, User, CalendarFilter, int)
	 */
	public static List<Event> getUpcomingEvents(DateTimeZone timeZone, CalendarFilter filter, int limit) {
		return getUpcomingEvents(timeZone, null, filter, limit);
	}

	/**
	 * Returns upcoming events for the passed timezone and filter and applies a paging. Upcoming in this sense means,
	 * that the event is either starting or ending after the start of today (yesterday midnight). Page starts at '1'.
	 * Note that this method does not guarantee that every returned event is valid. No permission check is performed.
	 * 
	 * @param timeZone
	 *            Timezone of the user
	 * @param filter
	 *            The filter that should be applied.
	 * @param page
	 *            Page to start with when fetching results. Starts with page '1'.
	 * @param length
	 *            amount of events to fetch.
	 * @return List of upcoming event or empty list of no events could be found.
	 */
	public static List<Event> getUpcomingEventsWithPaging(DateTimeZone timeZone, CalendarFilter filter, int page,
			int length) {
		JpqlSelect select = new JpqlSelect();
		select.from("Event e");
		applyWhereFutureEvent(select, timeZone);
		select.orderBy("e.startDate,e.endDate");

		if (filter != null) {
			filter.apply(select);
		}

		return Event.find(select.toString(), select.getParams().toArray()).fetch(page, length);
	}
	
	/**
	 * Returns the amount of events scheduled today. It takes all events into consideration that started today, ended
	 * today or are still ongoing today.
	 */
	public static long getTodayEventCount() {
		final DateTime todayUTC = DateUtils.getTodayBorder(DateTimeZone.UTC);
		final DateTime tommorrowUTC = DateUtils.getDateBorder(new DateTime().plusDays(1), DateTimeZone.UTC);
		return Event.count("(e.endDate >= ? AND e.startDate <= ?)", todayUTC, tommorrowUTC);
	}

	private static void applyWhereFutureEvent(JpqlSelect select, DateTimeZone timeZone) {
		final DateTime todayUTC = DateUtils.getTodayBorder(DateTimeZone.UTC);
		final DateTime todayUser = DateUtils.getTodayBorder(timeZone);
		select.where(
				"((e.fulltime = true AND (e.endDate >= ? OR e.startDate >= ?)) OR (e.fulltime = false AND (e.endDate >= ? OR e.startDate >= ?)))")
				.params(todayUTC, todayUTC, todayUser, todayUser);
	}

	public static List<Event> getArchivedEvents(final DateTimeZone timeZone) {
		// TODO : cache for one day
		final DateTime todayUTC = DateUtils.getTodayBorder(DateTimeZone.UTC);
		final DateTime todayUser = DateUtils.getTodayBorder(timeZone);

		return Event
				.find("(fulltime = true AND (startDate < ? AND (endDate IS NULL OR endDate < ?))) OR (fulltime = false AND (startDate < ? AND (endDate IS NULL OR endDate < ?))) ORDER BY startDate,endDate  ASC",
						todayUTC, todayUTC, todayUser, todayUser).fetch();
	}

	/**
	 * Fetches all events of a week, grouped by the day of the week.
	 * 
	 * @param dayInWeek
	 *            Any day in the week.
	 * @param timeZone
	 *            Timezone of the user
	 * @param filter
	 * @return Map with 7 sets
	 */
	public static Map<LocalDate, Set<Event>> getWeekEvents(DateTime dayInWeek, DateTimeZone timeZone,
			CalendarFilter filter) {
		final DateTime startUTC = DateUtils.getDateBorder(dayInWeek.withDayOfWeek(1), DateTimeZone.UTC);
		final DateTime endUTC = startUTC.withDayOfWeek(7).plusDays(1).minusMillis(1);
		final DateTime startUser = DateUtils.getDateBorder(dayInWeek.withDayOfWeek(1), timeZone);
		final DateTime endUser = startUser.withDayOfWeek(7).plusDays(1).minusMillis(1);

		// query for all events in week
		JpqlSelect select = new JpqlSelect();
		select.select("e").from("Event e").orderBy("startDate,endDate");
		select.where("((fulltime = true AND (startDate >= ? AND startDate <= ?) OR (endDate >= ? AND endDate <= ?) OR (startDate <= ? AND endDate >= ?)) OR (fulltime = false AND (startDate >= ? AND startDate <= ?) OR (endDate >= ? AND endDate <= ?) OR (startDate <= ? AND endDate >= ?)))");
		select.params(startUTC, endUTC, startUTC, endUTC, startUTC, endUTC, startUser, endUser, startUser, endUser,
				startUser, endUser);

		filter.apply(select);

		// prepare result
		Map<LocalDate, Set<Event>> result = new HashMap<>();
		for (int i = 1; i <= 7; i++) {
			result.put(dayInWeek.withDayOfWeek(i).toLocalDate(), new LinkedHashSet<Event>());
		}

		// check permissions
		List<Event> fetched = Event.find(select.toString(), select.getParams().toArray()).fetch();
		for (final Event event : fetched) {
			if (Security.check(Permission.LIST_EVENT, event.id)) {
				// put into correct weekday list
				LocalDate startDate = event.getStartDate(timeZone).toLocalDate();
				LocalDate endDate = event.getEndDate(timeZone).toLocalDate();

				LocalDate current = startDate;
				while (!current.isAfter(endDate)) {
					if (result.containsKey(current)) {
						result.get(current).add(event);
					}
					current = current.plusDays(1);
				}
			}
		}

		return result;
	}

	@Override
	public boolean isSharable() {
		return visibility != EventVisibility.PRIVATE;
	}

	public EventBase() {
		super();
	}

	public EventBase(final User creator) {
		this.creator = creator;
	}

	@Override
	protected void beforeSave() throws Exception {
		// avoid zero-admins
		if (admins == null) {
			admins = new ArrayList<>();
		}
		if (admins.size() == 0) {
			admins.add(creator);
		}
		super.beforeSave();
	}

	@Override
	public List<User> getAdmins() {
		return admins;
	}

	@Override
	public String getSenderType() {
		return "event";
	}

	public String getColor() {
		if (calendar != null) {
			return calendar.color;
		}
		return ThemeSettings.getThemeSettings().getString("themeBaseColor");
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + EVENT_ADMIN_TABLE + " WHERE event_base_id = " + id).executeUpdate();

		super.beforeDelete();
	}

	public boolean isEventSeriesChild() {
		return false;
	}

	@Override
	public String getSearchInfoText() {
		return JodaJavaExtensions.format(getStartDate(UserLoader.getConnectedUser().getDateTimeZone()),
				DateI18N.getFullFormat());
	}
	
	@Override
	public String getVisibilityIcon() {
		return visibility.icon;
	}
	
	@Override
	public String getVisibilityText() {
		return visibility.getLabel();
	}
}

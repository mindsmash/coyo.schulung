package models.event;

import java.util.ArrayList;
import java.util.List;

import models.User;
import models.app.CalendarApp;

import org.apache.commons.lang.StringUtils;

import play.db.helper.JpqlSelect;
import play.db.helper.SqlSelect.Where;
import play.mvc.Http.Request;
import util.ModelUtil;
import util.Util;
import utils.JPAUtils;
import acl.Permission;
import controllers.Security;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class CalendarFilter {

	private static final String EVENTS_FILTER_KEY = "eventsFilter";

	public boolean showGlobal;
	public List<CalendarApp> showCalendars = new ArrayList<>();

	/**
	 * Applies this filter to an Event-select.
	 * 
	 * @param select
	 */
	public void apply(JpqlSelect select) {
		Where filterBlock = select.where();
		if (showCalendars.size() == 0) {
			filterBlock.where("calendar.id = 0");
		} else {
			filterBlock.where("calendar.id IN (" + ModelUtil.getIds(showCalendars, ",") + ")");
		}

		if (showGlobal) {
			filterBlock.orWhere("calendar IS NULL");
		} else {
			filterBlock.andWhere("calendar IS NOT NULL");
		}

		select.andWhere(filterBlock);
	}

	/**
	 * Loads a filter for only displaying global events.
	 * 
	 * @return
	 */
	public static CalendarFilter loadGlobal() {
		CalendarFilter cf = new CalendarFilter();
		cf.showGlobal = true;
		return cf;
	}

	/**
	 * Loads a calendar filter for displaying a single calendar.
	 * 
	 * @param calendar
	 * @return
	 */
	public static CalendarFilter loadSingleCalendar(CalendarApp calendar) {
		CalendarFilter cf = new CalendarFilter();
		cf.showGlobal = false;
		cf.showCalendars.add(calendar);
		return cf;
	}

	/**
	 * Loads the default calendar filter for the given user.
	 * 
	 * @param user
	 * @return
	 */
	public static CalendarFilter loadDefault(User user) {
		CalendarFilter calendarFilter = new CalendarFilter();

		String filter = Request.current().params.get("filter");

		// try to load filter from user properties
		// filter format: "global,1,2,3,4"
		if (filter == null && user.properties.containsKey(EVENTS_FILTER_KEY)) {
			filter = user.properties.get(EVENTS_FILTER_KEY);
		}

		// if no filter, load all by default
		List<CalendarApp> unfilteredCalendars;
		if (filter == null) {
			unfilteredCalendars = CalendarApp.findAll();
			calendarFilter.showGlobal = true;
		} else {
			// load filter data from user props
			unfilteredCalendars = new ArrayList<CalendarApp>();

			for (final String id : Util.parseList(filter, ",")) {
				if (!StringUtils.isEmpty(id)) {
					if (id.equals("global")) {
						calendarFilter.showGlobal = true;
					} else if (StringUtils.isNumeric(id)) {
						final CalendarApp cal = CalendarApp.findById(Long.parseLong(id));
						unfilteredCalendars.add(cal);
					}
				}
			}

			// store filter if changed
			if (!filter.equals(user.properties.get(EVENTS_FILTER_KEY))) {
				user.properties.put(EVENTS_FILTER_KEY, filter);
				user.save();
				JPAUtils.makeTransactionWritable();
			}
		}

		// filter calendars by interest and access
		for (final CalendarApp cal : unfilteredCalendars) {
			if (cal != null && Security.check(Permission.ACCESS_APP, cal.id) && cal.sender.isFollower(user)) {
				calendarFilter.showCalendars.add(cal);
			}
		}

		// renderArgs.put("calendars", filteredCalendars);
		return calendarFilter;
	}

	@Override
	public String toString() {
		List items = ModelUtil.getIdList(showCalendars);
		if (showGlobal) {
			items.add("global");
		}
		return Util.implode(items, ",");
	}
}

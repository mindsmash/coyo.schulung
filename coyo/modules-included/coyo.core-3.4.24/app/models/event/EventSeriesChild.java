package models.event;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.User;
import models.event.EventUser.EventResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.data.binding.NoBinding;
import storage.FlexibleBlob;
import fishr.Indexed;
import fishr.search.Searchable;

/**
 * Represents a single occurrence of an event series.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
@Entity
@Indexed
@Searchable(category = "events")
@Table(name = "event_series_child")
public class EventSeriesChild extends Event {
	@NoBinding
	@ManyToOne(optional = false)
	public EventSeries series;

	/**
	 * Constructs a new child event for the given series and start and end
	 * dates.
	 * 
	 * @param series
	 *            The event series.
	 * @param start
	 *            The start date.
	 * @param end
	 *            The end date.
	 */
	EventSeriesChild(final EventSeries series, final DateTime start, final DateTime end) {
		this.series = series;
		this.creator = series.creator;
		this.startDate = start;
		this.endDate = end;
		this.users = EventBase.copyUsers(this, series, true);

		// set Booleans (and other auto initialized values) to null so we can
		// use the master's value
		this.avatar = null;
		this.thumbnail = null;
		this.allowGuestInvite = null;
		this.suppressMaybe = null;

		// copy calendar, fulltime, sendReminder and visibility as they are used
		// in some SQL queries
		this.calendar = series.calendar;
		this.fulltime = series.fulltime;
		this.sendReminder = series.sendReminder;
		this.visibility = series.visibility;
	}

	/**
	 * Constructs a new child event for the given series, using its start and
	 * end dates.
	 * 
	 * @param series
	 */
	public EventSeriesChild(final EventSeries series) {
		this(series, series.startDate, series.endDate);
	}

	@Override
	protected void deleteAttachments() {
		if (avatar == series.avatar && thumbnail == series.thumbnail) {
			// do nothing, as avatar and thumbnail belong to the master!
			return;
		}
		super.deleteAttachments();
	}

	public String getAbout() {
		return about == null ? series.about : about;
	}

	@Override
	public List<User> getAdmins() {
		return series.getAdmins();
	}

	public Boolean getAllowGuestInvite() {
		return allowGuestInvite == null ? series.allowGuestInvite : allowGuestInvite;
	}

	public FlexibleBlob getAvatar() {
		return (avatar == null || !avatar.exists()) ? series.avatar : avatar;
	}

	public String getLocation() {
		return location == null ? series.location : location;
	}

	public String getName() {
		return name == null ? series.name : name;
	}

	public Boolean getSuppressMaybe() {
		return suppressMaybe == null ? series.suppressMaybe : suppressMaybe;
	}

	public FlexibleBlob getThumbnail() {
		return (thumbnail == null || !thumbnail.exists()) ? series.thumbnail : thumbnail;
	}

	public String getRecurrenceRule() {
		return series.recurrenceRule;
	}

	/**
	 * Resets this instance, meaning all "shadowed" properties are set back to
	 * null.
	 */
	public void reset() {
		this.about = null;
		this.admins = null;
		this.allowGuestInvite = null;
		this.avatar = null;
		this.location = null;
		this.name = null;
		this.suppressMaybe = null;
		this.thumbnail = null;

		// reset user responses
		for (final EventUser eventUser : this.users) {
			if (!eventUser.user.equals(this.creator)) {
				eventUser.response = EventResponse.WAITING;
			}
		}
	}

	public RecurrenceRuleInfo getRecurrenceRuleInfo(DateTimeZone dateTimeZone) {
		return new RecurrenceRuleInfo(series.recurrenceRule, dateTimeZone);
	}

	@Override
	public boolean isEventSeriesChild() {
		return true;
	}
}

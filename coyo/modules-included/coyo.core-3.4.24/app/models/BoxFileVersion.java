package models;

import java.io.InputStream;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.app.files.File;
import models.app.files.FileVersion;

import org.joda.time.DateTime;

import storage.FlexibleBlob;

@Entity
@Table(name = "box_file_version")
public class BoxFileVersion extends TenantModel implements FileVersion {

	public FlexibleBlob data;
	public Long size;

	@ManyToOne
	public User creator;
	@ManyToOne
	public BoxFile file;

	/**
	 * @return Version number, starting with the first version and number 1
	 */
	public int getVersionNumber() {
		return file.versions.size() - file.versions.indexOf(this);
	}

	public long getSize() {
		if (size == null) {
			size = data.length();
		}
		return size;
	}

	public void updateSize() {
		this.size = data.length();
	}

	public void setData(FlexibleBlob data) {
		this.data = data;
		updateSize();
	}

	@Override
	protected void afterDelete() throws Exception {
		super.afterDelete();

		// delete attachments
		data.delete();
	}

	public DateTime getCreated(User user) {
		return new DateTime(created.getTime(), user.getDateTimeZone());
	}

	public DateTime getModified(User user) {
		return new DateTime(modified.getTime(), user.getDateTimeZone());
	}

	@Override
	public InputStream get() {
		return data.get();
	}

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public User getCreator() {
		return creator;
	}
}

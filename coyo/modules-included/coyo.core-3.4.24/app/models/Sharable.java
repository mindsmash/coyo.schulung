package models;

import models.wall.Wall;
import models.wall.post.Post;

/**
 * 
 * Any class can be shareable by implementing this interface.
 * 
 * @author mindsmash GmbH
 * 
 */
public interface Sharable {

	public Long getId();
	
	public Post share(User user, Wall wall, String message);

	public String getDisplayName();

	public boolean isSharable();
}
package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.app.FormsApp;

@Entity
@Table(name = "app_forms_result")
public class FormResult extends TenantModel {

	@ManyToOne(optional = false)
	public FormsApp form;

	@ManyToOne
	public User user;

	@OneToMany(mappedBy = "result", cascade = CascadeType.ALL)
	public List<FormResultValue> values = new ArrayList<FormResultValue>();

	public void addValue(FormField field, String value) {
		FormResultValue v = new FormResultValue();
		v.result = this;
		v.field = field;
		v.value = value;
		values.add(v);
	}

	public String getFieldValue(FormField field) {
		for (FormResultValue value : values) {
			if (value.field == field) {
				return value.value;
			}
		}
		return null;
	}
}

package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.app.FormsApp;
import play.data.validation.Max;
import play.data.validation.MaxSize;

@Entity
@Table(name = "app_forms_field")
public class FormField extends TenantModel implements Comparable<FormField> {

	@ManyToOne
	public FormsApp form;

	public int fieldOrder;

	@MaxSize(255)

	public String type;
	@MaxSize(255)

	public String label;
	@MaxSize(255)

	public String placeholder;
	@MaxSize(255)

	public String helpText;
	@MaxSize(255)

	public String cssClass;

	public boolean required;

	@OneToMany(mappedBy = "field", cascade = CascadeType.ALL)
	public List<FormFieldOption> options = new ArrayList<FormFieldOption>();

	@OneToMany(mappedBy = "field", cascade = CascadeType.ALL)
	public List<FormResultValue> values = new ArrayList<FormResultValue>();

	public int compareTo(FormField f) {
		if (this.fieldOrder == f.fieldOrder) {
			return 0;
		}

		return this.fieldOrder - f.fieldOrder;
	};

	public void bindOptions(List<FormFieldOption> newOptions) {
		if (newOptions == null) {
			newOptions = new ArrayList<FormFieldOption>();
		}

		// remove obsolete options
		if (options != null) {
			for (FormFieldOption option : options) {
				if (!newOptions.contains(option)) {
					option.delete();
				}
			}

			options.clear();
		} else {
			options = new ArrayList<FormFieldOption>();
		}

		for (FormFieldOption option : newOptions) {
			FormFieldOption saveOption;

			if (option.id != null) {
				saveOption = FormFieldOption.findById(option.id);
			} else {
				saveOption = new FormFieldOption();
			}

			saveOption.field = this;
			saveOption.fieldOrder = option.fieldOrder;
			saveOption.label = option.label;
			saveOption.value = option.value;
			options.add(saveOption);
		}
	}

	public List<FormFieldOption> getOptions() {
		if (options != null) {
			Collections.sort(options);
		}
		return options;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[id=" + this.id + ", label=" + this.label + "]";
	}

	public boolean isValueField() {
		return !type.equals("h2") && !type.equals("hr");
	}
}

package models.embeddables;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.BaseModel;
import models.comments.Comment;
import models.comments.Commentable;
import models.Sender;
import models.TenantModel;
import models.User;
import play.data.validation.MaxSize;
import play.data.validation.Unique;

/**
 * 
 * Represents a Link.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "embeddable_link")
public class Link extends TenantModel implements Commentable {

	@Column(nullable = false)
	@Unique
	@MaxSize(255)

	public String url;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = "embeddable_link_comment")
	public List<Comment> comments = new ArrayList<Comment>();

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		save();
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return true;
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return false;
	}

	@Override
	public Sender getSender() {
		return null;
	}
}

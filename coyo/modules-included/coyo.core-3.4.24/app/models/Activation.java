package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import play.data.validation.Unique;
import play.libs.Crypto;

/**
 * Model for pending registrations.
 * 
 * @author Marko Ilic
 */
@Entity
@Table(name = "activation")
// TODO: delete after x days
public class Activation extends TenantModel {

	@Unique
	@Column(nullable = false)
	public String email;

	@Column(nullable = false)
	public String token;

	/**
	 * @param email
	 *            see {@link #email}
	 * @param token
	 *            see {@link #token}
	 */
	public Activation(String email) {
		// remove existing activations
		Activation.delete("email=?", email);

		this.email = email;
		this.token = Crypto.passwordHash(System.currentTimeMillis() + "");
	}

	@Override
	public final String toString() {
		return "Activation[email=" + email + ", token=" + token + "]";
	}
}

package models;

import java.util.Collection;
import java.util.List;

/**
 * 
 * Any class can be likable by implementing this interface.
 * 
 * @author mindsmash GmbH
 * 
 */
public interface Likable {

	public void like(User user);

	public void unlike(User user);

	public boolean likes(User user);

	public List<User> getLikes();

	/**
	 * @return The list of users that are interested in receiving updates about new likes on this likable.
	 */
	public Collection<User> getInterestedUsers();

	// TODO: activate and use for performance improvement
	// public long getLikeCount();

	public BaseModel getModel();

	public boolean checkLikePermission(User user);
}

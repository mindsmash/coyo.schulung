package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.data.validation.Required;
import utils.JPAUtils;
import fishr.Field;

@Entity
@Table(name = Box.TABLE)
public class Box extends TenantModel {

	public static final String TABLE = "box";

	@Field
	@ManyToOne(optional = false)
	public Sender sender;

	@OneToMany(mappedBy = "box", cascade = CascadeType.ALL)
	public List<BoxFile> files = new ArrayList<BoxFile>();

	@Transient
	private BoxFile root;

	@Transient
	private User creator;

	@Field
	@Required
	@Column(columnDefinition = "TEXT")
	public String description;

	public boolean collaboration = true;

	public boolean anonymous = false;

	public boolean adminsonly = false;

	public Box(Sender sender, User creator) {
		this.sender = sender;
		this.creator = creator;
	}

	public BoxFile getRoot() {
		if (root == null) {
			root = BoxFile.find("box = ? AND isRoot = true", this).first();

			if (root == null) {
				// auto-create root
				root = new BoxFile(creator, this, null, "Home", true);
				root.isRoot = true;
				root.save();
				JPAUtils.makeTransactionWritable();
			}
		}
		return root;
	}

	public List<BoxFile> getFiles() {
		return BoxFile.find("box = ? AND parent IS NOT NULL ORDER BY name ASC", this).fetch();
	}

	public List<BoxFile> getFolders() {
		return BoxFile.find("box = ? AND folder = true AND parent IS NOT NULL ORDER BY name ASC", this).fetch();
	}
}
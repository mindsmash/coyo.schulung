package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.JPA;

/**
 * 
 * Represents a Role.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "userrole")
public class UserRole extends TenantModel {

	@Column(nullable = false)
	@Required
	@Unique
	@MaxSize(255)

	public String name;

	@OneToMany(mappedBy = "role")
	public List<User> users = new ArrayList<User>();

	@OneToMany(mappedBy = "role")
	public List<UserGroup> groups = new ArrayList<UserGroup>();

	@ElementCollection
	@JoinTable(name = "userrole_grant")
	public List<String> grants = new ArrayList<String>();

	public boolean defaultRole = false;

	public boolean hasGrant(String grant) {
		return grants.contains(grant);
	}

	public List<User> getGroupUsers() {
		List<User> users = new ArrayList<User>();
		for (UserGroup group : groups) {
			users.addAll(group.users);
		}
		return users;
	}

	// faster then calling users.size()
	public long getUserCount() {
		return User.count("e.role = ?", this);
	}

	public long getGroupUserCount() {
		long count = 0L;
		for (UserGroup group : groups) {
			count += group.getUserCount();
		}
		return count;
	}

	@Override
	public void _save() {
		super._save();

		// make sure only one role is the default
		if (defaultRole) {
			List<UserRole> others = find("id != ? AND defaultRole = true", id).fetch();
			for (UserRole role : others) {
				role.defaultRole = false;
				role.save();
			}
		}
	}
	
	@Override
	public void _delete() {
		for (UserGroup group : groups) {
			group.role = null;
			group.save();
		}
		
		super._delete();
	}

	public void updateUsers(List<Long> users) {
		// clear first
		JPA.em().createQuery("UPDATE User SET role = NULL WHERE role.id = :id").setParameter("id", id).executeUpdate();

		// update
		if (users != null && users.size() > 0) {
			JPA.em().createQuery("UPDATE Sender s SET s.role = :role WHERE s.id IN :users").setParameter("role", this)
					.setParameter("users", users).executeUpdate();
		}
	}

	public void updateGroups(List<Long> groups) {
		// clear first
		JPA.em().createQuery("UPDATE UserGroup SET role = NULL WHERE role.id = :id").setParameter("id", id)
				.executeUpdate();

		// update
		if (groups != null && groups.size() > 0) {
			JPA.em().createQuery("UPDATE UserGroup SET role = :role WHERE id IN :groups").setParameter("role", this)
					.setParameter("groups", groups).executeUpdate();
		}
	}

	/**
	 * @return Default role or null.
	 */
	public static UserRole findDefaultRole() {
		return find("defaultRole = true").first();
	}
}

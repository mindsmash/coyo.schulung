package models;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;

import data.ExcludeFromExport;
import multitenancy.MTA;
import net.fortuna.ical4j.model.ConstraintViolationException;
import play.Logger;
import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Unique;
import utils.JPAUtils;

/**
 * Generic settings model which can be used for all kinds of purposes. Each settings row has a unique type so that it
 * can be uniquely identified.
 * 
 * @author Jan Marquardt
 */
@Entity
@Table(name = "settings")
public class Settings extends TenantModel {

	@Unique
	@Column(nullable = false)
	@MaxSize(255)
	public String type;

	@ElementCollection
	@CollectionTable(name = "settings_property")
	@Column(length = 20000)
	private Map<String, String> properties = new HashMap<String, String>();

	public String getString(String key) {
		return properties.get(key);
	}

	public boolean getBoolean(String key) {
		return Boolean.parseBoolean(properties.get(key));
	}

	public int getInteger(String key) {
		return Integer.parseInt(properties.get(key));
	}

	public long getLong(String key) {
		return Long.parseLong(properties.get(key));
	}

	public void setProperty(String key, String value) {
		properties.put(key, value);
	}

	public void removeProperty(String key) {
		properties.remove(key);
	}

	public boolean contains(String key) {
		return properties.containsKey(key);
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public static Settings findOrCreate(String type) {
		Settings settings = Settings.find("type = ?", type).first();
		if (settings == null) {
			settings = new Settings();
			settings.type = type;
			settings.save();
			settings.refresh();
		}
		return settings;
	}

	public static Settings findApplicationSettings() {
		// caching is done be EntityManager
		return findOrCreate("application");
	}

	@Override
	protected void beforeInsert() throws Exception {
		super.beforeInsert();

		// type unique constraint
		if (Settings.count("type = ?", type) > 0) {
			throw new ConstraintViolationException("type [" + type + "] already in use");
		}
	}

	@Override
	protected void beforeSave() throws Exception {
		super.beforeSave();

		// type unique constraint
		Long count = Settings.count("type = ?", type);
		if (count > 1) {
			Logger.warn("there are %s (more than one!) setting objects for type %s", count, type);
		}
	}

	/**
	 * Persists the settings immediately, closing the current transaction and opening a new one. Afterwards, this
	 * settings object is reloaded and returned.
	 * 
	 * @return This reloaded settings object
	 */
	public Settings persistAndReload() {
		JPAUtils.commitAndRefreshTransaction();
		MTA.activateMultitenancy(tenant);
		return Settings.findOrCreate(type);
	}
}

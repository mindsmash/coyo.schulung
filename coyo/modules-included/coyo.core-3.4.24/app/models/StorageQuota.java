package models;

import java.io.Serializable;

import javax.persistence.Embeddable;

import play.Play;
import storage.FlexibleBlob;
import utils.HardDriveUtil;

@Embeddable
public class StorageQuota implements Serializable {

	private static final Long MAX_SPACE = Long.valueOf(Play.configuration.getProperty("coyo.quota.maxSpace", "0"));

	/**
	 * can be set or left as null. if null, the total disk space will be used.
	 */
	public Long maxSpace;

	/**
	 * regularly calculated by {@link jobs.StorageQuotaCalculator}
	 */
	public Long currentUsage;

	public Long getTotalSpace() {
		if (maxSpace != null && maxSpace.compareTo(0L) > 0) {
			return maxSpace;
		}

		// config
		if (MAX_SPACE != null && MAX_SPACE.compareTo(0L) > 0) {
			return MAX_SPACE;
		}

		return HardDriveUtil.getTotalSpace();
	}

	public Long getCurrentUsage() {
		if (currentUsage == null) {
			currentUsage = FlexibleBlob.storage.storageLength();
		}
		return currentUsage;
	}

	public Float getUsagePercent() {
		Float percent = currentUsage > 0 ? ((float) currentUsage / (float) getTotalSpace() * 100F) : 0F;
		return percent > 100 ? 100f : percent;
	}

	public boolean isLow() {
		return getUsagePercent() > 95;
	}
}

package models.app;

import acl.AppsPermission;
import acl.Permission;
import controllers.Security;
import controllers.Uploads;
import controllers.Uploads.TempUpload;
import controllers.WebBaseController;
import extensions.CommonExtensions;
import fishr.Field;
import fishr.Fishr;
import fishr.Indexed;
import fishr.Related;
import fishr.search.Searchable;
import models.BaseModel;
import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Likable;
import models.Sender;
import models.Sharable;
import models.User;
import models.app.dao.BlogAppDao;
import models.comments.Comment;
import models.comments.Commentable;
import models.helper.ClearManyToMany;
import models.medialibrary.MediaFile;
import models.notification.BlogArticleNotification;
import models.notification.BlogCommentNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareBlogArticlePost;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.JPA;
import security.CheckCaller;
import storage.FlexibleBlob;
import utils.ErrorHelper;
import utils.Hashifier;
import utils.ImageUtils;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Indexed(boost = 2F)
@Searchable(category = "blogArticles")
@Table(name = "app_blog_article")
public class BlogAppArticle extends ExtendedTenantModel implements Commentable, Likable, Sharable,
		ExtendedSearchableModel {

	public static final String BLOG_COMMENT_TABLE = "app_blog_article_comment";
	public static final String BLOG_LIKE_TABLE = "app_blog_article_like";

	public static final String TEASER_SEP = "<div style=\"page-break-after: always;\">";

	public static final int TEASER_IMAGE_WIDTH = 150;
	private static final int TEASER_MAX_LENGTH = 500;

	private static final Logger LOG = LoggerFactory.getLogger(BlogAppArticle.class);

	@Field
	@ManyToOne(optional = false)
	public BlogApp app;

	@ManyToOne(optional = false)
	public User author;

	@MaxSize(255)
	@Field
	@Required
	public String title;

	@Field
	@Lob
	public String text;

	@Field
	@Lob
	public String teaser;

	public Long teaserImage;

	@Transient
	public String newTeaserImageUpload;

	public boolean showTeaser;

	@Required
	@As(lang = { "de", "*" }, value = { "dd-MM-yyyy", "MM-dd-yyyy" })
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime publishDate = new DateTime();

	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime autoshareDate;

	@OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
	public List<ShareBlogArticlePost> posts = new ArrayList<ShareBlogArticlePost>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "blogArticle")
	public List<BlogArticleNotification> notifications = new ArrayList<BlogArticleNotification>();

	public boolean isPublished() {
		if (publishDate == null) {
			return false;
		}
		return publishDate.isBeforeNow();
	}

	/**
	 * Returns the teaser text of the blog article. This is either the text specified as teaser text or (if not set) a
	 * part of the main text. If the main text is used the text is limited to given chars and all html is stripped
	 * (except for br).
	 */
	public String getTeaserText() {
		String teaserText = "";
		if (StringUtils.isNotEmpty(teaser)) {
			teaserText = Jsoup.clean(teaser, StringUtils.EMPTY, Whitelist.none(),
					new OutputSettings().prettyPrint(false));
			/*
				CommonExtensions.nl2br replaces newlines with breaks + escapes the HTML (which we do not want to do)
				=> Do it manually via replaceAll
			 */
			teaserText = teaserText.replaceAll("\n", "<br>");
		} else if (StringUtils.isNotEmpty(text)) {
			teaserText = CommonExtensions.shortenHtml(text, TEASER_MAX_LENGTH);
		}

		if (!teaserText.isEmpty()) {
			if (Fishr.HASHTAGS) {
				teaserText = Hashifier.hashify(teaserText);
			}
		}
		return teaserText;
	}

	public boolean commentable = true;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = BLOG_COMMENT_TABLE)
	public List<Comment> comments = new ArrayList<Comment>();

	@ManyToMany
	@JoinTable(name = BLOG_LIKE_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> likes = new ArrayList<User>();

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();

		BlogCommentNotification.raise(this, comment);
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		save();

		BlogCommentNotification.cleanup(this);
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	public void like(User user) {
		if (!likes(user)) {
			likes.add(user);
			save();
		}
	}

	public void unlike(User user) {
		if (likes(user)) {
			likes.remove(user);
			save();
		}
	}

	public boolean likes(User user) {
		return likes.contains(user);
	}

	@Override
	public List<User> getLikes() {
		return likes;
	}

	@Override
	public Collection<User> getInterestedUsers() {
		return app.sender.followers;
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareBlogArticlePost post = new ShareBlogArticlePost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.article = this;
		post.save();
		return post;
	}

	@Override
	public boolean isSharable() {
		return isPublished() && app.sender.isSharable();
	}

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.ACCESS_APP, app);
	}

	@Override
	protected void beforeSave() throws Exception {
		/* remove duplicates */
		Set<User> cleared = new HashSet<User>(likes);
		likes.clear();
		likes.addAll(cleared);

		super.beforeSave();
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + BLOG_LIKE_TABLE + " WHERE app_blog_article_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}

	@Override
	public void _save() {
		// handle new teaser image
		if (!StringUtils.isEmpty(newTeaserImageUpload)) {
			if ("delete".equals(newTeaserImageUpload)) {
				// we never really remove anything from the media library
				// so we just delete the pointer
				teaserImage = null;
			} else {
				try {
					MediaFile file = new MediaFile();
					file.library = app.sender.mediaLibrary;
					file.data = new FlexibleBlob();

					TempUpload upload = Uploads.loadUpload(newTeaserImageUpload);
					ImageUtils
							.scaleToWidth(upload.blob.get(), upload.blob.type(), file.data, TEASER_IMAGE_WIDTH, false);

					file.name = upload.filename;
					file.save();
					teaserImage = file.id;
				} catch (Exception e) {
					ErrorHelper.handleWarning(LOG, e, "[BlogAppArticle] Error saving teaser image");
					teaserImage = null;
				}
			}
		}

		super._save();
	}

	@Override
	public void _delete() {
		// hibernate fix
		for (BlogArticleNotification notification : notifications) {
			notification.delete();
		}
		notifications.clear();

		super._delete();
	}

	/**
	 * Create shared blog post on apps default wall.
	 */
	public void doAutoShare() {
		final ShareBlogArticlePost post = new ShareBlogArticlePost();
		post.author = app.sender;
		post.message = "";
		post.article = this;
		post.important = app.autoshareImportant;
		post.wall = app.sender.getDefaultWall();
		post.save();

		// mark shared
		autoshareDate = new DateTime();
		save();

		LOG.debug("[BlogAutoshare] Created new post on wall [{}] for blog article [{}].", app.sender.getDefaultWall(),
				this.id);
	}

	/**
	 * Check if the article can be shared automatically on the apps sender default wall.
	 *
	 * @return boolean
	 */
	public boolean canBeAutoShared() {
		return app.active && app.autoshare && autoshareDate == null && isPublished() &&
				app.sender.getDefaultWall() != null && !BlogAppDao.hasBeenAutoShared(this);
	}

	/**
	 * Checks if the article has been shared.
	 *
	 * @return boolean
	 */
	public boolean isShared() {
		return BlogAppDao.hasArticleBeenShared(this);
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	public DateTime getPublishDate(User user) {
		if (publishDate == null) {
			return null;
		}
		return publishDate.withZone(user.getDateTimeZone());
	}

	@Override
	public void redirectToResult() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("articleId", id);
		WebBaseController.redirectToSender(app.sender, app.id, args);
	}

	@Override
	public boolean checkLikePermission(User user) {
		return CheckCaller.check(AppsPermission.LIKE_BLOG_ARTICLE, this);
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return CheckCaller.check(AppsPermission.COMMENT_BLOG_ARTICLE, this);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return CheckCaller.check(AppsPermission.EDIT_BLOG_ARTICLE, this);
	}

	@Override
	public Sender getSearchThumbSender() {
		return app.sender;
	}

	@Override
	public String getSearchInfoText() {
		return app.getSearchInfoText();
	}

	@Related
	public String getRelatedSearchInfo() {
		return app.getDisplayName() + " " + app.getRelatedSearchInfo();
	}

	@Field
	public List<String> getRelatedSearchComments() {
		return Comment.getSearchInfo(comments);
	}

	@Override
	public Sender getSender() {
		return this.app.sender;
	}

	public String getURL() {
		return app.getURL() + "#!/blog/" + app.id + "/article/" + id;
	}
}

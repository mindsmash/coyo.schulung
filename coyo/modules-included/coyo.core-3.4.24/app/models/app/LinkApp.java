package models.app;

import javax.persistence.Entity;
import javax.persistence.Table;

import play.data.validation.Required;
import play.mvc.Http.Request;
import play.mvc.results.Redirect;
import security.CheckCaller;
import validation.SimpleURL;
import acl.Permission;
import apps.LinkAppDescriptor;

@Entity
@Table(name = "app_link")
public class LinkApp extends App {

	@Required
	@SimpleURL
	public String url;

	@Override
	public void beforeRender(Request request) {
		if (!CheckCaller.check(Permission.EDIT_SENDER, sender.id)) {
			throw new Redirect(url);
		}
	}

	@Override
	public String getAppKey() {
		return LinkAppDescriptor.KEY;
	}
}

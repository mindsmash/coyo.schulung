package models.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.Box;
import models.BoxFile;
import models.User;
import models.app.files.File;
import play.i18n.Messages;
import play.mvc.Http.Request;
import session.UserLoader;
import apps.LocalFilesAppDescriptor;

@Entity
@Table(name = "app_files_local")
public class LocalFilesApp extends FilesApp {

	@Override
	public void bind(Request request) {
		if (box == null) {
			box = new Box(sender, UserLoader.getConnectedUser());
		}
	}

	@Override
	public String getAppKey() {
		return LocalFilesAppDescriptor.KEY;
	}

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public Box box;

	@Override
	public boolean isReadOnly() {
		return false;
	}

	@Override
	public File getFile(String uid) {
		try {
			return BoxFile.find("id = ? AND box = ?", Long.parseLong(uid), box).first();
		} catch (NumberFormatException ignored) {
		}
		return null;
	}

	@Override
	public File getRoot() {
		return box.getRoot();
	}

	@Override
	public File createFile(User user, File parent, String name, InputStream in, String contentType, Long contentLength) {
		BoxFile file = null;
		BoxFile boxFileParent = (BoxFile) parent;

		// check already exists
		for (BoxFile sibling : boxFileParent.children) {
			if (sibling.name.equals(name)) {
				file = sibling;
				break;
			}
		}

		if (file != null) {
			User lock = file.getLock();
			if (lock != null && lock != user) {
				throw new FilesAppException(Messages.get("files.error.locked"), "file is locked");
			}
		}

		// if not exists
		if (file == null) {
			// prepare file
			file = new BoxFile();
			file.name = name;
			file.box = boxFileParent.box;
			file.parent = boxFileParent;
			file.creator = user;
			file.folder = false;
		}

		if (!file.validateAndSave()) {
			throw new FilesAppException(Messages.get("files.error.create"), "validation failed");
		}

		try {
			file.addVersion(in, user, contentType, contentLength);
		} catch (IOException e) {
			throw new FilesAppException(Messages.get("files.error"), e.getMessage(), e);
		}

		return file;
	}

	@Override
	public File createFolder(User user, File parent, String name) {
		BoxFile boxFileParent = (BoxFile) parent;
		BoxFile folder = new BoxFile();
		folder.name = name;
		folder.box = boxFileParent.box;
		folder.parent = boxFileParent;
		folder.creator = user;
		folder.folder = true;

		if (!folder.validateAndSave()) {
			throw new FilesAppException(Messages.get("files.error.create"), "validation failed");
		}

		return folder;
	}

	@Override
	public List<File> getLatestFiles(int limit) {
		return BoxFile.find("box = ? AND folder = false ORDER BY modified DESC", box).fetch(limit);
	}
}

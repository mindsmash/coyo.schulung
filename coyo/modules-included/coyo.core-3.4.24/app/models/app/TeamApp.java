package models.app;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import models.User;
import models.helper.ClearManyToMany;

import org.hibernate.annotations.OrderBy;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Http.Request;
import apps.TeamAppDescriptor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import csrf.CheckAuthenticity;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_team")
public class TeamApp extends App {

	public static final String TEAM_MEMBER_TABLE = "app_team_user";

	@ManyToMany
	@JoinTable(name = TEAM_MEMBER_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public Map<Integer, User> members = new TreeMap<Integer, User>();
	
	public int currentPriority = 0;

	@Override
	public void bind(Request request) {
		// bind user list
		members = new TreeMap<Integer, User>();
		List<String> ids = request.params.get("members", List.class);
		if (ids != null) {
			currentPriority = 0;
			for (String id : ids) {
				User user = User.findById(Long.valueOf(id));
				if (user != null) {
					members.put(currentPriority++, user);
				}
			}
		}

		super.bind(request);
	}

	public List<User> getActiveMembers() {
		List<User> r = new LinkedList<User>();
		
		Iterator it = members.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			User user = (User)pair.getValue();
			if (!user.deleted && user.active) {
				r.add(user);
			}
		}
		return r;
	}

	@Override
	public String getAppKey() {
		return TeamAppDescriptor.KEY;
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + TEAM_MEMBER_TABLE + " WHERE app_team_id = " + id).executeUpdate();

		super.beforeDelete();
	}
	
	public void clearList() {
		members.clear();
	}
	
	public void addMember(int priority, User user) {
		members.put(priority, user);
	}
}

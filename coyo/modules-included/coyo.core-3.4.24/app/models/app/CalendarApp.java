package models.app;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.Sender;
import models.User;
import models.UserGroup;
import models.event.CalendarFilter;
import models.event.Event;
import models.event.EventBase;
import models.event.EventSeries;
import models.page.Page;
import models.workspace.Workspace;
import net.fortuna.ical4j.model.Recur;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.db.helper.JpqlSelect;
import play.mvc.Http.Request;
import utils.DateUtils;
import apps.CalendarAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "apps")
@Table(name = "app_calendar")
public class CalendarApp extends App {
	public enum CalendarType {
		RESOURCE_BOOKING
	}

	@OneToMany(mappedBy = "calendar", cascade = CascadeType.ALL)
	public List<EventBase> events = new ArrayList<EventBase>();

	public String color = "#AAAAAA";

	@MaxSize(255)
	public String displayName;

	public boolean onlyAdminsEdit;

	@Enumerated(EnumType.STRING)
	public CalendarType calendarType;

	@Override
	public void bind(Request request) {
		onlyAdminsEdit = request.params._contains("onlyAdminsEdit")
				&& Boolean.valueOf(request.params.get("onlyAdminsEdit")).booleanValue();
		calendarType = (request.params._contains("eventsMayNotOverlap") && request.params.get("eventsMayNotOverlap",
				Boolean.class)) ? CalendarType.RESOURCE_BOOKING : null;
		super.bind(request);
	}

	public boolean eventsMayNotOverlap() {
		return CalendarType.RESOURCE_BOOKING.equals(calendarType);
	}

	@Override
	public String getAppKey() {
		return CalendarAppDescriptor.KEY;
	}

	public String getDisplayName() {
		if (StringUtils.isEmpty(displayName)) {
			return title;
		}
		return displayName;
	}

	public List<User> getInvitableUsers() {
		if (sender instanceof Page && ((Page) sender).visibility.hasMembers()) {
			return ((Page) sender).getAllMembers();
		} else if (sender instanceof Workspace) {
			return ((Workspace) sender).getUserMembers();
		}
		return null;
	}

	public List<UserGroup> getInvitableGroups() {
		if (sender instanceof Page && ((Page) sender).visibility.hasMembers()) {
			return ((Page) sender).memberGroups;
		} else if (sender instanceof Workspace) {
			return new ArrayList<UserGroup>();
		}
		return UserGroup.findAll();
	}

	public List<Event> getUpcomingEvents(DateTimeZone timeZone, int limit) {
		return Event.getUpcomingEvents(timeZone, getFilter(), limit);
	}

	public long getUpcomingEventsCount(DateTimeZone timeZone) {
		return Event.getUpcomingEventsCount(timeZone, getFilter());
	}

	/**
	 * Gathers all the calendar apps that the given user is interested in,
	 * grouped by senders.
	 * 
	 * @param user
	 * @return Calendars, grouped by sender
	 */
	public static Map<Sender, List<CalendarApp>> getGroupedCalendars(User user) {
		List<CalendarApp> calendars;
		if (user != null) {
			calendars = find("SELECT a FROM CalendarApp a WHERE a.active = true AND ? MEMBER OF a.sender.followers",
					user).fetch();
		} else {
			calendars = find("active = true").fetch();
		}

		Map<Sender, List<CalendarApp>> mappedCalendars = new TreeMap<Sender, List<CalendarApp>>();

		for (CalendarApp cal : calendars) {
			if (cal.sender.isActive()) {
				if (!mappedCalendars.containsKey(cal.sender)) {
					mappedCalendars.put(cal.sender, new ArrayList<CalendarApp>());
				}
				mappedCalendars.get(cal.sender).add(cal);
			}
		}
		return mappedCalendars;
	}

	/**
	 * Finds and returns the first {@link Event} in this calendar which overlaps
	 * the given interval between start and end date (itself excluded).
	 */
	public Event getFirstOverlappingEvent(final DateTime startDate, DateTime endDate, final Long existingEventId) {
		if (startDate == null || endDate == null) {
			return null;
		}
		final JpqlSelect select = new JpqlSelect();
		if (endDate.withTimeAtStartOfDay().equals(endDate)) {
			// move to end of day to make query easier
			endDate = endDate.plusDays(1).minus(1);
		}
		// two events overlap if: (event1.startDate < event2.endDate) and
		// (event1.endDate > event2.startDate)
		select.from("Event e")
				.where("calendar.id = ? and ((fulltime = true and startDate <= ? and endDate >= ?) "
						+ "or (fulltime = false and startDate < ? and endDate > ?))")
				.params(this.id, endDate, startDate.withTimeAtStartOfDay(), endDate, startDate);
		if (existingEventId != null) {
			select.andWhere("id != ?").param(existingEventId);
		}
		return Event.find(select.toString(), select.getParams().toArray()).first();
	}

	/**
	 * Finds and returns the first {@link Event} in this calendar which overlaps
	 * the given recurrence rule's recurrence set.
	 */
	public Event getFirstOverlappingEvent(final DateTime startDate, final DateTime endDate, DateTimeZone timeZone, final Long existingEventId,
			final String recurrenceRule, final Long existingSeriesId) {
		Recur recur = null;
		try {
			recur = new Recur(recurrenceRule);
		} catch (ParseException e) {
			return null;
		}

		// calculate the date which should be the upper bound for the interval
		// in which we look for possible overlaps
		DateTime seriesEndDate = recur.getUntil() == null ? startDate.plusYears(EventSeries
				.getNumberOfYearsForChildGeneration(recurrenceRule)) : new DateTime(recur.getUntil())
				.withZone(DateTimeZone.UTC).plusDays(1).minus(1);

		// 1. find all events in this calendar in the period
		List<Object[]> existingEvents = Event
				.em()
				.createQuery(
						"SELECT id, startDate, endDate, series.id FROM Event WHERE "
								+ "calendar.id = :calendarId AND startDate >= :startDate AND endDate <= :endDate")
				.setParameter("calendarId", this.id).setParameter("startDate", startDate.withTimeAtStartOfDay())
				.setParameter("endDate", seriesEndDate).getResultList();

		if (existingEvents.isEmpty()) {
			return null;
		}

		// 2. calculate the recurrence set for the given recurrence rule
		List<Interval> recurrenceSet = DateUtils.getRecurrenceSet(recurrenceRule, startDate, endDate.equals(endDate
				.withTimeAtStartOfDay()) ? endDate.plusDays(1).minus(1) : endDate, timeZone, new Interval(startDate,
				seriesEndDate));
		// we need to include the first instance!
		recurrenceSet.add(new Interval(startDate, endDate.equals(endDate.withTimeAtStartOfDay()) ? endDate.plusDays(1)
				.minus(1) : endDate));

		// 3. check for every interval if there is an existing event
		for (Interval interval : recurrenceSet) {
			// brute force: search through complete list of existing events
			// TODO optimize by using two Maps to lookup the data?!
			for (Object[] result : existingEvents) {
				Long id = (Long) result[0];
				DateTime existingStartDate = ((DateTime) result[1]).withZone(DateTimeZone.UTC);
				DateTime existingEndDate = ((DateTime) result[2]).withZone(DateTimeZone.UTC);
				existingEndDate = existingEndDate.withTimeAtStartOfDay().equals(existingEndDate) ? existingEndDate
						.plusDays(1).minus(1) : existingEndDate;
				Long seriesId = (Long) result[3];

				// exclude the existing event itself and also events in the same
				// series (relevant when editing an
				// existing series)
				if ((seriesId == null || !seriesId.equals(existingSeriesId)) && !id.equals(existingEventId)
						&& interval.overlaps(new Interval(existingStartDate, existingEndDate))) {
					return Event.findById(id);
				}
			}
		}

		// nothing found
		return null;
	}

	public CalendarFilter getFilter() {
		return CalendarFilter.loadSingleCalendar(this);
	}

	@Override
	public void _delete() {
		// manually fix cascading issues
		List<EventSeries> series = EventSeries.find("calendar = ?", this).fetch();
		for (EventSeries eventSeries : series) {
			eventSeries._delete();
		}
		refresh();
		for (EventBase event : events) {
			event._delete();
		}
		events.clear();

		super._delete();
	}
}

package models.app;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.Sender;
import models.wall.Wall;
import play.mvc.Http.Request;
import apps.WallAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_wall")
public class WallApp extends App {

	@OneToOne(cascade = CascadeType.ALL)
	public Wall wall = new Wall(sender);
	
	public boolean showInfo = true;

	public void setSender(Sender sender) {
		this.sender = sender;
		this.wall.sender = sender;
	}

	@Override
	public void bind(Request request) {
		wall.restrictPosts = request.params._contains("restrictPosts")
				&& Boolean.valueOf(request.params.get("restrictPosts")).booleanValue();

		wall.restrictCollaboration = request.params._contains("restrictCollaboration")
				&& Boolean.valueOf(request.params.get("restrictCollaboration")).booleanValue();

		wall.noActivityStream = !active;
		
		showInfo = Boolean.parseBoolean(request.params.get("showInfo"));

		super.bind(request);
	}

	@Override
	public String getAppKey() {
		return WallAppDescriptor.KEY;
	}
}
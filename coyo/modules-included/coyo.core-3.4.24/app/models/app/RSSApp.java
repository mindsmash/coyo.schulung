package models.app;

import apps.RSSAppDescriptor;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import fishr.Indexed;
import fishr.search.Searchable;
import jobs.RSSUpdater;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import play.Logger;
import play.cache.Cache;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.libs.Crypto;
import validation.SimpleURL;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Class used to display RSS Feeds as page.
 *
 * @author Drews Clausen, Jan Marquardt
 */
@Entity
@Indexed
@Searchable(category = "apps")
@Table(name = "app_rss")
public class RSSApp extends App {

	@SimpleURL
	@Required
	@MaxSize(255)
	public String rssUrl;

	@MaxSize(255)
	public String username;

	@MaxSize(255)
	public String password;

	public boolean postOnWall;

	public Date lastUpdated;

	public void setPassword(String password) {
		try {
			this.password = Crypto.encryptAES(password);
		} catch (Exception ignored) {
		}
	}

	public String getPassword() {
		try {
			if (password != null) {
				return Crypto.decryptAES(password);
			}
		} catch (Exception ignored) {
		}
		return null;
	}

	@Override
	public String getAppKey() {
		return RSSAppDescriptor.KEY;
	}

	public String getEntryLink(SyndEntry entry) {
		UrlValidator urlValidator = new UrlValidator();
		return (urlValidator.isValid(entry.getLink())) ? entry.getLink() : "";
	}

	public List getEntries() {
		long start = System.currentTimeMillis();
		List entries = (List) Cache.get("rss-" + id);

		if (entries == null) {
			try {
				HttpURLConnection conn = (HttpURLConnection) new URL(rssUrl).openConnection();
				final int status = conn.getResponseCode();

				// redirect?
				if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
						|| status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
					conn = (HttpURLConnection) new URL(conn.getHeaderField("Location")).openConnection();
				}

				if (!StringUtils.isEmpty(username) && password != null) {
					String encoding = new sun.misc.BASE64Encoder().encode((username + ":" + password).getBytes());
					conn.setRequestProperty("Authorization", "Basic " + encoding);
				}

				final SyndFeed feed = new SyndFeedInput().build(new XmlReader(conn));

				if (feed != null) {
					entries = feed.getEntries();
					Cache.add("rss-" + id, entries, "15mn");
				}

				clearCurrentError();
			} catch (IOException e) {
				// IO error can be displayed to app admin (most likely HTTP code
				// is not 200)
				setCurrentError(e.getMessage());
			} catch (Exception e) {
				Logger.debug(e, "could not fetch rss feed [%s]", rssUrl);
			}
		}

		Logger.debug("fetch rss [%s] took %s ms", rssUrl, System.currentTimeMillis() - start);

		return entries;
	}

	public static String getMessage(SyndEntry entry, Integer max) {
		String message = "";
		if (entry != null && entry.getDescription() != null) {
			message = entry.getDescription().getValue();
			if (!StringUtils.isEmpty(message)) {
				message = Jsoup.clean(message, Whitelist.basic());
				if (max != null) {
					message = StringUtils.abbreviate(message, max);
				}
			}
		}
		return message;
	}

	@Override
	protected void afterSave() throws Exception {
		Cache.delete("rss-" + id);
		super.afterSave();
	}

	public String getCurrentError() {
		return Cache.get("rss-error-" + id, String.class);
	}

	public void setCurrentError(String error) {
		Cache.add("rss-error-" + id, error);
	}

	public void clearCurrentError() {
		Cache.delete("rss-error-" + id);
	}

}

package models.app.inno;

import models.TenantModel;
import models.app.InnoApp;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "app_inno_category")
public class Category extends TenantModel {
	@Column(updatable = false)
	public String name;

	@ManyToOne
	public InnoApp app;

	@OneToMany
	public List<Idea> ideas;

	public static void removeIdea(final Idea idea, final InnoApp app) {
		final String sql = "SELECT ca FROM Category ca WHERE ca.app = :app AND :idea MEMBER OF ca.ideas";
		final TypedQuery<Category> q = em().createQuery(sql, Category.class);
		q.setParameter("idea", idea);
		q.setParameter("app", app);

		try {
			final Category category = q.getSingleResult();
			category.ideas.remove(idea);
			category.save();
		} catch (NoResultException ex) {
		}
	}

	public static void addIdea(final Idea idea, final Long categoryId) {
		final InnoApp app = idea.getApp();
		Category.removeIdea(idea, app);
		final Category category = Category.findById(categoryId);
		category.ideas.add(idea);
		category.save();
	}
}

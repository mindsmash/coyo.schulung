package models.app.inno;

import containers.form.ItemFormData;

import java.util.List;

public class IdeaDTO {
	public Long id;
	public ItemFormData formData;
	public String title;
	public String description;
	public Long categoryId;
	public String hiddenField;
	public Boolean isAnon;

	public List<IdeaRating> ratings;
	public IdeaDTO() {
	}

}

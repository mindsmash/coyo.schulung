package models.app.inno;

import acl.Permission;
import com.google.common.collect.Iterables;
import controllers.Security;
import models.BaseModel;
import models.User;
import models.container.Container;
import models.container.Field;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import session.UserLoader;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class IdeaDAO {

	/**
	 * Find ideas using filters and pagination
	 *
	 * @param filterParams
	 * @return
	 */
	public static List<Idea> findIdeasFilteredAndPaged(final FilterParams filterParams) {
		final StringBuffer sql = new StringBuffer();
		sql.append("SELECT idea FROM Idea idea LEFT JOIN idea.ratings ratings_ WHERE idea.container.id = :containerId");

		// text
		if (!StringUtils.isEmpty(filterParams.getText())) {
			sql.append(" AND (idea.title LIKE :text OR idea.description LIKE :text)");
		}
		// sensitive information
		if (filterParams.getSensitiveInformation() != null) {
			if (filterParams.getSensitiveInformation()) {
				sql.append(" AND idea.hiddenField IS NOT NULL");
			} else {
				sql.append(" AND idea.hiddenField IS NULL");
			}
		}
		// status
		if (filterParams.getStatus() != null) {
			sql.append(" AND idea.status = :status");
		}
		// author
		if (filterParams.getAuthor() != null) {
			sql.append(" AND idea.creator = :author");
		}
		// from
		if (!StringUtils.isEmpty(filterParams.getFrom())) {
			sql.append(" AND idea.created >= :from");
		}
		// till
		if (!StringUtils.isEmpty(filterParams.getTill())) {
			sql.append(" AND idea.created <= :till");
		}
		// category
		if (filterParams.getCategory() != null) {
			sql.append(" AND EXISTS (SELECT c_ FROM Category c_ WHERE c_.id = :categoryId AND idea MEMBER OF c_.ideas)");
		}

		sql.append(" GROUP BY idea ");

		if (filterParams.getOrderParam() != null && filterParams.getOrderParam() == OrderParam.RATING) {
			sql.append(" ORDER BY SUM(ratings_.rating) DESC");
		} else if (filterParams.getOrderParam() != null && filterParams.getOrderParam() == OrderParam.NEWEST) {
			sql.append(" ORDER BY idea.created DESC");
		} else if (filterParams.getOrderParam() != null && filterParams.getOrderParam() == OrderParam.OLDEST) {
			sql.append(" ORDER BY idea.created ASC");
		} else {
			sql.append(" ORDER BY idea.created DESC");
		}

		final TypedQuery<Idea> q = BaseModel.em().createQuery(sql.toString(), Idea.class);

		q.setParameter("containerId", filterParams.getContainerId());

		if (!StringUtils.isEmpty(filterParams.getText())) {
			q.setParameter("text", "%" + filterParams.getText() + "%");
		}
		if (filterParams.getStatus() != null) {
			q.setParameter("status", filterParams.getStatus());
		}
		if (filterParams.getAuthor() != null) {
			q.setParameter("author", filterParams.getAuthor());
		}
		if (filterParams.getFrom() != null) {
			q.setParameter("from", new DateTime(filterParams.getFrom()).withTime(0, 0, 0, 0).toDate());
		}
		if (filterParams.getTill() != null) {
			q.setParameter("till", new DateTime(filterParams.getTill()).withTime(23, 59, 59, 999).toDate());
		}
		if (filterParams.getCategory() != null) {
			q.setParameter("categoryId", filterParams.getCategory());
		}

		// pagination
		q.setFirstResult(filterParams.getFirstResult());
		if(filterParams.getMaxResults() > 0) {
			q.setMaxResults(filterParams.getMaxResults());
		}

		return q.getResultList();
	}


	/**
	 * Find ideas using filters and pagination
	 *
	 * @param filterParams
	 * @return
	 */
	public static Long countIdeasFilteredAndPaged(final FilterParams filterParams) {
		final StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(idea) FROM Idea idea WHERE idea.container.id = :containerId");

		// text
		if (!StringUtils.isEmpty(filterParams.getText())) {
			sql.append(" AND (idea.title LIKE :text OR idea.description LIKE :text)");

		}
		// sensitive information
		if (filterParams.getSensitiveInformation() != null) {
			if (filterParams.getSensitiveInformation()) {
				sql.append(" AND idea.hiddenField IS NOT NULL");
			} else {
				sql.append(" AND idea.hiddenField IS NULL");
			}
		}
		// status
		if (filterParams.getStatus() != null) {
			sql.append(" AND idea.status = :status");

		}
		// author
		if (filterParams.getAuthor() != null) {
			sql.append(" AND idea.creator = :author");

		}
		// from
		if (!StringUtils.isEmpty(filterParams.getFrom())) {
			sql.append(" AND idea.created >= :from");
		}
		// till
		if (!StringUtils.isEmpty(filterParams.getTill())) {
			sql.append(" AND idea.created <= :till");

		}
		// category
		if (filterParams.getCategory() != null) {
			sql.append(
					" AND EXISTS (SELECT c_ FROM Category c_ WHERE c_.id = :categoryId AND idea MEMBER OF c_.ideas)");
		}

		final TypedQuery<Long> qC = BaseModel.em().createQuery(sql.toString(), Long.class);

		qC.setParameter("containerId", filterParams.getContainerId());

		if (!StringUtils.isEmpty(filterParams.getText())) {
			qC.setParameter("text", "%" + filterParams.getText() + "%");
		}
		if (filterParams.getStatus() != null) {
			qC.setParameter("status", filterParams.getStatus());
		}
		if (filterParams.getAuthor() != null) {
			qC.setParameter("author", filterParams.getAuthor());
		}
		if (filterParams.getFrom() != null) {
			qC.setParameter("from", new DateTime(filterParams.getFrom()).toDate());
		}
		if (filterParams.getTill() != null) {
			qC.setParameter("till", new DateTime(filterParams.getTill()).toDate());

		}
		if (filterParams.getCategory() != null) {
			qC.setParameter("categoryId", filterParams.getCategory());
		}

		return qC.getSingleResult();
	}




	/**
	 * Find all ideas which where the current user is creator, moderator or admin, follower or member of the sender
	 *
	 * @param user
	 * @param term
	 * @return
	 */
	public static List<Idea> findLinkableIdeas(final Idea idea, final User user, final String term) {
		if (term == null) {
			return null;
		}
		final String sql = "SELECT idea FROM Idea idea WHERE idea <> :idea AND LOWER(idea.title) LIKE :term AND "
				+ "EXISTS (SELECT app FROM InnoApp app WHERE idea MEMBER OF app.container.items AND app = :app)";

		final TypedQuery<Idea> q = Idea.em().createQuery(sql, Idea.class);
		q.setParameter("app", idea.getApp());
		q.setParameter("term", "%" + term.toLowerCase() + "%");
		q.setParameter("idea", idea);
		q.setMaxResults(4);

		return q.getResultList();
	}

	/**
	 * Find ideas of other users where current user if member or follower of sender
	 *
	 * @return
	 */
	public static List<Idea> getIdeasOfOtherUsers(final User user, final Integer max) {
		final String sql = "SELECT idea FROM Idea idea WHERE idea.creator <> :user AND "
				+ "EXISTS (SELECT app FROM InnoApp app WHERE app.active = true AND app.container = idea.container) AND "
				+ "( EXISTS (SELECT app_ FROM InnoApp app_ WHERE :user MEMBER OF app_.moderators) OR "
				+ ":user MEMBER OF idea.container.sender.followers OR "
				+ "EXISTS (SELECT wm FROM WorkspaceMember wm WHERE (wm.user = :user AND wm.workspace = idea.container.sender)) ) "
				+ "ORDER BY idea.created DESC";

		final TypedQuery<Idea> q = Idea.em().createQuery(sql, Idea.class);
		q.setParameter("user", user);

		if (max != null && max > 0) {
			q.setMaxResults(max);
		}

		return q.getResultList();
	}

	/**
	 * Calculates and returns the average rating for one idea
	 *
	 * @param idea
	 * @return average rating value or 0
	 */
	public static double getAverageRating(final Idea idea) {
		final String sql = "SELECT AVG(rating_.rating) FROM IdeaRating as rating_ WHERE rating_.idea = :idea";
		final TypedQuery<Double> q = Idea.em().createQuery(sql, Double.class);
		q.setParameter("idea", idea);

		try {
			return q.getSingleResult();
		} catch (NoResultException | NullPointerException ex) {
			return 0d;
		}
	}

	public static List<Idea> getAllIdeasFromContainer(final User user, final Field orderBy, final Container container,
			final Boolean allCanBeReadBy, final Boolean asc) {
		final List<Idea> ideas = Idea.find("container = ?", container).fetch();

		/* filter items by user permissions if necessary */
		if (user != null && !allCanBeReadBy) {
			Iterables.removeIf(ideas, new com.google.common.base.Predicate<Idea>() {
				@Override
				public boolean apply(Idea idea) {
					return !Security.check(Permission.READ_CONTAINER_ITEM, idea.id);
				}
			});
		}
		return ideas;
	}

	/**
	 * Find ideas of the current user
	 *
	 * @return
	 */
	public static List<Idea> findUserIdeas() {
		final String sql = "SELECT idea FROM Idea AS idea WHERE idea.creator = :user ORDER BY idea.created DESC";
		final TypedQuery<Idea> q = Idea.em().createQuery(sql, Idea.class);
		q.setParameter("user", UserLoader.getConnectedUser());

		return q.getResultList();
	}
}

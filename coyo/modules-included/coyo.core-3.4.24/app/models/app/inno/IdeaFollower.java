package models.app.inno;

import models.TenantModel;
import models.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 */
@Entity
@Table(name = "app_inno_idea_follower")
public class IdeaFollower extends TenantModel {

	@ManyToOne
	public User user;

	@ManyToOne
	public Idea idea;

	public static boolean doesFollow(final Idea idea, final User user) {
		for (IdeaFollower ideaFollower : idea.followers) {
			if (ideaFollower.user == user) {
				return true;
			}
		}

		return false;
	}
}

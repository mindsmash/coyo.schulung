package models.app.inno;

/**
 * Enum reflects the visibility of ideas
 */
public enum Visibility {
	PUBLIC("PUBLIC"),
	CHOICE("CHOICE"),
	ANON("ANON");

	private final String visibility;

	private Visibility(String status) {
		this.visibility = status;
	}
}
package models.app.inno;

/**
 * Status Enum reflects the different states of an innovation item
 */
public enum Status {
	NEW("NEW"),
	AUDIT("AUDIT"),
	ACCEPTED("ACCEPTED"),
	DECLINED("DECLINED");

	private final String status;

	private Status(String status) {
		this.status = status;
	}
}

package models.app.inno;

import models.User;
import org.apache.commons.lang3.StringUtils;
import play.Logger;

import java.util.Date;

public class FilterParams {
	private Long containerId;
	private String categoryId;
	private Status status;
	private OrderParam orderParam;
	private User author;
	private String from;
	private String till;
	private String text;
	private int firstResult;
	private int maxResults;
	private Boolean sensitiveInformation;

	public Boolean getSensitiveInformation() {
		return sensitiveInformation;
	}

	public void setSensitiveInformation(Boolean sensitiveInformation) {
		this.sensitiveInformation = sensitiveInformation;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}

	public Long getCategory() {
		if (StringUtils.isEmpty(categoryId)) {
			return null;
		}
		try {
			return Long.parseLong(categoryId);
		} catch (NumberFormatException e) {
			Logger.error("[InnoApp] Failed to read categroy id '%'", categoryId);
			return null;
		}

	}

	public OrderParam getOrderParam() {
		return orderParam;
	}

	public void setOrderParam(OrderParam orderParam) {
		this.orderParam = orderParam;
	}

	public void setCategory(String categoryId) {
		this.categoryId = categoryId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTill() {
		return till;
	}

	public void setTill(String till) {
		this.till = till;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

package models.app.inno;

import models.TenantModel;
import models.User;
import play.data.validation.Unique;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 */
@Entity
@Table(name = "app_inno_idea_rating")
public class IdeaRating extends TenantModel {

	@ManyToOne
	public User user;

	@ManyToOne
	public Idea idea;

	@Min(1)
	public int rating;
}

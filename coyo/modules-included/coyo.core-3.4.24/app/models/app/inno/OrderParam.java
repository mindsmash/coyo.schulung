package models.app.inno;

/**
 * Used for ordering idea search results (passed via JSON RPC)
 */
public enum OrderParam {
	RATING("RATING"),
	OLDEST("OLDEST"),
	NEWEST("NEWEST");

	private final String value;

	private OrderParam(String value) {
		this.value = value;
	}
}

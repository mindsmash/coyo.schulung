package models.app.inno;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.BaseModel;
import models.ExtendedSearchableModel;
import models.Sender;
import models.User;
import models.app.InnoApp;
import models.comments.Comment;
import models.comments.Commentable;
import models.container.Container;
import models.container.Item;
import models.notification.innoapp.IdeaChangedNotification;
import models.notification.innoapp.IdeaCommentedNotification;
import models.notification.innoapp.IdeaStateChangedNotification;
import models.notification.innoapp.IdeacreatedNotification;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.binding.NoBinding;
import play.data.validation.MaxSize;
import play.db.jpa.JPA;
import acl.Permission;
import controllers.Security;
import controllers.WebBaseController;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

/**
 * One idea for an InnovationApp
 *
 * @author Gerrit Dongus
 */
@Entity
@Indexed
@Searchable(category = "idea")
@Table(name = Idea.APP_INNO_IDEA_TABLE)
public class Idea extends Item implements Commentable, ExtendedSearchableModel {

	public static final String IDEA_COMMENTS_TABLE = "innoappitem_comments";
	public static final String APP_INNO_LINKED_IDEAS_TABLE = "app_inno_linked_ideas";
	public static final String APP_INNO_IDEA_TABLE = "app_inno_idea";

	// title
	@Field
	@NotEmpty
	@MaxSize(255)
	public String title = "";

	// hide display author information to others
	public Boolean isAnon = false;

	// status of the item
	@Enumerated(EnumType.STRING)
	public Status status = Status.NEW;

	// comment which can be added when changing the state
	@Lob
	public String statusComment;

	// description of this idea/item
	@Lob
	@Field
	@NotEmpty
	public String description;

	// hidden field only visible for moderators
	@Lob
	public String hiddenField;

	// comments of this idea
	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = IDEA_COMMENTS_TABLE)
	public List<Comment> comments = new ArrayList<>();

	// timestamp when the current state was set
	@NoBinding
	public Date stateChanged;

	@ManyToMany
	@JoinTable(name = APP_INNO_LINKED_IDEAS_TABLE)
	public Set<Idea> linkedIdeas = new HashSet<>();

	// ratings (users:stars)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idea")
	public List<IdeaRating> ratings = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idea")
	public Set<IdeaFollower> followers = new HashSet<>();

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "idea")
	public IdeaStateChangedNotification stateChangedNotification;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "idea")
	public IdeacreatedNotification createdNotification;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "idea")
	public IdeaCommentedNotification commentedNotification;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "idea")
	public IdeaChangedNotification changedNotification;

	/**
	 * Constructor
	 *
	 * @param container
	 * @param creator
	 */
	public Idea(final Container container, final User creator) {
		super(container, creator);
		this.status = Status.NEW;
	}

	/**
	 * Looks up if one InnoApps category is already used by one of its items
	 *
	 * @param category
	 *            Category
	 * @return true or false
	 */
	public static boolean isCategoryUsed(final Category category) {
		return !category.ideas.isEmpty();
	}

	public void rate(final User user, final int rating) {
		if (user == null) {
			return;
		}

		IdeaRating ideaRating = IdeaRating.find("user = ? AND idea = ?", user, this).first();

		if (ideaRating != null) {
			ideaRating.rating = rating;
			ideaRating.save();
		} else {
			ideaRating = new IdeaRating();
			ideaRating.user = user;
			ideaRating.rating = rating;
			ideaRating.idea = this;
			ideaRating.save();
			this.ratings.add(ideaRating);
			this.save();
		}
	}

	/**
	 * Add user to follower list
	 *
	 * @param user
	 */
	public void follow(final User user) {
		if (user == null) {
			return;
		}
		if (!IdeaFollower.doesFollow(this, user)) {
			final IdeaFollower ideaFollower = new IdeaFollower();
			ideaFollower.user = user;
			ideaFollower.idea = this;
			ideaFollower.save();
			this.followers.add(ideaFollower);
			this.save();
		}
	}

	/**
	 * Add user to follower list
	 *
	 * @param user
	 */
	public void unfollow(final User user) {
		if (user == null) {
			return;
		}
		final IdeaFollower ideaFollower = IdeaFollower.find("user = ? AND idea = ?", user, this).first();
		if (ideaFollower != null) {
			this.followers.remove(ideaFollower);
			ideaFollower.delete();
		}
		this.save();
	}

	/**
	 * Creates and returns a set of users that are following one idea Followers are the creator, moderators, followers
	 * and users who liked or commented an item
	 *
	 * @return Set<User> or null
	 */
	public Set<User> getAllFollowers() {
		final Set<User> followers = new HashSet<>();

		// creator
		followers.add(this.creator);
		// follower

		for (IdeaFollower ideaFollower : this.followers) {
			followers.add(ideaFollower.user);
		}

		// app moderators
		followers.addAll(this.getApp().moderators);

		// comments
		for (Comment comment : this.comments) {
			followers.add(comment.author);
		}

		return followers;
	}

	/**
	 * Add one comment
	 *
	 * @param comment
	 */
	@Override
	public void addComment(final Comment comment) {
		this.comments.add(comment);

		this.save();

		// create user notifications
		IdeaCommentedNotification.raise(this, comment);
	}

	/**
	 * Remove one comment
	 *
	 * @param comment
	 */
	@Override
	public void removeComment(Comment comment) {
		this.comments.remove(comment);
		this.save();

		// cleaup user notifications
		IdeaCommentedNotification.cleanup(this);
	}

	/**
	 * Get all comments
	 *
	 * @return
	 */
	@Override
	public List<Comment> getComments() {
		return this.comments;
	}

	/**
	 * Get status
	 *
	 * @return
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Change status
	 *
	 * @param status
	 * @param comment
	 */
	public void setStatus(final Status status, final String comment) {
		this.status = status;
		this.stateChanged = new Date();
		this.statusComment = comment;

		this.save();

		IdeaStateChangedNotification.raise(this);
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	/**
	 * @param user
	 * @return
	 */
	@Override
	public boolean checkCreateCommentPermission(User user) {
		return Security.check(Permission.ACCESS_APP, getApp().id);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return false;
	}

	@Override
	public void redirectToResult() {
		final Map<String, Object> args = new HashMap<String, Object>();
		args.put("idea", id);
		final InnoApp app = getApp();

		WebBaseController.redirectToSender(app.sender, app.id, args);
	}

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public Sender getSearchThumbSender() {
		InnoApp app = getApp();
		return app.sender;
	}

	@Override
	public String getSearchInfoText() {
		InnoApp app = getApp();
		return app.getSearchInfoText();
	}

	@Override
	public boolean checkSearchPermission() {
		InnoApp app = getApp();
		return Security.check(Permission.ACCESS_APP, app);
	}

	public InnoApp getApp() {
		return InnoApp.find("container = ?", container).first();
	}

	public Category getCategory(final InnoApp app) {
		Category category = (Category) play.cache.Cache.get(this.tenant + "-models.app.inno.Idea-" + this.id);

		if (category == null) {
			for (Category c : app.categories) {
				if (c.ideas != null && c.ideas.contains(this)) {
					category = c;
					break;
				}
			}
		}

		if (category != null) {
			play.cache.Cache.set(this.tenant + "-models.app.inno.Idea-" + this.id, category, "1d");
		}

		return category;
	}

	public double calculateAverageRating() {
		if (ratings == null || ratings.isEmpty()) {
			return 0.0;
		}

		long sum = 0L;
		int n = ratings.size();

		for (int i = 0; i < n; i++) {
			sum += ratings.get(i).rating;
		}

		return ((double) sum) / n;
	}

	public Integer getUserRating(final User user) {
		for (IdeaRating rating : ratings) {
			if (user == rating.user) {
				return rating.rating;
			}
		}

		return null;
	}

	@Field
	public List<String> getRelatedSearchComments() {
		return Comment.getSearchInfo(comments);
	}

	@Override
	public Sender getSender() {
		return this.container.sender;
	}

	@Override
	protected void beforeDelete() throws Exception {
		JPA.em().createNativeQuery("DELETE FROM " + IDEA_COMMENTS_TABLE + " WHERE APP_INNO_IDEA_ID  = " + id)
				.executeUpdate();
		super.beforeDelete();
	}

	@Override
	protected void afterUpdate() throws Exception {
		play.cache.Cache.delete(this.tenant + "-models.app.inno.Idea-" + this.id);
		super.afterInsert();
	}
}

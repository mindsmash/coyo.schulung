package models.app;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.User;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import play.data.validation.MinSize;
import fishr.Field;
import fishr.Indexed;
import fishr.Related;

@Entity
@Table(name = "app_wiki_article_version")
public class WikiAppArticleVersion extends TenantModel {

	@ManyToOne(optional = false)
	public WikiAppArticle article;

	@ManyToOne(optional = false)
	public User author;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Lob
	public String text;

	@MinSize(1)
	public long num;

	public String getCleanText() {
		return Jsoup.clean(text, Whitelist.none());
	}

	@Override
	protected void beforeInsert() throws Exception {
		num = count("article = ?", article) + 1;
		super.beforeInsert();
	}
}

package models.app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.google.gson.JsonElement;

import apps.SocialLinklistAppDescriptor;
import fishr.Field;
import fishr.search.Searchable;
import models.app.dao.SocialLinklistAppDao;
import play.mvc.Http.Request;

@Entity
@Searchable(category = "apps")
@Table(name = SocialLinklistApp.SOCIAL_LINKLIST_TABLE)
public class SocialLinklistApp extends App {

	public static final String SOCIAL_LINKLIST_TABLE = "app_social_linklist";

	public boolean usersCanAddLinks = true;
	public boolean usersCanEditLinks = true;
	public boolean linksTransferred = true;

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<SocialLink> links = new ArrayList<>();

	@Override
	public String getAppKey() {
		return SocialLinklistAppDescriptor.KEY;
	}
}
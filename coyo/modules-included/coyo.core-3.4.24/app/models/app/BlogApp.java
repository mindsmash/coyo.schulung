package models.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

import apps.BlogAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;
import models.User;
import models.app.dao.BlogAppDao;
import play.mvc.Http.Request;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_blog")
public class BlogApp extends App {

	@OneToMany(mappedBy = "app" , cascade = CascadeType.ALL)
	@OrderBy("publishDate DESC")
	public List<BlogAppArticle> articles = new ArrayList<BlogAppArticle>();

	public boolean autoshare = false;
	// make share posts sticky
	public boolean autoshareImportant = false;

	public boolean allowGuestAuthor = false;
	/**
	 * Users can comment articles in blog
	 */
	@Column(columnDefinition = "boolean default true")
	public boolean usersCanComment = true;

	public BlogAppArticle getArticle(String id) {
		if (!StringUtils.isNumeric(id)) {
			return null;
		}
		return BlogAppDao.getArticle(Long.valueOf(id), this);
	}

	public List<BlogAppArticle> getLatestArticles(int limit) {
		return BlogAppDao.getLatestArticle(this, limit);
	}

	public Map<Integer, Map<Integer, Long>> getArticleCountByMonth(User currentUser) {
		return BlogAppDao.getArticleCountByMonth(this, currentUser);
	}

	public List<BlogAppArticle> getArticlesByMonth(String yearMonth) {
		if (!StringUtils.isNumeric(yearMonth)) {
			return Collections.EMPTY_LIST;
		}
		Integer month = Integer.valueOf(yearMonth) % 100;
		Integer year = (Integer.valueOf(yearMonth) / 100);
		return getArticlesByMonth(year, month);
	}

	public List<BlogAppArticle> getArticlesByMonth(int year, int month) {
		if (month < 1 || month > 12 || year > 9999) {
			return Collections.EMPTY_LIST;
		}
		return BlogAppDao.getArticlesByMonth(this, month, year);
	}

	@Override
	public String getAppKey() {
		return BlogAppDescriptor.KEY;
	}

	@Override
	public void bind(Request request) {
		autoshare = Boolean.parseBoolean(request.params.get("autoshare"));
		allowGuestAuthor = Boolean.parseBoolean(request.params.get("allowGuestAuthor"));
		autoshareImportant = Boolean.parseBoolean(request.params.get("autoshareImportant"));
		usersCanComment = Boolean.parseBoolean(request.params.get("usersCanComment"));

		super.bind(request);
	}

	@Override
	public void _delete() {
		for (BlogAppArticle article : articles) {
			article.delete();
		}
		articles.clear();
		super._delete();
	}
}

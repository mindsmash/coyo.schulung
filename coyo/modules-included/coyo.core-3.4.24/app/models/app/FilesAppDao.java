package models.app;

import models.BoxFile;

import javax.persistence.TypedQuery;

import session.UserLoader;

import java.util.List;

public class FilesAppDao {

	public static List<BoxFile> getLatestFiles() {
		final String sql = "SELECT boxfile FROM BoxFile AS boxfile WHERE "
				+ "boxfile.folder = false AND EXISTS (SELECT box FROM Box AS box WHERE boxfile MEMBER OF box.files AND EXISTS "
				+ "(SELECT filesapp FROM LocalFilesApp AS filesapp WHERE filesapp.box = box AND filesapp.active = true AND "
				+ ":user MEMBER OF filesapp.sender.followers)) ORDER BY boxfile.modified DESC";

		final TypedQuery<BoxFile> query = LocalFilesApp.em().createQuery(sql, BoxFile.class);
		query.setParameter("user", UserLoader.getConnectedUser());
		query.setMaxResults(5);

		return query.getResultList();
	}
}

package models.app.dao;

import java.util.List;

import com.google.gson.JsonElement;

import models.app.App;
import models.app.SocialLink;
import models.app.SocialLinklistApp;

public class SocialLinklistAppDao {

	public static List<SocialLink> getLinkList(App linklistApp) {
		return SocialLink.find("app = ? ORDER BY priority ASC, created ASC", linklistApp).fetch();
	}

	public static List<SocialLink> getTopFiveLinks(App linklistApp) {
		return SocialLink.find("app = ? ORDER BY priority ASC, created ASC", linklistApp).fetch(5);
	}
}

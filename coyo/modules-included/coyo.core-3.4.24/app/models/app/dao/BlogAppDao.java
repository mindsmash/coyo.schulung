package models.app.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.TypedQuery;

import models.User;
import models.app.App;
import models.app.BlogApp;
import models.app.BlogAppArticle;

import models.wall.Wall;
import models.wall.post.ShareBlogArticlePost;
import org.joda.time.DateTime;

import play.db.jpa.JPA;
import security.CheckCaller;
import session.UserLoader;
import acl.AppsPermission;

public class BlogAppDao {

	private static String editSenderPermissionCheck = " AND ((a.app.allowGuestAuthor = TRUE AND a.author = :connectedUser) OR (a.publishDate <= :publishDate)) ";

	public static BlogAppArticle getArticle(Long id, App blogApp) {
		return BlogAppArticle.find("id = ? AND app = ?", Long.valueOf(id), blogApp).first();
	}

	public static List<BlogAppArticle> getLatestArticle(App blogApp, int limit) {
		return BlogAppArticle.find("app = ? AND publishDate <= ? ORDER BY publishDate DESC", blogApp, new DateTime())
				.fetch(limit);
	}

	/**
	 * retrieves the latest articles of a {@link BlogApp} does all the permission checks within the sql
	 *
	 * @param blogApp
	 * @param firstResult
	 * @param maxResult
	 * @param canEditSenderPermission
	 * @return
	 */
	public static List<BlogAppArticle> getLatestArticlesPaged(BlogApp blogApp, Integer year, Integer month, int firstResult, int maxResult,
			boolean canEditSenderPermission) {
		final StringBuffer sql = new StringBuffer();

		sql.append("SELECT a FROM BlogAppArticle a WHERE a.app = :app ");

		if (!canEditSenderPermission) {
			sql.append(editSenderPermissionCheck);
		}

		if (year != null && month != null) {
			if (month < 1 || month > 12 || year > 9999) {
				return Collections.EMPTY_LIST;
			} else {
				sql.append(" AND MONTH(a.publishDate) = :month and YEAR(a.publishDate) = :year ");
			}
		}
		sql.append(" ORDER BY a.publishDate DESC");

		final TypedQuery<BlogAppArticle> q = JPA.em().createQuery(sql.toString(), BlogAppArticle.class);
		q.setParameter("app", blogApp);

		if (!canEditSenderPermission) {
			q.setParameter("publishDate", new DateTime());
			q.setParameter("connectedUser", UserLoader.getConnectedUser());
		}

		if (year != null && month != null) {
			q.setParameter("month", month);
			q.setParameter("year", year);
		}

		q.setFirstResult(firstResult);
		if (maxResult > 0) {
			q.setMaxResults(maxResult);
		}

		return q.getResultList();
	}

	public static Long countLatestArticles(BlogApp blogApp, Integer year, Integer month, boolean canEditSenderPermission) {
		final StringBuffer sql = new StringBuffer();

		sql.append("SELECT COUNT(a) FROM BlogAppArticle a WHERE a.app = :app ");

		if (!canEditSenderPermission) {
			sql.append(editSenderPermissionCheck);
		}

		if (year != null && month != null) {
			if (month < 1 || month > 12 || year > 9999) {
				return 0L;
			} else {
				sql.append(" AND MONTH(a.publishDate) = :month and YEAR(a.publishDate) = :year ");
			}
		}

		final TypedQuery<Long> q = JPA.em().createQuery(sql.toString(), Long.class);
		q.setParameter("app", blogApp);

		if (!canEditSenderPermission) {
			q.setParameter("publishDate", new DateTime());
			q.setParameter("connectedUser", UserLoader.getConnectedUser());
		}

		if (year != null && month != null) {
			q.setParameter("month", month);
			q.setParameter("year", year);
		}

		return q.getSingleResult();
	}


	public static Map<Integer, Map<Integer, Long>> getArticleCountByMonth(App blogApp, User currentUser) {
		Map<Integer, Map<Integer, Long>> result = new HashMap<>();

		boolean canEdit = CheckCaller.check(AppsPermission.EDIT_BLOG, blogApp.id);

		List<BlogAppArticle> articles = BlogAppArticle.find("app = ?", blogApp).fetch();
		for(BlogAppArticle article : articles) {
			if(article.publishDate.isAfterNow() && !canEdit) {
				continue;
			}
			if(CheckCaller.check(AppsPermission.ACCESS_BLOG_ARTICLE, article)) {
				if(!result.containsKey(article.publishDate.getYear())) {
					result.put(article.publishDate.getYear(), new TreeMap<Integer, Long>(java.util.Collections.reverseOrder()));
				}
				Map<Integer, Long> map = result.get(article.publishDate.getYear());
				map.put(
						article.publishDate.getMonthOfYear(),
						map.containsKey(article.publishDate.getMonthOfYear()) ?
								map.get(article.publishDate.getMonthOfYear()) + 1l :
								1l);
			}
		}
		return result;
	}

	public static List<BlogAppArticle> getArticlesByMonth(App blogApp, int month, int year) {
		List<BlogAppArticle> r = new ArrayList<>();
		List<BlogAppArticle> raw = BlogAppArticle
				.find("app = ? AND MONTH(publishDate) = ? and YEAR(publishDate) = ? ORDER BY publishDate DESC", blogApp,
						month, year)
				.fetch();
		for (BlogAppArticle article : raw) {
			if (CheckCaller.check(AppsPermission.ACCESS_BLOG_ARTICLE, article)) {
				r.add(article);
			}
		}
		return r;
	}

	public static boolean hasArticleBeenShared(final BlogAppArticle article) {
		return ShareBlogArticlePost.count("article = ?", article) > 0;
	}

	public static boolean hasBeenAutoShared(final BlogAppArticle article) {
		final Wall defaultWall = article.app.sender.getDefaultWall();

		return defaultWall == null
				? false
				: ShareBlogArticlePost.count("article = ? AND wall = ?", article, defaultWall) > 0;
	}
}

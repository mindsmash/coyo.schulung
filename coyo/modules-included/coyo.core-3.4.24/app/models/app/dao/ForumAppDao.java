package models.app.dao;

import java.util.List;

import models.app.ForumApp;
import models.app.forum.ForumAppTopic;
import models.app.forum.ForumPost;

public class ForumAppDao {
	
	public static List<ForumAppTopic> getLatestTopics(ForumApp app, int limit) {
		return ForumAppTopic.find("app = ? ORDER BY modified DESC", app).fetch(limit);
	}

}
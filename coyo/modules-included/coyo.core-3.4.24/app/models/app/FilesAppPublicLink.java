package models.app;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;

@Entity
@Table(name = "app_files_public_link")
public class FilesAppPublicLink extends TenantModel {

	@ManyToOne(optional = false)
	public FilesApp app;
	public String fileUid;
	public String token;
}

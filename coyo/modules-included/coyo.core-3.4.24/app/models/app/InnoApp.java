package models.app;

import apps.InnoAppDescriptor;
import containers.form.ContainerFormData;
import containers.form.FieldFormData;
import fishr.Indexed;
import fishr.search.Searchable;
import models.User;
import models.app.inno.Category;
import models.app.inno.Idea;
import models.app.inno.Status;
import models.app.inno.Visibility;
import models.container.*;
import models.container.fields.OptionsField;
import models.helper.ClearManyToManyHelper;
import models.notification.Notification;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.LoggerFactory;
import play.Logger;
import play.Play;
import play.data.binding.NoBinding;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import play.db.jpa.Transactional;
import play.mvc.Http.Request;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "app_inno")
@Indexed
@Searchable(category = "apps")
public class InnoApp extends App implements AngularApp {
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(InnoApp.class);

	public static final String INNO_APP_FIELD = "innoApp";
	public static final String CONTAINER_FIELD = "container";

	// enable/disable configuration parameters
	public static final boolean FEATURE_AUTHOR_SEARCH = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.app.inno.authorSearch", "true"));

	public static final boolean FEATURE_REVEAL = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.app.inno.reveal", "true"));

	public boolean commentsAllowed = false;

	public boolean canBeRevoked = false;

	public boolean archived = false;

	public boolean ideasHiddenField = false;

	public boolean allowSearchByAuthor = true;

	public boolean allowDeanonymization = true;

	public int ttl = 0;

	@Lob
	@NotEmpty
	public String description;

	@Enumerated(EnumType.STRING)
	public Visibility visibility = Visibility.PUBLIC;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app")
	@OrderBy("name ASC")
	public Set<Category> categories = new HashSet<>();

	@ManyToMany
	public Set<User> moderators = new HashSet<>();

	@OneToOne(optional = false, cascade =  CascadeType.ALL)
	public Container container;

	@Override
	public String getAppKey() {
		return InnoAppDescriptor.KEY;
	}

	@Override
	@Transactional
	public void bind(Request request) {
		if (!request.params._contains(INNO_APP_FIELD)) {
			throw new IllegalArgumentException("[InnoApp] Missing InnoApp form data");
		}
		final InnoApp app = controllers.InnoApp.createBuilder().create()
				.fromJson(request.params.get(INNO_APP_FIELD), InnoApp.class);
		final ContainerFormData formData =  controllers.InnoApp.createBuilder().create()
				.fromJson(request.params.get(CONTAINER_FIELD), ContainerFormData.class);

		// if disabled via config do not allow configuration of these app settings
		this.allowSearchByAuthor = InnoApp.FEATURE_AUTHOR_SEARCH == false ? InnoApp.FEATURE_AUTHOR_SEARCH : app.allowSearchByAuthor;
		this.allowDeanonymization = InnoApp.FEATURE_REVEAL == false ? InnoApp.FEATURE_REVEAL : app.allowDeanonymization;

		this.title = app.title;
		this.description = app.description;
		this.visibility = app.visibility;
		this.canBeRevoked = app.canBeRevoked;
		this.commentsAllowed = app.commentsAllowed;
		this.archived = app.archived;
		this.ideasHiddenField = app.ideasHiddenField;
		this.ttl = app.ttl;


		// if no container is present (new app) create one and merge so it contains an id
		if (this.container == null) {
			this.container = createContainer(app.container.fields);
		}

		syncCategories(app.categories);
		syncModerators(app.moderators);

		try {
			updateContainerData(formData);

		} catch (Throwable throwable) {
			LOGGER.error(throwable.getLocalizedMessage(), throwable);
			throw new RuntimeException(throwable);
		}

		play.cache.Cache.delete(getCacheId(id, tenant.toString()));

		this.save();
		super.bind(request);
	}

	/**
	 * Creates a new container which is used to hold new innovation item
	 *
	 * @param fields
	 * @return
	 */
	private Container createContainer(final List<Field> fields) {
		final Container container = new Container(sender);
		container.name = this.getDisplayName();
		container.setPermission(ContainerPermission.Create, true);
		container.setPermission(ContainerPermission.ReadAll, true);
		container.setPermission(ContainerPermission.ReadOwn, true);
		container.setPermission(ContainerPermission.UpdateAll, true);
		container.setPermission(ContainerPermission.UpdateOwn, true);
		container.setPermission(ContainerPermission.DeleteAll, true);
		container.setPermission(ContainerPermission.DeleteOwn, true);
		container.save();

		return container;
	}

	private void updateContainerData(ContainerFormData containerFormData) {
		final List<FieldFormData> fields = containerFormData.fields;
		final List<Field> addedFields = new ArrayList<>();

		int priority = 0;
		for (FieldFormData fieldFormData : fields) {
			priority = priority +10;
			Field field = Field.updateOrCreateField(container, fieldFormData);
			field.priority = priority;
			field.save();
			container.addField(field);
			addedFields.add(field);

			if (field instanceof OptionsField) {
				FieldFactory.updateOptions((OptionsField) field, fieldFormData.options);
			}

			field.save();
		}

		container.removeNonPresentFields(addedFields);
		container.save();
	}

	/**
	 * Sync model with app config moderator list
	 *
	 * @param newModerators
	 */
	private void syncModerators(final Set<User> newModerators) {
		// fetch from db as the deserialize user obj is missing some data
		this.moderators.clear();

		for (User user : newModerators) {
			final User moderator = User.findById(user.id);
			if (moderator != null && !moderators.contains(moderator)) {
				this.moderators.add(moderator);
			}
		}
	}

	/**
	 * Sync model with app config categories
	 */
	private void syncCategories(final Set<Category> newCategories) {
		// delete orphaned categories
		for (Iterator<Category> iterator = categories.iterator(); iterator.hasNext(); ) {
			Category category = iterator.next();
			if (!newCategories.contains(category) && !Idea.isCategoryUsed(category)) {
				// get managed entitiy
				final Category categoryOrphaned = Category.findById(category.id);
				iterator.remove();
				categoryOrphaned.delete();
				this.save();
			}
		}
		// add new category items
		for (Category newCategory : newCategories) {
			if (!categories.contains(newCategory) && !StringUtils.isEmpty(newCategory.name)) {
				this.save();
				newCategory.app = this;
				newCategory.save();
				categories.add(newCategory);
			}
		}


	}

	/**
	 * Finds and returns ideas with expired states
	 *
	 * @return
	 */
	// TODO solve by nested SQL (get app -> get days param -> create date from days -> query comparing created with date param
	public static List<Idea> findExpiredIdeas() {
		final List<InnoApp> innoApps = InnoApp.find("ttl > 0").fetch(); // 0 == forever
		final List<Idea> expiredItems = new ArrayList<>();

		for (InnoApp innoApp : innoApps) {
			final List<Item> items = innoApp.container.items;

			for (Item item : items) {
				final Idea idea = (Idea) item;

				// skip if idea status != NEW
				if (idea.getStatus() != Status.NEW) {
					continue;
				}

				if (Days.daysBetween(new DateTime(idea.created), DateTime.now()).getDays() >= innoApp.ttl) {
					expiredItems.add(idea);
				}
			}
		}
		return expiredItems;
	}


	public static String getCacheId(final Long id, final String tenant) {
		return tenant + "models.app.InnoApp-" + id;
	}

	/**
	 * Remove linked ideas to solve many to many constraint violation when deleting the app
	 *
	 * @throws Exception
	 */
	@Override
	protected void beforeDelete() throws Exception {
		for (Item item : container.items) {
			final Idea idea = (Idea) item;
			final String sql = "SELECT idea FROM Idea AS idea WHERE :idea MEMBER OF idea.linkedIdeas";
			final TypedQuery<Idea> q = Idea.em().createQuery(sql, Idea.class);
			q.setParameter("idea", idea);

			final List<Idea> linkedIdeas = q.getResultList();

			for (Idea linkedIdea : linkedIdeas) {
				linkedIdea.linkedIdeas.remove(idea);
				linkedIdea.save();
			}
		}

		super.beforeDelete();
	}
}
package models.app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;

import org.apache.commons.lang3.StringUtils;

import apps.AppWidget;

/**
 * Mapping between a target app and the widgets of other source apps which should be displayed on the rightbar of that
 * target app.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Table(name = "app_widget")
public class WidgetMapping extends TenantModel {

	/**
	 * The app that the widget is displayed on.
	 */
	@ManyToOne(optional = false)
	public App targetApp;

	/**
	 * The app that the widgets is from.
	 */
	@ManyToOne(optional = false)
	public App sourceApp;

	@Column(nullable = false)
	public String widgetKey;
	
	public int priority = 9999;

	public String title;

	public AppWidget getAppWidget() {
		return sourceApp.getAppDescriptor().getWidget(widgetKey);
	}
	
	public String getTitle() {
		return (StringUtils.isEmpty(title)) ? getAppWidget().getName() : title;
	}



	@Override
	protected void beforeSave() throws Exception {
		if (getAppWidget() == null) {
			throw new IllegalArgumentException("Cannot save WidgetMapping because the given widget [" + widgetKey
					+ "] does not exist in app [" + sourceApp.getAppKey() + "]");
		}

		if (sourceApp.sender != targetApp.sender) {
			throw new IllegalArgumentException("Cannot create WidgetMapping for apps from different senders]");
		}

		super.beforeSave();
	}
}

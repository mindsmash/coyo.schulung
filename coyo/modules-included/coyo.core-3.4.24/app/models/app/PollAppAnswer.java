package models.app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.*;
import models.helper.ClearManyToMany;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.JPA;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;
import fishr.search.SearchableModel;

@Entity
@Table(name = "app_polls_answer")
public class PollAppAnswer extends TenantModel {

	public static final String POLL_ANSWER_USER_TABLE = "app_polls_answer_user";

	@ManyToOne(optional = false)
	public PollApp app;

	@ManyToMany
	@JoinTable(name = POLL_ANSWER_USER_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> users = new ArrayList<User>();

	@Required
	@MaxSize(255)
	public String name;

	public int rank = 9999;

	public float getPercent() {
		if (users.size() == 0) {
			return 0F;
		}

		float total = 0;
		for (PollAppAnswer answer : app.answers) {
			total += answer.users.size();
		}
		return users.size() / total * 100;
	}

	public boolean canVote(User user) {
		// check frozen
		if (app.frozen) {
			return false;
		}

		// check already voted
		if (users.contains(user)) {
			return false;
		}

		// check too many votes
		if (PollAppAnswer.count("e.app = ? AND ? MEMBER OF e.users", app, user) >= app.maxAnswers) {
			return false;
		}
		return true;
	}

	public boolean canUnvote(User user) {
		// check frozen
		if (app.frozen) {
			return false;
		}

		return users.contains(user);
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + POLL_ANSWER_USER_TABLE + " WHERE app_polls_answer_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}

	@Override
	protected void beforeSave() throws Exception {
		// assure no double answers
		if (this.users != null) {
			Set<User> users = new HashSet<User>(this.users);
			this.users.clear();
			this.users.addAll(users);
		}

		super.beforeSave();
	}

//	@Override
//	public boolean checkSearchPermission() {
//		return app.checkSearchPermission();
//	}
//
//	@Override
//	public void redirectToResult() {
//		app.redirectToResult();
//	}
//
//	@Override
//	public String getDisplayName() {
//		return name;
//	}
//
//	@Override
//	public Sender getSearchThumbSender() {
//		return app.sender;
//	}
//
//	@Override
//	public String getSearchInfoText() {
//		return app.getSearchInfoText();
//	}
}

package models.app;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.container.Container;
import models.container.ContainerPermission;

import org.apache.commons.lang.BooleanUtils;

import org.slf4j.LoggerFactory;
import play.mvc.Http.Request;
import apps.ListAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@ContainerApp(fieldName = "container")
@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_list")
public class ListApp extends App implements AngularApp{
	
	public static enum NotificationRecipient {
		NONE, ADMINS, FOLLOWERS;
	}
	
	private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ListApp.class);

	@OneToOne(cascade = CascadeType.ALL)
	public Container container;

	public NotificationRecipient notificationRecipient = NotificationRecipient.FOLLOWERS;

	@Override
	public String getAppKey() {
		return ListAppDescriptor.KEY;
	}

	@Override
	public void bind(Request request) {
		if (container == null) {
			container = new Container(sender);
		}

		/* set entity name */
		container.name = request.params.get("entityName");

		/* set all permissions */
		container.setPermission(ContainerPermission.Create, BooleanUtils.toBoolean(request.params.get("Create")));
		container.setPermission(ContainerPermission.ReadAll, checkPermissionParameter(request, "Read", "all"));
		container.setPermission(ContainerPermission.ReadOwn, checkPermissionParameter(request, "Read", "all", "own"));
		container.setPermission(ContainerPermission.UpdateAll, checkPermissionParameter(request, "Update", "all"));
		container.setPermission(ContainerPermission.UpdateOwn,
				checkPermissionParameter(request, "Update", "all", "own"));
		container.setPermission(ContainerPermission.DeleteAll, checkPermissionParameter(request, "Delete", "all"));
		container.setPermission(ContainerPermission.DeleteOwn,
				checkPermissionParameter(request, "Delete", "all", "own"));

		/* save container */
		container.save();

		// set notification recipient
		try {
			this.notificationRecipient = NotificationRecipient.valueOf(request.params.get("notificationRecipient"));
		} catch (IllegalArgumentException ex) {
			LOG.error("[ListApp] " + ex.getLocalizedMessage(), ex);
		}

		super.bind(request);
	}

	/**
	 * Returns the property for users to receive notifications about new or modified list items. If this property was
	 * never set, return the default.
	 * 
	 * @return the property for users to receive notifications or the default value.
	 */
	public NotificationRecipient getNotificationRecipient() {
		return (notificationRecipient != null) ? notificationRecipient : NotificationRecipient.FOLLOWERS;
	}

	private static Boolean checkPermissionParameter(Request request, String key, String... expectedValues) {
		Boolean result = false;
		String parameter = request.params.get(key);
		if (parameter != null) {
			for (String value : expectedValues) {
				result = result || parameter.equals(value);
			}
		}
		return result;
	}

}
package models.app.files;

import java.io.InputStream;

import models.User;

import org.joda.time.DateTime;

/**
 * 
 * Interface to implement, when you want a {@link File} to have multiple
 * FileVersions, otherways implement a dummy FileVersion for your corresponding
 * {@link File}.
 * 
 * @author mindsmash GmbH
 * 
 */
public interface FileVersion {

	User getCreator();

	File getFile();

	long getSize();

	InputStream get();

	int getVersionNumber();

	DateTime getCreated(User user);
}

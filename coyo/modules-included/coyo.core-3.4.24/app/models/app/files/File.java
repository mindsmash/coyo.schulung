package models.app.files;

import java.io.InputStream;
import java.util.List;

import models.ExtendedSearchableModel;
import models.Previewable;
import models.Sharable;
import models.User;
import models.app.FilesApp;

import org.joda.time.DateTime;

/**
 * @author mindsmash GmbH
 */
public interface File extends Comparable<File>, Sharable, Previewable {

	String getUid();

	String getName();

	DateTime getModified(User user);

	DateTime getCreated(User user);

	String getContentType();

	User getCreator();

	User getModifier();

	void rename(String name);

	void move(File destination);

	void copy(File destination);

	void remove();

	boolean lock(User user);

	boolean unlock(User user);

	User getLock();

	boolean isFolder();

	FileVersion getCurrentVersion();

	List<? extends FileVersion> getVersions();

	FileVersion getVersion(int no);

	FileVersion addVersion(User user, InputStream in, Long length);

	File getParent();

	List<? extends File> getChildren();

	List<? extends File> getChildFolders();

	boolean isChild(File file, boolean recursive);

	boolean checkAccessPermission(User user);

	boolean checkModifyPermission(User user);
	
	FilesApp getApp();
	
	boolean allowVersioning();

	boolean allowRename();

	boolean allowMove();

	boolean allowCopy();
	
	boolean allowDelete();
}

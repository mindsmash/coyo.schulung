package models.app;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Sender;
import models.User;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import validation.SimpleURL;
import acl.Permission;
import controllers.Security;
import controllers.WebBaseController;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Table(name = SocialLink.SOCIAL_LINK_TABLE)
@Searchable(category = "socialLinks")
public class SocialLink extends ExtendedTenantModel implements ExtendedSearchableModel {

	public static final String SOCIAL_LINK_TABLE = "app_social_linklist_link";

	@ManyToOne(optional = false)
	public SocialLinklistApp app;

	public int priority = 9999;

	@Field
	@Required
	@MaxSize(120)
	public String title;

	@Required
	@SimpleURL
	@MaxSize(255)
	public String linkUrl;

	@Field
	@Lob
	public String description;

	public String faviconUrl;

	@ManyToOne(optional = true)
	public User author;

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public Sender getSearchThumbSender() {
		return this.app.sender;
	}

	@Override
	public String getSearchInfoText() {
		return this.app.getSearchInfoText();
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.ACCESS_APP, app);
	}

	@Override
	public void redirectToResult() {
		WebBaseController.redirectToSender(app.sender, app.id);
	}

}
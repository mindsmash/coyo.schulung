package models.app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import play.cache.Cache;
import play.db.jpa.JPA;
import play.mvc.Http.Request;
import apps.WikiAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_wiki")
public class WikiApp extends App {

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	@OrderBy("priority, title")
	public List<WikiAppArticle> articles = new ArrayList<WikiAppArticle>();

	/**
	 * Only admins can delete wiki articles.
	 */
	public boolean onlyAdminsCanDelete;

	/**
	 * Only admins can edit wiki articles. Implies {@link WikiApp#onlyAdminsCanDelete}.
	 */
	public boolean onlyAdminsCanEdit;
	
	/**
	 * Users can comment articles in wiki
	 */
	@Column(columnDefinition="boolean default false")
	public boolean usersCanComment = false;

	@Transient
	private WikiAppArticle home;

	public WikiAppArticle getHome() {
		if (home == null) {
			home = WikiAppArticle.find("app = ? AND home = true", this).first();
		}
		return home;
	}

	@Override
	public void bind(Request request) {
		onlyAdminsCanDelete = request.params._contains("onlyAdminsCanDelete")
				&& Boolean.valueOf(request.params.get("onlyAdminsCanDelete")).booleanValue();
		onlyAdminsCanEdit = request.params._contains("onlyAdminsCanEdit")
				&& Boolean.valueOf(request.params.get("onlyAdminsCanEdit")).booleanValue();
		usersCanComment = request.params._contains("usersCanComment")
				&& Boolean.valueOf(request.params.get("usersCanComment")).booleanValue();

		if (isPersistent()) {
			Long homeArticleId = request.params.get("homeArticleId", Long.class);
			if (homeArticleId == null) {
				// unset home
				JPA.em().createQuery("UPDATE WikiAppArticle a SET a.home = false WHERE a.app = ?")
						.setParameter(1, this).executeUpdate();
			} else {
				WikiAppArticle article = WikiAppArticle.findById(homeArticleId);
				if (article.app == this) {
					article.home = true;
					article.save();
				}
			}
		}

		super.bind(request);
	}

	public WikiAppArticle getArticle(String id) {
		if (!StringUtils.isNumeric(id)) {
			return null;
		}
		return WikiAppArticle.find("id = ? AND app = ?", Long.valueOf(id), this).first();
	}

	public List<WikiAppArticle> getLatestArticles(int limit) {
		return WikiAppArticle.find("app = ? ORDER BY modified DESC", this).fetch(limit);
	}

	public List<WikiAppArticle> getRootArticles() {
		return WikiAppArticle.find("app = ? AND parent IS NULL ORDER BY priority, title ASC", this).fetch();
	}

	@Override
	public String getAppKey() {
		return WikiAppDescriptor.KEY;
	}

	@Override
	protected void afterSave() throws Exception {
		// clear cache
		Cache.delete("wiki-nav-" + id);

		super.afterSave();
	}

	@Override
	public void _delete() {
		// delete articles
		for (WikiAppArticle article : getRootArticles()) {
			article.delete();
		}

		super._delete();
	}

	public void setOnlyAdminsCanEdit(boolean value) {
		this.onlyAdminsCanEdit = value;
		if (value == true) {
			this.onlyAdminsCanDelete = true;
		}
	}
}

package models.app;

import javax.persistence.Entity;
import javax.persistence.Table;

import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Min;
import validation.SimpleURL;
import apps.URLAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_url")
public class URLApp extends App {

	// iframe url
	@SimpleURL
	@MaxSize(255)

	public String url;

	@Min(value = 1)
	public int height = 500;
	public boolean scrolling = true;

	@Override
	public String getAppKey() {
		return URLAppDescriptor.KEY;
	}
}

package models.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.FormField;
import models.FormResult;
import models.User;
import models.helper.ClearManyToMany;

import org.apache.commons.lang.StringUtils;

import play.db.jpa.JPA;
import play.mvc.Http.Request;
import util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_forms")
public class FormsApp extends App {

	public static final String FORM_RECIPIENT_TABLE = "app_forms_recipient";
	public static final String FORM_RECIPIENT_EXTERNAL_TABLE = "app_forms_recipient_external";

	@Lob
	@Field
	public String description;

	@ManyToMany
	@JoinTable(name = FORM_RECIPIENT_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> receivers = new ArrayList<User>();

	@ElementCollection
	@JoinTable(name = FORM_RECIPIENT_EXTERNAL_TABLE)
	public List<String> externalRecipients = new ArrayList<String>();

	public boolean sendToUser;

	@OrderBy("fieldOrder ASC")
	@OneToMany(mappedBy = "form", cascade = CascadeType.ALL)
	public List<FormField> fields = new ArrayList<FormField>();

	@OneToMany(mappedBy = "form", cascade = CascadeType.ALL)
	public List<FormResult> results = new ArrayList<FormResult>();

	public Integer maxSubmits;

	@Override
	public String getAppKey() {
		return "forms";
	}

	private void clearFields() {
		for (FormField field : fields) {
			field.delete();
		}
		fields.clear();
	}

	public boolean maxReached(User user) {
		return maxSubmits != null && maxSubmits > 0
				&& FormResult.count("form = ? AND user = ?", this, user) >= maxSubmits;
	}

	@Override
	public void bind(Request request) {
		// bind recipients
		this.receivers.clear();
		String[] userIdsString = request.params.getAll("forms_receivers");
		if (userIdsString != null) {
			List<String> userIds = Arrays.asList(userIdsString);
			for (String userId : userIds) {
				this.receivers.add((User) User.findById(Long.valueOf(userId)));
			}
		}

		// bind external recipients
		if (!StringUtils.isEmpty(request.params.get("forms_external_recipients"))) {
			List<String> emailList = Util.parseEmailList(request.params.get("forms_external_recipients"));
			this.externalRecipients.clear();
			this.externalRecipients.addAll(emailList);
		} else {
			this.externalRecipients.clear();
		}

		// bind form
		String formsString = request.params.get("forms_formString");

		JsonElement parse = new JsonParser().parse(formsString);

		if (!parse.isJsonArray()) {
			clearFields();
			super.bind(request);
			return;
		}

		JsonArray fields = parse.getAsJsonArray();
		Gson gson = new Gson();
		List<FormField> addFields = new ArrayList<FormField>();

		for (JsonElement field : fields) {
			FormField jsonField = gson.fromJson(field, FormField.class);
			FormField formField;

			if (jsonField.id != null) {
				formField = FormField.findById(jsonField.id);
			} else {
				formField = new FormField();
				formField.form = this;
				formField.type = jsonField.type;
			}

			formField.fieldOrder = jsonField.fieldOrder;
			formField.label = jsonField.label;
			formField.placeholder = jsonField.placeholder;
			formField.helpText = jsonField.helpText;
			formField.bindOptions(jsonField.options);
			formField.required = jsonField.required;
			formField.cssClass = jsonField.cssClass;

			addFields.add(formField);
		}

		// delete obsolete fields
		this.fields.removeAll(addFields);
		clearFields();
		this.fields.addAll(addFields);
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + FORM_RECIPIENT_TABLE + " WHERE app_forms_id = " + id)
				.executeUpdate();

		// clear element collection (for some reason not done automatically,
		// when sender is deleted)
		JPA.em().createNativeQuery("DELETE FROM " + FORM_RECIPIENT_EXTERNAL_TABLE + " WHERE FormsApp_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}
}

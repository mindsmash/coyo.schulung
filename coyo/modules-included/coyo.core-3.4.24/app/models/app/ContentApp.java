package models.app;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import play.data.validation.Required;
import play.mvc.Http.Request;
import apps.ContentAppDescriptor;
import extensions.CommonExtensions;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "apps")
@Table(name = "app_content")
public class ContentApp extends App {

	@Lob
	@Required
	@Field
	public String html = "";

	/**
	 * Returns the content as plain text. This is used to display an extract for the search result.
	 */
	public String getText() {
		return Jsoup.clean(html, Whitelist.none());
	}

	@Override
	public String getAppKey() {
		return ContentAppDescriptor.KEY;
	}
}
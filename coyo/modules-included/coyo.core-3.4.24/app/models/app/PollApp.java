package models.app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.Sharable;
import models.User;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.SharePollPost;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Http.Request;
import security.CheckCaller;
import acl.AppsPermission;
import apps.PollAppDescriptor;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "polls")
@Table(name = "app_polls")
public class PollApp extends App implements Sharable {

	@Required
	@Field
	@MaxSize(255)
	public String question;
	@Lob
	@Field
	public String description;
	public boolean anonymous;
	public int maxAnswers = 1; // per user
	public String type = "pie";
	public boolean frozen;

	@OrderBy("rank")
	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<PollAppAnswer> answers = new ArrayList<PollAppAnswer>();

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<SharePollPost> posts = new ArrayList<SharePollPost>();

	@Override
	public void bind(Request request) {
		answers.clear();

		// iterate all submitted answers
		List<String> done = new ArrayList<String>();
		for (String param : request.params.allSimple().keySet()) {
			if (!param.startsWith("answer")) {
				continue;
			}
			String i = param.substring(7, param.lastIndexOf("."));
			if (!StringUtils.isNumeric(i) || done.contains(i)) {
				continue;
			}

			String name = request.params.get("answer." + i + ".name");
			String rank = request.params.get("answer." + i + ".rank");
			String id = request.params.get("answer." + i + ".id");

			PollAppAnswer answer;
			if (!StringUtils.isEmpty(id) && StringUtils.isNumeric(id)) {
				answer = PollAppAnswer.findById(new Long(id));
				Logger.debug("loading existing answer [%s]", name);
			} else {
				answer = new PollAppAnswer();
				answer.users = null; // required to avoid exception
				answer.app = this;
				Logger.debug("adding new answer [%s]", name);
			}

			answer.name = name;
			answer.rank = Integer.parseInt(rank);
			answers.add(answer);
			done.add(i);
		}

		// check for no answers
		if (answers.size() == 0) {
			Validation.addError("app.answers", "formsapp.error.noAnswers");
		}

		super.bind(request);
	}

	@Override
	public void _save() {
		// retain answers
		if (isPersistent()) {
			List<PollAppAnswer> answers = PollAppAnswer.find("app = ?", this).fetch();
			for (PollAppAnswer answer : answers) {
				if (!this.answers.contains(answer)) {
					Logger.debug("deleting answer [%s]", answer.name);
					answer.delete();
				}
			}

			// make sure transient answers are saved
			for (PollAppAnswer answer : this.answers) {
				answer.save();
			}
		}

		super._save();
	}

	@Override
	public String getAppKey() {
		return PollAppDescriptor.KEY;
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		SharePollPost post = new SharePollPost();
		post.app = this;
		post.wall = wall;
		post.message = message;
		post.author = user;
		post.save();
		return post;
	}

	@Override
	public String getDisplayName() {
		return question;
	}

	@Override
	public boolean isSharable() {
		return sender.isSharable();
	}

	public boolean hasAnswered(User user) {
		return PollAppAnswer.count("e.app = ? AND ? MEMBER OF e.users", this, user) > 0;
	}
	
	public boolean canVote(User user) {
		return CheckCaller.check(AppsPermission.POLL_VOTE, this);
	}

	@Override
	public void _delete() {
		// delete posts that reference this poll
		List<SharePollPost> posts = SharePollPost.find("app = ?", this).fetch();
		for (SharePollPost post : posts) {
			post.delete();
		}

		super._delete();
	}

	@Field
	public String getRelatedSearchInfo() {
		String r = "";
		for (PollAppAnswer answer : answers) {
			r += answer.name + " ";
		}
		return r;
	}
}

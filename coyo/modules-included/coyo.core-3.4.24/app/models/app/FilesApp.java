package models.app;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.User;
import models.app.files.File;
import models.wall.post.ShareFilePost;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_files")
public abstract class FilesApp extends App {

	public boolean anonymous;
	public boolean adminsOnly;

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<FilesAppPublicLink> publicLinks = new ArrayList<FilesAppPublicLink>();

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<ShareFilePost> posts = new ArrayList<ShareFilePost>();

	abstract public File getRoot();

	abstract public File getFile(String uid);

	/**
	 * Ordered by modified date.
	 */
	abstract public List<File> getLatestFiles(int limit);

	abstract public File createFile(User user, File parent, String name, InputStream in, String contentType,
			Long contentLength);

	abstract public File createFolder(User user, File parent, String name);

	public boolean isReadOnly() {
		return false;
	}

	public boolean hasPublicLink(File file) {
		return FilesAppPublicLink.count("app = ? AND fileUid = ?", this, file.getUid()) > 0L;
	}

	/**
	 * An exception that can display an error message to the user.
	 */
	public static class FilesAppException extends RuntimeException {

		public String displayMessage;

		public FilesAppException(String displayMessage, String logMessage) {
			super(logMessage);
			this.displayMessage = displayMessage;
		}

		public FilesAppException(String displayMessage, String message, Throwable t) {
			super(message, t);
			this.displayMessage = displayMessage;
		}
	}

	public List<File> getLatestFiles(final User user, int limit) {
		List<File> files = new ArrayList<File>();
		files.addAll(getLatestFiles(limit));

		// order
		Collections.sort(files, new Comparator<File>() {
			public int compare(File o1, File o2) {
				return o2.getModified(user).compareTo(o1.getModified(user));
			}
		});

		if (files.size() > limit) {
			return files.subList(0, limit);
		}
		return files;
	}
}

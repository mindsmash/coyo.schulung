package models.app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fishr.Indexed;
import fishr.search.Searchable;
import models.app.forum.ForumAppTopic;
import models.wall.Wall;
import apps.ForumAppDescriptor;

@Entity
@Indexed
@Searchable(category = "apps")
@Table(name = "app_forum")
public class ForumApp extends App {

	public static final String FORUM_SEARCH_CATEGORY = "forum";

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "app")
	public List<ForumAppTopic> topics = new ArrayList<ForumAppTopic>();

	@Override
	public String getAppKey() {
		return ForumAppDescriptor.KEY;
	}

	public static ForumApp findByTopic(Wall topic) {
		return ForumApp.find("SELECT e FROM ForumApp e WHERE ? MEMBER OF e.topics", topic).first();
	}

}

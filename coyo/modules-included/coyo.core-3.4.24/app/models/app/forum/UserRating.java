package models.app.forum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.User;
import models.wall.post.Post;

@Entity
@Table(name = UserRating.RATING_TABLENAME)
public class UserRating extends TenantModel {

	public static final String RATING_TABLENAME = "app_forum_rating";

	public enum Rating {
		UP, DOWN
	}

	@Enumerated(EnumType.STRING)
	public Rating rating;

	@ManyToOne(optional = false)
	public User user;

	@ManyToOne(optional = false)
	public Post post;

	public UserRating(Post post, User user, Rating rating) {
		super();
		this.rating = rating;
		this.user = user;
		this.post = post;
	}
}

package models.app.forum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.app.App;
import models.app.ForumApp;
import models.wall.ExcludeFromWall;
import models.wall.post.Post;
import controllers.WebBaseController;
import fishr.Indexed;
import fishr.Related;
import fishr.search.Searchable;

@Entity
@Indexed
@ExcludeFromWall
@Searchable(category = ForumApp.FORUM_SEARCH_CATEGORY)
@Table(name = ForumPost.POST_TABLENAME)
public class ForumPost extends Post {

	public static final String POST_TABLENAME = "post_type_forum";

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
	public List<UserRating> rating = new ArrayList<UserRating>();

	@Override
	public void redirectToResult() {
		App app = ForumApp.findByTopic(wall);
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("postId", id);
		WebBaseController.redirectToSender(app.sender, app.id, args);
	}
	
	public ForumAppTopic getTopic() {
		if (wall instanceof ForumAppTopic) {
			return (ForumAppTopic) wall;
		}
		return null;
	}

	@Related
	public String getRelatedSearchInfo() {
		return getTopic().title;
	}
}

package models.app.forum;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import jobs.PublishHashtagUsageJob;
import models.ExtendedSearchableModel;
import models.Sender;
import models.User;
import models.app.ForumApp;
import models.notification.ForumTopicNotification;
import models.wall.Wall;
import models.wall.post.Post;
import multitenancy.MTA;
import play.data.validation.MaxSize;
import play.i18n.Messages;
import play.templates.JavaExtensions;
import session.UserLoader;
import util.ModelUtil;
import utils.ModifiedDateComparator;
import acl.Permission;
import controllers.Security;
import controllers.WebBaseController;
import edu.emory.mathcs.backport.java.util.Collections;
import fishr.Field;
import fishr.Indexed;
import fishr.Related;
import fishr.util.ConvertionUtils;
import fishr.util.HashTagged;

@Entity
@Indexed
@Table(name = ForumAppTopic.TOPIC_TABLENAME)
public class ForumAppTopic extends Wall implements HashTagged, ExtendedSearchableModel {

	public static final String TOPIC_TABLENAME = "app_forum_topic";

	@ManyToOne(optional = false)
	public ForumApp app;

	@ManyToOne
	public User author;

	@Field
	@MaxSize(255)
	public String title;

	public boolean closed = false;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "topic")
	public List<ForumTopicNotification> notifications = new ArrayList<ForumTopicNotification>();

	public ForumAppTopic(ForumApp app, User author, String title, String content) {
		super(app.sender, true);
		this.app = app;
		this.title = title;
		this.author = author;
		this.posts = new ArrayList<Post>();

		// create first post
		Post post = new ForumPost();
		post.message = content;
		post.author = author;
		post.wall = this;
		post.important = true;
		this.posts.add(post);
	}

	/**
	 * We need to override Wall#getPosts here, since we need the posts in their original order (in which they where
	 * added to the list) and not in chronological descending order (like on a wall).
	 */
	public List<Post> getPosts() {
		if (isPersistent()) {
			return Post.find("wall = ? ORDER BY modified ASC", this).fetch();
		}
		Collections.sort(posts, new ModifiedDateComparator(false));
		return posts;
	}

	@Related
	public String getRelatedSearchInfo() {
		return app.getRelatedSearchInfo();
	}

	// Interface implementations
	@Override
	public final boolean checkSearchPermission() {
		return Security.check(Permission.ACCESS_APP, app);
	}

	public String getSlug() {
		return JavaExtensions.slugify(title);
	}

	@Override
	public void redirectToResult() {
		WebBaseController.redirectToSender(sender, id);
	}

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public Sender getSearchThumbSender() {
		return sender;
	}

	@Override
	public String getSearchInfoText() {
		return Messages.get("inName", sender.getDisplayName());
	}

	@Override
	public Set<String> getHashTags() {
		return ConvertionUtils.extractHashtags(this);
	}

	@Override
	protected void afterInsert() throws Exception {
		final List<String> hashtags = new ArrayList<>(this.getHashTags());

		if (hashtags != null && !hashtags.isEmpty()) {
			new PublishHashtagUsageJob(hashtags, MTA.getActiveTenant(), ModelUtil.serialize(this),
					UserLoader.getConnectedUser(false)).in(2);
		}

		super.afterInsert();
	}
}
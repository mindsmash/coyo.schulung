package models.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.ExtendedTenantModel;
import models.Sender;
import models.Settings;
import models.notification.AppNotification;
import models.workspace.Workspace;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.Http.Request;
import play.templates.JavaExtensions;
import acl.Permission;
import apps.AppDescriptor;
import apps.AppManager;
import conf.ApplicationSettings;
import controllers.Security;
import controllers.WebBaseController;
import fishr.Field;
import fishr.Indexed;
import fishr.Related;
import fishr.search.Searchable;

/**
 * <p>
 * Each app model must extend this class. Own properties and methods can be defined. To find out more about the Play
 * Framework ORM please visit http://www.playframework.org.
 * </p>
 * 
 * <p>
 * In addition to the model class each app consists of two basic templates. One for rendering the form and one for
 * rendering the app content itself. The names of those templates are defined by convention as follows:<br />
 * Form template: {VIEWPATH}/Apps/_form.{APP-KEY-LOWERCASE}.html<br />
 * Display template: {VIEWPATH}/Apps/_render.{APP-KEY-LOWERCASE}.html
 * </p>
 * 
 * <p>
 * If you require access from your javascript code to certain reverse routes of your app, you may define an additional
 * template at the following location for holding your javascript reverse routes:<br>
 * Form template: {VIEWPATH}/Apps/_routes.{APP-KEY-LOWERCASE}.html<br>
 * <br>
 * These routes will be available in the global JS scope through the variable 'ROUTES'.
 * </p>
 * 
 * @author mindsmash GmbH
 */
@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class App extends ExtendedTenantModel {

	@Field
	@ManyToOne(optional = false)
	public Sender sender;

	@Field
	@MaxSize(50)
	@Required
	public String title = "App"; // TODO : i18n - messages do not work

	public int priority = 9999;
	public boolean active = true;

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	public List<AppNotification> notifications = new ArrayList<>();

	/**
	 * The widgets that other apps are using from this app.
	 */
	@OneToMany(mappedBy = "sourceApp", cascade = CascadeType.ALL)
	public List<WidgetMapping> sourcedWidgets = new ArrayList<>();

	/**
	 * The widgets that have been added to this app.
	 */
	@OrderBy("priority ASC")
	@OneToMany(mappedBy = "targetApp", cascade = CascadeType.ALL)
	public List<WidgetMapping> widgets = new ArrayList<>();

	/**
	 * Can be used to store and persist any kind of data related to an app.
	 */
	@Field
	@ElementCollection
	@CollectionTable(name = "app_property")
	@Column(length = 2000)
	public Map<String, String> properties = new HashMap<String, String>();

	/**
	 * @return The unique key of your app. Can be hard coded or mapped to a constant in your app descriptor class.
	 */
	public abstract String getAppKey();

	public final AppDescriptor getAppDescriptor() {
		return AppManager.getApp(getAppKey());
	}

	/**
	 * If you need to bind custom data from your app's form to the model during create or edit operations you can
	 * overwrite the {@link #bind(Request)} method and extract the data from the {@link Request} object via
	 * {@link Request#params}.
	 * 
	 * @param request
	 *            The request from the form submission.
	 */
	public void bind(Request request) {
	}

	/**
	 * Can be used to execute various code prior to rendering the app's form.
	 * 
	 * @param request
	 *            The request of the form request.
	 */
	public void beforeForm(Request request) {
	}

	/**
	 * Can be used to execute various code prior to rendering an app.
	 * 
	 * @param request
	 *            The request of the render request.
	 */
	public void beforeRender(Request request) {
	}

	@Override
	protected final void beforeInsert() throws Exception {
		App last = App.find("sender = ? ORDER BY priority DESC", sender).first();
		if (last != null) {
			priority = last.priority + 1;
		}
		super.beforeInsert();
	}

	public boolean isActive() {
		// active flag
		boolean r = active;

		// app is active in general
		r &= AppManager.isActive(getAppDescriptor(), sender.getSenderType());

		// workspaces are active if app is in workspace
		r &= (!(sender instanceof Workspace) || Settings.findApplicationSettings().getBoolean(
				ApplicationSettings.WORKSPACES));

		return r;
	}

	@Override
	public final boolean checkSearchPermission() {
		return Security.check(Permission.ACCESS_APP, this);
	}

	public String getSlug() {
		return JavaExtensions.slugify(title);
	}

	@Override
	public void redirectToResult() {
		WebBaseController.redirectToSender(sender, id);
	}

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public Sender getSearchThumbSender() {
		return sender;
	}

	@Override
	public String getSearchInfoText() {
		return Messages.get("inName", sender.getDisplayName());
	}

	@Related
	public String getRelatedSearchInfo() {
		return sender != null ? sender.getDisplayName() : "";
	}

	public String getURL() {
		return sender.getURL(id);
	}
}

package models.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import models.BaseModel;
import models.ExtendedSearchableModel;
import models.ExtendedTenantModel;
import models.Sender;
import models.Sharable;
import models.User;
import models.comments.Comment;
import models.comments.Commentable;
import models.notification.WikiCommentNotification;
import models.notification.WikiNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareWikiArticlePost;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import play.cache.Cache;
import play.data.binding.NoBinding;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.JPA;
import security.CheckCaller;
import acl.AppsPermission;
import acl.Permission;
import checks.WikiArticleParentCheck;
import controllers.Security;
import edu.emory.mathcs.backport.java.util.Collections;
import fishr.Field;
import fishr.Indexed;
import fishr.Related;
import fishr.search.Searchable;

@Entity
@Indexed(boost = 2F)
@Searchable(category = "wikiArticles")
@Table(name = WikiAppArticle.WIKI_ARTICLE_TABLE)
public class WikiAppArticle extends ExtendedTenantModel implements Commentable, ExtendedSearchableModel, Sharable {

	public static final String SORT_CREATED = "created";
	public static final String SORT_NUM = "num";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";

	public static final String WIKI_ARTICLE_TABLE = "app_wiki_article";
	public static final String WIKI_COMMENT_TABLE = "app_wiki_article_comment";

	@Field
	@ManyToOne(optional = false)
	public WikiApp app;

	@CheckWith(WikiArticleParentCheck.class)
	@ManyToOne
	public WikiAppArticle parent;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
	@OrderBy("priority, title ASC ")
	@LazyCollection(LazyCollectionOption.EXTRA)
	public List<WikiAppArticle> children = new ArrayList<WikiAppArticle>();

	@OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
	public List<WikiAppArticleVersion> versions = new ArrayList<WikiAppArticleVersion>();

	@Field
	@Required
	@MaxSize(50)
	public String title;

	@Field
	@Required
	public Boolean showSubArticles = true;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "wikiArticle")
	public List<WikiNotification> notifications = new ArrayList<WikiNotification>();

	@OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
	public List<ShareWikiArticlePost> posts = new ArrayList<ShareWikiArticlePost>();

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = WIKI_COMMENT_TABLE)
	public List<Comment> comments = new ArrayList<Comment>();

	@ManyToOne
	@NoBinding
	public User lock;

	@NoBinding
	public boolean home;

	// order
	public int priority = 99999;

	public boolean isLocked() {
		// max 30 minutes
		return lock != null && modified.after(DateUtils.addMinutes(new Date(), -30));
	}

	public boolean isLocked(User user) {
		return isLocked() && lock != user;
	}

	public WikiAppArticleVersion getCurrentVersion() {
		if (!isPersistent()) {
			return null;
		}

		WikiAppArticleVersion latest = null;
		for (WikiAppArticleVersion version : versions) {
			if (latest != null && version.num > latest.num) {
				latest = version;
			} else {
				latest = version;
			}
		}

		return latest;
	}

	public List<WikiAppArticleVersion> getVersions(int limit) {
		final TypedQuery<WikiAppArticleVersion> q = em().createQuery(
				"SELECT v FROM WikiAppArticleVersion v " + "WHERE v.article = :article " + "ORDER BY v.num DESC ",
				WikiAppArticleVersion.class);
		q.setParameter("article", this);
		q.setMaxResults(limit);

		return q.getResultList();
	}

	public WikiAppArticleVersion getVersion(long version) {
		return WikiAppArticleVersion.find("article = ? AND num = ?", this, version).first();
	}

	public long getVersionCount() {
		return WikiAppArticleVersion.count("article = ?", this);
	}

	public void addVersion(String text, User author) {
		WikiAppArticleVersion version = new WikiAppArticleVersion();
		version.article = this;
		version.author = author;
		version.text = text;
		version.save();
		this.versions.add(version);
		this.save();
		refresh();
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.ACCESS_APP, app);
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareWikiArticlePost post = new ShareWikiArticlePost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.article = this;
		post.save();
		return post;
	}

	@Override
	public String getDisplayName() {
		return title;
	}

	@Override
	public boolean isSharable() {
		return app.sender.isSharable();
	}

	public String getCleanTeaserText() {
		WikiAppArticleVersion waav = getCurrentVersion();
		return Jsoup.clean((waav == null) ? "" : waav.text, Whitelist.none());
	}

	public boolean isEmpty() {
		WikiAppArticleVersion waav = getCurrentVersion();
		return (waav == null) || (versions.size() == 0) || StringUtils.isEmpty(waav.text);
	}

	public boolean isParent(WikiAppArticle article) {
		return parent != null && (parent == article || parent.isParent(article));
	}

	public boolean isChild(WikiAppArticle article) {
		return article.isParent(this);
	}

	@Override
	protected void afterSave() throws Exception {
		// clear cache
		Cache.delete("wiki-nav-" + app.id);

		super.afterSave();
	}

	public List<WikiAppArticle> getBreadcrumbs() {
		List<WikiAppArticle> r = new ArrayList<WikiAppArticle>();
		WikiAppArticle current = parent;
		while (current != null) {
			r.add(current);
			current = current.parent;
		}
		Collections.reverse(r);
		return r;
	}

	@Override
	public void _save() {
		super._save();

		// make sure only one article is the home article
		if (home) {
			JPA.em().createQuery("UPDATE WikiAppArticle a SET a.home = false WHERE a.id != ? AND a.app = ?")
					.setParameter(1, id).setParameter(2, app).executeUpdate();
		}
	}

	public void delete(boolean retainChildren) {
		if (retainChildren) {
			// move childs one level up
			for (WikiAppArticle child : children) {
				child.parent = parent;
				child.save();
			}
			refresh();
		}
		delete();
	}

	@Override
	public void _delete() {
		// delete all children recursively
		for (WikiAppArticle child : children) {
			child.delete();
		}
		children.clear();
		super._delete();
	}

	@Override
	public void redirectToResult() {
		controllers.WikiApp.article(id);
	}

	@Override
	public Sender getSearchThumbSender() {
		return app.sender;
	}

	@Override
	public String getSearchInfoText() {
		return app.getSearchInfoText();
	}

	public List<WikiAppArticle> getChildrenSorted(String sortBy, String sortOrder) {
		// order by WikiArticleVersion .created or .num
		if (StringUtils.isEmpty(sortBy) || sortBy.equals(SORT_CREATED)) {
			sortBy = SORT_CREATED;
		} else {
			sortBy = SORT_NUM;
		}

		// sort ASC or DESC
		if (StringUtils.isEmpty(sortOrder) || sortOrder.equals(ASC)) {
			sortOrder = ASC;
		} else {
			sortOrder = DESC;
		}

		final TypedQuery<WikiAppArticle> q = em().createQuery(
				String.format("SELECT DISTINCT a FROM WikiAppArticle a " + "LEFT OUTER JOIN a.versions v "
						+ "WHERE a.parent = :article AND v.article = a " + "ORDER BY v.%s %s", sortBy, sortOrder),
				WikiAppArticle.class);
		q.setParameter("article", this);

		return q.getResultList();
	}

	public static void moveArticle(final Long id, final Long destId, final int destIndex) {
		final WikiAppArticle article = WikiAppArticle.findById(id);
		if (article == null) {
			throw new IllegalArgumentException("Failed to find WikiArticle: " + id);
		}

		if (!Security.check(AppsPermission.EDIT_WIKI, article.app.id)) {
			throw new IllegalAccessError("Missing permissions to edit wiki article");
		}

		final WikiAppArticle destParent;
		if (destId != null) {
			destParent = WikiAppArticle.findById(destId);
		} else {
			destParent = null;
		}

		// if (destParent == null) {
		// throw new IllegalArgumentException("Failed to find WikiArticle: " + id);
		// }

		if (destParent != null) {
			final List<WikiAppArticle> children = destParent.children;
			children.remove(article);

			// re-priorize old list
			int priority = 0;
			for (int i = 0; i < children.size(); i++) {
				if (i == destIndex) {
					priority += 1;
				}

				final WikiAppArticle appArticle = children.get(i);
				appArticle.priority = priority;
				appArticle.save();

				priority += 1;
			}
			children.add(article);
			destParent.save();

		} else {
			final List<WikiAppArticle> roots = article.app.getRootArticles();
			roots.remove(article);

			// re-priorize old list
			int priority = 0;
			for (int i = 0; i < roots.size(); i++) {
				if (i == destIndex) {
					priority += 1;
				}

				final WikiAppArticle appArticle = roots.get(i);
				appArticle.priority = priority;
				appArticle.save();

				priority += 1;
			}
			roots.add(article);
		}

		// remove article from old parent
		if (article.parent != null) {
			final WikiAppArticle parentOld = article.parent;
			parentOld.children.remove(article);
			parentOld.save();
		}

		article.priority = destIndex;
		article.parent = destParent;
		article.save();

	}

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();

		WikiCommentNotification.raise(this, comment);
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		WikiCommentNotification.cleanup(this);
		save();
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return CheckCaller.check(AppsPermission.COMMENT_WIKI_ARTICLE, this);
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return CheckCaller.check(AppsPermission.DELETE_COMMENT_WIKI_ARTICLE, this);
	}

	@Related
	public String getRelatedSearchInfo() {
		return app.getDisplayName() + " " + app.getRelatedSearchInfo();
	}

	@Field
	public List<String> getRelatedSearchComments() {
		return Comment.getSearchInfo(comments);
	}

	@Field
	public String getRelatedSearchLastVersion() {
		WikiAppArticleVersion currentVersion = getCurrentVersion();
		if (currentVersion != null) {
			return currentVersion.getCleanText();
		}
		return null;
	}

	@Override
	public Sender getSender() {
		return this.app.sender;
	}
}

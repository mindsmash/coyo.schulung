package models.app;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.Sender;
import models.media.MediaRoom;
import play.mvc.Http.Request;
import apps.MediaAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_media")
public class MediaApp extends App {

	@OneToOne(cascade = CascadeType.ALL)
	public MediaRoom room = new MediaRoom(sender);

	public void setSender(Sender sender) {
		this.sender = sender;
		this.room.sender = sender;
	}

	@Override
	public void bind(Request request) {
		room.allowGuestUpload = request.params.get("allowGuestUpload", Boolean.class);
	}

	@Override
	public String getAppKey() {
		return MediaAppDescriptor.KEY;
	}
}

package models;

import storage.FlexibleBlob;

/**
 * Any class can be a previewable by implementing this interface. You can then
 * use the previewlable tag to dynamically generate an inline preview.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 * 
 */
public interface Previewable {

	Long getParentSenderId();

	String getDisplayName();

	String getFileName();

	FlexibleBlob getFileData();

	String getContentType();

	String getDownloadUrl();

	String getUniqueId();
}

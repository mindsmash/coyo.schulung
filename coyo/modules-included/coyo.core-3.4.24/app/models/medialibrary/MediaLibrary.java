package models.medialibrary;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.Sender;
import models.TenantModel;
import play.Logger;
import storage.FlexibleBlob;
import utils.ImageUtils;

/**
 * The media library is a simple storage room for media files. It's main purpose
 * is to serve as the media browser for CKEditor but it's generic and you can
 * use it for anything that is related to a sender.
 * 
 * @author Jan Marquardt
 */
@Entity
@Table(name = "medialibrary")
public class MediaLibrary extends TenantModel {

	@OneToMany(mappedBy = "library", cascade = CascadeType.ALL)
	public List<MediaFile> files = new ArrayList<MediaFile>();

	public MediaFile storeMedia(String name, FlexibleBlob blob) {
		MediaFile file = new MediaFile();
		file.name = name;
		file.library = this;
		file.data = blob;
		file.save();
		return file;
	}
	
	public Long getSenderId() {
		Sender sender = Sender.find("mediaLibrary = ?", this).first();
		if (sender == null) {
			Logger.warn("cannot find sender for media library [%s]", id);
			return null;
		}
		return sender.id;
	}
}

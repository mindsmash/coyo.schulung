package models.medialibrary;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import play.data.validation.Max;
import play.data.validation.MaxSize;
import storage.FlexibleBlob;

/**
 * 
 * Stores a File.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "medialibrary_file")
public class MediaFile extends TenantModel {

	@MaxSize(255)

	public String name;
	public FlexibleBlob data = new FlexibleBlob();

	@ManyToOne(optional = false)
	public MediaLibrary library;

	@Override
	protected void afterDelete() throws Exception {
		super.afterDelete();

		// delete attachments
		data.delete();
	}
}

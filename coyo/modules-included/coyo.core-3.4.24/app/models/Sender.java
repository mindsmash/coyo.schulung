package models;

import acl.Permission;
import apps.AppManager;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import controllers.Security;
import controllers.WebBaseController;
import fishr.Field;
import jobs.NotificationDistributor;
import models.app.App;
import models.bookmark.Bookmark;
import models.container.Container;
import models.helper.ClearManyToMany;
import models.helper.ClearManyToManyHelper;
import models.media.MediaRoom;
import models.medialibrary.MediaLibrary;
import models.notification.Notification;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareSenderPost;
import models.workspace.Workspace;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import play.Logger;
import play.Play;
import play.data.binding.NoBinding;
import play.db.jpa.JPA;
import play.mvc.Router;
import play.templates.JavaExtensions;
import play.utils.HTML;
import plugins.play.JobExecutorPlugin;
import storage.FlexibleBlob;
import theme.Theme;
import util.CacheUtil;
import util.Util;
import utils.JPAUtils;
import utils.Linkifier;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@FilterDefs({ @FilterDef(name = Sender.SOFT_DELETE, defaultCondition = "deleted <> '1'"),
		@FilterDef(name = Sender.EXTERNAL, defaultCondition = Sender.EXTERNAL + " <> '1'") })
@Filters({ @Filter(name = Sender.SOFT_DELETE), @Filter(name = Sender.EXTERNAL) })
@Table(name = "sender")
public abstract class Sender extends ExtendedTenantModel implements Sharable, ExtendedSearchableModel,
		Comparable<Sender> {

	public static final int AVATAR_WIDTH = 170;
	public static final int THUMB_WIDTH = 60;

	protected static final String TABLE_FOLLOWERS = "sender_follower";
	public static final String EXTERNAL = "external";
	public static final String SOFT_DELETE = "softDelete";

	@NoBinding
	public boolean deleted = false;

	@NoBinding
	public boolean external = false;

	public FlexibleBlob avatar = new FlexibleBlob(); // medium
	public FlexibleBlob thumbnail = new FlexibleBlob(); // thumb

	@Lob
	@Field
	public String about;

	@Field
	@ElementCollection
	@CollectionTable(name = "sender_property")
	@Column(length = 2000)
	public Map<String, String> properties = new HashMap<String, String>();

	@NoBinding
	@ManyToMany
	@JoinTable(name = TABLE_FOLLOWERS)
	@ClearManyToMany(appliesFor = User.class)
	@OrderBy("lastName, firstName")
	public Set<User> followers = new HashSet<User>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
	@OrderBy("priority, title")
	public List<App> apps = new ArrayList<App>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
	public List<ShareSenderPost> shareSenderPosts = new ArrayList<ShareSenderPost>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Wall> walls = new ArrayList<>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Container> containers = new ArrayList<>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Bookmark> bookmarks = new ArrayList<>();

	@NoBinding
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<MediaRoom> mediaRooms = new ArrayList<>();

	@NoBinding
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public MediaLibrary mediaLibrary;

	@NoBinding
	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public List<Post> posts = new ArrayList<>();

	@NoBinding
	@OneToMany(mappedBy = "sender", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public List<Translation> translations = new ArrayList<>();

	public MediaLibrary getMediaLibrary() {
		Logger.debug("loading media library for sender %s [%s]", getDisplayName(), getClass().getSimpleName());

		if (mediaLibrary == null && isPersistent()) {
			mediaLibrary = new MediaLibrary();
			mediaLibrary.save();
			mediaLibrary.refresh();
			save();

			JPAUtils.makeTransactionWritable();
		}

		Logger.debug("media library id: " + mediaLibrary.id);
		return mediaLibrary;
	}

	public App getDefaultApp() {
		for (App app : apps) {
			if (AppManager.isActive(app.getAppDescriptor(), getSenderType())
					&& Security.check(Permission.ACCESS_APP, app.id)) {
				return app;
			}
		}
		return null;
	}

	/**
	 * Return all active apps of this sender. The apps are sorted by priority and title descending.
	 */
	public List<App> getActiveApps() {
		return Lists.newArrayList(Iterables.filter(apps, new Predicate<App>() {
			@Override
			public boolean apply(App app) {
				return app.active;
			}
		}));
	}

	public String getLinkifiedAbout() {
		return Linkifier.linkify(HTML.htmlEscape(about));
	}

	public String getSlug() {
		return JavaExtensions.slugify(getDisplayName());
	}

	/**
	 * Changes the User's avatar. The old avatar file will be deleted!
	 * 
	 * @param avatar
	 *            new user avatar
	 */
	public final void setAvatar(FlexibleBlob avatar) {
		if (this.avatar != null && this.avatar.exists()) {
			this.avatar.delete();
		}
		this.avatar = avatar;
	}

	public final void setThumbnail(FlexibleBlob thumbnail) {
		if (this.thumbnail != null && this.thumbnail.exists()) {
			this.thumbnail.delete();
		}

		this.thumbnail = thumbnail;
	}

	public String getSenderType() {
		SenderType senderType = this.getClass().getAnnotation(SenderType.class);
		if (senderType == null) {
			throw new IllegalStateException("found a sender without a @SenderType. forgot to add annotation? Class: "
					+ this.getClass());
		}
		return senderType.value();
	}

	public abstract Wall getDefaultWall();

	public abstract String getDisplayName();

	public abstract List<User> getAdmins();


	public final Collection<Long> getFollowerIds() {
		return em().createQuery("SELECT u.id FROM User u WHERE ? MEMBER OF u.following").setParameter(1, this)
				.getResultList();
	}

	public final Collection<Long> getFollowerIdsIncludingExternals() {
		boolean isExternalFilterActive = ((org.hibernate.Session) JPA.em().getDelegate())
				.getEnabledFilter(Sender.EXTERNAL) != null;
		try {
			if (isExternalFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			}
			return getFollowerIds();
		} finally {
			if (isExternalFilterActive) {
				((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.EXTERNAL);
			}
		}
	}

	public boolean isFollower(User user) {
		return count("SELECT COUNT(s) FROM Sender s WHERE ? MEMBER OF s.followers AND s.id = ?", user, id) > 0;
	}

	/**
	 * This is called before this object is saved to give your implementation a chance to update the follower list in
	 * case you are using any other logic for holding a user-relation to this object (e.g. WorkspaceMember).
	 */
	protected abstract void updateFollowers();

	/**
	 * Distributes a notification to the interested users of this entity asynchronously.
	 * 
	 * @param notification
	 * @param excluded
	 */
	public Collection<Long> distributeNotification(Notification notification, List<User> excluded) {
		// include externals for workspaces
		final Collection<Long> users = (this instanceof Workspace)
				? getFollowerIdsIncludingExternals()
				: getFollowerIds();

		if (excluded != null && excluded.size() > 0) {
			for (User user : excluded) {
				users.remove(user.id);
			}
		}
		JobExecutorPlugin.addJob(new NotificationDistributor(tenant, notification.id, users));
		return users;
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareSenderPost post = new ShareSenderPost();
		post.author = user;
		post.message = message;
		post.wall = wall;
		post.sender = this;
		post.save();
		return post;
	}

	public void storeAppOrder(String apporder) {
		List<String> order = Util.parseList(apporder, ",");
		int priority = 0;
		for (String id : order) {
			if (!StringUtils.isEmpty(id)) {
				App app = App.findById(Long.valueOf(id));
				if (app != null && apps.contains(app)) {
					app.priority = priority++;
					app.save();
				}
			}
		}
	}

	@Override
	public boolean isSharable() {
		return false;
	}

	// TODO: store in DB
	public boolean isActive() {
		return true;
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.LIST_SENDER, this) && !deleted;
	}

	@Override
	protected void afterDelete() throws Exception {
		super.afterDelete();
		deleteAttachments();
	}

	protected void deleteAttachments() {
		// delete attachments
		thumbnail.delete();
		avatar.delete();
	}

	public String getAvatarURL() {
		if (avatar != null && avatar.exists()) {
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("senderId", id);
			args.put("version", modified.getTime());
			return Router.reverse("Resource.senderAvatar", args).url;
		}
		return Theme.getResourceUrl("images/" + getSenderType() + "-nopic.png");
	}

	public String getThumbURL() {
		if (thumbnail != null && thumbnail.exists()) {
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("senderId", id);
			args.put("version", modified.getTime());
			return Router.reverse("Resource.senderThumb", args).url;
		}
		return Theme.getResourceUrl("images/" + getSenderType() + "-nothumb.png");
	}

	public String getURL(final Long appId) {
		return Router.reverse("Senders.show", ImmutableMap.<String, Object> of("id", this.id, "appId", appId)).url;
	}

	public long getFollowerCount() {
		return User.count("FROM User u WHERE ? MEMBER OF u.following AND u.active = true", this);
	}

	public long getAdminCount() {
		final List<User> admins = new ArrayList<>();
		for (User admin : getAdmins()) {
			if (!admin.deleted && admin.active) {
				admins.add(admin);
			}
		}
		return admins.size();
	}

	@Override
	public int compareTo(Sender o) {
		return getDisplayName().compareTo(o.getDisplayName());
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear all single-sided many-to-many relations
		ClearManyToManyHelper.clearAll(this);

		super.beforeDelete();
	}

	@Override
	protected void beforeSave() throws Exception {
		CacheUtil.clear(this);
		super.beforeSave();
	}

	@Override
	public void _save() {
		if (Logger.isDebugEnabled()) {
			Logger.debug("[Sender] updating followers for: %s", this);
		}
		updateFollowers();
		if (Logger.isDebugEnabled()) {
			Logger.debug("[Sender] followers of %s are: %s", this, followers);
		}

		super._save();
	}

	@Override
	public void _delete() {
		for (App app : apps) {
			app._delete();
		}
		apps.clear();

		super._delete();
	}

	@Override
	public void redirectToResult() {
		WebBaseController.redirectToSender(this, null);
	}

	@Override
	public Sender getSearchThumbSender() {
		return this;
	}

	@Override
	public String getSearchInfoText() {
		return about;
	}

	/**
	 * @return All available sender types.
	 */
	public static List<String> getSenderTypes() {
		List<String> r = new ArrayList<>();
		for (Class clazz : Play.classloader.getAnnotatedClasses(SenderType.class)) {
			SenderType senderType = (SenderType) clazz.getAnnotation(SenderType.class);
			r.add(senderType.value());
		}
		return r;
	}

	/**
	 * Tells if sender can carry apps e.g. workspace, page...
	 * 
	 * @return
	 */
	public boolean canHaveApps() {
		return false;
	}

	/**
	 * This icon is used to indicate the visibility of the sender (e.g. closed workspace).
	 * 
	 * @return Icon css class or null if no icon should be shown
	 */
	public String getVisibilityIcon() {
		return null;
	}

	/**
	 * This text is used to indicate the visibility of the sender (e.g. "Closed workspace"). The text is usually shown
	 * as the tooltip of the {@link #getVisibilityIcon()}.
	 * 
	 * @return Text or null if no text should be shown.
	 */
	public String getVisibilityText() {
		return null;
	}
}

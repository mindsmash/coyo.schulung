package models.statistics;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jobs.StatisticsPersisterJob;
import models.Settings;
import models.TenantModel;
import multitenancy.MTA;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.helper.JpqlSelect;
import play.db.helper.SqlSelect;
import play.db.jpa.JPQL;
import play.templates.JavaExtensions;
import statistics.StatisticsQueueItem;
import conf.ApplicationSettings;

@Entity
@Table(name = "statistics_record")
public class TrackingRecord extends TenantModel {

	public String recordType;

	@Temporal(TemporalType.DATE)
	public Date date = new Date();

	public int count = 0;

	public String discriminator;

	public static void trackOne(String trackType) {
		trackOne(trackType, null);
	}

	public static synchronized void trackOne(String trackType, String discriminator) {
		track(trackType, discriminator, 1, true);
	}

	public static void track(String trackType, int count) {
		track(trackType, null, count);
	}

	public static synchronized void track(String trackType, String discriminator, int count) {
		track(trackType, discriminator, count, false);
	}

	private static synchronized void track(String trackType, String discriminator, int count, boolean increment) {
		if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.STATISTICS)) {
			StatisticsPersisterJob.persist(new StatisticsQueueItem(MTA.getActiveTenant().id, new Date(), trackType,
					discriminator, increment, count));
		}
	}

	public static TrackingResult load(Date start, String trackType, int days) {
		final TrackingResult r;
		int total = 0;

		Map<Date, Long> data = new LinkedHashMap<>();

		// load data from DB
		for (int i = 0; i < days; i++) {
			Date d = DateUtils.addDays(start, i);
			SqlSelect select = getBaseSelect(trackType, d);
			select.select("SUM(r.count) AS total");

			Query query = em().createQuery(select.toString(), Long.class);
			JPQL.instance.bindParameters(query, select.getParams().toArray());

			try {
				Long sum = (Long) query.getSingleResult();
				if (sum == null) {
					sum = 0L;
				}
				total += sum;
				data.put(d, sum);
			} catch (NoResultException ignored) {
				data.put(d, 0L);
			}
		}

		r = new TrackingResult(total, data);

		Logger.debug("[Statistics] loaded tracking results for [%s], %s days starting from %s. total = %s", trackType,
				days, start, total);

		return r;
	}

	public static JpqlSelect getBaseSelect(String trackType, Date date) {
		JpqlSelect select = new JpqlSelect();
		select.from("TrackingRecord r");
		select.where("recordType = ?").param(trackType);
		DateTime dt = new DateTime(date);
		select.andWhere("DAY(date) = ?").andWhere("MONTH(date) = ?").andWhere("YEAR(date) = ?");
		select.param(dt.getDayOfMonth()).param(dt.getMonthOfYear()).param(dt.getYear());
		return select;
	}

}

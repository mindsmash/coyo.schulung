package models.statistics;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TrackingResult implements Serializable {

	public Integer total = 0;
	public Map<Date, Long> data = new HashMap<Date, Long>();

	public TrackingResult(Integer total, Map<Date, Long> data) {
		this.total = total;
		this.data = data;
	}
}

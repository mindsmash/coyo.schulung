package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.app.App;
import models.workspace.Workspace;

import org.apache.commons.lang.StringUtils;

import apps.TasksAppDescriptor;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "apps")
@Table(name = "app_tasks")
public class TasksApp extends App {

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	@OrderBy("priority")
	public List<TasksAppList> lists = new ArrayList<TasksAppList>();

	@OneToMany(mappedBy = "app", cascade = CascadeType.ALL)
	@OrderBy("priority")
	public List<TasksAppTask> tasks = new ArrayList<TasksAppTask>();

	public List<TasksAppTask> getInbox() {
		return TasksAppTask.find("app = ? AND list IS NULL", this).fetch();
	}

	public List<TasksAppTask> getInbox(boolean completed) {
		return TasksAppTask.find("app = ? AND list IS NULL AND completed = ? ORDER BY priority ASC", this, completed)
				.fetch();
	}

	public TasksAppList getList(String id) {
		if (!StringUtils.isNumeric(id)) {
			return null;
		}
		return TasksAppList.find("app = ? AND id = ?", this, Long.valueOf(id)).first();
	}

	public TasksAppTask getTask(String id) {
		if (!StringUtils.isNumeric(id)) {
			return null;
		}
		return TasksAppTask.find("app = ? AND id = ?", this, Long.valueOf(id)).first();
	}

	public List<User> getUsers() {
		if (sender instanceof Workspace) {
			return ((Workspace) sender).getUserMembers();
		}
		return new ArrayList<User>();
	}

	@Override
	public String getAppKey() {
		return TasksAppDescriptor.KEY;
	}
}

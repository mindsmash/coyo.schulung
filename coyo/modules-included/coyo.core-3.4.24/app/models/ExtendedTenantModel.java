package models;

import java.util.Collection;
import java.util.Set;

import javax.persistence.MappedSuperclass;

import jobs.PublishHashtagUsageJob;
import multitenancy.MTA;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import session.UserLoader;
import util.ModelUtil;
import utils.ErrorHelper;
import fishr.Indexed;
import fishr.util.ConvertionUtils;
import fishr.util.HashTagged;

/**
 * Extends the {@link TenantModel} with application specific methods (e.g. tagging).
 *
 * @author Gerrit Dongus, mindsmash GmbH
 */
@MappedSuperclass
public abstract class ExtendedTenantModel extends TenantModel implements HashTagged, ExtendedSearchableModel {

	private static final Logger LOG = LoggerFactory.getLogger(ExtendedTenantModel.class);

	/**
	 * Default implementation extracts the hashtags from all indexed fields of the current object.
	 */
	@Override
	public Set<String> getHashTags() {
		return ConvertionUtils.extractHashtags(this);
	}

	@Override
	protected void afterInsert() throws Exception {
		try {
			if (this.getClass().getAnnotation(Indexed.class) != null) {
				raiseHashtagNotifications(getHashTags());
			}
		} catch (Exception e) {
			ErrorHelper.handleWarning(LOG, e,
					"[HashtagNotification] Could not raise notification for hashtags (saving %s)", this);
		}

		super.afterInsert();
	}

	/**
	 * Raises notifications for the given hashtags and associates those notifications to this object.
	 * 
	 * @param hashtags
	 */
	public void raiseHashtagNotifications(Collection<String> hashtags) {
		if (hashtags != null && !hashtags.isEmpty()) {
			new PublishHashtagUsageJob(hashtags, MTA.getActiveTenant(), ModelUtil.serialize(this),
					UserLoader.getConnectedUser(false)).in(2);
		}
	}
}

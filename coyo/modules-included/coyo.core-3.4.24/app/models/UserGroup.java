package models;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import models.page.Page;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.JPA;

/**
 * 
 * Represents a group of users.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "usergroup")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class UserGroup extends TenantModel {

	public static final String GROUP_USER_TABLE = "usergroup_user";

	@Required
	@MaxSize(255)
	public String name;

	@ManyToOne
	public UserRole role;

	public boolean defaultGroup = false;

	@ManyToMany
	@JoinTable(name = GROUP_USER_TABLE)
	public Set<User> users = new LinkedHashSet<User>();

	public boolean hasGrant(String grant) {
		return role != null && role.hasGrant(grant);
	}

	public List<User> getActiveUsers() {
		List<User> r = new ArrayList<User>();
		for (User user : users) {
			if (user.active) {
				r.add(user);
			}
		}
		return r;
	}

	public void addUser(User user) {
		if (user.isPersistent()) {
			users.add(user);
			save();
		}
	}

	public long getUserCount() {
		return User.count("? MEMBER OF e.groups", this);
	}

	@Override
	public void _save() {
		super._save();

		// make sure only one group is the default
		if (defaultGroup) {
			JPA.em().createQuery("UPDATE UserGroup g SET g.defaultGroup = false WHERE g.id != ?").setParameter(1, id)
					.executeUpdate();
		}

		// notify pages that use this group for forced followers
		// TODO: do this using just IDs to avoid loading all this stuff into memory
		List<Page> pages = Page.find("SELECT p FROM Page p WHERE ? MEMBER OF p.forcedFollowerGroups OR p.forceAllFollow = true", this).fetch();
		for (Page page : pages) {
			page.save();
		}
	}

	/**
	 * @return Default group or null.
	 */
	public static UserGroup findDefaultGroup() {
		return find("defaultGroup = true").first();
	}

	public boolean isDeletable() {
		return false;
	}

	public boolean isUserManageable() {
		return false;
	}

	@Override
	@PreRemove
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + Page.MEMBER_GROUP_TABLE + " WHERE membergroups_id = " + id)
				.executeUpdate();

		JPA.em()
				.createNativeQuery(
						"DELETE FROM " + Page.FORCE_FOLLOWER_GROUP_TABLE + " WHERE forcedfollowergroups_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}
}

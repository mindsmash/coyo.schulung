package models.container;

import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import json.list.ItemSerializer;
import models.TenantModel;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Validation.ValidationResult;

@Entity
@Table(name = "container_item_val")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class ItemValue extends TenantModel {
	@ManyToOne(optional = false)
	public Item item;

	@ManyToOne(optional = false)
	public Field field;

	public ItemValue() {
	}

	public ItemValue(Field field) {
		this.field = field;
	}

	protected Boolean getBooleanPropertyElseNull(String propertyKey) {
		String value = field.getProperty(propertyKey);
		return StringUtils.isBlank(value) ? null : Boolean.valueOf(value);
	}

	protected Long getPropertyAsLongElseNull(String propertyKey) {
		String value = field.getProperty(propertyKey);
		return StringUtils.isBlank(value) ? null : Long.valueOf(value);
	}

	/**
	 * Is this value valid?
	 * 
	 * Can be used by subclasses to implement specific validation dependent on their properties.
	 */
	public List<ValidationResult> getValidationResults() {
		return Collections.EMPTY_LIST;
	}

	/**
	 * Can be overwritten to determine, which field of the returned object, should be used for displaying, filtering,
	 * and sorting. This is necessary if the field returns an object. It must not be implemented for primitive types.
	 * The default implementation returns an empty string. Please note that the specified field must be rendered by the
	 * {@link ItemSerializer} as well.
	 * 
	 * @return name of a field of the returned object.
	 */
	public String getDisplayProperty() {
		return "";
	}

	public abstract Object getValue();

	public abstract ItemValue setValue(String value);

	public abstract String getStringValue();
}

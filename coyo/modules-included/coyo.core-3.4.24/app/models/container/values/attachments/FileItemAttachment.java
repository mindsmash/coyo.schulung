package models.container.values.attachments;

import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.FileAttachment;
import models.Previewable;
import models.TenantModel;
import models.container.values.FileItemValue;
import org.slf4j.LoggerFactory;
import play.Logger;
import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.mvc.Router;
import storage.FlexibleBlob;

@Entity
@Table(name = "container_item_value_file_attachment")
public class FileItemAttachment extends TenantModel implements FileAttachment {
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(FileItemAttachment.class);

	@MaxSize(255)
	public String name;

	public FlexibleBlob file;

	@ManyToOne(optional = false)
	public FileItemValue fileItemValue;
	
	@Override
	public String getFileName() {
		return name;
	}

	@Override
	public FlexibleBlob getFileData() {
		return file;
	}

	@Override
	public String getContentType() {
		return file.type();
	}

	@Override
	public Long getParentSenderId() {
		return fileItemValue.item.getApp().sender.getId();
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public String getDownloadUrl() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", id);
		return Router.reverse("Containers.fileAttachment", args).url;
	}

	@Override
	public String getUniqueId() {
		try (InputStream is = file.get()) {
			return org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
			return "containerItemFileAttachment-" + id + name;
		}
	}
	
	@Override
	public Long getSize() {
		return file.length();
	}

	@Override
	public void _delete() {
		file.delete();
		super._delete();
	}

	@Override
	public InputStream get() {
		return file.get();
	}

	@Override
	public long length() {
		return file.length();
	}

	@Override
	public String type() {
		return file.type();
	}
}

package models.container.values;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Lob;

import models.container.Field;
import models.container.ItemValue;
import play.data.validation.Validation.ValidationResult;
import utils.ValidationHelper;

import com.google.common.collect.Lists;

@Entity
public class StringItemValue extends ItemValue {
	private static final String MIN_PROPERTY_KEY = "minLength";
	private static final String MAX_PROPERTY_KEY = "maxLength";

	@Lob
	public String stringValue;

	public StringItemValue() {
		super();
	}

	public StringItemValue(Field field) {
		super(field);
	}

	public StringItemValue(Field field, String stringValue) {
		super(field);
		this.stringValue = stringValue;
	}

	@Override
	public List<ValidationResult> getValidationResults() {
		ArrayList<ValidationResult> result = Lists.newArrayList();

		Long minProperty = getPropertyAsLongElseNull(MIN_PROPERTY_KEY);
		if (minProperty != null && stringValue.length() < minProperty) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.minSize",
					new String[] { minProperty.toString() }));
		}
		Long maxProperty = getPropertyAsLongElseNull(MAX_PROPERTY_KEY);
		if (maxProperty != null && stringValue.length() > maxProperty) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.maxSize",
					new String[] { maxProperty.toString() }));
		}

		return result;
	}

	@Override
	public Object getValue() {
		return stringValue;
	}

	@Override
	public ItemValue setValue(String value) {
		this.stringValue = value;
		return this;
	}

	@Override public String getStringValue() {
		return stringValue;
	}
}

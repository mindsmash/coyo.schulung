package models.container.values;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import models.container.Field;
import models.container.ItemValue;
import models.container.fields.OptionsFieldOption;
import models.helper.ClearManyToMany;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Entity
public class OptionsItemValue extends ItemValue {

	@ManyToMany
	@ClearManyToMany(appliesFor=OptionsFieldOption.class)
	@JoinTable(name = "container_item_value_selectedoptions", joinColumns = { @JoinColumn(name = "VALUE_ID", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "OPTION_ID", referencedColumnName = "ID") })
	public Set<OptionsFieldOption> selectedOptions = new LinkedHashSet();

	public OptionsItemValue() {
		super();
	}

	public OptionsItemValue(Field field) {
		super(field);
	}

	@Override
	public Object getValue() {
		if (!selectedOptions.isEmpty() && BooleanUtils.isNotTrue(getBooleanPropertyElseNull("multiple"))) {
			return selectedOptions.toArray()[0];
		}
		return selectedOptions;
	}

	@Override
	public ItemValue setValue(String value) {
		selectedOptions.clear();
		String[] idStrings = value.split(",");
		for (String idString : idStrings) {
			Long id = NumberUtils.createLong(idString);
			OptionsFieldOption option = OptionsFieldOption.findById(id);
			if (option != null) {
				selectedOptions.add(option);
			}
		}
		return this;
	}

	@Override public String getStringValue() {
		final StringBuffer sB = new StringBuffer();
		for (OptionsFieldOption o : selectedOptions) {
			sB.append(o.label).append(" ");
		}

		return sB.toString();
	}

	@Override
	public String getDisplayProperty() {
		return "label";
	}
}
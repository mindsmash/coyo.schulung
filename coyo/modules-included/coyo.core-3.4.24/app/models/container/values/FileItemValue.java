package models.container.values;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.collect.Sets;

import models.container.Field;
import models.container.ItemValue;
import models.container.values.attachments.FileItemAttachment;
import controllers.Uploads;
import controllers.Uploads.TempUpload;

@Entity
public class FileItemValue extends ItemValue {

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fileItemValue")
	public Set<FileItemAttachment> files = new HashSet<>();

	public FileItemValue() {
		super();
	}

	public FileItemValue(Field field) {
		super(field);
	}

	@Override
	public Object getValue() {
		return files;
	}

	/**
	 * This method receives a string with comma separated IDs. These IDs either belong to an uploaded file or an already
	 * existing {@link FileItemAttachment} model. For each newly uploaded file an FileItemAttachment model is created.
	 * Attachments that where linked before to this entity, but have not been submitted are deleted.
	 */
	@Override
	public ItemValue setValue(String value) {
		Set<FileItemAttachment> newFiles = new HashSet<>();
		String[] idStrings = value.split(",");
		for (String idString : idStrings) {
			TempUpload upload = Uploads.loadUpload(idString);
			if (upload != null) {
				FileItemAttachment fa = new FileItemAttachment();
				fa.file = upload.blob;
				fa.name = upload.filename;
				fa.fileItemValue = this;
				newFiles.add(fa);
			} else {
				Long id = NumberUtils.createLong(idString);
				FileItemAttachment attachment = FileItemAttachment.findById(id);
				if (attachment != null) {
					newFiles.add(attachment);
				}
			}
		}

		/* delete files that haven't been submitted */
		for (FileItemAttachment attachmentToDelete : Sets.difference(files, newFiles)) {
			attachmentToDelete.delete();
		}
		this.files = newFiles;

		return this;
	}

	@Override public String getStringValue() {
		final StringBuffer stringBuffer = new StringBuffer();
		for (FileItemAttachment f : files) {
			stringBuffer.append(f.getFileName()).append(" ");
		}
		return stringBuffer.toString();
	}

	@Override
	public String getDisplayProperty() {
		return "fileName";
	}
}

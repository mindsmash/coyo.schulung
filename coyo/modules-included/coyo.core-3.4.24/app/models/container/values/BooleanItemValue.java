package models.container.values;

import javax.persistence.Entity;

import models.container.Field;
import models.container.ItemValue;

import org.apache.commons.lang.BooleanUtils;

@Entity
public class BooleanItemValue extends ItemValue {
	private static final String DEFAULT_PROPERTY_KEY = "default";

	public Boolean booleanValue;

	public BooleanItemValue() {
		super();
	}

	public BooleanItemValue(Field field) {
		super(field);
		Boolean defaultValue = getBooleanPropertyElseNull(DEFAULT_PROPERTY_KEY);
		if (booleanValue == null && defaultValue != null) {
			booleanValue = defaultValue;
		}
	}

	public BooleanItemValue(Field field, Boolean booleanValue) {
		super(field);
		this.booleanValue = booleanValue;
	}

	@Override
	public Object getValue() {
		return booleanValue;
	}

	@Override
	public ItemValue setValue(String value) {
		this.booleanValue = BooleanUtils.toBooleanObject(value);
		return this;
	}

	@Override public String getStringValue() {
		return booleanValue.toString();
	}
}

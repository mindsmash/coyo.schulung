package models.container.values;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import libs.DateI18N;
import models.container.Field;
import models.container.ItemValue;
import play.data.validation.Validation.ValidationResult;
import utils.ValidationHelper;

import com.google.common.collect.Lists;

@Entity
public class DateItemValue extends ItemValue {

	@Transient
	private DateTimeFormatter format = DateTimeFormat.forPattern(DateI18N.getNormalFormat());

	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime date = new DateTime();	

	public DateItemValue() {
		super();
	}

	public DateItemValue(Field field) {
		super(field);
	}

	public DateItemValue(Field field, String date, String format) {
		super(field);
		setValue(date);
	}

	@Override
	public List<ValidationResult> getValidationResults() {
		ArrayList<ValidationResult> result = Lists.newArrayList();
		if(this.date == null) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.date.format", new String[] { DateI18N.getNormalFormat() }));
		}
		return result;
	}

	@Override
	public Object getValue() {
		return date;
	}

	@Override
	public ItemValue setValue(String value) {
		try {
			date = format.parseDateTime(value);
		} catch(UnsupportedOperationException | IllegalArgumentException e) {
			// this.date = null;
		}
		return this;
	}

	@Override
	public String getStringValue() {
		if(date != null) {
			return format.print(date);
		}
		return "";
	}
}

package models.container.values;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import models.Sender;
import models.container.Field;
import models.container.ItemValue;
import models.helper.ClearManyToMany;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;

import play.db.jpa.JPA;

@Entity
public class SenderItemValue extends ItemValue {
	
	private static final String CONTAINER_ITEM_VALUE_SENDER_TABLE = "container_item_val_sender";
	
	@ManyToMany
	@JoinTable(name = CONTAINER_ITEM_VALUE_SENDER_TABLE)
	@ClearManyToMany(appliesFor = Sender.class)
	public Set<Sender> senders = new TreeSet<>();

	public SenderItemValue() {
		super();
	}

	public SenderItemValue(Field field) {
		super(field);
	}

	@Override
	public Object getValue() {
		if (!senders.isEmpty() && BooleanUtils.isNotTrue(getBooleanPropertyElseNull("multiple"))) {
			return senders.toArray()[0];
		}
		return senders;
	}

	@Override
	public ItemValue setValue(String value) {
		senders.clear();
		String[] idStrings = value.split(",");
		for (String idString : idStrings) {
			Long id = NumberUtils.createLong(idString);
			Sender sender = Sender.findById(id);
			if (sender != null) {
				senders.add(sender);
			}
		}
		return this;
	}

	@Override public String getStringValue() {
		final StringBuffer sB = new StringBuffer();
		for (Sender s: senders) {
			sB.append(s.getDisplayName()).append(" ");
		}

		return sB.toString();
	}

	@Override
	public String getDisplayProperty() {
		return "fullName";
	}
	
	@Override
	protected void beforeDelete() throws Exception {
		JPA.em().createNativeQuery("DELETE FROM " + CONTAINER_ITEM_VALUE_SENDER_TABLE + " WHERE container_item_val_id = " + id).executeUpdate();
		super.beforeDelete();
	}
}

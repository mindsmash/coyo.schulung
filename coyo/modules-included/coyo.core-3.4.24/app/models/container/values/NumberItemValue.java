package models.container.values;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import models.container.Field;
import models.container.ItemValue;
import play.data.validation.Validation.ValidationResult;
import utils.ValidationHelper;

import com.google.common.collect.Lists;

@Entity
public class NumberItemValue extends ItemValue {
	private static final String MIN_PROPERTY_KEY = "min";
	private static final String MAX_PROPERTY_KEY = "max";

	public BigDecimal numberValue;

	public NumberItemValue() {
		super();
	}

	public NumberItemValue(Field field) {
		super(field);
	}

	public NumberItemValue(Field field, BigDecimal numberValue) {
		super(field);
		this.numberValue = numberValue;
	}

	@Override
	public List<ValidationResult> getValidationResults() {
		ArrayList<ValidationResult> result = Lists.newArrayList();

		Long minProperty = getPropertyAsLongElseNull(MIN_PROPERTY_KEY);
		if (minProperty != null && numberValue.compareTo(BigDecimal.valueOf(minProperty)) == -1) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.min",
					new String[] { minProperty.toString() }));
		}
		Long maxProperty = getPropertyAsLongElseNull(MAX_PROPERTY_KEY);
		if (maxProperty != null && numberValue.compareTo(BigDecimal.valueOf(maxProperty)) == 1) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.max",
					new String[] { maxProperty.toString() }));
		}

		return result;
	}

	@Override
	public Object getValue() {
		return numberValue;
	}

	@Override
	public ItemValue setValue(String value) {
		this.numberValue = new BigDecimal(value);
		return this;
	}

	@Override public String getStringValue() {
		return numberValue.toPlainString();
	}
}

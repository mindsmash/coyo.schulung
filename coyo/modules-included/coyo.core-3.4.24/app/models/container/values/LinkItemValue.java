package models.container.values;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.Lob;

import models.container.Field;
import models.container.ItemValue;
import play.data.validation.Validation.ValidationResult;
import utils.ValidationHelper;

import com.google.common.collect.Lists;

@Entity
public class LinkItemValue extends ItemValue {

	private static final Pattern URL_PROTOCOL_PATTERN = Pattern.compile("https?|ftp");
	
	@Lob
	public String linkValue;

	public LinkItemValue() {
		super();
	}

	public LinkItemValue(Field field) {
		super(field);
	}

	public LinkItemValue(Field field, String stringValue) {
		super(field);
		this.linkValue = stringValue;
	}

	@Override
	public List<ValidationResult> getValidationResults() {
		ArrayList<ValidationResult> result = Lists.newArrayList();
		try {
			URL url = new URL(linkValue);
			Matcher matcher = URL_PROTOCOL_PATTERN.matcher(url.getProtocol());
			if (!matcher.matches()) {
				result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.url.protocol", new String[] { url.getProtocol() }));
			}
		} catch (MalformedURLException e) {
			result.add(ValidationHelper.makeValidationResultWithError(field.id.toString(), "validation.url", new String[] { linkValue }));
		}
		return result;
	}

	@Override
	public Object getValue() {
		return linkValue;
	}

	@Override
	public ItemValue setValue(String value) {
		this.linkValue = value;
		return this;
	}

	@Override public String getStringValue() {
		return linkValue;
	}
}

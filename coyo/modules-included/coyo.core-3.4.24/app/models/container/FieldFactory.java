package models.container;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.container.fields.BooleanField;
import models.container.fields.FileField;
import models.container.fields.LinkField;
import models.container.fields.NumberField;
import models.container.fields.OptionsField;
import models.container.fields.OptionsFieldOption;
import models.container.fields.TextField;
import models.container.fields.DateField;
import models.container.fields.UserField;

import org.apache.commons.lang3.StringUtils;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;
import containers.form.FieldFormData;
import containers.form.FieldOptionsFormdata;

public final class FieldFactory {

	public static Field create(Container container, FieldFormData fieldFormData) {
		try {
			Field field = getClasz(fieldFormData.type).newInstance();
			field.container = container;
			return update(field, fieldFormData.type, fieldFormData.label, fieldFormData.required, fieldFormData.hidden,
					fieldFormData.priority, fieldFormData.properties);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static Field update(Field field, FieldFormData fieldFormData) {
		return update(field, fieldFormData.type, fieldFormData.label, fieldFormData.required, fieldFormData.hidden,
				fieldFormData.priority, fieldFormData.properties);
	}

	public static Class<? extends Field> getClasz(String fieldType) {
		switch (FieldType.valueOf(fieldType)) {
		case BOOLEAN:
			return BooleanField.class;
		case NUMBER:
			return NumberField.class;
		case OPTIONS:
			return OptionsField.class;
		case USER:
			return UserField.class;
		case TEXT:
			return TextField.class;
		case DATE:
			return DateField.class;
		case FILE:
			return FileField.class;
		case LINK:
			return LinkField.class;
		default:
			throw new IllegalArgumentException("Unsupported field type: " + fieldType);
		}
	}

	public static void updateOptions(OptionsField field, List<FieldOptionsFormdata> options) {
		if (options != null && !options.isEmpty()) {
			OptionsField optionsField = (OptionsField) field;
			Set<OptionsFieldOption> addedOptions = new HashSet();

			// create new options and update existing ones
			for (int i = 0; i < options.size(); i++) {
				FieldOptionsFormdata optionFormdata = options.get(i);
				if (optionFormdata.label.trim().length() > 0) {
					OptionsFieldOption option = getOrCreateOption(optionFormdata);
					option.field = optionsField;
					option.label = optionFormdata.label;
					option.priority = i;
					option.save();
					optionsField.options.add(option);
					addedOptions.add(option);
				}
			}

			// delete all options that haven't been submitted
			Set<OptionsFieldOption> optionsToDelete = new HashSet(optionsField.options);
			optionsToDelete.removeAll(addedOptions);
			for (OptionsFieldOption option : optionsToDelete) {
				optionsField.options.remove(option);
				option.delete();
			}
		}
	}

	private static Field update(Field field, String type, String label, Boolean required, Boolean hidden, int priority,
			Map<String, String> properties) {
		field.label = label;
		field.priority = priority;
		field.required = required;
		field.hidden = hidden;
		if (properties != null) {
			for (FieldTypeProperty fieldTypeProperty : FieldTypeDescriptor.getDescriptor(type).properties) {
				String propertyValue = properties.get(fieldTypeProperty.key);
				if (StringUtils.isNotBlank(propertyValue)) {
					field.properties.put(fieldTypeProperty.key, fieldTypeProperty.convertValue(propertyValue)
							.toString());
				} else {
					// remove property
					field.properties.remove(fieldTypeProperty.key);
				}
			}
		}
		return field;
	}

	private static OptionsFieldOption getOrCreateOption(FieldOptionsFormdata optionFormdata) {
		if (optionFormdata.id != null) {
			OptionsFieldOption op = OptionsFieldOption.findById(optionFormdata.id);
			if (op != null) {
				return op;
			}
		}
		return new OptionsFieldOption();
	}

}
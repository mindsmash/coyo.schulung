package models.container;

import containers.FieldTypeDescriptor;
import containers.form.FieldFormData;
import models.TenantModel;
import models.container.fields.OptionsField;
import org.elasticsearch.common.collect.Maps;
import org.elasticsearch.common.collect.Maps.EntryTransformer;
import play.data.validation.Required;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "container_field")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Field extends TenantModel {

	private static final String CONTAINER_FIELD_PROPERTY_TABLE = "container_field_prop";

	@ManyToOne(optional = false)
	public Container container;

	@OneToMany(mappedBy = "field", cascade = CascadeType.ALL)
	public List<ItemValue> values = new ArrayList<>();

	@ElementCollection
	@CollectionTable(name = CONTAINER_FIELD_PROPERTY_TABLE)
	@Column(length = 2000)
	public Map<String, String> properties = new HashMap<String, String>();

	// sorting
	public int priority = 9999;

	@Column(nullable = false)
	@Required
	public String label;

	public boolean required;

	public boolean hidden;

	@Lob
	public String help;

	public Field() {
		// empty
	}

	public Field(Container parent) {
		this.container = parent;
	}

	public String getProperty(String propertyKey) {
		return properties.get(propertyKey);
	}

	public abstract String getType();

	public Map<String, Object> getTypedProperties() {
		return Maps.transformEntries(properties, new EntryTransformer<String, String, Object>() {
			@Override
			public Object transformEntry(String key, String value) {
				// get FieldTypeDescriptor for key and do conversion of value
				return FieldTypeDescriptor.getDescriptor(getType()).convertPropertyValue(key, value);
			}
		});
	}

	public static Field updateOrCreateField(Container container, FieldFormData fieldFormData) {
		if (fieldFormData.id != null) {
			final Field field = Field.findById(fieldFormData.id);
			if (field != null) {
				return FieldFactory.update(field, fieldFormData);
			}
		}
		return FieldFactory.create(container, fieldFormData);
	}

	public static List<Field> createFieldListFromFromData(final List<FieldFormData> dataList,
			final Container container) {

		final List<Field> fields = new ArrayList<>();

		for (FieldFormData fieldFormData : dataList) {
			final Field field = Field.updateOrCreateField(container, fieldFormData);

			if (field instanceof OptionsField) {
				FieldFactory.updateOptions((OptionsField) field, fieldFormData.options);
			}
			field.save();
			fields.add(field);
		}

		return fields;
	}

	@Override
	protected void beforeDelete() throws Exception {
		JPA.em().createNativeQuery("DELETE FROM " + CONTAINER_FIELD_PROPERTY_TABLE + " WHERE field_id = " + id)
				.executeUpdate();
		super.beforeDelete();
	}
}

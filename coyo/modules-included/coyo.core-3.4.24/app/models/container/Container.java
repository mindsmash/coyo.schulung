package models.container;

import acl.Permission;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import controllers.Security;
import models.Sender;
import models.TenantModel;
import models.User;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.helper.JpqlSelect;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "container")
@Inheritance(strategy = InheritanceType.JOINED)
public class Container extends TenantModel {
	/**
	 * Permissions are internally mapped as a bit set with 7 bits.
	 */
	@Transient
	private BitSet permissionSet;

	@ManyToOne(optional = false)
	public Sender sender;

	@Column(nullable = false)
	@Required
	@MaxSize(255)

	public String name;

	@OneToMany(mappedBy = "container", cascade = CascadeType.ALL)
	@OrderBy("priority")
	public List<Field> fields = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "container")
	@OrderBy("modified DESC")
	public List<Item> items = new ArrayList<>();

	public Container(Sender sender) {
		this.sender = sender;
		this.setAllPermissions(true);
	}

	public Container(Sender sender, String name) {
		this(sender);
		this.name = name;
	}

	public void addField(Field field) {
		if (field == null) {
			return;
		} else if (fields.contains(field)) {
			fields.remove(field);
		}

		this.fields.add(field);
	}

	public void removeNonPresentFields(final List<Field> fields) {
		final List<Field> fieldsToDelete = new ArrayList(this.fields);
		fieldsToDelete.removeAll(fields);

		for (Field field : fieldsToDelete) {
			this.fields.remove(field);
			field.delete();
		}
		this.save();
	}


	public void addItem(Item item) {
		this.items.add(item);
	}

	public boolean allCanBeReadBy(User connectedUser) {
		return hasPermission(ContainerPermission.ReadAll);
	}

	public boolean canBeReadBy(User user) {
		return allCanBeReadBy(user) || hasPermission(ContainerPermission.ReadOwn);
	}

	public List<Item> getAllItems(final User user, final Field orderBy, final Boolean asc) {
		JpqlSelect select = new JpqlSelect();
		select.from("Item").where("container = ?").param(this);
		List<Item> items = Item.find(select.toString(), select.getParams().toArray()).fetch();
		
		/* filter items by user permissions if necessary */
		if (user != null && !allCanBeReadBy(user)) {
			Iterables.removeIf(items, new Predicate<Item>() {
				@Override
				public boolean apply(Item item) {
					return !Security.check(Permission.READ_CONTAINER_ITEM, item.id);
				}
			});
		}
		return items;
	}

	public Field getFieldById(Long fieldId) {
		for (Field field : fields) {
			if (field.id.equals(fieldId)) {
				return field;
			}
		}
		return null;
	}

	@Access(AccessType.PROPERTY)
	@Column(name = "permissions", nullable = false)
	public Long getPermissions() {
		long[] permissionArray = getPermissionSet().toLongArray();
		return permissionArray.length == 0 ? 0l : permissionArray[0];
	}

	private BitSet getPermissionSet() {
		if (permissionSet == null) {
			permissionSet = new BitSet(7);
		}
		return permissionSet;
	}

	public Collection<Field> getRequiredFields() {
		return Collections2.filter(fields, new Predicate<Field>() {
			@Override
			public boolean apply(Field field) {
				return field.required;
			}
		});
	}

	public boolean hasPermission(ContainerPermission permission) {
		return getPermissionSet().get(permission.bit);
	}

	private void setAllPermissions(boolean value) {
		ContainerPermission[] values = ContainerPermission.values();
		for (ContainerPermission containerPermission : values) {
			setPermission(containerPermission, value);
		}
	}

	public void setPermission(ContainerPermission permission, boolean value) {
		getPermissionSet().set(permission.bit, value);
	}

	public void setPermissions(Long data) {
		if (data != null) {
			permissionSet = BitSet.valueOf(new long[] { data });
		}
	}

	public <T> T getEntityUsingContainer(Class<T> clazz, String fieldName) {
		final String sql = "FROM " + clazz.getSimpleName() + " WHERE " + fieldName + " = :container";
		final TypedQuery<T> query = em().createQuery(sql, clazz);
		query.setParameter("container", this);
		try {
			return query.getSingleResult();
		} catch (NoResultException ex){
			return null;
		}
	}
}

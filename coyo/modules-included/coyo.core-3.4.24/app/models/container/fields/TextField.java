package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;

@Entity
public class TextField extends Field {
	public static final String TYPE = "TEXT";

	public TextField(Container parent) {
		super(parent);
	}

	public TextField() {
		super();
	}

	@Override
	public String getType() {
		return TYPE;
	}
}

package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;

@Entity
public class NumberField extends Field {
	public static final String TYPE = "NUMBER";

	public NumberField(Container parent) {
		super(parent);
	}

	public NumberField() {
		super();
	}

	@Override
	public String getType() {
		return TYPE;
	}
}

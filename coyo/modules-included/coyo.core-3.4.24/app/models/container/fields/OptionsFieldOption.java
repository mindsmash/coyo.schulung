package models.container.fields;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.TenantModel;
import models.container.values.OptionsItemValue;
import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.helper.JpqlSelect;

@Entity
@Table(name = "container_field_option")
public class OptionsFieldOption extends TenantModel {
	@Required
	@MaxSize(255)
	public String label;

	@Column(nullable = false)
	public Integer priority = 9999;

	@ManyToOne(optional = false)
	public OptionsField field;

	/**
	 * Before deletion we need to remove all references to this option manually.
	 */
	@Override
	protected void beforeDelete() throws Exception {
		JpqlSelect select = new JpqlSelect();
		select.select("o from OptionsItemValue o join o.selectedOptions s where s.id = ?").param(id);
		List<OptionsItemValue> optionValues = OptionsItemValue.find(select.toString(), select.getParams().toArray()).fetch();
		
		for (OptionsItemValue optionsItemValue : optionValues) {
			optionsItemValue.selectedOptions.remove(this);
			optionsItemValue.willBeSaved = true;
		}
		
		super.beforeDelete();
	}
	
}
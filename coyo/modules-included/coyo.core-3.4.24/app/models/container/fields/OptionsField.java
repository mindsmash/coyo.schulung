package models.container.fields;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import models.container.Container;
import models.container.Field;
import models.container.values.OptionsItemValue;
import play.db.helper.JpqlSelect;
import containers.FieldType;

@Entity
public class OptionsField extends Field {

	@OneToMany(mappedBy = "field", cascade = CascadeType.ALL)
	@OrderBy("priority")
	public Set<OptionsFieldOption> options = new LinkedHashSet<>();

	public OptionsField(Container parent) {
		super(parent);
	}

	public OptionsField() {
		super();
	}

	@Override
	public String getType() {
		return FieldType.OPTIONS.toString();
	}

	/**
	 * Before deletion of the option field itself, we need to delete all item values manually. Otherwise we will get an
	 * referential integrity constraint violation, since Hibernate can not resolve the items correctly.
	 */
	@Override
	public void _delete() {
		JpqlSelect select = new JpqlSelect();
		select.from("OptionsItemValue");
		select.where("field = ?").param(this);
		List<OptionsItemValue> optionValues = OptionsItemValue.find(select.toString(), select.getParams().toArray()).fetch();

		for (OptionsItemValue optionsItemValue : optionValues) {
			optionsItemValue.delete();
		}

		super._delete();
	}
}

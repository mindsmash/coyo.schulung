package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;

@Entity
public class LinkField extends Field {
	public static final String TYPE = "LINK";

	public LinkField(Container parent) {
		super(parent);
	}

	public LinkField() {
		super();
	}

	@Override
	public String getType() {
		return TYPE;
	}
}

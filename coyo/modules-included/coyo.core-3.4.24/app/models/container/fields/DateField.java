package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;

@Entity
public class DateField extends Field {
	public static final String TYPE = "DATE";

	public DateField(Container parent) {
		super(parent);
	}

	public DateField() {
		super();
	}

	@Override
	public String getType() {
		return TYPE;
	}
}

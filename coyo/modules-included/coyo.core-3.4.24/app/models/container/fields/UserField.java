package models.container.fields;

import java.util.List;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;
import models.container.values.SenderItemValue;
import play.db.helper.JpqlSelect;
import containers.FieldType;

@Entity
public class UserField extends Field {
	public UserField(Container parent) {
		super(parent);
	}

	public UserField() {
		super();
	}

	@Override
	public String getType() {
		return FieldType.USER.toString();
	}
	
	/**
	 * Before deletion of the sender field itself, we need to delete all item values manually. Otherwise we will get an
	 * referential integrity constraint violation, since Hibernate can not resolve the items correctly.
	 */
	@Override
	public void _delete() {
		JpqlSelect select = new JpqlSelect();
		select.from("SenderItemValue");
		select.where("field = ?").param(this);
		List<SenderItemValue> senderValues = SenderItemValue.find(select.toString(), select.getParams().toArray()).fetch();

		for (SenderItemValue senderItemValue : senderValues) {
			senderItemValue.delete();
		}

		super._delete();
	}
}

package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;

@Entity
public class BooleanField extends Field {
	public static final String TYPE = "BOOLEAN";

	public BooleanField() {
		super();
	}

	public BooleanField(Container parent) {
		super(parent);
	}

	@Override
	public String getType() {
		return TYPE;
	}
}
package models.container.fields;

import javax.persistence.Entity;

import models.container.Container;
import models.container.Field;
import containers.FieldType;

@Entity
public class FileField extends Field {
	
	public FileField(Container parent) {
		super(parent);
	}

	public FileField() {
		super();
	}

	@Override
	public String getType() {
		return FieldType.FILE.toString();
	}
	
}

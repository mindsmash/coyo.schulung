package models.container;

public enum ContainerPermission {
	Create(0), ReadAll(1), ReadOwn(2), UpdateAll(3), UpdateOwn(4), DeleteAll(5), DeleteOwn(6);

	public int bit;

	private ContainerPermission(int bit) {
		this.bit = bit;
	}
}
package models.container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.ExtendedSearchableModel;
import models.Sender;
import models.TenantModel;
import models.User;
import models.app.App;
import models.app.ContainerApp;
import models.notification.listapp.ListItemNotification;

import org.apache.commons.lang3.StringUtils;

import play.Play;
import play.classloading.ApplicationClasses;
import play.data.validation.Valid;
import play.data.validation.Validation.ValidationResult;
import utils.ValidationHelper;
import acl.Permission;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import controllers.Security;
import controllers.WebBaseController;
import fishr.Indexed;
import fishr.search.Searchable;

@Entity
@Indexed
@Searchable(category = "container_item")
@Table(name = "container_item")
@Inheritance(strategy = InheritanceType.JOINED)
public class Item extends TenantModel implements ExtendedSearchableModel {

	public static final String ITEM_PARAM_NAME = "itemId";
	@ManyToOne(optional = false)
	public Container container;

	@OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
	@Valid
	public List<ItemValue> values = new ArrayList<>();

	@ManyToOne(optional = false)
	public User creator;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "item")
	public Set<ListItemNotification> itemNotifications = new HashSet<ListItemNotification>();

	public Item(Container container, User creator) {
		this.container = container;
		this.creator = creator;
	}

	public void addValue(ItemValue value) {
		values.add(value);
		value.item = this;
	}

	public void removeValue(Long fieldId) {
		ItemValue itemValue = getItemValueForField(fieldId);
		values.remove(itemValue);
		itemValue.delete();
	}

	public boolean canBeDeletedBy(User user) {
		return container.hasPermission(ContainerPermission.DeleteAll)
				|| (container.hasPermission(ContainerPermission.DeleteOwn) && isOwnedBy(user));
	}

	public boolean canBeReadBy(User user) {
		return container.hasPermission(ContainerPermission.ReadAll)
				|| (container.hasPermission(ContainerPermission.ReadOwn) && isOwnedBy(user));
	}

	public boolean canBeUpdatedBy(User user) {
		return container.hasPermission(ContainerPermission.UpdateAll)
				|| (container.hasPermission(ContainerPermission.UpdateOwn) && isOwnedBy(user));
	}

	public ItemValue getItemValueForField(final Long fieldId) {
		return Iterables.find(values, new Predicate<ItemValue>() {
			@Override
			public boolean apply(ItemValue itemValue) {
				return itemValue.field.id.equals(fieldId);
			}
		}, null);
	}

	public List<ValidationResult> getValidationResults() {
		ArrayList<ValidationResult> result = Lists.newArrayList();

		// first check if all required fields are there
		Collection<Field> requiredFields = container.getRequiredFields();
		for (Field requiredField : requiredFields) {
			if (!hasValueForField(requiredField)) {
				result.add(ValidationHelper.makeValidationResultWithError(requiredField.id.toString(),
						"validation.required", new String[] {}));
			}
		}

		// then check all item values
		for (ItemValue itemValue : values) {
			result.addAll(itemValue.getValidationResults());
		}

		return result;
	}

	public Object getValueForField(Long fieldId) {
		ItemValue itemValue = getItemValueForField(fieldId);
		return itemValue == null ? null : itemValue.getValue();
	}

	public boolean hasValueForField(final Field field) {
		return Iterables.any(values, new Predicate<ItemValue>() {
			@Override
			public boolean apply(ItemValue itemValue) {
				return itemValue.field.equals(field);
			}
		});
	}

	private boolean isOwnedBy(User user) {
		return creator != null && creator.equals(user);
	}

	public void setValue(final Long fieldId, String value) {
		ItemValue itemValue = getItemValueForField(fieldId);
		if (itemValue == null) {
			throw new IllegalArgumentException(String.format(
					"Cannot set value for field with ID %d because it does not exist.", fieldId));
		}
		itemValue.setValue(value);
	}

	@Override
	public void _delete() {
		// manually deleting values to avoid cascading delete Hibernate bugs
		for (ItemValue itemValue : values) {
			itemValue.delete();
		}
		values.clear();
		super._delete();
	}

	@fishr.Field
	public String getIndexableItemValues() {
		if (values == null) {
			return null;
		}

		final StringBuffer sB = new StringBuffer();
		for (Iterator<ItemValue> iter = values.iterator(); iter.hasNext();) {
			final ItemValue itemValue = iter.next();
			if (itemValue == null) {
				continue;
			}
			final String stringValue = itemValue.getStringValue();
			if (StringUtils.isEmpty(stringValue)) {
				continue;
			}
			sB.append(stringValue);
			if (iter.hasNext()) {
				sB.append(" ");
			}
		}
		return sB.toString();
	}

	@Override
	public void redirectToResult() {
		final App app = getApp();
		final Map<String, Object> args = new HashMap<String, Object>();
		args.put(ITEM_PARAM_NAME, id);

		if (app != null) {
			WebBaseController.redirectToSender(container.sender, app.id, args);
		} else {
			WebBaseController.redirectToSender(container.sender, null, null);
		}
	}

	/**
	 * Returns display name for search results Try to find StringItemValue or OptionsItemValue else the string value of
	 * the first item is returned
	 * 
	 * @return String
	 */
	@Override
	public String getDisplayName() {
		final StringBuffer sB = new StringBuffer();
		for (Iterator<ItemValue> iter = values.iterator(); iter.hasNext();) {
			final ItemValue itemValue = iter.next();
			if (itemValue == null) {
				continue;
			}
			final String stringValue = itemValue.getStringValue();
			if (StringUtils.isEmpty(stringValue)) {
				continue;
			}
			if (itemValue.field.hidden) {
				continue;
			}
			sB.append(stringValue);
			if (iter.hasNext()) {
				sB.append(", ");
			}
		}
		return sB.toString();
	}

	@Override
	public Sender getSearchThumbSender() {
		return container.sender;
	}

	@Override
	public String getSearchInfoText() {
		final App app = getApp();
		return app.getSearchInfoText();
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.READ_CONTAINER_ITEM, this);
	}

	/**
	 * Try to find the app connected to this item Using ContainerApp annotation to search within classes using the
	 * container class
	 * 
	 * @return App
	 */
	public App getApp() {
		List<ApplicationClasses.ApplicationClass> applicationClasses = Play.classes
				.getAnnotatedClasses(ContainerApp.class);

		for (ApplicationClasses.ApplicationClass applicationClass : applicationClasses) {
			if (!applicationClass.isClass()) {
				continue;
			}
			final ContainerApp containerApp = applicationClass.javaClass.getAnnotation(ContainerApp.class);
			final String fieldName = containerApp.fieldName();
			final Class clazz = applicationClass.javaClass;

			final Object o = container.getEntityUsingContainer(clazz, fieldName);
			if (o instanceof App) {
				return (App) o;
			}
		}

		return null;
	}
}

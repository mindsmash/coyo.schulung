package models;

import play.data.validation.Required;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * A device is used to grant access to our API using oauth 2.0 authentication. A {@link User} can own many devices.
 * Each device must have a name, access token, a timestamp when the access token was created and a refresh token.
 * <p/>
 * The access token can be used to access our API providing it as a bearer authorization header with your requests.
 * Each access token has a configurable lifetime. If the access token is no longer valid a new one can be acquired using
 * the refresh token.
 * <p/>
 * New device, token creation and token access control is done by {@link controllers.api.OAuth2}.
 */
@Table(name = "user_device")
@Entity
public class UserDevice extends TenantModel {

	@ManyToOne
	public User user;

	@Required
	public String name;

	public String description;

	@Required
	public String accessToken;

	@Required
	public Date accessTokenCreated;

	@Required
	public String refreshToken;
}

package models.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

import acl.Permission;
import controllers.Security;
import models.User;
import models.UserGroup;
import models.page.Page;
import models.page.Page.PageVisibility;
import play.db.helper.JpqlSelect;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.JPA;
import session.UserLoader;
import util.Util;

public class PageDao {

	/**
	 * Fetches the pages that are interesting to the given user, excluding pages
	 * that the user already follows. Pages are sorted by their number of
	 * followers (desc). Access permissions are considered.
	 * 
	 * @param user
	 * @param max
	 *            Max pages to return
	 * @return List of pages
	 */
	public List<Page> getInterestingPages(User user, int max) {
		List<Page> show = new ArrayList<>();

		List<Page> pages = Page
				.find("SELECT p FROM Page p WHERE ? NOT MEMBER OF p.followers ORDER BY SIZE(p.followers) DESC", user)
				.fetch();
		for (Page page : pages) {
			if (Security.check(Permission.ACCESS_PAGE, page.id)) {
				show.add(page);
			}

			if (show.size() >= max) {
				break;
			}
		}
		return show;
	}

	/**
	 * Fetches the IDs of the users that are forced to follow this page.
	 * 
	 * @param page
	 * @return Collection of IDs.
	 */
	public Collection<Long> getForceFollowUserIds(Page page) {
		Monitor monitor = MonitorFactory.start("Page.getForceFollowUserIds");

		try {
			if (page.forceAllFollow) {
				// fetch IDs of all active users
				return page.em().createQuery("SELECT u.id FROM User u WHERE u.active = true AND u.external = false").getResultList();
			}

			Set<Long> ids = new HashSet<>();

			// fetch and add forced user followers
			ids.addAll(
					page.em()
							.createQuery(
									"SELECT u.id FROM Page p JOIN p.forcedFollowers u WHERE p.id = ? AND u.active = true")
							.setParameter(1, page.id).getResultList());

			// fetch and add forced group followers
			ids.addAll(page.em()
					.createQuery(
							"SELECT u.id FROM Page p JOIN p.forcedFollowerGroups g JOIN g.users u WHERE p.id = ? AND u.active = true")
					.setParameter(1, page.id).getResultList());

			return ids;
		} finally {
			monitor.stop();
		}
	}

	/**
	 * Fetches the users that are forced to follow this page.
	 * 
	 * Deprecated: Please use {@link #getForceFollowUserIds(Page)} instead.
	 * 
	 * @param page
	 * @return Collection of users
	 */
	@Deprecated
	public Collection<User> getForceFollowUsers(Page page) {
		if (page.forceAllFollow) {
			return User.findAll();
		}

		Set<User> users = new HashSet<>();
		users.addAll(page.forcedFollowers);
		for (UserGroup group : page.forcedFollowerGroups) {
			users.addAll(group.users);
		}
		return users;
	}

	/**
	 * Fetches sorted pages that given user can see.
	 * 
	 * @param user
	 * @param page
	 * @param length
	 * @param categoryId
	 * @param filterQuery
	 * @return
	 */
	public List<Page> fetchSorted(User user, Integer page, Integer length, Long categoryId, String filterQuery) {
		JpqlSelect select = getBaseSelect(user, true);
		addCategoryFilter(select, categoryId);
		addNameFilter(select, filterQuery);
		return executeBaseSelect(select, UserLoader.getConnectedUser(), true, page, length);
	}

	/**
	 * Fetches all pages that the given user can see and that he either follows
	 * or is admin of.
	 * 
	 * @param user
	 * @param page
	 * @param length
	 * @param categoryId
	 * @param filterQuery
	 * @return
	 */
	public List<Page> fetchSortedUserPages(User user, Integer page, Integer length, Long categoryId,
			String filterQuery) {
		JpqlSelect select = getBaseSelect(user, true);
		select.andWhere("(? MEMBER OF p.admins OR ? MEMBER OF p.followers)").params(user, user);
		addCategoryFilter(select, categoryId);
		addNameFilter(select, filterQuery);
		return executeBaseSelect(select, user, true, page, length);
	}

	/**
	 * Counts the pages that the user can see (list).
	 * 
	 * @param user
	 * @return Number of pages.
	 */
	public long count(User user) {
		if (user.superadminMode) {
			return Page.count();
		}
		JpqlSelect select = getBaseSelect(user, true);
		return Page.count(select.toString(), select.getParams().toArray());
	}

	/**
	 * Fetches all pages that the given user can see and that he is forced to
	 * follow.
	 * 
	 * @param user
	 * @return
	 */
	public Collection<Page> getForcedFollowPages(User user) {
		JpqlSelect select = getBaseSelect(user, false);
		select.select("p");
		select.from("Page p LEFT JOIN p.forcedFollowerGroups gr");
		select.andWhere("(p.forceAllFollow = true OR ? MEMBER OF p.forcedFollowers OR ? MEMBER OF gr.users)");
		select.param(user).param(user);
		return Page.find(select.toString(), select.getParams().toArray()).fetch();
	}

	private void addCategoryFilter(JpqlSelect select, Long categoryId) {
		if (categoryId != null) {
			select.andWhere("p.category.id = ?").param(categoryId);
		}
	}

	private void addNameFilter(JpqlSelect select, String filterString) {
		if (StringUtils.isNotEmpty(filterString)) {
			select.andWhere("LOWER(p.name) LIKE ?").param("%" + filterString.toLowerCase() + "%");
		}
	}

	/**
	 * Prepares a basic select for pages that a user can see (list) without
	 * sorting and paging. Should be used in combination with
	 * {@link #executeBaseSelect(JpqlSelect, User, boolean, Integer, Integer)}.
	 * 
	 * @param user
	 * @return Select
	 */
	private JpqlSelect getBaseSelect(User user, boolean withFrom) {
		JpqlSelect select = new JpqlSelect();

		if (withFrom) {
			select.from("Page p");
		}

		if (!user.superadminMode) {
			String where = "p.visibility != ? OR ? MEMBER OF p.members OR ? MEMBER OF p.admins";
			if (user.groups.size() > 0) {
				where += " OR p IN (SELECT p2 FROM Page p2 JOIN p2.memberGroups g WHERE g IN ("
						+ Util.implode(user.getGroupIds(), ",") + "))";
			}
			select.where("(" + where + ")");
			select.params(PageVisibility.PRIVATE, user.id, user.id);
		}

		return select;
	}

	/**
	 * Executes the select created by {@link #getBaseSelect(User)}, optionally
	 * adding sorting and paging.
	 * 
	 * @param select
	 * @param user
	 * @param addSorting
	 * @param page
	 * @param length
	 * @return Limited list of pages
	 */
	private List<Page> executeBaseSelect(JpqlSelect select, User user, boolean addSorting, Integer page,
			Integer length) {
		if (addSorting) {
			select.select(
					"p, (p.forceAllFollow + (SELECT COUNT(f) FROM Page f WHERE f.id = p.id AND ? MEMBER OF f.forcedFollowers) + (SELECT COUNT(f) FROM Page f JOIN f.forcedFollowerGroups g WHERE f.id = p.id AND ? MEMBER OF g.users)) AS forced");

			select.getParams().add(0, user);
			select.getParams().add(0, user);

			select.orderBy("forced DESC, p.name ASC");

			Query query = JPA.em().createQuery(select.toString());

			if (page != null && length != null) {
				query.setFirstResult(page * length);
			}
			if (length != null) {
				query.setMaxResults(length);
			}

			for (int i = 0; i < select.getParams().size(); i++) {
				query.setParameter((i + 1), select.getParams().get(i));
			}

			List<Object[]> data = query.getResultList();

			List<Page> result = new ArrayList<>();
			for (Object[] arr : data) {
				result.add((Page) arr[0]);
			}

			return result;
		} else {
			JPAQuery query = Page.find(select.toString(), select.getParams().toArray());
			if (page != null && length != null) {
				return query.fetch(page + 1, length);
			} else {
				return query.fetch();
			}
		}
	}
}

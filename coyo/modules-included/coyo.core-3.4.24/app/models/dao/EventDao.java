package models.dao;

import java.util.List;

import models.User;
import models.event.Event;
import models.event.EventBase;
import models.event.EventUser;
import models.event.EventUser.EventResponse;

import org.joda.time.DateTime;

/**
 * A Data Access Object for {@link Event} models.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class EventDao {

	/**
	 * Return all upcoming events, that the passed user has created, ordered by the events start date ascending.
	 */
	public List<Event> getUpcomingCreatedEvents(User user) {
		return Event.find("SELECT e FROM Event e WHERE ? MEMBER OF e.admins AND e.endDate >= ? ORDER BY startDate ASC",
				user, new DateTime()).fetch();
	}

	/**
	 * Returns an unordered list of all invited users of this event
	 */
	public List<User> getInvitedUsers(EventBase event) {
		return EventUser.find("SELECT eu.user FROM EventUser eu WHERE eu.event = ?", event).fetch();
	}

	/**
	 * Returns an unordered list of all users of the given event, who set the passed response.
	 */
	public List<User> getUsers(EventBase event, EventResponse response) {
		return EventUser.find("SELECT eu.user FROM EventUser eu WHERE eu.response = ? AND eu.event = ?", response,
				event).fetch();
	}

	/**
	 * Returns the count of users who set the passed response to the given event.
	 */
	public long getUserCount(EventBase event, EventResponse response) {
		return EventUser.count("SELECT COUNT(eu) FROM EventUser eu WHERE eu.response = ? AND eu.event = ?", response,
				event);
	}

}
package models.dao;

import acl.Permission;
import controllers.Security;
import models.Application;
import models.Sender;
import models.User;
import models.wall.post.Post;
import models.workspace.Workspace;
import org.joda.time.DateTime;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import util.Util;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDao {

	private static final String SQL_POSTS = "SELECT p.id FROM Post p WHERE p.wall.noActivityStream = false AND ("
			+ " p.author = :user OR p.wall.sender = :sender1 OR p.wall.sender = :user OR"
			+ " p.wall.sender.id IN (SELECT s.id FROM Sender s WHERE :user MEMBER OF s.followers) )";

	private static final String SQL_POSTS_FLAGGED = "SELECT p.id FROM Post p INNER JOIN p.userRelations pu WHERE pu.post = p AND pu.user = :user "
			+ "AND p.wall.noActivityStream = false AND pu.flagged = true";

	private static final String SQL_USER_TO_MENTION_WORKSPACE = "SELECT user FROM User user "
			+ "WHERE active=true AND slug != :term "
			+ "AND (LOWER(slug) LIKE :slug OR LOWER(firstName) LIKE :term OR LOWER(lastName) LIKE :term) "
			+ "AND EXISTS "
			+ "(SELECT wm FROM WorkspaceMember wm WHERE wm.user = user.id AND wm.workspace.id = :senderID) "
			+ "ORDER BY lastName, firstName";

	private static final String SQL_POSTS_COUNT_FLAGGED = "SELECT COUNT(*) FROM Post p "
			+ "INNER JOIN p.userRelations pu "
			+ "WHERE pu.post = p AND pu.user = :user AND p.wall.noActivityStream = false AND pu.flagged = true";

	private static final String SQL_POSTS_COUNT = "SELECT COUNT (*) FROM Post p "
			+ "WHERE p.wall.noActivityStream = false "
			+ "AND ( p.author = :user OR p.wall.sender = :sender OR p.wall.sender = :user OR p.wall.sender.id "
			+ "IN (SELECT s.id FROM Sender s WHERE :user MEMBER OF s.followers))";

	private static final String SQL_ORDER_BY_ID = "p.id DESC";

	private static final String SQL_ORDER_BY_IMPORTANCE = " ORDER BY CASE WHEN (p.important = true AND p.created > :usercreated AND "
			+ "NOT EXISTS (SELECT pu FROM PostUser pu WHERE pu.post = p AND pu.user = :user AND pu.isRead = true)) "
			+ "THEN 0 ELSE 1 END ";

	/*
	 * days from year 0 until modified: "((YEAR(p.created) * 365) + (MONTH(p.created) * 30.44) + DAY(p.created))";
	 * 
	 * days from year 0 until now:
	 * "((YEAR(CURRENT_TIMESTAMP) * 365) + (MONTH(CURRENT_TIMESTAMP) * 30.44) + DAY(CURRENT_TIMESTAMP))";
	 */
	private static final Object SQL_ORDER_BY_TOP_RATING = "(((YEAR(p.modified) * 365) + (MONTH(p.modified) * 30.44) + DAY(p.modified)) "
			+ " - ((YEAR(CURRENT_TIMESTAMP) * 365) "
			+ " + (MONTH(CURRENT_TIMESTAMP) * 30.44) + DAY(CURRENT_TIMESTAMP))) DESC, "
			+ " ((SELECT COUNT(pu) FROM PostUser pu WHERE pu.post = p AND pu.likes = true) + "
			+ " (SIZE(p.comments) * 4)) DESC, modified DESC";

	/**
	 * Fetches the posts of a certain user. The posts are filtered by DisplayType and a start and end date.
	 *
	 * @param user
	 *            the user the posts belong to
	 * @param displayType
	 *            DisplayType used for filtering the posts
	 * @param before
	 *            end date filter
	 * @param after
	 *            start date filter
	 * @param page
	 *            page count used for pagination
	 * @param pageSize
	 *            page size used for pagination
	 *
	 * @return the found posts
	 */
	public static List<models.wall.post.Post> fetchPosts(final User user,
			final User.ActivityStreamSelect.DisplayType displayType, final Date before, final Date after,
			final Integer page, final Integer pageSize) {
		StringBuffer sb = new StringBuffer();

		if (displayType == User.ActivityStreamSelect.DisplayType.FLAGGED_POSTS) {
			sb.append(SQL_POSTS_FLAGGED);
			sb.append(SQL_ORDER_BY_IMPORTANCE);
			sb.append(",");
			sb.append(SQL_ORDER_BY_ID);
		} else {
			sb.append(SQL_POSTS);
			if (before != null) {
				sb.append(" AND p.created <= :before ");
			}
			if (after != null) {
				sb.append(" AND p.created > :after ");
			}
			if (displayType == User.ActivityStreamSelect.DisplayType.NATURAL) {
				sb.append(SQL_ORDER_BY_IMPORTANCE);
				sb.append(",");
				sb.append(SQL_ORDER_BY_ID);
			} else if (displayType == User.ActivityStreamSelect.DisplayType.TOP_POSTS) {
				sb.append(SQL_ORDER_BY_IMPORTANCE);
				sb.append(",");
				sb.append(SQL_ORDER_BY_TOP_RATING);
			}
		}

		final TypedQuery<Long> q = JPA.em().createQuery(sb.toString(), Long.class);
		q.setParameter("user", user);
		q.setParameter("usercreated", user.created);

		if (displayType != User.ActivityStreamSelect.DisplayType.FLAGGED_POSTS) {
			q.setParameter("sender1", Application.get());
			if (after != null) {
				q.setParameter("after", after);
			}
			if (before != null) {
				q.setParameter("before", before);
			}
		}

		if (null == page && pageSize != null) {
			q.setMaxResults(pageSize);
		} else if (pageSize != null && page != null) {
			q.setMaxResults(pageSize);
			q.setFirstResult(pageSize * (page - 1));
		}

		final List<Long> postIds = q.getResultList();

		// check permissions
		List<Post> posts = new ArrayList<Post>();
		if (!postIds.isEmpty()) {
			final List<Post> results = Post.find("id IN (" + Util.implode(postIds, ",") + ")").fetch();

			// need to keep origin order
			for (Long postId : postIds) {
				for (Post post : results) {
					if (post.id.equals(postId) && Security.check(Permission.ACCESS_POST, post)) {
						posts.add(post);
						break;
					}
				}
			}
		}
		return posts;
	}

	/**
	 * Counts the posts of a certain user. The posts are filtered by DisplayType and a start and end date.
	 * 
	 * @param user
	 *            the user the posts belong to
	 * @param displayType
	 *            DisplayType
	 * @param before
	 *            the end date filter
	 * @param after
	 *            the start date filter
	 *
	 * @return the count of found posts
	 */
	public static Long countPosts(final User user, final Date before, final Date after) {
		final StringBuffer sb = new StringBuffer(SQL_POSTS_COUNT);
		if (before != null) {
			sb.append(" AND p.created <= :before");
		}
		if (after != null) {
			sb.append(" AND p.created > :after");
		}

		final TypedQuery<Long> q = JPA.em().createQuery(sb.toString(), Long.class);
		q.setParameter("user", user);
		q.setParameter("sender", Application.get());

		if (after != null) {
			q.setParameter("after", after);
		}
		if (before != null) {
			q.setParameter("after", before);
		}

		return q.getSingleResult();
	}

	/**
	 * Counts all flagged posts for a given user.
	 * 
	 * @param user
	 *            the user the posts belong to
	 * @return amount of all found flagged posts, or zero if none.
	 */
	public static Long countFlaggedPosts(User user) {
		final Query pq = JPA.em().createQuery(SQL_POSTS_FLAGGED);
		pq.setParameter("user", user);
		final List<Long> list = pq.getResultList();

		// filter posts that the user once flagged but that he can't access any more
		Long count = 0L;
		for(Long postId : list) {
			if(Security.check(Permission.ACCESS_POST, postId)) {
				++count;
			}
		}

		return count;
	}

	/**
	 * Get all users that can be mentioned in a comment or post, which belongs to the passed sender and start with the
	 * passed term. The result is limited to 'limit' users.
	 */
	public static List<User> getUserToMention(Sender sender, String term, int limit) {
		// add case for sender is workspace - plain jpql single query
		if (sender != null && sender instanceof Workspace) {
			Workspace workspace = (Workspace) sender;
			return findToMention(workspace, term, limit);
		} else {
			// TODO: only allow those users to be marked in a post that have access
			// to the sender of the wall.
			JpqlSelect select = new JpqlSelect();
			select.andWhere("active = true").orderBy("lastName, firstName");
			// search for slug or name match
			select.andWhere("slug != ? AND (LOWER(slug) LIKE ? OR LOWER(firstName) LIKE ? OR LOWER(lastName) LIKE ?)")
					.params(term, term + "%", term + "%", term + "%");
			// determine if externals should be shown
			select.andWhere("external = false");

			return User.find(select.toString(), select.getParams().toArray()).fetch(limit);
		}
	}

	private static List<User> findToMention(Workspace workspace, String term, Integer maxResults) {
		TypedQuery<User> query = JPA.em().createQuery(SQL_USER_TO_MENTION_WORKSPACE, User.class);
		query.setParameter("term", term + "%");
		query.setParameter("slug", term);
		query.setParameter("senderID", workspace.id);
		return query.setMaxResults(maxResults).getResultList();
	}

	/**
	 * Returns the latest active users that have logged in. Can be used to find new members.
	 * 
	 * @param maxResults
	 *            max results threshold
	 * @param daysThreshold
	 *            days threshold to look up users
	 * @return
	 */
	public static List<User> getLatestUsers(final int maxResults, final int daysThreshold) {
		final String sql = "SELECT user_ FROM User user_ WHERE user_.active = true AND user_.external = false AND user_.lastLogin IS NOT NULL "
				+ "AND user_.created > :threshold ORDER BY user_.created DESC";
		final TypedQuery<User> query = User.em().createQuery(sql, User.class);

		query.setParameter("threshold", new DateTime().minusDays(daysThreshold).toDate());
		query.setMaxResults(maxResults);

		return query.getResultList();
	}
}
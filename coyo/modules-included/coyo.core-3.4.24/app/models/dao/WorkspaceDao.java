package models.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import models.User;
import models.workspace.Workspace;
import models.workspace.Workspace.WorkspaceVisibility;
import models.workspace.WorkspaceMember;

import org.apache.commons.lang.StringUtils;

import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import acl.Permission;
import controllers.Security;

public class WorkspaceDao {

	/**
	 * Returns the list of active users who are member of the passed workspace. The users are primarily ordered by admin
	 * descending (admins first) and secondarily by lastname, firstname. The list can be limited by setting the
	 * parameter "limit". This parameter is optional.
	 */
	public List<User> getUserMember(Workspace workspace, Integer limit) {
		final TypedQuery<User> memberQuery = JPA.em().createQuery(
				"SELECT wm.user FROM WorkspaceMember wm "
						+ "WHERE wm.workspace = :workspace AND wm.user.active = true "
						+ "ORDER BY wm.admin DESC, wm.user.lastName, wm.user.firstName", User.class);

		// limit query results
		if (limit != null) {
			memberQuery.setMaxResults(limit);
		}
		memberQuery.setParameter("workspace", workspace);
		return memberQuery.getResultList();
	}

	/**
	 * Returns the list of active users who are member of the passed workspace. The list can be limited by setting the
	 * parameter "limit". This parameter is optional.
	 * 
	 * In comparison to {@link #getUserMember(Workspace, Integer)} a list of {@linkplain WorkspaceMember} objects is
	 * returned, instead of {@linkplain User}. The results are not ordered in any way.
	 */
	public List<WorkspaceMember> getMember(Workspace workspace, Integer limit) {
		final TypedQuery<WorkspaceMember> memberQuery = JPA.em().createQuery(
				"SELECT wm FROM WorkspaceMember wm WHERE wm.workspace = :workspace AND wm.user.active = true "
						+ "ORDER BY wm.user.lastName, wm.user.firstName", WorkspaceMember.class);

		// limit query results
		if (limit != null) {
			memberQuery.setMaxResults(limit);
		}
		memberQuery.setParameter("workspace", workspace);
		return memberQuery.getResultList();
	}

	/**
	 * Returns a list of active users that are workspace members
	 *
	 * @param id
	 *            The id of the {@linkplain Workspace}
	 * @return An unordered list of active {@linkplain User}s that are {@linkplain WorkspaceMember}s in the
	 *         {@linkplain Workspace} with the id id
	 */
	public List<User> getActiveWorkspaceMembers(Long id) {
		final TypedQuery<User> memberQuery = JPA.em().createQuery(
				"SELECT wm.user FROM WorkspaceMember wm "
						+ "WHERE wm.workspace.id = :id AND wm.user.active = true ORDER BY wm.created DESC", User.class);
		memberQuery.setParameter("id", id);
		return memberQuery.getResultList();
	}

	/**
	 * Returns all workspaces for a passed user and a category. In addition the list can be filtered by a string query
	 * term. The term and the category id are optional and can be null. This method supports pagination via page and
	 * limit. It returns a list of workspaces matching the given criteria or an empty list if none could be found.
	 */
	public List<Workspace> getWorkspaces(int page, int limit, User user, Long categoryId, String filterQuery,
			boolean archived) {
		/* create select for getting workspaces */
		JpqlSelect select = new JpqlSelect();
		select.from("Workspace w");
		select.where("archived = ?").param(archived);

		/*
		 * This basically reflects the permission check for LIST_WORKSPACE. Only get workspaces that are not private or
		 * that I am a member of.
		 */
		if (!Security.check(Permission.SUPERADMIN_MODE)) {
			select.andWhere(
					"(w.visibility != ? OR EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace.id = w.id AND wm.user.id = ?))")
					.params(WorkspaceVisibility.PRIVATE, user.id);
		}

		/* Filter by category if one is set */
		if (categoryId != null) {
			select.andWhere("category.id = ?").param(categoryId);
		}

		/* Filter by query if set */
		if (!StringUtils.isEmpty(filterQuery)) {
			select.andWhere("LOWER(name) LIKE ?").param("%" + filterQuery.toLowerCase() + "%");
		}
		select.orderBy("name");

		return Workspace.find(select.toString(), select.getParams().toArray()).fetch(page, limit);
	}

	/**
	 * Returns a list of workspaces where a give user is member. In addition the list can be filtered by a string query
	 * term. The term and the category id are optional and can be null. This method supports pagination via page and
	 * limit. It returns a list of workspaces matching the given criteria or an empty list if none could be found.
	 */
	public List<Workspace> getWorkspacesWhereUserIsMember(int page, int limit, User user, String filterQuery,
			boolean archived) {
		/* create select for getting workspaces */
		JpqlSelect select = new JpqlSelect();
		select.from("Workspace w");
		select.where("archived = ?").param(archived);

		select.andWhere("(EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace.id = w.id AND wm.user.id = ?))")
				.params(user.id);

		/* Filter by query if set */
		if (!StringUtils.isEmpty(filterQuery)) {
			select.andWhere("LOWER(name) LIKE ?").param("%" + filterQuery.toLowerCase() + "%");
		}
		select.orderBy("name");

		return Workspace.find(select.toString(), select.getParams().toArray()).fetch(page, limit);
	}

	/**
	 * Returns the number of workspaces matching the given criteria. If no workspaces could be found '0' is returned.
	 */
	public long getWorkspaceCount(User user, Long categoryId, boolean archived) {
		JpqlSelect select = new JpqlSelect();
		select.from("Workspace w");
		select.where("archived = ?").param(archived);

		/*
		 * This basically reflects the permission check for LIST_WORKSPACE. Only get workspaces that are not private or
		 * that I am a member of.
		 */
		if (!Security.check(Permission.SUPERADMIN_MODE)) {
			select.andWhere(
					"(w.visibility != ? OR EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace.id = w.id AND wm.user.id = ?))")
					.params(WorkspaceVisibility.PRIVATE, user.id);
		}

		/* Filter by category if one is set */
		if (categoryId != null) {
			select.andWhere("category.id = ?").param(categoryId);
		}
		return Workspace.count(select.toString(), select.getParams().toArray());
	}

}

package models.dao;

import java.util.List;

import models.Sender;
import models.bookmark.Bookmark;

public class BookmarkDao {

	/**
	 * Get all bookmarks for passed user from database. Private and global ones.
	 */
	public static List<Bookmark> getBookmarks(Sender user) {
		return Bookmark.find("sender = null OR sender = ? ORDER BY sender ASC, priority ASC, label ASC", user).fetch();
	}

	/**
	 * Get all global bookmarks.
	 */
	public static List<Bookmark> getGlobalBookmarks() {
		return Bookmark.find("sender = null ORDER BY priority ASC").fetch();
	}

}
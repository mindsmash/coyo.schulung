package models;

import java.io.File;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.wall.Wall;
import play.Play;
import play.data.binding.NoBinding;
import storage.FlexibleBlob;
import utils.Version;
import fishr.Indexed;

@Entity
@Table(name = "application")
@SenderType("application")
@Indexed
public class Application extends Sender {

	public static final String DATA_DIR = Play.configuration.getProperty("coyo.application.dataDir", "data");
	public static final boolean POSTS_ENABLED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.application.posts.enabled", "true"));
	public static final String NOTIFICATIONS_DISABLED = Play.configuration.getProperty(
			"coyo.application.posts.exclude", "");
	public static final int BRUTEFORCE_PROTECTION_MAX = Integer.parseInt(Play.configuration.getProperty(
			"coyo.protection.bruteForce.max", "100"));
	public static final String BRUTEFORCE_PROTECTION_BAN = Play.configuration.getProperty(
			"coyo.protection.bruteForce.banSeconds", "30") + "s";
	public static final String X_FRAME_OPTIONS = Play.configuration.getProperty("coyo.protection.xFrameOptions");
	public static final boolean DATA_IMPORT_ALLOWED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.dataImport.allowed", "true"));
	public static final boolean DATA_EXPORT_ALLOWED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.dataExport.allowed", "true"));

	public FlexibleBlob logo;
	public FlexibleBlob smallLogo;
	public FlexibleBlob favicon;

	@NoBinding
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public UserNotificationSettings defaultNotificationSettings = new UserNotificationSettings();

	public UserNotificationSettings getDefaultNotificationSettings() {
		if (defaultNotificationSettings == null) {
			defaultNotificationSettings = new UserNotificationSettings();
		}
		return defaultNotificationSettings;
	}

	@Override
	public Wall getDefaultWall() {
		// create default wall
		if (walls.size() == 0) {
			Wall.inject(this);
		}
		return walls.get(0);
	}

	@Override
	public String getDisplayName() {
		return Play.configuration.getProperty("application.name");
	}

	public static Application get() {
		if (count() == 0) {
			new Application().save();
		}
		return find("").first();
	}

	@Override
	protected void beforeSave() throws Exception {
		if (!isPersistent() && count() >= 1) {
			throw new Error("cannot create more than one application sender");
		}
		super.beforeSave();
	}

	@Override
	public List<User> getAdmins() {
		return User.find("active = true AND superadmin = true").fetch();
	}

	@Override
	protected void updateFollowers() {
		// has no followers
	}

	public static String getApplicationVersion() {
		return Play.configuration.getProperty("application.version");
	}

	public static String getDbSchemaVersion() {
		Version appVersion = new Version(getApplicationVersion());
		Version schemaVersion = Version.getSchemaVersionFromProductVersion(appVersion);
		return (schemaVersion != null) ? schemaVersion.toString() : "";
	}

	/**
	 * Method for getting the general data folder for a given tenant. The application can store any data in this home
	 * folder.
	 * 
	 * @param tenant
	 * @return
	 */
	public static File getDataDir(AbstractTenant tenant) {
		String dirName = DATA_DIR + File.separator + "tenant-" + tenant.id;

		File dataDir;
		if (new File(dirName).isAbsolute()) {
			dataDir = new File(dirName);
		} else {
			dataDir = Play.getFile(dirName);
		}

		if (!dataDir.exists()) {
			dataDir.mkdir();
		}

		return dataDir;
	}

	@Override
	public String getSearchInfoText() {
		return null;
	}
}

package models;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.app.FilesApp;
import models.app.FilesApp.FilesAppException;
import models.app.LocalFilesApp;
import models.app.files.File;
import models.app.files.FileVersion;
import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.ShareFilePost;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import play.Logger;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.MimeTypes;
import play.mvc.Router;
import storage.FlexibleBlob;
import acl.AppsPermission;
import checks.DuplicateFileNameCheck;
import controllers.Security;
import controllers.app.Files;
import fishr.Field;
import fishr.Indexed;
import fishr.search.ContentIndexable;
import fishr.search.Searchable;

@Indexed(boost = 2F)
@Entity
@Searchable(category = "files")
@Table(name = "box_file")
public class BoxFile extends TenantModel implements File, Sharable, ExtendedSearchableModel, Previewable {

	@ManyToOne
	public Box box;

	public boolean isRoot;

	@Transient
	private FilesApp app;

	@ManyToOne
	public BoxFile parent;
	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
	@OrderBy("folder DESC, name ASC")
	public List<BoxFile> children = new ArrayList<BoxFile>();
	@Field
	@Required
	@CheckWith(DuplicateFileNameCheck.class)
	@MaxSize(255)
	public String name;
	public boolean folder;
	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL)
	@OrderBy("created DESC")
	public List<BoxFileVersion> versions = new ArrayList<BoxFileVersion>();
	public boolean shared = false;
	@MaxSize(255)
	public String shareToken;
	@ManyToOne
	public User creator;
	public int downloads;

	@ManyToOne
	public User lockedBy;
	public Date lockedAt;

	public BoxFile() {
	}

	public BoxFile(User creator, Box box, String name, boolean folder) {
		this(creator, box, box.getRoot(), name, folder);
	}

	public BoxFile(User creator, Box box, BoxFile parent, String name, boolean folder) {
		this.creator = creator;
		this.box = box;
		this.parent = parent;
		this.name = name;
		this.folder = folder;
	}

	public BoxFileVersion getCurrentVersion() {
		if (versions == null || versions.size() == 0) {
			return null;
		}
		return versions.get(0);
	}

	public void activateSharing(boolean newToken) {
		if (newToken || shareToken == null) {
			shareToken = Codec.UUID();
		}
		shared = true;
	}

	public void deactivateSharing() {
		shared = false;
	}

	public boolean lock(User user) {
		// check for active lock and that lock has not expired
		User lock = getLock();
		if (lock != null && lock != user) {
			return false;
		}
		lockedBy = user;
		lockedAt = new Date();
		save();
		return true;
	}

	public boolean unlock(User user) {
		if (lockedBy == user) {
			lockedBy = null;
			lockedAt = null;
			save();
			return true;
		}
		return false;
	}

	public Long getSize() {
		if (folder) {
			long fSize = 0;
			for (BoxFile child : children) {
				if (!child.folder) {
					fSize += child.getSize();
				}
			}
			return new Long(fSize);
		}

		return getCurrentVersion().size;
	}

	public Collection<BoxFile> getBreadcrumbs() {
		ArrayList<BoxFile> breadcrumbs = new ArrayList<BoxFile>();

		BoxFile current = this;
		breadcrumbs.add(current);
		while (current.parent != null) {
			breadcrumbs.add(0, current.parent);
			current = current.parent;
		}
		return breadcrumbs;
	}

	@Override
	public Long getParentSenderId() {
		return box.sender.getId();
	}

	public String getDisplayName() {
		return name;
	}

	public String getFileExtension(boolean withDot) {
		if (folder || name.indexOf(".") < 0) {
			return "";
		}
		return name.substring(name.lastIndexOf(".") + (withDot ? 0 : 1)).toLowerCase();
	}

	public String getFileExtension() {
		return getFileExtension(true);
	}

	public boolean isChild(BoxFile file, boolean recursive) {
		if (!folder || file.box != box) {
			return false;
		}
		if (children.contains(file)) {
			return true;
		}
		return recursive && file.getBreadcrumbs().contains(this);
	}

	public int getDepth() {
		int depth = 0;
		BoxFile current = this;
		while (current.parent != null) {
			depth++;
			current = current.parent;
		}
		return depth;
	}

	public String getType() {
		if (folder) {
			return null;
		}

		return getCurrentVersion().data.type();
	}

	public void addVersion(FlexibleBlob data, User creator) throws IOException {
		addVersion(data, creator, data.type());
	}

	public void addVersion(FlexibleBlob data, User creator, String mimeType) throws IOException {
		try (InputStream is = data.get();) {
			addVersion(is, creator, mimeType, data.length());
		}
	}

	public void addVersion(InputStream is, User creator, String mimeType, Long length) throws IOException {
		if (!folder) {
			BoxFileVersion version = new BoxFileVersion();
			version.file = this;
			version.creator = creator;
			FlexibleBlob blob = new FlexibleBlob();
			blob.set(is, mimeType, length);
			version.data = blob;
			version.save();
			refresh();
		}
	}

	public boolean containsFile(String name) {
		for (BoxFile child : children) {
			if (child.name.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public void move(BoxFile destination) {
		if (destination.box != box) {
			throw new UnsupportedOperationException("moving files to another box is not supported yet");
		}

		Logger.debug("moving file [%s] to destination parent [%s]", this, destination);

		parent = destination;
		save();
	}

	private String cutOfExtension (String name) {		
		int dot = name.lastIndexOf('.');
		if (folder || dot != -1) {
			return name.substring(0, dot);	
		}
		return name;
	}
	
	public void copy(BoxFile destination) {
		if (destination.box != box) {
			throw new UnsupportedOperationException("copying files to another box is not supported yet");
		}

		Logger.debug("copying file [%s] to destination parent [%s]", this, destination);

		// avoid duplicate name
		String name = getDisplayName();
		BoxFile file = new BoxFile().findByName(destination, name);
		String fileExtension = getFileExtension(true);
		
		while (destination.containsFile(name)) {	
			if (!file.folder) {
				name = cutOfExtension(name);
			}
			name += " (Copy )" + fileExtension;
			// TODO : localization			
			// TODO: do not add (Copy) twice, instead use (Copy 2) and so on
		}

		name += fileExtension;
		
		BoxFile copy = new BoxFile(creator, destination.box, destination, name, folder);
		if (!copy.folder) {
			try {
				copy.name = cutOfExtension(name);
				copy.save();
				copy.addVersion(getCurrentVersion().data, getCurrentVersion().creator, MimeTypes.getContentType(name));
			} catch (IOException e) {
				throw new FilesAppException(Messages.get("files.copy.error"), "original file not found", e);
			}
		}
		copy.save();

		// recursive
		for (BoxFile child : children) {
			child.copy(copy);
		}
	}

	@Override
	public String toString() {
		return "File[id=" + id + ",name=" + name + ", currentVersion=" + getCurrentVersion() + "]";
	}

	public static BoxFile findByName(BoxFile parent, String name) {
		return find("name = ? AND parent = ?", name, parent).first();
	}

	@Override
	public Post share(User user, Wall wall, String message) {
		ShareFilePost post = new ShareFilePost();
		post.author = user;
		post.wall = wall;
		post.message = message;
		post.app = getApp();
		post.fileUid = id + "";
		post.save();

		return post;
	}

	@Override
	public boolean isSharable() {
		return box.sender.isSharable();
	}

	@Override
	public boolean checkSearchPermission() {
		return getApp() != null && Security.check(AppsPermission.ACCESS_FILE, getApp(), this);
	}

	public void revert(BoxFileVersion version, User user) throws IOException {
		addVersion(version.data, user, version.data.type());
	}

	public DateTime getCreated(User user) {
		return new DateTime(created.getTime(), user.getDateTimeZone());
	}

	public DateTime getModified(User user) {
		/*
		 * As the main file entity gets modified just by locking the file, we use the modification date of the current
		 * version instead.
		 */
		BoxFileVersion currentVersion = getCurrentVersion();
		Date fileOrCurrentVersionModified = currentVersion == null ? this.modified : currentVersion.modified;
		return new DateTime(fileOrCurrentVersionModified.getTime(), user.getDateTimeZone());
	}

	@Override
	public int compareTo(File arg0) {
		return getName().compareTo(arg0.getName());
	}

	@Override
	public String getName() {
		return name.replaceAll("/", "-");
	}

	@Override
	public boolean isFolder() {
		return folder;
	}

	@Override
	public List<? extends File> getChildren() {
		return children;
	}

	@Override
	public List<? extends File> getChildFolders() {
		List<File> r = new ArrayList<File>();
		for (File file : children) {
			if (file.isFolder()) {
				r.add(file);
			}
		}
		return r;
	}

	@Override
	public String getUid() {
		return id + "";
	}

	@Override
	public File getParent() {
		return parent;
	}

	public FilesApp getApp() {
		if (app == null) {
			app = LocalFilesApp.find("box = ?", box).first();
		}
		return app;
	}

	@Override
	public List<? extends FileVersion> getVersions() {
		return versions;
	}

	@Override
	public FileVersion getVersion(int no) {
		return versions.get(versions.size() - no);
	}

	@Override
	public String getContentType() {
		return getType();
	}

	@Override
	public User getLock() {
		// check lock expired
		if (lockedBy != null) {
			// if lock expired (5 min)
			if (lockedAt.before(DateUtils.addMinutes(new Date(), -5))) {
				unlock(lockedBy);
			}
		}
		return lockedBy;
	}

	@Override
	public FileVersion addVersion(User user, InputStream in, Long length) {
		if (!folder) {
			BoxFileVersion version = new BoxFileVersion();
			version.file = this;
			version.creator = user;
			FlexibleBlob blob = new FlexibleBlob();
			blob.set(in, getCurrentVersion().data.type(), length);
			version.data = blob;
			versions.add(version);
			version.save();
			return version;
		}
		return null;
	}

	@Override
	public void rename(String name) {
		this.name = name;
		save();
	}

	@Override
	public boolean isChild(File file, boolean recursive) {
		return isChild((BoxFile) file, recursive);
	}

	@Override
	public void move(File destination) {
		move((BoxFile) destination);
	}

	@Override
	public void copy(File destination) {
		copy((BoxFile) destination);
	}

	@Override
	public void remove() {
		delete();
	}

	@Override
	public User getCreator() {
		return creator;
	}

	@Override
	public User getModifier() {
		if (folder) {
			return creator;
		}
		return getCurrentVersion().creator;
	}

	@Override
	public boolean checkAccessPermission(User user) {
		return true;
	}

	@Override
	public boolean checkModifyPermission(User user) {
		return true;
	}

	@Override
	public String getFileName() {
		return name;
	}

	@Override
	public FlexibleBlob getFileData() {
		if (!folder) {
			return getCurrentVersion().data;
		}
		return null;
	}

	@Override
	public String getDownloadUrl() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", getApp().id);
		args.put("uid", getUid());
		return Router.reverse("app.Files.download", args).url;
	}

	@Override
	public String getUniqueId() {
		String uid = "boxfile-" + id;

		if (getCurrentVersion() != null) {
			uid += "-version-" + getCurrentVersion().id;
		}

		return uid;
	}

	@Override
	public void redirectToResult() {
		if (!isFolder()) {
			Files.show(getApp(), getParent());
		}
		Files.show(getApp(), this);
	}

	@Override
	public Sender getSearchThumbSender() {
		return box.sender;
	}

	@Override
	public String getSearchInfoText() {
		return name;
	}

	@Override
	public boolean allowVersioning() {
		return true;
	}

	@Override
	public boolean allowRename() {
		return true;
	}

	@Override
	public boolean allowMove() {
		return true;
	}

	@Override
	public boolean allowCopy() {
		return true;
	}

	@Override
	public boolean allowDelete() {
		return true;
	}

	/**
	 * Have the latest file version indexed
	 */
	@Field
	public ContentIndexable getRelatedSearchLatestVersion() {
		if (isFolder())
			return null;

		final FileVersion version = getCurrentVersion();
		if (version == null)
			return null;

		return new ContentIndexable() {
			@Override
			public String type() {
				return getContentType();
			}

			@Override
			public long length() {
				return version.getSize();
			}

			@Override
			public InputStream get() {
				return version.get();
			}
		};
	}
}

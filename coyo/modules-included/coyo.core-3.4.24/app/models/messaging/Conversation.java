package models.messaging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.Sender;
import models.SenderType;
import models.User;
import models.wall.Wall;
import models.wall.post.Post;
import play.data.binding.NoBinding;
import play.db.jpa.JPA;
import session.UserLoader;
import util.Util;
import acl.Permission;
import controllers.Messaging;
import controllers.Security;

@Entity
@Table(name = "conversation")
@SenderType("conversation")
public class Conversation extends Sender {

	@NoBinding
	@OneToMany(mappedBy = "conversation", cascade = CascadeType.ALL)
	@OrderBy("id")
	public List<ConversationMember> members = new ArrayList<ConversationMember>();

	public Date lastPost;

	@Override
	public Wall getDefaultWall() {
		// create default wall
		if (walls.size() == 0) {
			Wall wall = Wall.inject(this);
			wall.noActivityStream = true;
			wall.restrictCollaboration = true;
			wall.save();
		}

		return walls.get(0);
	}

	@Override
	public String getDisplayName() {
		List<String> names = new ArrayList<String>();
		for (ConversationMember cm : members) {
			if (!cm.isDeleted()) {
				names.add(cm.user.getDisplayName());
			}
		}
		return Util.implode(names, ", ");
	}

	@Override
	public List<User> getAdmins() {
		return new ArrayList<User>();
	}

	public User getThumbnailUser() {
		return members.get(0).user;
	}

	public List<User> getUsers() {
		if (!isPersistent()) {
			List<User> r = new ArrayList<>();
			for (ConversationMember cm : members) {
				r.add(cm.user);
			}
			return r;
		}
		return User
				.find("SELECT cm.user FROM ConversationMember cm WHERE cm.conversation = ? ORDER BY cm.id ASC", this)
				.fetch();
	}

	public List<Long> getUserIds() {
		return em().createQuery("SELECT cm.user.id FROM ConversationMember cm WHERE cm.conversation = ?")
				.setParameter(1, this).getResultList();
	}

	public List<User> getOtherUsers(User user) {
		return User
				.find("SELECT cm.user FROM ConversationMember cm WHERE cm.conversation = ? AND cm.user != ? ORDER BY cm.id  ASC",
						this, user).fetch();
	}

	public ConversationMember getMemberObject(User user) {
		return ConversationMember.find("user = ? AND conversation = ?", user, this).first();
	}

	public long getActiveMemberCount() {
		return ConversationMember.count("conversation = ? AND status != ?", this, ConversationStatus.DELETED);
	}

	@Override
	protected void updateFollowers() {
		followers.clear();
		followers.addAll(getAdmins());
		followers.addAll(getUsers());
	}

	@Override
	protected void beforeSave() throws Exception {
		if (members.size() < 2) {
			throw new IllegalStateException("a conversation must have at least two members");
		}

		super.beforeSave();
	}

	public Date getLastPost() {
		if (lastPost == null) {
			return modified;
		}
		return lastPost;
	}

	private Date getLastPostCreatedDate() {
		if (isPersistent() && walls.size() > 0) {
			Post lastPost = Post.find("wall = ? ORDER BY id DESC", getDefaultWall()).first();
			if (lastPost != null) {
				return lastPost.created;
			}
		}
		return modified;
	}

	@Override
	public void _save() {
		List<User> checked = new ArrayList<User>();

		// remove duplicates & mark changed for all active users
		for (ConversationMember cm : new ArrayList<ConversationMember>(members)) {
			if (!checked.contains(cm.user)) {
				checked.add(cm.user);
			} else {
				members.remove(cm);
			}
		}

		lastPost = getLastPostCreatedDate();

		super._save();

		// assert walls are not in activity-stream
		// TODO : can be removed in 2.7 because new walls are now always created
		// with noActivityStream=true
		JPA.em().createQuery("UPDATE Wall w SET w.noActivityStream = true WHERE w.sender = ?").setParameter(1, this)
				.executeUpdate();

	}

	@Override
	public boolean checkSearchPermission() {
		// it is important to check if the user is really member
		// of the conversation. otherwise, superadmins could also
		// read this conversation if they are not part of it.
		ConversationMember cm = getMemberObject(UserLoader.getConnectedUser());
		return cm != null && !cm.isDeleted() && Security.check(Permission.ACCESS_CONVERSATION, this);
	}

	@Override
	public void redirectToResult() {
		// get member id of first user and render portal
		Long memberId = getMemberObject(getThumbnailUser()).id;
		Messaging.portal(memberId);
	}

	@Override
	public String getSearchInfoText() {
		return null;
	}
}
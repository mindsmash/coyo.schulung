package models.messaging;

import java.util.Arrays;
import java.util.List;

import play.i18n.Messages;

public enum ConversationStatus {

	INBOX("icon-inbox"), ARCHIVED("icon-folder-close"), INVISIBLE("icon-eye-close"), DELETED("icon-remove");

	public String icon;

	public String getLabel() {
		return Messages.get("messaging.conversation.status." + this);
	}

	private ConversationStatus(String icon) {
		this.icon = icon;
	}

	public static List<ConversationStatus> getDefault() {
		return Arrays.asList(INBOX, ARCHIVED);
	}
}

package models.messaging;

import models.Sender;
import models.User;
import play.i18n.Messages;

public enum ConversationEvent {
	JOIN, LEAVE;

	public String getLabel(User connectedUser, Sender affected) {
		String message = "messaging.event." + this.toString();
		if (connectedUser == affected) {
			message += ".you";
		}
		return Messages.get(message, affected.getDisplayName());
	}
}

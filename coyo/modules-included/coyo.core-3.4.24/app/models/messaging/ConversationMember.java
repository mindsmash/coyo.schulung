package models.messaging;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.TenantModel;
import models.User;
import models.wall.post.ConversationPost;
import play.cache.Cache;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPABase;
import theme.Theme;
import util.Util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@Entity
@Table(name = "conversation_member")
public class ConversationMember extends TenantModel {

	@ManyToOne(optional = false)
	public User user;

	@ManyToOne(optional = false)
	public Conversation conversation;

	// read / unread status
	public Date lastAccess;
	@Transient
	public boolean lastAccessChanged;

	// only show posts from this date on
	public Date showFrom = new Date();

	// active for chat
	public boolean active;

	@Column(nullable = false)
	public ConversationStatus status = ConversationStatus.INVISIBLE;

	public String getSubject() {
		String cacheKey = "conversation-subject-cm-" + id + "-" + conversation.modified.getTime();
		String subject = Cache.get(cacheKey, String.class);
		if (subject != null) {
			return subject;
		}

		List<String> names = new ArrayList<String>();
		for (ConversationMember cm : conversation.members) {
			if (!cm.isDeleted() && cm.user != user) {
				names.add(cm.user.getDisplayName());
			}
		}
		if (names.size() > 0) {
			subject = Util.implode(names, ", ");
		} else {
			subject = conversation.getOtherUsers(user).get(0).getDisplayName();
		}

		Cache.set(cacheKey, subject, "1d");
		return subject;
	}

	public boolean isArchived() {
		return status == ConversationStatus.ARCHIVED;
	}

	public boolean isDeleted() {
		return status == ConversationStatus.DELETED;
	}

	public boolean isUnread() {
		return lastAccess == null || lastAccess.before(conversation.lastPost);
	}

	public boolean updateActive() {
		// if this user is online
		if (user.isOnline()) {
			// and at least one other user is online
			for (ConversationMember cm : conversation.members) {
				if (cm != this && !cm.isDeleted() && cm.user.isOnline()) {
					active = true;
					break;
				}
			}
		}
		return active;
	}

	public Long getUnreadCount() {
		Date lastPost = conversation.getLastPost();
		String cacheKey = "conversation-unreadcount-cm-" + id + "-" + (lastPost == null ? 0 : lastPost.getTime()) + "-"
				+ (lastAccess == null ? 0 : lastAccess.getTime());
		Long count = Cache.get(cacheKey, Long.class);
		if (count != null) {
			return count;
		}

		JpqlSelect select = getPostsSelect(null, null, (lastAccess == null ? null : lastAccess.getTime()));
		select.andWhere("author != ?").param(user).andWhere("event IS NULL");
		count = ConversationPost.count(select.toString(), select.getParams().toArray());

		Cache.set(cacheKey, count, "1d");
		return count;
	}

	public List<ConversationPost> getUnreadPosts() {
		return getPostsQuery(null, null, (lastAccess == null ? null : lastAccess.getTime())).fetch();
	}

	public List<ConversationPost> getPosts() {
		return getPostsQuery(null, null, null).fetch();
	}

	public JPAQuery getPostsQuery(Long beforeId, Long afterId, Long modifiedAfter) {
		JpqlSelect select = getPostsSelect(beforeId, afterId, modifiedAfter);
		select.orderBy("id DESC");
		return ConversationPost.find(select.toString(), select.getParams().toArray());
	}

	public JpqlSelect getPostsSelect(Long beforeId, Long afterId, Long modifiedAfter) {
		JpqlSelect select = new JpqlSelect();
		select.where("wall = ?").param(conversation.getDefaultWall());
		if (showFrom != null) {
			// only posts after last deletion
			select.andWhere("modified >= ?").param(showFrom);
		}
		if (beforeId != null) {
			select.andWhere("id < ?").param(beforeId);
		}
		if (afterId != null) {
			select.andWhere("id > ?").param(afterId);
		}
		if (modifiedAfter != null) {
			select.andWhere("modified >= ?").param(new Date(modifiedAfter));
		}
		return select;
	}

	public String getThumbURL() {
		User thumbUser = null;

		// if only two active members
		if (conversation.getActiveMemberCount() == 2) {
			for (ConversationMember cm : conversation.members) {
				if (cm != this && !cm.isDeleted()) {
					thumbUser = cm.user;
					break;
				}
			}
		} else if (conversation.members.size() == 2) { // if only two members
														// total
			thumbUser = conversation.getOtherUsers(user).get(0);
		}

		if (thumbUser != null) {
			return thumbUser.getThumbURL();
		}

		return Theme.getResourceUrl("images/group-thumb.png");
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
		this.lastAccessChanged = true;
	}

	public <T extends JPABase> T softDelete() {
		status = ConversationStatus.DELETED;
		showFrom = new Date();
		return save();
	}

	public static class ConversationMemberJsonSerializer implements JsonSerializer<ConversationMember> {
		@Override
		public JsonElement serialize(ConversationMember cm, Type type, JsonSerializationContext context) {
			JsonObject o = new JsonObject();
			o.addProperty("id", cm.id);
			o.addProperty("status", cm.status.toString());
			o.addProperty("modified", cm.conversation.lastPost.getTime());
			o.addProperty("unreadCount", cm.getUnreadCount());
			o.addProperty("label", cm.getSubject());
			o.addProperty("avatar", cm.getThumbURL());
			return o;
		}
	}
}

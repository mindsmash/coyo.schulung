package models.workspace;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.TenantModel;
import models.User;
import play.cache.Cache;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import util.Util;
import acl.Permission;
import controllers.Security;

/**
 * 
 * Represents a List of Workspaces
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "workspace_category")
public class WorkspaceCategory extends TenantModel {

	@Required
	@MaxSize(25)
	@Unique
	@Column(nullable = false)
	public String name;

	public int priority = 9999;

	@OneToMany(mappedBy = "category")
	public List<Workspace> workspaces = new ArrayList<Workspace>();

	/**
	 * 
	 * Updates the priority of the workspace categories by a String in the
	 * format categoryid,categoryid,...,categoryid.
	 * 
	 * @param orderString
	 */
	public static void storeOrder(String orderString) {
		List<String> order = Util.parseList(orderString, ",");
		int priority = 0;
		for (String id : order) {
			WorkspaceCategory category = WorkspaceCategory.findById(Long.valueOf(id));
			if (category != null) {
				category.priority = priority++;
				category.save();
			}
		}
	}

	public List<Workspace> getWorkspaces(User user) {
		String cacheKey = getCacheKey(user);
		List<Workspace> r = Cache.get(cacheKey, List.class);

		if (r == null) {
			r = new ArrayList<Workspace>();
			for (Workspace workspace : workspaces) {
				if (Security.check(Permission.LIST_WORKSPACE, workspace.id) && !workspace.archived) {
					r.add(workspace);
				}
			}

			Cache.set(cacheKey, r, "5min");
		}
		return r;
	}

	public String getCacheKey(User user) {
		return "workspaceCategory-" + id + "-workspaces-" + user.id;
	}

	public static List<WorkspaceCategory> findAllSorted() {
		return find("ORDER BY priority ASC").fetch();
	}
}

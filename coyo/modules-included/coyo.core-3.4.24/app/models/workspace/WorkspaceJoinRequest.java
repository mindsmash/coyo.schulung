package models.workspace;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import models.TenantModel;
import models.User;
import models.notification.Notification;
import models.notification.WorkspaceDenyNotification;
import models.notification.WorkspaceJoinNotification;

/**
 * 
 * Represents a {@link User} request to join a closed workspace.
 * 
 * @author mindsmash GmbH
 * 
 */
@Entity
@Table(name = "workspace_join_request")
public class WorkspaceJoinRequest extends TenantModel {

	@ManyToOne
	public User user;

	@ManyToOne
	public Workspace workspace;

	public WorkspaceJoinRequest(Workspace workspace, User user) {
		this.workspace = workspace;
		this.user = user;
	}

	public void accept() {
		workspace.addMember(user, false);
		workspace.save();
		WorkspaceJoinNotification.raise(workspace, user);
		delete();
	}

	public void deny() {
		WorkspaceDenyNotification.raise(workspace, user);
		// save workspace to invalidate cache
		workspace.save();
		delete();
	}
}

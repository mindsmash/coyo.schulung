package models.workspace;

import models.TenantModel;
import models.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents a member of a workspace.
 *
 * @author mindsmash GmbH
 */
@Entity
@Table(name = "workspace_member")
public class WorkspaceMember extends TenantModel {

	@ManyToOne(optional = false)
	public Workspace workspace;
	@ManyToOne(optional = false)
	public User user;

	public boolean admin = false;

	@Override
	public void _save() {
		if (!workspace.isPersistent()) {
			throw new IllegalStateException("workspace is not persistent. save workspace before adding members");
		}

		super._save();

		// update followers
		workspace.save();
	}

	@Override
	public String toString() {
		return "WorkspaceMember[user=" + user.id + ", workspace=" + workspace.id + ", admin=" + admin + "]";
	}
}

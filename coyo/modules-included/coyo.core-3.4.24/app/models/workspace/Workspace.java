package models.workspace;

import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.CallContext;
import models.Sender;
import models.SenderType;
import models.User;
import models.app.App;
import models.app.WallApp;
import models.dao.WorkspaceDao;
import models.event.Event;
import models.notification.WorkspaceInvitationNotification;
import models.notification.WorkspaceNotification;
import models.wall.Wall;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.Logger;
import play.Play;
import play.data.binding.NoBinding;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.Crypto;
import play.mvc.Router;
import play.mvc.Scope.Session;
import play.templates.JavaExtensions;
import plugins.PluginEvents;
import plugins.PluginManager;
import session.UserLoader;
import util.Util;
import checks.WorkspaceNameCheck;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

/**
 * Represents a Workspace.
 *
 * @author mindsmash GmbH
 */
@Entity
@Indexed(boost = 4F)
@Searchable(category = "workspaces")
@Table(name = "workspace")
@SenderType("workspace")
@InjectionSupport
public class Workspace extends Sender implements CallContext {

	/**
	 * Simple function to transform a {@link WorkspaceMember} into a {@link User}.
	 */
	private final class MemberToUserFunction implements Function<WorkspaceMember, User> {
		public User apply(WorkspaceMember wm) {
			return wm.user;
		}
	}

	public enum WorkspaceVisibility {
		PUBLIC("globe"), CLOSED("lock"), PRIVATE("eye-close");

		public String icon;

		private WorkspaceVisibility(String icon) {
			this.icon = icon;
		}

		public String getLabel() {
			return Messages.get("workspace.visibility." + this + ".label");
		}

		public String getDescription() {
			return Messages.get("workspace.visibility." + this + ".description");
		}
	}

	public boolean archived = false;

	@Field
	@CheckWith(WorkspaceNameCheck.class)
	@Required
	@MaxSize(50)
	@Unique
	@Column(nullable = false)
	public String name;

	@ManyToOne
	public WorkspaceCategory category;

	@Column(nullable = false)
	public WorkspaceVisibility visibility = WorkspaceVisibility.PUBLIC;

	@NoBinding
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	public User creator;

	@NoBinding
	@OneToMany(mappedBy = "workspace", cascade = CascadeType.ALL)
	@OrderBy("admin DESC")
	public List<WorkspaceMember> members = new ArrayList<WorkspaceMember>();

	@NoBinding
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workspace")
	public List<WorkspaceNotification> notifications = new ArrayList<WorkspaceNotification>();

	@NoBinding
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workspace")
	public List<WorkspaceJoinRequest> joinRequests = new ArrayList<WorkspaceJoinRequest>();

	@NoBinding
	@MaxSize(255)
	public String externalShareToken;

	@NoBinding
	@MaxSize(255)
	public String externalSharePassword;

	@NoBinding
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	public DateTime externalShareTokenExpiryDate;

	@Inject(configuration = "coyo.di.dao.workspace", defaultClass = WorkspaceDao.class, singleton = true)
	private static WorkspaceDao workspaceDao;

	public Workspace() {
	}

	public Workspace(User creator) {
		setCreator(creator);
	}

	/**
	 * Sets the creator and adds him as admin. Use this on unsaved workspaces.
	 */
	public void setCreator(User user) {
		if (isPersistent()) {
			Logger.warn("setting creator on persistent workspace [%s] not allowed", this);
			return;
		}

		creator = user;

		WorkspaceMember wm = new WorkspaceMember();
		wm.admin = true;
		wm.workspace = this;
		wm.user = user;
		members.add(wm);

	}

	public void setExternalSharePassword(String sharePassword) {
		if (sharePassword == null) {
			this.externalSharePassword = null;
			return;
		}
		this.externalSharePassword = Crypto.passwordHash(sharePassword);
	}

	public void activateExternalSharing(String password) {
		// generate random token
		externalShareToken = Codec.UUID().replaceAll("-", "");
		setExternalSharePassword(password);
		externalShareTokenExpiryDate = calculateExternalShareTokenExpiryDate();
	}

	public void deactivateExternalSharing() {
		externalShareToken = null;
		externalSharePassword = null;
		externalShareTokenExpiryDate = null;
	}

	public boolean hasValidExternalShareToken() {
		return externalShareToken != null && externalShareTokenExpiryDate != null
				&& externalShareTokenExpiryDate.isAfterNow();
	}

	public DateTime getExternalShareTokenExpiryDate(DateTimeZone dateTimeZone) {
		return externalShareTokenExpiryDate == null ? null : externalShareTokenExpiryDate.withZone(dateTimeZone);
	}

	private DateTime calculateExternalShareTokenExpiryDate() {
		return new DateTime(DateTimeZone.UTC).plusHours(Integer.valueOf(Play.configuration.getProperty(
				"workspace.externalShareToken.expiresAfter", "24")));
	}

	@Override
	public Wall getDefaultWall() {
		for (App app : apps) {
			if (app instanceof WallApp) {
				return ((WallApp) app).wall;
			}
		}
		return null;
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	public boolean isMember(User user) {
		if (!isPersistent()) {
			throw new IllegalAccessError("workspace is not persistent");
		}
		return WorkspaceMember.count("workspace = ? AND user = ?", this, user) > 0;
	}

	public boolean isAdmin(User user) {
		if (!isPersistent()) {
			throw new IllegalAccessError("workspace is not persistent");
		}
		return WorkspaceMember.count("workspace = ? AND user = ? AND admin = true", this, user) > 0;
	}

	public List<User> getUserMembers(Integer limit) {
		// implementation for non-persistent workspaces
		if (!isPersistent()) {
			// Creating copy to not sort original list..
			List<WorkspaceMember> memberList = new ArrayList<>(members);

			// sort (admins first) + transform to user list
			Collections.sort(memberList, new WorkspaceMemberAdminComparator());
			List<User> users = Lists.transform(memberList, new MemberToUserFunction());

			// limit returned list size
			return null != limit ? users.subList(0, limit) : users;

		} else {
			return workspaceDao.getUserMember(this, limit);
		}
	}

	public List<User> getUserMembers() {
		return getUserMembers(null);
	}

	public List<User> getAdmins() {
		// implementation for non-persistent workspaces
		if (!isPersistent()) {
			return getAdminsFromList();
		}

		// implementation for persistent workspaces
		return User
				.find("SELECT wm.user FROM WorkspaceMember wm WHERE wm.workspace = ? AND wm.user.active = true AND wm.admin = true ORDER BY wm.user.lastName, wm.user.firstName ASC",
						this).fetch();
	}

	public List<User> getAdminsFromList() {
		List<User> admins = new ArrayList<User>();
		for (WorkspaceMember member : members) {
			if (member.admin) {
				admins.add(member.user);
			}
		}
		return admins;
	}

	public long getMemberCount() {
		// implementation for non-persistent workspaces
		if (!isPersistent()) {
			return members.size();
		}

		// implementation for persistent workspaces
		return WorkspaceMember.count("workspace = ? AND user.active = true", this);
	}

	/**
	 * Adds a user as {@link WorkspaceMember} to a workspace. Be careful when using this method in bulk operations,
	 * since the workspace will be saved, followers will be updated and notifications will be send. This method can be
	 * very slow.
	 */
	public WorkspaceMember addMember(User user, boolean admin) {
		WorkspaceMember member = getMember(user);
		if (member == null) {
			member = new WorkspaceMember();
			member.workspace = this;
			member.user = user;
			member.admin = admin;

			User connectedUser = UserLoader.getConnectedUser(false);
			if ((connectedUser == null) || !user.equals(connectedUser)) {
				WorkspaceInvitationNotification.raise(this, user);
			}
		} else {
			member.admin = admin;
		}
		member.save();
		refresh();
		return member;
	}

	public WorkspaceMember getMember(User user) {
		if (user != null) {
			for (WorkspaceMember member : members) {
				if (member.user == user) {
					return member;
				}
			}
		}
		return null;
	}

	@Override
	public boolean isActive() {
		return super.isActive() && !archived;
	}

	public List<Event> getEvents() {
		return Event.find("reference = ?", this).fetch();
	}

	public boolean hasRequestedToJoin(User user) {
		return WorkspaceJoinRequest.count("workspace = ? AND user = ?", this, user) > 0;
	}

	@Override
	public boolean isSharable() {
		return visibility == WorkspaceVisibility.PUBLIC;
	}

	@Override
	public void _save() {
		// auto-remove duplicate members
		List<Long> checked = new ArrayList<Long>();
		for (WorkspaceMember member : new ArrayList<WorkspaceMember>(members)) {
			if (checked.contains(member.user.id)) {
				members.remove(member);
				member.delete();
			} else {
				checked.add(member.user.id);
			}
		}

		// plugins
		PluginManager.raiseEvent(PluginEvents.WORKSPACE_SAVE, this);

		super._save();
	}

	@Override
	protected void updateFollowers() {
		followers.clear();
		followers.addAll(Lists.transform(members, new MemberToUserFunction()));
	}

	@Override
	protected void beforeSave() throws Exception {
		// zero-members constraint
		if (members.size() == 0) {
			throw new IllegalStateException("a workspace cannot be without members");
		}

		// zero-admin constraint
		if (getAdminsFromList().size() == 0) {
			// make first active user admin
			WorkspaceMember wm = null;
			for (WorkspaceMember member : members) {
				if (!member.user.external && member.user.active) {
					wm = member;
					break;
				}
			}

			if (wm != null) {
				wm.admin = true;
				wm.willBeSaved = true;
			} else {
				throw new IllegalStateException("a workspace cannot be without an admin");
			}
		}

		super.beforeSave();
	}

	public static List<Workspace> findActive() {
		return find("archived = false ORDER BY name ASC").fetch();
	}

	public static List<Workspace> findArchived() {
		return find("archived = true ORDER BY name ASC").fetch();
	}

	/**
	 * Counts the workspaces that the user can see (list).
	 *
	 * @param user
	 * @return Number of workspaces.
	 */
	public static long count(User user) {
		if (user.superadminMode) {
			return count("archived = false");
		}
		return count(
				"SELECT COUNT(w) FROM Workspace w WHERE w.archived = false AND (w.visibility != ? OR EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace.id = w.id AND wm.user.id = ?))",
				WorkspaceVisibility.PRIVATE, user.id);
	}

	public static class WorkspaceActivitySorter implements Comparator<Workspace> {

		private static final String SESSION_KEY = "workspaceActivity";
		private List<Workspace> workspaces;

		public WorkspaceActivitySorter() {
			this.workspaces = load();
		}

		public static boolean isValidState() {
			return Session.current() != null;
		}

		public static void activate(Workspace ws) {
			if (Session.current() != null) {
				List<Workspace> workspaces = load();
				workspaces.remove(ws);
				workspaces.add(0, ws);
				store(workspaces);
			}
		}

		private static List<Workspace> load() {
			List<Workspace> r = new ArrayList<Workspace>();
			if (Session.current() != null) {
				String ids = Session.current().get(SESSION_KEY);
				if (!StringUtils.isEmpty(ids)) {
					List<String> idList = Util.parseList(ids, ",");
					for (String id : idList) {
						try {
							Workspace ws = Workspace.findById(Long.parseLong(id));
							if (ws != null) {
								r.add(ws);
							}
						} catch (EntityNotFoundException ignored) {
						}
					}
				}
			}
			return r;
		}

		private static void store(List<Workspace> workspaces) {
			if (Session.current() != null) {
				List<Long> idList = new ArrayList<Long>();
				for (Workspace ws : workspaces) {
					idList.add(ws.id);
				}
				Session.current().put(SESSION_KEY, Util.implode(idList, ","));
			}
		}

		@Override
		public int compare(Workspace o1, Workspace o2) {
			if (workspaces.contains(o1)) {
				if (workspaces.contains(o2)) {
					return workspaces.indexOf(o1) - workspaces.indexOf(o2);
				} else {
					return -1;
				}
			} else if (workspaces.contains(o2)) {
				return 1;
			}
			return o1.compareTo(o2);
		}
	}

	@Override
	public String toString() {
		return "Workspace[id=" + id + ", name=" + getDisplayName() + "]";
	}

	@Override
	public String getSearchInfoText() {
		long memberCount = getMemberCount();
		return memberCount + " " + Messages.get("member" + JavaExtensions.pluralize(memberCount));
	}

	private class WorkspaceMemberAdminComparator implements Comparator<WorkspaceMember> {
		@Override
		public int compare(WorkspaceMember o1, WorkspaceMember o2) {
			if ((o1.admin && o2.admin || !o1.admin && !o2.admin)) {
				return 0;
			} else if (o1.admin && !o2.admin) {
				return -1;
			} else if (!o1.admin && o2.admin) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean canHaveApps() {
		return true;
	}

	@Override
	public String getVisibilityIcon() {
		return visibility.icon;
	}

	@Override
	public String getVisibilityText() {
		return visibility.getLabel();
	}

	@Override
	public String getURL(Long appId) {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("slug", getSlug());

		if (appId != null) {
			App app = App.findById(appId);
			if (app != null && app.sender == this) {
				args.put("appId", appId);
				args.put("appSlug", app.getSlug());
			}
		}

		return Router.reverse("Workspaces.show", args).url;
	}

	@Override
	public String getURL() {
		Map<String, Object> args = new HashMap<>();
		args.put("id", id);
		args.put("slug", getSlug());

		return Router.reverse("Workspaces.show", args).url;
	}

	@Override
	public String getTitle() {
		return getDisplayName();
	}
}

package models;

import play.data.validation.Max;
import play.data.validation.MaxSize;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "app_forms_field_option")
public class FormFieldOption extends TenantModel implements Comparable<FormFieldOption> {

	@ManyToOne
	public FormField field;

	public int fieldOrder;
	@MaxSize(255)

	public String label;
	@MaxSize(255)

	public String value;

	@OneToMany
	public int compareTo(FormFieldOption f) {
		if (this.fieldOrder == f.fieldOrder) {
			return 0;
		}

		return this.fieldOrder - f.fieldOrder;
	};

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[id=" + this.id + ", field=" + this.field + ", label=" + label + "]";
	}
}

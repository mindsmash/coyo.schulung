package models;

import fishr.search.ContentIndexable;

public interface FileAttachment extends Previewable, ContentIndexable {

	public Long getId();
	
	public Long getSize();
	
}

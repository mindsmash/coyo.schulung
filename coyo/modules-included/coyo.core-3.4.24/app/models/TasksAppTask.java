package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import models.comments.Comment;
import models.comments.Commentable;
import models.helper.ClearManyToMany;
import models.notification.TaskNotification;
import models.page.Page;
import models.workspace.Workspace;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.JPA;
import fishr.Field;
import fishr.Indexed;
import fishr.search.Searchable;

@Indexed
@Entity
@Searchable(category = "tasks")
@Table(name = "app_tasks_task")
public class TasksAppTask extends ExtendedTenantModel implements Commentable, Likable, ExtendedSearchableModel {

	private static final String TASK_USER_TABLE = "app_tasks_task_user";
	private static final String TASK_LIKE_TABLE = "app_tasks_task_like";

	@ManyToOne
	public TasksAppList list;

	@Field
	@ManyToOne(optional = false)
	public TasksApp app;

	@ManyToMany
	@JoinTable(name = TASK_USER_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> users = new ArrayList<User>();

	@Required
	@Field
	@MaxSize(255)
	public String name;
	public Date due;
	public int priority = 9999;
	public boolean completed = false;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("created ASC")
	@JoinTable(name = "app_tasks_task_comment")
	public List<Comment> comments = new ArrayList<Comment>();

	@ManyToMany
	@JoinTable(name = TASK_LIKE_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> likes = new ArrayList<User>();

	@OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
	public List<TaskNotification> notifications = new ArrayList<TaskNotification>();

	public boolean isOverdue() {
		if (due == null) {
			return false;
		}
		return due.before(new Date());
	}

	@Override
	public void addComment(Comment comment) {
		comments.add(comment);
		save();
	}

	@Override
	public void removeComment(Comment comment) {
		comments.remove(comment);
		save();
	}

	@Override
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public void like(User user) {
		likes.add(user);
		save();
	}

	@Override
	public void unlike(User user) {
		likes.remove(user);
		save();
	}

	@Override
	public boolean likes(User user) {
		return likes.contains(user);
	}

	@Override
	public List<User> getLikes() {
		return likes;
	}

	@Override public Collection<User> getInterestedUsers() {
		return app.sender.followers;
	}

	public void setAssignee(User assignee) {
		this.users.clear();
		this.users.add(assignee);
	}

	public User getAssignee() {
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + TASK_USER_TABLE + " WHERE app_tasks_task_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}

	@Override
	public boolean checkSearchPermission() {
		return !completed && app.checkSearchPermission();
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	@Override
	public void redirectToResult() {
		app.redirectToResult();
	}

	public List<User> getAssignableUsers() {
		Sender sender = app.sender;
		if (sender instanceof Page && ((Page) sender).visibility.hasMembers()) {
			return ((Page) sender).getAllMembers();
		} else if (sender instanceof Workspace) {
			return ((Workspace) sender).getUserMembers();
		}
		return null;
	}

	@Override
	public boolean checkLikePermission(User user) {
		return true;
	}

	@Override
	public boolean checkCreateCommentPermission(User user) {
		return true;
	}

	@Override
	public boolean checkDeleteCommentPermission(Comment comment, User user) {
		return false;
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public Sender getSearchThumbSender() {
		return app.sender;
	}

	@Override
	public String getSearchInfoText() {
		return app.getSearchInfoText();
	}

	@Override
	public Sender getSender() {
		return this.app.sender;
	}
}

package models.teaser;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import controllers.Uploads;
import controllers.Uploads.TempUpload;
import models.Application;
import models.TenantModel;
import models.User;
import play.data.validation.MaxSize;
import play.data.validation.MinSize;
import play.data.validation.Required;
import storage.FlexibleBlob;
import utils.ImageUtils;
import validation.SimpleURLWithRelative;

@Entity
@Table(name = TeaserItem.TEASER_TABLENAME)
public class TeaserItem extends TenantModel {

	public static final String TEASER_TABLENAME = "teaser_item";
	
	public boolean active = true;
	
	public int priority = 9999;
	
	@ManyToOne(optional = false)
	public User author;
	
	@Required
	public String title;
	
	@Lob
	public String description;
	
	@SimpleURLWithRelative
	public String link;
	
	public boolean externalLink;
	
	@Required
	public FlexibleBlob image;
	
	@Transient
	public String[] newImageUUID;
	
	public void setNewImageUUID(String[] newImageUUID) {
		if((newImageUUID != null) && (newImageUUID.length > 0)) {
			TempUpload upload = Uploads.loadUpload(newImageUUID[0]);
			if(upload != null) {
				image = upload.blob;
			}
		}
	}
	
	@Transient
	public String toString() {
		return "TeaserItem[" +
				"active: " + active +
				", priority: " + priority +
				", author: " + author +
				", title: " + title +
				", description: " + description +
				", link: " + link +
				", external link: " + externalLink +
				", image: " + image +
				"]";
	}
	
}
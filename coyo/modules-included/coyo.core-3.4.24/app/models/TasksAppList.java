package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import validation.ColorCode;

@Entity
@Table(name = "app_tasks_list")
public class TasksAppList extends TenantModel {

	@ManyToOne(optional = false)
	public TasksApp app;

	@OneToMany(mappedBy = "list", cascade = CascadeType.ALL)
	@OrderBy("priority")
	public List<TasksAppTask> tasks = new ArrayList<TasksAppTask>();

	public List<TasksAppTask> getTasks(boolean completed) {
		return TasksAppTask.find("list = ? AND completed = ? ORDER BY priority ASC", this, completed).fetch();
	}

	@Required
	@MaxSize(255)

	public String name;

	@ColorCode
	@MaxSize(255)

	public String color;

	public int priority = 9999;
}

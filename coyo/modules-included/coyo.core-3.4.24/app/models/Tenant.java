package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.TypedQuery;

import models.embeddables.Link;
import models.event.Event;
import models.event.EventSeries;
import models.media.MediaRoom;
import models.messaging.Conversation;
import models.notification.Notification;
import models.page.Page;
import models.teaser.TeaserItem;
import models.wall.post.NotificationPost;
import models.workspace.Workspace;
import onlinestatus.OnlineStatusServiceFactory;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;

/**
 * Main tenant class.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
@Entity
public class Tenant extends AbstractTenant {

	@Lob
	public String license;

	public boolean active = true;

	@Embedded
	public StorageQuota quota;

	@ElementCollection
	@CollectionTable(name = "tenant_property")
	@Column(length = 2000)
	public Map<String, String> properties = new HashMap<String, String>();

	public StorageQuota getQuota() {
		if (quota == null) {
			quota = new StorageQuota();
		}
		return quota;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * Deletes an entire tenant. This is rather complex due to complex foreign key contraints and cross-db dependencies.
	 * During the process, almost all entities will be fetched from the DB to delete them one-by-one to ensure that all
	 * cascades are applied correctly. Therefore, the process of deleting a tenant might take very long and should not
	 * be done blocking.
	 */
	@Override
	public <T extends JPABase> T delete() {
		long start = System.currentTimeMillis();
		Logger.info("starting tenant [%s] deletion", id);

		((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
		((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);

		active = false;
		save();

		// delete all notifications
		UserNotification.delete("DELETE FROM UserNotification un WHERE un.tenant = ?", this);
		List<NotificationPost> notificationPosts = NotificationPost.find("tenant = ?", this).fetch();
		deleteAll(notificationPosts);
		List<Notification> notifications = Notification.find("tenant = ?", this).fetch();
		deleteAll(notifications);

		// delete embeddable entities
		List<Link> embeddableLinks = Link.find("tenant = ?", this).fetch();
		deleteAll(embeddableLinks);

		// delete all senders in the correct order
		List<Conversation> conversations = Conversation.find("tenant = ?", this).fetch();
		deleteAll(conversations);
		List<Page> pages = Page.find("tenant = ?", this).fetch();
		deleteAll(pages);
		List<EventSeries> eventSeries = EventSeries.find("tenant = ?", this).fetch();
		deleteAll(eventSeries);
		List<Event> events = Event.find("tenant = ?", this).fetch();
		deleteAll(events);
		List<Workspace> workspaces = Workspace.find("tenant = ?", this).fetch();
		deleteAll(workspaces);

		// delete media rooms before users are deleted to avoid cross-user room-creator constraints
		List<MediaRoom> mediaRooms = MediaRoom.find("tenant = ?", this).fetch();
		deleteAll(mediaRooms);

		// delete teaser items before users are deleted to avoid cross-user room-creator constraints
		final List<TeaserItem> teaserItems = TeaserItem.find("tenant = ?" , this).fetch();
		deleteAll(teaserItems);

		List<Call> calls = Call.find("tenant = ?", this).fetch();
		deleteAll(calls);
		List<User> users = User.find("tenant = ?", this).fetch();
		deleteAll(users);
		List<Application> applications = Application.find("tenant = ?", this).fetch();
		deleteAll(applications);

		// delete senders avoiding loading all sender objects at once
		final TypedQuery<Long> query = em().createQuery("SELECT s.id FROM Sender AS s WHERE tenant = :tenant",
				Long.class);
		query.setParameter("tenant", this);
		final List<Long> senderIds = query.getResultList();

		for (Long senderId : senderIds) {
			final Sender sender = Sender.findById(senderId);
			if (sender != null) {
				sender._delete();
				em().flush();
			}
		}

		// delete all remaining entities
		List<Class> classes = Play.classloader.getAssignableClasses(TenantModel.class);
		for (Class clazz : classes) {
			if (clazz.isAnnotationPresent(Entity.class)) {
				List<TenantModel> data = em()
						.createQuery("SELECT e FROM " + clazz.getSimpleName() + " e WHERE e.tenant = ?")
						.setParameter(1, this).getResultList();
				deleteAll(data);
			}
		}

		// remove onLineStatusService for deleted tenant
		OnlineStatusServiceFactory.removeOnlineStatusInstance(this);

		Logger.info("finished tenant [%s] deletion. took %s ms", id, (System.currentTimeMillis() - start));

		return super.delete();
	}

	private void deleteAll(List<? extends TenantModel> list) {
		for (TenantModel m : list) {
			Logger.debug("deleting entity: %s", m);
			m._delete();
		}
		em().flush();
	}
}

package models.helper;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import models.TenantModel;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;

/**
 * We are using single-sided {@link ManyToMany} relations, which cause foreign
 * key constraints when certain entities are deleted. This helper helps to
 * delete these orphan {@link ManyToMany} relations. You can use this helper
 * before you are deleting an entity.
 * 
 * @author mindsmash GmbH
 */
public class ClearManyToManyHelper {

	private static Set<Field> fields;

	private ClearManyToManyHelper() {
	}

	public static void clearAll(TenantModel model) {
		Logger.debug("[ClearManyToManyHelper] start clearing of many-to-many relations for entity %s", model);

		for (Field field : getFields(model.getClass())) {
			JoinTable jt = field.getAnnotation(JoinTable.class);

			Logger.debug("[ClearManyToManyHelper] clearing many-to-many entries in table %s for entity %s", jt.name(),
					model);
			JPA.em().createNativeQuery("DELETE FROM " + jt.name() + " WHERE " + field.getName() + "_id = " + model.id)
					.executeUpdate();
		}
	}

	private static Set<Field> getFields(Class clazz) {
		Set<Field> fields = new HashSet<>();
		for (Field field : getAllFields()) {
			if (field.getAnnotation(ClearManyToMany.class).appliesFor().isAssignableFrom(clazz)) {
				fields.add(field);
			}
		}
		return fields;
	}

	private static Set<Field> getAllFields() {
		if (fields == null) {
			fields = new HashSet<>();
			List<Class> classes = Play.classloader.getAssignableClasses(TenantModel.class);
			for (Class clazz : classes) {
				for (Field field : clazz.getFields()) {
					if (field.isAnnotationPresent(ClearManyToMany.class)) {
						if (!Collection.class.isAssignableFrom(field.getType())) {
							Logger.warn(
									"field [%s] of class [%s] is marked for clearing many-to-many relations but is not a collection.",
									field, clazz);
							continue;
						}
						if (!field.isAnnotationPresent(JoinTable.class)) {
							Logger.warn(
									"field [%s] of class [%s] is marked for clearing many-to-many relations but no JoinTable annotation is present.",
									field, clazz);
							continue;
						}

						fields.add(field);
					}
				}
			}
		}
		return fields;
	}
}

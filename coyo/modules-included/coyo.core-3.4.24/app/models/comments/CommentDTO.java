package models.comments;

import java.util.List;

public class CommentDTO {
	public Long id;
	public String text;
	public List<String> attachments;
}

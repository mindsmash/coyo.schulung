package models.comments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import libs.DateI18N;
import models.BaseModel;
import models.ExtendedTenantModel;
import models.Likable;
import models.Sender;
import models.User;
import models.app.BlogAppArticle;
import models.app.WikiAppArticle;
import models.helper.ClearManyToMany;
import models.media.Media;
import models.statistics.TrackingRecord;
import models.wall.post.Post;
import play.data.Upload;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.utils.HTML;
import storage.FlexibleBlob;
import utils.Hashifier;
import utils.Linkifier;
import utils.Smilifier;
import acl.Permission;
import controllers.Posts;
import controllers.Security;
import extensions.CommonExtensions;
import fishr.Field;
import fishr.Fishr;
import fishr.util.ConvertionUtils;

/**
 * Model for post comments.
 *
 * @author Marko Ilic
 */
@Entity
@Table(name = "comment")
public class Comment extends ExtendedTenantModel implements Likable {

	private static final String LIKE_TABLE = "comment_like";

	@ManyToOne(optional = false)
	// TODO : change to Sender
	public User author;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "comment", fetch = FetchType.LAZY)
	public List<CommentFileAttachment> attachments = new ArrayList<>();

	@Lob
	@Field
	public String text;

	@ManyToMany
	@JoinTable(name = LIKE_TABLE)
	@ClearManyToMany(appliesFor = User.class)
	public List<User> likes = new ArrayList<User>();

	@Transient
	public Commentable source;

	/**
	 * This constructor only exists to support Play's auto-binding (which requires a simple no-args constructor).
	 */
	@Deprecated
	public Comment() {
	}

	public Comment(Commentable source) {
		this.source = source;
	}

	public String getLinkifiedText() {
		String escapedHtml = HTML.htmlEscape(text);
		escapedHtml = Linkifier.linkify(escapedHtml);
		escapedHtml = Smilifier.smilify(escapedHtml);
		if (Fishr.HASHTAGS) {
			escapedHtml = Hashifier.hashify(escapedHtml);
		}

		return escapedHtml;
	}

	public void like(User user) {
		if (!likes(user)) {
			likes.add(user);
			save();
		}
	}

	@Override
	public Collection<User> getInterestedUsers() {
		// if comment is attached to a post
		Post post = getPost();
		if (post != null) {
			return post.getInterestedUsers();
		}

		return author.followers;
	}

	public void unlike(User user) {
		if (likes(user)) {
			likes.remove(user);
			save();
		}
	}

	public boolean likes(User user) {
		return likes.contains(user);
	}

	public List<User> getLikes() {
		return likes;
	}

	@Override
	public BaseModel getModel() {
		return this;
	}

	// TODO: hack: get post of comment or null

	/**
	 * @return The post that this comment is attached to or NULL if this comment is attached to some other entity.
	 */
	public Post getPost() {
		return Post.find("SELECT p FROM Post p WHERE ? MEMBER OF p.comments", this).first();
	}

	@Override
	public boolean checkLikePermission(User user) {
		// hack: if post, then check for post
		Post post = getPost();
		if (post != null) {
			return post.checkLikePermission(user);
		}

		return true;
	}

	public String getSearchInfo() {
		return text;
	}

	public static List<String> getSearchInfo(List<Comment> comments) {
		List<String> r = new ArrayList<>();
		for (Comment comment : comments) {
			r.add(comment.getSearchInfo());
		}
		return r;
	}

	@Override
	protected void beforeDelete() throws Exception {
		// clear manyToMany
		JPA.em().createNativeQuery("DELETE FROM " + LIKE_TABLE + " WHERE comment_id = " + id).executeUpdate();

		// TODO: comments should not know about the entities that are using them
		JPA.em().createNativeQuery("DELETE FROM " + Post.POST_COMMENT_TABLE + " WHERE comments_id = " + id)
				.executeUpdate();

		// TODO: comments should not know about the entities that are using them
		JPA.em().createNativeQuery("DELETE FROM " + Media.MEDIA_COMMENT_TABLE + " WHERE comments_id = " + id)
				.executeUpdate();

		// TODO : comment model must not depend on an app
		JPA.em().createNativeQuery("DELETE FROM " + BlogAppArticle.BLOG_COMMENT_TABLE + " WHERE comments_id = " + id)
				.executeUpdate();

		// TODO : comment model must not depend on an app
		JPA.em().createNativeQuery("DELETE FROM " + WikiAppArticle.WIKI_COMMENT_TABLE + " WHERE comments_id = " + id)
				.executeUpdate();

		super.beforeDelete();
	}

	@Override
	@PostPersist
	protected void afterInsert() throws Exception {
		TrackingRecord.trackOne("comments");

		if (source != null && source instanceof ExtendedTenantModel) {
			((ExtendedTenantModel) source).raiseHashtagNotifications(ConvertionUtils.extractHashtags(this));
		}

		super.afterInsert();
	}

	@Override
	public void redirectToResult() {
		Posts.show(getPost().getId());
	}

	@Override
	public String getDisplayName() {
		return CommonExtensions.shorten(text, 100, "...");
	}

	@Override
	public Sender getSearchThumbSender() {
		return author;
	}

	@Override
	public String getSearchInfoText() {
		Post post = getPost();
		String by = CommonExtensions.format(created, DateI18N.getFullFormat()) + " "
				+ Messages.get("byName", author.getDisplayName());
		if (post != null && author != post.author) {
			return by + " ›› " + post.author.getDisplayName();
		}
		return by;
	}

	@Override
	public boolean checkSearchPermission() {
		return Security.check(Permission.READ_POST, getPost());
	}

	/**
	 * Adds uploaded files to the comment
	 *
	 * @param uploads
	 * @return
	 */
	public void addAttachments(final List<Upload> uploads) {
		if (uploads != null) {
			for (Object o : uploads) {
				if (o != null && o instanceof Upload) {
					Upload upload = (Upload) o;
					if (upload.getSize().longValue() > 0L) {
						final FlexibleBlob blob = new FlexibleBlob();
						blob.set(upload.asStream(), upload.getContentType(), upload.getSize());

						final CommentFileAttachment fileAttachment = new CommentFileAttachment();
						fileAttachment.file = blob;
						fileAttachment.name = upload.getFileName();
						fileAttachment.comment = this;
						this.attachments.add(fileAttachment);
					}
				}
			}
			this.save();
		}
	}
}

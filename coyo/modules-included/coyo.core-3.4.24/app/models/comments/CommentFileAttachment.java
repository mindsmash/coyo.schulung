package models.comments;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.common.collect.ImmutableMap;
import models.FileAttachment;
import models.TenantModel;
import models.wall.post.Post;
import org.slf4j.LoggerFactory;
import play.Logger;
import play.data.validation.MaxSize;
import play.mvc.Router;
import storage.FlexibleBlob;
import util.ModelUtil;

@Entity
@Table(name = "comment_file_attachment")
public class CommentFileAttachment extends TenantModel implements FileAttachment {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CommentFileAttachment.class);

	@MaxSize(255)
	public String name;

	public FlexibleBlob file;

	@ManyToOne(optional = false)
	public Comment comment;

	@Transient
	public Commentable source;

	@Override
	public String getFileName() {
		return name;
	}

	@Override
	public FlexibleBlob getFileData() {
		return file;
	}

	@Override
	public String getContentType() {
		return file.type();
	}

	@Override
	public Long getParentSenderId() {
		if (source != null && source.getSender() != null) {
			return source.getSender().id;
		} else {
			Post post = comment.getPost();
			if (post != null) {
				return post.getSender().getId();
			}
		}

		return null;
	}

	@Override
	public String getDisplayName() {
		return name;
	}

	@Override
	public String getDownloadUrl() {
		if(source == null) {
			throw new IllegalStateException("Source (commentable) is not set but required");
		}

		final Map<String, Object> args = ImmutableMap.<String, Object> builder()
				.put("id", id)
				.put("commentableUid", ModelUtil.serialize(source.getModel()))
				.build();

		return Router.reverse("api.CommentsAPI.fileAttachment", args).url;
	}

	@Override
	public String getUniqueId() {
		try (InputStream is = file.get()) {
			if (is == null) {
				throw new IllegalStateException("Failed to get InputStream for CommentFileAttachment id: " + this.id);
			} else {
				return org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
			}
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			return "commentFileAttachment-" + id + name;
		}
	}

	@Override
	public Long getSize() {
		return file.length();
	}

	@Override
	public void _delete() {
		file.delete();
		super._delete();
	}

	@Override
	public InputStream get() {
		return file.get();
	}

	@Override
	public long length() {
		return file.length();
	}

	@Override
	public String type() {
		return file.type();
	}
}

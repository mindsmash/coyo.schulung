package models.comments;

import models.BaseModel;
import models.Sender;
import models.User;
import models.comments.Comment;

import java.util.List;

/**
 * 
 * Any class can be commentable by implementing this interface.
 * 
 * @author mindsmash GmbH
 * 
 */
public interface Commentable {

	public void addComment(Comment comment);

	public void removeComment(Comment comment);

	public List<Comment> getComments();

	public Long getId();

	public BaseModel getModel();

	/**
	 * Returns the sender of the commentable. This is needed to determine the users, that can be mentioned in the
	 * comments. The method is allowed to return 'null'.
	 */
	public Sender getSender();

	/**
	 * @param user
	 * @return True if user can create comment for this entity.
	 */
	public boolean checkCreateCommentPermission(User user);

	/**
	 * Check if comment can be deleted by user. The permission checker will always allow comment authors to delete their
	 * comments. Additional checks can be done here.
	 * 
	 * @param comment
	 * @param user
	 * @return True if user can delete comment. False otherwise. Authorship is already checked.
	 */
	public boolean checkDeleteCommentPermission(Comment comment, User user);
}

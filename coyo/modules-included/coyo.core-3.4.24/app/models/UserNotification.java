package models;

import events.type.NewUserNotificationEvent;
import models.notification.Notification;
import org.hibernate.annotations.Index;
import play.Logger;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 *
 * Maps users to notifications including a read flag.
 * 
 * @author Jan Marquardt
 */
@Entity
@javax.persistence.Table(name = UserNotification.TABLE)
@org.hibernate.annotations.Table(appliesTo = UserNotification.TABLE, indexes = {
		@Index(name = "idx_tenant_user_read", columnNames = { "tenant_id", "user_id", "isRead" }) })
public class UserNotification extends TenantModel {

	public static final String TABLE = "user_notification";

	@ManyToOne(optional = false)
	public User user;
	
	@ManyToOne(optional = false)
	public Notification notification;
	
	public boolean isRead = false;

	/**
	 * Date when the event mapped to this {@link UserNotification} occured. Same date as when this {@link UserNotification}
	 * was distributed to the {@link User}
	 */
	private Date eventDate;
	
	public UserNotification(User user, Notification notification) {
		this.user = user;
		this.notification = notification;
	}

	@Override
	protected void afterInsert() throws Exception {
		super.afterInsert();

		Logger.debug("[UserNotification] Informing user [%s] about notification [%s]", user, notification);

		// live event in frontend
		new NewUserNotificationEvent(this).raise(this.user);
	}

	/**
	 * Creates the text for this user notification
	 * @return
	 */
	public String getText() {
		if (this.notification != null) {
			return this.notification.getText(this);
		} else {
			return null;
		}
	}

	public Date getEventDate() {
		return eventDate != null ? eventDate : notification.modified;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
}

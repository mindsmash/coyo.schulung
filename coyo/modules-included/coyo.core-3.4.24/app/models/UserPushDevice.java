package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Required;

import com.mindsmash.pns.client.Device;

/**
 * A device that push messages can be sent to.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Entity
@Table(name = "user_pushdevice")
public class UserPushDevice extends TenantModel {

	@Required
	@ManyToOne(optional = false)
	public User user;

	public String name;

	public boolean active;

	@Required
	@Column(nullable = false)
	public String type;

	@Required
	@Column(nullable = false)
	@Lob
	public String token;

	public String getDisplayName() {
		if (StringUtils.isEmpty(name)) {
			return type + "-" + token;
		}

		return name;
	}

	public Device toPnsDevice() {
		return new Device(token, type);
	}

	@Override
	protected void beforeInsert() throws Exception {
		super.beforeInsert();

		// assert correct user
		UserPushDevice existing = UserPushDevice.find("type = ? AND token = ?", type, token).first();
		if (existing != null && existing.user != user) {
			throw new IllegalArgumentException("Trying to save a UserPushDevice of another user");
		}
	}
}

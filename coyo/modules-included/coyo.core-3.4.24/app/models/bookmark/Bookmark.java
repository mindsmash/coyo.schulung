package models.bookmark;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Sender;
import models.TenantModel;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import util.Util;
import validation.SimpleURLWithRelative;

/**
 * Represents a Bookmark. A bookmark can be either global or personal. A global bookmark has no sender, whereas a
 * personal bookmark has a sender (usually a user) assigned.
 * 
 * @author Jan Marquardt
 * @author Sven Hoffmann
 */
@Entity
@Table(name = "bookmark")
public class Bookmark extends TenantModel {

	@Required
	@Column(nullable = false)
	@MaxSize(255)
	public String label;

	@SimpleURLWithRelative
	@Required
	@Lob
	public String url;

	@ManyToOne(optional = true)
	public Sender sender;

	public int priority = 9999;
	
	public boolean isGlobal() {
		return (sender == null);
	}

	public static void storeOrder(String bookmarkOrder) {
		List<String> order = Util.parseList(bookmarkOrder, ",");
		int priority = 0;
		for (String id : order) {
			Bookmark bookmark = Bookmark.findById(Long.valueOf(id));
			if (bookmark != null) {
				bookmark.priority = priority++;
				bookmark.save();
			}
		}
	}
}

package storage;

import injection.Inject;
import injection.InjectionSupport;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import models.Tenant;
import multitenancy.MTA;
import play.Play;
import storage.local.LocalFileStorage;

@InjectionSupport
public class FileStorageWrapper implements FlexibleBlobStorage {

	@Inject(configuration = "coyo.storage.class", defaultClass = LocalFileStorage.class)
	public static FlexibleBlobStorage storage;

	@Override
	public InputStream get(String id) {
		return storage.get(id);
	}

	@Override
	public void set(InputStream is, String id, String type) {
		checkStorageSpace();
		storage.set(is, id, type);
	}

	@Override
	public void set(InputStream is, String id, String type, Long length) {
		checkStorageSpace();
		storage.set(is, id, type, length);
	}

	private void checkStorageSpace() {
		Tenant tenant = (Tenant) MTA.getActiveTenant();
		if (Play.mode.isProd() && tenant.quota.isLow()) {
			throw new IllegalStateException("cannot save file because the disk space is low");
		}
	}

	@Override
	public long length(String id) {
		return storage.length(id);
	}

	@Override
	public long storageLength() {
		return storage.storageLength();
	}

	@Override
	public boolean exists(String id) {
		return storage.exists(id);
	}

	@Override
	public boolean delete(String id) {
		return storage.delete(id);
	}

	@Override
	public File getAsFile(String id) {
		return storage.getAsFile(id);
	}

	@Override
	public void finalize(String id) {
		storage.finalize(id);
	}

	@Override
	public List<String> listAll() {
		return storage.listAll();
	}
}

package assets;

import assets.id.IdResolver;
import models.AbstractTenant;
import multitenancy.MTA;
import play.db.jpa.JPA;
import assets.id.IdResolver;

/**
 * Resolve assets ID by active tenant from {@link MTA}.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class TenantAssetsIdResolver implements IdResolver {

	public static final String JS_SUFFIX = "js";

	@Override
	public String resolve(String assetType) {
		if (!assetType.equals(JS_SUFFIX) && JPA.isInsideTransaction() && MTA.isMultitenancyActive()) {
			AbstractTenant tenant = MTA.nullSafeGetActiveTenant();
			return "tenant-" + tenant.id;
		}
		return "notenant";
	}
}

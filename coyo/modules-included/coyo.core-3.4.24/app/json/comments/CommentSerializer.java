package json.comments;

import java.lang.reflect.Type;
import java.util.Date;

import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.Sender;
import models.User;
import models.comments.Comment;
import models.comments.CommentFileAttachment;
import models.comments.Commentable;
import security.CheckCaller;
import session.UserLoader;
import acl.Permission;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CommentSerializer implements JsonSerializer<Comment> {

	private User user;
	private Commentable commentable;

	public CommentSerializer(final User user, final Commentable commentable) {
		this.user = user;
		this.commentable = commentable;
	}

	@Override
	public JsonElement serialize(Comment comment, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();

		out.addProperty("id", comment.id);
		out.addProperty("clazz", comment.getClass().getName());
		out.addProperty("text", comment.text);
		out.addProperty("linkifiedText", comment.getLinkifiedText());
		out.addProperty("created", comment.created.getTime());
		out.addProperty("modified", comment.modified.getTime());
		out.addProperty("userLikes", comment.likes(user));

		out.add("author", context.serialize(comment.author));
		out.add("attachments", createBuilder(commentable).create().toJsonTree(comment.attachments));

		if (commentable != null) {
			out.addProperty("canDelete", CheckCaller.check(Permission.DELETE_COMMENT, comment.id, commentable));
		}

		return out;
	}

	public static GsonBuilder createBuilder(Commentable commentable) {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Comment.class, new CommentMinimalSerializer(UserLoader.getConnectedUser(), commentable));
		builder.registerTypeAdapter(CommentFileAttachment.class, new CommentFileAttachmentSerializer(commentable));
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

		return builder;
	}
}
package json.comments;

import java.lang.reflect.Type;

import models.User;
import models.comments.Comment;
import models.comments.Commentable;
import security.CheckCaller;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CommentMinimalSerializer implements JsonSerializer<Comment> {

	private User user;
	private Commentable commentable;

	public CommentMinimalSerializer(final User user, final Commentable commentable) {
		this.user = user;
		this.commentable = commentable;
	}

	@Override
	public JsonElement serialize(Comment comment, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();

		out.addProperty("id", comment.id);
		out.addProperty("clazz", comment.getClass().getName());
		out.addProperty("text", comment.text);
		out.addProperty("linkifiedText", comment.getLinkifiedText());
		out.addProperty("created", comment.created.getTime());
		out.addProperty("modified", comment.modified.getTime());
		out.addProperty("userLikes", comment.likes(user));

		out.add("author", context.serialize(comment.author));

		if (commentable != null) {
			out.addProperty("canDelete", CheckCaller.check(Permission.DELETE_COMMENT, comment.id, commentable));
		}

		return out;
	}
}
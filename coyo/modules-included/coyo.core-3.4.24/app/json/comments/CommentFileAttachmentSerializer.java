package json.comments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import json.FileAttachmentSerializer;
import models.comments.CommentFileAttachment;
import models.comments.Commentable;
import utils.PreviewUtils;

import java.lang.reflect.Type;

public class CommentFileAttachmentSerializer implements JsonSerializer<CommentFileAttachment>{
	private Commentable commentable;

	public CommentFileAttachmentSerializer(final Commentable commentable) {
		this.commentable = commentable;
	}

	@Override
	public JsonElement serialize(CommentFileAttachment attachment, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		attachment.source = commentable;

		out.addProperty("id", attachment.id);
		out.addProperty("clazz", attachment.getClass().getName());

		out.addProperty("name", attachment.name);
		out.addProperty("length", attachment.length());
		out.addProperty("contentType", attachment.getContentType());
		out.addProperty("displayName", attachment.getDisplayName());


		out.addProperty("fileName", attachment.getFileName());
		out.addProperty("size", attachment.getSize());
		out.addProperty("uniqueId", attachment.getUniqueId());
		out.addProperty("type", attachment.type());

		out.addProperty("downloadUrl", attachment.getDownloadUrl());

		if (PreviewUtils.hasPreview(attachment)) {
			out.add("preview", PreviewUtils.createJsonPreview(attachment));
		}
		
		return out;
	}

}
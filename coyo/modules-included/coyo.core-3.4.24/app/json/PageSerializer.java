package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.page.Page;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a workspace.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class PageSerializer implements JsonSerializer<Page> {

	@Override
	public JsonElement serialize(Page page, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", page.id);
		out.addProperty("name", page.getDisplayName());
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", page.id);
		args.put("appId", null);
		args.put("slug", page.getSlug());
		args.put("appSlug", null);
		out.addProperty("link", Router.reverse("Pages.show", args).url);
		
		return out;
	}

}

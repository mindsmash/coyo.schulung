package json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.CallContext;

import java.lang.reflect.Type;

/**
 * @author Denis Meyer, mindsmash GmbH
 */
public class CallContextSerializer implements JsonSerializer<CallContext> {

	@Override
	public JsonElement serialize(CallContext callContext, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("title", callContext.getTitle());
		out.addProperty("url", callContext.getURL());
		return out;
	}

}

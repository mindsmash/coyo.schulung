package json;

import java.lang.reflect.Type;

import models.UserPushDevice;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserPushDeviceSerializer implements JsonSerializer<UserPushDevice> {

	@Override
	public JsonElement serialize(UserPushDevice device, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", device.id);
		out.addProperty("userId", device.user.id);
		out.addProperty("displayName", device.getDisplayName());
		out.addProperty("name", device.name);
		out.addProperty("type", device.type);
		out.addProperty("token", device.token);
		out.addProperty("active", device.active);
		out.addProperty("created", device.created.getTime());
		return out;
	}

}

package json;

import java.lang.reflect.Type;

import apps.AppWidget;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class AppWidgetDefinitionSerializer implements JsonSerializer<AppWidget> {

	public JsonObject serialize(AppWidget widget, boolean selectable) {
		JsonObject out = new JsonObject();
		out.addProperty("key", widget.getKey());
		out.addProperty("name", widget.getName());
		out.addProperty("description", widget.getDescription());
		out.addProperty("selectable", selectable);
		return out;
	}

	@Override
	public JsonElement serialize(AppWidget widget, Type arg1, JsonSerializationContext arg2) {
		return serialize(widget, true);
	}
}

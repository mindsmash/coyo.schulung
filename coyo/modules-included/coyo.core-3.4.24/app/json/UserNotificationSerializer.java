package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.UserNotification;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserNotificationSerializer implements JsonSerializer<UserNotification> {

	@Override
	public JsonElement serialize(UserNotification un, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		Map args = new HashMap<String, String>();
		args.put("id", un.notification.id.toString());
		out.addProperty("id", un.id);
		out.addProperty("isRead", un.isRead);
		out.addProperty("source", Router.reverse("Notifications.goToSource", args).url);
		out.addProperty("label", un.notification.getText(un));
		out.add("notification", context.serialize(un.notification));
		return out;
	}

}

package json;

import java.lang.reflect.Type;

import models.app.App;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class AvailableAppWidgetsAppSerializer implements JsonSerializer<App> {

	@Override
	public JsonElement serialize(App app, Type arg1, JsonSerializationContext arg2) {
		JsonObject out = new JsonObject();
		out.addProperty("id", app.id);
		out.addProperty("key", app.getAppKey());
		out.addProperty("icon", app.getAppDescriptor().getIconClass());
		out.addProperty("title", app.title);
		return out;
	}

}

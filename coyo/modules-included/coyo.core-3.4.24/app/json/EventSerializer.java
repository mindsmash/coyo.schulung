package json;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import events.Event;

public class EventSerializer implements JsonSerializer<Event> {

	@Override
	public JsonElement serialize(Event event, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", event.getId());
		out.addProperty("type", event.getType());
		out.add("data", context.serialize(event.getData()));
		return out;
	}

}

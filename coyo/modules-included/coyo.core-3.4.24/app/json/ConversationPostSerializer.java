package json;

import java.lang.reflect.Type;

import session.UserLoader;
import models.wall.post.ConversationPost;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Json Serializer for {@link ConversationPost}.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class ConversationPostSerializer implements JsonSerializer<ConversationPost> {

	@Override
	public JsonElement serialize(ConversationPost post, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", post.id);
		out.addProperty("event", null != post.event);
		if (null != post.event) {
			out.addProperty("message", post.getEventMessage(UserLoader.getConnectedUser()));
		} else {
			out.addProperty("message", post.message);
			out.addProperty("linkifiedMessage", post.getLinkifiedMessage());
		}
		out.add("author", context.serialize(post.author));
		out.addProperty("created", "" + post.created.getTime());
		out.add("attachments", context.serialize(post.attachments));
		return out;
	}

}
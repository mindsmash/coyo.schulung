package json;

import java.lang.reflect.Type;
import java.util.List;

import acl.Permission;
import models.User;
import models.messaging.ConversationMember;
import onlinestatus.OnlineStatusService;
import onlinestatus.OnlineStatusServiceFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import security.CheckCaller;

/**
 * The conversation json serializer.
 *
 * @author Daniel Busch, mindsmash GmbH
 *
 */
public class ConversationsSerializer implements JsonSerializer<ConversationMember> {

	private User connectedUser;

	public ConversationsSerializer(User connectedUser) {
		this.connectedUser = connectedUser;
	}

	@Override
	public JsonElement serialize(ConversationMember cm, Type type, JsonSerializationContext context) {
		// gather conversation users
		JsonArray users = new JsonArray();
		JsonObject user = null;

		OnlineStatusService onlineStatusService = OnlineStatusServiceFactory.getOnlineStatusService();
		for (Long userId : cm.conversation.getUserIds()) {
			user = new JsonObject();
			user.addProperty("id", userId);
			user.addProperty("status", onlineStatusService.getOnlineStatus(userId).status.toString());
			users.add(user);
		}

		JsonObject out = new JsonObject();
		if (connectedUser != null) {
			List<User> otherUsers = cm.conversation.getOtherUsers(connectedUser);
			JsonArray otherUsersJson = new JsonArray();
			JsonObject otherUserJson = null;
			
			for (User u : otherUsers) {
				otherUserJson = new JsonObject();
				otherUserJson.addProperty("id", u.id);
				otherUserJson.addProperty("fullName", u.getFullName());
				otherUserJson.addProperty("status", u.getOnlineStatus().status.toString());
				otherUserJson.addProperty("canAccessCall", CheckCaller.check(Permission.ACCESS_WEBRTC_CALL, u.id));
				otherUserJson.addProperty("isCallable", CheckCaller.check(Permission.IS_WEBRTC_CALLABLE, u.id));
				otherUsersJson.add(otherUserJson);
			}
			out.add("otherUsers", otherUsersJson);
		
			if (otherUsers.size() == 1) {
				out.addProperty("connectedUser", connectedUser.id);
				out.addProperty("conversationStatus", otherUsers.get(0).getOnlineStatus().status.toString());
			}
		}
		out.addProperty("remove", false);
		out.addProperty("id", cm.id);
		out.add("users", users);
		out.addProperty("status", cm.status.toString());
		out.addProperty("modified", cm.conversation.lastPost.getTime());
		out.addProperty("unreadCount", cm.getUnreadCount());
		out.addProperty("subject", cm.getSubject());
		out.addProperty("activeMemberCount", cm.conversation.getActiveMemberCount());
		out.addProperty("archived", cm.isArchived());
		out.addProperty("thumbURL", cm.getThumbURL());
		return out;
	}
}
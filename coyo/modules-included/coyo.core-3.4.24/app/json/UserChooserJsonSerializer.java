package json;

import java.lang.reflect.Type;

import models.User;

import org.apache.commons.lang.StringUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * This serializer generates the JSON output for the user chooser select box.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class UserChooserJsonSerializer implements JsonSerializer<User> {

	public JsonElement serialize(User user, Type type, JsonSerializationContext context) {
		JsonObject o = new JsonObject();
		o.addProperty("id", user.id);
		o.addProperty("thumbURL", user.getThumbURL());
		o.addProperty("fullName", user.getFullName());
		o.addProperty("firstName", user.firstName);
		o.addProperty("nickname", user.nickname);
		o.addProperty("lastName", user.lastName);
		o.addProperty("department", user.department);
		o.addProperty("office", user.office);
		o.addProperty("jobTitle", user.jobTitle);
		o.addProperty("company", user.company);
		o.addProperty("showDetails",
				(!StringUtils.isEmpty(user.company) || !StringUtils.isEmpty(user.department) || !StringUtils
						.isEmpty(user.jobTitle)));
		return o;
	}
}
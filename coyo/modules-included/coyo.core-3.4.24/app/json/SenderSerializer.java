package json;

import java.lang.reflect.Type;

import models.Sender;
import models.wall.Wall;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a sender for the wall chooser dialog. Therefore only the name and the id of the default wall are needed.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class SenderSerializer implements JsonSerializer<Sender> {

	@Override
	public JsonElement serialize(Sender sender, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", sender.id);
		out.addProperty("name", sender.getDisplayName());

		Wall wall = sender.getDefaultWall();
		out.addProperty("defaultWall", (wall != null) ? wall.id : null);

		return out;
	}

}

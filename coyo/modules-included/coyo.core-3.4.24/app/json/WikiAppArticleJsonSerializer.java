package json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.app.WikiAppArticle;
import models.app.WikiAppArticleVersion;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Type;

/**
 *
 * WikiAppArticle JSON Serializer
 */
public class WikiAppArticleJsonSerializer implements JsonSerializer<WikiAppArticle> {
	@Override
	public JsonElement serialize(WikiAppArticle w, Type type, JsonSerializationContext context) {
		JsonObject o = new JsonObject();
		o.addProperty("id", w.id);
		o.addProperty("title", w.title);

		final WikiAppArticleVersion latest = w.getCurrentVersion();

		if (w.parent != null) {
			o.addProperty("parentId", w.parent.id);
		}
		o.add("created", context.serialize(w.created));
		o.add("modified", context.serialize(latest.modified));

		o.add("nodes", context.serialize(w.children));
		o.add("author", context.serialize(latest.author));

		final boolean empty = (latest == null) || (w.versions.size() == 0) || StringUtils.isEmpty(latest.text);
		o.addProperty("empty", empty);

		if (latest != null && latest.author != null) {
			o.addProperty("authorID", latest.author.id);
		}

		return o;
	}
}
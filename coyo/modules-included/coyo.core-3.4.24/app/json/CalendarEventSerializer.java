package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.event.Event;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a workspace.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class CalendarEventSerializer implements JsonSerializer<Event> {

	@Override
	public JsonElement serialize(Event event, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", event.id);
		out.addProperty("name", event.getDisplayName());
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", event.id);
		args.put("slug", event.getSlug());
		out.addProperty("link", Router.reverse("Events.show", args).url);
		
		return out;
	}

}

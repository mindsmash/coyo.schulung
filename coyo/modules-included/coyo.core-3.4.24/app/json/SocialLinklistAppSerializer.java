package json;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import models.User;
import models.app.SocialLink;
import models.app.SocialLinklistApp;
import models.app.dao.SocialLinklistAppDao;
import session.UserLoader;

public class SocialLinklistAppSerializer implements JsonSerializer<SocialLinklistApp> {

	private User user;
	
	public SocialLinklistAppSerializer(User user) {
		this.user = user; 
	}
	
	@Override
	public JsonElement serialize(SocialLinklistApp app, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		User user = UserLoader.getConnectedUser();
		
		out.addProperty("id", app.id);
		out.addProperty("senderId", app.sender.id);
		out.addProperty("usersCanAddLinks", app.usersCanAddLinks);
		out.addProperty("usersCanEditLinks", app.usersCanEditLinks);
		out.addProperty("admin", app.sender.getAdmins().contains(user));
		
		out.add("linklist",context.serialize(SocialLinklistAppDao.getLinkList(app)));
		
		return out;
	}

}

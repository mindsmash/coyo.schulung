package json;

import acl.AppsPermission;
import acl.Permission;
import com.google.gson.*;
import controllers.Security;
import controllers.Senders;
import models.BoxFile;
import models.Sender;
import models.User;
import models.app.InnoApp;
import models.app.inno.*;
import models.container.Container;
import models.container.Field;
import models.container.ItemValue;
import models.container.fields.OptionsFieldOption;
import models.container.values.attachments.FileItemAttachment;
import theme.Theme;

import java.lang.reflect.Type;
import java.util.Date;

public class BoxFileSerializer implements JsonSerializer<BoxFile> {

	public BoxFileSerializer() {
	}

	@Override
	public JsonElement serialize(BoxFile boxfile, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();
		out.add("name", context.serialize(boxfile.name));
		out.add("uid", context.serialize(boxfile.getUid()));
		out.add("created", context.serialize(boxfile.created));
		out.add("modified", context.serialize(boxfile.modified));
		out.addProperty("appId", boxfile.getApp().id);
		out.add("id", context.serialize(boxfile.id));
		out.add("clazz", context.serialize(boxfile.getClass().getName()));
		out.add("creator", context.serialize(boxfile.creator));
		out.add("senderDisplayName", context.serialize(boxfile.getApp().sender.getDisplayName()));
		out.add("senderUrl", context.serialize(boxfile.getApp().sender.getURL(boxfile.getApp().id)));

		if (boxfile.getCreated(session.UserLoader.getConnectedUser()).isAfter(new org.joda.time.DateTime().minusDays(1))) {
			out.addProperty("tagnew", true);
		} else if (boxfile.getModified(session.UserLoader.getConnectedUser()).isAfter(new org.joda.time.DateTime().minusHours(4))) {
			out.addProperty("tagmodified", true);
		}

		return out;
	}
}
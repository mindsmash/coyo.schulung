package json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * serialize and deserialize date objects
 */
public class Dates {
	public final static JsonSerializer<Date> serializer = new JsonSerializer<Date>() {
		@Override
		public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
				context) {
			return src == null ? null : new JsonPrimitive(src.getTime());
		}
	};

	public final static JsonDeserializer<Date> deserializer = new JsonDeserializer<Date>() {
		@Override
		public Date deserialize(JsonElement json, Type typeOfT,
				JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};
}

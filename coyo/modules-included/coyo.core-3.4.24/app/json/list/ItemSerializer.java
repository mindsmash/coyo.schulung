package json.list;

import java.lang.reflect.Type;

import models.container.Item;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Security;

public class ItemSerializer implements JsonSerializer<Item> {
	@Override
	public JsonElement serialize(Item item, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.add("id", context.serialize(item.id));
		out.add("values", context.serialize(item.values));
		out.addProperty("editable", Security.check(Permission.EDIT_CONTAINER_ITEM, item.id));
		out.addProperty("deletable", Security.check(Permission.DELETE_CONTAINER_ITEM, item.id));
		out.add("created", context.serialize(item.created.getTime()));
		out.add("modified", context.serialize(item.modified.getTime()));
		out.add("creator", context.serialize(item.creator));
		return out;
	}
}
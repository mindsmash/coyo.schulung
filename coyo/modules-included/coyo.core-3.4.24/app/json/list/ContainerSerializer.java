package json.list;

import java.lang.reflect.Type;

import models.app.ListApp;
import models.container.Container;
import acl.Permission;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Security;

/**
 * @author Jan Tammen, mindsmash GmbH
 */
public class ContainerSerializer implements JsonSerializer<Container> {
	private boolean includeContainerItems;

	public ContainerSerializer() {
		// empty default c'tor
	}

	public ContainerSerializer(boolean includeContainerItems) {
		this.includeContainerItems = includeContainerItems;
	}

	@Override
	public JsonElement serialize(Container container, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", container.id);
		out.addProperty("senderId", container.sender.id);
		out.addProperty("name", container.name);
		out.addProperty("readAll", Security.check(Permission.READ_ALL_CONTAINER_ITEMS, container.id));
		out.addProperty("readable", Security.check(Permission.READ_CONTAINER_ITEMS, container.id));
		out.addProperty("editable", Security.check(Permission.CREATE_CONTAINER_ITEM, container.id));
		out.addProperty("configurable", Security.check(Permission.EDIT_SENDER, container.sender.id));
		
		out.add("fields", context.serialize(container.fields));
		
		if (includeContainerItems) {
			out.add("items", context.serialize(container.items));
		} else {
			out.add("items", context.serialize(Lists.newArrayList()));
		}

		final ListApp app = ListApp.find("container = ?", container).first();
		if (app != null) {
			out.addProperty("notificationRecipient", app.notificationRecipient.toString());
		}

		return out;
	}
}

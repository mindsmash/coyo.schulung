package json.list;

import java.lang.reflect.Type;

import models.container.Field;
import models.container.ItemValue;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author Jan Tammen, mindsmash GmbH
 */
public class ItemValueSerializer implements JsonSerializer<ItemValue>, JsonDeserializer<ItemValue> {
	@Override
	public ItemValue deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		try {
			JsonObject jsonObject = jsonElement.getAsJsonObject();
			Class<? extends ItemValue> clasz = (Class<? extends ItemValue>) Class.forName(jsonObject.get("type")
					.getAsString());
			ItemValue itemValue = clasz.newInstance();
			itemValue.id = context.deserialize(jsonObject.get("id"), Long.class);
			itemValue.setValue(jsonObject.get("value").getAsString());
			itemValue.field = Field.findById(jsonObject.get("fieldId").getAsLong());
			return itemValue;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			throw new JsonParseException(e);
		}
	}

	@Override
	public JsonElement serialize(ItemValue itemValue, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.add("fieldId", context.serialize(itemValue.field.id));
		out.add("value", context.serialize(itemValue.getValue()));
		out.add("type", context.serialize(itemValue.getClass().getName()));
		out.add("displayProperty", context.serialize(itemValue.getDisplayProperty()));
		return out;
	}
}

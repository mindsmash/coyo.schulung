package json.list;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Map.Entry;

import models.container.Field;
import models.container.FieldFactory;
import models.container.fields.OptionsField;

import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class FieldSerializer implements JsonSerializer<Field>, JsonDeserializer<Field> {
	@Override
	public JsonElement serialize(Field field, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", field.id);
		out.add("help", context.serialize(field.help));
		out.add("hidden", context.serialize(field.hidden));
		out.add("label", context.serialize(field.label));
		out.add("priority", context.serialize(field.priority));
		out.add("properties", context.serialize(field.getTypedProperties()));
		out.add("required", context.serialize(field.required));
		out.add("type", context.serialize(field.getType()));
		if (field instanceof OptionsField) {
			OptionsField optionsField = (OptionsField) field;
			out.add("options", context.serialize(optionsField.options));
		}
		return out;
	}

	@Override
	public Field deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jsonObject = json.getAsJsonObject();
		Class<? extends Field> clasz = FieldFactory.getClasz(jsonObject.get("type").getAsString());
		Field field;
		try {
			field = clasz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new JsonParseException(e);
		}
		field.id = context.deserialize(jsonObject.get("id"), Long.class);
		field.help = context.deserialize(jsonObject.get("help"), String.class);
		field.hidden = context.deserialize(jsonObject.get("hidden"), Boolean.class);
		field.label = context.deserialize(jsonObject.get("label"), String.class);

		if (jsonObject.has("priority")) {
			field.priority = context.deserialize(jsonObject.get("priority"), Integer.class);
		}
		// make sure we get a Map<String, String>
		field.properties = Maps.<String, String> newHashMap();
		for (Entry<String, Object> entry : context.<Map<String, Object>> deserialize(jsonObject.get("properties"),
				Map.class).entrySet()) {
			Object value = entry.getValue();
			if (value instanceof Number) {
				// we want to get integers instead of doubles...
				value = Double.valueOf(value.toString()).intValue();
			}
			field.properties.put(entry.getKey(), value.toString());
		}
		field.required = context.deserialize(jsonObject.get("required"), Boolean.class);
		return field;
	}
}

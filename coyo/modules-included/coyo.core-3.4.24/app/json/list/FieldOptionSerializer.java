package json.list;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Map.Entry;

import models.container.Field;
import models.container.fields.OptionsField;
import models.container.fields.OptionsFieldOption;

import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class FieldOptionSerializer implements JsonSerializer<OptionsFieldOption> {

	@Override
	public JsonElement serialize(OptionsFieldOption option, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", option.id);
		out.addProperty("label", option.label);
		return out;
	}
	
}

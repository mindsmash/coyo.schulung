package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.app.ForumApp;
import models.wall.post.Post;
import play.mvc.Router;
import security.CheckCaller;
import acl.AppsPermission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

public class ForumPostSerializer extends PostSerializer {

	@Override
	public JsonElement serialize(Post post, Type type, JsonSerializationContext context) {
		JsonObject out = super.serialize(post, type, context).getAsJsonObject();
		
		/* Render a link to the latest post */
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", post.wall.sender.id);
		args.put("appId", ForumApp.findByTopic(post.wall).id);
		args.put("postId", post.id);
		out.addProperty("url", Router.reverse("Senders.show", args).url);
		
		out.addProperty("canDelete", CheckCaller.check(AppsPermission.DELETE_FORUM_POST, post.id, post.wall.id));
		
		return out;
	}
}
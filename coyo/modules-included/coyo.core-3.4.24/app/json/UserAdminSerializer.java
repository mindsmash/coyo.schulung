package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import libs.DateI18N;
import models.User;
import models.UserGroup;
import play.mvc.Router;
import security.CheckCaller;
import session.UserLoader;
import acl.Permission;
import acl.PermissionChecker;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Security;
import controllers.WebBaseController;
import extensions.JodaJavaExtensions;

/**
 * Serializes a user representation used for lists in the administration area.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class UserAdminSerializer implements JsonSerializer<User> {
	
	/**
	 * Private class that transforms all UserGroup entries of a list to just their names.
	 */
	private static class GroupNameTransformer implements Function<UserGroup, String> {
		@Override
		public String apply(UserGroup group) {
			return group.name;
		}
	}

	@Override
	public JsonElement serialize(User user, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		
		out.addProperty("id", user.id);
		out.addProperty("status", (user.active) ? "active" : "inactive");		
		out.addProperty("name", user.getFullName());
		out.addProperty("email", user.email);
		out.addProperty("superadmin", user.superadmin);
		out.addProperty("logins", user.loginCount);
		out.addProperty("lastLogin", JodaJavaExtensions.format(user.getLocalDate(user.lastLogin), DateI18N.getFullFormat()));
		out.add("groups", context.serialize(Lists.transform(user.groups, new GroupNameTransformer())));
		out.addProperty("role", (user.role != null) ? user.role.name : "");
		out.addProperty("authSource", user.authSource);
		out.addProperty("authUid", user.authUid);
		out.addProperty("authUid2", user.authUid2);
		out.addProperty("external", user.external);
		out.addProperty("local", user.isLocal());
		out.addProperty("me", user == UserLoader.getConnectedUser());
		out.addProperty("modified", user.modified.getTime());
		
		out.addProperty("canEdit", Security.check(Permission.ADMIN_EDIT_USER, user.id));
		
		/* render urls */
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("userId", user.id);
		out.addProperty("editUrl", Router.reverse("administration.Users.edit", args).url);
		out.addProperty("changePasswordUrl", Router.reverse("administration.Users.changePassword", args).url);
		out.addProperty("recoverUrl", Router.reverse("administration.Users.recover", args).url);

		out.add("userDevices", context.serialize(user.userDevices));

		return out;
	}

}
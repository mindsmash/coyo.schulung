package json;

import java.lang.reflect.Type;

import models.User;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserProfileSerializer implements JsonSerializer<User> {

	@Override
	public JsonElement serialize(User un, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("thumbUrl", un.getThumbURL());
		out.addProperty("id", un.id.toString());
		// User info
		out.addProperty("fullName", un.getFullNameWithTitle());
		out.addProperty("firstName", un.firstName);
		out.addProperty("lastName", un.lastName);
		out.addProperty("job", un.jobTitle);
		out.addProperty("company", un.company);
		// contact maps
		out.addProperty("mobile", un.mobilePhone);
		out.addProperty("phone", un.workPhone);
		out.addProperty("email", un.email);
		out.addProperty("fax", un.workFax);
		// address
		out.addProperty("street", un.street);
		out.addProperty("streetNumber", un.streetNumber);
		out.addProperty("addressCity", un.city);
		out.addProperty("zip", un.zipCode);
		out.addProperty("country", un.country);
		
		out.addProperty("language", un.language.getLanguage());
		// Online status
		out.addProperty("status", un.getOnlineStatus().status.toString());

		return out;
	}

}

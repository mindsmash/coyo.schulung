package json.blogapp;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import acl.AppsPermission;
import extensions.CommonExtensions;
import extensions.CoreJavaExtensions;
import models.User;
import models.app.BlogApp;
import models.app.BlogAppArticle;
import security.CheckCaller;
import utils.DateUtils;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class BlogAppSerializer implements JsonSerializer<BlogApp> {

	private User user;

	public BlogAppSerializer(User user) {
		this.user = user;
	}

	@Override
	public JsonElement serialize(BlogApp app, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		out.addProperty("id", app.id);
		out.addProperty("senderId", app.sender.id);
		out.addProperty("title", app.title);

		Map<Integer, Map<Integer, Long>> articleCountByMonth = app.getArticleCountByMonth(user);
		JsonArray articleCount = new JsonArray();
		for (int year : articleCountByMonth.keySet()) {
			JsonObject xYear = new JsonObject();
			xYear.addProperty("year", year);

			JsonArray months = new JsonArray();
			for (int month : articleCountByMonth.get(year).keySet()) {
				long count = articleCountByMonth.get(year).get(month);

				if (count > 0) {
					JsonObject xMonth = new JsonObject();
					xMonth.addProperty("year", year);
					xMonth.addProperty("month", month);
					xMonth.addProperty("name", DateUtils.getMonthDisplayName(month, user));
					xMonth.addProperty("count", count);
					months.add(xMonth);
				}
			}

			if (months.size() > 0) {
				xYear.add("months", months);
				articleCount.add(xYear);
			}
		}
		out.add("articleCount", articleCount);

		out.addProperty("mediaLibraryId", app.sender.mediaLibrary.id);
		out.addProperty("teaserImageWidth", BlogAppArticle.TEASER_IMAGE_WIDTH);
		out.addProperty("canCreateArticle", CheckCaller.check(AppsPermission.EDIT_BLOG, app.id));

		return out;
	}

}

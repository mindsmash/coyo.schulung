package json.blogapp;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import libs.DateI18N;
import models.User;
import models.app.BlogAppArticle;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.ISODateTimeFormat;

import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import security.CheckCaller;
import acl.AppsPermission;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Collaboration;
import extensions.CoreJavaExtensions;
import extensions.JodaJavaExtensions;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class BlogArticleSerializer implements JsonSerializer<BlogAppArticle> {

	private User user;

	public BlogArticleSerializer(User connectedUser) {
		user = connectedUser;
	}

	@Override
	public JsonElement serialize(BlogAppArticle article, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		boolean canComment = CheckCaller.check(Permission.CREATE_COMMENT, article);

		out.addProperty("id", article.id);
		out.addProperty("appId", article.app.id);
		out.addProperty("url", article.getURL());
		out.addProperty("title", article.title);

		// likeable
		out.addProperty("clazz", article.getClass().getName());
		out.addProperty("userLikes", article.likes(user));

		// commentable
		out.addProperty("senderId", article.app.sender.id);
		out.addProperty("canCreateComment", canComment);

		if (StringUtils.isEmpty(article.text)) {
			out.addProperty("outputText", StringUtils.EMPTY);
			out.addProperty("text", StringUtils.EMPTY);
		} else {
			out.addProperty("outputText", CoreJavaExtensions.hashify(article.text).data);
			out.addProperty("text", article.text);
		}

		out.addProperty("outputTeaser", article.getTeaserText());

		out.addProperty("teaser", article.teaser);

		out.addProperty("showTeaser", article.showTeaser);

		if (article.teaserImage != null) {
			Map<String, Object> args = new HashMap<>();
			args.put("id", article.teaserImage);
			ActionDefinition ad = Router.reverse("MediaLibraries.file", args);
			out.addProperty("teaserImageUrl", ad.url);
		}

		out.addProperty("published", article.isPublished());

		if (article.publishDate != null) {
			out.add("publishDate", context.serialize(ISODateTimeFormat.dateTime().print(article.getPublishDate(user))));
			out.add("publishDateMillis", context.serialize(article.getPublishDate(user).getMillis()));
			out.addProperty("publishDateFormatted",
					JodaJavaExtensions.format(article.getPublishDate(user), DateI18N.getFullFormat()));
		}

		out.add("author", context.serialize(article.author));

		out.addProperty("commentCount", article.getComments().size());

		out.addProperty("canEdit", CheckCaller.check(AppsPermission.EDIT_BLOG_ARTICLE, article));
		out.addProperty("canLike", CheckCaller.check(Permission.LIKE, article));
		out.addProperty("canComment", canComment);
		out.addProperty("canShare", CheckCaller.check(Permission.SHARE, article));

		// TODO: remove when sharing is done through AngularJS
		out.addProperty("shareUrl", Collaboration.getShareUrl(article));

		out.addProperty("isShared", article.isShared());
		out.addProperty("isPublished", article.isPublished());

		return out;
	}

}

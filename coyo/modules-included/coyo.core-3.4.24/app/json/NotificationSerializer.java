package json;

import java.lang.reflect.Type;

import models.notification.Notification;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class NotificationSerializer implements JsonSerializer<Notification> {

	@Override
	public JsonElement serialize(Notification notification, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", notification.id);
		out.addProperty("type", notification.getClass().getSimpleName());
		out.addProperty("image", notification.getImageUrl());
		out.addProperty("time", notification.created.getTime());
		out.addProperty("avatar", notification.getThumbnailSender().getThumbURL());
		out.addProperty("senderId", notification.getThumbnailSender().id);

		return out;
	}
}

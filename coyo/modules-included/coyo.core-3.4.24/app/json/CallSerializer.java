package json;

import java.lang.reflect.Type;

import models.Call;
import models.User;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import util.ModelUtil;

public class CallSerializer implements JsonSerializer<Call> {
	
	private User currentUser;

	public CallSerializer(User currentUser) {
		super();
		this.currentUser = currentUser;
	}

	@Override
	public JsonElement serialize(Call call, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", call.id);
		out.addProperty("created", call.created.getTime());
		out.addProperty("summary", call.summary);
		out.addProperty("status", call.status.name());
		out.addProperty("incoming", call.isIncoming(currentUser));
		if(call.contextUid != null && !call.contextUid.isEmpty()) {
			out.add("context", context.serialize(ModelUtil.deserialize(call.contextUid)));
		}
		out.add("otherUsers", context.serialize(call.getOtherUser(currentUser)));
		return out;
	}

}

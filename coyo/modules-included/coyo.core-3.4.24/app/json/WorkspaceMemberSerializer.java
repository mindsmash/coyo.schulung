package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.workspace.Workspace;
import models.workspace.WorkspaceMember;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a workspace member. This serializer needs a user serializer (like {@linkplain MinimalUserSerializer})
 * within its context to work correctly.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class WorkspaceMemberSerializer implements JsonSerializer<WorkspaceMember> {

	@Override
	public JsonElement serialize(WorkspaceMember workspaceMember, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", workspaceMember.id);
		out.addProperty("admin", workspaceMember.admin);
		out.add("user", context.serialize(workspaceMember.user));
		
		return out;
	}

}
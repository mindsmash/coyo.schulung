package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.Settings;
import models.User;
import play.mvc.Router;
import security.CheckCaller;
import session.UserLoader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.administration.Teasers.TeaserSettings;

/**
 * Serializes the properties of any {@link Settings} model.
 * 
 * @author mindsmash GmbH
 */
public class SettingsSerializer implements JsonSerializer<Settings> {

	@Override
	public JsonElement serialize(Settings settings, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("type", settings.type);
		out.add("properties", context.serialize(settings.getProperties()));
		return out;
	}

}

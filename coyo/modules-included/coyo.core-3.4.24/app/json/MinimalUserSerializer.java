package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import acl.Permission;
import models.User;
import play.mvc.Router;
import security.CheckCaller;
import session.UserLoader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a minimal user representation used for lists.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class MinimalUserSerializer implements JsonSerializer<User> {

	@Override
	public JsonElement serialize(User user, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", user.id);
		out.addProperty("fullName", user.getFullName());
		out.addProperty("firstName", user.firstName);
		out.addProperty("lastName", user.lastName);
		out.addProperty("currentUser", user == UserLoader.getConnectedUser());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", user.id);
		args.put("slug", user.slug);
		out.addProperty("wallUrl", Router.reverse("Users.wall", args).url);

		out.addProperty("thumbUrl", user.getThumbURL());
		out.addProperty("avatarUrl", user.getAvatarURL());

		// Online status
		out.addProperty("status", user.getOnlineStatus().status.toString());

		out.addProperty("external", user.external);

		out.addProperty("accessUser", CheckCaller.check(acl.Permission.ACCESS_USER, user.id));
		out.addProperty("canAccessCall", CheckCaller.check(Permission.ACCESS_WEBRTC_CALL, user.id));
		out.addProperty("isCallable", CheckCaller.check(Permission.IS_WEBRTC_CALLABLE, user.id));

		return out;
	}

}

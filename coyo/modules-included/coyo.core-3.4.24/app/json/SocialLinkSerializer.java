package json;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import acl.AppsPermission;
import models.User;
import models.app.SocialLink;
import security.CheckCaller;
import session.UserLoader;
import utils.WebLinkInfo;

public class SocialLinkSerializer implements JsonSerializer<SocialLink> {
	
	@Override
	public JsonElement serialize(SocialLink link, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();		
		
		out.addProperty("id", link.id);
		out.addProperty("title", link.title);
		out.addProperty("linkUrl", link.linkUrl);
		out.addProperty("description", link.description);
		out.addProperty("faviconUrl", link.faviconUrl);

		out.addProperty("canEdit", (link.id != null) ? CheckCaller.check(AppsPermission.EDIT_SOCIAL_LINK, link.app.id, link.id) : false);
		
		return out;
	}
	
}
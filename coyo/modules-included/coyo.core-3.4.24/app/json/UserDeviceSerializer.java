package json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.UserDevice;

import java.lang.reflect.Type;

public class UserDeviceSerializer implements JsonSerializer<UserDevice> {

	@Override
	public JsonElement serialize(UserDevice device, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", device.id);
		out.addProperty("userId", device.user.id);
		out.addProperty("name", device.name);
		out.addProperty("description", device.description);

		out.addProperty("accessToken", device.accessToken);
		out.addProperty("refreshToken", device.refreshToken);

		out.addProperty("created", device.created.getTime());
		return out;
	}

}

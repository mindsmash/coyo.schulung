package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.User;
import models.workspace.Workspace;
import play.mvc.Router;
import security.CheckCaller;
import session.UserLoader;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

public class WorkspaceDetailSerializer extends WorkspaceSerializer {
	
	private static final int MEMBER_LIMIT = 10;

	@Override
	public JsonElement serialize(Workspace workspace, Type type, JsonSerializationContext context) {
		JsonObject out = (JsonObject) super.serialize(workspace, type, context);
		
		User user = UserLoader.getConnectedUser();
		
		out.addProperty("about", workspace.about);
		out.addProperty("avatarUrl", workspace.getAvatarURL());
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", workspace.id);
		out.addProperty("memberLink", Router.reverse("Workspaces.members", args).url);
		out.addProperty("visibilityIcon", workspace.visibility.icon);
		out.addProperty("visibilityLabel", workspace.visibility.getLabel());
		
		out.add("members", context.serialize(workspace.getUserMembers(MEMBER_LIMIT)));
		out.add("memberCount", context.serialize(workspace.getUserMembers().size()));
		out.addProperty("isMember", workspace.isMember(user));
		out.addProperty("isJoinRequested", workspace.hasRequestedToJoin(user));
		
		out.addProperty("canJoin", CheckCaller.check(Permission.JOIN_WORKSPACE, workspace.id));
		
		return out;
	}

}

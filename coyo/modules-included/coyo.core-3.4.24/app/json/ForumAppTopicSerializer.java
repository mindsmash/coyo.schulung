package json;

import java.lang.reflect.Type;

import models.app.forum.ForumAppTopic;
import models.wall.post.Post;
import security.CheckCaller;
import acl.AppsPermission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Json Serializer for {@link ForumAppTopic}.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 * 
 */
public class ForumAppTopicSerializer implements JsonSerializer<ForumAppTopic> {

	@Override
	public JsonElement serialize(ForumAppTopic topic, Type type, JsonSerializationContext context) {
		int postsSize = topic.getPosts().size();
		Post lastPost = topic.getPosts().get(postsSize - 1);
		
		JsonObject out = new JsonObject();
		out.addProperty("id", topic.id);
		out.addProperty("title", topic.title);
		out.addProperty("postCount", postsSize);
		out.addProperty("closed", topic.closed);
		out.addProperty("created", topic.created.getTime());
		out.addProperty("modified", topic.modified.getTime());
		out.add("author", context.serialize(topic.author));
		out.add("lastPost", context.serialize(lastPost));
		out.addProperty("canEdit", CheckCaller.check(AppsPermission.EDIT_FORUM_TOPIC, topic.id));
		out.addProperty("canPost", CheckCaller.check(AppsPermission.ADD_FORUM_POST, topic.id));
		return out;
	}

}
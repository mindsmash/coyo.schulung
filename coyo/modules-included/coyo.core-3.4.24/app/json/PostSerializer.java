package json;

import java.lang.reflect.Type;

import models.wall.post.Post;
import security.CheckCaller;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * JSON serializer for Post class. Since this serializer is only used for the
 * forum app right now, it only supports properties needed in this context.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 * 
 */
public class PostSerializer implements JsonSerializer<Post> {

	@Override
	public JsonElement serialize(Post post, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", post.id);
		out.addProperty("message", post.getLinkifiedMessage());
		out.add("author", context.serialize(post.author));
		out.addProperty("created", post.created.getTime());
		out.addProperty("modified", post.modified.getTime());
		out.addProperty("important", post.important);
		out.add("attachments", context.serialize(post.attachments));
		return out;
	}

}
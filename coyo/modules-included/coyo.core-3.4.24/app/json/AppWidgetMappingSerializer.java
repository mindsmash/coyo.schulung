package json;

import java.lang.reflect.Type;

import session.UserLoader;
import models.User;
import models.app.WidgetMapping;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class AppWidgetMappingSerializer implements JsonSerializer<WidgetMapping> {

	public JsonElement serialize(WidgetMapping wm, User connectedUser) {
		JsonObject o = new JsonObject();
		o.addProperty("id", wm.id);
		o.addProperty("sourceApp", wm.sourceApp.id);
		o.addProperty("sourceAppKey", wm.sourceApp.getAppKey());
		o.addProperty("key", wm.getAppWidget().getKey());
		o.addProperty("title", wm.title);
		o.add("data", wm.getAppWidget().getRenderData(wm.sourceApp, connectedUser));
		return o;
	}

	@Override
	public JsonElement serialize(WidgetMapping wm, Type arg1, JsonSerializationContext arg2) {
		return serialize(wm, UserLoader.getConnectedUser());
	}

}

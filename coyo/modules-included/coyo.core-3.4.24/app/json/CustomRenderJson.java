package json;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.exceptions.UnexpectedException;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.Result;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSerializer;

/*
 * Custom RenderJson to register TypeAdapter for all subclasses of
 * the class that is handled by the given Serializer.
 */
public class CustomRenderJson extends Result {
	String json;

	public CustomRenderJson(Object o) {
		json = new Gson().toJson(o);
	}

	public CustomRenderJson(Object o, Type type) {
		json = new Gson().toJson(o, type);
	}

	public CustomRenderJson(Object o, JsonSerializer<?>... adapters) {
		GsonBuilder gson = new GsonBuilder();
		for (Object adapter : adapters) {
			Type t = getMethod(adapter.getClass(), "serialize").getParameterTypes()[0];
			gson.registerTypeAdapter(t, adapter);
			List<ApplicationClass> classes = Play.classes.getAssignableClasses((Class<?>) t);
			for (ApplicationClass clazz : classes) {
				gson.registerTypeAdapter(clazz.javaClass, adapter);
			}
		}
		json = gson.create().toJson(o);
	}

	public CustomRenderJson(String jsonString) {
		json = jsonString;
	}

	public void apply(Request request, Response response) {
		try {
			String encoding = getEncoding();
			setContentTypeIfNotSet(response, "application/json; charset=" + encoding);
			response.out.write(json.getBytes(encoding));
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	static Method getMethod(Class<?> clazz, String name) {
		for (Method m : clazz.getDeclaredMethods()) {
			if (m.getName().equals(name) && !m.isBridge()) {
				return m;
			}
		}
		return null;
	}
}

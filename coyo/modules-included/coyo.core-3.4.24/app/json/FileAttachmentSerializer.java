package json;

import java.lang.reflect.Type;

import models.FileAttachment;
import play.Logger;
import utils.PreviewUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializer for attachments.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class FileAttachmentSerializer implements JsonSerializer<FileAttachment> {

	@Override
	public JsonElement serialize(FileAttachment attachment, Type type, JsonSerializationContext context) {
		try {
			JsonObject out = new JsonObject();
			out.addProperty("id", attachment.getId());
			out.addProperty("type", "file");
			out.addProperty("fileName", attachment.getFileName());
			out.addProperty("displayName", attachment.getDisplayName());
			out.addProperty("contentType", attachment.getContentType());
			out.addProperty("downloadUrl", attachment.getDownloadUrl());
			out.addProperty("size", attachment.getSize());

			if (PreviewUtils.hasPreview(attachment)) {
				out.add("preview", PreviewUtils.createJsonPreview(attachment));
			}

			return out;
		} catch (Exception e) {
			Logger.error("[FileAttachmentSerializer] Error serializing attachment: %s", attachment);
			Logger.debug(e, "[FileAttachmentSerializer] Error serializing attachment: %s", attachment);
			return null;
		}
	}
}

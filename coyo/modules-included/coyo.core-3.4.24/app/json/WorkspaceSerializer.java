package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.workspace.Workspace;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a workspace.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class WorkspaceSerializer implements JsonSerializer<Workspace> {

	@Override
	public JsonElement serialize(Workspace workspace, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", workspace.id);
		out.addProperty("name", workspace.getDisplayName());
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", workspace.id);
		args.put("appId", null);
		args.put("slug", workspace.getSlug());
		args.put("appSlug", null);
		out.addProperty("link", Router.reverse("Workspaces.show", args).url);
		
		return out;
	}

}

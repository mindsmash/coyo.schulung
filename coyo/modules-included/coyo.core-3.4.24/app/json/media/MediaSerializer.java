package json.media;

import java.lang.reflect.Type;

import models.User;
import models.media.Media;
import security.CheckCaller;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Collaboration;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class MediaSerializer implements JsonSerializer<Media> {

	private User user;

	public MediaSerializer(User connectedUser) {
		user = connectedUser;
	}

	@Override
	public JsonElement serialize(Media media, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		out.addProperty("id", media.id);
		out.addProperty("senderId", media.getSender().id);
		out.addProperty("clazz", media.getClass().getName());
		out.addProperty("userLikes", media.likes(user));
		out.addProperty("typeKey", media.getTypeKey());
		out.addProperty("typeName", media.getTypeName());

		out.addProperty("collectionId", media.collection.id);
		out.addProperty("title", media.title);
		out.addProperty("filename", media.filename);
		out.addProperty("displayName", media.getDisplayName());
		out.addProperty("generatingVariants", media.isGeneratingVariants());

		out.addProperty("thumbUrl", media.getThumbUrl());
		out.addProperty("downloadUrl", media.getDownloadUrl());

		out.addProperty("modified", media.created.getTime());
		out.addProperty("created", media.created.getTime());

		out.add("creator", context.serialize(media.creator));

		out.addProperty("canCreateComment", CheckCaller.check(Permission.CREATE_COMMENT, media));
		out.addProperty("canComment", CheckCaller.check(Permission.COMMENT_MEDIA, media));
		out.addProperty("canLike", CheckCaller.check(Permission.LIKE_MEDIA, media));

		out.addProperty("canEdit", CheckCaller.check(Permission.EDIT_MEDIA_ROOM, media.collection.room.id));
		out.addProperty("canShare", media.isSharable() && CheckCaller.check(Permission.SHARE, media));

		out.addProperty("isCover", media == media.collection.getCover());

		out.addProperty("next", media.getNext().id);
		out.addProperty("previous", media.getPrevious().id);

		// TODO: remove when sharing is done through AngularJS
		out.addProperty("shareUrl", Collaboration.getShareUrl(media));

		media.jsonSerializeCustomFields(out, user, context);

		return out;
	}

}

package json.media;

import java.lang.reflect.Type;

import security.CheckCaller;
import models.media.MediaRoom;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class MediaRoomSerializer implements JsonSerializer<MediaRoom> {

	@Override
	public JsonElement serialize(MediaRoom room, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		out.addProperty("id", room.id);
		out.addProperty("senderId", room.sender.id);
		out.addProperty("canEdit", CheckCaller.check(Permission.EDIT_MEDIA_ROOM, room.id));
		out.add("collections", context.serialize(room.collections));

		return out;
	}

}

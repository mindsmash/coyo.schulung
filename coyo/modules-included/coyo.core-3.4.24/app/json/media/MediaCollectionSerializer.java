package json.media;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import play.mvc.Router;
import security.CheckCaller;
import models.media.MediaCollection;
import models.media.MediaRoom;
import acl.Permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import controllers.Collaboration;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class MediaCollectionSerializer implements JsonSerializer<MediaCollection> {

	@Override
	public JsonElement serialize(MediaCollection collection, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		out.addProperty("id", collection.id);
		out.addProperty("roomId", collection.room.id);
		out.addProperty("name", collection.name);
		out.addProperty("uid", collection.uid);
		out.addProperty("coverUrl", collection.getCoverUrl());
		out.addProperty("mediaCount", collection.getMediaCount());
		out.addProperty("canEdit", CheckCaller.check(Permission.EDIT_MEDIA_ROOM, collection.room.id));
		out.addProperty("canShare", collection.isSharable() && CheckCaller.check(Permission.SHARE, collection));
		
		// TODO: remove when sharing is done through AngularJS
		out.addProperty("shareUrl", Collaboration.getShareUrl(collection));

		return out;
	}

}

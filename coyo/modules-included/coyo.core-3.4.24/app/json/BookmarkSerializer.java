package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import models.bookmark.Bookmark;
import play.mvc.Router;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a workspace.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class BookmarkSerializer implements JsonSerializer<Bookmark> {

	@Override
	public JsonElement serialize(Bookmark bookmark, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", bookmark.id);
		out.addProperty("name", bookmark.label);
		out.addProperty("link", bookmark.url);
		out.addProperty("global", bookmark.isGlobal());
		return out;
	}

}

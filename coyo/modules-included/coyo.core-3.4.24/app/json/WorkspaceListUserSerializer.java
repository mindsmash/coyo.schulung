package json;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import acl.Permission;
import models.User;
import play.mvc.Router;
import security.CheckCaller;
import session.UserLoader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializes a minimal user representation used for lists.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class WorkspaceListUserSerializer implements JsonSerializer<User> {

	@Override
	public JsonElement serialize(User user, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", user.id);
		out.addProperty("fullName", user.getFullName());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", user.id);
		args.put("slug", user.slug);
		out.addProperty("wallUrl", Router.reverse("Users.wall", args).url);

		out.addProperty("thumbUrl", user.getThumbURL());

		out.addProperty("accessUser", CheckCaller.check(acl.Permission.ACCESS_USER, user.id));
		return out;
	}

}

package json.innoapp;

import acl.AppsPermission;
import acl.Permission;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import controllers.Security;
import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.comments.CommentFileAttachmentSerializer;
import json.comments.CommentSerializer;
import models.comments.CommentFileAttachment;
import models.comments.Commentable;
import models.Sender;
import models.User;
import models.app.InnoApp;
import models.app.inno.*;
import models.comments.Comment;
import session.UserLoader;
import theme.Theme;
import utils.HibernateUtils;
import utils.JPAUtils;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * We use this serializer to avoid cyclic serialization by idea.linkedIdeas
 */
public class MinimalIdeaSerializer implements JsonSerializer<Idea> {
	private User usr;
	private InnoApp app;

	public MinimalIdeaSerializer(final User user, final InnoApp app) {
		this.usr = user;
		this.app = app;
	}

	@Override
	public JsonElement serialize(Idea idea, Type type, JsonSerializationContext context) {
		if (app == null) {
			app = idea.getApp();
		}

		final JsonObject out = new JsonObject();

		out.add("id", context.serialize(idea.id));
		out.add("created", context.serialize(idea.created));
		out.add("modified", context.serialize(idea.modified));
		out.addProperty("senderId", idea.getSender().id);

		out.add("clazz", context.serialize(idea.getClass().getName()));

		out.add("creator", context.serialize(idea.creator));
		out.add("status", context.serialize(idea.status));


		out.add("category", context.serialize(idea.getCategory(app)));
		out.add("commentsLength", context.serialize(idea.comments.size()));
		out.addProperty("following", IdeaFollower.doesFollow(idea, usr));

		out.add("ratings", context.serialize(idea.ratings));
		out.addProperty("rating", idea.getUserRating(usr));
		out.addProperty("averageRating", new DecimalFormat("#.#").format(idea.calculateAverageRating()));
		out.addProperty("ttl", app.ttl);
		out.addProperty("isAnon", idea.isAnon);
		out.addProperty("anonThumbUrl", Theme.getResourceUrl("images/user-nopic.png"));
		out.addProperty("description", idea.description);
		out.addProperty("title", idea.title);
		out.addProperty("statusComment", idea.statusComment);
		out.addProperty("deletable", Security.check(Permission.DELETE_CONTAINER_ITEM, idea.id));
		out.addProperty("editable", Security.check(AppsPermission.EDIT_IDEA, app.sender, usr, app.id, idea.id));
		out.addProperty("url", controllers.InnoApp.getIdeaUrl(app.id, idea.id));

		out.addProperty("canCreateComment", true);
		out.addProperty("owner", idea.creator == usr);

		return out;
	}
}
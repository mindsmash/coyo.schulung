package json.innoapp;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.app.inno.Category;
import models.app.inno.Idea;

import java.lang.reflect.Type;

public class InnoAppCategorySerializer implements JsonSerializer<Category> {

	@Override
	public JsonElement serialize(Category item, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();

		out.addProperty("id", item.id);
		out.addProperty("name", item.name);
		out.addProperty("usedByItem", Idea.isCategoryUsed(item));

		return out;
	}
}
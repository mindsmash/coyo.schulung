package json.innoapp;

import acl.AppsPermission;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import controllers.Security;
import models.User;
import models.app.InnoApp;

import java.lang.reflect.Type;

/**
 * InnoAppSerializer JSON Serializer
 */
public class InnoAppSerializer implements JsonSerializer<InnoApp> {
	private User usr;

	public InnoAppSerializer(final User user) {
		this.usr = user;
	}

	@Override
	public JsonElement serialize(InnoApp innoApp, Type type, JsonSerializationContext context) {
		JsonObject o = new JsonObject();
		o.addProperty("id", innoApp.id);
		o.addProperty("title", innoApp.title);
		o.addProperty("description", innoApp.description);
		o.add("categories", context.serialize(innoApp.categories));
		o.add("visibility", context.serialize(innoApp.visibility));
		o.add("ideasHiddenField", context.serialize(innoApp.ideasHiddenField));
		o.add("allowDeanonymization", context.serialize(innoApp.allowDeanonymization));
		o.addProperty("commentsAllowed", innoApp.commentsAllowed);
		o.addProperty("canBeRevoked", innoApp.canBeRevoked);
		o.addProperty("archived", innoApp.archived);
		o.addProperty("ttl", innoApp.ttl);
		o.addProperty("allowSearchByAuthor", innoApp.allowSearchByAuthor);
		o.add("moderators", context.serialize(innoApp.moderators));
		o.add("container", context.serialize(innoApp.container));
		o.add("senderDisplayName", context.serialize(innoApp.container.sender.getDisplayName()));

		return o;
	}
}
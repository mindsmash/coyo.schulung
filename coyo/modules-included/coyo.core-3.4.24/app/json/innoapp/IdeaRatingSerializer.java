package json.innoapp;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.app.inno.IdeaRating;

import java.lang.reflect.Type;

public class IdeaRatingSerializer implements JsonSerializer<IdeaRating> {

	public IdeaRatingSerializer() {

	}

	@Override
	public JsonElement serialize(IdeaRating ideaRating, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();

		out.add("created", context.serialize(ideaRating.created));
		out.add("modified", context.serialize(ideaRating.modified));

		out.add("id", context.serialize(ideaRating.id));

		out.add("clazz", context.serialize(ideaRating.getClass().getName()));
		out.add("user", context.serialize(ideaRating.user));
		out.add("rating", context.serialize(ideaRating.rating));

		return out;
	}
}
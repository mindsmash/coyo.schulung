package json.innoapp;

import acl.AppsPermission;
import acl.Permission;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import controllers.Security;
import models.User;
import models.app.InnoApp;
import models.app.inno.*;
import theme.Theme;

import java.lang.reflect.Type;

/**
 * We use this serializer to avoid cyclic serialization by idea.linkedIdeas
 */
public class NestedIdeaSerializer implements JsonSerializer<Idea> {
	private User usr;

	public NestedIdeaSerializer(final User user) {
		this.usr = user;
	}

	@Override
	public JsonElement serialize(Idea idea, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();
		InnoApp app = InnoApp.find("container = ?", idea.container).first();

		out.add("id", context.serialize(idea.id));
		out.addProperty("senderId", idea.getSender().id);
		out.add("clazz", context.serialize(idea.getClass().getName()));
		out.add("values", context.serialize(idea.values));
		
		out.add("following", context.serialize(IdeaFollower.doesFollow(idea, usr)));

		out.add("creator", context.serialize(idea.creator));

		out.add("category", context.serialize(idea.getCategory(app)));

		out.add("status", context.serialize(idea.status));
		out.add("comments", context.serialize(idea.comments));
		out.add("commentsLength", context.serialize(idea.comments.size()));


		out.addProperty("ttl", idea.getApp().ttl);
		out.addProperty("isAnon", idea.isAnon);
		out.addProperty("anonThumbUrl", Theme.getResourceUrl("images/user-nopic.png"));

		out.addProperty("description", idea.description);
		out.addProperty("title", idea.title);
		out.addProperty("editable", Security.check(Permission.EDIT_CONTAINER_ITEM, idea.id));
		out.addProperty("deletable", Security.check(Permission.DELETE_CONTAINER_ITEM, idea.id));
		out.addProperty("statusComment", idea.statusComment);

		out.add("created", context.serialize(idea.created));
		out.add("modified", context.serialize(idea.modified));

		out.addProperty("editable", Security.check(AppsPermission.EDIT_IDEA, app.sender, usr, app.id, idea.id));
		out.addProperty("url", controllers.InnoApp.getIdeaUrl(app.id, idea.id));

		out.addProperty("canCreateComment", true);

		if (idea.creator.id == usr.id) {
			out.addProperty("owner", true);
		} else {
			out.addProperty("owner", false);
		}

		return out;
	}
}
package json.innoapp;

import acl.AppsPermission;
import acl.Permission;

import com.google.gson.*;

import controllers.Security;
import json.HibernateProxyTypeAdapter;
import json.comments.CommentFileAttachmentSerializer;
import json.list.ContainerSerializer;
import json.list.FieldOptionSerializer;
import json.list.FieldSerializer;
import json.list.ItemValueSerializer;
import json.Dates;
import json.FileAttachmentSerializer;
import json.MinimalUserSerializer;
import json.comments.CommentSerializer;
import models.comments.CommentFileAttachment;
import models.comments.Commentable;
import models.Sender;
import models.User;
import models.app.InnoApp;
import models.app.inno.*;
import models.comments.Comment;
import models.container.Container;
import models.container.Field;
import models.container.ItemValue;
import models.container.fields.OptionsFieldOption;
import models.container.values.attachments.FileItemAttachment;
import session.UserLoader;
import theme.Theme;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.Date;

public class IdeaSerializer implements JsonSerializer<Idea> {
	private User usr;

	public IdeaSerializer(final User user) {
		this.usr = user;
	}

	@Override
	public JsonElement serialize(Idea idea, Type type, JsonSerializationContext context) {
		final JsonObject out = new JsonObject();
		final InnoApp app = InnoApp.find("container = ?", idea.container).first();

		out.add("created", context.serialize(idea.created));
		out.add("modified", context.serialize(idea.modified));
		out.addProperty("canCreateComment", true);
		out.add("id", context.serialize(idea.id));
		out.addProperty("senderId", idea.getSender().id);
		out.add("clazz", context.serialize(idea.getClass().getName()));
		out.add("values", context.serialize(idea.values));
		out.add("creator", context.serialize(idea.creator));
		out.add("category", context.serialize(idea.getCategory(app)));

		if (Security.check(
				AppsPermission.MODERATE_IDEA, app.sender, usr, app.id)) {
			out.addProperty("hiddenField", idea.hiddenField);
		}

		out.addProperty("anonThumbUrl", Theme.getResourceUrl("images/user-nopic.png"));
		out.addProperty("isAnon", idea.isAnon);
		out.addProperty("description", idea.description);
		out.addProperty("title", idea.title);
		out.addProperty("ttl", idea.getApp().ttl);

		out.add("status", context.serialize(idea.status));
		out.addProperty("statusComment", idea.statusComment);

		out.addProperty("following", IdeaFollower.doesFollow(idea, usr));

		out.add("ratings", context.serialize(idea.ratings));
		out.addProperty("rating", idea.getUserRating(usr));
		out.addProperty("averageRating", new DecimalFormat("#.#").format(idea.calculateAverageRating()));

		out.add("comments", createBuilder(idea).create().toJsonTree(idea.comments));
		out.add("commentsLength", context.serialize(idea.comments.size()));


		// frontend permissions
		out.addProperty("editable", Security.check(Permission.EDIT_CONTAINER_ITEM, idea.id));
		out.addProperty("deletable", Security.check(Permission.DELETE_CONTAINER_ITEM, idea.id));
		out.addProperty("editable", Security.check(AppsPermission.EDIT_IDEA, app.sender, usr, app.id, idea.id));

		// idea url (deep linking)
		out.addProperty("url", controllers.InnoApp.getIdeaUrl(app.id, idea.id));

		// create builder in serializer using serializer that excludes this field to avoid circular references
		out.add("linkedIdeas", createBuilder(idea).create().toJsonTree(idea.linkedIdeas));

		// user == creator?
		if (idea.creator.id == usr.id) {
			out.addProperty("owner", true);
		} else {
			out.addProperty("owner", false);
		}

		return out;
	}

	public GsonBuilder createBuilder(Commentable commentable) {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Container.class, new ContainerSerializer(true));
		builder.registerTypeAdapter(Idea.class, new NestedIdeaSerializer(usr)); // break circular reference!
		builder.registerTypeHierarchyAdapter(Field.class, new FieldSerializer());
		builder.registerTypeHierarchyAdapter(OptionsFieldOption.class, new FieldOptionSerializer());
		builder.registerTypeHierarchyAdapter(FileItemAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeHierarchyAdapter(ItemValue.class, new ItemValueSerializer());
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(Category.class, new InnoAppCategorySerializer());
		builder.registerTypeAdapter(CommentFileAttachment.class, new CommentFileAttachmentSerializer(commentable));
		builder.registerTypeAdapter(Comment.class, new CommentSerializer(usr, commentable));
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}
}
package json;

import java.lang.reflect.Type;

import models.wall.attachments.LinkPostAttachment;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializer for attachments.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class LinkAttachmentSerializer implements JsonSerializer<LinkPostAttachment> {

	@Override
	public JsonElement serialize(LinkPostAttachment attachment, Type type, JsonSerializationContext context) {
		JsonObject out = new JsonObject();
		out.addProperty("id", attachment.id);
		out.addProperty("type", "link");
		out.addProperty("url", attachment.url);
		return out;
	}

}

package upgrade;

import java.util.ArrayList;
import java.util.List;

import jobs.SearchReindexer;
import liquibase.exception.DatabaseException;
import models.Tenant;
import apps.AppManager;
import apps.MediaAppDescriptor;
import fishr.Fishr;
import fishr.ManagedIndex;

/**
 * Is executed when app is upgraded to a higher version. Allows for the implementation of custom upgrade logic. Consider
 * multitenancy.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class UpgradeScript {

	/**
	 * First
	 */
	public void preLiquibase() {
	}

	/**
	 * Second
	 */
	public void postLiquibase() throws DatabaseException {
	}

	/**
	 * After hibernate DDL (third)
	 */
	public void postHibernate() throws Exception {
	}

	/**
	 * After hibernate DDL per tenant. Here, the JPA context is initialized.
	 */
	public void upgradeTenant(Tenant tenant) throws Exception {
		new SearchReindexer(tenant).in(30);
	}

	/**
	 * Last
	 */
	public void finish() {
	}
}
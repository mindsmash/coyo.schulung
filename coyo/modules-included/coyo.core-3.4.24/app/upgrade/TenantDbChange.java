package upgrade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Table;

import liquibase.change.custom.CustomSqlChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.NotNullConstraint;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.AddColumnStatement;
import liquibase.statement.core.UpdateStatement;
import models.TenantModel;

import org.hibernate.Session;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;

import play.Logger;
import play.Play;
import play.db.jpa.JPA;

public class TenantDbChange implements CustomSqlChange {

	@Override
	public String getConfirmationMessage() {
		return "Successfully modified tables for single tenant setup.";
	}

	@Override
	public void setUp() throws SetupException {
	}

	@Override
	public void setFileOpener(ResourceAccessor resourceAccessor) {
	}

	@Override
	public ValidationErrors validate(Database database) {
		return null;
	}

	@Override
	public SqlStatement[] generateStatements(Database database) throws CustomChangeException {
		List<SqlStatement> r = new ArrayList<SqlStatement>();

		List<Class> classes = Play.classloader.getAssignableClasses(TenantModel.class);
		for (Class clazz : classes) {
			if (clazz.getSuperclass() == TenantModel.class) {
				String tableName = getTableName(clazz);

				Logger.debug("creating tenant update statement for: %s", clazz.getName());

				r.add(new AddColumnStatement(null, null, tableName, "tenant_id", "bigint", null,
						new NotNullConstraint()));

				UpdateStatement update = new UpdateStatement(null, null, tableName);
				update.addNewColumnValue("tenant_id", 1);
				r.add(update);
			}
		}

		return r.toArray(new SqlStatement[r.size()]);
	}

	private String getTableName(Class clazz) {
		Table table = (Table) clazz.getAnnotation(Table.class);
		if (table != null) {
			return table.name();
		}

		Session session = (Session) JPA.em().getDelegate();
		ClassMetadata hibernateMetadata = session.getSessionFactory().getClassMetadata(clazz);
		if (hibernateMetadata instanceof AbstractEntityPersister) {
			AbstractEntityPersister persister = (AbstractEntityPersister) hibernateMetadata;
			return persister.getTableName();
		}

		throw new IllegalStateException("cannot find table name for class: " + clazz.getName());
	}
}

package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.UserNotificationSettings.MailInterval;
import play.jobs.On;

@On("0 0 * * * ?")
public class UpdateMailer1H extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new UpdateMailer(tenant, MailInterval.HOURLY);
	}
}
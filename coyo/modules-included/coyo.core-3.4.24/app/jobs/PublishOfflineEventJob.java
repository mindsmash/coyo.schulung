package jobs;

import multitenancy.MTA;
import onlinestatus.OnlineStatusServiceFactory;
import onlinestatus.UserOnlineStatus;
import play.Logger;
import play.jobs.Job;

public class PublishOfflineEventJob extends Job {

	private Long userId;
	private UserOnlineStatus currentStatus;

	public PublishOfflineEventJob(final Long userId, final UserOnlineStatus currentStatus) {
		this.userId = userId;
		this.currentStatus = currentStatus;
	}

	@Override
	public void doJob() throws Exception {
		try {
			MTA.forceMultitenancyDeactivation();
			OnlineStatusServiceFactory.raiseEventIfNecessary(userId, currentStatus, new UserOnlineStatus(
					UserOnlineStatus.OnlineStatus.OFFLINE));
		} catch (Exception ex) {
			Logger.error(ex, "[OnlineStatusService] Error: " + ex.getLocalizedMessage());
		} finally {
			MTA.clearThreadLocals();
		}
	}
}

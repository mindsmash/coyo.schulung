package jobs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import models.User;
import multitenancy.MTA;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import events.EventQueue;
import events.EventService;

/**
 * Closes all EventQueues that are no longer needed because the user is inactive.
 * 
 * @author Piers Wermbter, mindsmash GmbH
 * 
 */
@Every("2min")
public class EventCleanJob extends Job {

	@Override
	public void doJob() throws Exception {
		MTA.forceMultitenancyDeactivation();
		
		Map<Long, EventQueue> queues = EventService.getQueues();
		List<Long> toClose = new ArrayList<Long>();
		Logger.debug("running EventQueue cleaner: %s open EventStreams", queues.size());
		for (Entry<Long, EventQueue> entry : queues.entrySet()) {
			User user = User.findById(entry.getKey());
			if (user != null && !user.isOnline()) {
				toClose.add(entry.getKey());
			}
		}
		for (Long id : toClose) {
			EventService.closeQueue(id);
		}
	}
}

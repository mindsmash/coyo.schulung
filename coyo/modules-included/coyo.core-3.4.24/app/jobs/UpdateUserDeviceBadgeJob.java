package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;

import java.util.List;

import models.AbstractTenant;
import models.Sender;
import models.User;
import play.db.jpa.JPA;
import play.jobs.On;
import plugins.play.PushNotificationPlugin;

/**
 * This job updates the badge for all users' devices.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0/30 * * * ?")
public class UpdateUserDeviceBadgeJob extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {
			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				// include external users
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);

				List<User> users = User.find("SELECT u FROM User u WHERE u.active = true AND u.deleted = false")
						.fetch();

				for (User user : users) {
					PushNotificationPlugin.push(user, new Long(user.getTotalUnreadCount()).intValue());
				}
			}
		};
	}
}

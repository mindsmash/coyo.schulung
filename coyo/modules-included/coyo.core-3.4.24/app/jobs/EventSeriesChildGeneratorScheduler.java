package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;

import java.util.Arrays;
import java.util.List;

import models.AbstractTenant;
import models.event.EventSeries;
import models.event.EventSeriesChild;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

import play.Logger;
import play.db.jpa.JPA;
import play.jobs.On;
import utils.DateUtils;

/**
 * Schedules the event series child generator task.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
@On("0 15 0 1/1 * ? *")
public class EventSeriesChildGeneratorScheduler extends TenantTaskScheduler {
	@Override
	protected TenantTask createTask(final AbstractTenant tenant) {
		return new EventSeriesChildGeneratorTask(tenant);
	}

	/**
	 * Generates new child instances for event series.
	 * 
	 * @see models.event.EventSeries
	 * @see models.event.EventSeriesChild
	 */
	public static class EventSeriesChildGeneratorTask extends TenantTask {
		private Long eventSeriesId;

		public EventSeriesChildGeneratorTask(final AbstractTenant tenant) {
			super(tenant.id);
		}

		public EventSeriesChildGeneratorTask(final AbstractTenant tenant, final Long eventSeriesMasterId) {
			this(tenant);
			this.eventSeriesId = eventSeriesMasterId;
		}

		@Override
		public void execute(final AbstractTenant tenant) throws Exception {
			final DateTime referenceDate = DateTime.now(DateTimeZone.UTC);
			Logger.debug("starting event series child generator with reference date %s", referenceDate);

			List<EventSeries> eventSeriesList;
			if (eventSeriesId != null) {
				final EventSeries eventSeries = EventSeries.<EventSeries> findById(eventSeriesId);
				/*
				 * This could happen if the job has been started before the entity was committed.
				 */
				if (eventSeries == null) {
					Logger.debug("event series master for given ID %d was null, rescheduling job to run again in 1s",
							eventSeriesId);
					new EventSeriesChildGeneratorTask(tenant, eventSeriesId).in("1s");
					return;
				}
				eventSeriesList = Arrays.asList(eventSeries);
			} else {
				eventSeriesList = EventSeries.find(
						"isCancelled = ? AND (childrenGeneratedUntil IS NULL OR startDate < ?)", false, referenceDate)
						.fetch();
			}

			Logger.debug("processing %d event series", eventSeriesList.size());
			for (final EventSeries event : eventSeriesList) {
				/*
				 * Calculate the recurrence set for the given event series master. The interval used for the generation
				 * is derived from the event's date properties:
				 * 
				 * If it is a "new" event, i.e. there have never been children generated for it, the interval will go
				 * from the event's start date until...
				 * 
				 * - (start date + 1 year) for frequency daily and weekly,
				 * 
				 * - (start date + 3 years) for frequency monthly and
				 * 
				 * - (start date + 10 years for frequency yearly.
				 * 
				 * Otherwise, the interval will be based on the stored date up to which the children have been
				 * generated.
				 */
				final DateTime periodStart = event.childrenGeneratedUntil == null ? event.startDate
						: event.childrenGeneratedUntil.withZone(DateTimeZone.UTC);
				final DateTime periodEnd = getPeriodEnd(referenceDate, event);

				// subtract 1 msec means exclusive this day
				event.childrenGeneratedUntil = periodEnd.minus(1);

				// do nothing if start is after end, which means the event has
				// not started yet
				if (periodStart.isAfter(periodEnd)) {
					Logger.debug(
							"not calculating recurrence set for event '%s' as period start (%s) is after period end (%s)",
							event.name, periodStart, periodEnd);
					continue;
				}

				Logger.debug("calculating recurrence set for event '%s' in period %s till %s", event.name, periodStart,
						periodEnd);
				final List<Interval> recurrenceSet = DateUtils.getRecurrenceSet(event.recurrenceRule, event.startDate,
						event.endDate, event.getCreationTimeZone(), new Interval(periodStart, periodEnd));

				Logger.debug("recurrence set size: %d for event '%s'", recurrenceSet.size(), event.name);
				int numOfSaves = 0;
				for (final Interval interval : recurrenceSet) {
					// flush the EM every now and then to avoid slow down wrapped in a try catch block since it may
					// fail and shouldn't cancel the process.
					try {
						if ((numOfSaves++ % 100) == 0) {
							JPA.em().flush();
						}
					} catch (Exception e) {
						// fail silently
					}

					Logger.debug("creating new instance of event '%s' for interval: %s", event.name, interval);
					// save directly, not via the parent due to Hibernate problems
					final EventSeriesChild child = event.createChildEvent(interval);
					event.children.add(child);
					child.save();
				}

				// the new childrenGeneratedUntil date has to be saved
				event.save();
			}

			Logger.debug("finished event series child generator");
		}

		/**
		 * Get the end of the period for which new children should generated. The amount of years that we go ahead into
		 * future depends on the series' recurrence frequency.
		 */
		private DateTime getPeriodEnd(final DateTime referenceDate, final EventSeries event) {
			int plusYears = EventSeries.getNumberOfYearsForChildGeneration(event.recurrenceRule);
			if (event.childrenGeneratedUntil != null) {
				return referenceDate.plusYears(plusYears);
			}
			return event.startDate.plusYears(plusYears);
		}
	}
}

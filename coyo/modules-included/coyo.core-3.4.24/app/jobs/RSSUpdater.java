package jobs;

import com.sun.syndication.feed.synd.SyndEntry;
import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.app.RSSApp;
import models.wall.Wall;
import models.wall.attachments.LinkPostAttachment;
import models.wall.post.Post;
import models.wall.post.SimplePost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPA;
import play.jobs.On;
import utils.ErrorHelper;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Job to get the latest RSS articles.
 *
 * @author Drews Clausen, Marko Ilic, Jan Marquardt
 */
@On("0 0/10 * * * ?")
public class RSSUpdater extends TenantTaskScheduler {
	private static final Logger LOG = LoggerFactory.getLogger(RSSUpdater.class);

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {

			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				List<RSSApp> apps = RSSApp.find("postOnWall = true").fetch();

				LOG.debug("[RSSUpdater] Running RSS update. Processing " + apps.size() + " feed polls.");

				// iterate over all RSS apps to get latest updates
				for (Iterator<RSSApp> pageIterator = apps.iterator(); pageIterator.hasNext(); ) {
					RSSApp app = pageIterator.next();

					if (app.active && app.sender.getDefaultWall() != null) {
						try {
							List entries = app.getEntries();
							if (entries != null) {
								Collections.reverse(entries);
								if (entries != null) {
									// iterate over all items contained in RSS
									// Feed
									for (Iterator itemIterator = entries.iterator(); itemIterator.hasNext(); ) {
										createPost(app, (SyndEntry) itemIterator.next());
									}
								}

								app.lastUpdated = new Date();
								app.save();

								JPA.em().flush();
							}
						} catch (Exception e) {
							ErrorHelper.handleWarning(LOG, e,
									"[RSSUpdater] Could not process rss feed '%s' due to an exception.", app.rssUrl);
						}
					}
				}

			}

			private void createPost(RSSApp app, SyndEntry item) {
				try {
					final Wall wall = app.sender.getDefaultWall();

					// check if post for this entry already exists
					final List<LinkPostAttachment> existing = LinkPostAttachment.find("url = ?", item.getLink())
							.fetch();
					for (LinkPostAttachment link : existing) {
						if (link.post.author == app.sender && link.post.wall == wall) {
							return;
						}
					}

					final Post rssPost = new SimplePost();

					// try to find publication date
					Date published = item.getPublishedDate();
					if (published == null) {
						published = item.getUpdatedDate();
					}

					/*
					 * A publish date in the future results in errors in the activity stream. Therefore we set the
					 * current date instead.
					 */
					if (published.after(new Date())) {
						published = new Date();
						LOG.warn(String.format(
								"[RSSUpdater] Item with title '%s' has a publication date in the future. Setting the date to now.",
								item.getTitle()));
					}

					rssPost.created = published;
					rssPost.author = app.sender;
					rssPost.message = item.getTitle();
					rssPost.wall = wall;

					rssPost.save();

					final LinkPostAttachment attachment = new LinkPostAttachment();
					attachment.url = item.getLink();
					attachment.post = rssPost;
					attachment.save();
				} catch (Exception e) {
					ErrorHelper.handleWarning(LOG, e, "[RSSUpdater] Error parsing RSS item [%s]", item);
				}
			}
		};
	}
}

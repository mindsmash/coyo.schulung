package jobs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import models.AbstractTenant;
import models.app.FilesApp;
import multitenancy.MTA;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.libs.Codec;

/**
 * 
 * Job to zip a File or Folder from a {@link FilesApp}.
 * 
 * @author mindsmash GmbH
 * 
 */
public class Zip extends TenantJob<java.io.File> {

	public static final String ZIP_DIR = Play.configuration.getProperty("zips.path");

	private String name;
	private Long filesAppId;
	private List<String> files;
	private Set<String> usedPaths = new HashSet<String>();

	public Zip(Long filesAppId, String name, List<String> files) {
		this(MTA.getActiveTenant(), filesAppId, name, files);
	}

	public Zip(AbstractTenant tenant, Long filesAppId, String name, List<String> files) {
		super(tenant);
		this.filesAppId = filesAppId;
		this.name = strippedFileName(name);
		// needs to reloaded due to new EntityManager context
		this.files = files;
	}

	@Override
	public java.io.File doTenantJobWithResult() throws Exception {
		FilesApp app = FilesApp.findById(filesAppId);

		// create zip
		java.io.File zip = new File(Play.tmpDir, Codec.UUID() + ".zip");
		zip.createNewFile();

		// create zip stream
		FileOutputStream fos = new FileOutputStream(zip);
		ZipOutputStream zos = new ZipOutputStream(fos);

		Logger.debug("starting to zip[%s]", name);

		// root
		ZipEntry zipEntry = new ZipEntry(name + "/");
		zos.putNextEntry(zipEntry);

		for (String uid : files) {
			models.app.files.File file = app.getFile(uid);
			zipFile(name, file, zos);
		}

		// close
		Logger.debug("finished zipping[%s]", name);

		zos.finish();
		zos.close();
		fos.close();

		return zip;
	}

	private void zipFile(String parentPath, models.app.files.File file, ZipOutputStream zos) throws IOException {
		String path = uniqueStrippedFilePath(parentPath, file);

		// add file to zip
		if (!file.isFolder()) {
			Logger.debug("zipping process for file[%s]: adding file[%s]", file, path);

			ZipEntry zipEntry = new ZipEntry(path);
			zos.putNextEntry(zipEntry);
			IOUtils.copy(file.getCurrentVersion().get(), zos);
		} else { // add folder to zip
			Logger.debug("zipping process for file[%s]: adding folder[%s]", file, path);

			ZipEntry zipEntry = new ZipEntry(path + "/");
			zos.putNextEntry(zipEntry);
		}

		// add children
		if (file.isFolder()) {
			for (models.app.files.File child : file.getChildren()) {
				zipFile(path, child, zos);
			}
		}
	}

	private String strippedFileName(String name) {
		// strip, fix and replace
		name = name.replaceAll("/", "\\\\"); // TODO : find a solution for using
												// slashes in filenames
		name = StringUtils.strip(name);

		return name;
	}

	private String uniqueStrippedFilePath(String parentPath, models.app.files.File file) {
		String name = strippedFileName(file.getName());

		// increment if duplicate
		String path = parentPath + "/" + name;
		while (usedPaths.contains(path)) {
			name = incrementString(name);
			path = parentPath + "/" + name;
		}
		usedPaths.add(path);

		return path;
	}

	private String incrementString(String string) {
		if (!string.contains("_")) {
			return string + "_2";
		}

		if (string.endsWith("_")) {
			return string + "2";
		}

		if (string.charAt(string.length() - 2) == '_') {
			try {
				int parseInt = Integer.parseInt(string.charAt(string.length() - 1) + "");
				return string.substring(0, string.length() - 1) + (parseInt + 1);
			} catch (NumberFormatException ignored) {
			}
		}

		return string + "_2";
	}
}

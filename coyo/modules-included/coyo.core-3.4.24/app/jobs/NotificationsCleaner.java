package jobs;

import hazelcast.Task;
import hazelcast.TaskScheduler;
import models.UserNotification;
import models.notification.Notification;
import multitenancy.MTA;
import org.apache.commons.lang.time.DateUtils;
import play.Logger;
import play.Play;
import play.jobs.On;
import utils.JPAUtils;

import java.util.Date;
import java.util.List;

/**
 * Job to delete old notifications.
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0 4 * * ?")
public class NotificationsCleaner extends TaskScheduler {
	private static final int DAYS_TO_KEEP_NOTIFICATIONS = Integer
			.parseInt(Play.configuration.getProperty("coyo.notifications.store.days", "30"));

	@Override
	protected Task createTask() {
		return new Task() {

			@Override
			public void execute() throws Exception {
				Logger.debug("[NotificationsCleaner] start");
				final Date border = DateUtils.addDays(new Date(), -1 * DAYS_TO_KEEP_NOTIFICATIONS);

				try {
					MTA.forceMultitenancyDeactivation();

					UserNotification.delete("DELETE FROM UserNotification un WHERE un.created < ?", border);

					// avoid long lasting lock
					JPAUtils.commitAndRefreshTransaction();

					// avoid foreign key constraint violation as cascade delete of user notifications is not working
					final List<Notification> notifications = Notification
							.find("SELECT n FROM Notification n WHERE n.created < ? AND SIZE(n.posts) = 0 AND SIZE(n.userNotifications) = 0",
									border).fetch();

					for (Notification notification : notifications) {
						for (UserNotification userNotification : notification.userNotifications) {
							userNotification.delete();
						}
						notification.userNotifications.clear();
						notification.delete();
					}

				} catch (Exception ex) {
					Logger.error(ex, "[NotificationsCleaner] " + ex.getLocalizedMessage());
				} finally {
					MTA.clearThreadLocals();
				}

				Logger.debug("[NotificationsCleaner] end");
			}
		};
	}
}

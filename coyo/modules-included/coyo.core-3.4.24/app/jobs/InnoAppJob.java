package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.app.InnoApp;
import models.app.inno.Idea;
import models.app.inno.Status;
import play.Logger;
import play.i18n.Messages;
import play.jobs.On;

import java.util.List;

/**
 * Find ideas that are in state NEW and the days between when they have been created are more than the configured ttl
 * of the innoapp.
 * Those ideas will be transitioned to the state AUDIT.
 */
@On("0 0 21 * * ?")
public class InnoAppJob extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {
			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				Logger.debug("[InnoAppJob] starting daily InnoApp job");

				final List<Idea> expiredIdeas = InnoApp.findExpiredIdeas();
				final String statusComment = Messages.get("app.inno.job.message");

				for (Idea item : expiredIdeas) {
					if (item.getStatus() == Status.NEW) {
						item.setStatus(Status.AUDIT, statusComment);
						item.save();
					}
				}
				Logger.debug("[InnoAppJob] finished daily InnoApp job");
			}
		};
	}
}
package jobs;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import models.statistics.TrackingRecord;
import multitenancy.MTA;

import org.apache.commons.lang3.StringUtils;

import play.Logger;
import play.db.helper.JpqlSelect;
import play.jobs.Job;
import play.jobs.On;
import statistics.StatisticsQueueItem;
import utils.ErrorHelper;

/**
 * Writes statistical data to database
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0 * * * ?")
public class StatisticsPersisterJob extends Job {
	public static BlockingQueue<StatisticsQueueItem> indexQueue = new LinkedBlockingQueue<>();

	@Override
	public void doJob() throws Exception {
		syncQueueItems();
	}

	public static void syncQueueItems() {
		try {
			final Map<String, StatisticsQueueItem> merged = mergeItems();

			// store collected statistics
			for (StatisticsQueueItem item : merged.values()) {
				try {
					MTA.activateMultitenancy(item.getTenant());

					final JpqlSelect select = TrackingRecord.getBaseSelect(item.recordType, new Date());
					if (StringUtils.isNotEmpty(item.discriminator)) {
						select.andWhere("discriminator = ?").param(item.discriminator);
					}

					TrackingRecord r = TrackingRecord.find(select.toString(), select.getParams().toArray()).first();

					// not record found in db we create a new one
					if (r == null) {
						r = new TrackingRecord();
						r.tenant = item.getTenant();
						r.recordType = item.recordType;
						r.discriminator = item.discriminator;
					}

					if (item.increment) {
						r.count = r.count + item.count;
					} else {
						r.count = item.count;
					}

					if (Logger.isDebugEnabled()) {
						Logger.debug("[Statistics] Saving record [type=%s, date=%s, count=%s]", r.recordType, r.date,
								r.count);
					}
					r.save();

				} catch (Exception e) {
					ErrorHelper.handleWarning(e, "[Statistics] Error persisting item: %s. Skipping ...", item);
				} finally {
					MTA.forceMultitenancyDeactivation();
				}
			}
		} catch (Exception e) {
			Logger.error(e, "[Statistics] Error in statistics persister loop run. Rolling back transaction.");
		}
	}

	private static Map<String, StatisticsQueueItem> mergeItems() {
		final Map<String, StatisticsQueueItem> statistics = new HashMap<>();

		// first collect all item values by their key and attributes
		while (!indexQueue.isEmpty()) {
			try {
				final StatisticsQueueItem item = indexQueue.take();
				String key = item.recordType + item.tenantId;

				if (StringUtils.isNotEmpty(item.discriminator)) {
					key = key + item.discriminator;
				}

				// summ up ?
				if (item.increment) {
					item.count = statistics.containsKey(key) ? (statistics.get(key).count + item.count) : item.count;
					statistics.put(key, item);

					// no need to imcrement so lets override item
				} else {
					statistics.put(key, item);
				}
			} catch (Exception e) {
				ErrorHelper.handleWarning(e, "[Statistics] Error summing up item. Skipping ...");
			}
		}

		return statistics;
	}

	public static void persist(StatisticsQueueItem item) {
		indexQueue.offer(item);
	}
}
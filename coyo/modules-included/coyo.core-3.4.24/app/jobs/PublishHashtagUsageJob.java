package jobs;

import java.util.Collection;
import java.util.List;

import javax.persistence.TypedQuery;

import models.AbstractTenant;
import models.HashtagBlackList;
import models.Sender;
import models.User;
import models.notification.HashTagNotification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.JPA;
import session.UserLoader;

/**
 * Notifies all interested users, that a new content with a followed hashtag was published.
 */
public class PublishHashtagUsageJob extends TenantJob {

	private static final Logger LOG = LoggerFactory.getLogger(PublishHashtagUsageJob.class);

	private Collection<String> hashtags;
	private String entityUID;
	private Sender user;

	public PublishHashtagUsageJob(final Collection<String> hashtags, final AbstractTenant tenant,
			final String entityUID, final Sender user) {
		super(tenant);
		this.hashtags = hashtags;
		this.entityUID = entityUID;
		this.user = user;
	}

	@Override
	public void doTenantJob() throws Exception {
		// skip notification for blacklisted hashtags
		for (String blacklisted : HashtagBlackList.getHashtags()) {
			hashtags.remove(blacklisted);
		}

		try {
			for (String hashtag : hashtags) {
				final StringBuffer sql = new StringBuffer();

				sql.append("SELECT us FROM User us WHERE :hashtag IN elements(us.hashtags)");

				if (user != null) {
					sql.append(" AND us != :user");
				}

				final TypedQuery<User> query = JPA.em().createQuery(sql.toString(), User.class);
				query.setParameter("hashtag", hashtag);
				if (user != null) {
					query.setParameter("user", user);
				}

				final List<User> users = query.getResultList();
				HashTagNotification.raise(users, hashtag, entityUID);
			}
		} catch (Exception ex) {
			LOG.error("[Hashtags] Error: " + ex.getLocalizedMessage(), ex);
		}
	}
}

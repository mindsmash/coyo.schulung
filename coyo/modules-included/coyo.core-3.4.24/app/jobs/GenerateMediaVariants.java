package jobs;

import models.AbstractTenant;
import models.media.Media;
import play.Logger;
import utils.ErrorHelper;

public class GenerateMediaVariants extends TenantJob {

	private Long mediaId;
	private int trial = 1;
	private int maxTrials = 3;

	public GenerateMediaVariants(AbstractTenant tenant, Long mediaId) {
		super(tenant);
		this.mediaId = mediaId;
	}

	public GenerateMediaVariants(AbstractTenant tenant, Long mediaId, int trial) {
		this(tenant, mediaId);
		this.trial = trial;
	}

	@Override
	public void doTenantJob() throws Exception {
		Logger.debug("[GenerateMediaVariants] Starting media variant generation for media: %s (trial %s)", mediaId,
				trial);

		Media media = Media.findById(mediaId);
		if (media == null) {
			Logger.warn("[GenerateMediaVariants] Could not find media with id %s for media variant processing", mediaId);
			return;
		}

		try {
			media.markGeneratingVariants(true);
			media.generateVariants();
			media.save();
			media.markGeneratingVariants(false);
		} catch (Exception e) {
			ErrorHelper.handleWarning(e, "[GenerateMediaVariants] Error generating variants for media: %s", mediaId);

			if (trial++ <= maxTrials) {
				new GenerateMediaVariants(tenant, mediaId, trial).in(1);
			} else {
				Logger.warn("[GenerateMediaVariants] Generating variants for media [%s] finally failed.", mediaId);
				media.markGeneratingVariants(false);
			}
		}
	}
}

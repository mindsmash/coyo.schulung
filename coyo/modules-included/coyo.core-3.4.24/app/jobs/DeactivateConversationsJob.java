package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;

import org.joda.time.DateTime;

import play.db.jpa.JPA;
import play.jobs.On;

/**
 * Deactivates conversations that are older than 7 days.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0 1 * * ?")
public class DeactivateConversationsJob extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {
			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				JPA.em().createQuery("UPDATE ConversationMember m SET m.active = false WHERE m.lastAccess <= ?")
						.setParameter(1, new DateTime().minusDays(7).toDate()).executeUpdate();
			}
		};
	}
}
package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;

import java.util.List;

import models.AbstractTenant;
import models.app.BlogAppArticle;
import models.notification.BlogArticleNotification;

import models.wall.Wall;
import models.wall.post.ShareBlogArticlePost;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.JPA;
import play.jobs.On;
import utils.JPAUtils;

import javax.persistence.TypedQuery;

/**
 * Job to automatically share blog posts on the sender's wall and trigggers notification if it get's published.
 * 
 * Also, the job will check for newly published blog articles and raise a notification for them.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0/5 * * * ?")
public class BlogArticleJob extends TenantTaskScheduler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlogArticleJob.class);

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {

			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				LOG.debug("[BlogArticleJob] Running blog autoshare job");

				final String sql = "SELECT ba FROM BlogAppArticle ba WHERE ba.publishDate <= :publishDate AND "
						+ "ba.app.active = true AND ba.app.autoshare = true AND ba.autoshareDate IS NULL";

				final TypedQuery<BlogAppArticle> q = JPA.em().createQuery(sql, BlogAppArticle.class);
				q.setParameter("publishDate", DateTime.now());

				// try all published blog posts
				List<BlogAppArticle> articles = q.getResultList();

				LOG.debug("[BlogArticleJob] Found {} articles to process.", articles.size());

				for (BlogAppArticle article : articles) {
					// raise notification if publish date is not older than one day and no notification was raised yet
					if (article.notifications.size() == 0 &&
							article.publishDate.isAfter(DateTime.now().minusDays(1))) {
						LOG.debug("[BlogArticleJob] Raising notification for article [{}].", article.id);
						BlogArticleNotification.raise(article);
					}

					if (article.canBeAutoShared()) {
						article.doAutoShare();
					} else {
						LOG.debug("[BlogArticleJob] Blogarticle {} was already automatically shared. Skipping...",
								article.id);
					}
				}

				LOG.debug("[BlogArticleJob] Finished blog autoshare job");
			}
		};
	}
}

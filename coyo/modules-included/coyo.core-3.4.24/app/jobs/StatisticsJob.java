package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.User;
import models.event.Event;
import models.page.Page;
import models.statistics.TrackingRecord;
import models.workspace.Workspace;
import play.Logger;
import play.jobs.On;

/**
 * Tracks daily statistics.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@On("0 0 1 * * ?")
public class StatisticsJob extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {
			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				Logger.debug("starting daily statistics job");

				// track users (always current count)
				TrackingRecord.track("activeUsers", getActiveUserCount().intValue());
				TrackingRecord.track("inactiveUsers", new Long(User.count("active = false")).intValue());
				TrackingRecord.track("externalUsers", new Long(User.count("external = true")).intValue());

				// track workspaces
				TrackingRecord.track("activeWorkspaces", getActiveWorkspaceCount().intValue());

				// track pages
				TrackingRecord.track("pages", getPageCount().intValue());

				// track events
				TrackingRecord.track("todayEvents", getTodayEventCount().intValue());

				Logger.debug("finished daily statistics job");
			}
		};
	}

	public static Long getActiveUserCount() {
		return User.count("active = true");
	}

	public static Long getActiveWorkspaceCount() {
		return Workspace.count("archived = false");
	}

	public static Long getPageCount() {
		return Page.count();
	}

	public static Long getTodayEventCount() {
		return Event.getTodayEventCount();
	}
}
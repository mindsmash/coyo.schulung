package jobs;

import hazelcast.TenantTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.AbstractTenant;
import models.Sender;
import models.User;
import models.User.ActivityStreamSelect;
import models.UserNotification;
import models.UserNotificationSettings.MailInterval;
import models.messaging.ConversationMember;
import models.wall.post.Post;
import notifiers.ApplicationMailer;

import org.joda.time.DateTime;

import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import session.UserLoader;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

// will be scheduled by other jobs
public class UpdateMailer extends TenantTask {

	public static final boolean MAILS = Boolean.parseBoolean(Play.configuration.getProperty("coyo.mails", "true"));

	public static final String NEW_POSTS_MAIL_EVENT = "NewPostsMail";
	public static final String NEW_MESSAGES_MAIL_EVENT = "NewMessagesMail";

	private MailInterval interval;

	public UpdateMailer(AbstractTenant tenant, MailInterval interval) {
		super(tenant.id);

		this.interval = interval;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		super.writeData(out);
		out.writeInt(interval.ordinal());
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		super.readData(in);
		interval = MailInterval.values()[in.readInt()];
	}

	@Override
	public void execute(AbstractTenant tenant) throws Exception {
		if (!MAILS) {
			return;
		}

		Date lastExecution = new DateTime().minusMinutes(interval.intervalMinutes).toDate();

		Logger.debug("starting updates mailer job for interval: %s", interval);
		Logger.debug("last execution: %s", lastExecution);

		// include external users
		((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);

		// activate limit (30 days)
		Post.enableDateFilter(30);

		List<User> users = User
				.find("SELECT u FROM User u WHERE u.active = true AND u.deleted = false AND u.notSettings.receiveMails = true AND u.notSettings.mailInterval = ?",
						interval).fetch();

		try {
			for (User user : users) {
				// for permission checks
				UserLoader.setThreadLocalUser(user.id);

				// load notifications
				List<UserNotification> notifications = user.getEmailNotifications(lastExecution);

				// load posts
				List<Post> posts = new ArrayList<Post>();
				if (user.notSettings.isWanted(NEW_POSTS_MAIL_EVENT)) {
					ActivityStreamSelect activityStream = new ActivityStreamSelect(user);
					if (user.lastActivityStreamUpdate != null && user.lastActivityStreamUpdate.after(lastExecution)) {
						activityStream.after = user.lastActivityStreamUpdate;
					} else {
						activityStream.after = lastExecution;
					}
					posts = activityStream.fetch();
				}

				// load conversations
				List<ConversationMember> conversations = new ArrayList<ConversationMember>();
				if (user.notSettings.isWanted(NEW_MESSAGES_MAIL_EVENT)) {
					conversations = user.getUnreadConversations(lastExecution);
				}

				// if there are any, send mail
				if (notifications.size() > 0 || posts.size() > 0 || conversations.size() > 0) {
					Logger.debug("sending mail to user: %s", user);
					ApplicationMailer.updateMail(user, notifications, posts, conversations);
				}
			}
		} finally {
			UserLoader.clearThreadLocalUser();
			Logger.debug("finished updates mailer job for interval %s, processed %s users", interval, users.size());
		}
	}

}
package jobs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;

import models.AbstractTenant;
import models.Settings;
import multitenancy.MTA;
import play.Logger;
import play.Play;
import play.libs.Codec;
import play.templates.JavaExtensions;
import storage.FlexibleBlob;
import utils.JPAUtils;
import data.TenantDataExporter;

/**
 * Exports tenant data.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class TenantExportJob extends TenantJob {

	public static final String EXPORT_FILE_PREFIX = "dataexport-";
	public static final String MIME_TYPE = "application/zip";

	public TenantExportJob(AbstractTenant tenant) {
		super(tenant);
	}

	@Override
	public void doTenantJob() throws Exception {
		Logger.info("[Export] Starting tenant exporter job");

		Settings settings = Settings.findOrCreate("export");
		try {
			// check active
			if (settings.getBoolean("active")) {
				Logger.warn("[Export] Cannot start tenant exporter job because it is already active");
				return;
			}

			// set active
			settings.setProperty("active", "true");
			settings.save();

			// save status immediately
			settings = saveSettingsAndReload("export");

			// export
			String key = EXPORT_FILE_PREFIX + JavaExtensions.format(new Date(), "yyyy-MM-dd") + ".zip";
			FlexibleBlob exportFile = new FlexibleBlob(key, MIME_TYPE);
			if (exportFile.exists()) {
				exportFile.delete();
			}

			File tmp = new File(Play.tmpDir, Codec.UUID());
			tmp.createNewFile();
			FileOutputStream fos = new FileOutputStream(tmp);
			try {
				TenantDataExporter.export(tenant, fos);
			} finally {
				fos.close();
			}

			exportFile.set(new FileInputStream(tmp), "", tmp.length());

			// store file name
			Settings exportSettings = Settings.findOrCreate("dataexport");
			exportSettings.setProperty(key, System.currentTimeMillis() + "");
			exportSettings.save();

			settings = saveSettingsAndReload("export");
			settings.setProperty("finished", System.currentTimeMillis() + "");
		} finally {
			settings.setProperty("active", "false");
			settings.save();

			Logger.info("[Export] Finished tenant exporter job");
		}
	}

	private Settings saveSettingsAndReload(String settings) {
		JPAUtils.commitAndRefreshTransaction();
		MTA.activateMultitenancy(tenant);
		return Settings.findOrCreate(settings);
	}
}

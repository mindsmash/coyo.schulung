package jobs;

import java.util.Collection;

import models.AbstractTenant;
import models.User;
import models.notification.Notification;
import play.Logger;

/**
 * Distributes a notification to a list of users
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class NotificationDistributor extends TenantJob {

	private Long notificationId;
	private Collection<Long> userIds;

	public NotificationDistributor(AbstractTenant tenant, Long notificationId, Collection<Long> userIds) {
		super(tenant);
		this.notificationId = notificationId;
		this.userIds = userIds;
	}

	@Override
	public void doTenantJob() throws Exception {
		// load notification
		Notification notification = Notification.findById(notificationId);
		if (notification == null) {
			Logger.warn(
					"[NotificationDistributor] Could not distribute notification [ID=%s] because the notification could not be found.",
					notificationId);
			return;
		}

		// raise to users
		Logger.debug(
				"[NotificationDistributor] Starting notification distribution for notification [ID=%s] to %s users.",
				notificationId, userIds.size());

		// distribute to users
		for (Long id : userIds) {
			try {
				User user = User.findById(id);
				if (user != null) {
					user.receiveNotification(notification);
				}
			} catch (Exception e) {
				Logger.error(e, "[NotificationDistributor] Error distributing notification [%s] to user [ID=%s]",
						notification, id);
			}
		}

		Logger.debug("[NotificationDistributor] Finished notification [ID=%s] distribution", notificationId);
	}
}
package jobs;

import com.google.common.collect.ImmutableMap;
import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;

import java.util.List;

import models.AbstractTenant;
import models.event.Event;
import models.notification.EventReminderNotification;

import models.notification.Notification;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.Logger;
import play.jobs.On;

/**
 * 
 * Fires an {@link EventReminderNotification} when the {@link Event} reminding
 * time is reached.
 * 
 * @author mindsmash GmbH
 */
@On("0 0/15 * * * ?")
public class EventNotifications extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {

			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				// upcoming events without an existing reminder notification
				List<Event> events = Event
						.find("SELECT e FROM Event e WHERE e.sendReminder IS NOT NULL AND e.startDate >= ? AND NOT EXISTS (SELECT n FROM EventReminderNotification n WHERE n.event.id = e.id)",
								new DateTime(DateTimeZone.UTC)).fetch();
				for (Event event : events) {
					DateTime earliestNotificationTime = event.startDate.minusHours(event.sendReminder);
					if (earliestNotificationTime.isBeforeNow()) {
						Logger.debug("firing event reminder for event: %s", event);

						EventReminderNotification.raise(event);
					}
				}
			}
		};
	}
}

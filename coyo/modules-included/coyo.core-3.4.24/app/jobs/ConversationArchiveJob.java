package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.messaging.ConversationMember;
import models.messaging.ConversationStatus;
import org.apache.commons.lang.time.DateUtils;
import play.Logger;
import play.db.jpa.JPA;
import play.jobs.On;

import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Archives ConversationMembers with no activity since 30 days
 */
@On("30 1 2 * * ?")
public class ConversationArchiveJob extends TenantTaskScheduler {

	public static final int CONVERSATION_ARCHIVE_TRESHOLD = 30;

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {
			@Override
			public void execute(AbstractTenant tenant) throws Exception {
				Logger.debug("[ConversationArchiveJob] starting daily conversation archive job");

				final String sql = "SELECT cm FROM ConversationMember cm "
						+ "WHERE cm.status = :status AND cm.lastAccess < :treshold";

				final TypedQuery<ConversationMember> query = JPA.em().createQuery(sql, ConversationMember.class);
				query.setParameter("status", ConversationStatus.INBOX);
				query.setParameter("treshold", DateUtils.addDays(new Date(), -CONVERSATION_ARCHIVE_TRESHOLD));

				final List<ConversationMember> members = query.getResultList();

				for (ConversationMember member : members) {
					member.status = ConversationStatus.ARCHIVED;
					member.active = false;
					member.save();
				}

				Logger.debug("[ConversationArchiveJob] finished daily conversation archive job");
			}
		};
	}
}
package jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.AbstractTenant;
import models.Settings;
import multitenancy.MTA;
import play.Logger;
import utils.JPAUtils;
import fishr.Fishr;
import fishr.ManagedIndex;

/**
 * Rebuilds search indexes and returns the indexes that were renewed.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class SearchReindexer extends TenantJob {

	private List<String> indexes = new ArrayList<String>();

	public SearchReindexer(List<String> indexes) {
		this(MTA.getActiveTenant(), indexes);
	}

	public SearchReindexer(String... indexes) {
		this(MTA.getActiveTenant(), indexes);
	}

	public SearchReindexer(AbstractTenant abstractTenant, List<String> indexes) {
		super(abstractTenant);
		this.indexes = indexes;
	}

	public SearchReindexer(AbstractTenant tenant, String... indexes) {
		super(tenant);
		this.indexes = Arrays.asList(indexes);
	}
	
	public SearchReindexer(AbstractTenant tenant) {
		super(tenant);
		
		// all
		for (ManagedIndex index : Fishr.listIndexes()) {
			indexes.add(index.name);
		}
	}

	@Override
	public void doTenantJob() {
		Logger.info("starting search reindexer job");

		Settings settings = Settings.findOrCreate("search");
		try {
			// check active
			if (settings.getBoolean("active")) {
				Logger.warn("cannot start search reindexer job because search reindexing is already active");
				return;
			}

			// set active
			settings.setProperty("active", "true");
			settings.save();

			// save status immediately
			settings = settings.persistAndReload();

			// reindex
			for (String index : indexes) {
				settings.refresh();
				if (!settings.getBoolean("active")) {
					Logger.info("cancelling search reindexing");
					break;
				}

				try {
					Logger.debug("rebuilding index: %s", index);
					Fishr.getCurrentStore().rebuild(index);
				} catch (Exception e) {
					Logger.error(e, "error rebuilding search index: %s", index);
				}
			}

			settings = settings.persistAndReload();
			settings.setProperty("finished", System.currentTimeMillis() + "");
		} finally {
			settings.setProperty("active", "false");
			settings.save();

			Logger.info("finished search reindexer job");
		}
	}
}

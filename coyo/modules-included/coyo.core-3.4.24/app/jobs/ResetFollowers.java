package jobs;

import models.AbstractTenant;
import models.Sender;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * By saving each sender, all followers are reset.
 *
 * @author mindsmash GmbH
 */
public class ResetFollowers extends MultiTenantJob {

	private boolean reindexWhenDone = false;

	public ResetFollowers(boolean reindexWhenDone) {
		this.reindexWhenDone = reindexWhenDone;
	}

	@Override
	public void doTenantJob(AbstractTenant tenant) throws Exception {
		Logger.info("starting sender follower reset job");

		// load only ids and then iterate to reduce memory consumption
		final TypedQuery<Long> query = JPA.em().createQuery("SELECT s.id FROM Sender AS s", Long.class);
		final List<Long> senderIds = query.getResultList();

		int i = 0;
		for (Long senderId : senderIds) {
			final Sender sender = Sender.findById(senderId);
			if (sender != null) {
				try {
					sender.save();
				} catch (Exception e) {
					Logger.error(e, "error saving sender to reset followers: %s", sender);
				}
			}
			if (i++ % 100 == 0) {
				JPA.em().flush();
			}
		}

		if (reindexWhenDone) {
			Logger.info("scheduling search reindexing ...");
			new SearchReindexer(tenant).now();
		}

		Logger.info("finished sender follower reset job");
	}
}

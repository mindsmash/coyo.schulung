package jobs;

import hazelcast.TenantTask;
import hazelcast.TenantTaskScheduler;
import models.AbstractTenant;
import models.Tenant;
import play.Logger;
import play.jobs.On;
import storage.FlexibleBlob;

@On("0 0 0/12 * * ?")
public class StorageQuotaCalculator extends TenantTaskScheduler {

	@Override
	protected TenantTask createTask(AbstractTenant tenant) {
		return new TenantTask(tenant.id) {

			@Override
			public void execute(final AbstractTenant tenant) throws Exception {
				Tenant cTenant = (Tenant) tenant;
				try {
					long storageLength = FlexibleBlob.storage.storageLength();
					cTenant.quota.currentUsage = storageLength;
					cTenant.save();
					Logger.debug("[StorageQuotaCalculator] Calculated a quota of '%s' for tenant [%s].", storageLength, cTenant.id);
				} catch (Exception e) {
					Logger.error(e, "[StorageQuotaCalculator] Exception calculating disk quota for tenant [%s]!", cTenant.id);
				}
			}
		};
	}

}

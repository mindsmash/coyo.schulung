package binding;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import play.data.binding.TypeBinder;
import play.data.binding.types.DateTimeBinder;

public class ISO8601DateTimeBinder extends DateTimeBinder implements TypeBinder<DateTime> {

	@Override
	public DateTime bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType)
			throws Exception {

		try {
			DateTime dt = super.bind(name, annotations, value, actualClass, genericType);
			if (dt != null) {
				return dt;
			}
		} catch (Exception ignored) {
		}

		try {
			return ISODateTimeFormat.dateTime().parseDateTime(value);
		} catch (Exception ignored) {
		}

		return null;
	}
}

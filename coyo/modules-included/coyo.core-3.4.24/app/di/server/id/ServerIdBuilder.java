package di.server.id;


/**
 * Create a Server-Id
 *  
 * @author Klaas Behrend, mindsmash
 */
public interface ServerIdBuilder {
	
	/**
	 * Return the ServerId.
	 * 
	 * @return the id of the server.
	 */
	String getServerId();
}

package di.server.id;

import org.apache.commons.lang.StringUtils;

import di.user.UserNameBuilder;
import play.Logger;
import play.libs.Codec;
import play.libs.Crypto;
import util.ServerUtils;
import utils.JPAUtils;
import models.Settings;
import models.User;


/**
 * Default implementation of {@link ServerIdBuilder} which returns the server-Id using the mac-address.
 * 
 * @author Klaas Behrend, mindsmash GmbH
 *
 */
public class DefaultServerIdBuilder implements ServerIdBuilder {
	
	/**
	 * Return the ServerId. Created with the mac-address.
	 * 
	 * @return Id of the Server           
	 */
	public String getServerId() {
		Settings settings = Settings.findApplicationSettings();
		
		// find server MAC address
		String mac = ServerUtils.getMacAddress();
		if (mac == null) {
			mac = "MA-C0-NO-TF-OU-ND";
		}
		String key = StringUtils.rightPad(mac, 20, "X").substring(0, 16);

		try {
			// check if MAC address changed, then reset server ID
			if (!settings.contains("server.id") || !settings.contains("server.mac")
					|| !mac.equals(settings.getString("server.mac"))) {
				String uuid = Codec.UUID().toUpperCase();

				settings.setProperty("server.mac", mac);
				settings.setProperty("server.id", Crypto.encryptAES(uuid, key));

				settings.save();
				JPAUtils.makeTransactionWritable();

				Logger.info("Generated new server ID [%s] for server [%s]", uuid, mac);
			}

			return Crypto.decryptAES(settings.getString("server.id"), key);
		} catch (Exception e) {
			Logger.error(e, "error generating server ID [MAC:%s]", mac);
		}
		return null;
	}
}

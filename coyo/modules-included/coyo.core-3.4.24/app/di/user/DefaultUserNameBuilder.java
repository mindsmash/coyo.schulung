package di.user;

import org.apache.felix.scrplugin.helper.StringUtils;

import models.User;

/**
 * Default implementation of {@link UserNameBuilder} which returns the full name consisting of first name + last name
 * and title if set.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class DefaultUserNameBuilder implements UserNameBuilder {

	@Override
	public String getFullName(User user) {
		return user.firstName + " " + user.lastName;
	}

	@Override
	public String getFullNameWithTitle(User user) {
		String title = StringUtils.isEmpty(user.title) ? "" : user.title + " ";
		return title + getFullName(user);
	}

}

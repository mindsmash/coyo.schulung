package di.user;

import models.User;

/**
 * Creates a user name to be displayed.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public interface UserNameBuilder {

	/**
	 * Returns the full name of a user in the way it should be displayed in frontend.
	 * 
	 * @param user
	 *            the user to display the name of. Must not be null.
	 */
	String getFullName(User user);

	/**
	 * Returns the full name of a user including it's title. The method must be aware that the title might be empty or
	 * null.
	 * 
	 * @param user
	 *            the user to display the name of. Must not be null.
	 */
	String getFullNameWithTitle(User user);

}

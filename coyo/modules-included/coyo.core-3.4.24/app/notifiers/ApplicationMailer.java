package notifiers;

import injection.Inject;
import injection.InjectionSupport;

import java.util.List;

import models.Settings;
import models.User;
import models.UserNotification;
import models.event.Event;
import models.messaging.ConversationMember;
import models.wall.attachments.FilePostAttachment;
import models.wall.attachments.PostAttachment;
import models.wall.post.Post;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailAttachment;

import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Mailer;
import conf.ApplicationSettings;

/**
 * Sends all kinds of notification mails
 * 
 * @author mindsmash GmbH
 * 
 */
@InjectionSupport
public class ApplicationMailer extends Mailer {

	/**
	 * This will be shown as sender
	 */
	public static final String FROM = Play.configuration.getProperty("mail.from", "mailman@gocoyo.com");

	@Inject(configuration = "mail.filter.class", defaultClass = MailFilter.class)
	public static MailFilter filter;

	/**
	 * Can be used to filter the recipients of mails before mails are sent.
	 */
	public static class MailFilter {
		/**
		 * @param recipient
		 * @return True if this recipient should be excluded.
		 */
		public boolean exclude(String recipient) {
			return false;
		}
	}

	protected static void addRecipient(String... recipients) {
		for (String recipient : recipients) {
			if (StringUtils.isNotEmpty(recipient) && !filter.exclude(recipient)) {
				Mailer.addRecipient(recipient);
			}
		}
	}

	protected static void addCc(String... recipients) {
		for (String recipient : recipients) {
			if (StringUtils.isNotEmpty(recipient) && !filter.exclude(recipient)) {
				Mailer.addCc(recipient);
			}
		}
	}

	protected static void addBcc(String... recipients) {
		for (String recipient : recipients) {
			if (StringUtils.isNotEmpty(recipient) && !filter.exclude(recipient)) {
				Mailer.addBcc(recipient);
			}
		}
	}

	public static String getDefaultFrom() {
		// when not of form "Name <mail@mail.com>"
		if (!FROM.contains("<")) {
			// use dynamic application name
			return Settings.findApplicationSettings().getString(ApplicationSettings.APPLICATION_NAME) + " <" + FROM
					+ ">";
		}
		return FROM;
	}

	// Specific mails from here on...

	public static void activate(String recipient, String token) {
		boolean autoActivation = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOACTIVATION);
		setSubject(Messages.get("registration.mail.subject"));
		addRecipient(recipient);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(recipient, token, autoActivation);
	}

	public static void activateNewEmail(String email, String token) {
		setSubject(Messages.get("account.emailChange.mail.subject"));
		addRecipient(email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(email, token);
	}

	public static void forgotPassword(String email, String token) {
		setSubject(Messages.get("auth.forgotPassword.mail.subject"));
		addRecipient(email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(email, token);
	}

	public static void updateMail(User user, List<UserNotification> notifications, List<Post> posts,
			List<ConversationMember> conversations) {
		Lang.set(user.language.toString());

		int updateCount = notifications.size() + posts.size() + conversations.size();
		setSubject(Messages.get("update.mail.subject" + ((updateCount == 1) ? "" : "s"), updateCount));
		addRecipient(user.email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(user, notifications, posts, conversations);
	}

	public static void invite(String email, User user, String token) {
		String old = Lang.get();
		Lang.setDefaultLocale();

		String applicationName = Settings.findApplicationSettings().getString(ApplicationSettings.APPLICATION_NAME);

		setSubject(Messages.get("invitation.mail.subject", user.getDisplayName(),
				Play.configuration.getProperty("application.name")));
		addRecipient(email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(user, applicationName, email, token);

		Lang.set(old);
	}

	public static void sharePost(User user, Post post, boolean showComments, String message, String recipient) {
		Lang.setDefaultLocale();

		setSubject(Messages.get("post.share.mail.subject", user.getDisplayName()));
		addRecipient(recipient);
		setReplyTo(user.getFullName() + " <" + user.email + ">");
		setFrom(getDefaultFrom());
		setContentType("text/html");

		// attachments
		for (PostAttachment attachment : post.attachments) {
			if (attachment instanceof FilePostAttachment) {
				FilePostAttachment fa = (FilePostAttachment) attachment;
				EmailAttachment ea = new EmailAttachment();
				ea.setName(fa.name);
				ea.setPath(fa.file.asFile().getAbsolutePath());
				addAttachment(ea);
			}
		}

		send(user, post, showComments, message);
	}

	public static void eventMail(Event event, User user, String title, String message) {
		Lang.set(user.language.toString());

		setSubject(Messages.get("event.mail.subject", event.name));
		addRecipient(user.email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(event, user, title, message);
	}

	public static void userActivated(User user) {
		Lang.set(user.language.toString());

		setSubject(Messages.get("user.activated.mail.subject"));
		addRecipient(user.email);
		setFrom(getDefaultFrom());
		setContentType("text/html");
		send(user);
	}
}

package notifiers;

import injection.InjectionSupport;
import models.wall.post.Post;
import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Mailer;

/**
 * Sends emails for reported posts
 * 
 * @author mindsmash GmbH
 * 
 */
@InjectionSupport
public class PostReportMailer extends Mailer {

	/**
	 * This will be shown as sender
	 */
	public static void reportPost(Post post, String message, String recipient) {
		Lang.setDefaultLocale();

		setSubject(Messages.get("post.report.mail.subject"));
		addRecipient(recipient);
		setFrom(ApplicationMailer.getDefaultFrom());
		setContentType("text/html");

		send(post, message);
	}
}

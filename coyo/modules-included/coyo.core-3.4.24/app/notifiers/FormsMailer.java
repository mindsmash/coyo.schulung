package notifiers;

import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import models.FormField;
import models.User;
import models.app.FormsApp;
import org.joda.time.DateTimeZone;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Mailer;

public class FormsMailer extends Mailer {

	public static void result(FormsApp app, Map<FormField, String> data, String recipient, User user, Locale language,
							  DateTimeZone timeZone, boolean printTimeZone) {
		if(StringUtils.isEmpty(recipient)) {
			return;
		}
		
		Lang.set(language.toString());

		setSubject(Messages.get("formsapp.mail.subject", app.title));
		addRecipient(recipient);
		setFrom(ApplicationMailer.getDefaultFrom());
		setContentType("text/html");
		send(app, data, user, timeZone, printTimeZone);
	}
}

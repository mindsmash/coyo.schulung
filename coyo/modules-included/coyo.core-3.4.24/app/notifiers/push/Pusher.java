package notifiers.push;

import models.User;

/**
 * Pushes messages to users immediately.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public interface Pusher {

	void push(User user, String message, int badge);

	void push(User user, String message);

	void push(User user, int badge);
}

package notifiers.push;

import models.User;

/**
 * Holds all information of one push notification.
 */
public class PushNotification {
	private User user;
	private String message;
	private Integer badge;

	public PushNotification(User user, String message, Integer badge) {
		this.user = user;
		this.message = message;
		this.badge = badge;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getBadge() {
		return badge;
	}

	public void setBadge(Integer badge) {
		this.badge = badge;
	}
}

package notifiers.push;

import com.mindsmash.pns.client.PNS;
import com.mindsmash.pns.client.PayloadBuilder;
import com.mindsmash.pns.client.PnsClient;
import license.LicenseValidator;
import models.AbstractTenant;
import models.User;
import models.UserPushDevice;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.impl.ResponseImpl;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import play.Play;

import javax.ws.rs.client.ClientException;
import javax.ws.rs.client.InvocationCallback;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

/**
 * Implementation that pushes to mindsmash PNS.
 * <p/>
 * See https://bitbucket.org/mindsmash/pns
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
public class PNSPusher implements Pusher {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(PNSPusher.class);

	private static final String PNS_ENDPOINT = Play.configuration
			.getProperty("coyo.pns.endpoint", "https://pns.mindsmash.com");

	private static final Integer PNS_CLIENTS_ERROR_SLEEP_MS = Integer
			.parseInt(Play.configuration.getProperty("coyo.pns.client.error.sleep", "30000"));

	private static final ConcurrentHashMap<Long, PnsClient> PNS_CLIENTS = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Long, DateTime> PNS_CLIENTS_ERRORS = new ConcurrentHashMap<>();

	@Override
	public void push(User user, String message, int badge) {
		doPush(user, message, badge);
	}

	@Override
	public void push(User user, String message) {
		doPush(user, message, null);
	}

	@Override
	public void push(User user, int badge) {
		doPush(user, null, badge);
	}

	private void doPush(final User user, String message, Integer badge) {
		PnsClient pnsClient = null;
		pnsClient = getPnsClient(user.tenant);

		if (pnsClient == null) {
			return;
		}

		for (UserPushDevice device : user.pushDevices) {
			if (device.active) {
				PayloadBuilder payloadBuilder = PNS.newPayload();
				payloadBuilder.device(device.toPnsDevice());

				if (StringUtils.isNotEmpty(message)) {
					payloadBuilder.body(message);
				}
				if (badge != null) {
					payloadBuilder.badge(badge.intValue());
				}
				pnsClient.push(payloadBuilder.build(), new InvocationCallback<ResponseImpl>() {
					private AbstractTenant tenant = user.tenant;
					@Override
					public void completed(ResponseImpl response) {
						LOG.debug("[PNS] Successfully send notification for tenant: " + tenant.getId());
					}
					@Override
					public void failed(ClientException e) {
						LOG.error("[PNS] Could not send push notification to PNS {}.", PNS_ENDPOINT);
						markPnsClientError(tenant);
					}
				});
			}
		}
	}

	/**
	 * Used to obtain a {@link PnsClient} instance. will return null if no instance can be created or if during the last
	 * usage an error occurred and the configured timeout has not been exceeded.
	 *
	 * @param tenant the tenant which will be used to create a pns client instance
	 * @return {@link PnsClient} instance
	 */
	private PnsClient getPnsClient(final AbstractTenant tenant) {
		final DateTime lastError = PNS_CLIENTS_ERRORS.get(tenant.getId());
		if (lastError != null && DateTime.now().minusMillis(PNS_CLIENTS_ERROR_SLEEP_MS).isBefore(lastError)) {
			LOG.debug("[PNSPusher] Skipping push notification delivery cause of an previous error?");
			return null;
		}

		PnsClient pnsClient = PNS_CLIENTS.get(tenant.getId());
		if (pnsClient == null) {
			try {
				pnsClient = PNS.newClient().withBaseUrl(PNS_ENDPOINT).withLicense(LicenseValidator.loadLicenseString())
						.authenticateAndBuild();
				synchronized (PNS_CLIENTS) {
					PNS_CLIENTS.put(tenant.getId(), pnsClient);
				}
			} catch (Exception ex) {
				markPnsClientError(tenant);
				LOG.error("[PNS] Failed to create pns client instance: " + ex.getLocalizedMessage());
			}
		}

		return pnsClient;
	}

	/**
	 * Saves the time when an error occurred for a specific pns client. Also remove the currently used instance from the
	 * pns clients map.
	 *
	 * @param tenant
	 */
	private static void markPnsClientError(AbstractTenant tenant) {
		synchronized (PNS_CLIENTS_ERRORS) {
			PNS_CLIENTS_ERRORS.put(tenant.getId(), new DateTime());
		}
		synchronized (PNS_CLIENTS) {
			PNS_CLIENTS.remove(tenant.getId());
		}
	}
}

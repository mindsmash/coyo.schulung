package containers.form;

import java.util.List;
import java.util.Map;

import play.data.validation.Required;

public class FieldFormData {
	
	public Long id;
	
	public int priority;
	
	@Required
	public String type;

	@Required
	public boolean required;

	@Required
	public boolean hidden;

	@Required
	public String label;

	public Map<String, String> properties;
	
	public List<FieldOptionsFormdata> options; 

}

package containers.form;

import models.container.Container;
import models.container.Field;
import models.container.ItemValue;
import models.container.values.BooleanItemValue;
import models.container.values.FileItemValue;
import models.container.values.LinkItemValue;
import models.container.values.NumberItemValue;
import models.container.values.OptionsItemValue;
import models.container.values.SenderItemValue;
import models.container.values.StringItemValue;
import models.container.values.DateItemValue;
import play.data.validation.Required;
import containers.FieldType;

public class ItemValueFormData {
	private static ItemValue makeItemValue(final Field field) {
		switch (FieldType.valueOf(field.getType())) {
		case BOOLEAN:
			return new BooleanItemValue(field);
		case NUMBER:
			return new NumberItemValue(field);
		case OPTIONS:
			return new OptionsItemValue(field);
		case TEXT:
			return new StringItemValue(field);
		case DATE:
			return new DateItemValue(field);
		case USER:
			return new SenderItemValue(field);
		case FILE:
			return new FileItemValue(field);
		case LINK:
			return new LinkItemValue(field);
		default:
			throw new IllegalArgumentException("Cannot make value out of field " + field);
		}
	}

	public static ItemValue toItemValue(final Container container, final Long fieldId, final String value) {
		Field field = container.getFieldById(fieldId);
		if (field == null) {
			throw new IllegalArgumentException("Cannot make value out of field ID " + fieldId);
		}
		return makeItemValue(field).setValue(value);
	}

	@Required
	public Long fieldId;

	@Required
	public String value;

	public ItemValueFormData(Long id, String value) {
		this.fieldId = id;
		this.value = value;
	}
}

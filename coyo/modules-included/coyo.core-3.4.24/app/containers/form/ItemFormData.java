package containers.form;

import java.util.HashMap;

import models.User;
import models.container.Container;
import models.container.Field;
import models.container.Item;

public class ItemFormData extends HashMap<Long, String> {
	
	public ItemFormData(ItemValueFormData... itemValueFormData) {
		for (ItemValueFormData data : itemValueFormData) {
			this.put(data.fieldId, data.value);
		}
	}

	public ItemFormData() {
		// empty default c'tor
	}
	
	private static void validateFormData(ItemFormData itemFormData) {
		if (itemFormData == null || itemFormData.isEmpty()) {
			throw new IllegalArgumentException("Cannot create item: no values passed.");
		}
		for (Long fieldId : itemFormData.keySet()) {
			if (itemFormData.get(fieldId).isEmpty()) {
				throw new IllegalArgumentException(String.format("Invalid form data - field with ID '%d' has no value", fieldId));
			}
		}
	}
	
	public static Item createItem(final Container container, final ItemFormData itemFormData, final User creator) {
		validateFormData(itemFormData);
		Item item = new Item(container, creator);
		return updateItem(item, itemFormData);
	}

	/**
	 * Updates an existing item with new form data. itemFromData cannot be null nor empty.
	 * 
	 * Three cases are possible: 
	 * 1. A value exists in item and form data: Update value
	 * 2. A value exists in item but not in form data: Delete value
	 * 3. A value does not exist in item but in form data: Create value
	 */
	public static Item updateItem(Item item, ItemFormData itemFormData) {
		validateFormData(itemFormData);

		for (Field field : item.container.fields) {
			boolean itemHasValue = item.hasValueForField(field);
			boolean formHasValue = (itemFormData.get(field.id) != null && !itemFormData.get(field.id).isEmpty());
			if (itemHasValue && formHasValue) {
				// Update
				item.setValue(field.id, itemFormData.get(field.id));
			} else if (itemHasValue && !formHasValue) {
				// Delete
				item.removeValue(field.id);
			} else if (!itemHasValue && formHasValue) {
				// Create
				item.addValue(ItemValueFormData.toItemValue(item.container, field.id, itemFormData.get(field.id)));
			}
		}
		return item;
	}

}

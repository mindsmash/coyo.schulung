package containers.form;

import java.util.List;

import play.data.validation.Required;
import play.data.validation.Valid;

import com.google.common.collect.Lists;

public class ContainerFormData {
	
	public Long id;
	
	@Required
	public Long senderId;

	@Required
	public String name;

	@Required
	@Valid
	public List<FieldFormData> fields;

}

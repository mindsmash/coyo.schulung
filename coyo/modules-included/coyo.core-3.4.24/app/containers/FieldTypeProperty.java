package containers;

import org.apache.commons.lang3.BooleanUtils;

/**
 * This class represents a property of a field.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class FieldTypeProperty {

	public FieldType type;
	public String key;
	public String label;

	public FieldTypeProperty(FieldType type, String key, String label) {
		this.type = type;
		this.key = key;
		this.label = label;
	}

	public Object convertValue(String propertyValue) {
		switch (type) {
		case BOOLEAN:
			return BooleanUtils.toBoolean(propertyValue);
		case NUMBER:
			return Long.valueOf(propertyValue);
		default:
			return propertyValue;
		}
	}
}
package containers;

/**
 * Defines all supported field types for fields and
 * their properties.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public enum FieldType {
	TEXT, DATE, NUMBER, OPTIONS, BOOLEAN, USER, FILE, LINK
}
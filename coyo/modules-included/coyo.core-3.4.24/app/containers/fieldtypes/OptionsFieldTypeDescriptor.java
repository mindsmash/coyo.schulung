package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class OptionsFieldTypeDescriptor extends FieldTypeDescriptor {
	public OptionsFieldTypeDescriptor() {
		this.type = FieldType.OPTIONS;
		this.label = Messages.get("container.fieldType.options.label");
		this.properties = Lists.newArrayList(new FieldTypeProperty(FieldType.BOOLEAN, "multiple", Messages
				.get("container.fieldType.options.property.multiple.label")));
	}
}
package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class LinkFieldTypeDescriptor extends FieldTypeDescriptor {
	public LinkFieldTypeDescriptor() {
		this.type = FieldType.LINK;
		this.label = Messages.get("container.fieldType.link.label");
		this.properties = Lists.newArrayList(new FieldTypeProperty(FieldType.BOOLEAN, "sametab", Messages
				.get("container.fieldType.link.property.sametab.label")));
	}
}
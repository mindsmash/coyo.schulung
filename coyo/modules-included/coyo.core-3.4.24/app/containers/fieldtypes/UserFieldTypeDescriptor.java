package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class UserFieldTypeDescriptor extends FieldTypeDescriptor {
	public UserFieldTypeDescriptor() {
		this.type = FieldType.USER;
		this.label = Messages.get("container.fieldType.user.label");
		this.properties = Lists.newArrayList(new FieldTypeProperty(FieldType.BOOLEAN, "multiple", Messages
				.get("container.fieldType.options.property.multiple.label")));
	}
}
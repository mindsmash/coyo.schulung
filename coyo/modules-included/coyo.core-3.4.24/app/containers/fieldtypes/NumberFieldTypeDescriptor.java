package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class NumberFieldTypeDescriptor extends FieldTypeDescriptor {
	public NumberFieldTypeDescriptor() {
		this.type = FieldType.NUMBER;
		this.label = Messages.get("container.fieldType.number.label");
		this.properties = Lists.newArrayList(
				new FieldTypeProperty(FieldType.NUMBER, "min", Messages
						.get("container.fieldType.number.property.min.label")), new FieldTypeProperty(
						FieldType.NUMBER, "max", Messages.get("container.fieldType.number.property.max.label")));
	}
}
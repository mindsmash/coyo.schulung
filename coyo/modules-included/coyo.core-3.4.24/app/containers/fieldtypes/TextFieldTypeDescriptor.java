package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class TextFieldTypeDescriptor extends FieldTypeDescriptor {
	public TextFieldTypeDescriptor() {
		this.type = FieldType.TEXT;
		this.label = Messages.get("container.fieldType.text.label");
		this.properties = Lists.newArrayList(
				new FieldTypeProperty(FieldType.BOOLEAN, "multiline", Messages
						.get("container.fieldType.text.property.multiline.label")),
				new FieldTypeProperty(FieldType.NUMBER, "minLength", Messages
						.get("container.fieldType.text.property.minLength.label")),
				new FieldTypeProperty(FieldType.NUMBER, "maxLength", Messages
						.get("container.fieldType.text.property.maxLength.label")));
	}
}
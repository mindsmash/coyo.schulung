package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class DateFieldTypeDescriptor extends FieldTypeDescriptor {
	public DateFieldTypeDescriptor() {
		this.type = FieldType.DATE;
		this.label = Messages.get("container.fieldType.date.label");
		this.properties = Lists.newArrayList();
	}
}
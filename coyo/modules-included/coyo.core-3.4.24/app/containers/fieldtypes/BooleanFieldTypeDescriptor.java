package containers.fieldtypes;

import play.i18n.Messages;

import com.google.common.collect.Lists;

import containers.FieldType;
import containers.FieldTypeDescriptor;
import containers.FieldTypeProperty;

public class BooleanFieldTypeDescriptor extends FieldTypeDescriptor {
	public BooleanFieldTypeDescriptor() {
		this.type = FieldType.BOOLEAN;
		this.label = Messages.get("container.fieldType.boolean.label");
		this.properties = Lists.newArrayList(new FieldTypeProperty(FieldType.BOOLEAN, "preselected", Messages
				.get("container.fieldType.boolean.property.default.label")));
	}
}
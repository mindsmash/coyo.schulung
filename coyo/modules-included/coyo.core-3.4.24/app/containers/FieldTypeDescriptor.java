package containers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.container.Container;
import play.i18n.Messages;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import containers.fieldtypes.BooleanFieldTypeDescriptor;
import containers.fieldtypes.LinkFieldTypeDescriptor;
import containers.fieldtypes.NumberFieldTypeDescriptor;
import containers.fieldtypes.OptionsFieldTypeDescriptor;
import containers.fieldtypes.TextFieldTypeDescriptor;
import containers.fieldtypes.DateFieldTypeDescriptor;
import containers.fieldtypes.UserFieldTypeDescriptor;

/**
 * Describes a field type that can be used in a {@link Container}.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public abstract class FieldTypeDescriptor {

	public FieldType type;
	public String label;
	public List<FieldTypeProperty> properties = new ArrayList<>();

	public static class FileFieldTypeDescriptor extends FieldTypeDescriptor {
		public FileFieldTypeDescriptor() {
			this.type = FieldType.FILE;
			this.label = Messages.get("container.fieldType.file.label");
		}
	}

	public static FieldTypeDescriptor getDescriptor(String fieldType) {
		return FIELD_TYPE_DESCRIPTORS.get(FieldType.valueOf(fieldType));
	}

	public static Map<FieldType, FieldTypeDescriptor> getFieldTypeDescriptors() {
		return FIELD_TYPE_DESCRIPTORS;
	}

	public Object convertPropertyValue(String propertyKey, String value) {
		FieldTypeProperty property = getProperty(propertyKey);
		return property == null ? value : property.convertValue(value);
	}

	public FieldTypeProperty getProperty(final String propertyKey) {
		return Iterables.find(properties, new Predicate<FieldTypeProperty>() {
			@Override
			public boolean apply(FieldTypeProperty ftp) {
				return ftp.key.equals(propertyKey);
			}
		}, null);
	}

	private static final Map<FieldType, FieldTypeDescriptor> FIELD_TYPE_DESCRIPTORS = Maps.uniqueIndex(Lists
			.<FieldTypeDescriptor> newArrayList(new TextFieldTypeDescriptor(), new DateFieldTypeDescriptor(), new NumberFieldTypeDescriptor(),
					new BooleanFieldTypeDescriptor(), new OptionsFieldTypeDescriptor(), new UserFieldTypeDescriptor(),
					new FileFieldTypeDescriptor(), new LinkFieldTypeDescriptor()), new Function<FieldTypeDescriptor, FieldType>() {
		@Override
		public FieldType apply(FieldTypeDescriptor ftd) {
			return ftd.type;
		}
	});
}

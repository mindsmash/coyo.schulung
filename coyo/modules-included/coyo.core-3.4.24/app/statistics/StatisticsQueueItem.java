package statistics;

import java.io.Serializable;
import java.util.Date;

import models.AbstractTenant;

/**
 * Statistics data that can be persisted.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class StatisticsQueueItem implements Serializable {
	
	public Long tenantId;
	public Date date;
	public String recordType;
	public String discriminator;
	public int count;
	public boolean increment;

	public StatisticsQueueItem(Long tenantId, Date date, String recordType, String discriminator, boolean increment, int count) {
		this.tenantId = tenantId;
		this.date = date;
		this.recordType = recordType;
		this.discriminator = discriminator;
		this.count = count;
		this.increment = increment;
	}
	
	public AbstractTenant getTenant() {
		return (AbstractTenant) AbstractTenant.<AbstractTenant>findById(tenantId);
	}
}

package events;

import java.io.Serializable;

import play.db.jpa.JPA;
import play.db.jpa.JPABase;

public class EventModelWrapper implements Serializable {

	private Class<? extends JPABase> clazz;
	public Object key;

	public EventModelWrapper(JPABase model) {
		this.clazz = model.getClass();
		this.key = model._key();
	}

	public JPABase get() {
		return (JPABase) JPA.em().find(clazz, key);
	}
}

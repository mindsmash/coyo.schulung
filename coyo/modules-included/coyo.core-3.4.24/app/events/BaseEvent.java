package events;

import models.User;

public abstract class BaseEvent extends Event {
	public BaseEvent(String type) {
		super(type);
	}

	public BaseEvent(String type, Object data) {
		super(type, data);
	}

	/**
	 * Publish this event for the user with the given ID.
	 */
	public void raise(Long userId) {
		EventService.publishEvent(this, userId);
	}

	/**
	 * Publish this event for the given user.
	 */
	public void raise(User user) {
		raise(user.id);
	}
}

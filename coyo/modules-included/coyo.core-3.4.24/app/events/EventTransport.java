package events;

import java.io.Serializable;

/**
 * EventWrapper to transport the given information via hazelcast.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class EventTransport implements Serializable {
	public Long userId;
	public Event event;

	public EventTransport(final Long userId, final Event event) {
		this.userId = userId;
		this.event = event;
	}
}
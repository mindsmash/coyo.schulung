package events.type;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import models.User;
import controllers.api.WebRTC.Response;
import events.BaseEvent;

public class RTCRespondEvent extends BaseEvent {
	
	public static final String EVENT_TYPE = "rtcRespondEvent";
	
	public final static class Payload implements Serializable {
		public User sender;
		public Response response;
		
		public Payload(User sender, Response response) {
			this.sender = sender;
			this.response = response;
		}
	}

	public RTCRespondEvent(User sender, Response response) {
		super(EVENT_TYPE, new Payload(sender, response));
	}

}

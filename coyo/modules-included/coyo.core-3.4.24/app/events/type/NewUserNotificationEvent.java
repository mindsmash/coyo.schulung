package events.type;

import models.UserNotification;
import events.ModelEvent;

public class NewUserNotificationEvent extends ModelEvent {
	public NewUserNotificationEvent(UserNotification o) {
		super("newNotification", o);
	}
}

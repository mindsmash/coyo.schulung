package events.type;

import java.io.Serializable;

import acl.Permission;
import acl.PermissionChecker;
import onlinestatus.UserOnlineStatus;
import events.BaseEvent;
import security.CheckCaller;

/**
 * Event that is raised when a user's online status changes.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public class OnlineStatusChangedEvent extends BaseEvent {
	public final static class OnlineStatusChangedEventPayload implements Serializable {
		public Long userId;
		public UserOnlineStatus.OnlineStatus newStatus;
		public UserOnlineStatus.OnlineStatus oldStatus;
		public boolean isOnline;
		public Long onlineUserCount;
		public boolean isCallable;

		public OnlineStatusChangedEventPayload(Long userId, UserOnlineStatus oldStatus, UserOnlineStatus newStatus,
				Long onlineUserCount) {
			this.userId = userId;
			this.newStatus = newStatus.status;
			if (oldStatus != null) {
				this.oldStatus = oldStatus.status;
			}
			this.isOnline = newStatus.isOnline();
			this.onlineUserCount = onlineUserCount;
			this.isCallable = CheckCaller.check(Permission.IS_WEBRTC_CALLABLE, userId);
		}
	}

	public OnlineStatusChangedEvent(Long userId, UserOnlineStatus oldStatus, UserOnlineStatus newStatus,
			Long onlineUserCount) {
		super("onlineStatusChanged", new OnlineStatusChangedEventPayload(userId, oldStatus, newStatus, onlineUserCount));
	}
}

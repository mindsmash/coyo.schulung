package events.type;

import models.messaging.ConversationMember;
import events.ModelEvent;

public class NewConversationPostEvent extends ModelEvent {
	public NewConversationPostEvent(ConversationMember o) {
		super("newConversationPost", o);
	}
}

package events.type;

import events.BaseEvent;

public class RTCEvent extends BaseEvent {

	public static final String EVENT_TYPE = "rtcEvent";
	
	public RTCEvent(String jsonData) {
		super(EVENT_TYPE, jsonData);
	}

}
package events.type;

import controllers.api.WebRTC;
import models.User;
import events.BaseEvent;

import java.io.Serializable;

public class RTCCallEvent extends BaseEvent {

	public static final String EVENT_TYPE = "rtcCallEvent";

	public final static class Payload implements Serializable {
		public User sender;
		boolean audioOnly;

		public Payload(User sender, boolean audioOnly) {
			this.sender = sender;
			this.audioOnly = audioOnly;
		}
	}

	public RTCCallEvent(User sender, boolean audioOnly) {
		super(EVENT_TYPE, new Payload(sender, audioOnly));
	}

}

package events.type;

import events.BaseEvent;

import java.io.Serializable;

/**
 * Event that is raised when a likable was liked.
 */
public class LikeEvent extends BaseEvent {

	public final static class Payload implements Serializable {
		public String clazz;
		public Long id;

		public Payload(final Long id, final String clazzName) {
			this.id = id;
			this.clazz = clazzName;
		}
	}

	public LikeEvent(Long likeableId, final String clazzName) {
		super("LikeEvent", new Payload(likeableId, clazzName));
	}
}

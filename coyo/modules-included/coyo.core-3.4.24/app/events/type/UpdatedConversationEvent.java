package events.type;

import models.messaging.ConversationMember;
import events.ModelEvent;

public class UpdatedConversationEvent extends ModelEvent {
	public UpdatedConversationEvent(ConversationMember o) {
		super("updatedConversation", o);
	}
}

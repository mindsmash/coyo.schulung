package events;

import models.User;
import play.db.jpa.JPABase;
import plugins.play.EventServicePlugin;

public class ModelEvent extends BaseEvent {
	public ModelEvent(String type, JPABase data) {
		super(type, new EventModelWrapper(data));
	}

	@Override
	public Object getData() {
		return ((EventModelWrapper) super.getData()).get();
	}

	@Override
	public void raise(Long userId) {
		// do not publish this directly but defer to end of action invocation
		EventServicePlugin.addEventToPublish(userId, this);
	}
}

package events;

import play.Logger;
import play.libs.F.Promise;
import plugins.play.HazelcastPlugin;

import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

/**
 * 
 * EventProvider for multi server environments, publishes and manages
 * eventqueues via HazelCast.
 * 
 * @author pierswermbter
 * 
 */
public class HazelEventProvider extends EventProvider implements MessageListener<EventTransport> {

	protected final AsyncTasks<EventQueue> tasks = new AsyncTasks<EventQueue>();

	private ITopic<EventTransport> topic;
	private IAtomicLong idGenerator;

	/*
	 * Listens on incoming events.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hazelcast.core.MessageListener#onMessage(com.hazelcast.core.Message)
	 */
	@Override
	public void onMessage(Message<EventTransport> message) {
		Long userId = message.getMessageObject().userId;
		Event event = message.getMessageObject().event;

		if (event instanceof StreamCreatedEvent) {
			tasks.resolve(userId, queues.getOrCreate(userId));
			return;
		}

		queues.publishIfExists(userId, event);
	}

	@Override
	public synchronized void initialize() {
		register();
		init = true;
	}

	/**
	 * Register the HazelCast event listener and init the global ID counter.
	 */
	private void register() {
		if (Logger.isDebugEnabled()) {
			Logger.debug("Registered HazelCast Event Listener");
		}
		idGenerator = HazelcastPlugin.getHazel().getAtomicLong("eventID");
		topic = HazelcastPlugin.getHazel().getTopic("eventService");
		topic.addMessageListener(this);
	}

	@Override
	protected void publishEventOverTransport(Event event, Long id) {
		if (Logger.isDebugEnabled()) {
			Logger.debug("[EventService] Publishing Event '%s' for: %s", event.getType(), id);
		}

		/*
		 * Publish over HazelCast to insert the event into the users eventqueues
		 * on all servers
		 */
		topic.publish(new EventTransport(id, event));
	}

	@Override
	protected Long getNextEventId() {
		return idGenerator.incrementAndGet();
	}

	@Override
	public Promise<EventQueue> getOrCreateEventStream(Long id) {
		AsyncTasks<EventQueue>.AsyncTask defer = tasks.defer(id);

		/*
		 * Publish over HazelCast to create the users eventqueues on every
		 * server.
		 */
		topic.publish(new EventTransport(id, new StreamCreatedEvent()));

		return defer;
	}

	/**
	 * Published when hazelcast creates stream.
	 * 
	 * @author Daniel Busch, mindsmash GmbH
	 */
	public static class StreamCreatedEvent extends Event {

		public StreamCreatedEvent() {
			super("HazelcastStreamCreated");
		}

	}
}

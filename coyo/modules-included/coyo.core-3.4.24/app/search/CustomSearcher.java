package search;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import play.Play;
import fishr.search.SearchableModel;
import fishr.search.Searcher;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class CustomSearcher extends Searcher {

	private static final Float FUZZY_FACTOR = Float.valueOf(Play.configuration.getProperty("coyo.search.fuzzyFactor",
			"0.8"));

	@Override
	public Map<String, List<Class<SearchableModel>>> getCategories() {
		// custom sorting for search categories
		Map<String, List<Class<SearchableModel>>> categories = super.getCategories();
		Map<String, List<Class<SearchableModel>>> r = new LinkedHashMap<String, List<Class<SearchableModel>>>();
		r.put("users", categories.get("users"));
		r.put("pages", categories.get("pages"));
		r.put("workspaces", categories.get("workspaces"));
		r.put("events", categories.get("events"));
		r.putAll(categories);
		return r;
	}

	@Override
	protected float getFuzzyFactor() {
		return FUZZY_FACTOR;
	}
}
package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

public class CoreFastTags extends FastTags {

	public static void _pluralize(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		int count = Integer.parseInt(args.get("arg") + "");
		if (count == 1) {
			out.print(Messages.get(args.get("key") + ".singular"));
			return;
		}
		out.print(Messages.get(args.get("key") + ".plural"));
	}
}

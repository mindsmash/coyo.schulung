package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import play.Logger;
import play.exceptions.TemplateNotFoundException;
import play.mvc.Http.Request;
import play.templates.BaseTemplate;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.TagContext;
import play.templates.TemplateLoader;
import plugins.PluginBase;
import plugins.PluginException;
import plugins.PluginHook;
import plugins.PluginHooks;
import plugins.PluginManager;
import plugins.results.TemplatePluginResult;
import util.Util;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

@FastTags.Namespace("plugin")
public class PluginFastTags extends FastTags {

	public static void _hook(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		PluginHooks hook = (PluginHooks) args.get("arg");
		args.remove("arg");
		
		Monitor outerMonitor = MonitorFactory.start("PluginHook." + hook);
		Monitor monitor = null;

		// iterate plugins
		for (PluginBase plugin : PluginManager.getActivePlugins()) {
			// iterate affected methods
			for (Method method : plugin.getHookMethods(hook)) {

				try {
					monitor = MonitorFactory.start("Plugin[" + plugin.getKey() + "]." + hook);

					// check if hook applies to current action
					boolean applies = false;
					for (String action : method.getAnnotation(PluginHook.class).actions()) {
						if (Util.matches(Request.current().action, action)) {
							applies = true;
							break;
						}
					}

					if (applies) {
						try {
							// invoke plugin hook
							if (args.size() == 0) {
								method.invoke(plugin);
							} else {
								method.invoke(plugin, args);
							}
						} catch (InvocationTargetException e) {
							// look for result
							if (e.getCause() instanceof TemplatePluginResult) {
								TemplatePluginResult r = (TemplatePluginResult) e.getCause();

								BaseTemplate t = (BaseTemplate) TemplateLoader.load(r.template);
								Map<String, Object> newArgs = new HashMap<String, Object>();
								newArgs.putAll(template.getBinding().getVariables());
								newArgs.putAll(r.args);
								newArgs.put("_isInclude", true);
								t.render(newArgs);
							} else {
								throw e;
							}
						}
					}
					
					monitor.stop();
					monitor = null;
				} catch (Exception e) {
					Logger.error(e, "error executing plugin hook [%s] for plugin [%s]", hook, plugin);
				}
			}
		}
		
		outerMonitor.stop();
	}

	public static void _settings(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		try {
			PluginBase plugin = PluginManager.getPlugin("" + args.get("arg"));
			if (plugin != null) {
				try {
					plugin.renderSettings();
				} catch (TemplatePluginResult r) {
					BaseTemplate t = (BaseTemplate) TemplateLoader.load(r.template);
					Map<String, Object> newArgs = new HashMap<String, Object>();
					newArgs.putAll(template.getBinding().getVariables());
					newArgs.putAll(r.args);
					newArgs.put("_isInclude", true);
					t.render(newArgs);
					return;
				}
			}
		} catch (TemplateNotFoundException e) {
			throw new TemplateNotFoundException(e.getPath(), template.template, fromLine);
		} catch (Exception e) {
			throw new PluginException("error rendering plugin settings", e);
		}

		TagContext.parent().data.put("_executeNextElse", true);
	}
}

package utils.dbunit;

import java.sql.Types;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;

/**
 * Custom enhanced MySQL data type factory for dbunit imports to support casting
 * true|false string values to BIT.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class EnhancedMySqlDataTypeFactory extends MySqlDataTypeFactory {

	@Override
	public DataType createDataType(int sqlType, String sqlTypeName) throws DataTypeException {
		if (sqlType == Types.BIT) {
			return DataType.BOOLEAN;
		}
		DataType createDataType = super.createDataType(sqlType, sqlTypeName);
		return createDataType;
	}
}
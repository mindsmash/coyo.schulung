package utils;

import java.util.ArrayList;
import java.util.List;

import models.AbstractTenant;
import models.Sender;
import multitenancy.MTA;
import play.cache.Cache;

/**
 * Helps with caching Coyo entities.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class CacheUtils {

	private CacheUtils() {
	}

	/**
	 * Retrieves list of {@link Sender} from cache that were previously cached with {@link #cacheSenders(List)}.
	 * 
	 * Permissions are not checked.
	 * 
	 * @param cacheKey
	 * @return List of senders or null if none found.
	 */
	public static List<Sender> getCachedSenders(String cacheKey) {
		List<Long> ids = Cache.get(cacheKey, List.class);
		if (ids == null) {
			return null;
		}

		List<Sender> r = new ArrayList<>();
		for (Long id : ids) {
			Sender sender = Sender.findById(id);
			if (sender != null) {
				r.add(sender);
			}
		}
		return r;
	}

	/**
	 * Caches a list of senders. They can be retrieved with {@link #getCachedSenders(String)}.
	 * 
	 * @param senders
	 * @param expiration
	 *            e.g. 15min, 1h, 2d
	 */
	public static void cacheSenders(List<Sender> senders, String cacheKey, String expiration) {
		if (senders == null) {
			return;
		}
		List<Long> ids = new ArrayList<>();
		for (Sender sender : senders) {
			ids.add(sender.id);
		}
		Cache.set(cacheKey, ids, expiration);
	}

	/**
	 * Creates a cache key unique for each tenant. The tenant id is added to the passed prefix separated by a dash. If
	 * no tenant could be found, the string "NOTENANT" is appended. E.g. PREFIX-<ID>
	 */
	public static String createTenantUniqueCacheKey(String prefix) {
		AbstractTenant activeTenant = MTA.getActiveTenant();
		if (activeTenant != null) {
			return prefix + "-" + activeTenant.id;
		}
		return prefix + "-NOTENANT";
	}
}

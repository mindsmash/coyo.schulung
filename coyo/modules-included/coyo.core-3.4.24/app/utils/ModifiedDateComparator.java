package utils;

import java.util.Comparator;

import models.GenericBaseModel;

/**
 * Comparator to order model objects by modification date. Sort order can be
 * reversed to ascending by setting 'reverse' flag. Default order is descending.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class ModifiedDateComparator implements Comparator<GenericBaseModel> {

	private boolean reverse;

	public ModifiedDateComparator(boolean reverse) {
		this.reverse = reverse;
	}

	@Override
	public int compare(GenericBaseModel o1, GenericBaseModel o2) {
		int result = o1.modified.compareTo(o2.modified);
		return reverse ? -1 * result : result;
	}

}
package utils;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableMap;

/**
 * Provides a method to replace smilies in text (like ":)" or ";-)") by span elements which can be rendered in HTML.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class Smilifier {

	private static final String SMILEY_REPLACEMENT = "$1<span class=\"smiley smiley-%s\">%s</span>";

	private static final String SMILEY_REGEXP_PREFIX = "(^|\\s+)";

	private static final Map<String, Map.Entry<String, Pattern>> PATTERNS_MAP = new HashMap<>();


	static {
		final Map<String, String> smileyMap = ImmutableMap.<String, String> builder().put(":)", "smiling").put("=)", "smiling")
				.put(":-)", "smiling").put(";)", "winking").put(";-)", "winking").put(":D", "grinning")
				.put(":-D", "grinning").put(";D", "winking_grinning").put(";-D", "winking_grinning")
				.put(":-(", "frowning").put(":(", "frowning").put("=(", "frowning").put(":O", "gasping")
				.put(":-O", "gasping").put(":*", "kissing").put(":-*", "kissing").put(":p", "tongue_out_left")
				.put(":P", "tongue_out_left").put(":-P", "tongue_out_left").put(";P", "winking_tongue_out")
				.put(";-P", "winking_tongue_out").put("B|", "cool").put("B-|", "cool").put("8|", "cool")
				.put("8-|", "cool").put("^^", "happy_smiling").put("^_^", "happy_smiling").put(":-/", "unsure")
				.put(":X", "lips_sealed").put(":-X", "lips_sealed").put(":'(", "crying").put("&lt;3", "heart")
				.put("<3", "heart").build();

		for (String smiley : smileyMap.keySet()) {
			final String regExp = SMILEY_REGEXP_PREFIX + Pattern.quote(smiley);
			final String replacement = String.format(SMILEY_REPLACEMENT, smileyMap.get(smiley), smiley);
			final Pattern pattern = Pattern.compile(regExp);

			final Map.Entry<String,Pattern> pair = new AbstractMap.SimpleEntry<String, Pattern>(replacement, pattern);
			PATTERNS_MAP.put(smiley, pair);
		}

	}

	/**
	 * Smilify the incoming text. All supported smileys are replaced by a span element, that is displayed with an
	 * according smiley graphic in frontend. The passed text can be html escaped.
	 */
	public static String smilify(String text) {
		if (text != null) {
			for (String smiley : PATTERNS_MAP.keySet()) {
				final Map.Entry<String, Pattern> pair = PATTERNS_MAP.get(smiley);
				final Pattern pattern = pair.getValue();
				final String  replacement = pair.getKey();
				text = pattern.matcher(text).replaceAll(replacement);
			}
		}
		return text;
	}

}
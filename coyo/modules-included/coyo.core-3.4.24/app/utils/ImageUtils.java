package utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import com.drew.metadata.jpeg.JpegDirectory;
import com.sun.media.jai.codec.SeekableStream;
import org.imgscalr.Scalr;
import org.slf4j.LoggerFactory;
import play.Play;
import play.data.Upload;
import play.libs.Codec;
import storage.FlexibleBlob;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import javax.media.jai.JAI;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ImageUtils {
	private static final List<String> IMAGE_TYPES = new ArrayList<String>();

	static {
		IMAGE_TYPES.add("image/jpeg");
		IMAGE_TYPES.add("image/pjpeg");
		IMAGE_TYPES.add("image/gif");
		IMAGE_TYPES.add("image/png");
		IMAGE_TYPES.add("image/x-png");
		IMAGE_TYPES.add("image/tiff");
	}

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ImageUtils.class);


	public static void scaleToWidth(InputStream stream, String type, FlexibleBlob target, int width, boolean enlarge)
			throws IOException {
		if (stream == null || target == null) {
			return;
		}

		scaleToWidth(generateScaledToWidth(stream, type, width, enlarge), target, width);
	}

	public static void scaleToWidth(BufferedImage bi, FlexibleBlob target, int width) throws IOException {
		if (bi == null || target == null) {
			return;
		}

		File temp = new File(Play.tmpDir.getAbsolutePath() + File.separator + Codec.UUID());
		temp.createNewFile();

		// do it
		scaleToWidth(bi, temp, width);

		// that's all for the avatar, now save.
		target.set(new FileInputStream(temp), "image/png", temp.length());

		temp.delete();
	}

	public static void scaleToWidth(BufferedImage bi, File target, int width) throws IOException {
		if (bi == null || target == null) {
			return;
		}

		// resize to width and scale automatically
		ImageIO.write(bi, "png", target);
	}

	public static void scaleToWidth(InputStream stream, String type, File target, int width, boolean enlarge)
			throws IOException {
		if (stream == null || target == null) {
			return;
		}

		// resize to width and scale automatically
		ImageIO.write(generateScaledToWidth(stream, type, width, enlarge), "png", target);
	}

	public static boolean canHandleImage(InputStream stream, String type) {
		try {
			read(stream, type);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void setThumb(InputStream stream, String type, FlexibleBlob target, int width) throws IOException {
		if (stream == null || target == null) {
			return;
		}

		// create temp file
		File temp = new File(Play.tmpDir.getAbsolutePath() + File.separator + Codec.UUID());
		if (temp.createNewFile() && temp.canWrite()) {
			// write new thumb to temp
			BufferedImage bi = generateThumb(stream, type, width);
			if (bi != null) {
				ImageIO.write(bi, "png", temp);

				// load stream from temp into blob
				target.set(new FileInputStream(temp), "image/png", temp.length());
			}

			// done, delete temp
			temp.delete();
		}
	}

	public static void setThumb(InputStream stream, String type, File target, int width) throws IOException {
		if (stream == null || target == null) {
			return;
		}

		ImageIO.write(generateThumb(stream, type, width), "png", target);
	}

	private static BufferedImage generateThumb(InputStream stream, String type, int width) throws IOException {
		if (stream == null) {
			return null;
		}

		// now create a thumbnail from the avatar
		BufferedImage thumbnail = read(stream, type);
		if (thumbnail == null) {
			return null;
		}

		/**
		 * We need a square for the thumbnail. To avoid whitespace in the image, we first check if it's
		 * portrait of landscape format. If it's in portrait format, it is first compressed horizontally, landscape
		 * images are compressed vertically. Then a clip of THUMBNAIL_WIDTH x THUMBNAIL_WIDTH is taken from the middle
		 * of the image.
		 */
		if (thumbnail.getWidth() < thumbnail.getHeight()) {
			thumbnail = Scalr.resize(thumbnail, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH, width);
			int y = thumbnail.getHeight() / 2 - (width / 2);
			thumbnail = Scalr.crop(thumbnail, 0, y, width, width);
		} else {
			thumbnail = Scalr.resize(thumbnail, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_HEIGHT, width);
			int x = thumbnail.getWidth() / 2 - (width / 2);
			thumbnail = Scalr.crop(thumbnail, x, 0, width, width);
		}

		return thumbnail;
	}

	public static boolean isImage(Upload upload) {
		if (upload == null) {
			return false;
		}
		return isImage(upload.getContentType());
	}

	public static boolean isImage(FlexibleBlob blob) {
		if (blob == null) {
			return false;
		}
		return isImage(blob.type());
	}

	public static boolean isImage(String type) {
		return type != null && IMAGE_TYPES.contains(type);
	}

	/**
	 * Actually resizes an image to fit a width, keeping ratio.
	 *
	 * @param stream
	 * @param type
	 * @param width
	 * @param enlarge
	 * @return Resized image or null if something went wrong.
	 * @throws IOException
	 */
	public static BufferedImage generateScaledToWidth(InputStream stream, String type, int width, boolean enlarge)
			throws IOException {
		if (stream == null) {
			return null;
		}

		BufferedImage bi = read(stream, type);
		stream.close();
		if (bi == null) {
			return null;
		}

		if (!enlarge) {
			if (bi.getWidth() <= width) {
				return bi;
			}
		}

		// resize to width and scale automatically
		return Scalr.resize(bi, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH, width);
	}

	public static BufferedImage read(InputStream stream, String type) throws IOException {
		try {
			return ImageIO.read(stream);
		} catch (Exception unsupportedImageType) {
			// try to load with JAI
			SeekableStream seekableStream = SeekableStream.wrapInputStream(stream, true);
			ParameterBlock pb = new ParameterBlock();
			pb.add(seekableStream);
			if (IMAGE_TYPES.contains(type)) {
				return JAI.create(type.replaceAll("image/", ""), pb).getAsBufferedImage();
			}
		}
		return null;
	}

	/**
	 * Returns an rotated image if the image needs to be rotated else null.
	 * Uses jpeg exif image data to handle image rotation.
	 * Also returns null on error.
	 *
	 * @param file
	 * @return
	 */
	public static BufferedImage rotateImage(File file) throws IOException {
		try {
			final Metadata metadata = ImageMetadataReader.readMetadata(file);
			final Directory directory = metadata.getDirectory(ExifIFD0Directory.class);
			final JpegDirectory jpegDirectory = metadata.getDirectory(JpegDirectory.class);
			int orientation = 1;
			int width = 0;
			int height = 0;

			// if exif information is missing we just return
			if (directory == null) {
				return null;
			}
			// try to read dimensions if not found in directory
			if (jpegDirectory == null) {
				try (ImageInputStream input = ImageIO.createImageInputStream(file)) {
					final Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
					if (!readers.hasNext()) {
						throw new IllegalArgumentException("No reader for: " + file);
					}

					final ImageReader reader = readers.next();
					try {
						width = reader.getWidth(0);
						height = reader.getHeight(0);
					} finally {
						reader.dispose();
					}
				}
			} else {
				width = jpegDirectory.getImageWidth();
				height = jpegDirectory.getImageHeight();
			}

			if (directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
				orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
			}

			// if no rotation is needed or width/height is missing skip transformation
			if (orientation < 2 || width < 1 || height < 1) {
				return null;
			}

			// transform image (rotate)
			return transformImage(ImageIO.read(file), orientation, width, height);

		} catch (Exception e) {
			LOGGER.debug(e.getLocalizedMessage(), e);
		}

		return null;
	}


	public static InputStream createInputStreamFromBufferedImage(final BufferedImage image, final String mimeType)
			throws IOException {
		ImageWriter writer = ImageIO.getImageWritersByMIMEType(mimeType).next();
		ImageWriteParam writeParam = writer.getDefaultWriteParam();
		writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		writeParam.setCompressionQuality(0.95F); //

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		writer.setOutput(new MemoryCacheImageOutputStream(outputStream));
		IIOImage outputImage = new IIOImage(image, null, null);
		writer.write(null, outputImage, writeParam);
		writer.dispose();

		try (final InputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {
			return is;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Create tranformation information.
	 * Currently used to rotate an image.
	 *
	 * @param oldImage
	 * @param orientation
	 * @param width
	 * @param height
	 * @return
	 */
	private static BufferedImage transformImage(BufferedImage oldImage, final int orientation, final int width, final int height) throws Exception {
		BufferedImage newImage = new BufferedImage(
				oldImage.getWidth(),
				oldImage.getHeight(),
				(oldImage.getTransparency() == Transparency.OPAQUE ?
						BufferedImage.TYPE_INT_RGB :
						BufferedImage.TYPE_INT_ARGB));
		Graphics2D graphics = (Graphics2D) newImage.getGraphics();


		switch (orientation) {
			case 1:
				break;
			case 2: // Flip X
				graphics.scale(-1.0, 1.0);
				graphics.translate(-width, 0);
				break;
			case 3: // PI rotation
				graphics.translate(width, height);
				graphics.rotate(Math.PI);
				break;
			case 4: // Flip Y
				graphics.scale(1.0, -1.0);
				graphics.translate(0, -height);
				break;
			case 5: // - PI/2 and Flip X
				graphics.rotate(-Math.PI / 2);
				graphics.scale(-1.0, 1.0);
				break;
			case 6: // -PI/2 and -width
				newImage = new BufferedImage(
						oldImage.getHeight(),
						oldImage.getWidth(),
						(oldImage.getTransparency() == Transparency.OPAQUE ?
								BufferedImage.TYPE_INT_RGB :
								BufferedImage.TYPE_INT_ARGB));
				graphics = (Graphics2D) newImage.getGraphics();
				graphics.rotate(Math.toRadians(90), newImage.getWidth() / 2, newImage.getHeight() / 2);
				graphics.translate((newImage.getWidth() - oldImage.getWidth()) / 2, (newImage.getHeight() - oldImage.getHeight()) / 2);
				break;
			case 7: // PI/2 and Flip
				graphics.scale(-1.0, 1.0);
				graphics.translate(-height, 0);
				graphics.translate(0, width);
				graphics.rotate(3 * Math.PI / 2);
				break;
			case 8: // PI / 2
				graphics.translate(0, width);
				graphics.rotate(3 * Math.PI / 2);
				break;
		}

		graphics.drawImage(oldImage, 0, 0, oldImage.getWidth(), oldImage.getHeight(), null);

		graphics.dispose();
		return newImage;
	}
}

package utils;

/**
 * Utils for videos.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class VideoUtils {

	private VideoUtils() {
	}

	public static enum VideoType {
		MP4("video/mp4", "mp4"), OGG("video/ogg", "ogv"), WEBM("video/webm", "webm");

		private String mimeType;
		private String fileSuffix;

		private VideoType(String mimeType, String fileSuffix) {
			this.mimeType = mimeType;
			this.fileSuffix = fileSuffix;
		}

		public String getMimeType() {
			return mimeType;
		}

		public String getFileSuffix() {
			return fileSuffix;
		}
		
		public static VideoType getByFileSuffix(String fileSuffix) {
			for(VideoType vt : values()) {
				if(vt.getFileSuffix().equals(fileSuffix)) {
					return vt;
				}
			}
			return null;
		}
	}

	public static boolean isVideo(String mimeType) {
		if (mimeType != null) {
			for (VideoType vt : VideoType.values()) {
				if (vt.getMimeType().equals(mimeType)) {
					return true;
				}
			}
		}
		return false;
	}
}

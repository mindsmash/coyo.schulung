package utils;

import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.User;
import models.medialibrary.MediaFile;

import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import acl.Permission;
import controllers.Security;
import play.Logger;
import play.mvc.Router;
import play.templates.BaseTemplate.RawData;
import theme.Theme;

public class PDFUtil {

	private static final String MEDIA_FILE_URL = Router.reverse("MediaLibraries.file").url;
	private static final Pattern MEDIA_FILE_ID_PATTERN = Pattern
			.compile(Pattern.quote(MEDIA_FILE_URL) + "\\?id=(\\d+)");

	/**
	 * Replaces all relative image src links with links to a representation of the image itself in the local file
	 * storage. If no image could be found, the link is not replaced and a class "no-img" is added to the image tag. In
	 * addition a warn message is logged.
	 * 
	 * At the moment only image links pointing to an image within the MediaLibrary are supported. Other images like
	 * images in wall posts or photo albums, will not be recognized.
	 */
	public static RawData replaceInternalImageLinks(String content) {
		/* use Jsoup to find all image tags */
		Document document = Jsoup.parse(content);
		Elements imageTags = document.select("img");

		/* process every single image and get the links */
		for (Element element : imageTags) {
			String srcAttr = element.attr("src");

			try {
				/* Only process relative links. Absolute links of images are processed by pdf module */
				URI uri = new URI(srcAttr, false);
				if (uri.isRelativeURI()) {
					/* parse image url and get media file url */
					Matcher matcher = MEDIA_FILE_ID_PATTERN.matcher(uri.toString());
					while (matcher.find()) {
						Long id = NumberUtils.toLong(matcher.group(1));
						MediaFile file = MediaFile.findById(id);
						if (file != null && Security.check(Permission.ACCESS_SENDER, file.library.getSenderId())) {
							/* replace img urls by file urls */
							String url = file.data.asFile().toURI().toURL().toString();
							element.attr("src", url);
						} else {
							Logger.warn(
									"[PDFUtil] Could not find image for url '%s' or permissions are not sufficient.",
									uri.toString());
							element.addClass("no-img");
						}
					}
				}

			} catch (URIException | NullPointerException e) {
				ErrorHelper.handleWarning(e, "[PDFUtil] Invalid URL for image '%s'.", srcAttr);
				element.addClass("no-img");
			} catch (MalformedURLException e) {
				ErrorHelper.handleWarning(e, "[PDFUtil] Invalid file path for image with url '%s'.", srcAttr);
				element.addClass("no-img");
			} catch (Exception e) {
				ErrorHelper.handleWarning(e, "[PDFUtil] Could not process image with url '%s'.", srcAttr);
				element.addClass("no-img");
			}
		}
		return new RawData(document.html());
	}

}
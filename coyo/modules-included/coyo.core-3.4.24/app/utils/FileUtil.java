package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import models.app.files.File;

public class FileUtil {

	private static final List<String> OFFICE_DOC_TYPES = Arrays.asList("doc", "docx", "xls", "xlsx", "xlsm", "ppt", "ppsx",
			"pptx", "vsd", "vsdx");

	private FileUtil() {
	}

	public static List<File> getBreadcrumb(File file) {
		List<File> r = new ArrayList<File>();
		File current = file.getParent();
		while (current != null) {
			r.add(current);
			current = current.getParent();
		}
		Collections.reverse(r);
		return r;
	}

	public static String getDisplayName(File file) {
		if (file.isFolder() || file.getName().indexOf(".") < 0) {
			return file.getName();
		}
		String name = file.getName();
		return name.substring(0, name.lastIndexOf("."));
	}

	public static String getFileExtension(File file, boolean withDot) {
		if (file.isFolder() || file.getName().indexOf(".") < 0) {
			return "";
		}
		return file.getName().substring(file.getName().lastIndexOf(".") + (withDot ? 0 : 1)).toLowerCase();
	}

	public static boolean isOfficeDoc(File file) {
		return OFFICE_DOC_TYPES.contains(getFileExtension(file, false).toLowerCase());
	}

	public static boolean hasFolderChild(File file) {
		for (File child : file.getChildren()) {
			if (child.isFolder()) {
				return true;
			}
		}
		return false;
	}
}

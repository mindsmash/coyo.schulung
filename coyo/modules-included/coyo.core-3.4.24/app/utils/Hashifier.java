package utils;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.HashtagBlackList;
import play.mvc.Router;
import fishr.util.ConvertionUtils;

/**
 * Provides a method to replace hashtags by links to search request for the hashtags.
 */
public class Hashifier {
	private static final String SearchURL;
	private static final String REPLACEMENT;
	private static final Pattern HEX_COLOR_PATTERN = Pattern.compile("([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})");
	
	public static final String HASHTAG_BLACKLIST = "HASHTAG_BLACKLIST";
	public static final String HASHTAG_REGEXP = ConvertionUtils.HASHTAG_PATTERN.toString();

	static {
		SearchURL = Router.reverse("Search.index").url;
		REPLACEMENT = "$1<a href=\"" + SearchURL + "?searchString=%23$2\">#$2</a>";
	}

	/**
	 * Convenience method for {@link #hashify(String, boolean)}. Sets 'skipHexColors' to 'false'.
	 */
	public static String hashify(final String text) {
		return hashify(text, false);
	}

	/**
	 * Hashify text
	 */
	public static String hashify(final String text, boolean skipHexColors) {
		if (text != null) {
			final String key = CacheUtils.createTenantUniqueCacheKey(HASHTAG_BLACKLIST);
			Set<String> hashTagBlackList = (Set<String>) play.cache.Cache.get(key);

			if (hashTagBlackList == null) {
				hashTagBlackList = HashtagBlackList.getHashtags();
				play.cache.Cache.set(key, hashTagBlackList, "1min");
			}

			final Matcher m = ConvertionUtils.HASHTAG_PATTERN.matcher(text);
			final StringBuffer buffer = new StringBuffer();

			while (m.find()) {
				final String tagName = m.group(ConvertionUtils.HASHTAG_MATCHING_GROUP);
				if (skipHexColors && HEX_COLOR_PATTERN.matcher(tagName).matches()) {
					continue;
				}
				if (!hashTagBlackList.contains(tagName)) {
					m.appendReplacement(buffer, REPLACEMENT);
				} else {
					m.appendReplacement(buffer, m.group());
				}
			}
			m.appendTail(buffer);

			return buffer.toString();
		}

		return text;
	}

}
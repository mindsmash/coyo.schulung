package utils;

import java.lang.reflect.Field;

import models.BaseModel;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.mvc.Http.Request;

public class JPAUtils {

	private JPAUtils() {
	}

	// /**
	// * force data commit in case that transaction is read-only by controller
	// */
	// public static void safeCommit() {
	// if (isTransactionReadOnly()) {
	// Logger.debug("transaction is read only. committing manually [%s]",
	// Request.current().action);
	//
	// if (JPA.isInsideTransaction()) {
	// JPA.em().getTransaction().commit();
	// } else {
	// Logger.warn("cannot safely commit a transaction but none is active");
	// }
	// }
	// }

	public static boolean isTransactionReadOnly() {
		try {
			Field f = JPA.class.getDeclaredField("readonly");
			f.setAccessible(true);
			return (Boolean) f.get(JPA.local.get());
		} catch (Exception e) {
			Logger.warn(e, "error determining transaction readonly status");
		}
		return false;
	}

	public static void makeTransactionWritable() {
		if (JPA.isInsideTransaction() && isTransactionReadOnly()) {
			Logger.debug("changing transaction status to NOT-readonly [%s]", Request.current().action);

			try {
				Field f = JPA.class.getDeclaredField("readonly");
				f.setAccessible(true);
				f.set(JPA.local.get(), false);
			} catch (Exception e) {
				Logger.warn(e, "error changing transaction status to NOT-readonly");
			}
		}
	}

	public static void commitAndRefreshTransaction() {
		JPAPlugin plugin = new JPAPlugin();
		plugin.closeTx(false);
		plugin.startTx(false);
	}
	
	public static <T extends BaseModel> T ensureManaged(BaseModel model) {
		if(JPA.em().contains(model)) {
			return (T) model;
		}
		return (T) JPA.em().find(model.getClass(), model.id);
	}
}

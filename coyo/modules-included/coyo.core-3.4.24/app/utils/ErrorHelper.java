package utils;

import org.slf4j.Logger;

public class ErrorHelper {

	/**
	 * Uses the play logger to handle a warning. A warning is logged with stack trace in debug mode, but without it
	 * otherwise.
	 * 
	 * @deprecated Use {@link ErrorHelper#handleWarning(Logger, Throwable, String, Object...) instead.}
	 */
	public static void handleWarning(Throwable e, String message, Object... args) {
		if (play.Logger.isDebugEnabled()) {
			play.Logger.warn(e, message, args);
		} else {
			play.Logger.warn(message, args);
		}
	}

	/**
	 * A warning is logged with stack trace in debug mode, but without it otherwise.
	 */
	public static void handleWarning(Logger log, Throwable e, String message, Object... args) {
		String formattedMessage = String.format(message, args);
		if (log.isDebugEnabled()) {
			log.warn(formattedMessage, e);
		} else {
			log.warn(formattedMessage);
		}
	}

}
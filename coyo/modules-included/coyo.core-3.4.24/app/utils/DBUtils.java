package utils;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import play.Play;
import play.db.DB;
import play.exceptions.UnexpectedException;

/**
 * @author Jan Marquardt, mindsmash GmbH
 */
public class DBUtils {

	private DBUtils() {
	}

	/**
	 * @return True if app is running with H2 DB.
	 */
	public static boolean isH2() {
		try {
			DatabaseMetaData metaData = DB.getConnection().getMetaData();
			return metaData != null && metaData.getDatabaseProductName() != null
					&& metaData.getDatabaseProductName().toLowerCase().equals("h2");
		} catch (SQLException e) {
			throw new UnexpectedException(e);
		}
	}
	
	public static boolean showH2Warning() {
		return isH2() && Play.mode.isProd();
	}
}

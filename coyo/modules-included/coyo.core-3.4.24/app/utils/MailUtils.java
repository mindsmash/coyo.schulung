package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import play.Play;
import play.mvc.Http;

public class MailUtils {

	private MailUtils() {
	}

	public static String findAndReplaceLocalLinks(String html) {
		Pattern hrefPattern = Pattern.compile("(href=)\"/(.*?)\"");
		Matcher hrefMatcher = hrefPattern.matcher(html);
		html = hrefMatcher.replaceAll("href=\"" + getBaseUrl() + "/$2\"");

		Pattern srcPattern = Pattern.compile("(src=)\"/(.*?)\"");
		Matcher srcMatcher = srcPattern.matcher(html);
		html = srcMatcher.replaceAll("src=\"" + getBaseUrl() + "/$2\"");

		return html;
	}

	// copied from Router.getBaseUrl()
	protected static String getBaseUrl() {
		if (Http.Request.current() == null) {
			// No current request is present - must get baseUrl from config
			String appBaseUrl = Play.configuration.getProperty("application.baseUrl", "application.baseUrl");
			if (appBaseUrl.endsWith("/")) {
				// remove the trailing slash
				appBaseUrl = appBaseUrl.substring(0, appBaseUrl.length() - 1);
			}
			return appBaseUrl;

		} else {
			return Http.Request.current().getBase();
		}
	}
}

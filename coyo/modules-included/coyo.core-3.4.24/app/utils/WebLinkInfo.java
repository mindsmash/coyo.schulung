package utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.User;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.libs.F.Promise;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Http.StatusCode;
import play.templates.JavaExtensions;
import session.UserLoader;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

import controllers.TokenAuthController;

/**
 * @author Drews Clausen, Jan Marquardt
 */
public class WebLinkInfo implements Serializable {

	public static final String NO_TITLE = "No title";

	private static final Pattern BASE_URL_PATTERN = Pattern.compile("(https?://[a-zA-Z0-9:.-]+).*");

	public static final long HTTP_TIMEOUT = Long.parseLong(Play.configuration.getProperty(
			"mindsmash.weblinkinfo.timeout", "10"));

	// TODO add more attributes to ensure trying everything to get the right
	// data
	private final static String[] titleAttributes = { "og:title", "og:site_name", "title" };
	private final static String[] descriptionAttributes = { "og:description", "description" };
	private final static String[] imageAttributes = { "og:image" };
	private final static String[] videoAttributes = { "og:video" };

	private Map<String, String> webdata;
	private String url;

	private String title;
	private String description;
	private String thumbnailUrl;
	private String videoUrl;

	private WebLinkInfo(String url) {
		this.url = url;
	}

	// caching
	public static final WebLinkInfo get(String url) {
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		return new WebLinkInfo(url);
	}

	public String getTitle() {
		if (title == null) {
			initLazy();

			for (String name : titleAttributes) {
				if (webdata.containsKey(name)) {
					title = StringEscapeUtils.unescapeHtml(webdata.get(name));
				}
			}

			// still null, use title tag
			if (title == null) {
				title = webdata.get("titleTag");
			}

			// default
			if (title == null) {
				title = NO_TITLE;
			}
		}

		return title;
	}

	public String getDescription() {
		if (description == null) {
			initLazy();

			for (String name : descriptionAttributes) {
				if (webdata.containsKey(name)) {
					description = StringEscapeUtils.unescapeHtml(webdata.get(name));
				}
			}

			// default
			if (description == null) {
				description = "";
			}
		}
		return description;
	}

	public String getFaviconUrl() {
		initLazy();
		return webdata.get("faviconUrl");
	}

	public String getThumbnailUrl() {
		if (thumbnailUrl == null) {
			initLazy();

			for (String name : imageAttributes) {
				if (webdata.containsKey(name)) {
					thumbnailUrl = webdata.get(name);
				}
			}
		}
		return thumbnailUrl;
	}

	public String getVideoUrl() {
		if (videoUrl == null) {
			initLazy();

			for (String name : videoAttributes) {
				if (webdata.containsKey(name)) {
					videoUrl = webdata.get(name);
				}
			}

			// custom YouTube logic here (force SSL)
			if (videoUrl != null && videoUrl.startsWith("http://www.youtube.com")) {
				videoUrl = videoUrl.replaceAll("http://www.youtube.com", "https://www.youtube.com");
			}
		}

		return videoUrl;
	}

	public void initLazy() {
		if (webdata == null) {
			String cacheKey = "weblinkinfo-" + JavaExtensions.slugify(url);
			webdata = Cache.get(cacheKey, Map.class);

			if (webdata == null) {
				Monitor m = MonitorFactory.start("WebLinkInfo.initLazy");
				Logger.debug("[WebLinkInfo] Fetching from %s", url);

				webdata = new HashMap<String, String>();

				try {
					User user = UserLoader.getConnectedUser();
					WSRequest request = WS.url(url);

					// if requesting myself, use token authentication
					final String baseURl = Play.configuration.getProperty("application.baseUrl");
					if (StringUtils.isNotEmpty(baseURl) && url.startsWith(baseURl)) {
						request.setHeader(TokenAuthController.HEADER_NAME, TokenAuthController.createToken(user.id));
						request.setParameter(TokenAuthController.HEADER_NAME, TokenAuthController.createToken(user.id));
					}

					Promise<HttpResponse> promise = request.getAsync();
					HttpResponse response = promise.get(HTTP_TIMEOUT, TimeUnit.SECONDS);
					if (response == null) {
						Logger.debug("[WebLinkInfo] Response is null for url [%s]", url);
						return;
					}

					if (response.getStatus() != StatusCode.OK) {
						Logger.debug("[WebLinkInfo] Response status [%s] for url [%s]", response.getStatus(), url);
					}

					String headContentsStr = response.getString();
					HtmlCleaner cleaner = new HtmlCleaner();

					// parse the string HTML
					TagNode pageData = cleaner.clean(headContentsStr);

					// try default favicon URL
					Matcher matcher = BASE_URL_PATTERN.matcher(url);
					if (matcher.matches()) {
						String fvUrl = matcher.group(1) + "/favicon.ico";
						if (WS.url(fvUrl).headAsync().get(3, TimeUnit.SECONDS).success()) {
							webdata.put("faviconUrl", fvUrl);
						}
					}
					if (!webdata.containsKey("faviconUrl")) {
						// try to find favicon
						TagNode[] linkData = pageData.getElementsByName("link", true);
						for (TagNode linkElement : linkData) {
							if (linkElement.hasAttribute("rel") && linkElement.hasAttribute("href")
									&& "shortcut icon".equals(linkElement.getAttributeByName("rel"))) {
								webdata.put("faviconUrl", linkElement.getAttributeByName("href"));
							}
						}
					}

					// load meta tags
					TagNode[] metaData = pageData.getElementsByName("meta", true);
					for (TagNode metaElement : metaData) {
						String target = null;
						if (metaElement.hasAttribute("property"))
							target = "property";
						else if (metaElement.hasAttribute("name"))
							target = "name";

						if (target != null) {
							String attr = metaElement.getAttributeByName(target);
							String content = metaElement.getAttributeByName("content");
							webdata.put(attr, content);
						}
					}

					// load title tag
					TagNode[] titleData = pageData.getElementsByName("title", true);
					if (titleData.length > 0) {
						webdata.put("titleTag", titleData[0].getText().toString());
					}

					Cache.add(cacheKey, webdata, "24h");
					m.stop();
				} catch (Exception e) {
					Logger.debug(e, "[WebLinkInfo] Error pasing url [%s]", url);
				}
			} else {
				Logger.debug("[WebLinkInfo] Fetching from %s from cache", url);
			}
		}
	}
}

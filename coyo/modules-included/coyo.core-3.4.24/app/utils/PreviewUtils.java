package utils;

import com.google.gson.JsonObject;
import injection.Inject;
import injection.InjectionSupport;
import models.FileAttachment;
import models.Previewable;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import play.Logger;
import play.libs.Crypto;
import play.mvc.Router;
import storage.FlexibleBlob;
import theme.Theme;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@InjectionSupport
public class PreviewUtils {
	@Inject(configuration = "coyo.pdfRenderer.class", defaultClass = PDFRenderer.class)
	private static PDFRenderer pdfRenderer;

	private PreviewUtils() {
	}

	public static boolean hasPreview(Previewable previewable) {
		return isImage(previewable) || isPDF(previewable) || isAudio(previewable) || isVideo(previewable);
	}

	public static boolean isImage(Previewable previewable) {
		return ImageUtils.isImage(previewable.getContentType());
	}

	public static boolean isPDF(Previewable previewable) {
		return "application/pdf".equals(previewable.getContentType());
	}

	public static boolean isAudio(Previewable previewable) {
		return previewable.getFileName().endsWith(".mp3");
	}

	public static boolean isVideo(Previewable previewable) {
		String name = previewable.getFileName();
		return name.endsWith(".mp4")
				|| (name.endsWith(".mov") && !"video/quicktime".equalsIgnoreCase(previewable.getContentType()))
				|| name.endsWith(".m4v") || name.endsWith(".ogg") || name.endsWith(".ogv");
	}

	public static String getImagePreviewUrl(Previewable previewable, int width) {
		try {
			if (isImage(previewable)) {
				String previewKey = "preview-" + previewable.getUniqueId() + "-" + width;

				// try to load existing preview
				FlexibleBlob preview = getPreview(previewKey, previewable.getContentType());

				// generate new preview
				if (!preview.exists()) {
					Logger.debug("creating image preview file: %s", previewKey);

					final FlexibleBlob flexibleBlob = previewable.getFileData();
					final BufferedImage rotatedImage = ImageUtils.rotateImage(flexibleBlob.asFile());
					final InputStream inputStream;

					if (rotatedImage != null && flexibleBlob != null) {
						try (InputStream is = ImageUtils.createInputStreamFromBufferedImage(rotatedImage, flexibleBlob.type())) {
							flexibleBlob.set(is, flexibleBlob.type(), flexibleBlob.length());
							is.reset();
							return downscaleImage(previewable, width, previewKey, preview, is);
						} catch (Exception e) {
							Logger.debug("[Preview] failed to rotate image");
						}
					} else {
						inputStream = flexibleBlob.get();
						return downscaleImage(previewable, width, previewKey, preview, inputStream);
					}

				} else {
					return getImageUrl(previewable.getParentSenderId(), previewKey);
				}
			}
		} catch (Exception e) {
			ErrorHelper.handleWarning(e, "[Preview] Could not create preview for image '%s'", previewable);
		}

		return previewable.getDownloadUrl();
	}

	private static String downscaleImage(Previewable previewable, int width, String previewKey, FlexibleBlob preview, InputStream is) throws IOException {
		ImageUtils.scaleToWidth(is, previewable.getContentType(), preview, width, false);
		return getImageUrl(previewable.getParentSenderId(), previewKey);
	}

	public static String getPDFPreviewUrl(Previewable previewable) {
		try {
			if (isPDF(previewable)) {
				int width = 100;

				String previewKey = "preview-" + previewable.getUniqueId() + "-" + width;

				// try to load existing preview
				FlexibleBlob preview = getPreview(previewKey, previewable.getContentType());

				// generate new preview
				if (!preview.exists()) {
					Logger.debug("creating PDF preview file: %s", previewKey);
					InputStream previewableInputStream = previewable.getFileData().get();
					try {
						BufferedImage previewImage = pdfRenderer.convertFirstPageToImage(previewableInputStream);

						// this could happen if the PDF file does not have at
						// least 1 page
						if (previewImage == null) {
							return Theme.getResourceUrl("/images/pdf-preview.png");
						}

						ImageUtils.scaleToWidth(previewImage, preview, width);
					} finally {
						if (previewableInputStream != null) {
							previewableInputStream.close();
						}
					}
				}

				return getImageUrl(previewable.getParentSenderId(), previewKey);
			}
		} catch (Exception e) {
			ErrorHelper.handleWarning(e, "[Preview] Could not create preview for image '%s'", previewable);
			return Theme.getResourceUrl("/images/pdf-preview.png");
		}
		return null;
	}

	public static FlexibleBlob getPreview(String key, String type) {
		return new FlexibleBlob(key, type);
	}

	private static String getImageUrl(Long id, String key) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", Crypto.encryptAES(((id != null) ? id : -1) + "----" + key));
		return Router.reverse("Resource.imagePreview", args).url;
	}

	public static class PDFRenderer {
		public BufferedImage convertFirstPageToImage(InputStream pdfInputStream) throws Exception {
			PDDocument document = null;
			try {
				document = PDDocument.load(pdfInputStream);
				List<PDPage> pages = document.getDocumentCatalog().getAllPages();
				if(document.isEncrypted()) {
					return null;
				}
				if (pages.size() > 0) {
					return pages.get(0).convertToImage(BufferedImage.TYPE_INT_RGB, 72);
				}
			} finally {
				if (document != null) {
					document.close();
				}
			}

			return null;
		}
	}

	public static JsonObject createJsonPreview(final FileAttachment attachment) {
		JsonObject preview = new JsonObject();
		// image, pdf, audio, video, download
		if (PreviewUtils.isImage(attachment)) {
			preview.addProperty("type", "image");
			preview.addProperty("url", PreviewUtils.getImagePreviewUrl(attachment, 500));
		} else if (PreviewUtils.isPDF(attachment)) {
			String pdfUrl = PreviewUtils.getPDFPreviewUrl(attachment);
			if (null != pdfUrl) {
				preview.addProperty("type", "pdf");
				preview.addProperty("url", pdfUrl);
			}
		} else if (PreviewUtils.isAudio(attachment)) {
			preview.addProperty("type", "audio");
		} else if (PreviewUtils.isVideo(attachment)) {
			preview.addProperty("type", "video");
		}

		return preview;
	}
}

package utils;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.BadRequest;
import play.mvc.results.RenderBinary;
import storage.FlexibleBlob;

/**
 * Helper for streaming files in HTTP.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class HTTPStreamUtils {

	private HTTPStreamUtils() {
	}

	public static void stream(FlexibleBlob blob, String fileName, Request request, Response response) throws Exception {
		response.setHeader("Accept-Ranges", "bytes");

		if (request.headers.containsKey("range")) {
			String range = request.headers.get("range").value();
			if (!range.startsWith("bytes=")) {
				throw new BadRequest();
			}

			String[] parts = range.split("=")[1].split("-");
			long length = blob.length();
			long start = Long.valueOf(parts[0]);
			long end = length - 1;
			if (parts.length > 1) {
				end = Long.valueOf(parts[1]);
			}
			long chunk = end - start + 1;

			response.status = 206;
			response.setHeader("Content-Range", "bytes " + start + "-" + end + "/" + length);

			// if not the full file was requested
			if (chunk != length) {
				throw new RenderBinary(new ContentRangeInputStream(blob.get(), BigInteger.valueOf(start),
						BigInteger.valueOf(chunk)), fileName, chunk, blob.type(), true);
			}
		}

		throw new RenderBinary(blob.get(), fileName, blob.length(), blob.type(), true);
	}

	public static class ContentRangeInputStream extends FilterInputStream {

		private static final int BUFFER_SIZE = 4096;

		private long offset;
		private long length;
		private long remaining;

		public ContentRangeInputStream(InputStream stream, BigInteger offset, BigInteger length) {
			super(stream);

			this.offset = offset != null ? offset.longValue() : 0;
			this.length = length != null ? length.longValue() : Long.MAX_VALUE;

			this.remaining = this.length;

			if (this.offset > 0) {
				skipBytes();
			}
		}

		private void skipBytes() {
			long remainingSkipBytes = offset;

			try {
				while (remainingSkipBytes > 0) {
					long skipped = super.skip(remainingSkipBytes);
					remainingSkipBytes -= skipped;

					if (skipped == 0) {
						// stream might not support skipping
						skipBytesByReading(remainingSkipBytes);
						break;
					}
				}
			} catch (IOException e) {
				throw new RuntimeException("Skipping the stream failed!", e);
			}
		}

		private void skipBytesByReading(long remainingSkipBytes) {
			try {
				final byte[] buffer = new byte[BUFFER_SIZE];
				while (remainingSkipBytes > 0) {
					long skipped = super.read(buffer, 0, (int) Math.min(buffer.length, remainingSkipBytes));
					if (skipped == -1) {
						break;
					}

					remainingSkipBytes -= skipped;
				}
			} catch (IOException e) {
				throw new RuntimeException("Reading the stream failed!", e);
			}
		}

		@Override
		public boolean markSupported() {
			return false;
		}

		@Override
		public long skip(long n) throws IOException {
			if (remaining <= 0) {
				return 0;
			}

			long skipped = super.skip(n);
			remaining -= skipped;

			return skipped;
		}

		@Override
		public int available() throws IOException {
			if (remaining <= 0) {
				return 0;
			}

			int avail = super.available();

			if (remaining < avail) {
				return (int) remaining;
			}

			return avail;
		}

		@Override
		public int read() throws IOException {
			if (remaining <= 0) {
				return -1;
			}

			remaining--;

			return super.read();
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			if (remaining <= 0) {
				return -1;
			}

			int readBytes = super.read(b, off, (int) Math.min(len, remaining));
			if (readBytes == -1) {
				return -1;
			}

			remaining -= readBytes;

			return readBytes;
		}

		@Override
		public int read(byte[] b) throws IOException {
			return read(b, 0, b.length);
		}
	}
}

package utils;

import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import play.Logger;
import play.Play;

import java.io.File;
import java.io.IOException;

public class HardDriveUtil {
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HardDriveUtil.class);

	public static final String ATTACHMENTS_PATH = Play.configuration.getProperty("attachments.path", "");


	private HardDriveUtil() {
	}

	public static long getTotalSpace() {
		return getFreeSpace() + getCurrentUsage();
	}

	public static long getCurrentUsage() {
		try {
			return FileUtils.sizeOfDirectory(new File(ATTACHMENTS_PATH));
		} catch (Exception ex) {
		 	Logger.debug(ex.getLocalizedMessage(), ex);
			return 0;
		}
	}

	public static long getFreeSpace() {
		String folderToCheck = ATTACHMENTS_PATH;
		File folder = new File(folderToCheck);
		if(!folder.exists() || !folder.canRead()) {
			folderToCheck = System.getProperty("user.dir");
		}

		try {
			return FileSystemUtils.freeSpaceKb(folderToCheck) * FileUtils.ONE_KB;

		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
			return 0;
		}
	}

	public static boolean hasEnoughSpace() {
		return getTotalSpace() > 524288000L; // 500 MB
	}
}

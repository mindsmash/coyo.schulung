package utils;

import models.User;
import org.apache.commons.validator.routines.UrlValidator;
import play.Logger;
import play.mvc.Router;
import play.templates.BaseTemplate.RawData;
import play.templates.JavaExtensions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jan Marquardt
 */
public class Linkifier {

	public static final String MAILTO_TEMPLATE = "<a href=\"mailto:$2\" target=\"blank\">$1$2</a>";
	public static final String URL_TEMPLATE = "$1<a href=\"http://$2\" target=\"blank\">$2</a>";
	public static final String RELATIVE_URL_TEMPLATE = "<a href=\"$1\" target=\"blank\">$1</a>";

	public static final Pattern httpLinks = Pattern.compile("(https?:\\/\\/[a-zA-Z0-9-+&@#\\/%?=~_|!:,.;]+)");
	public static final Pattern fileLinks = Pattern.compile("(file:\\/\\/[a-zA-Z0-9-+&@#\\/%?=~_|!:,.;]+)");
	public static final Pattern wwwLinks = Pattern.compile("(^|\\s)(www\\.[a-zA-Z0-9-+&@#\\/%?=~_|!:,.;]+)");
	public static final Pattern mailLinks = Pattern.compile("(^|\\s+)([a-zA-Z0-9-_.]+@[a-zA-Z0-9-]+\\.[a-zA-Z.]+)");

	// source:  org.apache.commons.validator.routines.UrlValidator
	public static final Pattern validUrlPattern = Pattern
			.compile("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");

	private Linkifier() {
		// hide
	}

	public static String linkify(String text) {
		if (text != null) {
			final String[] schemes = { "http", "https" };
			final UrlValidator urlValidator = new UrlValidator(schemes);

			// find http links
			final Matcher httpMatcher = httpLinks.matcher(text);

			while (httpMatcher.find()) {
				final String httpTxt = httpMatcher.group(1);

				final Matcher validUrlMatcher = validUrlPattern.matcher(httpTxt);
				if (validUrlMatcher.matches()) {
					text = httpMatcher.replaceAll(RELATIVE_URL_TEMPLATE);

				} else {
					Logger.debug("[Linkifier] Invalid URL: '%s", httpTxt);
				}
			}

			// find file links with file://
			text = fileLinks.matcher(text).replaceAll(RELATIVE_URL_TEMPLATE);

			// find links without leading // and starting with www
			final Matcher wwwMatcher = wwwLinks.matcher(text);

			while (wwwMatcher.find()) {
				final String wwwTxt = wwwMatcher.group(2);
				// protocol needed to verify url
				if (urlValidator.isValid(String.format("http://%s", wwwTxt))) {
					text = wwwMatcher.replaceAll(URL_TEMPLATE);
				} else {
					Logger.debug("[Linkifier] Invalid URL: '%s", wwwTxt);
				}
			}

			// find user references (@first-lastname)
			for (String slug : findUserSlugs(text)) {
				User user = User.find("slug = ?", slug.substring(1)).first();

				if (user != null) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("id", user.id);
					params.put("slug", user.slug);

					text = text.replaceAll(slug,
							"<a href=\"" + Router.reverse("Users.wall", params).url + "\">" + user.getFullName()
									+ "</a>");
				}
			}

			// find email addresses - ?: prevents matching group creation
			text = mailLinks.matcher(text).replaceAll(MAILTO_TEMPLATE);

			// nl2br
			text = JavaExtensions.nl2br(new RawData(text)).toString();
		}

		return text;
	}

	public static List<User> getReferencedUsers(String text) {
		List<User> users = new ArrayList<User>();

		for (String slug : findUserSlugs(text)) {
			User user = User.find("slug = ?", slug.substring(1)).first();
			if (user != null) {
				users.add(user);
			}
		}

		return users;
	}

	private static List<String> findUserSlugs(String text) {
		List<String> slugs = new ArrayList<String>();

		Pattern userReferencePattern = Pattern.compile("(@[a-zA-Z0-9-_]+)");
		Matcher userReferenceMatcher = userReferencePattern.matcher(text);
		while (userReferenceMatcher.find()) {
			slugs.add(userReferenceMatcher.group(1));
		}

		return slugs;
	}
}

package utils;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Ordering;
import com.google.ical.compat.jodatime.DateTimeIteratorFactory;

import conf.ApplicationSettings;
import libs.DateI18N;
import models.Settings;
import models.User;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.WeekDay;
import net.fortuna.ical4j.model.WeekDayList;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.TzId;
import play.data.binding.types.DateBinder;
import play.i18n.Messages;

public class DateUtils {
	private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
	}

	/**
	 * Adds time in the format HH:mm to a date. Will set time to 0:00 if time is
	 * null. Also sets the correct user time zone.
	 * 
	 * @param date
	 *            Can be null, and if will return null
	 * @param time
	 *            Can be null
	 */
	public static DateTime timeToZoneDate(User user, DateTime date, String time) {
		if (date == null) {
			return null;
		}

		if (!StringUtils.isEmpty(time) && time.matches("^(\\d{2})(:)(\\d{2})$")) {
			// consider time zone for specific times
			date = date.withZoneRetainFields(user.getDateTimeZone());
			date = date.withHourOfDay(Integer.parseInt(time.substring(0, 2)));
			date = date.withMinuteOfHour(Integer.parseInt(time.substring(3, 5)));
		} else {
			// ignore time zone for fulltime
			date = date.withZoneRetainFields(DateTimeZone.UTC);
			date = date.withHourOfDay(0);
			date = date.withMinuteOfHour(0);
		}
		return date;
	}

	/**
	 * Returns a localized formatted date string
	 * 
	 * @param date
	 * @return
	 */
	public static String getFormattedString(final Date date) {
		final DateTimeFormatter format = DateTimeFormat.forPattern(DateI18N.getNormalFormat());
		return format.print(new DateTime(date));
	}

	/**
	 * Checks if the given String can be parsed into a iCalendar RRULE.
	 * Currently uses iCal4j as iCalendar parser.
	 * 
	 * @param recurrenceRule
	 *            The potential recurrence rule.
	 * @return true, if the String could be parsed as RRULE.
	 */
	public static boolean isValidRecurrenceRule(String recurrenceRule) {
		try {
			new Recur(recurrenceRule);
			return true;
		} catch (Exception ignore) {
			return false;
		}
	}

	/**
	 * Converts a iCalendar recurrence rule to a list of Periods. The start
	 * instance is <strong>not include</strong> in the result. Currently uses
	 * <a href="https://code.google.com/p/google-rfc-2445/">google-rfc-2445</a>
	 * as parser/helper.
	 * 
	 * @param recurrenceRule
	 *            The iCalendar RRULE recurrence rule.
	 * @param startDateTime
	 *            The reference start date.
	 * @param endDateTime
	 *            The reference end date.
	 * @param timeZone
	 *            The timezone that should be used for the calculation.
	 *            Important to consider daylight savings time.
	 * @param interval
	 *            The interval/period for which the recurrence set should be
	 *            calculated.
	 * @return List of intervals that represent the recurrence set for the given
	 *         rule and dates.
	 */
	public static List<Interval> getRecurrenceSet(String recurrenceRule, DateTime startDateTime, DateTime endDateTime,
			DateTimeZone timeZone, Interval interval) {
		if (StringUtils.isBlank(recurrenceRule)) {
			throw new IllegalArgumentException("recurrenceRule must not be blank");
		}
		if (startDateTime == null || endDateTime == null) {
			throw new IllegalArgumentException("startDateTime and endDateTime must not be null");
		}
		if (interval == null) {
			throw new IllegalArgumentException("interval must not be null");
		}

		try {
			List<Interval> result = new ArrayList<Interval>();
			// NOTE google-rfc-2445 needs the RRULE prefix
			recurrenceRule = String.format("RRULE:%s", recurrenceRule);
			for (DateTime recurringInstanceDateTime : DateTimeIteratorFactory.createDateTimeIterable(recurrenceRule,
					startDateTime, timeZone, true)) {
				// don't include the start instance
				if (isSameDay(recurringInstanceDateTime, startDateTime)) {
					continue;
				}

				// check if we are in the interval
				if (recurringInstanceDateTime.isBefore(interval.getStart())) {
					continue;
				}
				if (recurringInstanceDateTime.equals(interval.getEnd())
						|| recurringInstanceDateTime.isAfter(interval.getEnd())) {
					break;
				}

				result.add(new Interval(recurringInstanceDateTime,
						recurringInstanceDateTime.plus(endDateTime.minus(startDateTime.getMillis()).getMillis())));
			}

			return result;
		} catch (ParseException ignore) {
		}

		return Collections.EMPTY_LIST;
	}

	private static boolean isSameDay(DateTime dateTime, DateTime otherDateTime) {
		return dateTime != null && otherDateTime != null && dateTime.getDayOfYear() == otherDateTime.getDayOfYear()
				&& dateTime.getYear() == otherDateTime.getYear();
	}

	private static net.fortuna.ical4j.model.DateTime toICal4JDateTime(DateTime periodStart) throws ParseException {
		return new net.fortuna.ical4j.model.DateTime(toIsoFormattedString(periodStart));
	}

	private static String toIsoFormattedString(DateTime startDate) {
		DateTimeFormatter isoDateTimeFormatter = ISODateTimeFormat.basicDateTime();
		return startDate.toString(isoDateTimeFormatter);
	}

	private static net.fortuna.ical4j.model.TimeZone getTimeZone(DateTime dateTime) {
		VTimeZone vTimeZone = new VTimeZone();
		vTimeZone.getProperties().add(new TzId(dateTime.getZone().getID()));
		return new TimeZone(vTimeZone);
	}

	public static DateTimeZone getDefaultDateTimeZone() {
		final String timezone = Settings.findApplicationSettings().getString(ApplicationSettings.DEFAULT_TIMEZONE);
		if (!StringUtils.isEmpty(timezone)) {
			try {
				return DateTimeZone.forID(timezone);
			} catch (IllegalArgumentException ex) {
				LOG.error(ex.getLocalizedMessage(), ex);
			}
		}
		return DateTimeZone.UTC;
	}

	private static DateTime toJodaDateTime(net.fortuna.ical4j.model.DateTime dateTime) {
		return new DateTime(dateTime.getTime(), toDateTimeZone(dateTime.getTimeZone()));
	}

	private static DateTimeZone toDateTimeZone(net.fortuna.ical4j.model.TimeZone timeZone) {
		return DateTimeZone.forTimeZone(timeZone);
	}

	public static String getDescriptiveWeekdayList(WeekDayList dayList) {
		if (dayList.isEmpty()) {
			return "";
		}

		// check whether the list includes only working days/weekdays
		boolean onWeekdays = dayList.size() == 5;
		for (Object o : dayList) {
			WeekDay weekday = ((WeekDay) o);
			if (weekday.getDay().equals(WeekDay.SA) || weekday.getDay().equals(WeekDay.SU)) {
				onWeekdays = false;
				break;
			}
		}
		if (onWeekdays) {
			return String.format("%s", Messages.get("event.series.rrule.weekdays.everyWeekday"));
		}

		// sort list
		// TODO different order depending on user's locale?
		Collections.sort(dayList, Ordering.explicit(
				Arrays.asList(WeekDay.MO, WeekDay.TU, WeekDay.WE, WeekDay.TH, WeekDay.FR, WeekDay.SA, WeekDay.SU)));

		return String.format("%s", Joiner.on(", ").join(Collections2.transform(dayList, new Function<Object, String>() {
			@Override
			public String apply(Object o) {
				WeekDay weekday = ((WeekDay) o);
				return Messages.get("event.series.rrule.weekdays." + weekday.getDay());
			}
		})));
	}

	public static enum RecurrenceRuleFrequency {
		DAILY, WEEKLY, MONTHLY, YEARLY
	}

	public static RecurrenceRuleFrequency getRecurrenceFrequency(String recurrenceRule) {
		try {
			return getRecurrenceFrequency(new Recur(recurrenceRule));
		} catch (ParseException e) {
			return null;
		}
	}

	public static RecurrenceRuleFrequency getRecurrenceFrequency(Recur recur) {
		return RecurrenceRuleFrequency.valueOf(recur.getFrequency());
	}

	public static DateTime getDateBorder(final Date date, final DateTimeZone zone) {
		return getDateBorder(new DateTime(date), zone);
	}

	public static DateTime getDateBorder(DateTime dateTime, final DateTimeZone zone) {
		return dateTime.withZoneRetainFields(zone).withMillisOfDay(0);
	}

	public static DateTime getTodayBorder(final DateTimeZone zone) {
		return getDateBorder(new DateTime(), zone);
	}

	/**
	 * Compares two DateTime instances and returns true if they are representing
	 * a time within the same month and the same year. If one of the passed
	 * parameters is null, false is returned.
	 */
	public static boolean isSameMonth(DateTime date, DateTime other) {
		if (date == null || other == null) {
			return false;
		}
		return (date.year().equals(other.year()) && date.monthOfYear().equals(other.monthOfYear()));
	}

	/**
	 * Compares two DateTime instances and returns true if they are representing
	 * a time within the same year. If one of the passed parameters is null,
	 * false is returned.
	 */
	public static boolean isSameYear(DateTime date, DateTime other) {
		if (date == null || other == null) {
			return false;
		}
		return (date.year().equals(other.year()));
	}

	/**
	 * Returns the localized long name of the month passed by the parameter
	 * "month". Note that this method expects the month index to be 1 based - so
	 * January would be 1 not 0. The language to use is read from the passed
	 * user object. If this fails, the systems default locale is used.
	 * 
	 * For implementation details see: http://stackoverflow.com/a/1038580
	 */
	public static String getMonthDisplayName(Integer month, User user) {
		Locale locale = (user.language != null) ? user.language : Locale.getDefault();
		return new DateFormatSymbols(locale).getMonths()[month - 1];
	}

	public static String getISO8601Date(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateBinder.ISO8601);
		return sdf.format(date);
	}
}

package utils;

import play.data.validation.Error;
import play.data.validation.Validation.ValidationResult;

public class ValidationHelper {
	public static ValidationResult makeValidationResultWithError(String key, String message, String[] vars) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.ok = false;
		validationResult.error = new Error(key, message, vars);
		return validationResult;
	}
}

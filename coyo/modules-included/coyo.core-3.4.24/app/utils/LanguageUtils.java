package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import models.Settings;

import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;

import play.Play;
import play.i18n.Lang;
import util.Util;
import conf.ApplicationSettings;

/**
 * Language Utilities
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class LanguageUtils {

	private LanguageUtils() {
	}

	/**
	 * Get all application locales.
	 * 
	 * @return
	 */
	public static List<Locale> getApplicationLocales() {
		List<Locale> locales = new ArrayList<Locale>();
		for (String lang : Util.parseList(Play.configuration.getProperty("application.langs"), ",")) {
			locales.add(LocaleUtils.toLocale(lang));
		}
		return locales;
	}

	/**
	 * Check availability of given language
	 * 
	 * @param lang
	 * @return
	 */
	public static boolean isAvailable(String lang) {
		return !StringUtils.isEmpty(lang) && Play.langs.contains(lang);
	}

	/**
	 * Get the default locale.
	 * 
	 * @return
	 */
	public static Locale getDefaultLocale() {
		String defaultLanguage = Settings.findApplicationSettings().getString(ApplicationSettings.DEFAULT_LANGUAGE);
		if (isAvailable(defaultLanguage)) {
			// set by admin
			return new Locale(defaultLanguage);
		}
		return new Locale(Play.langs.get(0));

	}

	/**
	 * Set the given language.
	 * 
	 * @param lang
	 */
	public static void set(final String lang) {
		String set = lang;
		if (!isAvailable(set)) {
			set = "" + getDetectedLocale();
			if (!isAvailable(set)) {
				set = "" + getDefaultLocale();
			}
		}
		Lang.set(set);
	}

	/**
	 * Detect browser language.
	 * 
	 * @return
	 */
	public static Locale getDetectedLocale() {
		return Lang.getLocale();
	}
}

package utils;

import play.mvc.Http.Request;

/**
 * Allows the caching of objects in the request scope, if a request is
 * available.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class RequestCache {

	private RequestCache() {
	}

	public static Object get(String key) {
		if (Request.current() != null) {
			return Request.current().args.get(key);
		}
		return null;
	}

	public static void put(String key, Object object) {
		if (Request.current() != null) {
			Request.current().args.put(key, object);
		}
	}
}

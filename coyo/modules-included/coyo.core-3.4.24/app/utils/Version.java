package utils;

import org.apache.commons.lang.StringUtils;

/**
 * This is a helper class to validate and compare Coyo versions. For comparing this class utilizes
 * {@link ComparableVersion}.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
public class Version implements Comparable<Version> {
	private String versionString;

	public Version(String versionString) {
		this.versionString = versionString;
	}

	/**
	 * Returns true if this version is valid. This is the case if the version is not null nor empty, if it has at least
	 * two digits and if both of those digits are of type integer.
	 */
	public boolean isValid() {
		if (versionString == null || StringUtils.isEmpty(versionString)) {
			return false;
		}

		String[] token = versionString.split("\\.");
		if (token.length < 2) {
			return false;
		}

		try {
			Integer.valueOf(token[0]);
			Integer.valueOf(token[1]);
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return versionString;
	}

	@Override
	public int compareTo(Version other) {
		return new ComparableVersion(versionString).compareTo(new ComparableVersion(other.versionString));
	}

	/**
	 * Determines the schema version from the passed Coyo product version. The schema version basically consists of the
	 * first two digits of the version number. E.g. for product version 3.3.1 the schema version would be 3.3.
	 */
	public static Version getSchemaVersionFromProductVersion(Version version) {
		String[] token = version.toString().split("\\.");
		if (token.length >= 2) {
			String schemaVersion = token[0] + "." + token[1];
			return new Version(schemaVersion);
		}
		return null;
	}
}
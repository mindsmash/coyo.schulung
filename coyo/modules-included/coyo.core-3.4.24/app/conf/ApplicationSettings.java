package conf;

import java.util.ArrayList;
import java.util.List;

import models.Settings;
import play.i18n.Messages;

/**
 * Holds static application settings definitions.
 * 
 * @author Jan Marquardt
 */
public class ApplicationSettings {

	public static final String APPLICATION_NAME = "applicationName";

	public static final String DEFAULT_TIMEZONE = "defaultTimezone";
	public static final String DEFAULT_LANGUAGE = "defaultLanguage";

	public static final String PUSH = "push";
	
	public static final String ALLOW_ACCOUNT_DELETION = "allowAccountDeletion";
	public static final String AUTOACTIVATION = "autoactivation";
	public static final String REGISTRATION = "registration";
	public static final String REGISTRATION_DOMAINS = "registrationDomains";
	public static final String AUTH_OPENID = "authOpenID";
	public static final String AUTH_GOOGLE = "authGoogle";
	public static final String AUTH_TWITTER = "authTwitter";
	public static final String AUTH_TWITTER_KEY = "authTwitterKey";
	public static final String AUTH_TWITTER_SECRET = "authTwitterSecret";
	public static final String AUTH_FACEBOOK = "authFacebook";
	public static final String AUTH_FACEBOOK_KEY = "authFacebookKey";
	public static final String AUTH_FACEBOOK_SECRET = "authFacebookSecret";

	public static final String AUTOFOLLOW = "autofollow";
	public static final String WORKSPACES = "workspaces";
	public static final String MESSAGING = "messaging";
	public static final String MESSAGING_CHAT = "messagingChat";
	public static final String MESSAGING_WEBRTC = "messagingWebRTC";
	public static final String DOWNLOADS = "downloads";
	public static final String MOBILE_LINK = "mobileLink";
	public static final String EMBEDDABLES = "embeddables";
	public static final String TRACKING_CODE = "trackingCode";
	public static final String REPORTED_POST_MAIL = "reportedPostMail";
	public static final String STATISTICS = "statistics";

	public static final String HELP_HTML = "helpHtml";
	public static final String SUPPORT_CENTER_URL = "supportCenterUrl";

	// define application settings properties
	public static List<SettingsGroup> groups = new ArrayList<ApplicationSettings.SettingsGroup>();
	static {
		SettingsGroup general = new SettingsGroup("administration.settings.group.general");
		SettingsGroup workspaces = new SettingsGroup("administration.settings.group.workspaces");
		SettingsGroup messaging = new SettingsGroup("administration.settings.group.messaging");
		SettingsGroup authreg = new SettingsGroup("administration.settings.group.authreg");

		general.addProperty(APPLICATION_NAME, "input");
		general.addProperty(DEFAULT_TIMEZONE, "custom");
		general.addProperty(DEFAULT_LANGUAGE, "custom");
		general.addProperty(AUTOFOLLOW, "boolean");
		general.addProperty(EMBEDDABLES, "boolean");
		general.addProperty(DOWNLOADS, "boolean");
		general.addProperty(MOBILE_LINK, "boolean");
		general.addProperty(PUSH, "boolean");
		general.addProperty(STATISTICS, "boolean");
		general.addProperty(HELP_HTML, "textarea");
		general.addProperty(SUPPORT_CENTER_URL, "input");
		general.addProperty(TRACKING_CODE, "textarea");
		general.addProperty(REPORTED_POST_MAIL, "input");

		workspaces.addProperty(WORKSPACES, "boolean");

		messaging.addProperty(MESSAGING, "boolean");
		messaging.addProperty(MESSAGING_CHAT, "boolean");

		messaging.addProperty(MESSAGING_WEBRTC, "boolean");

		authreg.addProperty(REGISTRATION, "boolean");
		authreg.addProperty(AUTOACTIVATION, "boolean");
		authreg.addProperty(REGISTRATION_DOMAINS, "input");
		authreg.addProperty(ALLOW_ACCOUNT_DELETION, "boolean");
		authreg.addProperty("", "hr");
		authreg.addProperty(AUTH_OPENID, "boolean");
		authreg.addProperty(AUTH_GOOGLE, "boolean");
		authreg.addProperty(AUTH_TWITTER, "boolean");
		authreg.addProperty(AUTH_TWITTER_KEY, "input");
		authreg.addProperty(AUTH_TWITTER_SECRET, "input");
		authreg.addProperty(AUTH_FACEBOOK, "boolean");
		authreg.addProperty(AUTH_FACEBOOK_KEY, "input");
		authreg.addProperty(AUTH_FACEBOOK_SECRET, "input");

		groups.add(general);
		groups.add(messaging);
		groups.add(workspaces);
		groups.add(authreg);
	}

	private ApplicationSettings() {
		// hide
	}
	
	/**
	 * Applies default settings (e.g. after installation)
	 */
	public static void applyDefaultSettings(Settings settings) {
		settings.setProperty(ApplicationSettings.WORKSPACES, "true");
		settings.setProperty(ApplicationSettings.DOWNLOADS, "true");
		settings.setProperty(ApplicationSettings.MOBILE_LINK, "true");
		settings.setProperty(ApplicationSettings.MESSAGING, "true");
		settings.setProperty(ApplicationSettings.MESSAGING_CHAT, "true");
		settings.setProperty(ApplicationSettings.MESSAGING_WEBRTC, "true");
		settings.setProperty(ApplicationSettings.STATISTICS, "true");
		settings.setProperty(ApplicationSettings.PUSH, "true");
	}

	public static class SettingsGroup {
		public String name;
		public List<SettingsProperty> properties = new ArrayList<ApplicationSettings.SettingsProperty>();

		public SettingsGroup(String name) {
			this.name = name;
		}

		public String getLabel() {
			return Messages.get(name);
		}

		public void addProperty(String key, String type) {
			this.properties.add(new SettingsProperty(key, type));
		}
	}

	public static class SettingsProperty {
		public String key;
		public String type;

		public SettingsProperty(String key, String type) {
			this.key = key;
			this.type = type;
		}

		public String getLabel() {
			return Messages.get("administration.settings.key." + key);
		}

		public String getHelp() {
			return Messages.get("administration.settings.key." + key + ".help");
		}
	}
}

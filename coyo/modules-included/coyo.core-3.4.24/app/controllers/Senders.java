package controllers;

import java.util.HashMap;

import models.Sender;
import controllers.external.annotations.AllowExternals;

@AllowExternals
public class Senders extends WebBaseController {

	public static void show(Long id, Long appId) {
		Sender sender = Sender.findById(id);
		redirectToSender(sender, appId, new HashMap<String, Object>(params.allSimple()));
	}
}

package controllers;

import com.google.gson.GsonBuilder;
import controllers.api.OAuth2;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.UserDeviceSerializer;
import models.User;
import models.UserDevice;
import play.db.jpa.Transactional;
import session.UserLoader;

/**
 * This class provides access to parts of the coyo OAuth 2.0 functionality using a valid user session and can
 * therefore be used by webview components of mobile devices.
 */
public class OAuthWeb extends WebBaseController {

	/**
	 * Used to acquire a set of OAuth 2.0 tokens using a valid coyo session cookie. A device name that has to be unique
	 * for the current user must be provided.
	 *
	 *
	 * @param deviceName the name of the OAuth 2.0 device to be registered
	 * @param description the description text of the device
	 */
	@Transactional
	public static void getOAuthTokens(final String deviceName, final String description) {
		final User user = UserLoader.getConnectedUser();
		notFoundIfNull(user);

		final UserDevice userDevice = OAuth2.createUserDevice(user, deviceName, description);

		renderJSON(createBuilder().create().toJson(userDevice));
	}

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapter(UserDevice.class, new UserDeviceSerializer())
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}
}

package controllers.app;

import acl.AppsPermission;
import acl.Permission;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import controllers.Cache;
import controllers.FilesBaseController;
import controllers.Security;
import controllers.WebBaseController;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import jobs.Zip;
import json.BoxFileSerializer;
import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.BoxFile;
import models.User;
import models.app.FilesApp;
import models.app.FilesApp.FilesAppException;
import models.app.FilesAppDao;
import models.app.FilesAppPublicLink;
import models.app.LocalFilesApp;
import models.app.files.File;
import models.app.files.FileVersion;
import models.notification.FileNotification;
import models.wall.post.ShareFilePost;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.data.Upload;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.F.Promise;
import play.mvc.Catch;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.mvc.Scope;
import play.mvc.Scope.Session;
import play.mvc.Util;
import security.Secure;
import session.UserLoader;

import javax.persistence.TypedQuery;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllowExternals
public class Files extends WebBaseController {

	@Util
	public static void show(FilesApp app, File file) {
		if (file == null) {
			file = app.getRoot();
		}

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("file", file.getUid());
		args.put("app", app.id);
		redirectToSender(app.sender, app.id, args);
	}

	@Util
	public static String getBrowseUrl(FilesApp app, File file) {
		if (file == null) {
			file = app.getRoot();
		}

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", app.sender.id);
		args.put("appId", app.id);
		args.put("file", file.getUid());
		return Router.reverse("Senders.show", args).url;
	}

	public static void goToFile(Long appId, String fileUid) {
		FilesApp app = FilesApp.findById(appId);
		notFoundIfNull(app);

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", app.sender.id);
		args.put("appId", app.id);
		args.put("file", fileUid);

		redirectToSender(app.sender, appId, args);
	}

	// ajax
	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void browse(Long id, String uid, String view) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);

		File file;
		if (StringUtils.isEmpty(uid)) {
			file = app.getRoot();
		} else {
			file = app.getFile(uid);
			if (!file.isFolder()) {
				file = file.getParent();
			}
		}

		renderArgs.put("file", file);

		// determine view
		String sessionKey = "files-view-" + app.id;
		if (!StringUtils.isEmpty(view) && !view.equals("undefined") && !view.equals("null")) {
			Scope.Session.current().put(sessionKey, view); // set new
		}
		if (!Session.current().contains(sessionKey)) {
			Scope.Session.current().put(sessionKey, "table"); // default
		}
		renderArgs.put("view", Scope.Session.current().get(sessionKey)); // read

		render(app);
	}

	// ajax
	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void navigation(Long id) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		render(app);
	}

	// ajax
	@Secure(permissions = AppsPermission.ACCESS_FILE, params = { "id", "uid" })
	public static void preview(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);
		render(app, file);
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void createFolder(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);
		render(app, file);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void doCreateFolder(Long id, String uid, String name) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!StringUtils.isEmpty(name)) {
			try {
				app.createFolder(UserLoader.getConnectedUser(), file, name);
				flash.success(Messages.get("files.folder.save.success"));
			} catch (FilesAppException e) {
				Logger.debug(e, "[app.Files] Could not create folder [%s] in app [%s]", name, app);
				flash.error(e.displayMessage);
				error(e.displayMessage);
			}
		}

		show(app, file);
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void upload(Long id, String uid, Long fileID) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		render(app, file, fileID);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void doUpload(Long id, String uid, Long fileID, Upload leaveThisHereToForceUploadBinding)
			throws IOException {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		// retrieve upload
		List<Upload> uploads = (List<Upload>) request.args.get("__UPLOADS");
		if (uploads != null && uploads.size() > 0) {
			for (Upload upload : uploads) {
				if (upload != null) {
					if(upload.getSize() <= 0) {
						throw new FilesAppException(Messages.get("files.error.empty"), "The file is empty");
					}
					if (fileID != null) {
						final BoxFile boxFile = BoxFile.findById(fileID);
						if (boxFile != null) {
							boxFile.addVersion(upload.asStream(), UserLoader.getConnectedUser(), upload.getContentType(),
									upload.getSize());
							boxFile.save();
							FileNotification.raise(UserLoader.getConnectedUser(), app, boxFile, boxFile.getCurrentVersion());
						}
					} else {
						File newFile = app.createFile(UserLoader.getConnectedUser(), file, upload.getFileName(), upload.asStream(),
								upload.getContentType(), upload.getSize());
						FileNotification.raise(UserLoader.getConnectedUser(), app, newFile, newFile.getCurrentVersion());
					}
				}
			}
		}

		show(app, file);
	}

	// ajax
	@Secure(permissions = AppsPermission.ACCESS_FILE, params = { "id", "uid" })
	public static void versions(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);
		render(app, file);
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void revert(Long id, String uid, int version) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		FileVersion fv = file.getVersion(version);
		if (fv != null) {
			file.addVersion(UserLoader.getConnectedUser(), fv.get(), fv.getSize());
		}
		flash.success(Messages.get("files.revert.success"));
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void rename(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowRename()) {
			forbidden("not supported");
		}

		render(app, file);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void doRename(Long id, String uid, String name, String extension) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowRename()) {
			forbidden("not supported");
		}

		if (extension == null) {
			extension = "";
		}
		file.rename(name + extension);

		show(app, file.getParent());
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void move(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowMove()) {
			forbidden("not supported");
		}

		render(app, file);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void doMove(Long id, String uid, String destUid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowMove()) {
			forbidden("not supported");
		}

		File destination = app.getFile(destUid);
		notFoundIfNull(destination);

		if (file == null || destination == null || file.isChild(destination, true)) {
			flash.error(Messages.get("files.move.error"));
			show(app, file.getParent());
		}

		file.move(destination);
		flash.success(Messages.get("files.move.success"));
		show(app, destination);
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void copy(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowCopy()) {
			forbidden("not supported");
		}

		render(app, file);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void doCopy(Long id, String uid, String destUid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowCopy()) {
			forbidden("not supported");
		}

		File destination = app.getFile(destUid);
		notFoundIfNull(destination);

		if (file == null || destination == null || file.isChild(destination, true)) {
			flash.error(Messages.get("files.copy.error"));
			show(app, file.getParent());
		}

		file.copy(destination);
		flash.success(Messages.get("files.copy.success"));
		show(app, destination);
	}

	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void confirmDelete(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowDelete()) {
			forbidden("not supported");
		}

		render(app, file);
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void delete(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		if (!file.allowDelete()) {
			forbidden("not supported");
		}

		// delete related posts
		List<ShareFilePost> posts = ShareFilePost.find("app = ? AND fileUid = ?", app, file.getUid()).fetch();
		for (ShareFilePost post : posts) {
			post.delete();
		}

		// delete related notifications
		List<FileNotification> notifications = FileNotification.find("app = ? AND fileUid = ?", app, file.getUid())
				.fetch();
		for (FileNotification notification : notifications) {
			notification.delete();
		}

		// delete file and show success
		file.remove();
		flash.success(Messages.get("files.delete.success"));
	}

	// ajax
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void publicLink(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		FilesAppPublicLink publicLink = FilesAppPublicLink.find("app = ? AND fileUid = ?", app, uid).first();
		renderArgs.put("active", (publicLink != null));

		render(app, file, publicLink);
	}

	// ajax
	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	@CheckAuthenticity
	public static void togglePublicLink(Long id, String uid, boolean renew) {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		// try to find existing
		FilesAppPublicLink publicLink = FilesAppPublicLink.find("app = ? AND fileUid = ?", app, uid).first();
		if (publicLink != null) {
			if (renew) {
				publicLink.token = Codec.UUID();
				publicLink.save();
			} else {
				publicLink.delete();
				ok();
			}
		} else {
			publicLink = new FilesAppPublicLink();
			publicLink.app = app;
			publicLink.fileUid = uid;
			publicLink.token = Codec.UUID();
			publicLink.save();
		}

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("token", publicLink.token);
		ActionDefinition ad = Router.reverse("FilesAppExternal.download", args);
		ad.absolute();

		renderJSON("{\"url\" : " + new Gson().toJson(ad.url) + "}");
	}

	public static void download(Long id, String uid, Integer version) {
		/*
		 * We do not execute this check via annotation because this leads to a Play bug in combination with using Play
		 * promises.
		 */
		Security.checkAndCancel(AppsPermission.ACCESS_FILE, id, uid);

		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		File file = app.getFile(uid);
		notFoundIfNull(file);

		FileVersion fileVersion = null;
		if (version != null) {
			fileVersion = file.getVersion(version);
		}

		FilesBaseController.downloadFile(app, file, fileVersion, true);
	}

	@Catch(FilesAppException.class)
	public static void handleException(FilesAppException e) {
		Logger.error(e, "error in files app");
		// TODO : add user output
	}

	/*
	 * Do not remove! We need this so that Play enhances this class for continuations.
	 */
	@Util
	protected static void fake() {
		Promise<java.io.File> promise = new Zip(null, null, null).now();
		await(promise);
	}

	@Transactional
	@CheckAuthenticity
	public static void getLatestFiles() {
		List<BoxFile> latestFiles = FilesAppDao.getLatestFiles();
		Cache.cacheEntities(latestFiles, UserLoader.getConnectedUser().getPrivateCacheKey());
		renderJSON(createBuilder().create().toJson(latestFiles));
	}

	public static GsonBuilder createBuilder() {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(BoxFile.class, new BoxFileSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}
}

package controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.User;
import models.User.ActivityStreamSelect.DisplayType;
import models.wall.post.Post;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.db.jpa.Transactional;
import play.libs.Time;
import session.UserLoader;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import play.mvc.Scope;

/**
 * Handles all activity stream related actions.
 * 
 * @author Marko Ilic, Jan Marquardt
 */
@ShowExternals
public class ActivityStream extends WebBaseController {
	private static final String STATUS_INTERVAL = Play.configuration.getProperty("coyo.activitystream.status.interval", "30s");

	/**
	 * Default screen after login. Shows the activity stream.
	 */
	public static void index(DisplayType displayType, Boolean remember) {
		// store new value in session
		if (displayType != null && remember != null && remember) {
			Scope.Session.current().put("as-displayType", displayType.toString());
			// redirect for pretty URL
			index(null, null);
		}

		// try to load from session or use default
		if (displayType == null) {
			// from session
			String storedDisplayType = Scope.Session.current().get("as-displayType");
			if (!StringUtils.isEmpty(storedDisplayType)) {
				displayType = DisplayType.valueOf(storedDisplayType);
			}

			// default
			if (displayType == null) {
				displayType = DisplayType.sticky().get(0);
			}
		}

		render(displayType);
	}

	// ajax
	@Transactional
	public static void update() {
		// this is the click on the "load new posts" button
		renderArgs.put("posts", UserLoader.getConnectedUser().getNewActivityStream(true));
		render();
	}

	// ajax
	public static void status() {
		User user = UserLoader.getConnectedUser();

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("newPosts", user.getNewActivityStreamCount());
		renderJSON(data);
	}

	// ajax
	@Transactional
	public static void load(DisplayType displayType) {
		List<Post> posts = UserLoader.getConnectedUser().getActivityStream(displayType, EndlessHelper.getPage(),
				EndlessHelper.getLength());
		EndlessHelper.use(posts, true);

		renderArgs.put("wall", UserLoader.getConnectedUser().getDefaultWall());
		render();
	}

	public static long getStatusInterval() {
		return Time.parseDuration(STATUS_INTERVAL) * 1000L;
	}
}

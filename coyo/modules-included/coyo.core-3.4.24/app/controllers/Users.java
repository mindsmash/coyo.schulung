package controllers;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import acl.Permission;
import binding.ModelIdBinder;
import controllers.api.SSOTokenAPI;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import json.Dates;
import json.MinimalUserSerializer;
import json.UserChooserJsonSerializer;
import models.Sender;
import models.User;
import models.dao.UserDao;
import models.media.MediaPhoto;
import models.wall.post.Post;
import onlinestatus.OnlineStatusServiceFactory;
import play.data.Upload;
import play.data.binding.As;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.mvc.Util;
import security.Secure;
import session.UserLoader;
import storage.FlexibleBlob;
import theme.Theme;
import util.HttpUtil;
import utils.ImageUtils;
import utils.QRUtils;
import utils.hibernate.HibernateProxyTypeAdapter;
import validation.ImageUpload;

/**
 * Handles all actions for {@link User}s
 *
 * @author Jan Marquardt
 */
@AllowExternals
@ShowExternals
public class Users extends WebBaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(Users.class);

	/**
	 * This enum defines the filter field types, which can be queried
	 * dynamically from database
	 */
	public enum FilterFieldOption {
		NAME("name"), EMAIL("email"), JOBTITLE("jobTitle"), EXPERTISE("expertise"), DEPARTMENT("department"), OFFICE(
				"office"), COMPANY("company"), COUNTRY("country"), HOMETOWN("hometown"), INTERESTS("interests");

		private String fieldName;

		private FilterFieldOption(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getFieldName() {
			return fieldName;
		}
	}

	public static final char SPC = ' ';

	@Before(only = { "wall", "info", "media", "followers" })
	static void addSlug() {
		// if slug is null, redirect so same action with slug to add slug to URL
		if (StringUtils.isNumeric(params.get("id")) && params.get("slug") == null) {
			Long id = params.get("id", Long.class);
			User user = User.findById(id);
			if (user != null) {
				Map<String, Object> args = new HashMap<String, Object>();
				args.put("id", user.id);
				args.put("slug", user.getSlug());
				redirect(Router.reverse(request.action, args).url);
			}
		}
	}

	public static void wall(Long id, String slug) {
		User user = User.findById(id);
		notFoundIfNull(user);

		if (!Security.check(Permission.ACCESS_USER_WALL, id)) {
			info(id, slug);
		}

		render(user);
	}

	// ajax
	@Secure(permissions = Permission.ACCESS_USER_WALL, params = "id")
	@Transactional
	public static void loadWall(Long id) {
		User user = User.findById(id);
		notFoundIfNull(user);

		EndlessHelper.prepare(user.getWallPostsQuery(), true);

		/*
		 * Since the DB results are permission-checked during rendering, it may
		 * turn out that the current page is empty. However, there might still
		 * be some results on the next page so that's what we check for here. In
		 * such case, we render an empty item so that endless scrolling will
		 * continue loading the next page.
		 */
		List<Post> nextPage = user.getWallPostsQuery().fetch(EndlessHelper.getPage() + 1,
				EndlessHelper.getLength());
		if (!nextPage.isEmpty()) {
			renderArgs.put("hasMore", true);
		}

		renderArgs.put("wall", UserLoader.getConnectedUser().getDefaultWall());
		render();
	}

	@Secure(permissions = Permission.ACCESS_USER, redirect = "/", params = "id")
	public static void info(Long id, String slug) {
		User user = User.findById(id);
		notFoundIfNull(user);
		render(user);
	}

	@Secure(permissions = Permission.ACCESS_USER, redirect = "/", params = "id")
	public static void media(Long id, String slug) {
		User user = User.findById(id);
		notFoundIfNull(user);
		render(user);
	}

	public static void edit(Long id) {
		User user = null;
		if (id != null) {
			user = User.findById(id);
		} else {
			user = UserLoader.getConnectedUser();
		}

		notFoundIfNull(user);

		if (!Security.check(Permission.EDIT_USER_PROFILE, user.id)) {
			info(user.id, user.getSlug());
		}
		render(user);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(@As("profile") User user, @ImageUpload Upload avatar) throws Exception {
		if (!Security.check(Permission.EDIT_USER_PROFILE, user.id)) {
			info(user.id, user.getSlug());
		}

		Validation.valid("user", user);

		if (avatar != null && !ImageUtils.canHandleImage(avatar.asStream(), avatar.getContentType())) {
			Validation.addError("avatar", Messages.get("profile.avatar.error"));
		}

		if (Validation.hasErrors() || !user.validateAndSave()) {
			render("@edit", user);
		}

		if (avatar != null) {
			// save the full image first
			FlexibleBlob data = new FlexibleBlob();
			final BufferedImage rotatedImage = ImageUtils.rotateImage(avatar.asFile());

			if (rotatedImage != null) {
				try (final InputStream is = ImageUtils
						.createInputStreamFromBufferedImage(rotatedImage, avatar.getContentType())) {
					data.set(is, avatar.getContentType(), avatar.getSize());
				}
			} else {
				data.set(avatar.asStream(), avatar.getContentType(), avatar.getSize());
			}

			MediaPhoto.createPhoto(user.getProfilePictureCollection(true), user, avatar.getFileName(), data);

			ImageUtils.scaleToWidth(data.get(), avatar.getContentType(), user.avatar, Sender.AVATAR_WIDTH, true);
			ImageUtils.setThumb(data.get(), avatar.getContentType(), user.thumbnail, Sender.THUMB_WIDTH);
			user.save();
		}

		flash.success(Messages.get("profile.save.success"));
		info(user.id, user.getSlug());
	}

	@Secure(permissions = Permission.EDIT_USER_PROFILE, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void removeAvatar(Long id) {
		User user = User.findById(id);
		notFoundIfNull(user);

		if (user.thumbnail != null && user.thumbnail.exists()) {
			user.thumbnail.delete();
			user.thumbnail = null;
		}
		if (user.avatar != null && user.avatar.exists()) {
			user.avatar.delete();
			user.avatar = null;
		}
		user.save();
	}

	// ajax
	public static void listOnline() {
		List<User> onlineUsers = ModelIdBinder.bind(User.class,
				OnlineStatusServiceFactory.getOnlineStatusService().getOnlineUserIds());
		render(onlineUsers);
	}

	// ajax
	public static void chooser(boolean showExternals, boolean showMe, List<Long> selected, String term) {
		JpqlSelect select = UserLoader.getConnectedUser().getUserChooserSelect(showExternals, showMe, selected, term);
		EndlessHelper.prepare(User.find(select.toString(), select.getParams().toArray()), false);
		renderUsersChooserJson();
	}

	public static void mobileAuthQR(Integer size) throws Exception {
		if (size == null) {
			size = 400;
		}
		response.setContentTypeIfNotSet("image/png");
		ActionDefinition ad = Router.reverse("ActivityStream.index");
		ad.absolute();
		String url = HttpUtil.appendParam(ad.url, SSOTokenAPI.TOKEN_GET_PARAM, SSOTokenAPI.generateToken());
		QRUtils.generateQR(url, size, response.out);
	}

	@Util
	public static void renderUsersChooserJson() {
		List<User> users = (List<User>) renderArgs.get("data");
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(User.class, new UserChooserJsonSerializer());
		gsonBuilder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		Gson gson = gsonBuilder.create();
		renderJSON(gson.toJson(users));
	}

	@play.mvc.Util
	public static void renderUserListItems(JpqlSelect select) {
		renderUserListItems(User.find(select.toString(), select.getParams().toArray()));
	}

	@play.mvc.Util
	public static void renderUserListItems(JPAQuery query) {
		// currently, the user list items cannot be HTTP-cached because of their
		// online status
		EndlessHelper.prepare(query, false);
		render("Users/listItems.html");
	}

	@play.mvc.Util
	public static JpqlSelect getUserListSelect() {
		FilterFieldOption filterField;

		try {
			if (params.get("filterField") != null) {
				String filterValue = params.get("filterField").toUpperCase();
				filterField = FilterFieldOption.valueOf(filterValue);
			} else {
				filterField = null;
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("[Users] Unknown filter: " + e.getLocalizedMessage(), e);
			filterField = null;
		}
		String filterQuery = params.get("filterQuery");

		JpqlSelect select = new JpqlSelect();
		select.from("User u");
		select.orderBy("u.lastName,u.firstName");
		select.where("u.active = true");

		if ((filterField != null) && !StringUtils.isEmpty(filterQuery)) {
			if ("name".equals(filterField.fieldName)) {
				// all name terms must match
				List<String> terms = util.Util.parseList(filterQuery, " ");
				for (String string : terms) {
					String term = "%" + string.toLowerCase() + "%";
					select.andWhere("(LOWER(u.lastName) LIKE ? OR LOWER(u.firstName) LIKE ?)").params(term, term);
				}
			} else {
				select.andWhere("LOWER(" + filterField.fieldName + ") LIKE ?")
						.params("%" + filterQuery.toLowerCase() + "%");
			}
		}
		return select;
	}

	/**
	 * Removes a hashtag to the users list of followed hashtags
	 *
	 * @param tag
	 */
	@Transactional
	@CheckAuthenticity
	public static void followHashTag(String tag) {
		final User user = UserLoader.getConnectedUser();

		if (!Security.check(Permission.EDIT_USER_PROFILE, user.id)) {
			info(user.id, user.getSlug());
		}

		user.hashtags.add(tag);
		user.save();

		renderJSON(user.hashtags);
	}

	/**
	 * Adds a hashtag to the users list of followed hashtags
	 *
	 * @param tag
	 */
	@Transactional
	@CheckAuthenticity
	public static void unFollowHashTag(String tag) {
		final User user = UserLoader.getConnectedUser();

		if (!Security.check(Permission.EDIT_USER_PROFILE, user.id)) {
			info(user.id, user.getSlug());
		}

		user.hashtags.remove(tag);
		user.save();

		renderJSON(user.hashtags);
	}

	/**
	 * Returns the users list of followed hashtags
	 */
	@Transactional
	@CheckAuthenticity
	public static void getHashTags() {
		User user = UserLoader.getConnectedUser();
		if (user.hashtags != null) {
			renderJSON(user.hashtags);
		}

	}

	@Transactional
	@CheckAuthenticity
	public static void getNewestMembers() {
		response.cacheFor("15min");
		renderJSON(createBuilder().create().toJson(UserDao.getLatestUsers(5, 30)));
	}

	public static void forceDefaultTheme() {
		Theme.setActiveTheme("web");
		redirectToReferrer();
	}

	public static void forceMobileTheme() {
		Theme.setActiveTheme("mobile");
		redirectToReferrer();
	}

	public static GsonBuilder createBuilder() {
		final GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}
}
package controllers;

import models.app.PollApp;
import models.app.PollAppAnswer;
import play.db.jpa.Transactional;
import session.UserLoader;
import acl.AppsPermission;
import acl.Permission;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

@AllowExternals
public class Polls extends WebBaseController {

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void vote(Long id) {
		PollAppAnswer answer = PollAppAnswer.findById(id);
		notFoundIfNull(answer);

		// check allowed
		Security.checkAndCancel(AppsPermission.POLL_VOTE, answer.app);

		// check business constraints
		if (!answer.canVote(UserLoader.getConnectedUser())) {
			error();
		}

		// now vote
		answer.users.add(UserLoader.getConnectedUser());
		answer.save();
		ok();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void unvote(Long id) {
		PollAppAnswer answer = PollAppAnswer.findById(id);
		notFoundIfNull(answer);

		// check allowed
		Security.checkAndCancel(AppsPermission.POLL_VOTE, answer.app);

		// load possible answer
		answer.users.remove(UserLoader.getConnectedUser());
		answer.save();

		ok();
	}

	@Transactional
	@CheckAuthenticity
	public static void toggle(Long id) {
		PollApp app = PollApp.findById(id);
		notFoundIfNull(app);

		Security.checkAndCancel(Permission.EDIT_SENDER, app.sender.id);

		app.frozen = !app.frozen;
		app.save();
	}

	public static void render(Long id, int maxUsers) {
		PollApp app = PollApp.findById(id);
		notFoundIfNull(app);
		render(app, maxUsers);
	}
}

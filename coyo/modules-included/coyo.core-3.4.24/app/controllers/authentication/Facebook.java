package controllers.authentication;

import java.io.UnsupportedEncodingException;

import models.Settings;
import models.User;
import models.UserGroup;
import models.UserRole;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.i18n.Lang;
import play.i18n.Messages;
import play.libs.OAuth2;
import play.libs.OAuth2.Response;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;

import com.google.gson.JsonObject;

import conf.ApplicationSettings;
import controllers.Auth;
import controllers.Registration;

public class Facebook extends Controller {

	public static final String AUTH_SOURCE = "Facebook";
	public static final boolean AUTOCREATE = true;

	@Before
	public static void checkSettings() {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTH_FACEBOOK)) {
			redirect("/");
		}
		if (StringUtils.isEmpty(getAppKey()) || StringUtils.isEmpty(getAppSecret())) {
			Logger.warn("cannot load facebook auth module because no key or secret was specified");
			redirect("/");
		}
	}

	private static String getAppKey() {
		return Settings.findApplicationSettings().getString(ApplicationSettings.AUTH_FACEBOOK_KEY);
	}

	private static String getAppSecret() {
		return Settings.findApplicationSettings().getString(ApplicationSettings.AUTH_FACEBOOK_SECRET);
	}

	public static void launch() throws UnsupportedEncodingException {
		ActionDefinition ad = Router.reverse("authentication.Facebook.complete");
		ad.absolute();

		OAuth2 oauth = createOAuth();
		redirect(oauth.authorizationURL + "?client_id=" + oauth.clientid + "&scope=email&redirect_uri=" + ad.url);
	}

	public static void complete(String code) {
		if (OAuth2.isCodeResponse()) {

			// prepare OAuth
			OAuth2 oauth = createOAuth();
			Response response = oauth.retrieveAccessToken();

			if (response.error == null) {
				// get settings
				HttpResponse settingsResponse = WS.url("https://graph.facebook.com/me?access_token=%s",
						response.accessToken).get();

				if (settingsResponse.success()) {
					try {
						JsonObject data = settingsResponse.getJson().getAsJsonObject();

						String id = data.get("id").getAsString();

						// check if user exists
						User user = User.find("authUid = ? AND authSource = ?", id, AUTH_SOURCE).first();

						if (user == null && AUTOCREATE
								&& Settings.findApplicationSettings().getBoolean(ApplicationSettings.REGISTRATION)) {
							// register
							String email = data.get("email").getAsString();

							// check if email already exists
							if (Registration.emailExists(email)) {
								flash.keep();
								Auth.login();
							}

							user = new User();
							user.authUid = id;
							user.authSource = AUTH_SOURCE;
							user.password = "";
							user.language = Lang.getLocale();
							user.active = Settings.findApplicationSettings().getBoolean(
									ApplicationSettings.AUTOACTIVATION);
							user.role = UserRole.findDefaultRole();

							user.email = email;
							user.firstName = data.get("first_name").getAsString();
							user.lastName = data.get("last_name").getAsString();
							user.save();

							UserGroup defaultGroup = UserGroup.findDefaultGroup();
							if (defaultGroup != null) {
								defaultGroup.addUser(user);
							}

							// store facebook access token
							user.properties.put("facebookAccessToken", response.accessToken);
							user.save();

							if (user.active) {
								flash.success(Messages.get("registration.profile.success"));
								Auth.succeedAuthentication(user.email, false);
							}

							flash.success(Messages.get("registration.profile.activation"));
							Auth.login();
						} else if (user != null && user.isPersistent() && user.active) {
							// TODO: sync profile
							Auth.succeedAuthentication(user.email, true);
						}
					} catch (Exception e) {
						Logger.warn(e, "error executing facebook authentication");
					}
				}
			} else {
				Logger.warn("error connecting to facebook [%s: %s (%s)]", response.error.error,
						response.error.description, response.error.type);
			}
		}

		flash.error(Messages.get("auth.login.failed"));
		flash.keep();
		Auth.login();
	}

	private static OAuth2 createOAuth() {
		return new OAuth2("http://www.facebook.com/dialog/oauth", "https://graph.facebook.com/oauth/access_token",
				getAppKey(), getAppSecret());
	}
}

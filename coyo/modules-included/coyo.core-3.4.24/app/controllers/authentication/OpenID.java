package controllers.authentication;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import models.Settings;
import models.User;
import models.UserGroup;
import models.UserRole;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Lang;
import play.i18n.Messages;
import play.libs.Crypto;
import play.libs.OpenID.UserInfo;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import conf.ApplicationSettings;
import controllers.Auth;
import controllers.Registration;

public class OpenID extends Controller {

	public static final String AUTH_SOURCE = "OpenID";
	public static final boolean AUTOCREATE = true;

	@Before
	public static void checkSettings() {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTH_OPENID)
				&& !Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTH_GOOGLE)) {
			redirect("/");
		}
	}

	public static void form() {
		render();
	}

	public static void launch(String openid, boolean rememberMe) throws UnsupportedEncodingException {
		if (StringUtils.isEmpty(openid)) {
			redirect("/");
		}

		play.libs.OpenID id = play.libs.OpenID.id(openid);

		Map<String, Object> args = new HashMap<String, Object>();
		// args.put("username", username);
		// args.put("password", password);
		args.put("rememberMe", rememberMe);
		ActionDefinition ad = Router.reverse("authentication.OpenID.complete", args);
		ad.absolute();

		String baseUrl = Play.configuration.getProperty("application.baseUrl");
		if (!StringUtils.isEmpty(baseUrl)) {
			id.forRealm(baseUrl);
		}

		id.returnTo(ad.url);
		id.required("email");
		id.required("email", "http://axschema.org/contact/email");
		if (!id.verify()) {
			flash.error(Messages.get("auth.openid.invalid"));
			Auth.login();
		}
	}

	public static void complete(boolean rememberMe) {
		if (play.libs.OpenID.isAuthenticationResponse()) {
			// load user info
			UserInfo verifiedUser = play.libs.OpenID.getVerifiedID();
			if (verifiedUser != null) {
				User user = User.find("authUid = ? AND authSource = ?", verifiedUser.id, AUTH_SOURCE).first();

				// check if user exists
				if (user == null && AUTOCREATE
						&& Settings.findApplicationSettings().getBoolean(ApplicationSettings.REGISTRATION)) {
					renderArgs.put("token", Crypto.encryptAES(verifiedUser.id));
					String email = verifiedUser.extensions.get("email");

					// check if user already exists or existed before
					if (Registration.emailExists(email)) {
						Auth.login();
					}

					renderArgs.put("email", email);
					render();
				} else if (user != null && user.isPersistent() && user.active) {
					// TODO: sync profile

					// auth
					Auth.succeedAuthentication(user.email, rememberMe);
				}
			}
		}

		flash.error(Messages.get("auth.login.failed"));
		flash.keep();
		Auth.login();
	}

	public static void create(String token, @Email @Required String email, @Required String firstName,
			@Required String lastName) {
		String openId = Crypto.decryptAES(token);

		if (Validation.hasErrors() || Registration.emailExists(email)) {
			render("@complete", token, email, firstName, lastName);
		}

		User user = new User();
		user.authUid = openId;
		user.authSource = AUTH_SOURCE;
		user.password = "";
		user.language = Lang.getLocale();
		user.active = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOACTIVATION);
		user.role = UserRole.findDefaultRole();

		user.email = email;
		user.firstName = firstName;
		user.lastName = lastName;

		user.save();

		UserGroup defaultGroup = UserGroup.findDefaultGroup();
		if (defaultGroup != null) {
			defaultGroup.addUser(user);
		}

		if (user.active) {
			flash.success(Messages.get("registration.profile.success"));
			Auth.succeedAuthentication(user.email, false);
		}

		flash.success(Messages.get("registration.profile.activation"));
		Auth.login();
	}
}

package controllers.authentication;

import models.Settings;
import models.User;
import models.UserGroup;
import models.UserRole;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Lang;
import play.i18n.Messages;
import play.libs.Crypto;
import play.libs.OAuth;
import play.libs.OAuth.Response;
import play.libs.OAuth.ServiceInfo;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;

import com.google.gson.JsonObject;

import conf.ApplicationSettings;
import controllers.Auth;
import controllers.Registration;
import play.mvc.Scope;

public class Twitter extends Controller {

	public static final String AUTH_SOURCE = "Twitter";
	public static final boolean AUTOCREATE = true;

	@Before
	public static void checkSettings() {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTH_TWITTER)) {
			redirect("/");
		}

		if (StringUtils.isEmpty(getAppKey()) || StringUtils.isEmpty(getAppSecret())) {
			Logger.warn("cannot load twitter auth module because no key or secret was specified");
			redirect("/");
		}
	}

	private static String getAppKey() {
		return Settings.findApplicationSettings().getString(ApplicationSettings.AUTH_TWITTER_KEY);
	}

	private static String getAppSecret() {
		return Settings.findApplicationSettings().getString(ApplicationSettings.AUTH_TWITTER_SECRET);
	}

	public static void launch() {
		OAuth oauth = OAuth.service(getTwitterServiceInfo());

		ActionDefinition ad = Router.reverse("authentication.Twitter.complete");
		ad.absolute();

		Response response = oauth.retrieveRequestToken(ad.url);

		if (response.error != null) {
			Logger.warn(response.error.exception, "error connecting to twitter");
			flash.error(Messages.get("auth.login.failed"));
			flash.keep();
			Auth.login();
		}

		// store token and secret
		Scope.Session.current().put("twitterToken", response.token);
		Scope.Session.current().put("twitterSecret", response.secret);

		// let the user login at twitter
		redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + response.token);
	}

	public static void complete() {
		if (!OAuth.isVerifierResponse()) {
			redirect("/");
		}

		String twitterToken = Scope.Session.current().get("twitterToken");
		String twitterSecret = Scope.Session.current().get("twitterSecret");

		// get access tokens
		OAuth oauth = OAuth.service(getTwitterServiceInfo());
		Response response = oauth.retrieveAccessToken(twitterToken, twitterSecret);

		if (response.error == null) {
			// get settings
			HttpResponse settingsResponse = WS.url("https://api.twitter.com/1.1/account/verify_credentials.json")
					.oauth(getTwitterServiceInfo(), response.token, response.secret).get();
			if (settingsResponse.success()) {
				JsonObject data = settingsResponse.getJson().getAsJsonObject();
				String id = data.get("id").getAsString();

				// check if user exists
				User user = User.find("authUid = ? AND authSource = ?", id, AUTH_SOURCE).first();

				if (user == null && AUTOCREATE
						&& Settings.findApplicationSettings().getBoolean(ApplicationSettings.REGISTRATION)) {
					renderArgs.put("twitterId", Crypto.encryptAES(id));
					renderArgs.put("twitterToken", Crypto.encryptAES(response.token));
					renderArgs.put("twitterSecret", Crypto.encryptAES(response.secret));
					render();
				} else if (user != null && user.isPersistent() && user.active) {
					Auth.succeedAuthentication(user.email, true);
				}
			} else {
				Logger.warn(response.error.exception, "error getting twitter user data");
			}
		} else {
			Logger.warn(response.error.exception, "error connecting to twitter");
		}

		flash.error(Messages.get("auth.login.failed"));
		flash.keep();
		Auth.login();
	}

	public static void create(String twitterId, String twitterToken, String twitterSecret,
			@Email @Required String email, @Required String firstName, @Required String lastName) {
		String decryptedTwitterId = Crypto.decryptAES(twitterId);

		if (Validation.hasErrors() || Registration.emailExists(email)) {
			render("@complete", twitterId, twitterToken, twitterSecret, email, firstName, lastName);
		}

		User user = new User();
		user.authUid = decryptedTwitterId;
		user.authSource = AUTH_SOURCE;
		user.password = "";
		user.language = Lang.getLocale();
		user.active = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOACTIVATION);
		user.role = UserRole.findDefaultRole();

		user.email = email;
		user.firstName = firstName;
		user.lastName = lastName;
		user.save();

		UserGroup defaultGroup = UserGroup.findDefaultGroup();
		if (defaultGroup != null) {
			defaultGroup.addUser(user);
		}

		// store twitter token and secret
		user.properties.put("twitterToken", twitterToken);
		user.properties.put("twitterSecret", twitterSecret);
		user.save();

		if (user.active) {
			flash.success(Messages.get("registration.profile.success"));
			Auth.succeedAuthentication(user.email, false);
		}

		flash.success(Messages.get("registration.profile.activation"));
		Auth.login();
	}

	private static ServiceInfo getTwitterServiceInfo() {
		return new ServiceInfo("https://api.twitter.com/oauth/request_token",
				"https://api.twitter.com/oauth/access_token", "https://api.twitter.com/oauth/authorize", getAppKey(),
				getAppSecret());
	}
}

package controllers.administration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate your administration controller action to add it to the settings
 * navigation panel.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SettingsNav {

	String indexAction() default "index";
	
	/**
	 * Messages key
	 */
	String label();
}

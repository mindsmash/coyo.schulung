package controllers.administration;

import jobs.ResetFollowers;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;

@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Utils extends WebBaseController {
	
	public static void resetFollowers() {
		new ResetFollowers(true).now();
	}

}

package controllers.administration;

import acl.Permission;
import binding.JsonBody;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;
import models.HashtagBlackList;
import play.db.jpa.Transactional;
import security.Secure;

/**
 * Handles all actions for admins to manage the settings.
 *
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Hashtags extends WebBaseController {

	public static void index() {
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void getBlackList() {
		renderJSON(HashtagBlackList.getHashtags());
	}

	@Transactional
	@CheckAuthenticity
	public static void addToBlackList(@JsonBody final Hashtag hashtag) {
		renderJSON(HashtagBlackList.addHashtag(hashtag.getHashtag()));
	}

	@Transactional
	@CheckAuthenticity
	public static void removeFromBlackList(@JsonBody final Hashtag hashtag) {
		renderJSON(HashtagBlackList.removeHashtag(hashtag.getHashtag()));
	}

	private class Hashtag {
		public String getHashtag() {
			return hashtag;
		}

		public void setHashtag(String hashtag) {
			this.hashtag = hashtag;
		}

		private String hashtag;
	}
}

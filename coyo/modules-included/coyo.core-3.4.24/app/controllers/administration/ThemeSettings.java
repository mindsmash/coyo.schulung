package controllers.administration;

import java.awt.Color;

import java.io.IOException;

import java.util.Map;
import java.util.regex.Pattern;

import models.Application;
import models.Settings;
import play.Logger;
import play.Play;
import play.data.FileUpload;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Util;
import security.Secure;
import storage.FlexibleBlob;
import utils.ImageUtils;
import validation.checks.ColorCodeCheck;
import acl.Permission;
import assets.AssetsManager;
import conf.ApplicationSettings;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the theme settings.
 *
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_THEME, redirect = "/")
public class ThemeSettings extends WebBaseController {
	public static final Pattern imageMimeTypeP = Pattern.compile("(?:image/)([a-zA-Z0-9-_.]+)");
	public static final double LOGO_RATIO = 1.5;
	public static final int LOGO_WIDTH = 300;
	public static final double TOPBAR_LOGO_RATIO = 5;
	public static final int TOPBAR_LOGO_WIDTH = 200;
	public static final double FAVICON_RATIO = 1;
	public static final int FAVICON_WIDTH = 32;
	public static final String THEME_CSS = "themeCustomCss";

	public static void index() {
		renderArgs.put("themeSettings", Settings.findOrCreate("theme"));
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void save(Map<String, String> colors, FileUpload logo, FileUpload smallLogo, FileUpload favicon, String themeCustomCss) {

		Settings themeSettings = Settings.findOrCreate("theme");

		// load and validate color codes
		for (String key : colors.keySet()) {
			String color = colors.get(key);
			if (validation.required("colors[" + key + "]", color).ok && !ColorCodeCheck.check(color)) {
				validation.addError("colors[" + key + "]", ColorCodeCheck.MESSAGE);
			}
			themeSettings.setProperty(key, color);
		}
		if (validation.hasErrors()) {
			render("@index", themeSettings);
		}

		// determine brightness of main color and set theme style
		try {
			Color color = hex2Rgb(themeSettings.getString("themeBaseColor"));
			int brightness = ((color.getRed() * 299) + (color.getGreen() * 587) + (color.getBlue() * 114)) / 1000;
			if (brightness > 180) {
				themeSettings.setProperty("themeStyle", "light");
			} else {
				themeSettings.setProperty("themeStyle", "dark");
			}
		} catch (Exception e) {
			Logger.warn(e, "could not determine main color [%s] brightness", themeSettings.getString("themeBaseColor"));
			themeSettings.setProperty("themeStyle", "dark");
		}
		themeSettings.setProperty(THEME_CSS, themeCustomCss);
		themeSettings.save();

		// image uploads
		Application application = Application.get();

		try {
			if (logo != null) {
				if (application.logo == null || !application.logo.exists()) {
					application.logo = new FlexibleBlob();
				}
				handleImageUpload(logo, LOGO_RATIO, LOGO_WIDTH, application.logo);
			}

			if (smallLogo != null) {
				if (application.smallLogo == null || !application.smallLogo.exists()) {
					application.smallLogo = new FlexibleBlob();
				}
				handleImageUpload(smallLogo, TOPBAR_LOGO_RATIO, TOPBAR_LOGO_WIDTH, application.smallLogo);
			}

			if (favicon != null) {
				if (application.favicon == null || !application.favicon.exists()) {
					application.favicon = new FlexibleBlob();
				}
				handleImageUpload(favicon, FAVICON_RATIO, FAVICON_WIDTH, application.favicon);
			}

			application.save();
			AssetsManager.clearId(); // clear less and assets cache

		} catch (IOException ex) {
			flash.error(Messages.get("administration.theme.imageError"));
			index();
		}

		flash.success(Messages.get("administration.theme.save.success"));
		index();
	}

	/**
	 * Tests if an uploaded image is ok and if stores the image in a FlexibleBlob
	 * 
	 * @param icon
	 *            FileUpload
	 * @param desiredRatio
	 *            double
	 * @param desiredWidth
	 *            int
	 * @param blob
	 *            FlexibleBlob
	 * @throws IOException
	 */
	private static void handleImageUpload(FileUpload icon, double desiredRatio, int desiredWidth, FlexibleBlob blob)
			throws IOException {

		if (!ImageUtils.canHandleImage(icon.asStream(), icon.getContentType())) {
			flash.error(Messages.get("administration.theme.imageError"));
			index();
		}

		ImageUtils.scaleToWidth(icon.asStream(), icon.getContentType(), blob, desiredWidth, false);
	}

	@Transactional
	@CheckAuthenticity
	public static void reset() throws IOException {
		loadDefaultTheme();

		// delete logo uploads
		Application application = Application.get();

		if (application.logo != null && application.logo.exists()) {
			application.logo.delete();
		}
		if (application.smallLogo != null && application.smallLogo.exists()) {
			application.smallLogo.delete();
		}
		if (application.favicon != null && application.favicon.exists()) {
			application.favicon.delete();
		}

		// clear less cache
		AssetsManager.clearId();
	}

	@Util
	public static void loadDefaultTheme() {
		Settings themeSettings = Settings.findOrCreate("theme");
		themeSettings.setProperty("themeStyle", Play.configuration.getProperty("theme.style"));
		themeSettings.setProperty("themeBaseColor", Play.configuration.getProperty("theme.baseColor"));
		themeSettings.setProperty("themeSecondaryColor", Play.configuration.getProperty("theme.secondaryColor"));
		themeSettings.setProperty("themeTopbarTextColor", Play.configuration.getProperty("theme.topbarTextColor"));
		themeSettings.setProperty("themeTopbarHoverColor", Play.configuration.getProperty("theme.topbarHoverColor"));
		themeSettings.setProperty("themeTextColor", Play.configuration.getProperty("theme.textColor"));
		themeSettings.setProperty("themeLinkColor", Play.configuration.getProperty("theme.linkColor"));
		themeSettings
				.setProperty("themeSecondaryLinkColor", Play.configuration.getProperty("theme.secondaryLinkColor"));
		themeSettings.setProperty("themeButtonColor", Play.configuration.getProperty("theme.buttonColor"));
		themeSettings.setProperty("themeSecondaryButtonColor",
				Play.configuration.getProperty("theme.secondaryButtonColor"));
		themeSettings.setProperty("themeSmallLogoUrl", Play.configuration.getProperty("theme.smallLogoUrl"));
		themeSettings.setProperty("themeLogoUrl", Play.configuration.getProperty("theme.logoUrl"));
		themeSettings.setProperty(THEME_CSS, Play.configuration.getProperty("theme.css"));
		themeSettings.save();
	}

	@Util
	public static Settings getThemeSettings() {
		Settings themeSettings = Settings.findOrCreate("theme");
		if (null == themeSettings.getString("themeBaseColor")) {
			loadDefaultTheme();
		}
		return themeSettings;
	}

	@Util
	public static Color hex2Rgb(String hex) {
		return new Color(Integer.valueOf(hex.substring(1, 3), 16), Integer.valueOf(hex.substring(3, 5), 16),
				Integer.valueOf(hex.substring(5, 7), 16));
	}
}

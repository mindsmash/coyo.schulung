package controllers.administration;

import models.User;
import play.db.jpa.Transactional;
import security.Secure;
import session.UserLoader;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * 
 * Handles the action to toggle the superadmin mode.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class AdminMode extends WebBaseController {

	@Transactional
	@CheckAuthenticity
	public static void toggle(boolean active) {
		User user = UserLoader.getConnectedUser();
		user.superadminMode = active;
		user.save();
	}
}

package controllers.administration;

import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import plugins.PluginBase;
import plugins.PluginManager;
import security.Secure;
import util.Util;
import acl.Permission;
import apps.AppDescriptor;
import apps.AppManager;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the plugins.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Plugins extends WebBaseController {

	public static void index() {
		renderArgs.put("plugins", PluginManager.getPlugins());
		render();
	}

	public static void license(String key) {
		PluginBase plugin = PluginManager.getPlugin(key);
		notFoundIfNull(plugin);
		if (!plugin.isLicenseRequired()) {
			index();
		}
		render(plugin);
	}

	@Transactional
	@CheckAuthenticity
	public static void saveLicense(String key, String license) {
		PluginBase plugin = PluginManager.getPlugin(key);
		notFoundIfNull(plugin);
		if (!plugin.isLicenseRequired()) {
			index();
		}
		plugin.saveLicense(license);
		license(key);
	}
	
	public static void settings(String key) {
		PluginBase plugin = PluginManager.getPlugin(key);
		notFoundIfNull(plugin);
		render(plugin);
	}

	@Transactional
	@CheckAuthenticity
	public static void saveSettings(String key) {
		PluginBase plugin = PluginManager.getPlugin(key);
		notFoundIfNull(plugin);

		plugin.saveSettings(request);
		if (Validation.hasErrors()) {
			render("@settings", plugin);
		}

		flash.success(Messages.get("plugin.settings.save.success"));
		settings(key);
	}

	@Transactional
	@CheckAuthenticity
	public static void turn(String key) {
		PluginBase plugin = PluginManager.getPlugin(key);
		notFoundIfNull(plugin);

		if (PluginManager.isActive(plugin)) {
			PluginManager.deactivate(plugin);
		} else {
			PluginManager.activate(plugin);
		}
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void order(String order) {
		PluginManager.sort(Util.parseList(order, ","));
	}
}
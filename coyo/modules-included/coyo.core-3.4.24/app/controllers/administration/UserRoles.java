package controllers.administration;

import java.util.List;

import models.User;
import models.UserGroup;
import models.UserRole;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the user roles.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_USER, redirect = "/")
public class UserRoles extends WebBaseController {

	public static void index() {
		List<UserRole> roles = UserRole.findAll();
		render(roles);
	}

	public static void create() {
		UserRole role = new UserRole();
		render(role);
	}

	public static void edit(Long id) {
		UserRole role = UserRole.findById(id);
		notFoundIfNull(id);
		render(role);
	}

	public static void users(Long id) {
		UserRole role = UserRole.findById(id);
		notFoundIfNull(id);
		renderArgs.put("groups", UserGroup.find("ORDER BY name ASC").fetch());
		render(role);
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		UserRole role = UserRole.findById(id);
		notFoundIfNull(id);

		for (User user : role.users) {
			user.role = null;
			user.save();
		}

		role.delete();

		flash.success(Messages.get("user.role.delete.success"));
	}

	@Transactional
	@CheckAuthenticity
	public static void save(UserRole role, List<Long> users, List<Long> groups) {
		if (null == params.get("role.grants")) {
			role.grants.clear();
		}
		if (!role.validateAndSave()) {
			if (role.isPersistent()) {
				render("@edit", role);
			} else {
				render("@create", role);
			}
		}

		flash.success(Messages.get("user.role.save.success"));
		index();
	}

	@Transactional
	@CheckAuthenticity
	public static void saveUsers(UserRole role, List<Long> users, List<Long> groups) {
		role.updateUsers(users);
		role.updateGroups(groups);

		flash.success(Messages.get("user.role.save.success"));
		index();
	}
}

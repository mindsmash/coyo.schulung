package controllers.administration;

import play.db.jpa.Transactional;
import security.Secure;
import acl.Permission;
import apps.AppDescriptor;
import apps.AppManager;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the apps.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Apps extends WebBaseController {

	public static void index() {
		renderArgs.put("apps", AppManager.getApps());
		render();
	}

	public static void license(String key) {
		AppDescriptor app = AppManager.getApp(key);
		notFoundIfNull(app);
		if (!app.isLicenseRequired()) {
			index();
		}
		render(app);
	}

	@Transactional
	@CheckAuthenticity
	public static void saveLicense(String key, String license) {
		AppDescriptor app = AppManager.getApp(key);
		notFoundIfNull(app);
		if (!app.isLicenseRequired()) {
			index();
		}
		app.saveLicense(license);
		license(key);
	}

	@Transactional
	@CheckAuthenticity
	public static void turn(String key, String senderType, boolean active) {
		AppDescriptor ad = AppManager.getApp(key);
		notFoundIfNull(ad);

		AppManager.toggle(ad, senderType, active);
	}
}

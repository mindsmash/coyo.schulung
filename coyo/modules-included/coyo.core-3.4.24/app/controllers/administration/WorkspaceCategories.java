package controllers.administration;

import models.workspace.Workspace;
import models.workspace.WorkspaceCategory;
import play.cache.Cache;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the workspace categories.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_CATEGORIES, redirect = "/")
public class WorkspaceCategories extends WebBaseController {

	public static void index() {
		renderArgs.put("categories", WorkspaceCategory.findAllSorted());
		render();
	}

	public static void create() {
		WorkspaceCategory category = new WorkspaceCategory();
		render(category);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(@Valid WorkspaceCategory category) {
		if (Validation.hasErrors()) {
			render("@create", category);
		}

		category.save();
		Cache.delete(category.getCacheKey(UserLoader.getConnectedUser()));
		index();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void order(String order) {
		WorkspaceCategory.storeOrder(order);
	}

	public static void edit(Long id) {
		WorkspaceCategory category = WorkspaceCategory.findById(id);
		renderArgs.put("category", category);
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		WorkspaceCategory category = WorkspaceCategory.findById(id);

		// unassign all workspaces
		for (Workspace ws : category.workspaces) {
			ws.category = null;
			ws.save();
		}

		category.delete();
		flash.success(Messages.get("administration.workspaceCategories.delete.success"));
	}
}

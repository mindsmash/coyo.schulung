package controllers.administration;

import models.page.Page;
import models.page.PageCategory;
import play.cache.Cache;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the page categories.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_CATEGORIES, redirect = "/")
public class PageCategories extends WebBaseController {

	public static void index() {
		renderArgs.put("categories", PageCategory.findAllSorted());
		render();
	}

	public static void create() {
		PageCategory category = new PageCategory();
		render(category);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(@Valid PageCategory category) {
		if (Validation.hasErrors()) {
			render("@create", category);
		}

		category.save();
		Cache.delete(category.getCacheKey(UserLoader.getConnectedUser()));
		index();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void order(String order) {
		PageCategory.storeOrder(order);
	}

	public static void edit(Long id) {
		PageCategory category = PageCategory.findById(id);
		renderArgs.put("category", category);
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		PageCategory category = PageCategory.findById(id);

		// unassign all pages
		for (Page page : category.pages) {
			page.category = null;
			page.save();
		}

		category.delete();
		flash.success(Messages.get("administration.pageCategories.delete.success"));
	}
}

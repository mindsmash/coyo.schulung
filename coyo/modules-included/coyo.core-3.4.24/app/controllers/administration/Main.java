package controllers.administration;

import java.util.ArrayList;
import java.util.List;

import play.Logger;
import play.Play;
import play.i18n.Messages;
import play.mvc.Router;
import security.Secure;
import acl.Permission;
import controllers.Security;
import controllers.WebBaseController;

/**
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_INTERFACE_ACCESS, redirect = "/")
public class Main extends WebBaseController {

	private static List<SettingsNavItem> settingsNav;

	public static void index() {
		if (Security.check(Permission.ADMIN_USER)) {
			Users.index();
		}
		if (Security.check(Permission.ADMIN_TEASER)) {
			Teasers.index();
		}
		if (Security.check(Permission.ADMIN_CATEGORIES)) {
			PageCategories.index();
		}
		if (Security.check(Permission.ADMIN_BOOKMARKS)) {
			Bookmarks.index();
		}
		if (Security.check(Permission.ADMIN_THEME)) {
			ThemeSettings.index();
		}
		if (Security.check(Permission.ADMIN_STATISTICS)) {
			Statistics.index(null, null, null);
		}
		redirect("/");
	}

	public static List<SettingsNavItem> getSettingsNav() {
		if (settingsNav == null) {
			settingsNav = new ArrayList<>();
			List<Class> controllers = Play.classloader.getAnnotatedClasses(SettingsNav.class);
			for (Class controllerClass : controllers) {
				try {
					SettingsNav anno = (SettingsNav) controllerClass.getAnnotation(SettingsNav.class);

					SettingsNavItem item = new SettingsNavItem();
					item.controllerClass = controllerClass;
					item.label = Messages.get(anno.label());
					item.url = Router.reverse(controllerClass.getName().replaceFirst("controllers.", "") + "."
							+ anno.indexAction()).url;
					settingsNav.add(item);
				} catch (Exception e) {
					Logger.warn("[administration.Main] Error loading settings nav item for controller: %s", controllerClass);
					Logger.debug(e, "[administration.Main] Error loading settings nav item for controller: %s", controllerClass);
				}
			}
		}
		return settingsNav;
	}

	public static class SettingsNavItem {

		public Class controllerClass;
		public String url;
		public String label;
	}
}

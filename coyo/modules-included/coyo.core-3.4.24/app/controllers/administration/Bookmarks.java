package controllers.administration;

import models.bookmark.Bookmark;
import models.dao.BookmarkDao;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the bookmarks.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_BOOKMARKS, redirect = "/")
public class Bookmarks extends WebBaseController {

	public static void index() {
		renderArgs.put("bookmarks", BookmarkDao.getGlobalBookmarks());
		render();
	}

	public static void create() {
		Bookmark bookmark = new Bookmark();
		render(bookmark);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(@Valid Bookmark bookmark) {
		if (Validation.hasErrors()) {
			render("@create", bookmark);
		}

		bookmark.save();
		index();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void order(String order) {
		Bookmark.storeOrder(order);
	}

	public static void edit(Long id) {
		Bookmark bookmark = Bookmark.findById(id);
		renderArgs.put("bookmark", bookmark);
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		Bookmark bookmark = Bookmark.findById(id);
		bookmark.delete();
	}
}

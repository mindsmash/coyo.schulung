package controllers.administration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jobs.SearchReindexer;

import org.apache.commons.lang.StringUtils;

import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Util;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;
import fishr.Fishr;
import fishr.ManagedIndex;

/**
 * Handles all actions for admins to manage the search.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Search extends WebBaseController {

	public static void index() {
		models.Settings settings = models.Settings.findOrCreate("search");
		renderArgs.put("active", settings.getBoolean("active"));

		if (settings.contains("finished") && StringUtils.isNumeric(settings.getString("finished"))) {
			renderArgs.put("finished", new Date(Long.parseLong(settings.getString("finished"))));
		}
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void reindex() {
		// run
		List<String> indexes = new ArrayList<String>();
		for (ManagedIndex index : Fishr.listIndexes()) {
			indexes.add(index.name);
		}

		// execute
		new SearchReindexer(indexes).now();

		models.Settings settings = models.Settings.findOrCreate("search");
		settings.removeProperty("reindexRequired");
		settings.save();

		flash.success(Messages.get("admin.search.reindex.started"));
	}

	public static void status() {
		models.Settings settings = models.Settings.findOrCreate("search");
		renderHtml(settings.getBoolean("active"));
	}

	@Transactional
	@CheckAuthenticity
	public static void cancel() {
		models.Settings settings = models.Settings.findOrCreate("search");
		settings.setProperty("active", "false");
		settings.save();
	}

	@Util
	public static boolean reindexRequired() {
		models.Settings settings = models.Settings.findOrCreate("search");
		return settings.getBoolean("reindexRequired");
	}
}

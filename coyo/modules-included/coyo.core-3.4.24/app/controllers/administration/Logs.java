package controllers.administration;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Util;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;

/**
 * Handles all actions for admins to manage the logs.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Logs extends WebBaseController {

	public static final boolean SHOW_LOGS = Boolean.parseBoolean(Play.configuration
			.getProperty("coyo.showLogs", "true"));

	private static String logsDir = "logs";

	@Before
	static void checkActive() {
		if (!SHOW_LOGS) {
			redirect("/");
		}
	}

	public static void index() {
		File logs = Play.getFile(logsDir);

		List<File> files;
		if (logs != null && logs.exists()) {
			files = Arrays.asList(logs.listFiles());
		} else {
			files = new ArrayList<File>();
			flash.error(Messages.get("administration.logs.notFound"));
		}

		renderArgs.put("files", files);
		render();
	}

	public static void log(String file) {
		File log = Play.getFile(logsDir + File.separator + file);
		render(log);
	}

	public static void load(String file) throws Exception {
		File log = Play.getFile(logsDir + File.separator + file);

		renderArgs.put("data", tail(log));
		render(log);
	}

	public static void download(String file) throws Exception {
		File log = Play.getFile(logsDir + File.separator + file);
		renderBinary(new FileInputStream(log), file, log.length(), false);
	}

	@Util
	public static String tail(File file) throws Exception {
		StringBuffer buffer = new StringBuffer();

		RandomAccessFile raf = new RandomAccessFile(file, "r");

		try {
			// jump to end of file
			long seek = file.length() - 10000L;
			if (seek > 0) {
				raf.seek(seek);
			}

			// finish current line
			raf.readLine();

			// read remaining lines
			String nextLine;
			while (!StringUtils.isEmpty(nextLine = raf.readLine())) {
				buffer.append(nextLine + "\n");
			}
		} finally {
			raf.close();
		}

		return buffer.toString();
	}
}

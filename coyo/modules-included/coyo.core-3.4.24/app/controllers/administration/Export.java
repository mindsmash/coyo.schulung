package controllers.administration;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import jobs.TenantExportJob;
import models.Application;
import models.Settings;
import multitenancy.MTA;
import play.Play;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Util;
import security.Secure;
import storage.FlexibleBlob;
import acl.Permission;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * @author mindsmash GmbH
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Export extends WebBaseController {

	public static final Integer MAX_EXPORTS = Integer.valueOf(Play.configuration.getProperty("coyo.dataExport.max",
			"10"));

	static void checkAllowed() {
		if (!Application.DATA_EXPORT_ALLOWED) {
			Main.index();
		}
	}

	public static void index() {
		models.Settings settings = models.Settings.findOrCreate("export");
		renderArgs.put("active", settings.getBoolean("active"));
		renderArgs.put("exports", getExports());
		render();
	}

	@CheckAuthenticity
	public static void export() throws Exception {
		if (getExports().size() >= MAX_EXPORTS) {
			flash.error(Messages.get("admin.export.maxReached", MAX_EXPORTS));
			index();
		}

		// execute
		new TenantExportJob(MTA.getActiveTenant()).now();

		flash.success(Messages.get("admin.export.started"));
	}

	public static void status() {
		models.Settings settings = models.Settings.findOrCreate("export");
		renderHtml(settings.getBoolean("active"));
	}

	@Transactional
	@CheckAuthenticity
	public static void cancel() {
		models.Settings settings = models.Settings.findOrCreate("export");
		settings.setProperty("active", "false");
		settings.save();
	}

	public static void download(String file) {
		renderBinary(loadExportFile(file).get(), file);
	}

	@CheckAuthenticity
	public static void delete(String file) {
		loadExportFile(file).delete();
	}

	@Util
	private static Map<FlexibleBlob, Date> getExports() {
		Map<FlexibleBlob, Date> r = new LinkedHashMap<>();

		Settings exportSettings = Settings.findOrCreate("dataexport");
		for (String key : exportSettings.getProperties().keySet()) {
			FlexibleBlob blob = new FlexibleBlob(key, TenantExportJob.MIME_TYPE);
			if (blob.exists()) {
				r.put(blob, new Date(exportSettings.getLong(key)));
			}
		}

		return r;
	}

	@Util
	private static FlexibleBlob loadExportFile(String file) {
		notFoundIfNull(file);

		FlexibleBlob blob = new FlexibleBlob(file, TenantExportJob.MIME_TYPE);
		if (!blob.exists()) {
			notFound();
		}

		return blob;
	}
}
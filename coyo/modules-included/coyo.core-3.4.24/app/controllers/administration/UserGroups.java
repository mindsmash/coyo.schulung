package controllers.administration;

import java.util.HashSet;
import java.util.List;

import models.StaticUserGroup;
import models.User;
import models.UserGroup;
import models.UserRole;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import security.Secure;
import acl.Permission;
import binding.ModelIdBinder;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the user groups.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMIN_USER, redirect = "/")
public class UserGroups extends WebBaseController {

	@Before(only = { "edit", "create" })
	public static void prepareForm() {
		renderArgs.put("roles", UserRole.findAll());
	}

	public static void index() {
		List<UserGroup> groups = UserGroup.find("ORDER BY name ASC").fetch();
		render(groups);
	}

	public static void create() {
		StaticUserGroup group = new StaticUserGroup();
		render(group);
	}

	public static void edit(Long id) {
		UserGroup group = UserGroup.findById(id);
		notFoundIfNull(group);
		render(group);
	}

	public static void users(Long id) {
		UserGroup group = UserGroup.findById(id);
		notFoundIfNull(group);

		if (!group.isUserManageable()) {
			index();
		}

		render(group);
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		UserGroup group = UserGroup.findById(id);
		notFoundIfNull(group);

		if (group.isDeletable()) {
			group.delete();
			flash.success(Messages.get("group.delete.success"));
		}
	}

	@Transactional
	@CheckAuthenticity
	public static void save(StaticUserGroup group) {
		if (!group.validateAndSave()) {
			if (group.isPersistent()) {
				render("@edit", group);
			} else {
				render("@create", group);
			}
		}

		flash.success(Messages.get("group.save.success"));
		index();
	}

	@Transactional
	@CheckAuthenticity
	public static void saveUsers(StaticUserGroup group, List<Long> users) {
		if (group.isUserManageable()) {
			if (users != null) {
				List<User> userList = ModelIdBinder.bind(User.class, users);
				group.users = new HashSet<User>(userList);
			} else {
				group.users.clear();
			}

			group.save();
		}

		flash.success(Messages.get("group.save.success"));
		index();
	}
}

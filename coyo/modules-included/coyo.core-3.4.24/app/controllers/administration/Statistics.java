package controllers.administration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jobs.StatisticsJob;
import models.User;
import models.statistics.TrackingRecord;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import play.mvc.Http.Request;
import play.mvc.Scope.Session;
import play.mvc.Util;
import security.Secure;
import acl.Permission;
import controllers.WebBaseController;

@Secure(permissions = Permission.ADMIN_STATISTICS, redirect = "/")
public class Statistics extends WebBaseController {


	public static final String ACTIVE_USERS = "activeUsers";
	public static final String ACTIVE_WORKSPACES = "activeWorkspaces";
	public static final String PAGES = "pages";
	public static final String LIKES = "likes";

	public static void index(Date date, Integer days, Date inputDate) {
		if (date == null) {
			// beginning of week
			date = new DateTime().withDayOfWeek(1).toDate();
		}

		if (days == null) {
			// week
			days = 7;
		}

		if(inputDate == null) {
			date = new DateTime().withDayOfWeek(1).toDate();
		}

		// general daily
		renderArgs.put("activeUserCount", User.count("active = true"));
		renderArgs.put("logins", TrackingRecord.load(date, "logins", days));
		renderArgs.put("visits", TrackingRecord.load(date, "visits", days));
		renderArgs.put("pageviews", TrackingRecord.load(date, "pageviews", days));
		renderArgs.put("posts", TrackingRecord.load(date, "posts", days));
		renderArgs.put("comments", TrackingRecord.load(date, "comments", days));
		renderArgs.put("likes", TrackingRecord.load(date, "likes", days));
		renderArgs.put("workspaces", StatisticsJob.getActiveWorkspaceCount());
		renderArgs.put("pages", StatisticsJob.getPageCount());
		renderArgs.put("events", StatisticsJob.getTodayEventCount());

		render(date, days, inputDate);
	}

	public static void details(String trackType, Integer currentCount, Date date, Integer days,  boolean displayDirection ) {
		Date inputDate = date;

		if (days == null) {
			days = 7;
		}

		if (date == null) {
			// beginning of month
			date = new DateTime().withDayOfWeek(1).toDate();
			inputDate = date;
		}

		if(!displayDirection) {
			inputDate = date;
			date = DateUtils.addDays(date, -days);
		}

		switch(trackType){
			case ACTIVE_USERS : currentCount = (int) (long) User.count("active = true");
				break;
			case ACTIVE_WORKSPACES : currentCount = (int) (long) StatisticsJob.getActiveWorkspaceCount();
				break;
			case PAGES : currentCount = (int) (long) StatisticsJob.getPageCount();
				break;
			case LIKES : currentCount = TrackingRecord.load(date, "likes", days).total;
				break;
		}

		renderArgs.put("result", TrackingRecord.load(date, trackType, days));
		renderArgs.put("to", DateUtils.addDays(date, days));

		render(trackType, date, days, currentCount, inputDate, displayDirection);
	}

	@Util
	public static void trackPageLoad() {
		// track URL
		TrackingRecord.trackOne("pageviews", Request.current().path);

		// track visit only if session is not set
		if (!Session.current().contains("visitTracked")) {
			TrackingRecord.trackOne("visits");
			Session.current().put("visitTracked", "true");
		}
	}
}
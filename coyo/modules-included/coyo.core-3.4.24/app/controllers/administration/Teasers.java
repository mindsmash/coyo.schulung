package controllers.administration;

import java.util.List;

import json.FileAttachmentSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.FileAttachment;
import models.Settings;
import models.User;
import models.teaser.TeaserItem;
import play.data.validation.Required;
import play.db.jpa.Transactional;
import security.Secure;
import session.UserLoader;
import utils.ImageUtils;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import controllers.WebBaseController;
import controllers.api.TeaserAPI;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for admins to manage the teaser and the teaser carousel.
 * 
 * @author Sven Hoffmann, Denis Meyer
 */
@Secure(permissions = Permission.ADMIN_TEASER)
public class Teasers extends WebBaseController {

	public class TeaserSettings {
		@Required
		public Long interval;
		
		@Required
		public Integer height;
	}

	public static void index() {
		render();
	}
	
	public static void getTeasers(int page, int count) {
		List<TeaserItem> items = TeaserItem.find("ORDER BY priority ASC").fetch(page, count);
		Long total = TeaserItem.count();
		
		/* create JSON response containing the user paginated list of users and the total count */
		Gson gson = createBuilder().create();
		JsonObject result = new JsonObject();
		result.add("teaserList", gson.toJsonTree(items));
		result.addProperty("total", total);
		renderJSON(gson.toJson(result));
	}
	
	@CheckAuthenticity
	@Transactional
	public static void saveTeaserSettings(@JsonBody TeaserSettings teaserSettings) {
		if (validation.hasErrors()) {
			badRequest();
		}
		Settings settings = models.Settings.findOrCreate(TeaserAPI.TEASER_SETTINGS_KEY);
		settings.setProperty(TeaserAPI.TEASER_INTERVAL, teaserSettings.interval.toString());
		settings.setProperty(TeaserAPI.TEASER_HEIGHT, teaserSettings.height.toString());
		settings.save();
		renderJSON(createBuilder().create().toJson(settings));
	}
	
	public static void getTeaser(Long id) {
		TeaserItem teaser = TeaserItem.findById(id);
		notFoundIfNull(teaser);
		renderJSON(createBuilder().create().toJson(teaser));
	}
	
	@CheckAuthenticity
	@Transactional
	public static void save(@JsonBody TeaserItem teaser) {
		validation.isTrue(teaser.title != null && teaser.title.length() >= 1).message("validation.teasers.titleTooSmall");
		validation.isTrue(teaser.title != null && teaser.title.length() < 90).message("validation.teasers.titleTooLong");
		validation.isTrue(teaser.description == null || (teaser.description != null && teaser.description.length() < 540)).message("validation.teasers.descriptionTooLong");
		boolean imageUploaded = teaser.image != null && teaser.image.exists();
		validation.isTrue(imageUploaded).message("validation.teasers.noImage");
		if(imageUploaded) {
			validation.isTrue(ImageUtils.isImage(teaser.image)).message("validation.teasers.incorrectImageFormat");
		}
		if (validation.hasErrors()) {
	        JsonObject errorComplete = new JsonObject();
	        errorComplete.addProperty("error", "true");
	        JsonArray errors = new JsonArray();
			for(int i = 0; i < validation.errors().size(); ++i) {
		        JsonObject error = new JsonObject();
		        error.addProperty("key", validation.errors().get(i).getKey());
		        error.addProperty("msg", validation.errors().get(i).message());
		        errors.add(error);
			}
			errorComplete.add("errors", errors);
			renderJSON(createBuilder().create().toJson(errorComplete));
		}
		if (teaser.author == null) {
			teaser.author = UserLoader.getConnectedUser();
		}
		teaser.save();
		renderJSON(createBuilder().create().toJson(teaser));
	}
	
	@CheckAuthenticity
	@Transactional
	public static void toggle(Long id, boolean active) {
		TeaserItem teaser = TeaserItem.findById(id);
		notFoundIfNull(teaser);
		
		teaser.active = active;
		teaser.save();
	}
	
	@CheckAuthenticity
	@Transactional
	public static void delete(Long id) {
		TeaserItem teaser = TeaserItem.findById(id);
		notFoundIfNull(teaser);
		teaser.delete();
	}

	@CheckAuthenticity
	@Transactional
	public static void order() {
		final String json = params.getRootParamNode().getChild("body", true).getFirstValue(String.class);
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();

		int priority = 0;
		for (JsonElement element : array) {
			TeaserItem teaser = TeaserItem.findById(Long.valueOf(element.getAsLong()));
			if (teaser != null) {
				teaser.priority = priority++;
				teaser.save();
			}
		}
	}
	
	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(FileAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		return builder;
	}
	
}

package controllers.administration;

import java.util.List;
import java.util.Map;

import models.Application;
import models.Tenant;
import models.User;
import models.UserNotificationSettings;
import models.UserNotificationSettings.MailInterval;
import multitenancy.MTA;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Required;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import acl.Permission;
import conf.ApplicationSettings;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;
import util.Util;

/**
 * Handles all actions for admins to manage the settings.
 * 
 * @author Jan Marquardt
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class Settings extends WebBaseController {

	public static void index() {
		renderArgs.put("groups", ApplicationSettings.groups);
		renderArgs.put("intervalTypes", UserNotificationSettings.MailInterval.values());
		renderArgs.put("mailEvents", UserNotificationSettings.getAllMailEvents());
		renderArgs.put("notSettings", Application.get().defaultNotificationSettings);

		Tenant tenant = (Tenant) MTA.getActiveTenant();

		renderArgs.put("quota", tenant.quota);
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void save(Map<String, String> settings, @Required List<String> notificationKeys, boolean neverMail,
			MailInterval interval) {
		if (settings != null) {
			models.Settings appSettings = models.Settings.findApplicationSettings();

			boolean autofollowChanged = settings.containsKey(ApplicationSettings.AUTOFOLLOW)
					&& !settings.get(ApplicationSettings.AUTOFOLLOW).equals(
							appSettings.getString(ApplicationSettings.AUTOFOLLOW));

			// save new settings
			for (String key : settings.keySet()) {
				if (key.equals(ApplicationSettings.REPORTED_POST_MAIL)) {
					// add only valid emails
					appSettings.setProperty(key, StringUtils.join(Util.parseEmailList(settings.get(key)), ","));
				} else {
					appSettings.setProperty(key, settings.get(key));
				}
			}
			appSettings.save();

			// check auto-follow
			if (autofollowChanged) {
				List<User> all = User.findAll();
				for (User user : all) {
					user.save();
				}
			}
		}

		// save notification settings
		Application application = Application.get();
		if (interval != null) {
			application.defaultNotificationSettings.mailInterval = interval;
		}
		application.defaultNotificationSettings.receiveMails = !neverMail;

		if (!neverMail) {
			application.defaultNotificationSettings.mailEvents.clear();
			if (notificationKeys != null && notificationKeys.size() > 0) {
				application.defaultNotificationSettings.mailEvents.addAll(notificationKeys);
			} else {
				application.defaultNotificationSettings.receiveMails = false;
			}
		}
		application.save();

		flash.success(Messages.get("administration.settings.save.success"));
		index();
	}
}

package controllers.administration;

import org.apache.commons.lang.StringUtils;

import license.LicenseValidator;
import license.v2.License;
import models.Tenant;
import multitenancy.MTA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import acl.Permission;

import com.google.gson.JsonObject;

import conf.ApplicationSettings;
import controllers.WebBaseController;
import csrf.CheckAuthenticity;

/**
 * @author Jan Tammen, mindsmash GmbH
 */
@Secure(permissions = Permission.ADMINISTRATION, redirect = "/")
public class ServerInfo extends WebBaseController {
	public static void index() {
		renderArgs.put("groups", ApplicationSettings.groups);

		Tenant tenant = (Tenant) MTA.getActiveTenant();

		String license = LicenseValidator.loadLicenseString();
		renderArgs.put("licenseKey", license);

		if (StringUtils.isNotEmpty(license)) {
			JsonObject licenseData = License.evaluate(license);
			if (licenseData != null) {
				renderArgs.put("licenseInfo", LicenseValidator.info(LicenseValidator.PRODUCT, licenseData));
			}
		}

		renderArgs.put("quota", tenant.quota);
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void resetServerID() {
		LicenseValidator.resetServerID();
	}

	@Transactional
	@CheckAuthenticity
	public static void save(String license) {
		if (license != null) {
			Tenant tenant = ((Tenant) MTA.getActiveTenant());
			tenant.license = license;
			tenant.save();
		}

		// clear license cache
		LicenseValidator.clearCache();

		flash.success(Messages.get("administration.settings.save.success"));
		index();
	}
}

package controllers.administration;

import acl.Permission;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import conf.ApplicationSettings;
import controllers.Security;
import controllers.WebBaseController;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import json.UserAdminSerializer;
import json.UserDeviceSerializer;
import models.Sender;
import models.User;
import models.UserDevice;
import models.UserGroup;
import models.UserRole;
import notifiers.ApplicationMailer;
import org.hibernate.Session;
import play.data.validation.Equals;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Codec;
import play.mvc.Before;
import security.Secure;
import session.UserLoader;
import util.Util;

import java.util.List;
import java.util.Map;

/**
 * Handles all actions for admins to manage the users.
 * 
 * @author Drews Clausen, Jan Marquardt
 * 
 */
@ShowExternals
@Secure(permissions = Permission.ADMIN_USER, redirect = "/")
public class Users extends WebBaseController {
	
	private static final String SORTING_KEY_ID = "id";
	private static final String SORTING_KEY_NAME = "name";
	private static final String SORTING_KEY_EMAIL = "email";
	private static final String FILTER_KEY_NAME = "name";
	

	@Before(only = { "edit", "create" })
	static void prepareForm() {
		renderArgs.put("roles", UserRole.findAll());
		renderArgs.put("defaultRole", UserRole.findDefaultRole());
	}

	@Before(only = { "doEdit", "recover" })
	static void deactivateSoftDelete() {
		((Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
	}

	public static void index() {
		render();
	}

	public static void deleted() {
		render();
	}

	public static void list(boolean showDeleted, int page, int count, Map<String, String> sorting, Map<String, String> filter) {
		JpqlSelect select = new JpqlSelect();
		
		/* Sorting */
		if (sorting != null && sorting.size() > 0) {
			if (sorting.containsKey(SORTING_KEY_EMAIL)) {
				String direction = getDirection(sorting, SORTING_KEY_EMAIL);
				select.orderBy("email " + direction);
			} else if (sorting.containsKey(SORTING_KEY_ID)) {
				String direction = getDirection(sorting, SORTING_KEY_ID);
				select.orderBy("id " + direction);
			} else {
				String direction = getDirection(sorting, SORTING_KEY_NAME);
				select.orderBy("lastName " + direction + ", firstName " + direction);
			}
		} else {
			select.orderBy("lastName ASC, firstName ASC");
		}
		
		/* Search */
		if (filter != null && filter.size() > 0) {
			String query = filter.get(FILTER_KEY_NAME);
			List<String> terms = Util.parseList(query, " ");
			for (String string : terms) {
				String term = "%" + string.toLowerCase() + "%";
				select.andWhere("(LOWER(lastName) LIKE ? OR LOWER(firstName) LIKE ?)").params(term, term);
			}
		}
		
		/* show only deleted users */
		if (showDeleted) {
			((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
			select.andWhere("deleted = true");
		} 
		
		/* query users and determine total count */
		List<User> userList = User.find(select.toString(), select.getParams().toArray()).fetch(page, count);
		Long total = (showDeleted) ? User.count("deleted = true") : User.count();
		
		/* create JSON response containing the user paginated list of users and the total count */
		Gson gson = createBuilder().create();
		JsonObject result = new JsonObject();
		result.add("userlist", gson.toJsonTree(userList));
		result.addProperty("total", total);
		renderJSON(gson.toJson(result));
	}

	public static void create() {
		User user = new User();
		user.role = UserRole.findDefaultRole();
		render(user);
	}

	public static void edit(Long userId) {
		User user = User.findById(userId);
		notFoundIfNull(user);

		render(user);
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.ADMIN_EDIT_USER, params = { "userId" })
	public static void delete(Long userId) {
		User user = User.findById(userId);
		notFoundIfNull(user);

		user.delete();
		flash.success(Messages.get("administration.users.delete.success"));
	}

	@Transactional
	@CheckAuthenticity
	public static void doCreate(User user, @Required String password, boolean forceNew, boolean superadmin) {
		user.setPassword(password);

		((Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);

		User existingUser = User.find("byEmail", user.email).first();
		if (existingUser != null && existingUser.isPersistent()) {
			if (forceNew) {
				// invalidate old user email
				existingUser.email = "deleted-" + Codec.UUID() + "-" + existingUser.email;
				existingUser.save();
			} else {
				Validation.addError("email", Messages.get("user.email.alreadyInUse"));

				// show recovery option
				if (existingUser.deleted) {
					renderArgs.put("recover", existingUser);
				}
			}
		}

		((Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);

		// only superadmins can change superadmin flag
		if (UserLoader.getConnectedUser().superadmin) {
			user.superadmin = superadmin;
		}

		Validation.valid("user", user);
		if (Validation.hasErrors()) {
			Validation.keep();
			render("@create", user);
		}

		user.timezone = getSettings().getString(ApplicationSettings.DEFAULT_TIMEZONE);
		user.resetPassword = true;
		user.save();

		UserGroup defaultGroup = UserGroup.findDefaultGroup();
		if (defaultGroup != null) {
			defaultGroup.addUser(user);
		}

		flash.success(Messages.get("administration.users.create.success"));
		index();
	}

	@Transactional
	@CheckAuthenticity
	public static void doEdit(User user, boolean oldActiveStatus, boolean external, boolean superadmin, boolean deleted) {
		Security.checkAndCancel(Permission.ADMIN_EDIT_USER, user.id);
		User existingUser = User.find("byEmail", user.email).first();
		if (existingUser != null && existingUser.isPersistent() && user.id != existingUser.id) {
			Validation.addError("email", Messages.get("user.email.alreadyInUse"));
		}

		user.external = external;
		user.deleted = deleted;

		// only superadmins can change superadmin flag
		if (UserLoader.getConnectedUser().superadmin) {
			user.superadmin = superadmin;
		}

		Validation.valid("user", user);

		if (Validation.hasErrors()) {
			Validation.keep();
			render("@edit", user);
		}

		if (!user.deleted && !oldActiveStatus && user.active) {
			ApplicationMailer.userActivated(user);
		}

		user.save();
		flash.success(Messages.get("administration.users.save.success"));

		if (user.deleted) {
			deleted();
		}
		index();
	}

	public static void changePassword(Long userId) {
		User user = User.findById(userId);
		notFoundIfNull(user);

		if (!user.isLocal()) {
			index();
		}

		render(user);
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.ADMIN_EDIT_USER, params = { "userId" })
	public static void doChangePassword(Long userId, @Required String password,
			@Equals("password") String passwordRepeat) {
		User user = User.findById(userId);
		notFoundIfNull(user);

		if (!user.isLocal()) {
			index();
		}

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			Validation.keep(); // keep the errors for the next request
			JPA.em().detach(user);
			changePassword(userId);
		}

		user.resetPassword = true;
		user.password = password;
		user.save();

		index();
	}

	@Transactional
	@CheckAuthenticity
	public static void resetAllPasswords() {
		// reset for all local users
		JPA.em().createQuery("UPDATE User u SET u.resetPassword = true WHERE authSource IS NULL").executeUpdate();

		flash.success(Messages.get("administration.users.passwordReset.success"));
	}

	@Transactional
	@CheckAuthenticity
	public static void recover(Long userId) {
		// load deleted user
		User user = User.findById(userId);
		notFoundIfNull(user);

		if (user.deleted) {
			user.deleted = false;
			user.save();
			flash.success(Messages.get("administration.users.recover.sucess"));
		}
	}
	
	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new UserAdminSerializer());
		builder.registerTypeAdapter(UserDevice.class, new UserDeviceSerializer());
		return builder;
	}
	
	private static String getDirection(Map<String, String> sorting, String key) {
		return sorting.get(key).equalsIgnoreCase("asc") ? "asc" : "desc";
	}
}

package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Calendar;

import models.User;
import net.sourceforge.cardme.io.VCardWriter;
import net.sourceforge.cardme.vcard.EncodingType;
import net.sourceforge.cardme.vcard.VCardImpl;
import net.sourceforge.cardme.vcard.features.BirthdayFeature;
import net.sourceforge.cardme.vcard.features.EmailFeature;
import net.sourceforge.cardme.vcard.features.FormattedNameFeature;
import net.sourceforge.cardme.vcard.features.NameFeature;
import net.sourceforge.cardme.vcard.features.NoteFeature;
import net.sourceforge.cardme.vcard.features.OrganizationFeature;
import net.sourceforge.cardme.vcard.features.PhotoFeature;
import net.sourceforge.cardme.vcard.features.TelephoneFeature;
import net.sourceforge.cardme.vcard.features.URLFeature;
import net.sourceforge.cardme.vcard.types.AddressType;
import net.sourceforge.cardme.vcard.types.BirthdayType;
import net.sourceforge.cardme.vcard.types.EmailType;
import net.sourceforge.cardme.vcard.types.FormattedNameType;
import net.sourceforge.cardme.vcard.types.NameType;
import net.sourceforge.cardme.vcard.types.NicknameType;
import net.sourceforge.cardme.vcard.types.NoteType;
import net.sourceforge.cardme.vcard.types.OrganizationType;
import net.sourceforge.cardme.vcard.types.PhotoType;
import net.sourceforge.cardme.vcard.types.TelephoneType;
import net.sourceforge.cardme.vcard.types.TitleType;
import net.sourceforge.cardme.vcard.types.URLType;
import net.sourceforge.cardme.vcard.types.parameters.PhotoParameterType;
import net.sourceforge.cardme.vcard.types.parameters.TelephoneParameterType;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.libs.IO;
import security.Secure;
import acl.Permission;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;

/**
 * Generate a user vcard
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
@AllowExternals
@ShowExternals
public class VCard extends WebBaseController {

	@Secure(permissions = Permission.ACCESS_USER, params = { "id" })
	public static void vcf(final Long id, final String slug) {
		notFoundIfNull(id);
		User user = User.findById(id);
		notFoundIfNull(user);

		Charset charset = Charset.forName("iso-8859-1");

		net.sourceforge.cardme.vcard.VCard vcard = new VCardImpl();
		NameFeature name = new NameType(user.lastName, user.firstName);
		name.setCharset(charset);
		vcard.setName(name);

		FormattedNameFeature formattedName = new FormattedNameType(user.getFullNameWithTitle());
		formattedName.setCharset(charset);
		vcard.setFormattedName(formattedName);

		if (StringUtils.isNotEmpty(user.email)) {
			EmailFeature email = new EmailType(user.email);
			email.setCharset(charset);
			vcard.addEmail(email);
		}

		if (null != user.avatar && user.avatar.exists()) {
			try (InputStream is = user.avatar.get()) {
				PhotoFeature photo = new PhotoType(IO.readContent(is), EncodingType.BINARY,
						PhotoParameterType.VALUE);
				photo.setCharset(charset);
				vcard.addPhoto(photo);
			} catch (IOException ex) {
				Logger.error(ex, "[VCard] Failed to read user avatar for user: %s", user.id);
			}
		}

		if (StringUtils.isNotEmpty(user.jobTitle)) {
			TitleType title = new TitleType(user.jobTitle);
			title.setCharset(charset);
			vcard.setTitle(title);
		}

		if (StringUtils.isNotEmpty(user.nickname)) {
			NicknameType nicknames = new NicknameType();
			nicknames.addNickname(user.nickname);
			nicknames.setCharset(charset);
			vcard.setNicknames(nicknames);
		}

		if (StringUtils.isNotEmpty(user.company)) {
			OrganizationFeature organization = new OrganizationType(user.company);
			organization.setCharset(charset);
			vcard.setOrganizations(organization);
		}

		if (null != user.birthdate) {
			Calendar birthday = Calendar.getInstance();
			birthday.setTime(user.birthdate);
			BirthdayFeature birthdayType = new BirthdayType(birthday);
			birthdayType.setCharset(charset);
			vcard.setBirthday(birthdayType);
		}

		if (StringUtils.isNotEmpty(user.street) || StringUtils.isNotEmpty(user.streetNumber)
				|| StringUtils.isNotEmpty(user.zipCode) || StringUtils.isNotEmpty(user.city)
				|| StringUtils.isNotEmpty(user.country)) {
			AddressType address = new AddressType();
			address.setCharset(charset);
			if (StringUtils.isNotEmpty(user.street) || StringUtils.isNotEmpty(user.streetNumber)) {
				address.setStreetAddress(user.street + " " + user.streetNumber);
			}
			if (StringUtils.isNotEmpty(user.zipCode)) {
				address.setPostalCode(user.zipCode);
			}
			if (StringUtils.isNotEmpty(user.city)) {
				address.setLocality(user.city);
			}
			if (StringUtils.isNotEmpty(user.country)) {
				address.setCountryName(user.country);
			}
			vcard.addAddress(address);
		}

		if (StringUtils.isNotEmpty(user.mobilePhone)) {
			TelephoneFeature number = new TelephoneType(user.mobilePhone, TelephoneParameterType.CELL);
			number.setCharset(charset);
			vcard.addTelephoneNumber(number);
		}

		if (StringUtils.isNotEmpty(user.workPhone)) {
			TelephoneFeature number = new TelephoneType(user.workPhone, TelephoneParameterType.WORK);
			number.setCharset(charset);
			vcard.addTelephoneNumber(number);
		}

		if (StringUtils.isNotEmpty(user.workFax)) {
			TelephoneFeature number = new TelephoneType(user.workFax, TelephoneParameterType.FAX);
			number.setCharset(charset);
			vcard.addTelephoneNumber(number);
		}

		if (StringUtils.isNotEmpty(user.website)) {
			URLFeature url = new URLType(user.website);
			url.setCharset(charset);
			vcard.addURL(url);
		}

		if (StringUtils.isNotEmpty(user.expertise)) {
			NoteFeature note = new NoteType(user.expertise);
			note.setCharset(charset);
			vcard.addNote(note);
		}

		if (StringUtils.isNotEmpty(user.about)) {
			NoteFeature note = new NoteType(user.about);
			note.setCharset(charset);
			vcard.addNote(note);
		}

		VCardWriter writer = new VCardWriter();
		writer.setVCard(vcard);
		response.setContentTypeIfNotSet("text/x-vcard");
		response.encoding = charset.name();
		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}
		renderText(writer.buildVCardString());
	}
}

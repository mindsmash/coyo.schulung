package controllers.api;

import java.util.ArrayList;
import java.util.List;

import models.wall.Wall;
import models.wall.post.Post;
import models.wall.post.SimplePost;
import play.data.Upload;
import play.db.jpa.Transactional;
import session.UserLoader;
import acl.Permission;
import controllers.ActivityStream;
import controllers.Security;
import csrf.CheckAuthenticity;

/**
 * Handles all actions to access walls via API call.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class WallAPI extends API {

	/**
	 * Return posts from the wall in chronological order.
	 * 
	 * @param limit
	 */
	public static void posts(Long id, Integer limit) {
		Wall wall = Wall.findById(id);
		notFoundIfNull(wall);
		
		Security.checkAndCancel(Permission.ACCESS_SENDER, wall.sender);
		
		if (limit == null) {
			limit = 50;
		}

		List<Object> data = new ArrayList<Object>();
		List<Post> as = wall.getPostsQuery(UserLoader.getConnectedUser()).fetch(limit);
		for (Post post : as) {
			data.add(ActivityStreamAPI.convertPost(post));
		}
		renderJSON(data);
	}

	/**
	 * Creates a post on the {@link ActivityStream}.
	 * 
	 * @param id
	 *            ID of the sender.
	 * @param message
	 *            Message of the post.
	 * @param files
	 *            List of files to attach.
	 */
	@Transactional
	@CheckAuthenticity
	public static void post(Long id, String message, List<Upload> files) {
		Wall wall = Wall.findById(id);
		notFoundIfNull(wall);

		Security.checkAndCancel(Permission.CREATE_POST, wall.id);

		Post post = new SimplePost();
		post.author = UserLoader.getConnectedUser();
		post.message = message;
		post.wall = wall;
		post.save();

		post.createAttachments(files);

		renderJSON(ActivityStreamAPI.convertPost(post));
	}
}

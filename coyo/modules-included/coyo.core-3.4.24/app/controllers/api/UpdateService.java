package controllers.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import onlinestatus.OnlineStatusServiceFactory;
import org.apache.commons.lang.StringUtils;

import json.ConversationsSerializer;
import json.EventSerializer;
import json.MinimalUserSerializer;
import json.NotificationSerializer;
import json.UserNotificationSerializer;
import models.User;
import play.Logger;
import play.Play;
import play.libs.F.Either;
import play.libs.F.Matcher;
import play.libs.F.Promise;
import play.libs.F.Timeout;
import play.libs.Time;
import play.mvc.Finally;
import play.mvc.Util;
import session.UserLoader;
import controllers.external.annotations.AllowExternals;
import events.Event;
import events.EventQueue;
import events.EventService;

/**
 * 
 * Long Polling service for the global EventService.
 * 
 * @author Piers Wermbter, mindsmash GmbH
 * 
 */
@AllowExternals
public class UpdateService extends API {

	private static final String TIMEOUT = Play.configuration.getProperty("coyo.updateservice.await.timeout", "30s");

	public static void index() {
		User connectedUser = UserLoader.getConnectedUser();

		if (connectedUser != null) {
			OnlineStatusServiceFactory.getOnlineStatusService().refreshOnlineStatus(connectedUser);
		}

		Long id;
		boolean init = true;

		try {
			String lastEventId = params.get("lastEventId");
			
			EventQueue stream = await(EventService.getOrCreateEventStream(connectedUser.id), TIMEOUT);
			if (StringUtils.isNotEmpty(lastEventId)) {
				id = Long.valueOf(lastEventId);
				init = false;
			} else {
				id = stream.getLastEventId();
			}

			Logger.debug("[UpdateService] Update Service call from user [%s] with last event id: %s", connectedUser, id);

			Map<String, Object> u = new HashMap<String, Object>();
			u.put("lastEventId", id);

			if (!init) {
				try {
					List<Event> events = await(stream.nextEvents(id), TIMEOUT);

					if (Logger.isDebugEnabled()) {
						for (Event event : events) {
							Logger.debug("[UpdateService] Delivering %s to user [%s]", event.getType(), connectedUser);
						}
					}

					id = events.get(events.size() - 1).getId();
					u.put("events", events);
					u.put("lastEventId", id);
					u.putAll(getUnreadCounter());

					renderJSON(u, new EventSerializer(), new NotificationSerializer(), new MinimalUserSerializer(),
							new UserNotificationSerializer(), new ConversationsSerializer(connectedUser));
				} catch (TimeoutException e) {
					u.putAll(getUnreadCounter());
					renderJSON(u);
				}
			} else {
				u.putAll(getUnreadCounter());
				renderJSON(u);
			}
		} catch (TimeoutException e1) {
			error("[UpdateService] Could not init EventStream");
		}
	}

	@Util
	public static Map getUnreadCounter() {
		Map<String, Object> fuzzy = new HashMap<String, Object>();

		/* Get unread messages and notifications for the current user. */
		User user = UserLoader.getConnectedUser();
		fuzzy.put("unreadMessages", user.getUnreadConversationCount());
		fuzzy.put("unreadNotifications", user.getUnreadNotificationsCount());

		/*
		 * Get unread posts for the current user. Therefore we simply check the activity stream for unread posts. For
		 * external users this is always zero, since the don't have an activity stream.
		 */
		long unreadPosts = (!user.external) ? user.getNewActivityStream(false).size() : 0;
		fuzzy.put("unreadPosts", unreadPosts);

		return fuzzy;
	}

	protected static <T> T await(Promise<T> future, String timeout) throws TimeoutException {
		Either<T, Timeout> result = await(Promise.waitEither(future, new Timeout(timeout)));

		for (Timeout t : Matcher.ClassOf(Timeout.class).match(result._2)) {
			if (!future.cancel(true)) {
				Logger.warn("[UpdateService] Future could not be canceled after timeout");
			}
			throw new TimeoutException(t.delay);
		}

		return result._1.get();
	}

	public static class TimeoutException extends Exception {
		public TimeoutException(long delay) {
			super(String.format("[UpdateService] Request timed out after %d seconds", delay));
		}
	}

	@Util
	public static long getTimeoutAsMillis() {
		return Time.parseDuration(TIMEOUT) * 1000L;
	}

	/*
	 * We clear the cookies here so that logged out users, who are waiting for a long-polling request, are not re-logged
	 * in.
	 */
	@Finally
	static void clearCookieHeaders() {
		response.cookies.clear();
	}
}

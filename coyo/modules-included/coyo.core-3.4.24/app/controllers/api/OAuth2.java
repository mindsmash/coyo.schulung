package controllers.api;

import acl.Permission;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.GsonBuilder;
import controllers.Auth;
import controllers.Security;
import csrf.CheckAuthenticity;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.UserDeviceSerializer;
import models.User;
import models.UserDevice;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import play.Play;
import play.db.jpa.Transactional;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import session.UserLoader;

import java.util.Date;
import java.util.List;

/**
 * Oauth 2.0 controller endpoint and utility class.
 */
public class OAuth2 extends Controller {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(OAuth2.class);

    public static final int TOKEN_LIFETIME = Integer
            .parseInt(Play.configuration.getProperty("coyo.oauth2.token.lifetime", "3600000"));

    public static final String GRANT_TYPE_PASSWORD = "password";
    public static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    public static final String ACCESS_TOKEN = "access_token";
    public static final String SCOPE = "scope";
    public static final String SCOPE_ALL = "all";
    public static final String TOKEN_TYPE = "token_type";
    public static final String TOKEN_TYPE_BEARER = "bearer";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String AUTHORIZATION_HEADER = "authorization";
    public static final String EXPIRES_IN = "expires_in";

    /**
     * Endpoint to register new user devices where each device will get both an oauth2 access and refresh token.
     * The access token has a configurable lifetime and a new one can be obtained following the oauth specs using the
     * provided refresh token.
     * <p/>
     * New devices are registered using grant_type password. When registering new devices the user must provide a valid
     * username and password combination as also a device name. the device description is optional.
     * Devices can only get registered once.
     * The access and refresh token will be delivered within the requests response.
     * <p/>
     * New access tokens can be acquired using the grant_type refresh_token where a valid refresh token must be
     * provided.
     * <p/>
     * See also: https://tools.ietf.org/html/rfc6749
     *
     * @param grant_type         String currently supported are password or refresh_token
     * @param refresh_token      String the refresh token used for grant_type refresh_token
     * @param username           String the username for grant_type password
     * @param password           String the password for grant_type password
     * @param device_name        String the name of the device that should get registered when using grant_type password
     * @param device_description String the device description text
     */
    @Transactional
    public static void oauth2(final String grant_type, final String refresh_token, final String username,
                              final String password, final String device_name, final String device_description) {
        // new device and access token acquisition
        if (StringUtils.equals(grant_type, GRANT_TYPE_PASSWORD)) {
            if (StringUtils.isAnyEmpty(username, password, device_name)) {
                renderOAuth2Error("invalid_request", "missing arguments");
            }

            if (!Auth.getAuthenticator().authenticate(username, password)) {
                renderOAuth2Error("access_denied", "authentication failed");
            }
            final User user = User.findById((Long) Auth.getAuthenticator().getId(username));
            final UserDevice userDevice = createUserDevice(user, device_name, device_description);

            renderOAuth2Response(userDevice);

            // refresh access token for device
        } else if (StringUtils.equals(grant_type, GRANT_TYPE_REFRESH_TOKEN)) {
            if (StringUtils.isEmpty(refresh_token)) {
                renderOAuth2Error("invalid_request", "missing arguments");
            }

            final UserDevice device = UserDevice.find("refreshToken = ?", refresh_token).first();
            if (device == null) {
                renderOAuth2Error("invalid_request", "invalid refresh token");
            }
            device.accessToken = Codec.UUID();
            device.accessTokenCreated = new Date();
            device.save();

            renderOAuth2Response(device);
        }

        renderOAuth2Error("invalid_request", "unsupport grant_type");
    }

	/**
	 * Creates a new {@link UserDevice}. Which owns a OAuth access and refresh token
	 *
	 * @param user the owner of the device. See {@link User}
	 * @param deviceName the name of the device (must be unique for the users)
	 * @param description the description text for the user device
	 *
	 * @return the new {@link UserDevice}
	 */
	public static UserDevice createUserDevice(final User user, final String deviceName, final String description) {
		// avoid duplicate device
		if (UserDevice.count("user = ? AND name = ?", user, deviceName) > 0) {
			renderOAuth2Error("invalid_request", "device already registered");
		}
		final UserDevice device = new UserDevice();
		device.user = user;
		device.name = deviceName;
		device.description = description;

		final String accessToken = Codec.UUID();
		device.accessToken = accessToken;
		device.accessTokenCreated = new Date();

		final String refreshToken = Codec.UUID();
		device.refreshToken = refreshToken;

		device.save();

		return device;
	}

    private static void renderOAuth2Response(UserDevice device) {
        ObjectNode o = JsonNodeFactory.instance.objectNode();
        o.put(ACCESS_TOKEN, device.accessToken);
        o.put(TOKEN_TYPE, TOKEN_TYPE_BEARER);
        o.put(REFRESH_TOKEN, device.refreshToken);
        o.put(EXPIRES_IN, TOKEN_LIFETIME);
        o.put(SCOPE, SCOPE_ALL);
        renderJSON(o.toString());
    }

    private static void renderOAuth2Error(String error, String description) {
        response.status = 500;
        ObjectNode o = JsonNodeFactory.instance.objectNode();
        o.put("error", error);
        o.put("error_description", description);
        renderJSON(o.toString());
    }

    /**
     * Looks if with the current HTTP request a valid oauth2 access token was provided.
     *
     * @return boolean true if a valid token was found else false
     */
    @Util
    public static boolean checkAccessToken() {
        final User user = getOAuthUser();
        if (user != null) {
            request.args.put("user", user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Tries to load a user object using a oauth access token provided with the current requests authorization header.
     *
     * @return User the user mapped to that authorization header
     */
    public static User getOAuthUser() {
        if (!request.headers.containsKey(AUTHORIZATION_HEADER)) {
            log.debug("No authorization header found");
            return null;
        }

        final String header = request.headers.get(AUTHORIZATION_HEADER).value();
        if (StringUtils.isEmpty(header)) {
            log.debug("No authorization header found");
            return null;
        }

        final String[] headerValues = header.split("\\s+");
        if (headerValues.length < 2) {
            log.debug("Not enough header values found");
            return null;
        }

        if (!StringUtils.equalsIgnoreCase(TOKEN_TYPE_BEARER, headerValues[0].trim())) {
            log.debug("Invalid authorization type");
            return null;
        }

        final String accessToken = headerValues[1].trim();
        if (StringUtils.isEmpty(accessToken)) {
            log.debug("No access token found in request");
            return null;
        }

        final UserDevice device = UserDevice.find("accessToken = ?", accessToken).first();
        if (device == null || !new DateTime(device.accessTokenCreated).plusSeconds(TOKEN_LIFETIME).isAfterNow()) {
            log.debug("Access token not valid");
            return null;
        }

	    log.debug("Valid acces token found for user: " + device.user.id);

        return device.user;
    }

	/**
	 * Find all {@link UserDevice} object belonging to the current user.
	 */
	@Transactional
	@CheckAuthenticity
	public static void getUserDevices() {
		final List<UserDevice> userDevices = UserDevice.find("user = ?", UserLoader.getConnectedUser()).fetch();
		renderJSON(createBuilder().create().toJson(userDevices));
	}

	/**
	 * Deletes an oauth device for the current user.
	 *
	 * @param id the id of the {@link UserDevice}
	 */
	@Transactional
	@CheckAuthenticity
	public static void deleteDevice(Long id) {
		final UserDevice userDevice = UserDevice.findById(id);
		if (!Security.check(Permission.EDIT_OAUTH2_DEVICE, userDevice)) {
			forbidden();
		} else {
			userDevice.delete();
		}
	}

    /**
     * Deletes all oauth devices for a specific user.
     */
	@Transactional
	@CheckAuthenticity
	public static void deleteDevices(final Long id) {
        final User user = User.findById(id);
        notFoundIfNull(id);

        final List<UserDevice> userDevices = UserDevice.find("user = ?", user).fetch();
        for (UserDevice userDevice : userDevices) {
            if (!Security.check(Permission.EDIT_OAUTH2_DEVICE, userDevice)) {
                forbidden();
            } else {
                userDevice.delete();
            }
        }
    }

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapter(UserDevice.class, new UserDeviceSerializer())
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}
}

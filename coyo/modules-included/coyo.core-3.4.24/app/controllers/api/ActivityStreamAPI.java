package controllers.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.comments.Comment;
import models.User.ActivityStreamSelect.DisplayType;
import models.wall.post.Post;
import models.wall.post.SimplePost;
import play.db.jpa.Transactional;
import session.UserLoader;
import controllers.ActivityStream;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;

/**
 * Handles all actions to access the Activity Strem via API call.
 * 
 * @author Jan Marquardt
 */
public class ActivityStreamAPI extends API {

	/**
	 * Return all {@link ActivityStream} elements.
	 * 
	 * @param limit
	 */
	public static void stream(Integer limit) {
		if (limit == null) {
			limit = 50;
		}

		List<Object> data = new ArrayList<Object>();
		List<Post> as = UserLoader.getConnectedUser().getActivityStream(DisplayType.NATURAL, null, limit);
		for (Post post : as) {
			data.add(convertPost(post));
		}
		renderJSON(data);
	}

	/**
	 * Creates an post on the {@link ActivityStream}.
	 * 
	 * @param message
	 */
	@Transactional
	@CheckAuthenticity
	public static void post(String message) {
		Post post = new SimplePost();
		post.author = UserLoader.getConnectedUser();
		post.message = message;
		post.wall = UserLoader.getConnectedUser().getDefaultWall();
		post.save();

		renderJSON(convertPost(post));
	}

	/**
	 * Return the number of elements in the {@link ActivityStream}.
	 */
	@AllowExternals
	@ShowExternals
	public static void count() {
		renderJSON(UserLoader.getConnectedUser().getNewActivityStream(false).size());
	}

	/**
	 * Converts an {@link Post} to an JSON friendly format.
	 * 
	 * @param post
	 * @return
	 */
	protected static Map<String, Object> convertPost(Post post) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("id", post.id);
		data.put("date", post.created.getTime());
		data.put("author", post.author.getDisplayName());
		data.put("message", post.message);

		List<Object> comments = new ArrayList<Object>();
		for (Comment comment : post.comments) {
			comments.add(convertComment(comment));
		}
		data.put("comments", comments);

		return data;
	}

	/**
	 * 
	 * Converts an {@link Comment} to an JSOn freindly format.
	 * 
	 * @param comment
	 * @return
	 */
	protected static Map<String, Object> convertComment(Comment comment) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("id", comment.id);
		data.put("date", comment.created.getTime());
		data.put("author", comment.author.getDisplayName());
		data.put("message", comment.text);
		return data;
	}
}

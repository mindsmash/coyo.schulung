package controllers.api;

import java.util.Date;

import json.Dates;
import json.MinimalUserSerializer;
import models.Likable;
import models.Sender;
import models.User;
import models.statistics.TrackingRecord;
import play.db.jpa.Transactional;
import session.UserLoader;
import acl.Permission;

import com.google.gson.GsonBuilder;

import controllers.Cache;
import controllers.Collaboration;
import controllers.Security;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import events.type.LikeEvent;

/**
 * Likes REST API.
 * 
 * Old style likes handling (with global binder) can be found and used in class {@link Collaboration}
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@AllowExternals
public class LikesAPI extends API {

	/**
	 * Likes / dislikes given likable.
	 * 
	 * @param id
	 * @param clazz
	 */
	@Transactional
	@CheckAuthenticity
	public static void toggle(Long id, String clazz) {
		notFoundIfNull(id);
		notFoundIfNull(clazz);
		
		Likable likable = Collaboration.loadLikable(id, clazz);
		notFoundIfNull(likable);

		Security.checkAndCancel(Permission.LIKE, likable);

		if (likable.likes(UserLoader.getConnectedUser())) {
			likable.unlike(UserLoader.getConnectedUser());
		} else {
			likable.like(UserLoader.getConnectedUser());
			TrackingRecord.trackOne("likes");
		}

		// live inform interested users
		for (User interested : likable.getInterestedUsers()) {
			new LikeEvent(id, clazz).raise(interested.id);
		}

		renderLikes(likable);
	}

	/**
	 * Renders the list of users who have liked given likable.
	 * 
	 * @param id
	 * @param clazz
	 */
	public static void get(Long id, String clazz) {
		notFoundIfNull(id);
		notFoundIfNull(clazz);
		
		Likable likable = Collaboration.loadLikable(id, clazz);
		notFoundIfNull(likable);

		Security.checkAndCancel(Permission.LIST_LIKES, likable);

		renderLikes(likable);
	}
	
	private static void renderLikes(Likable likable) {
		notFoundIfNull(likable);
		Cache.cacheEntity(likable.getModel(), UserLoader.getConnectedUser().getPrivateCacheKey());
		renderJSON(createBuilder(likable).create().toJson(likable.getLikes()));
	}

	private static GsonBuilder createBuilder(Likable likable) {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}
}

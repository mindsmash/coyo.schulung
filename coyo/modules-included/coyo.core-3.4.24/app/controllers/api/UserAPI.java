package controllers.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.BookmarkSerializer;
import json.CalendarEventSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.PageSerializer;
import json.UserProfileSerializer;
import json.UserPushDeviceSerializer;
import json.WorkspaceSerializer;
import models.Sender;
import models.Settings;
import models.User;
import models.UserGroup;
import models.UserPushDevice;
import models.UserRole;
import models.bookmark.Bookmark;
import models.dao.BookmarkDao;
import models.event.Event;
import models.page.Page;
import models.workspace.Workspace;
import onlinestatus.OnlineStatusService;
import onlinestatus.OnlineStatusServiceFactory;
import onlinestatus.OnlineStatusSessionStore;
import onlinestatus.UserOnlineStatus.OnlineStatus;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.validation.Email;
import play.data.validation.Error;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import util.Util;
import acl.Permission;
import binding.JsonBody;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import conf.ApplicationSettings;
import controllers.Cache;
import controllers.Security;
import controllers.Users.FilterFieldOption;
import controllers.external.annotations.AllowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;

/**
 * Handles all actions to access a Sender via API
 *
 * @author Piers Wermbter
 */
public class UserAPI extends API {

	private static final Logger LOG = LoggerFactory.getLogger(UserAPI.class);

	private static final String FILTER_FIELD_ONLINE = "onlinestatus";

	public class MinimalUser {
		// required
		@Required
		public String firstName;

		@Required
		public String lastName;

		@Required
		@Email
		public String email;

		@Required
		public String password;

		// optional
		public Long groupId;
		public Long roleId;
	}

	@AllowExternals
	public static void current() {
		renderJSON(UserLoader.getConnectedUser(), new UserProfileSerializer());
	}

	@Secure(permissions = Permission.ACCESS_USER, params = "id")
	public static void getUser(Long id) {
		notFoundIfNull(id);
		User user = User.findById(id);
		notFoundIfNull(user);

		renderJSON(user, new UserProfileSerializer());
	}

	/**
	 * REST-API method to create a new user. The method returns a JSON representation of the user in case of success. If
	 * an error occurs the error message is returned as JSON.
	 * 
	 * The newly created user will have the default timezone set and has to choose a new password upon first login.
	 * 
	 * @param user
	 *            the data for creating a new user. The object must contain the following parameters: "firstName",
	 *            "lastName", "email", "password". Optionally the parameters "groupId" and "roleId" can be passed to
	 *            assign the new user to a group or a role. If no IDs where passed, the user will be assigned to the
	 *            default group / default role if existing.
	 */
	@Transactional
	@Secure(permissions = Permission.ADMIN_USER)
	public static void createUser(@JsonBody @Valid MinimalUser user) {
		if (user == null) {
			renderError("Could not create user: Params are empty.");
		}

		/* check whether an user with the same email address already exists */
		((Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
		User existingUser = User.find("byEmail", user.email).first();
		if (existingUser != null && existingUser.isPersistent()) {
			Validation.addError("email", Messages.get("user.email.alreadyInUse"));
		}
		((Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);

		/* validate passed parameters */
		if (Validation.current().hasErrors()) {
			renderError(Validation.current().errors());
		}

		try {
			Object[] args = { user.firstName + " " + user.lastName, user.email };
			LOG.info("[UserAPI] Creating new user with name [{}] and email [{}]", args);
			User newUser = new User();
			newUser.setFirstName(user.firstName);
			newUser.setLastName(user.lastName);
			newUser.email = user.email;
			newUser.setPassword(user.password);
			newUser.timezone = Settings.findApplicationSettings().getString(ApplicationSettings.DEFAULT_TIMEZONE);
			newUser.resetPassword = true;

			/* assign user to a role. Either the one passed as a parameter or the default role. */
			UserRole role = (user.roleId != null) ? (UserRole) UserRole.findById(user.roleId) : UserRole
					.findDefaultRole();
			if (role != null) {
				LOG.info("[UserAPI] Assigning role [{}] to new user.", role.name);
				newUser.role = role;
			}
			newUser.save();

			/* add the user to a group. Either the one passed as a parameter or the default group */
			UserGroup group = (user.groupId != null) ? (UserGroup) UserGroup.findById(user.groupId) : UserGroup
					.findDefaultGroup();
			if (group != null) {
				LOG.info("[UserAPI] Adding new user to group [{}].", group.name);
				group.addUser(newUser);
			}

			/* in case of success return the newly created user */
			renderJSON(createBuilder().create().toJson(newUser));
		} catch (Exception e) {
			LOG.error("[UserAPI] Could not create user.", e);
			renderError("Could not create user: " + e.toString());
		}
	}

	/**
	 * Returns a list of all users ordered by their last name ascending. Users can be filtered by either a valid filter
	 * field from {@link FilterFieldOption} or by "onlinestatus". The first allows to specify any text as filterQuery,
	 * whereas the latter expects a valid online status like "ONLINE", "OFFLINE", "BUSY" or "AWAY". If the specified
	 * filter is invalid or none is set, the whole list is returned along with the amount of all users. The list of
	 * users can be paged by specifying the parameters "page" and "length". The parameters for filterField and
	 * filterQuery are case insensitive.
	 */
	public static void list(final String filterField, final String filterQuery) {
		boolean isFilterSet = StringUtils.isNotEmpty(filterField) && StringUtils.isNotEmpty(filterQuery);
		boolean filtered = false;
		Map<String, Object> output = new HashMap<String, Object>();
		JpqlSelect select = new JpqlSelect();
		select.orderBy("lastName,firstName");
		select.where("active = true");

		/* filter by defined user fields */
		if (isFilterSet && EnumUtils.isValidEnum(FilterFieldOption.class, filterField.toUpperCase())) {
			LOG.debug("[UserAPI] Fetching user list with with field '{}' and term '{}'", filterField, filterQuery);
			if (filterField.equalsIgnoreCase(FilterFieldOption.NAME.getFieldName())) {
				// all name terms must match
				List<String> terms = Util.parseList(filterQuery, " ");
				for (String string : terms) {
					String term = "%" + string.toLowerCase() + "%";
					select.andWhere("(LOWER(lastName) LIKE ? OR LOWER(firstName) LIKE ?)").params(term, term);
				}
			} else {
				select.andWhere("LOWER(" + filterField + ") LIKE ?").params("%" + filterQuery.toLowerCase() + "%");
			}
			filtered = true;
		}

		List<User> users = User.find(select.toString(), select.getParams().toArray()).fetch();

		/* filter by online status */
		if (isFilterSet && filterField.equalsIgnoreCase(FILTER_FIELD_ONLINE)
				&& EnumUtils.isValidEnum(OnlineStatus.class, filterQuery.toUpperCase())) {
			LOG.debug("[UserAPI] Fetching user list with with field '{}' and term '{}'", FILTER_FIELD_ONLINE,
					filterQuery);
			Iterable<User> iterable = Iterables.filter(users, new Predicate<User>() {
				@Override
				public boolean apply(User user) {
					return user.getOnlineStatus().status.equals(OnlineStatus.valueOf(filterQuery));
				}
			});
			users = Lists.newArrayList(iterable);
			filtered = true;
		}

		/* log a warning if a filter was set, but none was applied. */
		if (isFilterSet && filtered == false) {
			LOG.warn("[UserAPI] User list is not filtered: Invalid field '{}' or term '{}' specified.", filterField,
					filterQuery);
		}

		EndlessHelper.prepare(users, true);
		output.put("users", renderArgs.get("data"));
		output.put("allUsersCount", users.size());
		renderJSON(output, new UserProfileSerializer());
	}

	/**
	 * Returns a list of users the current user is following. The list only contains active users. An empty list is
	 * returned, if the current user is not following any other users.
	 */
	public static void colleagues() {
		/*
		 * Not currently cached because the colleagues' online status is part of the response.
		 * Caching this would require the online status to be loaded uncached from elsewhere.
		 * 
		 * Cache.cache(UserLoader.getConnectedUser().modified.getTime());
		 */
		
		renderJSON(createBuilder().create().toJson(UserLoader.getConnectedUser().getRealFollowing()));
	}

	/**
	 * Returns a list of active workspaces the users is a member of.
	 */
	@AllowExternals
	public static void workspaces() {
		User user = UserLoader.getConnectedUser();
		List<Workspace> workspaces = user.getActiveWorkspaces(null);
		Cache.cacheEntities(workspaces, user.getPrivateCacheKey());
		renderJSON(createBuilder().create().toJson(workspaces));
	}

	/**
	 * Returns a list of active pages the users is following.
	 */
	public static void pages() {
		User user = UserLoader.getConnectedUser();
		List<Page> pages = user.getPages(null);
		Cache.cacheEntities(pages, user.getPrivateCacheKey());
		renderJSON(createBuilder().create().toJson(pages));
	}

	/**
	 * Returns a list of upcoming events for the user.
	 */
	public static void events() {
		User user = UserLoader.getConnectedUser();
		List<Event> events = user.getMyEvents();
		Cache.cacheEntities(events, user.getPrivateCacheKey());
		renderJSON(createBuilder().create().toJson(events));
	}

	/**
	 * Returns a list of bookmarks for the user.
	 */
	public static void bookmarks() {
		User user = UserLoader.getConnectedUser();
		List<Bookmark> bookmarks = BookmarkDao.getBookmarks(user);
		Cache.cacheEntities(bookmarks, user.getPrivateCacheKey());
		renderJSON(createBuilder().create().toJson(bookmarks));
	}

	/**
	 * Adds a personal bookmark for the current user
	 */
	@Transactional
	@CheckAuthenticity
	public static void addBookmark(@Valid @JsonBody Bookmark bookmark) {
		if (Validation.hasErrors()) {
			badRequest();
		}

		User user = UserLoader.getConnectedUser();
		bookmark.sender = user;
		bookmark.save();
		List<Bookmark> bookmarks = BookmarkDao.getBookmarks(user);
		renderJSON(createBuilder().create().toJson(bookmarks));
	}

	/**
	 * Deletes a personal bookmark of the current user. Whether the user is the owner of the bookmark is checked via
	 * permissions.
	 */
	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.DELETE_BOOKMARK, params = "id")
	public static void deleteBookmark(Long id) {
		Bookmark bookmark = Bookmark.findById(id);
		notFoundIfNull(bookmark);
		bookmark.delete();
	}

	/**
	 * Lists all push devices of the current user.
	 */
	public static void getPushDevices() {
		renderJSON(createBuilder().create().toJson(UserLoader.getConnectedUser().pushDevices));
	}

	/**
	 * Lists a single push device of the current user.
	 */
	public static void getPushDevice(Long id) {
		notFoundIfNull(id);
		UserPushDevice device = UserPushDevice.find("user = ? AND id = ?", UserLoader.getConnectedUser(), id).first();
		notFoundIfNull(device);
		renderJSON(createBuilder().create().toJson(device));
	}

	/**
	 * Creates or saves a push device for the current user.
	 */
	@Transactional
	@CheckAuthenticity
	public static void savePushDevice(@Valid @JsonBody UserPushDevice device) {
		if (device.isPersistent()) {
			Security.checkAndCancel(Permission.EDIT_PUSH_DEVICE, device.id);
		} else {
			// try to find existing by attributes, not by ID
			UserPushDevice existing = UserPushDevice.find("type = ? AND token = ?", device.type, device.token).first();
			if (existing != null) {
				if (existing.user != UserLoader.getConnectedUser()) {
					forbidden();
				} else {
					// bind data
					existing.active = device.active;
					device = existing;
				}
			}
		}

		User user = UserLoader.getConnectedUser();
		device.user = user;
		device.save();
		user.refresh();

		renderJSON(createBuilder().create().toJson(user.pushDevices));
	}

	/**
	 * Deletes one of the current user's push devices.
	 */
	@Transactional
	@CheckAuthenticity
	public static void deletePushDevice(Long id) {
		notFoundIfNull(id);
		UserPushDevice device = UserPushDevice.find("user = ? AND id = ?", UserLoader.getConnectedUser(), id).first();
		notFoundIfNull(device);
		device.delete();
	}

	@AllowExternals
	public static void getOnlineStatus() {
		renderJSON(OnlineStatusServiceFactory.getOnlineStatusService().getOnlineStatus(UserLoader.getConnectedUser()));
	}

	@AllowExternals
	@CheckAuthenticity
	@Secure(permissions = Permission.ACCESS_USER, params = "id")
	public static void getOnlineStatusOf(Long id) {
		User user = User.findById(id);
		notFoundIfNull(user);

		renderJSON(OnlineStatusServiceFactory.getOnlineStatusService().getOnlineStatus(user));
	}

	@CheckAuthenticity
	public static void setOnlineStatus(@JsonBody OnlineStatus status) {
		OnlineStatusService service = OnlineStatusServiceFactory.getOnlineStatusService();
		service.setOnlineStatus(UserLoader.getConnectedUser(), status);
		/* store new status in session */
		OnlineStatusSessionStore.storeStatus(status);
		renderJSON(service.getOnlineStatus(UserLoader.getConnectedUser()));
	}

	@Transactional
	@Secure(permissions = Permission.ADMIN_USER)
	@CheckAuthenticity
	public static void deleteUserByAuthUID(final String authUID, final String source) {
		final GenericModel.JPAQuery query = User.find("authSource like :source AND authUid like :authId");
		query.setParameter(":authId", authUID);
		query.setParameter(":source", source);

		final List<User> users = query.fetch();

		if (users.size() > 1) {
			error(String.format("For authSource '%s' and authUid '%s' more than one user was found.", source, authUID));

		} else if (users.isEmpty()) {
			error(String.format("For authSource '%s' and authUid '%s' we found no user.", source, authUID));

		} else {
			final User user = users.get(0);
			user.delete();
		}
	}

	// TODO dongusger addd error txt for rest response
	@Transactional
	@Secure(permissions = Permission.ADMIN_USER)
	@CheckAuthenticity
	public static void deleteUserByID(final Long id) {
		final User user = User.findById(id);
		if (null == user) {
			error(String.format("For id '%d' no user was found.", id));
		} else {
			user.delete();
		}
	}

	private static void renderError(String message) {
		JsonObject errorResult = new JsonObject();
		errorResult.addProperty("error", message);
		renderJSON(errorResult);
	}

	private static void renderError(List<Error> errors) {
		JsonObject errorResult = new JsonObject();
		JsonObject errorList = new JsonObject();
		for (Error error : errors) {
			errorList.addProperty(error.getKey(), error.message());
		}
		errorResult.add("error", errorList);
		renderJSON(errorResult);
	}

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapter(UserPushDevice.class, new UserPushDeviceSerializer())
				.registerTypeAdapter(Workspace.class, new WorkspaceSerializer())
				.registerTypeAdapter(Bookmark.class, new BookmarkSerializer())
				.registerTypeHierarchyAdapter(Page.class, new PageSerializer())
				.registerTypeHierarchyAdapter(Event.class, new CalendarEventSerializer())
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}

}

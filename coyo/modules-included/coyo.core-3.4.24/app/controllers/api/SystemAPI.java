package controllers.api;

import java.util.HashMap;
import java.util.Map;

import models.Settings;
import play.mvc.Router;

/**
 * Handles the public API.
 * 
 * @author mindsmash GmbH
 * 
 */
public class SystemAPI extends API {

	/**
	 * Returns the application version as JSON.
	 */
	public static void info() {
		Map<String, String> data = new HashMap<String, String>();
		Settings themeSettings = Settings.findOrCreate("theme");
		Settings settings = Settings.findApplicationSettings();
		data.put("color", themeSettings.getString("themeBaseColor").replace("#", ""));
		data.put("subcolor", themeSettings.getString("themeSecondaryColor").replace("#", ""));
		data.put("themeStyle", themeSettings.getString("themeStyle"));
		data.put("coyoName", settings.getString("applicationName"));
		data.put("messaging", new Boolean(settings.getBoolean("messaging")).toString());
		Map args = new HashMap<String, String>();
		args.put("small", "true");
		data.put("logo", Router.reverse("open.Application.logo", args).toString());
		renderJSON(data);
	}
}

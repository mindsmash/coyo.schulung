package controllers.api;

import controllers.Cache;
import utils.ImageUtils;
import json.FileAttachmentSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.SettingsSerializer;

import com.google.gson.GsonBuilder;

import models.FileAttachment;
import models.Settings;
import models.User;
import models.teaser.TeaserItem;

public class TeaserAPI extends API {
	
	public static final String TEASER_SETTINGS_KEY = "teaser";
	public static final String TEASER_HEIGHT = "height";
	public static final String TEASER_HEIGHT_DEFAULT = "320";
	public static final String TEASER_INTERVAL = "interval";
	public static final String TEASER_INTERVAL_DEFAULT = "8000";
	
	public static void getTeasers() {
		renderJSON(createBuilder().create().toJson(TeaserItem.find("active = true ORDER BY priority ASC").fetch()));
	}
	
	public static void getTeaserSettings() {
		Settings settings = models.Settings.findOrCreate(TEASER_SETTINGS_KEY);
		boolean changed = false;
		if (!settings.contains(TEASER_INTERVAL)) {
			settings.setProperty(TEASER_INTERVAL, TEASER_INTERVAL_DEFAULT);
			changed = true;
		}
		if (!settings.contains(TEASER_HEIGHT)) {
			settings.setProperty(TEASER_HEIGHT, TEASER_HEIGHT_DEFAULT);
			changed = true;
		}
		
		if (changed) {
			settings.save();
		}
		renderJSON(createBuilder().create().toJson(settings));
	}
	
	public static void getTeaserImage(Long id) {
		TeaserItem item = TeaserItem.findById(id);
		notFoundIfNull(item);
		
		if ((item.image == null) || !ImageUtils.isImage(item.image)) {
			badRequest();
		}

		Cache.cache(item.modified.getTime());

		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}
		renderBinary(item.image.get(), item.title, item.image.length(), item.image.type(), true);
	}
	
	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(FileAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		builder.registerTypeAdapter(Settings.class, new SettingsSerializer());
		return builder;
	}

}

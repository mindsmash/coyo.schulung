package controllers.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Sender;
import models.Settings;
import models.User;
import models.app.App;
import models.page.Page;
import models.workspace.Workspace;
import play.i18n.Messages;
import play.mvc.Router;
import session.UserLoader;
import conf.ApplicationSettings;

/**
 * Handles all actions to access the Menu via API
 * 
 * @author Piers Wermbter
 */
public class NavigationAPI extends API {

	public static class NavItem {
		public String title;
		public String link;
		public String icon;

		public NavItem(String title, String link, String icon) {
			this.icon = icon;
			this.link = link;
			this.title = title;
		}
	}

	public static class NavCategory {
		public String title;
		public List<NavItem> items;

		public NavCategory(String title) {
			this.title = title;
			this.items = new ArrayList<NavItem>();
		}
	}

	// @CheckAuthenticity
	public static void index() {
		User user = UserLoader.getConnectedUser();
		Settings settings = Settings.findApplicationSettings();

		Map<String, List> output = new HashMap<String, List>();
		List<NavCategory> menu = new ArrayList<NavCategory>();

		NavCategory coyo = new NavCategory("Coyo");
		coyo.items.add(new NavItem(Messages.get("nav.pages"), "/pages", "book"));
		if (settings.getBoolean(ApplicationSettings.WORKSPACES)) {
			coyo.items.add(new NavItem(Messages.get("nav.workspaces"), "/workspaces", "show_big_thumbnails"));
		}

		coyo.items.add(new NavItem(Messages.get("nav.events"), "/calendar", "calendar"));
		menu.add(coyo);

		NavCategory pages = new NavCategory(Messages.get("pages.my"));
		for (Page actPage : user.getPages(30)) {
			if (actPage.getDefaultApp() == null) {
				continue; // only consider fully configured pages
			}
			Map args = new HashMap<String, String>();
			args.put("id", actPage.id.toString());
			args.put("appId", actPage.getDefaultApp().id.toString());
			args.put("slug", actPage.getSlug());
			args.put("appSlug", actPage.getDefaultApp().getSlug());
			pages.items.add(new NavItem(actPage.name, Router.reverse("Pages.show", args).url, "book"));
		}

		if (pages.items.size() > 0) {
			menu.add(pages);
		}

		if (settings.getBoolean(ApplicationSettings.WORKSPACES)) {
			NavCategory workspaces = new NavCategory(Messages.get("workspaces.my"));
			for (Workspace workspace : user.getActiveWorkspaces(30)) {
				if (workspace.getDefaultApp() == null) {
					continue; // only consider fully configured workspaces
				}
				Map args = new HashMap<String, String>();
				args.put("id", workspace.id.toString());
				args.put("appId", workspace.getDefaultApp().id.toString());
				args.put("slug", workspace.getSlug());
				args.put("appSlug", workspace.getDefaultApp().getSlug());
				workspaces.items.add(new NavItem(workspace.name, Router.reverse("Workspaces.show", args).url,
						"show_big_thumbnails"));
			}

			if (workspaces.items.size() > 0) {
				menu.add(workspaces);
			}
		}

		output.put("structure", menu);
		renderJSON(output);
	}

	public static void appList(long id) {
		Map<String, Object> output = new HashMap<String, Object>();
		List<NavItem> apps = new ArrayList<NavItem>();
		Sender sender = Sender.findById(id);

		if (sender instanceof Workspace || sender instanceof Page) {
			for (App app : sender.getActiveApps()) {
				Map args = new HashMap<String, String>();
				args.put("id", sender.id.toString());
				args.put("appId", app.id.toString());
				if (sender.getSenderType().equals("workspace")) {
					apps.add(new NavItem(app.title, Router.reverse("Workspaces.show", args).url, app.getAppDescriptor()
							.getIconClass()));
				} else {
					apps.add(new NavItem(app.title, Router.reverse("Pages.show", args).url, app.getAppDescriptor()
							.getIconClass()));
				}
			}
			output.put("title", sender.getDisplayName());
			output.put("avatar", sender.getThumbURL());
		} else if (sender instanceof User) {
			Map args = new HashMap<String, String>();
			args.put("id", sender.id.toString());
			args.put("slug", sender.getSlug());
			apps.add(new NavItem(Messages.get("wall"), Router.reverse("Users.wall", args).url, "icon-list"));
			apps.add(new NavItem(Messages.get("users.info.title"), Router.reverse("Users.info", args).url, "icon-user"));
			apps.add(new NavItem(Messages.get("users.media.title"), Router.reverse("Users.media", args).url,
					"icon-picture"));
			output.put("title", sender.getDisplayName());
			output.put("avatar", sender.getThumbURL());
		}

		output.put("apps", apps);
		renderJSON(output);
	}
}

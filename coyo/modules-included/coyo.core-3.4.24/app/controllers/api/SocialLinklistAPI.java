package controllers.api;

import injection.Inject;
import injection.InjectionSupport;
import json.HibernateProxyTypeAdapter;
import json.SocialLinkSerializer;
import json.SocialLinklistAppSerializer;
import models.User;
import models.app.SocialLink;
import models.app.SocialLinklistApp;
import models.app.dao.SocialLinklistAppDao;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import security.Secure;
import session.UserLoader;
import util.HttpUtil;
import utils.WebLinkInfo;
import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import controllers.Security;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

@InjectionSupport
@AllowExternals
public class SocialLinklistAPI extends API {

	@Inject(configuration = "coyo.di.dao.sociallinklist", singleton = true, defaultClass = SocialLinklistAppDao.class)
	private static SocialLinklistAppDao dao;
	
	private static Logger LOG = LoggerFactory.getLogger(SocialLinklistAPI.class);
	
	public static void getMetaOfUrl(@JsonBody SocialLink link) {
		if (link != null) {
			String url = addProtocol(link.linkUrl);
			if (StringUtils.isNotEmpty(url) && validUrl(url)) {
				WebLinkInfo info = WebLinkInfo.get(url);
				link.linkUrl = url;
				link.title = info.getTitle();
				link.description = info.getDescription();
				link.faviconUrl = info.getFaviconUrl();
				if (link.title == WebLinkInfo.NO_TITLE) {
					link.title = "";
				}
				if (link.faviconUrl == null || link.faviconUrl.length() > 255) {
					link.faviconUrl = "";
				}
				renderJSON(createBuilder().create().toJson(link));
			}
		}
		JsonObject json = new JsonObject();
		json.addProperty("error", true);
		renderJSON(json);
	}

	@Transactional
	public static void save(long id, @JsonBody SocialLink link) {

		SocialLinklistApp app = SocialLinklistApp.findById(id);
		notFoundIfNull(app);

		link.app = app;

		if (!link.isPersistent()) {
			link.author = UserLoader.getConnectedUser();
			Security.checkAndCancel(AppsPermission.ADD_SOCIAL_LINK, app.id);
		} else {
			Security.checkAndCancel(AppsPermission.EDIT_SOCIAL_LINK, app.id, link.id);
		}

		if (validUrl(link.linkUrl) && link.validateAndSave()) {
			renderJSON(createBuilder().create().toJson(link));
		}

		JsonObject json = new JsonObject();
		json.addProperty("error", true);
	}

	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void getLinks(long id) {
		SocialLinklistApp app = SocialLinklistApp.findById(id);
		notFoundIfNull(app);

		if (app.linksTransferred == false) {
			for (SocialLink link : app.links) {
				addMetaToLink(link);
				app.linksTransferred = true;
				app.validateAndSave();
			}
		}
		renderJSON(createBuilder().create().toJson(app));
	}

	@Transactional
	public static void deleteLink(long id) {
		SocialLink link = SocialLink.findById(id);
		notFoundIfNull(link);
		Security.checkAndCancel(AppsPermission.EDIT_SOCIAL_LINK, link.app.id, link.id);
		link.delete();
		ok();
	}

	@CheckAuthenticity
	@Transactional
	@Secure(permissions = AppsPermission.EDIT_ALL_SOCIAL_LINKS, params = "id")
	public static void order(Long id) {
		final String json = params.getRootParamNode().getChild("body", true).getFirstValue(String.class);
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();

		int priority = 0;
		for (JsonElement element : array) {
			SocialLink link = SocialLink.findById(Long.valueOf(element.getAsLong()));
			if (link != null) {
				link.priority = priority++;
				link.save();
			}
		}
	}

	private static String addProtocol(String url) {
		if (!url.startsWith("http")) {
			return "http://" + url;
		}
		return url;
	}

	private static boolean validUrl(String url) {
		return HttpUtil.isValidUrlString(url) && HttpUtil.hasValidProtocol(url);
	}

	private static GsonBuilder createBuilder() {
		User user = UserLoader.getConnectedUser();
		return new GsonBuilder().registerTypeAdapter(SocialLink.class, new SocialLinkSerializer())
				.registerTypeAdapter(SocialLinklistApp.class, new SocialLinklistAppSerializer(user))
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}

	private static void addMetaToLink(SocialLink link) {
		if (link == null) {
			return;
		}
		String url = addProtocol(link.linkUrl);
		WebLinkInfo info = WebLinkInfo.get(url);
		link.linkUrl = url;
		link.title = info.getTitle();
		link.description = info.getDescription();
		link.faviconUrl = info.getFaviconUrl();
		if (link.title == WebLinkInfo.NO_TITLE) {
			link.title = "";
		}
		try {
			validUrl(link.linkUrl);
			link.validateAndSave();
		} catch (Exception se) {
			LOG.warn("[SocialLinkListAPI] Error while saving new link.", se);
		}
	}
}
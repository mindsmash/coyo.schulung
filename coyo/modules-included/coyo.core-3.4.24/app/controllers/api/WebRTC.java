package controllers.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import acl.Permission;
import models.User;
import play.Logger;
import play.Play;
import security.Secure;
import session.UserLoader;
import binding.JsonBody;
import controllers.external.annotations.AllowExternals;
import events.type.RTCCallEvent;
import events.type.RTCEvent;
import events.type.RTCEvent;
import events.type.RTCRespondEvent;

@AllowExternals
public class WebRTC extends API {

	public static final String STUN_SERVER = Play.configuration.getProperty("mindsmash.webrtc.stun", "");
	
	private static final String STUN_PREFIX = "stun:";
	private static final Pattern STUN_SERVER_PATTERN = Pattern.compile("([a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]+:[0-9]+");

	public enum Response {
		ACCEPTED, DECLINED, TIMEOUT, CANCEL, NOTSUPPORTED, NOTAVAILABLE
	}

	public class JsonData {
		public String data;
	}

	@Secure(permissions = Permission.IS_WEBRTC_CALLABLE, params = "id")
	public static void callUser(Long id, boolean audioOnly) {
		Logger.debug("[WebRTC] Calling user with id [%d], audio only: [%d].", id, audioOnly);
		new RTCCallEvent(UserLoader.getConnectedUser(), audioOnly).raise(id);
	}

	@Secure(permissions = Permission.ACCESS_WEBRTC_CALL, params = "id")
	public static void respond(Long id, Response response) {
		User user = UserLoader.getConnectedUser();
		Logger.debug("[WebRTC] User [%d] responds to [%d] with answer %s", user.id, id, response);
		new RTCRespondEvent(user, response).raise(id);
	}

	public static void signal(Long id, @JsonBody JsonData data) {
		Logger.debug("[WebRTC] Sending json data '%s' to user with ID [%d].", data.data, id);
		new RTCEvent(data.data).raise(id);
	}

	public static void stunServer() {
		List<String> stunServer = new ArrayList();
		for (String serverName : STUN_SERVER.split(",")) {
			if (STUN_SERVER_PATTERN.matcher(serverName).matches()) {
				stunServer.add(STUN_PREFIX + serverName);
			} else {
				Logger.warn(
						"[WebRTC] URL '%s' for STUN server set in configuration is invalid. URL must contain domain and protocol.",
						serverName);
			}
		}
		renderJSON(stunServer);
	}

}
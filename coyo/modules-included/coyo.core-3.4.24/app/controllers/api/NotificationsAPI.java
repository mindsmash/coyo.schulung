package controllers.api;

import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import events.type.DummyEvent;
import json.NotificationSerializer;
import json.UserNotificationSerializer;
import models.User;
import models.UserNotification;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import session.UserLoader;

import java.util.List;

/**
 * Handles all actions to access notifications via API call.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@AllowExternals
@ShowExternals
public class NotificationsAPI extends API {

	@Transactional
	public static void notifications() {
		final List<UserNotification> userNotifications = UserLoader.getConnectedUser().getNotifications(true, 30);
		renderJSON(userNotifications, new NotificationSerializer(), new UserNotificationSerializer());
	}

	public static void unreadNotifications() {
		renderJSON(UserLoader.getConnectedUser().getUnreadNotifications(), new NotificationSerializer(),
				new UserNotificationSerializer());
	}

	public static void unreadNotificationsCount() {
		renderJSON(UserLoader.getConnectedUser().getUnreadNotificationsCount());
	}

	/**
	 * Resets the user notification count. Therefore a current timestamp is set to the users model.
	 */
	@CheckAuthenticity
	@Transactional
	public static void resetNotificationsCount() {
		User user = UserLoader.getConnectedUser();
		user.resetUnreadNotificationCount();
		new DummyEvent().raise(user);
	}

	@CheckAuthenticity
	@Transactional
	public static void markNotificationsRead() {
		JPA.em()
				.createQuery(
						"UPDATE UserNotification un SET un.isRead = true WHERE un.isRead = false AND un.user = :user")
				.setParameter("user", UserLoader.getConnectedUser()).executeUpdate();

		new DummyEvent().raise(UserLoader.getConnectedUser());
	}
}

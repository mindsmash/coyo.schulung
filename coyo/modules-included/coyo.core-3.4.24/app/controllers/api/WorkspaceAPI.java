package controllers.api;

import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.List;

import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.WorkspaceDetailSerializer;
import json.WorkspaceListUserSerializer;
import json.WorkspaceMemberSerializer;
import lombok.extern.slf4j.Slf4j;
import models.User;
import models.dao.WorkspaceDao;
import models.notification.WorkspaceJoinNotification;
import models.notification.WorkspaceJoinReqNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.workspace.Workspace;
import models.workspace.WorkspaceCategory;
import models.workspace.WorkspaceJoinRequest;
import models.workspace.WorkspaceMember;
import play.Play;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import utils.HibernateUtils;
import acl.Permission;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import controllers.Cache;
import controllers.Security;
import controllers.Workspaces;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

@Slf4j
@InjectionSupport
public class WorkspaceAPI extends API {

	@Inject(configuration = "coyo.di.dao.workspace", defaultClass = WorkspaceDao.class, singleton = true)
	private static WorkspaceDao workspaceDao;
	
	public static final boolean SKIP_OLD_STICKY_POSTS = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.workspace.stickyPosts.skipOld.enabled", "true"));

	@AllowExternals
	@Secure(permissions = Permission.LIST_WORKSPACE, params = "id")
	public static void getMembers(Long id, int limit) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		List<WorkspaceMember> memberList = workspaceDao.getMember(workspace, limit);

		/* render members with information about total count for paging */
		GsonBuilder builder = createBuilder();
		Gson gson = builder.registerTypeAdapter(User.class, new MinimalUserSerializer()).create();
		JsonObject result = new JsonObject();
		result.addProperty("total", workspace.getMemberCount());
		result.add("memberList", gson.toJsonTree(memberList));
		renderJSON(gson.toJson(result));
	}

	/**
	 * Get all workspace categories and the amount of workspaces in this category in total. This method does not have
	 * any permission checks since everyone but externals are allowed to fetch this information. In addition permission
	 * checks are included in the methods fetching the number of workspaces.
	 */
	public static void getCategories() {
		JsonArray result = new JsonArray();
		User connectedUser = UserLoader.getConnectedUser();

		JsonObject myWorkspaces = new JsonObject();
		myWorkspaces.addProperty("id", "my");
		myWorkspaces.addProperty("name", Messages.get("workspaces.my"));
		myWorkspaces.addProperty("count", connectedUser.getActiveWorkspaceCount());
		result.add(myWorkspaces);

		JsonObject allWorkspaces = new JsonObject();
		allWorkspaces.addProperty("id", "all");
		allWorkspaces.addProperty("name", Messages.get("all"));
		allWorkspaces.addProperty("count", Workspace.count(UserLoader.getConnectedUser(false)));
		result.add(allWorkspaces);

		for (WorkspaceCategory category : WorkspaceCategory.findAllSorted()) {
			JsonObject newCategoryEntry = new JsonObject();
			newCategoryEntry.addProperty("id", category.id);
			newCategoryEntry.addProperty("name", category.name);
			newCategoryEntry.addProperty("count", workspaceDao.getWorkspaceCount(connectedUser, category.id, false));
			result.add(newCategoryEntry);
		}

		renderJSON(result);
	}

	/**
	 * Get all workspaces for a specific category and filter query. Both parameters are optional. This method does not
	 * have any permission checks since everyone but externals are allowed to fetch this information. In addition
	 * permission checks are included in the methods fetching the workspaces.
	 */
	public static void getWorkspaces(String categoryId, String filterQuery, int limit, int page) {
		User user = UserLoader.getConnectedUser();
		List<Workspace> workspaces = new ArrayList<>();

		if (categoryId.equals("my")) {
			workspaces = workspaceDao.getWorkspacesWhereUserIsMember(page, limit, user, filterQuery, false);
		} else if (categoryId.equals("all")) {
			workspaces = workspaceDao.getWorkspaces(page, limit, user, null, filterQuery, false);
		} else {
			Long catId = null;
			try {
				catId = Long.parseLong(categoryId);
				workspaces = workspaceDao.getWorkspaces(page, limit, user, catId, filterQuery, false);
			} catch (NumberFormatException e) {
				log.warn("[WorkspaceAPI] Invalid category id: {}", categoryId);
			}
		}

		/* cache list of workspaces */
		Cache.cacheEntities(workspaces, user.getPrivateCacheKey());

		GsonBuilder builder = createBuilder();
		Gson gson = builder.registerTypeAdapter(User.class, new WorkspaceListUserSerializer()).create();
		renderJSON(gson.toJson(workspaces));
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.LIST_WORKSPACE, params = "id")
	public static void joinWorkspace(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		User user = UserLoader.getConnectedUser();
		boolean joinWorkspace = false;

		if (Security.check(Permission.JOIN_WORKSPACE, id) && (!workspace.isMember(user))) {
			/* user can join directly */
			workspace.addMember(user, false);
			workspace.save();
			joinWorkspace = true;

			// notify
			WorkspaceJoinNotification.raise(workspace, user);
		} else if (!workspace.hasRequestedToJoin(user)) {
			/* request to join */
			WorkspaceJoinRequest joinRequest = new WorkspaceJoinRequest(workspace, user);
			joinRequest.save();
			workspace.joinRequests.add(joinRequest);
			workspace.save(); // invalidate caches			
			WorkspaceJoinReqNotification.raise(workspace, user);
		}	
		
		if (SKIP_OLD_STICKY_POSTS && joinWorkspace) {
			for (Wall wall : workspace.walls) {				
				List<Post> postList = wall.getPostsQuery(user).fetch();			
				for (Post post : postList) {
					if (post.isImportant(user) && !post.hasRead(user)) {
						post.toggleRead(user);	
					}
				}
			}
		}

		GsonBuilder builder = createBuilder();
		Gson gson = builder.registerTypeAdapter(User.class, new WorkspaceListUserSerializer()).create();
		renderJSON(gson.toJson(workspace));
	}

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(Workspace.class, new WorkspaceDetailSerializer())
				.registerTypeAdapter(WorkspaceMember.class, new WorkspaceMemberSerializer())
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}	
}

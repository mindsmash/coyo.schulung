package controllers.api;

import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;
import com.google.gson.GsonBuilder;
import controllers.Security;
import controllers.Uploads;
import controllers.Uploads.TempUpload;
import controllers.external.annotations.AllowExternals;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.blogapp.BlogAppSerializer;
import json.blogapp.BlogArticleSerializer;
import models.User;
import models.app.BlogApp;
import models.app.BlogAppArticle;
import models.app.dao.BlogAppDao;
import models.notification.BlogArticleNotification;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Http;
import security.Secure;
import session.UserLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllowExternals
public class BlogAppAPI extends API {

	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void info(long id) {
		BlogApp app = BlogApp.findById(id);
		notFoundIfNull(app);
		renderJSON(createBuilder().create().toJson(app));
	}

	@Deprecated
	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void articles(long id, Integer year, Integer month) {
		BlogApp app = BlogApp.findById(id);
		notFoundIfNull(app);

		final List<BlogAppArticle> list;
		if (year != null && month != null) {
			if (month < 1 || month > 12 || year > 9999) {
				list = Collections.EMPTY_LIST;
			} else {
				list = BlogAppDao.getArticlesByMonth(app, month, year);
			}

		} else {
			list = BlogAppDao.getLatestArticle(app, 20);
		}

		final List<BlogAppArticle> finalList = new ArrayList<BlogAppArticle>();
		for (BlogAppArticle article : list) {
			if (Security.check(AppsPermission.ACCESS_BLOG_ARTICLE, article)) {
				finalList.add(article);
			}
		}

		renderJSON(createBuilder().create().toJson(finalList));
	}

	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void articlesPaged(long id, int first, int max, Integer year, Integer month) {
		final BlogApp app = BlogApp.findById(id);
		notFoundIfNull(app);

		final boolean canEditSenderPermission = Security.check(Permission.EDIT_SENDER, app.sender.id);
		final List<BlogAppArticle> articles = BlogAppDao
				.getLatestArticlesPaged(app, year, month, first, max, canEditSenderPermission);

		final Map<String, Object> result = new HashMap<>();
		result.put("articles", articles);
		result.put("count", BlogAppDao.countLatestArticles(app, year, month, canEditSenderPermission));

		renderJSON(createBuilder().create().toJson(result));
	}

	@Secure(permissions = AppsPermission.ACCESS_BLOG_ARTICLE, params = "id")
	public static void article(long id) {
		BlogAppArticle article = BlogAppArticle.findById(id);
		notFoundIfNull(article);
		renderJSON(createBuilder().create().toJson(article));
	}

	@Transactional
	public static void save(long id, @JsonBody BlogAppArticle article) {
		BlogApp app = BlogApp.findById(id);
		notFoundIfNull(app);
		article.app = app;

		boolean created = false;

		if (!article.isPersistent()) {
			created = true;
			article.author = UserLoader.getConnectedUser();
			Security.checkAndRedirect(AppsPermission.EDIT_BLOG, "/", article.app.id);
		} else {
			Security.checkAndRedirect(AppsPermission.EDIT_BLOG_ARTICLE, "/", article.id);
		}

		if (!article.validateAndSave()) {
			error(Http.StatusCode.BAD_REQUEST, validation.errorsMap() + "");
		}

		if (created && article.isPublished()) {
			BlogArticleNotification.raise(article);
		}

		if (article.canBeAutoShared()) {
			article.doAutoShare();
		} else {
			Logger.debug("[BlogAutoshare] Blogarticle %s was already automatically shared. Skipping...",
					article.id);
		}

		renderJSON(createBuilder().create().toJson(article));
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_BLOG_ARTICLE, params = "id")
	public static void delete(long id) {
		BlogAppArticle article = BlogAppArticle.findById(id);
		notFoundIfNull(article);
		article.delete();
		ok();
	}

	public static void teaserImageUpload(String id) {
		TempUpload upload = Uploads.loadUpload(id);
		notFoundIfNull(upload);
		renderBinary(upload.blob.get(), upload.filename, upload.blob.type(), true);
	}

	private static GsonBuilder createBuilder() {
		User user = UserLoader.getConnectedUser();
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapter(BlogAppArticle.class, new BlogArticleSerializer(user))
				.registerTypeAdapter(BlogApp.class, new BlogAppSerializer(user))
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}
}
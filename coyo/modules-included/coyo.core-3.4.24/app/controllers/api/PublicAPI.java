package controllers.api;

import java.util.HashMap;
import java.util.Map;

import play.Play;

/**
 * @author mindsmash GmbH
 */
public class PublicAPI extends BaseAPI {

	/**
	 * Returns the application version as JSON.
	 */
	public static void info() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("version", Play.configuration.getProperty("application.version"));
		renderJSON(data);
	}

	public static void ping() {
		ok();
	}
}

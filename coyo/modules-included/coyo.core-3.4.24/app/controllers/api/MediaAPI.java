package controllers.api;

import java.util.ArrayList;
import java.util.List;

import json.HibernateProxyTypeAdapter;
import json.media.MediaCollectionSerializer;
import json.media.MediaRoomSerializer;
import json.media.MediaSerializer;
import json.MinimalUserSerializer;
import models.User;
import models.media.Media;
import models.media.MediaCollection;
import models.media.MediaPhoto;
import models.media.MediaRoom;
import models.media.MediaVideo;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import storage.FlexibleBlob;
import utils.HTTPStreamUtils;
import utils.ImageUtils;
import utils.VideoUtils;
import utils.VideoUtils.VideoType;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import controllers.Security;
import controllers.Uploads;
import controllers.Uploads.TempUpload;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

/**
 * @author mindsmash GmbH
 */
public class MediaAPI extends API {

	@AllowExternals
	@Secure(permissions = Permission.ACCESS_MEDIA_ROOM, params = "id")
	public static void room(Long id) {
		MediaRoom room = MediaRoom.findById(id);
		notFoundIfNull(room);
		renderJSON(createBuilder().create().toJson(room));
	}

	@AllowExternals
	public static void collection(Long id) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, collection.room.id);
		renderJSON(createBuilder().create().toJson(collection));
	}

	@AllowExternals
	public static void redirectToCollection(Long id) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		collection.redirectToResult();
	}

	@AllowExternals
	public static void mediaItems(Long id) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, collection.room.id);
		renderJSON(createBuilder().create().toJson(collection.media));
	}

	@AllowExternals
	public static void media(Long id) {
		Media media = Media.findById(id);
		notFoundIfNull(media);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, media.collection.room.id);
		renderJSON(createBuilder().create().toJson(media));
	}

	@AllowExternals
	public static void thumbnail(Long id) {
		Media media = Media.findById(id);
		notFoundIfNull(media);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, media.collection.room.id);

		FlexibleBlob blob = media.thumbnail;
		notFoundIfNull(blob);

		renderBinary(blob.get(), "media-thumb-" + id, blob.length(), blob.type(), true);
	}

	@AllowExternals
	public static void download(Long id) {
		Media media = Media.findById(id);
		notFoundIfNull(media);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, media.collection.room.id);

		FlexibleBlob blob = media.file;
		notFoundIfNull(blob);

		renderBinary(blob.get(), media.filename, blob.length(), blob.type(), false);
	}

	@AllowExternals
	public static void photo(Long id, boolean preview) {
		MediaPhoto photo = MediaPhoto.findById(id);
		notFoundIfNull(photo);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, photo.collection.room.id);

		FlexibleBlob blob = photo.file;
		if (preview) {
			blob = photo.preview;
		}

		notFoundIfNull(blob);

		renderBinary(blob.get(), photo.filename, blob.length(), blob.type(), true);
	}

	@AllowExternals
	public static void video(Long id, String suffix) throws Exception {
		MediaVideo video = MediaVideo.findById(id);
		notFoundIfNull(video);
		Security.checkAndCancel(Permission.ACCESS_MEDIA_ROOM, video.collection.room.id);

		String filename = "video-" + video.id;
		FlexibleBlob blob = video.file;

		if (StringUtils.isNotEmpty(suffix)) {
			VideoType vt = VideoType.getByFileSuffix(suffix);
			if (vt != null) {
				blob = video.getVariant(vt);
				filename += "." + vt.getFileSuffix();
			}
		}

		notFoundIfNull(blob);

		HTTPStreamUtils.stream(blob, filename, request, response);
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void setCover(Long id) {
		Media media = Media.findById(id);
		notFoundIfNull(media);
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, media.collection.room.id);
		media.cover = true;
		media.save();
		renderJSON(createBuilder().create().toJson(media));
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void saveMedia(@JsonBody Media media) {
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, media.collection.room.id);
		if (!media.validateAndSave()) {
			error("validation errors");
		}
		renderJSON(createBuilder().create().toJson(media));
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void deleteMedia(Long id) {
		Media media = Media.findById(id);
		notFoundIfNull(media);
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, media.collection.room.id);
		media.delete();
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void saveCollection(@JsonBody MediaCollection collection) {
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, collection.room.id);
		if (!collection.validateAndSave()) {
			error("validation errors");
		}
		renderJSON(createBuilder().create().toJson(collection));
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void deleteCollection(Long id) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, collection.room.id);
		collection.delete();
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	@Secure(permissions = Permission.EDIT_MEDIA_ROOM, params = "id")
	public static void sortCollections(Long id) {
		MediaRoom room = MediaRoom.findById(id);
		notFoundIfNull(room);

		final String json = params.getRootParamNode().getChild("body", true).getFirstValue(String.class);
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();

		int prio = 1;
		for (JsonElement e : array) {
			MediaCollection collection = MediaCollection.findById(e.getAsLong());
			if (collection != null && collection.room == room) {
				collection.priority = prio++;
				collection.save();
			}
		}
	}

	@Transactional
	@CheckAuthenticity
	public static void sortMedia(Long id, String sortOrder) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, collection.room.id);

		final String json = params.getRootParamNode().getChild("body", true).getFirstValue(String.class);
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();

		int prio = 1;
		for (JsonElement e : array) {
			Media media = Media.findById(e.getAsLong());
			if (media != null && media.collection == collection) {
				media.priority = prio++;
				media.save();
			}
		}
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void upload(Long id) {
		MediaCollection collection = MediaCollection.findById(id);
		notFoundIfNull(collection);
		Security.checkAndCancel(Permission.EDIT_MEDIA_ROOM, collection.room.id);

		Gson gson = createBuilder().create();
		JsonObject result = new JsonObject();
		JsonArray created = new JsonArray();
		JsonArray errors = new JsonArray();

		final String json = params.getRootParamNode().getChild("body", true).getFirstValue(String.class);
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();
		List<String> sortingList = new ArrayList<String>();
		
		for (JsonElement e : array) {
			TempUpload upload = Uploads.loadUpload(e.getAsString());
			if (upload != null && upload.blob.exists()) {
				sortingList.add(upload.filename);
			}
		}
		
		java.util.Collections.sort(sortingList);
		
		for (JsonElement e : array) {
			TempUpload upload = Uploads.loadUpload(e.getAsString());
			if (upload != null && upload.blob.exists()) {
				Media media = null;

				if (ImageUtils.isImage(upload.blob)) {
					Logger.debug("[MediaAPI] Saving image: %s", upload.filename);
					media = MediaPhoto.createPhoto(collection, UserLoader.getConnectedUser(), upload.filename, upload.blob);
				} else if (VideoUtils.isVideo(upload.blob.type())) {
					Logger.debug("[MediaAPI] Saving video: %s", upload.filename);
					media = MediaVideo.createVideo(collection, UserLoader.getConnectedUser(), upload.filename, upload.blob);
				}

				if (media != null) {					
					media.priority = sortingList.indexOf(upload.filename);
					media.save();
					created.add(gson.toJsonTree(media));
				} else {
					Logger.info("[MediaAPI] Cannot upload %s because the file format %s is not supported",
							upload.filename, upload.blob.type());
					JsonObject error = new JsonObject();
					error.addProperty("file", upload.filename);
					error.addProperty("message", Messages.get("media.error.unsupportedFileType", upload.filename));
					errors.add(error);
				}
			}
		}

		result.add("created", created);
		result.add("errors", errors);

		renderJSON(gson.toJson(result));
	}

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapter(MediaRoom.class, new MediaRoomSerializer())
				.registerTypeAdapter(MediaCollection.class, new MediaCollectionSerializer())
				.registerTypeAdapter(Media.class, new MediaSerializer(UserLoader.getConnectedUser()))
				.registerTypeAdapter(MediaPhoto.class, new MediaSerializer(UserLoader.getConnectedUser()))
				.registerTypeAdapter(MediaVideo.class, new MediaSerializer(UserLoader.getConnectedUser()))
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}

}

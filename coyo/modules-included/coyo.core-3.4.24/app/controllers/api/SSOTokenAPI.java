package controllers.api;

import models.User;
import multitenancy.MTA;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.libs.Codec;
import play.mvc.Util;
import session.UserLoader;
import utils.CacheUtils;

import com.google.gson.JsonPrimitive;

import controllers.Auth;
import controllers.TokenAuthController;

/**
 * SSO Token API generates a one time sso token that can be used for user authentication.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class SSOTokenAPI extends API {

	public static final String TOKEN_GET_PARAM = "sso-token";
	private static final String TOKEN_EXPIRATION = Play.configuration.getProperty("coyo.sso.expiration", "30s");

	public static void createToken() {
		String token = generateToken();
		if (token != null) {
			renderJSON(new JsonPrimitive(token).toString());
		}
		error();
	}

	public static boolean handleTokenAuthentication() {
		try {
			String token = getToken();
			if (null != token) {
				String cacheKey = CacheUtils.createTenantUniqueCacheKey(token);
				Long id = Cache.get(cacheKey, Long.class);

				if (null != id) {
					Cache.delete(cacheKey);
					User user = User.find("id = ?", id).first();
					if (user != null) {
						Auth.storeUserIdInSession(user.id);
						
						// supply auth token on the very first response after SSO
						TokenAuthController.setTokenHeader();
						
						return true;
					}
				}
			}
		} catch (Exception e) {
			Logger.warn(e, "error authenticating user via SSO token [%s]", request.url);
		}
		return false;
	}

	@Util
	public static String generateToken() {
		return generateToken(UserLoader.getConnectedUser());
	}
	
	@Util
	public static String generateToken(User user) {
		String token = Codec.UUID();
		String cacheKey = CacheUtils.createTenantUniqueCacheKey(token);
		if (Cache.safeAdd(cacheKey, user.id, TOKEN_EXPIRATION)) {
			return token;
		}
		return null;
	}
	
	private static String getToken() {
		return params.get(TOKEN_GET_PARAM);
	}
}

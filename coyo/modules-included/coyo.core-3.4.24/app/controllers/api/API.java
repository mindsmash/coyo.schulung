package controllers.api;

import json.CustomRenderJson;
import libs.Protection;
import models.Application;
import models.User;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.mvc.Before;
import play.mvc.Http.Header;
import play.mvc.Util;
import play.mvc.With;
import session.UserLoader;
import sun.misc.BASE64Decoder;

import com.google.gson.JsonSerializer;

import controllers.Auth;
import controllers.SecureBaseController;
import controllers.TokenAuthController;

/**
 * Handles authentificated API calls;
 * 
 * Supports BASIC authentication, GET param authentication (deprecated) and token authentication. After successful
 * authentication, the auth token is always supplied through the "auth-token" header.
 * 
 * The best way to use the API is:
 * 
 * 1. Authenticate via BASIC authentication to obtain an auth token<br>
 * 2. Make your REST calls by setting the "auth-token" request header
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@With(SecureBaseController.class)
public abstract class API extends BaseAPI {

	private static final String AUTHORIZATION = "authorization";
	private static final String WWW_AUTHENTICATE = "WWW-Authenticate";
	private static final String REALM = "Basic realm=\"Coyo\"";

	private static final String CORS_AC_ALLOW_ORIGIN = Play.configuration
			.getProperty("coyo.api.accessControl.allowOrigin");
	private static final boolean CORS_AC_ALLOW_CREDENTIALS = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.api.accessControl.allowCredentials", "false"));

	private static final String CSRF_DISABLED_KEY = "csrf-disabled";

	@Before(priority = 2)
	public static void auth() throws Exception {
		// first, try token-auth
		if (TokenAuthController.handleTokenAuthentication()) {
			Logger.debug("[API] User [%s] successfully authenticated with auth token", UserLoader.getConnectedUser());
			disableCSRF();
			TokenAuthController.setTokenHeader();
			return;
		}

		if (UserLoader.getConnectedUser(false) == null) {
			// if no user was authenticated yet, try basic or param authentication
			if (Protection.maxReached("apiAuth", Application.BRUTEFORCE_PROTECTION_MAX)) {
				forbidden("too many authentication failures. your IP is currently blocked. please wait a few minutes before trying again");
			}

			Logger.debug("[API] Authenticating user with basic authentication (username and password)");

			// Basic Auth
			Header authHeader = request.headers.get(AUTHORIZATION);
			if (authHeader == null) {
				response.setHeader(WWW_AUTHENTICATE, REALM);
				forbidden();
			}

			String auth = authHeader.value().substring(6);
			byte[] decodedAuth = new BASE64Decoder().decodeBuffer(auth);
			String[] credString = new String(decodedAuth, "UTF-8").split(":");

			if (credString == null || credString.length != 2) {
				forbidden();
			}

			String username = credString[0];
			String password = credString[1];

			if (Auth.getAuthenticator().authenticate(username, password)) {
				Long id = Long.parseLong("" + Auth.getAuthenticator().getId(username));
				User user = User.findById(id);
				if (user != null) {
					Logger.debug("[API] User [%s] successfully authenticated with username and password", user);
					request.args.put("user", user);
					disableCSRF();
					TokenAuthController.setTokenHeader();
					return;
				}
			} else {
				Protection.max("apiAuth", Application.BRUTEFORCE_PROTECTION_MAX, Application.BRUTEFORCE_PROTECTION_BAN);
			}

			forbidden();
		} else {
			Logger.debug("[API] User [%s] successfully authenticated with session", Auth.getCurrentId());
			// in case of session auth, CSRF needs to remain enabled
		}
	}

	protected static void renderJSON(Object o, JsonSerializer<?>... adapters) {
		throw new CustomRenderJson(o, adapters);
	}

	@Before(priority = 3)
	public static void accessControlHeaders() throws Exception {
		if (!StringUtils.isEmpty(CORS_AC_ALLOW_ORIGIN)) {
			response.accessControl(CORS_AC_ALLOW_ORIGIN, CORS_AC_ALLOW_CREDENTIALS);
		}
	}

	/**
	 * Disables CSRF for the current request.
	 */
	@Util
	private static void disableCSRF() {
		Logger.debug("[API] Disabling CSRF for the current request");
		request.args.put(CSRF_DISABLED_KEY, Boolean.TRUE);
	}

	/**
	 * @return True if CSRF is disabled for the current request.
	 */
	@Util
	public static boolean isCSRFDisabled() {
		return request.args.get(CSRF_DISABLED_KEY) == Boolean.TRUE;
	}
}

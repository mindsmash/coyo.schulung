package controllers.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import json.ConversationPostSerializer;
import json.ConversationsSerializer;
import json.FileAttachmentSerializer;
import json.LinkAttachmentSerializer;
import json.MinimalUserSerializer;
import models.User;
import models.messaging.Conversation;
import models.messaging.ConversationEvent;
import models.messaging.ConversationMember;
import models.messaging.ConversationStatus;
import models.wall.post.ConversationPost;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.Transactional;
import security.CheckCaller;
import security.Secure;
import session.UserLoader;
import util.Util;
import acl.Permission;
import binding.JsonBody;
import controllers.Cache;
import controllers.Messaging;
import controllers.Security;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import events.type.UpdatedConversationEvent;
import forms.ConversationPostForm;

/**
 * Handles all actions to access private messages via API call.
 * 
 * @author Jan Marquardt
 */
@AllowExternals
@ShowExternals
public class MessagingAPI extends API {

	/**
	 * Returns the number of unread messages as JSON.
	 */
	public static void count() {
		renderJSON(UserLoader.getConnectedUser().getUnreadConversationCount());
	}

	/**
	 * Returns the conversation for the given status.
	 * 
	 * @param status
	 * @param active
	 */
	public static void conversations(ConversationStatus status, Boolean active) {
		if (status == null) {
			status = ConversationStatus.INBOX;
		}

		List<ConversationMember> conversationMembers;
		if (null != active && active) {
			conversationMembers = UserLoader.getConnectedUser().getActiveConversations();
		} else {
			conversationMembers = UserLoader.getConnectedUser().getConversations(status);
		}

		Cache.cacheEntities(conversationMembers, UserLoader.getConnectedUser().getPrivateCacheKey());

		renderJSON(conversationMembers, new ConversationsSerializer(UserLoader.getConnectedUser()));
	}

	/**
	 * Change status of conversation.
	 * 
	 * @param id
	 * @param status
	 */
	@CheckAuthenticity
	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void status(Long id, @JsonBody ConversationStatus status) {
		notFoundIfNull(id);

		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		if (status != null) {
			if (status != ConversationStatus.INBOX) {
				cm.active = false;
			}

			cm.status = status;
			cm.save();
		}

		renderJSON(cm.status);
	}

	/**
	 * Change active status of conversation
	 * 
	 * @param id
	 * @param active
	 */
	@CheckAuthenticity
	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void active(Long id, @JsonBody Boolean active) {
		notFoundIfNull(id);

		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		cm.active = active;
		cm.save();

		renderJSON(cm.active);
	}

	/**
	 * Leave a conversation.
	 * 
	 * @param id
	 */
	@CheckAuthenticity
	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void delete(Long id) {
		notFoundIfNull(id);

		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		Conversation conversation = cm.conversation;

		if (conversation.getActiveMemberCount() > 2) {
			Messaging.addEvent(cm, ConversationEvent.LEAVE);
			cm.delete();
		} else {
			cm.softDelete();
		}
	}

	/**
	 * Load posts
	 * 
	 * @param id
	 *            conversation id
	 * @param beforeId
	 *            all posts before given id
	 * @param afterId
	 *            all posts after given id
	 * @param modifiedAfter
	 *            all posts modified after given timestamp
	 */
	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void posts(Long id, Long beforeId, Long afterId, Long modifiedAfter) {
		notFoundIfNull(id);

		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		// update last access date
		cm.lastAccess = new Date();
		cm.save();

		JPAQuery query = cm.getPostsQuery(beforeId, afterId, modifiedAfter);
		if (params._contains("page")) {
			EndlessHelper.prepare(query, false);
		} else {
			renderArgs.put("data", query.fetch());
		}

		new UpdatedConversationEvent(cm).raise(cm.user);

		response.setHeader("Conversation-Modified", cm.conversation.modified.getTime() + "");
		renderJSON(renderArgs.get("data"), new ConversationPostSerializer(), new MinimalUserSerializer(),
				new LinkAttachmentSerializer(), new FileAttachmentSerializer());
	}

	/**
	 * Write a new post.
	 * 
	 * @param id
	 * @param form
	 */
	@CheckAuthenticity
	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void write(Long id, @JsonBody ConversationPostForm form) {
		notFoundIfNull(id);

		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		ConversationPost post = form.save(cm);

		if (null == post) {
			badRequest();
		}

		cm.refresh();

		renderJSON(post, new ConversationPostSerializer(), new MinimalUserSerializer(), new LinkAttachmentSerializer(),
				new FileAttachmentSerializer());
	}

	@Transactional
	@CheckAuthenticity
	public static void conversationWith(Set<Long> members) {
		if (members == null) {
			members = new HashSet<Long>();
		}

		User connectedUser = UserLoader.getConnectedUser();

		for (Long memberId : members) {
			Security.checkAndCancel(Permission.CREATE_CONVERSATION, memberId);
		}

		members.add(connectedUser.id);

		if (members.size() < 2) {
			forbidden("at least two users are required for a conversation");
		}

		Conversation conversation = null;

		// try to find existing bi-directional conversation
		if (members.size() == 2) {
			conversation = Conversation
					.find("SELECT c FROM Conversation c WHERE SIZE(c.members) = 2 AND 2 = (SELECT COUNT(cm) FROM ConversationMember cm WHERE cm.conversation.id = c.id AND cm.user.id IN ("
							+ Util.implode(new ArrayList<Long>(members), ",") + "))").first();
		}

		// if non exists, create
		if (conversation == null) {
			conversation = new Conversation();

			for (Long id : members) {
				ConversationMember member = new ConversationMember();
				member.conversation = conversation;
				member.user = User.findById(id);
				conversation.members.add(member);
			}

			conversation.save();
			conversation.refresh();
		} else {
			// reactivate
			for (ConversationMember cm : conversation.members) {
				if (cm.isDeleted()) {
					cm.status = ConversationStatus.INVISIBLE;
					cm.save();
				}
			}
		}

		ConversationMember cm = conversation.getMemberObject(UserLoader.getConnectedUser());
		cm.active = true;
		cm.save();

		renderJSON(cm, new ConversationsSerializer(UserLoader.getConnectedUser()));
	}
}
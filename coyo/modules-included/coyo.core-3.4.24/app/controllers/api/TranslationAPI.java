package controllers.api;

import java.util.Locale;

import models.Translation;
import models.Translation.TranslationJsonSerializer;
import play.db.jpa.Transactional;
import security.Secure;
import acl.Permission;
import controllers.external.annotations.AllowExternals;

/**
 * API for managing {@link Translation}s.
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
@AllowExternals
public class TranslationAPI extends API {

	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	public static void list(Long senderId, String uid) {
		renderJSON(Translation.listTranslations(senderId, uid), new TranslationJsonSerializer(false));
	}

	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	public static void load(Long senderId, String uid, Locale language) {
		Translation translation = Translation.findTranslation(senderId, uid, language);
		notFoundIfNull(translation);
		renderJSON(translation, new TranslationJsonSerializer(true));
	}

	@Transactional
	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	public static void delete(Long senderId, String uid, Locale language) {
		Translation translation = Translation.findTranslation(senderId, uid, language);
		notFoundIfNull(translation);
		translation.delete();
		ok();
	}

	@Transactional
	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	public static void save(Long senderId, String uid, Locale language, String text) {
		Translation.storeTranslation(senderId, uid, language, text);
		ok();
	}
}

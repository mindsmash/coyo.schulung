package controllers.api;

import acl.Permission;
import binding.JsonBody;

import com.google.gson.GsonBuilder;

import controllers.Cache;
import controllers.Collaboration;
import controllers.Security;
import controllers.Uploads;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.comments.CommentFileAttachmentSerializer;
import json.comments.CommentSerializer;
import models.Sender;
import models.User;
import models.comments.Comment;
import models.comments.CommentDTO;
import models.comments.CommentFileAttachment;
import models.comments.Commentable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Play;
import play.data.validation.Required;
import play.db.jpa.Transactional;
import play.libs.Time;
import session.UserLoader;
import util.ModelUtil;

import java.util.Date;

/**
 * Comments REST API.
 * Old style comments handling (with global binder) can be found and used in class {@link Collaboration}
 *
 * @author Gerrit Dongus, mindsmash GmbH
 * @author Jan Marquardt, mindsmash GmbH
 */
@AllowExternals
public class CommentsAPI extends API {
	private static final Logger LOG = LoggerFactory.getLogger(CommentsAPI.class);
	private static final String UPDATE_INTERVAL = Play.configuration.getProperty("coyo.comments.update.interval", "30s");

	/**
	 * Create on comment and add file attachments if present.
	 *
	 * @param id
	 * @param clazz
	 */
	@Transactional
	@CheckAuthenticity
	@ShowExternals
	public static void create(final Long id, final String clazz, @Required @JsonBody final CommentDTO commentDTO) {
		if (validation.hasErrors()) {
			badRequest();
		}

		final Commentable commentable = Collaboration.loadCommentable(id, clazz);
		notFoundIfNull(commentable);

		Security.checkAndCancel(Permission.CREATE_COMMENT, commentable);

		final Comment comment = new Comment(commentable);
		comment.author = UserLoader.getConnectedUser();
		comment.text = commentDTO.text;

		for (String fileId : commentDTO.attachments) {
			final Uploads.TempUpload upload = Uploads.loadUpload(fileId);
			if (upload != null) {
				final CommentFileAttachment attachment = new CommentFileAttachment();
				attachment.file = upload.blob;
				attachment.name = upload.filename;
				attachment.comment = comment;
				comment.attachments.add(attachment);
			}
		}

		comment.save();
		commentable.addComment(comment);

		renderJSON(createBuilder(commentable).create().toJson(comment));
	}

	/**
	 * Deletes a comment
	 *
	 * @param commentId
	 * @param id
	 * @param clazz
	 */
	@Transactional
	@CheckAuthenticity
	public static void delete(Long commentId, Long id, String clazz) {
		Commentable commentable = Collaboration.loadCommentable(id, clazz);
		Comment comment = Comment.findById(commentId);
		notFoundIfNull(commentable);
		notFoundIfNull(comment);

		Security.checkAndCancel(Permission.DELETE_COMMENT, commentId, commentable);

		commentable.removeComment(comment);
		comment.delete();

		ok();
	}

	public static void comments(Long id, String clazz) {
		final Commentable commentable = Collaboration.loadCommentable(id, clazz);
		notFoundIfNull(commentable);

		Security.checkAndCancel(Permission.ACCESS_SENDER, commentable.getSender());

		renderJSON(createBuilder(commentable).create().toJson(commentable.getComments()));
	}

	/**
	 * Find and return the file attachment identified by id belonging to one commentable
	 *
	 * @param id
	 * @param commentableUid
	 */
	@AllowExternals
	public static void fileAttachment(final Long id, final String commentableUid) {
		final CommentFileAttachment attachment = CommentFileAttachment.findById(id);
		notFoundIfNull(attachment);

		// used for permission check
		final Commentable commentable = (Commentable) ModelUtil.deserialize(commentableUid);
		notFoundIfNull(commentable);

		Security.checkAndCancel(Permission.CREATE_COMMENT, commentable);

		Cache.cacheEntity(attachment, UserLoader.getConnectedUser().getPrivateCacheKey());
		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}

		renderBinary(attachment.file.get(), attachment.name, attachment.file.length(), attachment.file.type(), false);
	}

	public static long getUpdateInterval() {
		return Time.parseDuration(UPDATE_INTERVAL) * 1000L;
	}

	public static GsonBuilder createBuilder(Commentable commentable) {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Comment.class, new CommentSerializer(UserLoader.getConnectedUser(), commentable));
		builder.registerTypeAdapter(CommentFileAttachment.class, new CommentFileAttachmentSerializer(commentable));
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

		return builder;
	}
}

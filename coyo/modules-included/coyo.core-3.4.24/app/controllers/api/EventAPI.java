package controllers.api;

import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.WorkspaceMemberSerializer;
import acl.Permission;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import play.db.jpa.JPABase;
import security.Secure;
import injection.Inject;
import injection.InjectionSupport;
import models.User;
import models.dao.EventDao;
import models.event.Event;
import models.event.EventUser.EventResponse;
import models.workspace.WorkspaceMember;

@InjectionSupport
public class EventAPI extends API {

	@Inject(configuration = "coyo.di.dao.event", defaultClass = EventDao.class, singleton = true)
	private static EventDao eventDao;

	/**
	 * Returns all users of this event grouped by their response (whether they are attending this event or not). In
	 * addition the count of users is added per response.
	 * 
	 * @param id the id of the event of which the users should be returned
	 */
	@AllowExternals
	@ShowExternals
	@Secure(permissions = Permission.ACCESS_EVENT, params = "id")
	public static void getUsersByResponse(Long id) {
		Event event = Event.findById(id);
		notFoundIfNull(event);

		/* create json result with all users grouped by their response */
		Gson gson = createBuilder().create();
		JsonObject result = new JsonObject();
		for (EventResponse eventResponse : EventResponse.values()) {
			JsonObject responseObject = new JsonObject();
			responseObject.addProperty("label", eventResponse.getLabel());
			Long count = eventDao.getUserCount(event, eventResponse);
			responseObject.addProperty("count", count);
			responseObject.add("userList", gson.toJsonTree(eventDao.getUsers(event, eventResponse)));
			result.add(eventResponse.name(), responseObject);
		}
		renderJSON(gson.toJson(result));
	}

	private static GsonBuilder createBuilder() {
		return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
				.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
	}

}

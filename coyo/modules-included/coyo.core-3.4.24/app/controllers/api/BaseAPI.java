package controllers.api;

import models.Settings;
import play.Play;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import conf.ApplicationSettings;
import controllers.Auth;

public class BaseAPI extends Controller {

	@Before
	public static void setHeaders() {
		response.setHeader("coyo-version", Play.configuration.getProperty("application.version"));
	}
}
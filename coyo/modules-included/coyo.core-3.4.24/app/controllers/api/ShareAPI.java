package controllers.api;

import injection.Inject;
import injection.InjectionSupport;

import java.util.List;

import json.HibernateProxyTypeAdapter;
import json.SenderSerializer;
import models.Sender;
import models.Sharable;
import models.User;
import models.dao.EventDao;
import models.event.Event;
import models.page.Page;
import models.wall.Wall;
import models.wall.post.Post;
import models.workspace.Workspace;
import play.Logger;
import play.db.helper.JpqlSelect;
import play.db.jpa.Transactional;
import session.UserLoader;
import acl.Permission;
import binding.JsonBody;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import controllers.Collaboration;
import controllers.Security;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

/**
 * API controller for sharing entities on walls.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
@AllowExternals
@InjectionSupport
public class ShareAPI extends API {
	
	/**
	 * Data transfer object for sharing an object
	 */
	public class SharableFormData {
		Long id;
		Long wall;
		String clazz;
		String message;
	}

	/**
	 * Filters senders that do not have a wall from a collection.
	 */
	private static final class HasWallPredicate implements Predicate<Sender> {
		@Override
		public boolean apply(Sender sender) {
			return (sender.getDefaultWall() != null);
		}
	}
	
	@Inject(configuration="coyo.di.dao.event", defaultClass = EventDao.class, singleton = true)
	private static EventDao eventDao;
	
	/**
	 * Returns a hierarchically structured list of senders for the wall chooser in JSON format. It contains pages,
	 * workspaces, events and colleagues of the current user. In addition the current user itself is added to the root
	 * of the list.
	 */
	@AllowExternals
	public static void getSenderList() {
		JsonObject result = new JsonObject();
		User user = UserLoader.getConnectedUser();
		Gson gson = createBuilder().create();

		/* add the current user */
		result.add("user", gson.toJsonTree(user));

		/* filter all sender if the dont have a wall */
		Iterable<Page> pages = Iterables.filter(user.getPages(null), new HasWallPredicate());
		Iterable<Workspace> workspaces = Iterables.filter(user.getActiveWorkspaces(null), new HasWallPredicate());
		Iterable<Event> events = Iterables.filter(eventDao.getUpcomingCreatedEvents(user), new HasWallPredicate());
		JpqlSelect select = user.getUserChooserSelect(false, false, null, null);
		List<User> users = User.find(select.toString(), select.getParams().toArray()).fetch();

		/* add filtered results to json */
		JsonArray senders = new JsonArray();
		senders.add(createSectionObject("pages", pages, gson));
		senders.add(createSectionObject("workspaces", workspaces, gson));
		senders.add(createSectionObject("events", events, gson));
		senders.add(createSectionObject("colleagues", users, gson));

		result.add("senders", senders);
		renderJSON(result);
	}
	
	@Transactional
	@CheckAuthenticity
	public static void share(@JsonBody SharableFormData formData) {
		// get sharable and wall to share to
		Sharable sharable = Collaboration.loadSharable(formData.id, formData.clazz);
		notFoundIfNull(sharable);
		Wall wall = Wall.findById(formData.wall);
		notFoundIfNull(wall);
		
		// check permissions whether user can access chosen wall
		Security.check(Permission.ACCESS_SENDER, wall.sender.id);

		// create post
		Logger.debug("Sharing on wall %s of sender %s", wall, wall.sender);
		Post post = sharable.share(UserLoader.getConnectedUser(), wall, formData.message);
		post.save();
		
		// update activity stream
		UserLoader.getConnectedUser().getNewActivityStream(true);
	}

	private static JsonObject createSectionObject(String name, Iterable<? extends Sender> list, Gson gson) {
		JsonObject object = new JsonObject();
		object.addProperty("name", name);
		object.add("list", gson.toJsonTree(Lists.newArrayList(list)));
		return object;
	}
	
	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeHierarchyAdapter(Sender.class, new SenderSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		return builder;
	}

}

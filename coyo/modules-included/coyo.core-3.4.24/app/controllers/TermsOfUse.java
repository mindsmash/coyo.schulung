package controllers;

import java.util.Date;

import models.User;
import play.db.jpa.Transactional;
import play.mvc.Before;
import play.mvc.Util;
import plugins.PluginManager;
import plugins.internal.TermsOfUsePlugin;
import session.UserLoader;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

/**
 * Handles all actions for the Terms of Use Plugin.
 * 
 * @author mindsmash GmbH
 */
@AllowExternals
public class TermsOfUse extends WebBaseController {

	@Before(unless = "show")
	static void checkAlreadyAccepted() {
		if (UserLoader.getConnectedUser().termsOfUseAccepted != null) {
			ActivityStream.index(null, null);
		}
	}

	@Before
	static void checkPluginActive() {
		if (!PluginManager.isActive(PluginManager.getPlugin(TermsOfUsePlugin.KEY))) {
			ActivityStream.index(null, null);
		}
	}

	public static void form() {
		renderArgs.put("text", getTermsOfUseText());
		if (TermsOfUsePlugin.resultHandler.allowDeny(UserLoader.getConnectedUser())) {
			renderArgs.put("allowDeny", true);
		}
		render();
	}

	public static void show() {
		renderArgs.put("text", getTermsOfUseText());
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void accept() {
		User user = UserLoader.getConnectedUser();

		TermsOfUsePlugin.resultHandler.handleAccept(user);

		user.termsOfUseAccepted = new Date();
		user.save();
		ActivityStream.index(null, null);
	}

	@Transactional
	@CheckAuthenticity
	public static void deny() {
		TermsOfUsePlugin.resultHandler.handleDeny(UserLoader.getConnectedUser());

		ActivityStream.index(null, null);
	}
	
	@Util
	public static String getTermsOfUseText() {
		TermsOfUsePlugin plugin = (TermsOfUsePlugin) PluginManager.getPlugin(TermsOfUsePlugin.KEY);
		return plugin.getText();
	}
}

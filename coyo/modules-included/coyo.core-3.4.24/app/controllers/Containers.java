package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import json.list.ContainerSerializer;
import json.list.FieldOptionSerializer;
import json.list.FieldSerializer;
import json.list.ItemSerializer;
import json.list.ItemValueSerializer;
import json.FileAttachmentSerializer;
import json.MinimalUserSerializer;
import models.Sender;
import models.User;
import models.app.App;
import models.app.ListApp;
import models.container.Container;
import models.container.ContainerPermission;
import models.container.Field;
import models.container.FieldFactory;
import models.container.Item;
import models.container.ItemValue;
import models.container.fields.BooleanField;
import models.container.fields.OptionsField;
import models.container.fields.OptionsFieldOption;
import models.container.values.attachments.FileItemAttachment;
import models.notification.Notification;
import models.notification.WorkspaceLeaveNotification;
import models.notification.listapp.ListItemChangedNotification;
import models.notification.listapp.ListItemCreatedNotification;

import models.notification.listapp.ListItemNotification;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.IOUtils;

import play.data.validation.Error;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.data.validation.Validation.ValidationResult;
import play.db.jpa.Transactional;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.RenderJson;
import security.Secure;
import session.UserLoader;
import utils.ErrorHelper;
import acl.Permission;
import binding.JsonBody;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.GsonBuilder;

import containers.FieldTypeDescriptor;
import containers.form.ContainerFormData;
import containers.form.FieldFormData;
import containers.form.ItemFormData;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import extensions.CommonExtensions;

/**
 * @author Jan Tammen, mindsmash GmbH
 */
public class Containers extends WebBaseController {
	
	private static final String CSV_ENCODING = "ISO-8859-1";
	private static final String CSV_MIMETYPE = "text/csv";
	private static final String CSV_CONTENT_TYPE = CSV_MIMETYPE + ";charset=" + CSV_ENCODING;

	@Transactional
	@CheckAuthenticity
	public static void saveContainer(@Required @Valid @JsonBody ContainerFormData containerFormData) {
		if (containerFormData == null || validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		Sender sender = Sender.findById(containerFormData.senderId);
		if (sender == null) {
			badRequest();
		}

		Security.checkAndCancel(Permission.EDIT_SENDER, sender.id);

		Container container = Container.findById(containerFormData.id);
		if (container == null) {
			badRequest();
		}

		List<FieldFormData> fields = containerFormData.fields;
		List<Field> addedFields = new ArrayList<>();

		for (FieldFormData fieldFormData : fields) {
			Field field = Field.updateOrCreateField(container, fieldFormData);
			container.addField(field);
			addedFields.add(field);
			field.save();
			if (field instanceof OptionsField) {
				FieldFactory.updateOptions((OptionsField) field, fieldFormData.options);
			}

			field.save();
		}

		container.removeNonPresentFields(addedFields);

		container.save();
		created(container);
	}

	@Secure(permissions = Permission.CREATE_CONTAINER_ITEM, params = { "containerId" })
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void createItem(@Required Long containerId, @Required @Valid @JsonBody ItemFormData itemFormData) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		Container container = loadContainer(containerId);
		Item newItem = ItemFormData.createItem(container, itemFormData, UserLoader.getConnectedUser());

		// do some custom validation of item
		for (ValidationResult validationResult : newItem.getValidationResults()) {
			if (!validationResult.ok) {
				Error error = validationResult.error;
				validation.addError(error.getKey(), error.message());
			}
		}
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		// item is OK, add it and try to save container
		if (!newItem.validateAndSave()) {
			badRequest(validation.errorsMap());
		}
		container.refresh();

		final App app = newItem.getApp();
		if (app != null && app instanceof ListApp && app.active) {
			ListItemNotification.raise(ListItemCreatedNotification.class, app, newItem);
		}

		created(newItem);
	}

	@Secure(permissions = { Permission.DELETE_CONTAINER_ITEM }, params = { "itemId" })
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void deleteItem(@Required Long containerId, @Required Long itemId) {
		Container container = loadContainer(containerId);
		Item item = loadItem(itemId);
		container.items.remove(item);
		item.delete();
		container.save();
	}

	@AllowExternals
	public static void getContainer(@Required Long containerId, boolean includeItems) {
		Container container = loadContainer(containerId);
		Security.checkAndCancel(Permission.ACCESS_SENDER, container.sender.id);
		includeItems = (Security.check(Permission.READ_CONTAINER_ITEMS, containerId)) ? includeItems : false;
		if (includeItems) {
			// if the global right is not set, we must only send the user's items in the response!
			if (!Security.check(Permission.READ_ALL_CONTAINER_ITEMS, containerId)) {
				final User user = UserLoader.getConnectedUser();
				Iterables.removeIf(container.items, new Predicate<Item>() {
					@Override
					public boolean apply(Item item) {
						return !item.canBeReadBy(user);
					}
				});
			}
		}
		renderJSON(createJsonBuilder(includeItems).create().toJson(container));
	}

	public static void getFieldTypes() {
		renderJSON(FieldTypeDescriptor.getFieldTypeDescriptors());
	}

	public static void getPermissions(@Required Long containerId) {
		Container container = loadContainer(containerId);
		Map<String, Boolean> permissionMap = new HashMap<String, Boolean>();
		for (ContainerPermission permission : ContainerPermission.values()) {
			permissionMap.put(permission.name(), container.hasPermission(permission));
		}
		renderJSON(permissionMap);
	}

	@Secure(permissions = { Permission.READ_CONTAINER_ITEM }, params = { "itemId" })
	@AllowExternals
	public static void getItem(@Required Long itemId) {
		renderJSON(createJsonBuilder().create().toJson(loadItem(itemId)));
	}

	@Secure(permissions = { Permission.READ_CONTAINER_ITEMS }, params = { "containerId" })
	@AllowExternals
	public static void getItems(@Required Long containerId) {
		Container container = loadContainer(containerId);
		Security.checkAndCancel(Permission.ACCESS_SENDER, container.sender.id);
		renderJSON(createJsonBuilder().create().toJson(container.getAllItems(UserLoader.getConnectedUser(), null, null)));
	}

	@Secure(permissions = { Permission.EDIT_CONTAINER_ITEM }, params = { "itemId" })
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void saveItem(@Required Long itemId, @Required @Valid @JsonBody ItemFormData itemFormData) {
		Item item = loadItem(itemId);
		item = ItemFormData.updateItem(item, itemFormData);

		// do some custom validation of item
		for (ValidationResult validationResult : item.getValidationResults()) {
			if (!validationResult.ok) {
				Error error = validationResult.error;
				validation.addError(error.getKey(), error.message());
			}
		}
		if (!item.validateAndSave() || validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}


		final App app = item.getApp();
		if (app != null && app instanceof ListApp && app.active) {
			ListItemNotification.raise(ListItemChangedNotification.class, app, item);
		}

		renderJSON(createJsonBuilder().create().toJson(item));
	}

	/**
	 * This is a copy of {@linkplain Posts#fileAttachment(Long)} for {@link FileItemAttachment}
	 */
	@AllowExternals
	public static void fileAttachment(Long id) {
		FileItemAttachment fa = FileItemAttachment.findById(id);
		notFoundIfNull(fa);

		Security.checkAndCancel(Permission.READ_CONTAINER_ITEM, fa.fileItemValue.item.id);

		Cache.cacheEntity(fa, UserLoader.getConnectedUser().getPrivateCacheKey());

		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}
		renderBinary(fa.file.get(), fa.name, fa.file.length(), fa.file.type(), false);
	}

	/**
	 * Exports the given list as CSV. Values are separated by semicolon. Field labels are included as first line in the
	 * file.
	 */
	@Secure(permissions = { Permission.READ_CONTAINER_ITEMS }, params = { "containerId" })
	@AllowExternals
	public static void csv(Long containerId) {
		Container container = loadContainer(containerId);
		Security.checkAndCancel(Permission.ACCESS_SENDER, container.sender.id);

		StringBuilder builder = new StringBuilder();
		try (CSVPrinter printer = new CSVPrinter(builder, CSVFormat.EXCEL.withDelimiter(';'))) {

			/* Add header record */
			List<String> header = Lists.transform(container.fields, new Function<Field, String>() {
				@Override
				public String apply(Field field) {
					return field.label;
				}
			});
			printer.printRecord(header);

			/*
			 * Print items - we have to manually iterate over all fields per item, since some item values can be null.
			 * Those items should still be written as an empty value into the csv.
			 */
			for (Item item : container.getAllItems(UserLoader.getConnectedUser(), null, null)) {
				for (Field field : container.fields) {
					ItemValue itemValue = item.getItemValueForField(field.id);
					Object csvValue = (itemValue != null) ? itemValue.getStringValue() : ((field instanceof BooleanField) ? false : null);
					printer.print(csvValue);
				}
				printer.println();
			}

			// IE8 SSL download fix
			if (request.secure) {
				response.setHeader("Cache-Control", "private, max-age=15");
			}
			
			// render
			response.setContentTypeIfNotSet(CSV_MIMETYPE);
			response.contentType = CSV_CONTENT_TYPE;
			String fileName = CommonExtensions.slugify(container.sender.getDisplayName() + "-" + container.name)  + ".csv";
			renderBinary(IOUtils.toInputStream(builder.toString(), CSV_ENCODING), fileName);
		} catch (IOException e) {
			ErrorHelper.handleWarning(e, "Could not create CSV for container with id [%d]", container.id);
			badRequest();
		}
	}

	protected static void badRequest(final Map<String, List<Error>> errorsMap) {
		if (errorsMap == null || errorsMap.isEmpty()) {
			badRequest();
		}
		throw new RenderJson(errorsMap) {
			@Override
			public void apply(Request request, Response response) {
				response.status = Http.StatusCode.BAD_REQUEST;
				super.apply(request, response);
			}
		};
	}

	protected static void created(final Object object) {
		throw new RenderJson(createJsonBuilder().create().toJson(object)) {
			@Override
			public void apply(Request request, Response response) {
				response.status = Http.StatusCode.CREATED;
				super.apply(request, response);
			}
		};
	}

	private static GsonBuilder createJsonBuilder() {
		return createJsonBuilder(false);
	}

	private static GsonBuilder createJsonBuilder(boolean includeContainerItems) {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Container.class, new ContainerSerializer(includeContainerItems));
		builder.registerTypeAdapter(Item.class, new ItemSerializer());
		builder.registerTypeHierarchyAdapter(Field.class, new FieldSerializer());
		builder.registerTypeHierarchyAdapter(OptionsFieldOption.class, new FieldOptionSerializer());
		builder.registerTypeHierarchyAdapter(FileItemAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeHierarchyAdapter(ItemValue.class, new ItemValueSerializer());
		// TODO Ok for now since only users are allowed as sender
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		return builder;
	}

	protected static Container loadContainer(@Required Long containerId) {
		notFoundIfNull(containerId);
		Container container = Container.findById(containerId);
		notFoundIfNull(container);
		return container;
	}

	protected static Item loadItem(@Required Long itemId) {
		notFoundIfNull(itemId);
		Item item = Item.findById(itemId);
		notFoundIfNull(item);
		return item;
	}

}

package controllers;

import java.util.Locale;

import models.Sender;
import models.Translation;
import models.User;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jpa.Transactional;
import play.mvc.Util;
import security.Secure;
import session.UserLoader;
import util.HttpUtil;
import acl.Permission;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

@AllowExternals
public class Translations extends WebBaseController {

	private static final String CACHE_TIME = Play.configuration.getProperty("coyo.translations.permission.cache", "15mn");
	
	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	public static void form(Long senderId, String uid, Locale language, boolean editor) {
		checkAllowed(uid);
		
		Sender sender = Sender.findById(senderId);
		notFoundIfNull(sender);

		renderArgs.put("translation", Translation.findTranslation(senderId, uid, language));
		renderArgs.put("destination", HttpUtil.getReferrerUrl());
		render(sender, senderId, uid, language, editor);
	}

	@Transactional
	@Secure(permissions = Permission.ACCESS_SENDER, params = "senderId")
	@CheckAuthenticity
	public static void save(Long senderId, String uid, Locale language, String destination, String text) {
		checkAllowed(uid);
		
		Sender sender = Sender.findById(senderId);
		notFoundIfNull(sender);

		Translation.storeTranslation(senderId, uid, language, text);

		if (destination != null) {
			redirect(HttpUtil.appendParam(destination, "language", language.getLanguage()));
		}
		redirectToSender(sender, null);
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		Translation translation = Translation.findById(id);
		notFoundIfNull(translation);
		
		checkAllowed(translation.uid);

		Security.checkAndCancel(Permission.ACCESS_SENDER, translation.sender.id);
		translation.delete();

//		redirect(HttpUtil.removeParam(HttpUtil.getReferrerUrl(), "language"));
	}

	@Util
	public static void checkAllowed(String uid) {
		if (!"true".equals(Cache.get(getAllowanceCacheKey(uid, UserLoader.getConnectedUser())))) {
			forbidden();
		}
	}

	/**
	 * This method is called by the translatable template/tag.
	 * @param uid
	 * @param user
	 */
	@Util
	public static void allowUser(String uid, User user) {
		Logger.debug("[Translations] Allowing user [%s] for content [%s]", user, uid);
		play.cache.Cache.set(getAllowanceCacheKey(uid, user), "true", CACHE_TIME);
	}

	@Util
	private static String getAllowanceCacheKey(String uid, User user) {
		return "allow-translation[uid=" + uid + ",user=" + user.id + "]";
	}
}

package controllers;

import checks.EmailRegistrationCheck;
import checks.PasswordSecurityCheck;
import conf.ApplicationSettings;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import libs.Protection;
import models.Activation;
import models.Application;
import models.Settings;
import models.User;
import models.UserGroup;
import models.UserRole;
import models.notification.UserRegistrationNotification;
import notifiers.ApplicationMailer;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jpa.Transactional;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;

import java.util.Locale;

/**
 * This controller handles all requested associated with registration
 */
@ShowExternals
@Transactional
public class Registration extends Controller {

	@Before
	static void checkPreconditions() {
		// check settings
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.REGISTRATION)) {
			redirect("/");
		}

		// check logged in
		if (!StringUtils.isEmpty(Auth.getCurrentId())) {
			redirect("/");
		}
	}

	public static void register() {
		render();
	}

	@Util
	public static boolean emailExists(String email) {
		return User.findByEmail(email) != null;
	}

	/**
	 * Generates a token for the given <code>mail</code> and send's an email
	 * notification with an activation token to that mail address.
	 * 
	 * @param mail
	 */
	@CheckAuthenticity
	public static void send(@Email @Required @CheckWith(EmailRegistrationCheck.class) String email) {
		Logger.trace("Registering email address %s", email);

		// check for validation errors
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();

			Logger.debug("Can't register mail adress %s because of validation errors: %s", email, validation.errors()
					.toString());

			register();
		}

		if (Protection.max(Application.BRUTEFORCE_PROTECTION_MAX, Application.BRUTEFORCE_PROTECTION_BAN)) {
			forbidden("too many registrations. your IP is currently blocked. please wait a few minutes before trying again");
		}

		// check if email already exists
		if (emailExists(email)) {
			register();
		}

		// create activation
		Activation activation = new Activation(email);
		activation.save();

		// send activation mail
		ApplicationMailer.activate(email, activation.token);

		flash.success(Messages.get("registration.success"));
		redirect("Auth.login");
	}

	/**
	 * Tries to complete registration for the given <code>mail</code> and
	 * <code>token</code>. If there is an pending registration for the given
	 * <code>mail</code> and <code>token</code>, the user will be asked to fill
	 * out his profile information and can then complete his registration.
	 * 
	 * @param email
	 * @param token
	 */
	public static void complete(@Email @Required String email, @Required String token) {
		render(email, token);
	}

	/**
	 * This is the last step of the registration. The user now has entered his
	 * profile information, so we can create the user in the database.
	 * 
	 * @param email
	 * @param password
	 * @param firstName
	 * @param lastName
	 *            TODO : check password security and length
	 */
	@CheckAuthenticity
	public static void save(@Email @Required String email, @Required String token,
			@CheckWith(PasswordSecurityCheck.class) @Required String password, @Required String firstName,
			@Required String lastName) {
		Logger.trace("Finishing registration for %s", email);

		// check for validation errors
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();

			complete(email, token);
		}

		// everything is fine, we can now safely create our user
		User user = new User();
		user.email = email;
		user.password = password;
		user.firstName = firstName;
		user.lastName = lastName;

		final String defaultLang = Settings.findApplicationSettings().getString(ApplicationSettings.DEFAULT_LANGUAGE);
		Locale defaultLocale = null;
		if(StringUtils.isNotEmpty(defaultLang)) {
			defaultLocale = Lang.getLocale(defaultLang);
		}

		if ((defaultLocale != null) && Play.langs.contains(defaultLang)) {
			user.language = defaultLocale;
		} else {
			user.language = Lang.getLocale();
		}

		user.timezone = Settings.findApplicationSettings().getString(ApplicationSettings.DEFAULT_TIMEZONE);
		user.active = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOACTIVATION);
		user.role = UserRole.findDefaultRole();
		user.save();

		UserGroup defaultGroup = UserGroup.findDefaultGroup();
		if (defaultGroup != null) {
			defaultGroup.addUser(user);
		}

		// now that the user is created, we can delete the pending registration
		Activation.delete("email=?", email);

		// inform superadmins
		UserRegistrationNotification.raise(user);

		// login
		if (user.active) {
			flash.success(Messages.get("registration.profile.success"));
			Auth.succeedAuthentication(email, false);
		}

		flash.success(Messages.get("registration.profile.activation"));
		Auth.login();
	}

	/**
	 * Checks if the activation can be completed
	 */
	@Before(only = { "save", "complete" })
	public static void validateActivation() {
		Activation activation = Activation.find("email=?", params.get("email")).first();
		if (activation == null || !activation.token.equals(params.get("token"))) {
			// TODO handle this error correctly
			redirect("/");
		}
	}
}
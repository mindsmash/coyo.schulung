package controllers;

import checks.PasswordSecurityCheck;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import libs.Protection;
import models.Activation;
import models.Application;
import models.Settings;
import models.User;
import models.UserDevice;
import models.UserNotificationSettings;
import models.UserNotificationSettings.MailInterval;
import notifiers.ApplicationMailer;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import session.UserLoader;
import utils.LanguageUtils;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.LocaleUtils;

/**
 * The Controller for the account settings page.
 *
 * @author Drews Clausen, Jan Marquardt
 */
@AllowExternals
public class Account extends WebBaseController {

	@Before(only = { "password", "email", "saveEmail", "savePassword" })
	static void checkLocal() {
		// only local users can change password and email
		if (!UserLoader.getConnectedUser().isLocal()) {
			index();
		}
	}

	@Before(only = { "delete", "doDelete" })
	static void checkDeleteAllowed() {
		if (!getSettings().getBoolean(ApplicationSettings.ALLOW_ACCOUNT_DELETION)) {
			index();
		}
	}

	public static void index() {
		render();
	}

	public static void password() {
		render();
	}

	public static void email() {
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void saveEmail(@Required @Email String email) {
		if (Validation.hasErrors()) {
			render("@email");
		}
		else if (User.findByEmail(email) != null) {
			flash.error(Messages.get("registration.userAlreadyRegistered"));
			render("@email");
		}
		// brute force protection
		else if (Protection.max(Application.BRUTEFORCE_PROTECTION_MAX, Application.BRUTEFORCE_PROTECTION_BAN)) {
			forbidden("too many submits. please wait a few minutes before trying again.");
		}
		else {
			Activation activation = new Activation(email);
			activation.save();
			ApplicationMailer.activateNewEmail(email, activation.token);
			flash.success(Messages.get("account.email.success"));

			index();
		}
	}

	@Transactional
	public static void activateEmail(String token) {
		Activation activation = Activation.find("byToken", token).first();
		if (activation != null) {
			User user = UserLoader.getConnectedUser();
			user.email = activation.email;
			user.save();
			activation.delete();
			flash.success(Messages.get("account.email.activate.success"));
		}

		index();
	}

	@Transactional
	@CheckAuthenticity
	public static void savePassword(@Required String currentPassword,
			@Required @CheckWith(PasswordSecurityCheck.class) String password, @Required String passwordRepeat) {
		// check current password and add error
		if (!UserLoader.getConnectedUser().authenticate(currentPassword)) {
			validation.addError("currentPassword", "account.password.error.auth");
		}

		validation.equals(passwordRepeat, password);
		if (validation.hasErrors()) {
			validation.keep();
			password();
		}

		User user = UserLoader.getConnectedUser();
		user.setPassword(password);
		user.save();

		flash.success(Messages.get("account.password.success"));
		index();
	}

	public static void settings() {
		List<Locale> locales = LanguageUtils.getApplicationLocales();

		renderArgs.put("intervalTypes", UserNotificationSettings.MailInterval.values());
		renderArgs.put("mailEvents", UserNotificationSettings.getAllMailEvents());
		renderArgs.put("pushEnabled", Settings.findApplicationSettings().getBoolean(ApplicationSettings.PUSH));
		renderArgs.put("userDeviceCount", UserDevice.count("user = ?", UserLoader.getConnectedUser()));

		render(locales);
	}

	@ShowExternals
	@Transactional
	@CheckAuthenticity
	public static void saveSettings(String language, String timezone, @Required List<String> notificationKeys,
			boolean neverMail, MailInterval interval) {
		User user = UserLoader.getConnectedUser();
		user.language = LocaleUtils.toLocale(language);
		user.timezone = timezone;

		// save general settings
		if (!user.validateAndSave()) {
			Validation.keep();
			settings();
		}

		// save notification settings
		if (interval != null) {
			user.notSettings.mailInterval = interval;
		}
		user.notSettings.receiveMails = !neverMail;

		if (!neverMail) {
			user.notSettings.mailEvents.clear();
			if (notificationKeys != null && notificationKeys.size() > 0) {
				user.notSettings.mailEvents.addAll(notificationKeys);
			} else {
				user.notSettings.receiveMails = false;
			}
		}
		user.save();

		flash.success(Messages.get("account.settings.save.success"));
		index();
	}

	public static void delete() {
		render();
	}

	@ShowExternals
	@Transactional
	@CheckAuthenticity
	public static void doDelete(String password) {
		User user = UserLoader.getConnectedUser();
		if (user.authenticate(password)) {
			user.delete();
		} else {
			delete();
		}

		flash.success(Messages.get("account.delete.success"));
		index();
	}
}

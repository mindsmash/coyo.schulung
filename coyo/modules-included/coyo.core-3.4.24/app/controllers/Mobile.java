package controllers;

import models.comments.Comment;
import models.comments.Commentable;
import models.wall.Wall;
import models.wall.post.Post;

public class Mobile extends WebBaseController {

	// ajax
	public static void postOptions(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		render(post);
	}

	// ajax
	public static void postForm(Long wallId) {
		Wall wall = Wall.findById(wallId);
		notFoundIfNull(wall);
		render(wall);
	}

	// ajax
	public static void commentOptions(Long id, Long postId) {
		Comment comment = Comment.findById(id);
		notFoundIfNull(comment);
		Post post = Post.findById(postId);
		notFoundIfNull(post);
		render(comment, post);
	}

	// ajax
	public static void commentForm(Long id, String clazz) {
		Commentable commentable = Collaboration.loadCommentable(id, clazz);
		notFoundIfNull(commentable);
		render(commentable);
	}

	public static void image(String url) {
		render(url);
	}
}

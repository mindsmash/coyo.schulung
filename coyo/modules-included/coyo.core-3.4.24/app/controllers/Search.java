package controllers;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import models.ExtendedSearchableModel;
import models.HashtagBlackList;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.i18n.Messages;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.mvc.Util;
import search.CustomSearcher;
import utils.CacheUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fishr.Fishr;
import fishr.search.SearchableModel;
import fishr.search.Searcher.SearchResult;
import fishr.util.ConvertionUtils;

/**
 * this Controller handles all actions for the search field.
 *
 * @author mindsmash GmbH
 */
public class Search extends WebBaseController {
	public static final int HASHCLOUD_SIZE = 23;
	public static final String HASHTAGS = "hashtags";
	public static final String HASHTAGS_CACHE_TTL = "1min";
	public static int MAX_SEARCH_LENGTH = 200;
	public static int MAX_SEARCH_RESULTS = Integer.parseInt(Play.configuration.getProperty("coyo.search.maxResults",
			"25"));
	public static int MAX_SEARCH_AHEAD_RESULTS = Integer.parseInt(Play.configuration.getProperty(
			"coyo.search.ahead.maxResults", "9"));

	public static void index(String searchString) {
		// check empty
		if (StringUtils.isEmpty(searchString)) {
			render();
		}
		// check too long
		if (searchString.length() > MAX_SEARCH_LENGTH) {
			flash.error(Messages.get("search.error.tooLong"));
			render();
		}

		render(searchString);
	}

	// ajax
	public static void search(String searchString) {
		SearchResult searchResult = performSearch(searchString, MAX_SEARCH_RESULTS, false);
		notFoundIfNull(searchResult);

		JsonArray data = new JsonArray();
		for (String category : searchResult.getCategories()) {
			if (searchResult.getResults(category).isEmpty())
				continue;

			JsonObject categoryObject = new JsonObject();
			categoryObject.addProperty("key", category);
			categoryObject.addProperty("name", Messages.get("search.results." + category));

			JsonArray array = new JsonArray();
			for (SearchableModel model : searchResult.getModels(category)) {
				if (model instanceof ExtendedSearchableModel) {
					JsonObject o = serializeSearchResult((ExtendedSearchableModel) model);
					if (o != null) {
						array.add(o);
					}
				} else {
					Logger.debug(
							"[Search] Skipping %s because it is not of type "
									+ ExtendedSearchableModel.class.getSimpleName(), model);
				}
			}
			categoryObject.add("items", array);
			data.add(categoryObject);
		}

		renderJSON(new GsonBuilder().create().toJson(data));
	}

	private static JsonObject serializeSearchResult(ExtendedSearchableModel esm) {
		try {
			JsonObject o = new JsonObject();
			o.addProperty("displayName", esm.getDisplayName());
			o.addProperty("info", esm.getSearchInfoText());
			o.addProperty("thumbUrl", esm.getSearchThumbSender().getThumbURL());
			o.addProperty("thumbTitle", esm.getSearchThumbSender().getDisplayName());

			Map<String, Object> args = new HashMap<>();
			args.put("id", esm.getId());
			args.put("clazz", esm.getClass().getName());
			ActionDefinition ad = Router.reverse("Search.open", args);
			o.addProperty("url", ad.url);

			return o;
		} catch (Exception e) {
			Logger.warn("[Search] Could not serialize search result: %s", esm);
			Logger.debug(e, "[Search] Could not serialize search result: %s", esm);
		}
		return null;
	}

	// ajax
	public static void ahead(String searchString) {
		if (StringUtils.isEmpty(searchString) || searchString.length() > MAX_SEARCH_LENGTH) {
			error("search string empty or too long");
		}

		SearchResult searchResult = performSearch(searchString, MAX_SEARCH_AHEAD_RESULTS, true);
		renderArgs.put("results", searchResult);
		renderArgs.put("max", MAX_SEARCH_AHEAD_RESULTS);
		render();
	}

	private static SearchResult performSearch(String searchString, int max, boolean ahead) {
		searchString = searchString.trim();
		final Matcher matcher = ConvertionUtils.HASHTAG_PATTERN.matcher(searchString);

		// hashtag ?
		if (matcher.matches()) {
			final String term = matcher.group(ConvertionUtils.HASHTAG_MATCHING_GROUP);

			// hashtag blacklisted? -> return new empty search result
			if (HashtagBlackList.getHashtags().contains(term)) {
				renderArgs.put("blacklistedHashtag", true);
				return new SearchResult();

			} else {
				return new CustomSearcher().searchForHashTag(searchString, max, term);
			}
		}

		/* use default search, but differentiate between ahead and full search */
		if (ahead) {
			return new CustomSearcher().searchAhead(searchString, max);
		}
		return new CustomSearcher().search(searchString, max);
	}

	public static void open(Long id, String clazz) {
		ExtendedSearchableModel searchable = loadSearchable(id, clazz);
		notFoundIfNull(searchable);
		searchable.redirectToResult();
	}

	@Util
	public static ExtendedSearchableModel loadSearchable(Long id, String clazz) {
		try {
			Method m = Class.forName(clazz).getMethod("findById", Object.class);
			return (ExtendedSearchableModel) m.invoke(null, id);
		} catch (Exception e) {
			Logger.warn(e, "error loading searchable object with id [%s] and class [%s]", id, clazz);
		}
		return null;
	}

	/**
	 * Fetch all hashtags but filter the results using the hashtag blacklist
	 *
	 * @return
	 */
	public static void getHashTags(int max) {
		max = max < 1 ? 9999999 : max;
		final boolean filtered;

		if (params._contains("filtered") && !StringUtils.isEmpty(params.get("filtered"))) {
			filtered = Boolean.parseBoolean(params.get("filtered"));
		} else {
			filtered = false;
		}

		if (!filtered) {
			renderJSON(Fishr.getHashTags(max, HashtagBlackList.getHashtags()));

		} else {
			final String key = CacheUtils.createTenantUniqueCacheKey(HASHTAGS);
			final Map<String, Long> hashTags = (Map<String, Long>) play.cache.Cache.get(key);
			if (hashTags != null && !hashTags.isEmpty()) {
				renderJSON(hashTags);
			} else {
				final Map<String, Long> tags = Fishr.getHashTags(max, HashtagBlackList.getHashtags());
				play.cache.Cache.set(key, tags, HASHTAGS_CACHE_TTL);
				renderJSON(tags);
			}
		}
	}
}

package controllers;

import models.UserNotification;
import models.notification.Notification;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import session.UserLoader;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import events.type.DummyEvent;
import java.util.List;

/**
 * 
 * Handles all actions for the notification list.
 * 
 * @author Drews Clausen
 */
@AllowExternals
public class Notifications extends WebBaseController {

	public static void index() {
		renderArgs.put("notifications", UserLoader.getConnectedUser().getNotifications(true, 50));
		render();
	}

	public static void navbarItems() {
		renderArgs.put("notifications",  UserLoader.getConnectedUser().getNotifications(true, 5));
		render();
	}

	// use NotificationsAPI
	@Transactional
	@Deprecated
	@CheckAuthenticity
	public static void markAllAsRead() {
		JPA.em()
				.createQuery(
						"UPDATE UserNotification un SET un.isRead = true WHERE un.isRead = false AND un.user = :user")
				.setParameter("user", UserLoader.getConnectedUser()).executeUpdate();
		new DummyEvent().raise(UserLoader.getConnectedUser());
	}

	@Transactional
	public static void goToSource(Long id) {
		Notification notification = Notification.findById(id);
		notFoundIfNull(notification);

		UserNotification un = UserNotification.find("notification = ? AND user = ?", notification, UserLoader.getConnectedUser())
				.first();
		if (un != null) {
			// mark read
			un.isRead = true;
			// use the JPABase _save method so the modified date is not set again.
			// otherwise the un would appear on the top of the un list after getting marked as read.
			un._save();
		}

		notification.redirectToSource();
	}
}

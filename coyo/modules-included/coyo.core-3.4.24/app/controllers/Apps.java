package controllers;

import java.util.List;

import models.Sender;
import models.app.App;
import org.apache.commons.lang.exception.ExceptionUtils;
import play.Logger;
import play.data.validation.Validation;
import play.db.jpa.JPAPlugin;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Util;
import acl.Permission;
import apps.AppDescriptor;
import apps.AppManager;
import csrf.CheckAuthenticity;

/**
 *
 * Creates, deletes or edits new App Instances for a Page or Workspace.
 *
 * @author Jan Marquardt
 */
public class Apps extends WebBaseController {

	@Util
	private static Sender loadSenderAndCheckEditPermission(Long id) {
		Sender sender = Sender.findById(id);
		if (sender == null) {
			if (request.isAjax()) {
				notFound();
			} else {
				redirect("/");
			}
		}

		// TODO: check if sender can have apps (only page and workspace allowed)

		// security
		if (!Security.check(Permission.EDIT_SENDER, sender.id)) {
			redirectToSender(sender, null);
		}

		renderArgs.put("sender", sender);
		return sender;
	}

	// ajax & normal
	public static void choose(Long id) {
		final Sender sender = loadSenderAndCheckEditPermission(id);
		notFoundIfNull(sender);

		// selection of default apps only for workspaces and pages
		if (sender.canHaveApps()) {
			final List<AppDescriptor> apps = AppManager.getActiveApps(sender);
			render(apps);
		} else {
			ActivityStream.index(null, null);
		}
	}

	public static void create(Long id, String key) throws Exception {
		Sender sender = loadSenderAndCheckEditPermission(id);
		

		AppDescriptor appDescriptor = AppManager.getApp(key);

		App app = (App) appDescriptor.getAppModel().getConstructor().newInstance();

		if (!AppManager.isActive(appDescriptor, sender.getSenderType())) {
			flash.error(Messages.get("apps.inactive"));
			choose(id);
		}

		app.sender = sender;
		app.title = appDescriptor.getName();
		app.beforeForm(request);
		render(app);
	}

	public static void edit(Long id, Long appId) throws Exception {
		loadSenderAndCheckEditPermission(id);

		App app = App.findById(appId);
		notFoundIfNull(app);

		app.beforeForm(request);
		render(app);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(Long id, String key, Boolean redirectToEdit) {
		Sender sender = loadSenderAndCheckEditPermission(id);

		if (key == null) {
			redirectToSender(sender, null);
		}

		// do the binding by hand
		AppDescriptor appDescriptor = AppManager.getApp(key);

		App app = (App) new JPAPlugin().bind(params.getRootParamNode(), "app", appDescriptor.getAppModel(), null, null);

		// check active
		if (!AppManager.isActive(appDescriptor, sender.getSenderType())) {
			flash.error(Messages.get("apps.inactive"));
			choose(id);
		}

		try {
			// allow some binding by hand in app class
			app.bind(request);
		} catch (Exception ex) {
			Logger.error(ex.toString());
			Logger.error(ExceptionUtils.getStackTrace(ex));

			error();
		}


		// last app cannot be made inactive
		if (app.isPersistent() && app.sender.getActiveApps().size() == 0) {
			app.active = true;
		}

		if (Validation.hasErrors() || !app.validateAndSave() || Boolean.TRUE.equals(redirectToEdit)) {
			if (app.isPersistent()) {
				render("@edit", app);
			} else {
				render("@create", app);
			}
		}

		flash.success(Messages.get("app.save.success"));
		redirectToSender((Sender) renderArgs.get("sender"), app.id);
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id, Long appId) {
		loadSenderAndCheckEditPermission(id);

		App app = App.findById(appId);
		if (app != null && app.sender.apps.size() > 0) {
			app.delete();
			flash.success(Messages.get("app.delete.success"));
		}
	}
}

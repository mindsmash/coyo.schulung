package controllers;

import acl.Permission;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import models.medialibrary.MediaFile;
import models.medialibrary.MediaLibrary;
import play.Logger;
import play.data.Upload;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Router;
import storage.FlexibleBlob;
import util.HttpUtil;
import utils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Media libraries interact with the CKEditor
 * 
 * @author Jan Marquardt
 */
@AllowExternals
public class MediaLibraries extends WebBaseController {

	public static void render(Long id, String typeFilter) {
		MediaLibrary library = MediaLibrary.findById(id);
		notFoundIfNull(library);

		// library ID and sender ID are the same
		Security.checkAndCancel(Permission.ACCESS_SENDER, library.getSenderId());

		render(library, typeFilter);
	}

	public static void file(Long id) {
		MediaFile file = MediaFile.findById(id);
		notFoundIfNull(file);

		// library ID and sender ID are the same
		Security.checkAndCancel(Permission.ACCESS_SENDER, file.library.getSenderId());

		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}
		
		renderBinary(file.data.get(), file.name, file.data.length(), file.data.type(), true);
	}

	@Transactional
	@CheckAuthenticity
	public static void upload(Long id, String referrer, boolean returnUrl) {
		MediaLibrary library = MediaLibrary.findById(id);
		notFoundIfNull(library);

		// library ID and sender ID are the same
		Security.checkAndCancel(Permission.ACCESS_SENDER, library.getSenderId());

		Long fileId = null;

		List<Upload> uploads = (List<Upload>) request.args.get("__UPLOADS");
		if (uploads != null) {
			for (Upload upload : uploads) {
				if (upload != null && upload.getSize().longValue() > 0) {
					MediaFile file = new MediaFile();
					file.library = library;
					FlexibleBlob flexibleBlob = new FlexibleBlob();
					flexibleBlob.set(upload.asStream(), upload.getContentType(), upload.getSize());

					if (ImageUtils.isImage(upload)) {
						try {
							final BufferedImage rotatedImage = ImageUtils.rotateImage(flexibleBlob.asFile());
							if (rotatedImage != null) {
								try (InputStream is = ImageUtils
										.createInputStreamFromBufferedImage(rotatedImage, flexibleBlob.type())) {
									flexibleBlob.set(is, upload.getContentType(), upload.getSize());
								}
							}
						} catch (Exception e) {
							Logger.error("[MediaLibraries] Failed to rotate image: " + e.getLocalizedMessage());
						}
					}

					file.data = flexibleBlob;
					file.name = upload.getFileName();
					file.save();
					file.refresh();

					fileId = file.id;
				}
			}
		}

		flash.success(Messages.get("medialibrary.upload.success"));

		if (referrer != null) {
			if (fileId != null) {
				Map<String, Object> args = new HashMap<String, Object>();
				args.put("id", fileId);
				referrer = HttpUtil.appendParam(referrer, "select", Router.reverse("MediaLibraries.file", args));
			}
			redirect(referrer);
		}

		if (returnUrl) {
		}

		// send back url to CKEditor
		render(fileId);
	}
}

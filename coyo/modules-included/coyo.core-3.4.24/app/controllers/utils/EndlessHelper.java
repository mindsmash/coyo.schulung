package controllers.utils;

import java.util.List;

import models.BaseModel;
import play.db.jpa.GenericModel.JPAQuery;
import play.mvc.Controller;
import play.mvc.Util;
import session.UserLoader;
import controllers.Cache;

public class EndlessHelper extends Controller {
	@Util
	public static void prepare(List<? extends BaseModel> data, boolean cache) {
		int start = Math.min((getPage() - 1) * getLength(), data.size());
		int end = Math.min(getPage() * getLength(), data.size());

		use(data.subList(start, end), cache);
	}

	@Util
	public static void prepare(JPAQuery query, boolean cache) {
		List<BaseModel> page = query.fetch(getPage(), getLength());
		use(page, cache);
	}

	@Util
	public static void use(List<? extends BaseModel> data, boolean cache) {
		if (cache) {
			Cache.cacheEntities(data, UserLoader.getConnectedUser().getPrivateCacheKey());
		}
		renderArgs.put("data", data);
	}

	@Util
	public static int getPage() {
		Integer page = params.get("page", Integer.class);
		if (page == null || page == 0) {
			page = 1;
		}
		return page;
	}

	@Util
	public static int getLength() {
		Integer length = params.get("length", Integer.class);
		if (length == null || length == 0) {
			length = 10; // default
		}
		return length;
	}
}

package controllers;

import models.Sender;
import models.User;
import models.embeddables.Link;
import models.wall.attachments.LinkPostAttachment;
import models.wall.post.LikeLinkPost;
import models.wall.post.SimplePost;
import play.db.jpa.Transactional;
import play.mvc.Before;
import play.mvc.Util;
import play.templates.Template;
import play.templates.TemplateLoader;
import session.UserLoader;
import theme.Theme;
import utils.WebLinkInfo;
import conf.ApplicationSettings;
import csrf.CheckAuthenticity;

public class Embeddables extends WebBaseController {

	@Before(priority = 19)
	static void checkAccess() throws Exception {
		// check allowed
		if (!getSettings().getBoolean(ApplicationSettings.EMBEDDABLES)) {
			forbidden("not enabled");
		}

		// check for script
		if (request.actionMethod.equals("script")) {
			script();
		}

		// check logged in
		if (Auth.getCurrentId() == null || !UserLoader.getConnectedUser().active) {
			render("@login");
		}
	}

	@Util
	public static void script() {
		Template template = TemplateLoader.load(Theme.getVFResource("scripts/embeddables.js"));
		response.setHeader("Content-Type", "application/javascript");
		renderHtml(template.render());
	}

	public static void share(String url, boolean form) {
		if (form) {
			WebLinkInfo info = WebLinkInfo.get(url);
			render("@share-form", url, info);
		}
		render(url);
	}

	@Transactional
	@CheckAuthenticity
	public static void doShare(String url, String message) {
		SimplePost post = new SimplePost();
		post.author = UserLoader.getConnectedUser();
		post.wall = UserLoader.getConnectedUser().getDefaultWall();
		post.message = message;
		post.save();

		LinkPostAttachment link = new LinkPostAttachment();
		link.post = post;
		link.url = url;
		link.save();

		// load post into as
		UserLoader.getConnectedUser().getNewActivityStream(true);

		ok();
	}

	public static void follow(Long id) {
		User user = User.findById(id);
		if (user == null) {
			render("@notfound");
		}
		render(user);
	}

	public static void wall(Long id) {
		Sender sender = Sender.findById(id);
		if (sender == null || sender.getDefaultWall() == null) {
			render("@notfound");
		}
		renderArgs.put("wall", sender.getDefaultWall());
		render();
	}

	@Transactional
	public static void comments(String url) {
		Link link = Link.find("url = ?", url).first();
		if (link == null) {
			link = new Link();
			link.url = url;
			link.save();
		}
		render(link, url);
	}

	public static void like(String url, boolean likelist) {
		// check if already liked
		LikeLinkPost post = LikeLinkPost
				.find("SELECT p FROM LikeLinkPost p WHERE p.url = ? AND EXISTS (SELECT pu FROM PostUser pu WHERE pu.post = p AND pu.likes = true AND pu.user = ?)",
						url, UserLoader.getConnectedUser()).first();
		renderArgs.put("liked", post != null);
		render(url, post, likelist);
	}

	@Transactional
	@CheckAuthenticity
	public static void doLike(String url) {
		// check if anybody liked this before
		LikeLinkPost post = LikeLinkPost.find("url = ?", url).first();

		// create if not exists
		if (post == null) {
			post = new LikeLinkPost();
			post.author = UserLoader.getConnectedUser();
			post.wall = UserLoader.getConnectedUser().getDefaultWall();
			post.url = url;
			post.save();

			LinkPostAttachment link = new LinkPostAttachment();
			link.post = post;
			link.url = url;
			link.save();
		}

		// now like
		post.like(UserLoader.getConnectedUser());
		post.save();

		// load post into as
		UserLoader.getConnectedUser().getNewActivityStream(true);

		ok();
	}
}

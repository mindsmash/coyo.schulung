package controllers;

import models.app.FilesAppPublicLink;
import play.db.jpa.Transactional;

/**
 * External access w/o authentication.
 * 
 * @author Jan Marquardt
 */
public class FilesAppExternal extends FilesBaseController {

	@Transactional
	public static void download(String token) {
		FilesAppPublicLink publicLink = FilesAppPublicLink.find("token = ?", token).first();
		if (publicLink == null) {
			Auth.login();
		}

		downloadFile(publicLink.app, publicLink.app.getFile(publicLink.fileUid), null, true);
	}
}
package controllers.open;

import play.db.jpa.NoTransaction;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import storage.FlexibleBlob;
import theme.Theme;

import com.google.gson.Gson;

import controllers.Resource;

/**
 * 
 * Handles all actions for the Application.
 * 
 * @author mindsmash GmbH
 * 
 */
public class Application extends Controller {

	/**
	 * Render messages as a javascript array so it can be used in javascript
	 * files.
	 */
	@NoTransaction
	public static void messages(String lang) {
		// force html template
		request.format = "html";

		response.setContentTypeIfNotSet("application/x-javascript");
		String key = "messages-" + lang;
		String json = play.cache.Cache.get(key, String.class);
		if (null == json) {
			if ("en".equals(lang)) {
				json = new Gson().toJson(play.i18n.Messages.all(null));
			} else {
				json = new Gson().toJson(play.i18n.Messages.all(lang));
			}
			play.cache.Cache.set(key, json, "1h");
		}

		response.cacheFor("24h");
		renderArgs.put("messagesJson", json);
		render();
	}

	/**
	 * Render routes as a javascript so it can be used in javascript files.
	 */
	@NoTransaction
	public static void routes() {
		// force html template
		request.format = "html";
		response.setContentTypeIfNotSet("application/x-javascript");
		response.cacheFor("24h");
		render();
	}

	public static void authenticityToken() {
		request.format = "html";
		response.setContentTypeIfNotSet("application/x-javascript");
		render();
	}

	@Transactional(readOnly = true)
	public static void logo(boolean small, Long version) {
		models.Application application = models.Application.get();
		
		/* if a version was passed, cache the requested logo */
		if (version != null) {
			Resource.cache();
		}
		
		if (small) {
			if (application.smallLogo != null && application.smallLogo.exists()) {
				renderImage("logo-small.png", application.smallLogo, application.smallLogo.type());
			} else {
				renderBinary(Theme.getResource("images/logo-small.png").getAbsoluteFile());
			}
		} else {
			if (application.logo != null && application.logo.exists()) {
				renderImage("logo.png", application.logo, application.logo.type());
			} else {
				renderBinary(Theme.getResource("images/logo.png").getAbsoluteFile());
			}
		}
		
		notFound();
	}

	@Transactional(readOnly = true)
	public static void favicon() {
		models.Application application = models.Application.get();

		if (application.favicon != null && application.favicon.exists()) {
			renderImage("favicon.ico", application.favicon, application.favicon.type());
		}
		renderBinary(Theme.getResource("images/favicon.ico").getAbsoluteFile());
	}

	private static void renderImage(String name, FlexibleBlob blob, String type) {
		renderBinary(blob.get(), name, type, true);
	}
}

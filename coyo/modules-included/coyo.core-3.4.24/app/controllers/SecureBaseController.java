package controllers;

import models.AbstractTenant;
import models.Settings;
import models.User;
import multitenancy.MTA;

import org.apache.commons.lang.StringUtils;

import play.Invoker.Invocation;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.*;
import plugins.PluginManager;
import plugins.internal.TermsOfUsePlugin;
import query.Request;
import session.UserLoader;
import controllers.external.Externals;
import controllers.external.annotations.AllowExternals;
import exceptions.TenantException;
import utils.MobileUtils;

import java.util.Arrays;

/**
 * Base controller for all secured controllers. Auto-loads the connected user.
 */
@With({ Security.class, CSRF.class })
@Transactional(readOnly = true)
public class SecureBaseController extends Controller {

	@Before(priority = 20)
	static void checkAccess() throws Exception {
		User user = UserLoader.getConnectedUser();

		// check logged in
		if (user == null) {
			Logger.debug("no user session found, redirecting to login [destination=%s]", request.url);
			Auth.launchLogin(request.url);
		}

		// check tenant
		final AbstractTenant userTenant = user.tenant;
		final AbstractTenant activeTenant = MTA.getActiveTenant();
		final boolean bothDefined = userTenant != null && activeTenant != null;

		if (!bothDefined || (userTenant.getId() != activeTenant.getId())) {
			Logger.error("Invalid active tenant found. User tenant: % != active tenant: %", user.tenant.toString(), MTA.getActiveTenant().toString());
			Logger.error("request arguments: %", Arrays.toString(request.get().args.entrySet().toArray()));
			Logger.error("request cookies: %", Arrays.toString(request.get().cookies.entrySet().toArray()));

			Auth.logout();
		}

		// check active
		if (!user.active) {
			Auth.logout();
		}

		/*
		 * Redirects a user to the {@link TermsOfUse} controller if the {@link TermsOfUserPlugin} is activated and if
		 * the user has not yet accepted the terms of use.
		 */
		if (user.termsOfUseAccepted == null && !user.superadmin
				&& PluginManager.isActive(PluginManager.getPlugin(TermsOfUsePlugin.KEY))
				&& StringUtils.isNotEmpty(TermsOfUse.getTermsOfUseText())
				&& request.controllerClass != TermsOfUse.class) {
			TermsOfUse.form();
		}

		// check external
		if (user.external) {
			Externals.checkWorkspaceConstraint(user);

			// check if allowed
			if (!request.controllerClass.isAnnotationPresent(AllowExternals.class)
					&& !request.invokedMethod.isAnnotationPresent(AllowExternals.class)) {
				Logger.debug("[SecureBaseController] Denying access to [%s] for external user [%s]", request.action,
						user);

				if (request.isAjax()) {
					forbidden();
				}
				Externals.home();
			}
		}
	}

	@Before(priority = 30)
	static void prepareConnectedUser() {
		User user = UserLoader.getConnectedUser();
//		request.args.put("user", user);
		renderArgs.put("connectedUser", user);
	}

	@Before(priority = 31)
	static void prepareSettings() {
		Settings settings = getSettings();
		request.args.put("settings", settings);
		renderArgs.put("settings", settings);
	}

	@Before(priority = 32)
	static void prepareTenant() {
		AbstractTenant tenant = MTA.getActiveTenant();
		request.args.put("tenant", tenant);
		renderArgs.put("tenant", tenant);
	}

	/*
	 * Check mobile access:
	 * 1. Whether the device is a mobile device (mobileDevice)
	 * 2. Whether the access is via the iOS/Android app (mobileAppAccess)
	 */
	@Before(priority = 38)
	static void mobileChecks() {
		Http.Header userAgentHeader = request.headers.get("user-agent");
		String userAgent = (userAgentHeader != null) ? userAgentHeader.value() : "";
		renderArgs.put("mobileDevice", StringUtils.isEmpty(userAgent) ? false : MobileUtils.isMobileDevice(userAgent));
		renderArgs.put("mobileAppAccess",
				StringUtils.isEmpty(userAgent) ? false : MobileUtils.isMobileCoyoApp(userAgent));
	}

	@Util
	public static Settings getSettings() {
		// check for settings in request
		if (request != null && request.args.containsKey("settings")) {
			return (Settings) request.args.get("settings");
		}
		return Settings.findApplicationSettings();
	}

	@Catch(value = TenantException.class)
	static void handleTenantException(Exception e) {
		Logger.warn(e, "[SecureBaseController] Tenant violation detected for URL: %s", request.url);
		notFound();
	}
}
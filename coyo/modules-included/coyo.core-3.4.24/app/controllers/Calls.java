package controllers;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import acl.Permission;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import json.CallContextSerializer;
import json.CallSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.Call;
import models.Call.Status;
import models.CallContext;
import models.User;
import models.notification.CallNotification;
import play.Logger;
import play.db.helper.JpqlSelect;
import play.db.helper.SqlSelect.Where;
import play.db.jpa.GenericModel;
import play.db.jpa.Transactional;
import session.UserLoader;
import binding.JsonBody;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import csrf.CheckAuthenticity;
import util.ModelUtil;

/**
 * Controller to create and list incoming and outgoing video calls.
 * 
 * TODO:
 * - Security / Permissions
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
@AllowExternals
public class Calls extends WebBaseController {

	public class CallFormData {
		public Status status;
		public List<Long> participants;
	}
	
	private static final String SORTING_KEY_DATE = "date";
	private static final String SORTING_KEY_CONTEXT = "context";

	public static void index() {
		if(!getSettings().findApplicationSettings().getBoolean(ApplicationSettings.MESSAGING_CHAT) ||
				!getSettings().findApplicationSettings().getBoolean(ApplicationSettings.MESSAGING_WEBRTC)) {
			notFound();
		}
		render();
	}

	public static void getCalls(int page, int count, Map<String, String> sorting) {
		User currentUser = UserLoader.getConnectedUser();
		JpqlSelect select = new JpqlSelect();
		
		/* create separate where condition to use with query and count*/
		Where where = select.where();
		where.where("host = ? OR ? IN elements(participants)").params(currentUser, currentUser);
		select.where(where);
		
		/* add order by statement according to sorting */
		if (sorting != null && sorting.size() > 0) {
			if (sorting.containsKey(SORTING_KEY_CONTEXT)) {
				String direction = getDirection(sorting, SORTING_KEY_CONTEXT);
				select.orderBy("context " + direction);
			} else {
				String direction = getDirection(sorting, SORTING_KEY_DATE);
				select.orderBy("created " + direction);
			}
		} else {
			select.orderBy("created ASC");
		}
		
		/* query calls and determine total count */
		List<Call> list = Call.find(select.toString(), select.getParams().toArray()).fetch(page, count);
		Long total = Call.count(where.toString(), where.getParams().toArray());
		
		/* create JSON response containing the paginated list of calls and the total count */
		Gson gson = createBuilder().create();
		JsonObject result = new JsonObject();
		result.add("list", gson.toJsonTree(list));
		result.addProperty("total", total);
		renderJSON(gson.toJson(result));
	}

	@Transactional
	@CheckAuthenticity
	public static void addCall(@JsonBody CallFormData callFormData) {
		List<User> participants = Lists.transform(callFormData.participants, new Function<Long, User>() {
			@Override
			public User apply(Long id) {
				return User.findById(id);
			}
		});

		for(User u : participants) {
			Security.checkAndCancel(Permission.CREATE_WEBRTC_CALL, u);
		}

		Call call = new Call();
		call.status = callFormData.status;
		call.host = UserLoader.getConnectedUser();
		call.participants = new HashSet<User>(participants);
		call.save();

		if(call.status == Status.CANCELED) {
			CallNotification.raise(call);
		}
		
		renderJSON(createBuilder().create().toJson(call));
	}
	
	@Transactional
	@CheckAuthenticity
	public static void save(Long id, String clazz, @JsonBody Call call) {
		if (!call.validateAndSave()) {
			error("validation errors");
		}

		for(User u : call.participants) {
			Security.checkAndCancel(Permission.CREATE_WEBRTC_CALL, u);
		}

		/* id and clazz are allowed to be null */
		if((id!= null) && (clazz != null)) {
			GenericModel gModel = loadCallContext(id, clazz);
			if(gModel != null) {
				call.contextUid = ModelUtil.serialize(gModel);
			} else {
				call.contextUid = "";
			}
		} else {
			call.contextUid = "";
		}
		call.save();
		renderJSON(createBuilder().create().toJson(call));
	}

	private static GenericModel loadCallContext(Long id, String clazz) {
		try {
			Method m = Class.forName(clazz).getMethod("findById", Object.class);
			return (GenericModel) m.invoke(null, id);
		} catch (Exception e) {
			Logger.warn(e, "error loading CallContext object with id [%s] and class [%s]", id, clazz);
		}
		return null;
	}

	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(Call.class, new CallSerializer(UserLoader.getConnectedUser()));
		builder.registerTypeHierarchyAdapter(CallContext.class, new CallContextSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		return builder;
	}
	
	private static String getDirection(Map<String, String> sorting, String key) {
		return sorting.get(key).equalsIgnoreCase("asc") ? "asc" : "desc";
	}

}

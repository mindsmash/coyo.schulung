package controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import models.Sender;
import models.User;
import models.app.App;
import models.event.Event;
import models.page.Page;
import models.workspace.Workspace;
import onlinestatus.OnlineStatusServiceFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import play.Play;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Http;
import play.mvc.Http.Header;
import play.mvc.Router;
import play.mvc.Scope;
import play.mvc.Util;
import session.UserLoader;
import utils.JPAUtils;
import controllers.api.SSOTokenAPI;
import utils.MobileUtils;

/**
 * Base controller for all web controllers that require a login.
 */
public class WebBaseController extends SecureBaseController {

	private final static String XSRF_TOKEN_NAME = "XSRF-TOKEN";
	private static final boolean COOKIE_SECURE = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.auth.cookieSecure", "false"));

	@Before(priority = 17)
	@Deprecated
	static void checkTokenAuth(boolean redirect) {
		// allows some requests to single-time authenticate via a token
		// e.g. used by WebLinkInfo, update mail for loading thumbs and iCal export
		// DEPRECATED: Use SSO Token for authentication
		if (TokenAuthController.handleTokenAuthentication() && redirect) {
			redirect("/");
		}
	}

	@Before(priority = 18)
	static void checkSSOTokenAuth(boolean redirect) {
		// allows some requests to single-time authenticate via a token
		// request token by API POST call to /api/sso
		if (SSOTokenAPI.handleTokenAuthentication() && redirect) {
			redirect("/");
		}
	}

	/**
	 * Set session cookie with authenticity token for angular. The token is set if no token exists or if the token
	 * changed it's value. See: https://code.angularjs.org/1.2.24/docs/api/ng/service/$http
	 */
	@Before(priority = 33)
	static void setXsrfCookie() {
		Http.Cookie xsrfToken = request.cookies.get(XSRF_TOKEN_NAME);
		if (xsrfToken == null || !xsrfToken.value.equals(Scope.Session.current().getAuthenticityToken())) {
			String token = Scope.Session.current().getAuthenticityToken();
			response.setCookie(XSRF_TOKEN_NAME, token, null, "/", null, COOKIE_SECURE);
		}
	}
	
	/**
	 * Check whether session must be refreshed, e.g. if it is nearly expired.
	 */
	@Before(priority = 35)
	static void refreshSession() {
		if (!request.isAjax()) {
			Auth.refresh();
		}
	}

    /*
     * Check mobile access:
     * 1. Whether the device is a mobile device (mobileDevice)
     * 2. Whether the access is via the iOS/Android app (mobileAppAccess)
     */
	@Before(priority = 38)
	static void mobileChecks() {
		Header userAgentHeader = request.headers.get("user-agent");
		String userAgent = (userAgentHeader != null) ? userAgentHeader.value() : "";
		renderArgs.put("mobileDevice", StringUtils.isEmpty(userAgent) ? false : MobileUtils.isMobileDevice(userAgent));
		renderArgs.put("mobileAppAccess", StringUtils.isEmpty(userAgent) ? false : MobileUtils.isMobileCoyoApp(userAgent));
	}

	@Before(priority = 40)
	static void updateUserOnlineStatus() {
		OnlineStatusServiceFactory.getOnlineStatusService().refreshOnlineStatus(UserLoader.getConnectedUser());
	}

	@Before(priority = 50)
	static void checkExpiredPassword() throws Exception {
		User connectedUser = UserLoader.getConnectedUser();

		// TODO: move to settings
		int expireAfterDays = 0; // days

		if (connectedUser.resetPassword
				|| (expireAfterDays > 0 && (connectedUser.passwordModified == null || connectedUser.passwordModified
						.before(DateUtils.addDays(new Date(), -1 * expireAfterDays))))) {

			String token = Auth.enableResetPassword(connectedUser.authUid, false);

			JPAUtils.makeTransactionWritable();

			flash.error(Messages.get("account.password.expired.notice"));
			Auth.executeLogout(); // logout
			Auth.resetPassword(connectedUser.authUid, token);
		}
	}

	@Util
	public static void redirectToReferrer() {
		String referrer = "/";
		if (request.headers.containsKey("referer")) {
			referrer = request.headers.get("referer").value();
		} else if (request.params._contains("referrer")) {
			referrer = request.params.get("referrer");
		}
		redirect(referrer);
	}

	@Util
	public static void redirectToSender(Sender sender) {
		redirectToSender(sender, null, new HashMap<String, Object>());
	}

	@Util
	public static void redirectToSender(Sender sender, Long appId) {
		redirectToSender(sender, appId, new HashMap<String, Object>());
	}

	@Util
	public static void redirectToSender(Sender sender, Long appId, Map<String, Object> args) {
		if (sender != null) {
			args.put("id", sender.id);
			args.put("appId", appId);
			args.put("slug", sender.getSlug());

			if (appId != null) {
				App app = App.findById(appId);
				if (app != null && app.sender == sender) {
					args.put("appSlug", app.getSlug());
				}
			}

			if (sender instanceof User) {
				Users.wall(sender.id, sender.getSlug());
			} else if (sender instanceof Event) {
				Events.show(sender.id, sender.getSlug());
			} else if (sender instanceof Page) {
				redirect(Router.reverse("Pages.show", args).url);
			} else if (sender instanceof Workspace) {
				redirect(Router.reverse("Workspaces.show", args).url);
			}
		}
		redirect("/");
	}
}

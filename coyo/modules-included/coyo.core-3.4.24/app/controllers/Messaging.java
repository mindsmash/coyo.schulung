package controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import models.Settings;
import models.User;
import models.messaging.Conversation;
import models.messaging.ConversationEvent;
import models.messaging.ConversationMember;
import models.messaging.ConversationMember.ConversationMemberJsonSerializer;
import models.messaging.ConversationStatus;
import models.wall.post.ConversationPost;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.Transactional;
import play.mvc.Before;
import plugins.play.PushNotificationPlugin;
import security.Secure;
import session.UserLoader;
import util.Util;
import acl.Permission;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import events.type.NewConversationPostEvent;

/**
 * Handles all actions for the private messaging system and the chat.
 *
 * @author mindsmash GmbH
 */
@AllowExternals
@ShowExternals
public class Messaging extends WebBaseController {

	@Before
	static void checkActive() {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.MESSAGING)) {
			redirect("/");
		}
	}

	/**
	 * Renders the portal and opens the conversation with the conversation member defined by id.
	 *
	 * @param id
	 *            the conversation member id of the conversation to open.
	 */
	public static void portal(Long id) {
		render();
	}

	// ajax
	public static void conversations(Integer max) {
		List<ConversationMember> conversations = UserLoader.getConnectedUser().getConversations(ConversationStatus.INBOX, max);
		render(conversations);
	}

	// modal
	public static void create(Long id, boolean redirectToPortal) {
		if (id != null) {
			renderArgs.put("user", User.findById(id));
		}
		render(redirectToPortal);
	}

	// modal
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void invite(Long id) {
		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);
		render(cm);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(Set<Long> members, String message, boolean redirectToPortal) {
		// error("at least two users are required for a conversation");
		if (members == null || members.size() == 0) {
			portal(null);
		}

		for (Long userId : members) {
			if (!Security.check(Permission.CREATE_CONVERSATION, userId)) {
				portal(null);
			}
		}

		members.add(UserLoader.getConnectedUser().id);
		Conversation conversation = null;

		// try to find existing bi-directional conversation
		if (members.size() == 2) {
			conversation = Conversation
					.find("SELECT c FROM Conversation c WHERE SIZE(c.members) = 2 AND 2 = (SELECT COUNT(cm) FROM ConversationMember cm WHERE cm.conversation.id = c.id AND cm.user.id IN ("
							+ Util.implode(new ArrayList<Long>(members), ",") + "))").first();
		}

		// if non exists, create
		if (conversation == null) {
			conversation = new Conversation();

			for (Long id : members) {
				ConversationMember member = new ConversationMember();
				member.conversation = conversation;
				member.user = User.findById(id);
				conversation.members.add(member);
			}

			conversation.save();
			conversation.refresh();
		} else {
			// reactivate
			for (ConversationMember cm : conversation.members) {
				if (cm.isDeleted()) {
					cm.status = ConversationStatus.INVISIBLE;
					cm.save();
				}
			}
		}

		ConversationMember cm = conversation.getMemberObject(UserLoader.getConnectedUser());
		cm.updateActive();
		cm.save();

		// add post
		addPost(cm, message);

		if (redirectToPortal) {
			portal(cm.id);
		}

		cm.refresh();
		renderJSON(cm, new ConversationMemberJsonSerializer());
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void doInvite(Long id, List<Long> members) {
		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		for (Long userId : members) {
			if (!Security.check(Permission.CREATE_CONVERSATION, userId)) {
				continue;
			}
			User user = User.findById(userId);
			if (user != null) {
				// if user already in conversation
				ConversationMember ocm = cm.conversation.getMemberObject(user);
				if (ocm == null) {
					ocm = new ConversationMember();
					ocm.conversation = cm.conversation;
					ocm.user = user;
					ocm.status = ConversationStatus.INBOX;
					ocm.save();

					addEvent(ocm, ConversationEvent.JOIN);
				} else if (ocm.isDeleted()) {
					// if user deleted conversation before, let him join again
					ocm.status = ConversationStatus.INBOX;
					// show posts from now on
					ocm.showFrom = new Date();
					ocm.save();

					addEvent(ocm, ConversationEvent.JOIN);
				}
			}
		}

		portal(cm.id);
	}

	@Transactional
	@Secure(permissions = Permission.ACCESS_CONVERSATION_MEMBER, params = "id")
	public static void load(Long id, Long beforeId, Long afterId, Long modifiedAfter, boolean chat) {
		ConversationMember cm = ConversationMember.findById(id);
		notFoundIfNull(cm);

		// update last access date
		cm.lastAccess = new Date();
		cm.save();

		JPAQuery query = cm.getPostsQuery(beforeId, afterId, modifiedAfter);
		if (params._contains("page")) {
			EndlessHelper.prepare(query, false);
		} else {
			renderArgs.put("data", query.fetch());
		}

		response.setHeader("x-conversation-modified", cm.conversation.modified.getTime() + "");
		render(cm, chat);
	}

	@play.mvc.Util
	public static ConversationPost addPost(ConversationMember cm, String message) {
		Security.checkAndCancel(Permission.ACCESS_CONVERSATION_MEMBER, cm.id);

		final ConversationPost post = new ConversationPost();

		post.author = UserLoader.getConnectedUser();
		post.message = message;
		post.wall = cm.conversation.getDefaultWall();
		post.conversation = cm.conversation;

		post.save();

		// handle possible uploads
		Posts.handlePostAttachments(post);

		// update modified date in conversation
		cm.conversation.save();

		// update last access date
		cm.lastAccess = new Date();
		cm.save();

		// move to inbox and reactivate
		for (ConversationMember member : cm.conversation.members) {
			member.updateActive();
			member.status = ConversationStatus.INBOX;
			member.save();
			new NewConversationPostEvent(member).raise(member.user);

			// push to all except author
			if (member.getId() != cm.getId()) {
				PushNotificationPlugin.push(member, post);
			}
		}

		return post;
	}

	@play.mvc.Util
	public static ConversationPost addEvent(ConversationMember cm, ConversationEvent event) {
		ConversationPost post = new ConversationPost();
		post.event = event;
		post.author = cm.user;
		post.conversation = cm.conversation;
		post.wall = cm.conversation.getDefaultWall();
		post.save();

		// update modified date in conversation
		cm.conversation.save();

		/*
		 * Notify all members except the author of this event. Since events are wrapped into posts we have to raise a
		 * newConversationPostEvent.
		 */
		for (ConversationMember member : cm.conversation.members) {
			new NewConversationPostEvent(member).raise(member.user);

			// push to all except author
			if (member != cm) {
				PushNotificationPlugin.push(member, post);
			}
		}

		return post;
	}
}

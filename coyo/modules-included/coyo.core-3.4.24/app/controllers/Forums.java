package controllers;

import java.util.ArrayList;
import java.util.List;

import json.FileAttachmentSerializer;
import json.ForumAppTopicSerializer;
import json.ForumPostSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.User;
import models.app.ForumApp;
import models.app.forum.ForumAppTopic;
import models.app.forum.ForumPost;
import models.notification.ForumPostNotification;
import models.notification.ForumTopicNotification;
import models.wall.Wall;
import models.wall.attachments.FilePostAttachment;
import models.wall.attachments.PostAttachment;
import models.wall.post.Post;
import play.db.jpa.Transactional;
import session.UserLoader;
import utils.ModifiedDateComparator;
import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import controllers.Uploads.TempUpload;
import controllers.external.annotations.AllowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import edu.emory.mathcs.backport.java.util.Collections;

public class Forums extends WebBaseController {

	public class TopicFormData {
		public String title;
		public String content;
		public List<String> attachments;
	}

	public class PostFormData {
		public String content;
		public List<String> attachments;
	}

	@AllowExternals
	public static void getTopic(Long topicId) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		notFoundIfNull(topic);

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, topic.app.id);

		renderJSON(createBuilder().create().toJson(topic));
	}

	@AllowExternals
	public static void getTopicByPost(Long postId) {
		ForumPost post = ForumPost.findById(postId);
		notFoundIfNull(post);
		ForumAppTopic topic = (ForumAppTopic) post.wall;

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, topic.app.id);

		List<Post> posts = topic.getPosts();
		int index = posts.indexOf(post);
		int page = (int) (index / EndlessHelper.getLength()) + 1;

		Gson gson = createBuilder().create();
		JsonElement jsonResult = gson.toJsonTree(topic);
		jsonResult.getAsJsonObject().addProperty("page", page);
		renderJSON(gson.toJson(jsonResult));
	}

	@AllowExternals
	public static void getPosts(Long topicId) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		notFoundIfNull(topic);

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, topic.app.id);

		EndlessHelper.prepare(topic.getPosts(), true);
		renderJSON(createBuilder().create().toJson(renderArgs.get("data")));
	}

	@AllowExternals
	public static void getTopicList(Long appId) {
		models.app.ForumApp forumApp = models.app.ForumApp.findById(appId);
		notFoundIfNull(forumApp);
		notFoundIfNull(forumApp.topics);

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, forumApp.id);

		// sort topics
		// TODO: do via SQL / JPQL
		List<ForumAppTopic> sortedTopics = new ArrayList(forumApp.topics);
		Collections.sort(sortedTopics, (new ModifiedDateComparator(true)));

		// apply paging
		EndlessHelper.prepare(sortedTopics, true);
		renderJSON(createBuilder().create().toJson(renderArgs.get("data")));
	}

	@Transactional
	@AllowExternals
	@CheckAuthenticity
	public static void deleteTopic(Long topicId) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		notFoundIfNull(topic);

		// check permissions
		Security.checkAndCancel(AppsPermission.EDIT_FORUM_TOPIC, topic.id);

		topic.delete();
		ok();
	}

	@Transactional
	@AllowExternals
	@CheckAuthenticity
	public static void deletePost(Long postId, Long topicId) {
		Post post = ForumPost.findById(postId);
		notFoundIfNull(post);

		Wall topic = post.wall;

		// check permissions
		Security.checkAndCancel(AppsPermission.DELETE_FORUM_POST, post.id, topic.id);

		post.delete();
		topic.save();
		ok();
	}

	@Transactional
	@AllowExternals
	@CheckAuthenticity
	public static void addPost(Long topicId, @JsonBody PostFormData formData) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		notFoundIfNull(topic);

		// check permissions
		Security.checkAndCancel(AppsPermission.ADD_FORUM_POST, topic.id);

		User author = UserLoader.getConnectedUser();
		notFoundIfNull(author);

		// create post
		Post post = new ForumPost();
		post.message = formData.content;
		post.author = author;
		post.wall = topic;
		post.save();

		// handle attachments
		addAttachments(post, formData.attachments);

		// save topic
		topic.save();

		// notify author
		ForumPostNotification.raise(topic, post);

		// render post as JSON when done
		renderJSON(createBuilder().create().toJson(post));
	}

	@Transactional
	@AllowExternals
	@CheckAuthenticity
	public static void createTopic(Long appId, @JsonBody TopicFormData formData) {
		ForumApp app = ForumApp.findById(appId);
		notFoundIfNull(app);

		// check permissions
		Security.checkAndCancel(AppsPermission.CREATE_FORUM_TOPIC, app.id);

		User author = UserLoader.getConnectedUser();
		notFoundIfNull(author);

		// create topic
		ForumAppTopic topic = new ForumAppTopic(app, author, formData.title, formData.content);
		topic.save();

		// handle attachments
		addAttachments(topic.getPosts().get(0), formData.attachments);

		app.topics.add(topic);
		app.save();

		ForumTopicNotification.raise(topic);

		// return topic as json when done
		renderJSON(createBuilder().create().toJson(topic));
	}

	@Transactional
	@AllowExternals
	@CheckAuthenticity
	public static void setTopicClosed(Long topicId, Boolean closed) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		notFoundIfNull(topic);

		// check permissions
		Security.checkAndCancel(AppsPermission.EDIT_FORUM_TOPIC, topic.id);

		topic.closed = closed;
		topic.save();

		// return topic as json when done
		renderJSON(createBuilder().create().toJson(topic));
	}

	/**
	 * Helper method to add attachments to a post. The post is refreshed afterwards. The method is null safe regarding
	 * the list of attachments.
	 */
	private static void addAttachments(Post post, List<String> attachments) {
		// handle attachments
		if (attachments != null) {
			for (String idString : attachments) {
				TempUpload upload = Uploads.loadUpload(idString);
				if (upload != null) {
					FilePostAttachment fa = new FilePostAttachment();
					fa.file = upload.blob;
					fa.name = upload.filename;
					fa.post = post;
					fa.save();
				}
			}
			post.refresh();
		}
	}

	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(ForumAppTopic.class, new ForumAppTopicSerializer());
		builder.registerTypeAdapter(ForumPost.class, new ForumPostSerializer());
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(PostAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		return builder;
	}

}
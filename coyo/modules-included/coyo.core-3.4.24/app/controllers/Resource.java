package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import lombok.extern.slf4j.Slf4j;
import models.Sender;

import org.joda.time.DateTime;

import play.libs.Crypto;
import play.mvc.Util;
import play.mvc.With;
import play.utils.Utils;
import theme.Theme;
import utils.PreviewUtils;
import validation.checks.SimpleURLCheck;
import acl.Permission;
import controllers.external.annotations.AllowExternals;

/**
 * Renders scripts as templates, so we can use the template engine inside script
 * files.
 * 
 * @author Marko Ilic, Jan Marquardt
 */
@Slf4j
@AllowExternals
@With(TokenAuthController.class)
public class Resource extends SecureBaseController {

	private static final int CACHE_DURATION_SECONDS = 1296000;

	private static Sender loadAndCacheSender(Long senderId) {
		notFoundIfNull(senderId);
		Sender sender = Sender.findById(senderId);
		notFoundIfNull(sender);
		cache();
		return sender;
	}

	public static void senderAvatar(Long senderId, /* used for browser caching */Long version)
			throws FileNotFoundException {
		Sender sender = loadAndCacheSender(senderId);

		if (sender == null || !sender.avatar.exists() || !Security.check(Permission.LIST_SENDER, senderId)) {
			// important to leave input stream rendering, not file, to avoid
			// play caching
			response.setContentTypeIfNotSet("image/png");
			renderBinary(new FileInputStream(Theme.getResource("images/" + sender.getSenderType() + "-nopic.png")));
		}

		response.setContentTypeIfNotSet(sender.avatar.type());
		renderBinary(sender.avatar.get());
	}

	public static void senderThumb(Long senderId, /* used for browser caching */Long version)
			throws FileNotFoundException {
		Sender sender = loadAndCacheSender(senderId);

		if (sender == null || !sender.thumbnail.exists() || !Security.check(Permission.LIST_SENDER, senderId)) {
			// important to leave input stream rendering, not file, to avoid
			// play caching
			response.setContentTypeIfNotSet("image/png");
			renderBinary(new FileInputStream(Theme.getResource("images/" + sender.getSenderType() + "-nothumb.png")));
		}

		response.setContentTypeIfNotSet(sender.thumbnail.type());
		renderBinary(sender.thumbnail.get());
	}

	/**
	 * Renders an image in a modal dialog.
	 */
	// ajax
	public static void ajaxImage(String url, String downloadUrl) {
		SimpleURLCheck check = new SimpleURLCheck();
		// check for absolute or relative URLs
		if (!check.isSatisfied(null, url, null, null) && !url.startsWith("/")) {
			error("invalid URL");
		}
		if (!check.isSatisfied(null, downloadUrl, null, null) && !downloadUrl.startsWith("/")) {
			error("invalid URL");
		}
		render(url, downloadUrl);
	}

	/**
	 * Decrypts the preview image ID and renders the requested preview image
	 * @param id The AES encrypted sender ID + preview image key
	 * @throws Exception
	 */
	public static void imagePreview(String id) throws Exception {
		String info = "";
		try {
			info = Crypto.decryptAES(id);
		} catch(Exception ex) {
			Object[] args = {id, request.url};
			log.error("[Resource] Error decrypting id '{}' on URL '{}'", args, ex);
			notFound();
		}
		if(info.contains("----")) {
			String[] infoSplit = info.split("----");
			if(infoSplit.length == 2) {
				Long senderId = -1L;
				try {
					senderId = Long.valueOf(infoSplit[0]);
				} catch(NumberFormatException ex) {
					notFound();
				}
				if(!Security.check(Permission.ACCESS_SENDER, senderId)) {
					notFound();
				} else {
					cache();
					InputStream previewImageStream = PreviewUtils.getPreview(infoSplit[1], "image/png").get();
					notFoundIfNull(previewImageStream);
					renderBinary(previewImageStream, "preview.png");
				}
			}
		} else {
			notFound();
		}
	}

	public static void authToken() {
		render();
	}
	
	@Util
	public static void cache() {
		// browser cache for 15 days
		response.setHeader("Cache-Control", "max-age=" + CACHE_DURATION_SECONDS);
		DateTime expiration = new DateTime().plusSeconds(CACHE_DURATION_SECONDS);
		response.setHeader("Expires", Utils.getHttpDateFormatter().format(expiration.toDate()));
	}
}

package controllers;

import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;
import com.google.gson.GsonBuilder;
import containers.form.ItemFormData;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import extensions.CommonExtensions;
import fishr.Fishr;
import fishr.query.Query;
import fishr.query.QueryBlock;
import fishr.search.SearchableResult;
import fishr.search.Searcher;
import json.Dates;
import json.FileAttachmentSerializer;
import json.MinimalUserSerializer;
import json.comments.CommentSerializer;
import json.innoapp.IdeaRatingSerializer;
import json.innoapp.IdeaSerializer;
import json.innoapp.InnoAppCategorySerializer;
import json.innoapp.InnoAppSerializer;
import json.innoapp.MinimalIdeaSerializer;
import json.list.ContainerSerializer;
import json.list.FieldOptionSerializer;
import json.list.FieldSerializer;
import json.list.ItemValueSerializer;
import models.Sender;
import models.User;
import models.app.inno.Category;
import models.app.inno.FilterParams;
import models.app.inno.Idea;
import models.app.inno.IdeaDAO;
import models.app.inno.IdeaDTO;
import models.app.inno.IdeaRating;
import models.app.inno.Status;
import models.comments.Comment;
import models.container.Container;
import models.container.Field;
import models.container.ItemValue;
import models.container.fields.OptionsFieldOption;
import models.container.values.attachments.FileItemAttachment;
import models.notification.innoapp.IdeaChangedNotification;
import models.notification.innoapp.IdeacreatedNotification;
import models.page.Page;
import models.workspace.Workspace;
import multitenancy.MTA;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Router;
import security.Secure;
import session.UserLoader;
import utils.DateUtils;
import utils.ErrorHelper;

import javax.persistence.TypedQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Innovation app controller class
 *
 * @author Gerrit Dongus
 */
public class InnoApp extends Containers {
	private static final int SIMILAR_IDEAS_MAX_RESULTS = 3;
	private static final Logger LOG = LoggerFactory.getLogger(InnoApp.class);
	private static final String CSV_ENCODING = "ISO-8859-1";
	private static final String CSV_MIMETYPE = "text/csv";
	private static final String CSV_CONTENT_TYPE = CSV_MIMETYPE + ";charset=" + CSV_ENCODING;

	private static class InnoAppSearcher extends Searcher {
		public final static int PAGE_SIZE = 50;

		/**
		 * Looks for similar ideas by searching for terms of the new idea in all existing ideas of the same app.
		 *
		 * Note: This method only considers the title. The search for similar descriptions was deactivated since this
		 * takes way too long. We would need a similarity search (e.g. MLT in Elastic Search) here instead.
		 *
		 * @param idea
		 *            the idea for which similar ideas should be found.
		 * @param page
		 *            the page on which to look for. Should be set to '0' for the initial call.
		 * @return a Set of found results or an empty Set of no results could be found.
		 */
		public Set<SearchableResult> searchSimilar(Idea idea, final int page) {
			final Query query = Fishr.createQuery(Idea.class);
			final String searchStringTitle = StringUtils.trim(idea.title);

			final QueryBlock block = query.orBlock();

			// split search terms
			for (String term : util.Util.parseList(searchStringTitle, " ")) {
				block.orBlock().or(Fishr.ALL_FIELD, term, "~" + getFuzzyFactor()).or(Fishr.ALL_FIELD, term, "*~");
			}

			// always try exact expression search as well
			query.or(Fishr.ALL_FIELD, searchStringTitle, true);

			LOG.debug("[InnoApp] Find similar ideas query string: '{}'", query.toString());
			return executeQuery(query, Idea.class, PAGE_SIZE);
		}
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void getIdeasFiltered() {

		final FilterParams filterParams = controllers.InnoApp.createBuilder().create()
				.fromJson(request.params.get("body"), FilterParams.class);

		final Map<String, Object> result = new HashMap<>();

		result.put("ideas", IdeaDAO.findIdeasFilteredAndPaged(filterParams));
		result.put("count", IdeaDAO.countIdeasFilteredAndPaged(filterParams));

		final GsonBuilder builder = createBuilder();
		builder.registerTypeAdapter(Idea.class, new MinimalIdeaSerializer(UserLoader.getConnectedUser(), null));
		final String jsn = builder.create().toJson(result);

		renderJSON(jsn);
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void getModerators(final Long id) {
		notFoundIfNull(id);

		final models.app.InnoApp app = models.app.InnoApp.findById(id);
		notFoundIfNull(app);

		renderJSON(createBuilder().create().toJson(app.moderators));
	}

	@CheckAuthenticity
	public static void getStatusList() {
		renderJSON(Status.values());
	}

	@Transactional
	@CheckAuthenticity
	@AllowExternals
	@Secure(permissions = Permission.ACCESS_APP, params = { "id" })
	public static void getApp(final Long id, final Boolean cached) {
		notFoundIfNull(id);

		String jsn = (String) play.cache.Cache.get(models.app.InnoApp.getCacheId(id, MTA.getActiveTenant().toString()));

		if (StringUtils.isEmpty(jsn) || cached != null && cached == false) {
			final models.app.InnoApp app = models.app.InnoApp.findById(id);
			notFoundIfNull(app);

			final GsonBuilder builder = createBuilder();
			builder.registerTypeAdapter(Idea.class, new MinimalIdeaSerializer(UserLoader.getConnectedUser(), app));
			jsn = builder.create().toJson(app);

			play.cache.Cache.set(models.app.InnoApp.getCacheId(app.id, app.tenant.toString()), jsn, "1h");
		}

		renderJSON(jsn);
	}

	/**
	 * Add current users rating to an idea
	 */
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void rateIdea(@Required Long ideaId, int rating) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Idea idea = Idea.findById(ideaId);
		notFoundIfNull(idea);

		if (idea.status == Status.NEW) {
			idea.rate(UserLoader.getConnectedUser(), rating);
			renderJSON(createBuilder().create().toJson(idea));
		} else {
			error();
		}
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void withdrawRating(@Required final Long ideaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final User user = UserLoader.getConnectedUser();
		final IdeaRating rating = IdeaRating.find("user = ? AND idea.id = ?", user, ideaId).first();
		notFoundIfNull(rating);

		if (rating.idea.status == Status.NEW) {
			rating.idea.ratings.remove(rating);
			rating.idea.save();
			rating.delete();

			renderJSON(createBuilder().create().toJson(rating.idea));
		} else {
			error();
		}
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void follow(@Required final Long ideaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Idea idea = Idea.findById(ideaId);
		notFoundIfNull(idea);

		idea.follow(UserLoader.getConnectedUser());
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void unfollow(@Required final Long ideaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Idea idea = Idea.findById(ideaId);
		notFoundIfNull(idea);

		idea.unfollow(UserLoader.getConnectedUser());
	}

	@Secure(permissions = { Permission.DELETE_CONTAINER_ITEM }, params = { "ideaId" })
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void deleteIdea(@Required Long containerId, @Required Long ideaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		Container container = loadContainer(containerId);
		notFoundIfNull(container);

		Idea idea = Idea.findById(ideaId);
		notFoundIfNull(idea);

		final String sql = "SELECT idea FROM Idea AS idea WHERE :idea MEMBER OF idea.linkedIdeas";
		final TypedQuery<Idea> q = Idea.em().createQuery(sql, Idea.class);
		q.setParameter("idea", idea);
		final List<Idea> ideas = q.getResultList();

		for (Idea referencedIdea : ideas) {
			referencedIdea.linkedIdeas.remove(idea);
			referencedIdea.save();
		}

		Category.removeIdea(idea, idea.getApp());
		idea.delete();
		container.items.remove(idea);
		container.save();
	}

	/**
	 * Create new idea
	 *
	 * @param containerId
	 * @param categoryId
	 * @param ideaDTO
	 */
	@Secure(permissions = Permission.CREATE_CONTAINER_ITEM, params = { "containerId" })
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void createIdea(Long containerId, Long categoryId, @Required @Valid @JsonBody IdeaDTO ideaDTO) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Container container = Containers.loadContainer(containerId);
		notFoundIfNull(container);

		Idea idea = new Idea(container, UserLoader.getConnectedUser());
		idea = updateNewOrExistingIdea(idea, ideaDTO, categoryId, container);
		renderJSON(createBuilder().create().toJson(idea));
	}

	/**
	 * Update idea
	 *
	 * @param ideaId
	 * @param categoryId
	 * @param ideaDTO
	 */
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void editIdea(Long ideaId, Long categoryId, @Required @Valid @JsonBody IdeaDTO ideaDTO) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		Idea idea = Idea.findById(ideaId);
		notFoundIfNull(idea);

		final models.app.InnoApp app = idea.getApp();

		if (Security.check(AppsPermission.MODERATE_IDEA, app.sender, UserLoader.getConnectedUser(), app.id)
				|| Security.check(AppsPermission.EDIT_IDEA, app.sender, UserLoader.getConnectedUser(), app.id, idea.id)) {
			idea = updateNewOrExistingIdea(idea, ideaDTO, categoryId, null);
			renderJSON(createBuilder().create().toJson(idea));
		} else {
			forbidden();
		}
	}

	/**
	 * Creates or updates an idea. For internal use.
	 *
	 * @param idea
	 * @param ideaDTO
	 * @param categoryId
	 * @param container
	 */
	private static Idea updateNewOrExistingIdea(final Idea idea, final IdeaDTO ideaDTO, final Long categoryId,
			final Container container) {
		notFoundIfNull(idea);
		notFoundIfNull(ideaDTO);

		if (!ideaDTO.formData.isEmpty()) {
			ItemFormData.updateItem(idea, ideaDTO.formData);
		}
		final boolean isNew = idea.id == null;

		idea.title = ideaDTO.title;
		idea.description = ideaDTO.description;
		idea.hiddenField = ideaDTO.hiddenField;
		idea.isAnon = ideaDTO.isAnon;

		if (!idea.validateAndSave()) {
			badRequest(validation.errorsMap());
		}
		Category.addIdea(idea, categoryId);

		if (container != null) {
			container.refresh();
		}

		if (isNew) {
			IdeacreatedNotification.raise(idea);
		} else {
			IdeaChangedNotification.raise(idea);
		}

		return idea;
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void setStatus(@Required Long id, @Required String status) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		if (StringUtils.isEmpty(status) || !EnumUtils.isValidEnum(Status.class, status)) {
			error();
		}
		final Idea idea = Idea.findById(id);
		notFoundIfNull(idea);

		final models.app.InnoApp app = idea.getApp();
		notFoundIfNull(app);

		Security.checkAndCancel(AppsPermission.MODERATE_IDEA, app.sender, UserLoader.getConnectedUser(), app.id);

		final String comment = request.params.get("body");
		idea.setStatus(Status.valueOf(status), comment);

		idea.save();
		ok();
	}

	/**
	 * Finds other ideas that are similar to a given search term using fishr fuzzy search Only search within one app
	 * instance
	 *
	 * @param appId
	 * @param page
	 * @param idea
	 * @param ideas
	 */
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void getSimilarIdeas(final Long appId, Integer page, @JsonBody Idea idea, Set<Idea> ideas) {
		notFoundIfNull(appId);

		if (ideas == null) {
			ideas = new HashSet<>();
		}
		if (page == null) {
			page = 0;
		}

		final Set<SearchableResult> results = new InnoAppSearcher().searchSimilar(idea, page);

		Object[] args = { page, results.size() };
		LOG.debug("[InnoApp] Find similar ideas with page '{}' and count '{}'", args);

		for (SearchableResult sR : results) {
			if (sR.getModel() instanceof Idea) {
				final Idea similarIdea = (Idea) sR.getModel();

				if (!similarIdea.getApp().id.equals(appId)) {
					Object[] logArgs = { similarIdea, similarIdea.getApp().id, appId };
					LOG.debug("[InnoApp] Found similar idea {} in app [{}] does not belong to app [{}]", logArgs);
					continue;
				}

				if (!Security.check(AppsPermission.ACCESS_IDEA, similarIdea.getApp().sender,
						UserLoader.getConnectedUser(), similarIdea.getApp().id, similarIdea.id)) {
					LOG.debug("[InnoApp] Similar idea {} is not accessible.", similarIdea);
					continue;
				}

				LOG.debug("[InnoApp] Found similar idea: {}.", similarIdea);
				ideas.add(similarIdea);
			}

			// limit max results to 3
			if (ideas.size() >= SIMILAR_IDEAS_MAX_RESULTS) {
				break;
			}
		}

		// not enough accessible ideas found in first result page lets fetch page++
		if (ideas.size() < SIMILAR_IDEAS_MAX_RESULTS && results.size() == InnoAppSearcher.PAGE_SIZE) {
			getSimilarIdeas(appId, page++, idea, ideas);

		} else {
			final GsonBuilder builder = createBuilder();
			final models.app.InnoApp app = models.app.InnoApp.findById(appId);
			builder.registerTypeAdapter(Idea.class, new MinimalIdeaSerializer(UserLoader.getConnectedUser(), app));
			renderJSON(builder.create().toJson(ideas));
		}
	}

	@CheckAuthenticity
	@Secure(permissions = { Permission.READ_CONTAINER_ITEM }, params = { "ideaId" })
	@AllowExternals
	public static void getIdea(@Required Long ideaId) {
		renderJSON(createBuilder().create().toJson(loadItem(ideaId)));
	}

	/**
	 * Find all ideas one user has access to. This means user must be either: creator, moderator or sender admin
	 *
	 * @return
	 */
	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void getLinkableIdeas(final Long id) {
		notFoundIfNull(id);

		final String term = request.params.get("term");
		final Idea idea = Idea.findById(id);
		notFoundIfNull(idea);

		renderJSON(createBuilder().create()
				.toJson(IdeaDAO.findLinkableIdeas(idea, UserLoader.getConnectedUser(), term)));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void getUserIdeas() {
		final GsonBuilder builder = createBuilder();
		builder.registerTypeAdapter(Idea.class, new MinimalIdeaSerializer(UserLoader.getConnectedUser(), null));
		renderJSON(builder.create().toJson(IdeaDAO.findUserIdeas()));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void getIdeasOfOtherUsers() {
		final GsonBuilder builder = createBuilder();
		builder.registerTypeAdapter(Idea.class, new MinimalIdeaSerializer(UserLoader.getConnectedUser(), null));
		renderJSON(builder.create().toJson(IdeaDAO.getIdeasOfOtherUsers(UserLoader.getConnectedUser(), 25)));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void linkIdeas(@Required Long ideaId, @Required Long linkId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Idea rootIdea = Idea.findById(ideaId);
		notFoundIfNull(rootIdea);

		final Idea idea = Idea.findById(linkId);
		notFoundIfNull(idea);

		if (idea.id == rootIdea.id) {
			error();
		}
		rootIdea.linkedIdeas.add(idea);
		rootIdea.save();

		renderJSON(createBuilder().create().toJson(rootIdea));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void decoupleIdeas(@Required Long ideaId, @Required Long targetIdeaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Idea targetItem = Idea.findById(targetIdeaId);
		notFoundIfNull(targetItem);

		final Idea linkedItem = Idea.findById(ideaId);
		notFoundIfNull(linkedItem);

		targetItem.linkedIdeas.remove(linkedItem);
		targetItem.save();

		renderJSON(createBuilder().create().toJson(targetItem));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static void isModerator(@Required Long appId) {
		final models.app.InnoApp app = models.app.InnoApp.findById(appId);
		notFoundIfNull(app);
		renderJSON(createBuilder().create()
				.toJson(Security.check(
						AppsPermission.MODERATE_IDEA, app.sender, UserLoader.getConnectedUser(), app.id)));
	}

	@CheckAuthenticity
	@Transactional
	@AllowExternals
	public static String getIdeaUrl(@Required Long appId, @Required Long ideaId) {
		if (validation.hasErrors()) {
			badRequest(validation.errorsMap());
		}

		final Map<String, Object> args = new HashMap<>();
		final models.app.InnoApp app = models.app.InnoApp.findById(appId);
		notFoundIfNull(app);

		final Sender sender = app.sender;
		notFoundIfNull(sender);
		String url = null;

		if (sender != null) {
			args.put("id", sender.id);
			args.put("appId", appId);
			args.put("slug", sender.getSlug());
			args.put("appSlug", app.getSlug());

			if (sender instanceof Page) {
				url = Router.reverse("Pages.show", args).url + "#!/index?idea=" + ideaId.toString();
			} else if (sender instanceof Workspace) {
				url = Router.reverse("Workspaces.show", args).url + "#!index/?idea=" + ideaId.toString();
			}
		}
		return url;
	}

	/**
	 * Exports the given list as CSV. Values are separated by semicolon. Field labels are included as first line in the
	 * file.
	 */
	@AllowExternals
	public static void csv() {
		final FilterParams filterParams = (FilterParams) controllers.InnoApp.createBuilder().create()
				.fromJson(request.params.get("body"), FilterParams.class);

		if (filterParams == null) {
			error();
		}
		Long cID = filterParams.getContainerId();
		notFoundIfNull(cID);
		final Container container = Container.findById(cID);
		notFoundIfNull(container);

		Security.checkAndCancel(Permission.ACCESS_SENDER, container.sender.id);
		Security.checkAndCancel(Permission.READ_CONTAINER_ITEMS, container.id);

		final StringBuilder builder = new StringBuilder();
		try (CSVPrinter printer = new CSVPrinter(builder, CSVFormat.EXCEL.withDelimiter(';'))) {

			/* Add header record */
			final List<String> header = new ArrayList<>();
			header.add(Messages.get("app.inno.title"));
			header.add(Messages.get("app.inno.label.description"));
			header.add(Messages.get("app.inno.status"));
			header.add(Messages.get("app.inno.status") + " " + Messages.get("app.inno.comment"));

			header.add(Messages.get("app.inno.category"));
			header.add(Messages.get("app.inno.rating.average"));
			header.add(Messages.get("app.inno.ratings"));
			header.add(Messages.get("app.inno.date.creation"));
			header.add(Messages.get("app.inno.date.modified"));

			for (Field f : container.fields) {
				header.add(f.label);
			}

			printer.printRecord(header);

			/*
			 * Print items - we have to manually iterate over all fields per item, since some item values can be null.
			 * Those items should still be written as an empty value into the csv.
			 */
			for (Idea idea : IdeaDAO.findIdeasFilteredAndPaged(filterParams)) {
				printer.print(idea.title);
				printer.print(idea.description);
				printer.print(idea.getStatus().toString());
				printer.print(idea.statusComment);
				final Category category = idea.getCategory(idea.getApp());
				printer.print(category != null ? category.name : "");
				printer.print(IdeaDAO.getAverageRating(idea));
				printer.print(idea.ratings.size());
				printer.print(DateUtils.getFormattedString(idea.created));
				printer.print(DateUtils.getFormattedString(idea.modified));

				for (Field field : container.fields) {
					final ItemValue itemValue = idea.getItemValueForField(field.id);
					final Object csvValue = (itemValue != null) ? itemValue.getStringValue() : null;
					printer.print(csvValue);
				}

				printer.println();
			}

			// IE8 SSL download fix
			if (request.secure) {
				response.setHeader("Cache-Control", "private, max-age=15");
			}

			response.setContentTypeIfNotSet(CSV_MIMETYPE);
			response.contentType = CSV_CONTENT_TYPE;
			String fileName = CommonExtensions.slugify(container.sender.getDisplayName() + "-" + container.name)  + ".csv";
			renderBinary(IOUtils.toInputStream(builder.toString(), CSV_ENCODING), fileName);
		} catch (IOException e) {
			ErrorHelper.handleWarning(e, "Could not create CSV for container with id [%d]", container.id);
			badRequest();
		}
	}

	/**
	 * Creates GSonBuilder used to de-/serialize JSON/Java objects
	 *
	 * @return GsonBuilder
	 */
	public static GsonBuilder createBuilder() {
		final GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapter(models.app.InnoApp.class, new InnoAppSerializer(UserLoader.getConnectedUser()));
		builder.registerTypeAdapter(Container.class, new ContainerSerializer(false));
		builder.registerTypeAdapter(Idea.class, new IdeaSerializer(UserLoader.getConnectedUser()));
		builder.registerTypeHierarchyAdapter(Field.class, new FieldSerializer());
		builder.registerTypeHierarchyAdapter(OptionsFieldOption.class, new FieldOptionSerializer());
		builder.registerTypeHierarchyAdapter(FileItemAttachment.class, new FileAttachmentSerializer());
		builder.registerTypeHierarchyAdapter(ItemValue.class, new ItemValueSerializer());
		builder.registerTypeHierarchyAdapter(Sender.class, new MinimalUserSerializer());
		builder.registerTypeHierarchyAdapter(Category.class, new InnoAppCategorySerializer());
		builder.registerTypeAdapter(Comment.class, new CommentSerializer(UserLoader.getConnectedUser(), null));
		builder.registerTypeAdapter(IdeaRating.class, new IdeaRatingSerializer());

		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}
}
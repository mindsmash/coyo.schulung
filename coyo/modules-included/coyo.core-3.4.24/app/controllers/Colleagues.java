package controllers;

import java.util.List;

import models.User;
import onlinestatus.OnlineStatusServiceFactory;
import play.db.helper.JpqlSelect;
import play.db.jpa.GenericModel.JPAQuery;
import session.UserLoader;
import util.Util;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;

/**
 * Handles all action regarding the list of colleagues
 * 
 * @author Marko Ilic, Jan Marquardt, mindsmash GmbH
 */
public class Colleagues extends WebBaseController {

	public static void index() {
		renderArgs.put("count", User.count("active = true"));
		render();
	}

	public static void online() {
		renderArgs.put("count", OnlineStatusServiceFactory.getOnlineStatusService().getOnlineUserCount());
		render();
	}

	// ajax
	public static void load() {
		JpqlSelect select = Users.getUserListSelect();
		Users.renderUserListItems(select);
	}

	public static void loadFollowers() {
		JPAQuery query = UserLoader.getConnectedUser().getRealFollowersQuery();
		Users.renderUserListItems(query);
	}

	public static void loadFollowing() {
		JPAQuery query = UserLoader.getConnectedUser().getRealFollowingQuery();
		Users.renderUserListItems(query);
	}

	// ajax
	@ShowExternals
	public static void loadOnline() {
		JpqlSelect select = Users.getUserListSelect();
		List<Long> onlineUserIds = OnlineStatusServiceFactory.getOnlineStatusService().getOnlineUserIds();
		select.andWhere("u.id IN (" + Util.implode(onlineUserIds, ",") + ")");
		Users.renderUserListItems(select);
	}
}

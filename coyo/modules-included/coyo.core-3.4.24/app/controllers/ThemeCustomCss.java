package controllers;

import controllers.administration.ThemeSettings;
import models.Settings;
import org.apache.commons.lang3.StringUtils;
import play.libs.Codec;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Util;

public class ThemeCustomCss extends Controller {

    public static void getCss(String hash) {
        models.Settings settings = controllers.administration.ThemeSettings.getThemeSettings();
        String themeCss = settings.getString(ThemeSettings.THEME_CSS);

        response.setHeader("Content-Type", "text/css");
        response.setHeader("Cache-Control", "public, max-age=31536000");

        renderText(themeCss);
    }

    @Util
    public static String getHash() {
        models.Settings settings = controllers.administration.ThemeSettings.getThemeSettings();
        String themeCss = settings.getString(ThemeSettings.THEME_CSS);
        if(StringUtils.isEmpty(themeCss)) {
            return null;
        }
        return Codec.hexMD5(themeCss);
    }
}

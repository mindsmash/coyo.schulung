package controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.User;
import models.app.CalendarApp;
import models.event.CalendarFilter;
import models.event.Event;
import models.event.EventUser;
import models.event.EventUser.EventResponse;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VJournal;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.parameter.PartStat;
import net.fortuna.ical4j.model.parameter.Role;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Organizer;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Url;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.model.property.XProperty;
import net.fortuna.ical4j.util.UidGenerator;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.libs.Crypto;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.mvc.With;
import play.templates.JavaExtensions;
import session.UserLoader;
import acl.Permission;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;

/**
 * 
 * Handles all actions for offering a ICal calendar.
 * 
 * @author mindsmash GmbH
 * 
 */
// TODO: clean up
@AllowExternals
@ShowExternals
@With(TokenAuthController.class)
public class ICal extends SecureBaseController {

	@play.mvc.Util
	public static String createURL(User user, CalendarFilter filter) {
		String data = user.id + "";
		data += ":" + filter.showGlobal;

		// save calendar IDs into token
		if (filter.showCalendars != null) {
			for (CalendarApp calendar : filter.showCalendars) {
				if (calendar != null) {
					data += ":" + calendar.id;
				}
			}
		}

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("token", Crypto.encryptAES(data));
		args.put("name", createFeedName(filter.showCalendars, filter.showGlobal));

		return convertToICalURL(Router.reverse("ICal.ical", args));
	}

	@play.mvc.Util
	public static String createBirthdaysURL(User user) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(TokenAuthController.ICAL_TOKEN_NAME, TokenAuthController.createToken(UserLoader.getConnectedUser().id));
		return convertToICalURL(Router.reverse("ICal.birthdays", args));
	}

	@play.mvc.Util
	public static String convertToICalURL(ActionDefinition ad) {
		ad.absolute();
		String url = ad.url;
		url = url.replaceAll("https://", "webcal://");
		url = url.replaceAll("http://", "webcal://");
		return url;
	}

	private static String createFeedName(List<CalendarApp> calendars, boolean globalEvents) {
		String name;
		if (!globalEvents && calendars != null && calendars.size() == 1 && calendars.get(0) != null) {
			CalendarApp calendar = calendars.get(0);
			name = calendar.sender.getDisplayName() + "-" + calendar.title;
		} else {
			name = getSettings().getString(ApplicationSettings.APPLICATION_NAME);
		}

		return name != null ? JavaExtensions.slugify(name) : name;
	}

	public static void ical(String token, String name) throws Exception {
		// load encrypted data
		notFoundIfNull(token);

		// format: [userId:globalTrueFalse:id1:id2:id3:.....]
		List<String> data = null;
		try {
			data = TokenAuthController.getTokenData(token);
		} catch (Exception e) {
			Logger.warn("[ICal] Error authenticating user via token [URL=%s]", request.url);
			forbidden();
		}

		User user = UserLoader.getConnectedUser(false);
		if (user == null) {
			forbidden();
		}

		int calendarIdStartIndex = 2;
		boolean globalEvents = true;

		// check if global events should be included
		if (data.size() > 1) {
			// the below directive is for downwards compatibility only
			if (StringUtils.isNumeric(data.get(1))) {
				calendarIdStartIndex = 1;
				globalEvents = false;
			} else {
				// this is the new default!
				globalEvents = Boolean.parseBoolean(data.get(1));
			}
		}

		// try to load calendars
		List<CalendarApp> calendars = new ArrayList<CalendarApp>();
		if (data.size() > calendarIdStartIndex) {
			for (String id : data.subList(calendarIdStartIndex, data.size())) {
				try {
					CalendarApp calendar = CalendarApp.findById(Long.parseLong(id));
					if (calendar != null) {
						calendars.add(calendar);
					}
				} catch (Exception e) {
					Logger.warn(e, "error loading calendar with ID %s", id);
				}
			}
		}

		// name and headers
		if (name == null) {
			name = createFeedName(calendars, globalEvents);
		}
		String calName;
		if (!globalEvents && calendars.size() == 1) {
			calName = calendars.get(0).title;
		} else {
			calName = getSettings().getString(ApplicationSettings.APPLICATION_NAME);
		}

		// prepare calendar
		net.fortuna.ical4j.model.Calendar iCal = new net.fortuna.ical4j.model.Calendar();
		iCal.getProperties().add(new XProperty("X-PUBLISHED-TTL", "PT1H"));
		iCal.getProperties().add(new XProperty("X-WR-CALNAME", calName));
		iCal.getProperties().add(new ProdId(getSettings().getString(ApplicationSettings.APPLICATION_NAME)));
		iCal.getProperties().add(Version.VERSION_2_0);
		iCal.getProperties().add(CalScale.GREGORIAN);

		// load events
		org.joda.time.DateTime latest = new org.joda.time.DateTime().minusYears(1);
		for (Event event : getEvents(latest, calendars, globalEvents)) {
			iCal.getComponents().add(createVEvent(event, user));
		}

		// create an empty component so that ical works
		if (iCal.getComponents().size() == 0) {
			VJournal x = new VJournal();
			x.getProperties().add(new UidGenerator("empty").generateUid());
			iCal.getComponents().add(x);
		}

		response.setContentTypeIfNotSet("text/calendar");
		String contentDisposition = "%s; filename=\"%s\"";
		response.setHeader("Content-Disposition",
				String.format(contentDisposition, "attachment", JavaExtensions.slugify(name) + ".ics"));

		// stream it
		CalendarOutputter outputter = new CalendarOutputter();
		outputter.output(iCal, response.out);
	}

	@play.mvc.Util
	private static List<Event> getEvents(org.joda.time.DateTime latest, List<CalendarApp> calendars,
			boolean globalEvents) {
		List<Event> r = new ArrayList<Event>();

		// TODO: convert to JPQL select
		List<Event> events = Event.find("startDate > ? ORDER BY startDate, endDate ASC", latest).fetch();
		for (Event event : events) {
			// check permissions and possibly filter by reference
			if (Security.check(Permission.LIST_EVENT, event.id)
					&& ((globalEvents && event.calendar == null) || calendars.contains(event.calendar))) {
				r.add(event);
			}
		}

		return r;
	}

	@play.mvc.Util
	private static VEvent createVEvent(Event event, User user) throws Exception {
		Logger.debug("adding event to ical: %s", event);

		VEvent vevent;

		// check for whole day event
		if (event.fulltime) {
			if (event.endDate != null) {
				// plus 1 day: for some reason the ical reader subtracts one day
				vevent = new VEvent(new Date(event.startDate.getMillis()), new Date(event.endDate.plusDays(1)
						.getMillis()), event.name);
			} else {
				vevent = new VEvent(new Date(event.startDate.getMillis()), event.name);
			}
		} else {
			if (event.endDate != null) {
				vevent = new VEvent(new DateTime(event.startDate.getMillis()), new DateTime(event.endDate.getMillis()),
						event.name);
			} else {
				vevent = new VEvent(new DateTime(event.startDate.getMillis()), event.name);
			}
		}

		// place
		vevent.getProperties().add(new Location(event.location));

		// description
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", event.id);
		args.put("slug", event.getSlug());

		if (StringUtils.isNotEmpty(event.about)) {
			vevent.getProperties().add(new Description(event.about));
		}

		ActionDefinition ad = Router.reverse("Events.show", args);
		ad.absolute();
		vevent.getProperties().add(new Url(new java.net.URI(ad.url)));

		// organizers
		for (User admin : event.admins) {
			if (StringUtils.isNotEmpty(admin.email)) {
				vevent.getProperties().add(new Organizer(URI.create("mailto:" + admin.email)));
				break; // max 1
			}
		}

		// attendees
		for (EventUser eu : event.users) {
			Attendee attendee = new Attendee(URI.create("mailto:" + eu.user.email));
			attendee.getParameters().add(new Cn(eu.user.getDisplayName()));
			attendee.getParameters().add(Role.REQ_PARTICIPANT);

			if (eu.response == EventResponse.ATTENDING) {
				attendee.getParameters().add(PartStat.ACCEPTED);
			} else if (eu.response == EventResponse.NOT_ATTENDING) {
				attendee.getParameters().add(PartStat.DECLINED);
			} else if (eu.response == EventResponse.MAYBE) {
				attendee.getParameters().add(PartStat.TENTATIVE);
			} else {
				attendee.getParameters().add(PartStat.NEEDS_ACTION);
			}

			vevent.getProperties().add(attendee);
		}

		// uid
		vevent.getProperties().add(new UidGenerator(event.id + "").generateUid());

		return vevent;
	}

	public static void birthdays() throws Exception {
		// prepare calendar
		net.fortuna.ical4j.model.Calendar iCal = new net.fortuna.ical4j.model.Calendar();
		iCal.getProperties().add(new XProperty("X-WR-CALNAME", "Birthdays"));
		iCal.getProperties().add(new XProperty("X-PUBLISHED-TTL", "PT12H"));
		iCal.getProperties().add(new ProdId(getSettings().getString(ApplicationSettings.APPLICATION_NAME)));
		iCal.getProperties().add(Version.VERSION_2_0);
		iCal.getProperties().add(CalScale.GREGORIAN);

		// load birthdays
		List<User> allWithBirthday = User.find("active = true AND birthdate IS NOT NULL").fetch();
		for (User user : allWithBirthday) {
			VEvent vevent = new VEvent(new Date(user.getBirthday()), user.getFullName());
			vevent.getProperties().add(new UidGenerator(user.id + "").generateUid());
			iCal.getComponents().add(vevent);
		}

		// create an empty event so that ical works
		if (iCal.getComponents().size() == 0) {
			VEvent christmas = new VEvent(new Date(0L), "Empty calendar");
			christmas.getProperties().add(new UidGenerator("empty").generateUid());
			iCal.getComponents().add(christmas);
		}

		response.setContentTypeIfNotSet("text/calendar");
		String contentDisposition = "%s; filename=\"%s\"";
		response.setHeader("Content-Disposition", String.format(contentDisposition, "attachment", "birthdays.ics"));

		// stream it
		CalendarOutputter outputter = new CalendarOutputter();
		outputter.output(iCal, response.out);
	}
}

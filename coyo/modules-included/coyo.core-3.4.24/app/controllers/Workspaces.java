package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import models.Sender;
import models.User;
import models.UserGroup;
import models.app.App;
import models.notification.NewWorkspaceNotification;
import models.notification.WorkspaceInvitationNotification;
import models.notification.WorkspaceJoinNotification;
import models.notification.WorkspaceJoinReqNotification;
import models.notification.WorkspaceLeaveNotification;
import models.wall.Wall;
import models.wall.post.Post;
import models.workspace.Workspace;
import models.workspace.Workspace.WorkspaceActivitySorter;
import models.workspace.Workspace.WorkspaceVisibility;
import models.workspace.WorkspaceCategory;
import models.workspace.WorkspaceJoinRequest;
import models.workspace.WorkspaceMember;
import play.Logger;
import play.data.Upload;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import security.Secure;
import session.UserLoader;
import utils.ImageUtils;
import validation.ImageUpload;
import acl.Permission;
import binding.ModelIdBinder;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import controllers.api.WorkspaceAPI;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;

/**
 * 
 * Handles all actions for the lust of workspaces.
 * 
 * @author mindsmash GmbH
 * 
 */
public class Workspaces extends WebBaseController {

	@Before
	public static void checkActive() {
		if (!getSettings().getBoolean("workspaces")) {
			redirect("/");
		}
	}

	@Before(only = { "create", "edit", "save" })
	public static void prepareForm() {
		renderArgs.put("groups", UserGroup.find("ORDER BY name ASC").fetch());
		renderArgs.put("categories", WorkspaceCategory.findAllSorted());
	}

	public static void index() {
		render();
	}

	public static void archive() {
		List<Workspace> workspaces = Workspace.findArchived();
		render(workspaces);
	}

	@AllowExternals
	@ShowExternals
	@Secure(permissions = Permission.ACCESS_WORKSPACE, redirect = "Workspaces.index", params = "id")
	public static void show(Long id, Long appId, String slug, String appSlug) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		if (appId == null) {
			if (workspace.getDefaultApp() == null) {
				if (Security.check(Permission.EDIT_WORKSPACE, workspace.id)) {
					Apps.choose(workspace.id);
				}
				index();
			}
			show(id, workspace.getDefaultApp().id, workspace.getSlug(), workspace.getDefaultApp().getSlug());
		}

		App app = App.findById(appId);
		notFoundIfNull(app);
		if (app.sender != workspace) {
			show(id, workspace.getDefaultApp().id, workspace.getSlug(), workspace.getDefaultApp().getSlug());
		}

		// try to always have the slug in the URL
		if (slug == null) {
			show(id, appId, workspace.getSlug(), app.getSlug());
		}

		// check app permission
		Security.checkAndCancel(Permission.ACCESS_APP, app.id);

		// store activity
		WorkspaceActivitySorter.activate(workspace);

		app.beforeRender(request);
		render(workspace, app);
	}

	@Transactional
	@CheckAuthenticity
	public static void join(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);
		User user = UserLoader.getConnectedUser();
		boolean joinWorkspace = false;

		if (Security.check(Permission.JOIN_WORKSPACE, id) && (!workspace.isMember(user))) {
			/* user can join directly */
			workspace.addMember(user, false);
			workspace.save();
			joinWorkspace = true;

			// notify
			WorkspaceJoinNotification.raise(workspace, user);

			flash.success(Messages.get("workspace.join.success"));
		} else if (workspace.hasRequestedToJoin(user)) {
			/* already requested */
			flash.error(Messages.get("workspace.join.request.error"));
		} else {
			/* request to join */
			WorkspaceJoinRequest joinRequest = new WorkspaceJoinRequest(workspace, user);
			joinRequest.save();
			workspace.joinRequests.add(joinRequest);
			workspace.save(); // invalidate caches
			
			WorkspaceJoinReqNotification.raise(workspace, user);

			flash.success(Messages.get("workspace.join.request.success"));
		}
		if (WorkspaceAPI.SKIP_OLD_STICKY_POSTS && joinWorkspace) {
			markAllWallPostsAsRead(workspace, user);
		}
	}

	@Transactional
	@CheckAuthenticity
	public static void answerJoinRequest(Long id, boolean accept) {
		WorkspaceJoinRequest joinRequest = WorkspaceJoinRequest.findById(id);
		notFoundIfNull(id);
		Security.checkAndCancel(Permission.EDIT_WORKSPACE, joinRequest.workspace.id);

		if (accept) {
			joinRequest.accept();
		} else {
			joinRequest.deny();
		}
	}

	@Secure(permissions = Permission.LEAVE_WORKSPACE, redirect = "Workspaces.index", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void leave(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		final User connectedUser = UserLoader.getConnectedUser();
		// last admin cannot leave workspace

		final WorkspaceMember workspaceMember = workspace.getMember(connectedUser);

		if (workspaceMember != null && workspaceMember.admin && workspace.getAdmins().size() == 1) {
			flash.error(Messages.get("workspace.leave.error"));
		} else {
			final WorkspaceMember member = WorkspaceMember.find("user = ? AND workspace = ?", connectedUser, workspace)
					.first();
			member.delete();
			workspace.members.remove(member);
			workspace.save();

			WorkspaceLeaveNotification.raise(workspace, connectedUser);

			flash.success(Messages.get("workspace.leave.success"));
		}
	}

	// ajax
	@ShowExternals
	@Secure(permissions = Permission.EDIT_WORKSPACE, redirect = "Workspaces.index", params = "id")
	public static void invite(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);
		render(workspace);
	}

	@Transactional
	@CheckAuthenticity
	@Secure(permissions = Permission.EDIT_WORKSPACE, redirect = "Workspaces.index", params = "id")
	public static void doInvite(Long id, List<Long> newMembers) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		if (newMembers != null && !newMembers.isEmpty()) {
			Set<User> addedMembers = new HashSet<User>();
			List<User> newUsers = Lists.transform(newMembers, new Function<Long, User>() {
				@Override
				public User apply(Long userId) {
					return User.findById(userId);
				}
			});

			/* add user to workspace and save workspace, after all users are added */
			for (User user : newUsers) {
				if (user != null && !workspace.isMember(user)) {
					if (addMemberToWorkspace(workspace, user, false)) {
						addedMembers.add(user);
					}
				}
				workspace.save();
			}

			for (User user : addedMembers) {
				if (WorkspaceAPI.SKIP_OLD_STICKY_POSTS) {
					markAllWallPostsAsRead(workspace, user);
				}
			}

			/* send notifications to all new members after workspace was saved */
			for (User user : addedMembers) {
				if (user != null) {
					WorkspaceInvitationNotification.raise(workspace, user);
				}
			}
			flash.success(Messages.get("workspace.invite.success"));
		}
		redirectToSender(workspace);
	}

	@Secure(permissions = Permission.CREATE_WORKSPACE, redirect = "Workspaces.index")
	public static void create() {
		Workspace workspace = new Workspace(UserLoader.getConnectedUser());

		// default visibility
		if (!Security.check(Permission.CREATE_WORKSPACE, WorkspaceVisibility.PUBLIC)) {
			workspace.visibility = WorkspaceVisibility.CLOSED;
		}

		render(workspace);
	}

	@ShowExternals
	@Secure(permissions = Permission.EDIT_WORKSPACE, redirect = "Workspaces.index", params = "id")
	public static void edit(Long id) {
		Workspace workspace = Workspace.findById(id);
		if (workspace == null) {
			index();
		}
		render(workspace);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(Workspace workspace, List<Long> members, List<Long> memberGroups, List<Long> admins,
			@ImageUpload Upload avatar, String apporder) throws IOException {

		boolean created = false;
		User connectedUser = UserLoader.getConnectedUser();

		// check permission and optional setup/checking
		if (workspace.isPersistent()) {
			Security.checkAndRedirect(Permission.EDIT_WORKSPACE, "Workspaces.index", workspace.id);
		} else {
			Security.checkAndRedirect(Permission.CREATE_WORKSPACE, "Workspaces.index", workspace.visibility);
			workspace.setCreator(connectedUser);
			created = true;
		}

		Validation.valid("workspace", workspace);

		// avatar
		if (avatar != null) {
			try {
				ImageUtils.scaleToWidth(avatar.asStream(), avatar.getContentType(), workspace.avatar,
						Sender.AVATAR_WIDTH, true);
				ImageUtils
						.setThumb(avatar.asStream(), avatar.getContentType(), workspace.thumbnail, Sender.THUMB_WIDTH);
			} catch (Exception e) {
				Validation.addError("avatar", Messages.get("workspace.avatar.error"));
				Logger.warn(e, Messages.get("workspace.avatar.error"));
			}
		}

		if (Validation.hasErrors() || !workspace.validateAndSave()) {
			if (workspace.isPersistent()) {
				render("@edit", workspace);
			} else {
				render("@create", workspace);
			}
		}

		Set<User> addedMember = new HashSet<User>();
		Set<User> oldAdmins = new LinkedHashSet<User>(workspace.getAdmins());
		Set<User> oldMembers = new LinkedHashSet<User>(workspace.getUserMembers());
		Set<User> userMembers = new LinkedHashSet<User>(ModelIdBinder.bind(User.class, members));
		Set<User> adminMembers = new LinkedHashSet<User>();

		// admins
		if (Validation.required("admins", admins).ok) {
			adminMembers.addAll(ModelIdBinder.bind(User.class, admins));
			userMembers.addAll(adminMembers);
		}

		// add members from groups
		for (UserGroup group : ModelIdBinder.bind(UserGroup.class, memberGroups)) {
			userMembers.addAll(group.users);
		}

		// add all new members to workspace
		for (User user : userMembers) {
			if (!oldMembers.contains(user)) {
				if (addMemberToWorkspace(workspace, user, false)) {
					Logger.debug("[Workspace] Added user [%s] as member to workspace [%s].", user.getDisplayName(),
							workspace.name);
					addedMember.add(user);
				}
			}
		}

		// make admins and remove old members
		List<User> currAdmins = new ArrayList<>();
		for (WorkspaceMember member : new ArrayList<>(workspace.members)) {
			if (!userMembers.contains(member.user)) {
				if (member.isPersistent()) {
					workspace.members.remove(member);
					member.delete();
				}
			} else {
				member.admin = !member.user.external && adminMembers.contains(member.user);
			}
			if (member.admin) {
				Logger.debug("[Workspace] Made member [%s] admin of workspace [%s].", member.user.getDisplayName(),
						workspace.name);
				currAdmins.add(member.user);
			}
		}

		/* set app order and save workspace */
		workspace.storeAppOrder(apporder);
		workspace.save();

		/* send notification after workspace was saved */
		for (User u : currAdmins) {
			if (!oldAdmins.contains(u) && (u != connectedUser)) {
				WorkspaceInvitationNotification.raise(workspace, u);
			}
		}
		for (User u : addedMember) {
			if ((connectedUser == null) || !u.equals(connectedUser) && !currAdmins.contains(u)) {
				WorkspaceInvitationNotification.raise(workspace, u);
			}
		}
		if (created && workspace.visibility == WorkspaceVisibility.PUBLIC) {
			NewWorkspaceNotification.raise(workspace);
		}

		/* render success message and show workspace */
		flash.success(Messages.get("workspace.save.success"));
		show(workspace.id, null, workspace.getSlug(), null);
	}

	@Secure(permissions = Permission.DELETE_WORKSPACE, redirect = "Workspaces.index", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		Workspace workspace = Workspace.findById(id);
		if (workspace != null) {
			workspace.delete();
			flash.success(Messages.get("workspace.delete.success"));
		}
	}

	// ajax
	@Secure(permissions = Permission.INVITE_EXTERNALS_TO_WORKSPACE, params = "id")
	public static void shareExternal(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);
		render(workspace);
	}

	// ajax
	@Secure(permissions = Permission.INVITE_EXTERNALS_TO_WORKSPACE, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void activateShareExternal(Long id, String password) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		workspace.activateExternalSharing(password);
		workspace.save();

		render(workspace);
	}

	// ajax
	@Secure(permissions = Permission.INVITE_EXTERNALS_TO_WORKSPACE, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void deactivateShareExternal(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		workspace.deactivateExternalSharing();
		workspace.save();

		ok();
	}

	@ShowExternals
	@Secure(permissions = Permission.LIST_WORKSPACE, redirect = "Workspaces.index", params = "id")
	public static void members(Long id) {
		Workspace workspace = Workspace.findById(id);
		notFoundIfNull(workspace);

		if (request.isAjax()) {
			JpqlSelect select = Users.getUserListSelect();
			select.andWhere("EXISTS (SELECT wm FROM WorkspaceMember wm WHERE u = wm.user AND wm.workspace = ?)").param(
					workspace);
			Users.renderUserListItems(select);
		}

		render(workspace);
	}

	/**
	 * Helper method to add a user to a workspace as member without triggering any events, notifications or updates. The
	 * method checks, whether the user is already a member of the workspace. Only if this is not the case, the user is
	 * added.
	 * 
	 * @return returns "true" if a new member was added, "false" otherwise.
	 */
	private static boolean addMemberToWorkspace(Workspace workspace, User user, boolean isAdmin) {
		WorkspaceMember member = workspace.getMember(user);
		if (member == null) {
			member = new WorkspaceMember();
			member.workspace = workspace;
			member.user = user;
			member.admin = isAdmin;
			workspace.members.add(member);
			return true;
		}
		return false;
	}
	
	/**
	 * Marks all sticky posts as read for a new workspace member
	 */
	@Transactional
	private static void markAllWallPostsAsRead(Workspace workspace,  User user) {		
		for (Wall wall : workspace.walls) {				
			List<Post> postList = wall.getPostsQuery(user).fetch();			
			for (Post post : postList) {
				if (post.isImportant(user) && !post.hasRead(user)) {
					post.toggleRead(user);	
				}
			}
		}
	}
}

package controllers;

import java.util.ArrayList;
import java.util.List;

import models.TasksAppList;
import models.TasksAppTask;
import models.User;
import models.notification.TaskNotification;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.db.helper.JpqlSelect;
import play.db.jpa.Transactional;
import security.Secure;
import session.UserLoader;
import util.Util;
import acl.AppsPermission;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;

@AllowExternals
@ShowExternals
public class TasksApp extends WebBaseController {

	// ajax
	/**
	 * Renders the content of a modal dialog to create a new list.
	 */
	public static void createList(Long id) {
		models.TasksApp app = models.TasksApp.findById(id);
		notFoundIfNull(app);

		Security.checkAndCancel(AppsPermission.EDIT_TASKS, app.id);

		TasksAppList list = new TasksAppList();
		list.app = app;
		render(list);
	}

	// ajax
	/**
	 * Renders the content of a modal dialog to edit an existing list.
	 */
	public static void editList(Long id) {
		TasksAppList list = TasksAppList.findById(id);
		notFoundIfNull(list);

		Security.checkAndCancel(AppsPermission.EDIT_TASKS, list.app.id);

		render(list);
	}

	// ajax
	/**
	 * Renders the content of a modal dialog to edit an existing task.
	 */
	@Secure(permissions = AppsPermission.EDIT_TASK, params = "id")
	public static void editTask(Long id) {
		TasksAppTask task = TasksAppTask.findById(id);
		notFoundIfNull(task);
		render(task);
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void sortLists(String order) {
		if (!StringUtils.isEmpty(order)) {
			int prio = 1;
			for (String sid : Util.parseList(order, ",")) {
				if (StringUtils.isNumeric(sid)) {
					TasksAppList list = TasksAppList.findById(Long.valueOf(sid));
					list.priority = prio++;
					list.save();
				}
			}
		}
	}

	@Transactional
	@CheckAuthenticity
	public static void saveList(TasksAppList list) {
		Security.checkAndRedirect(AppsPermission.EDIT_TASKS, "/", list.app.id);

		if (!list.validateAndSave()) {
			validation.keep();
		}

		redirectToSender(list.app.sender, list.app.id);
	}

	@Transactional
	@CheckAuthenticity
	public static void deleteList(Long id) {
		TasksAppList list = TasksAppList.findById(id);
		notFoundIfNull(list);

		Security.checkAndRedirect(AppsPermission.EDIT_TASKS, "/", list.app.id);

		// detach tasks
		for (TasksAppTask task : list.tasks) {
			task.list = null;
			task.save();
		}

		list.delete();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void sortTasks(String order) {
		if (!StringUtils.isEmpty(order)) {
			int prio = 1;
			for (String sid : Util.parseList(order, ",")) {
				if (StringUtils.isNumeric(sid)) {
					TasksAppTask task = TasksAppTask.findById(Long.valueOf(sid));
					task.priority = prio++;
					task.save();
				}
			}
		}
	}

	@Secure(permissions = AppsPermission.EDIT_TASKS, params = "id")
	public static void tasks(Long id, Long listId) {
		models.TasksApp app = models.TasksApp.findById(id);
		notFoundIfNull(app);

		if (listId != null && listId > 0) {
			TasksAppList list = TasksAppList.findById(listId);
			notFoundIfNull(list);
			renderArgs.put("list", list);
			renderArgs.put("completed", list.getTasks(true));
			renderArgs.put("uncompleted", list.getTasks(false));
		} else {
			renderArgs.put("completed", app.getInbox(true));
			renderArgs.put("uncompleted", app.getInbox(false));
		}

		render(app);
	}

	@Transactional
	@CheckAuthenticity
	public static void saveTask(TasksAppTask task, User assignee) {
		boolean created = false;
		if (task.isPersistent()) {
			Security.checkAndRedirect(AppsPermission.EDIT_TASK, "/", task.id);
		} else {
			Security.checkAndRedirect(AppsPermission.EDIT_TASKS, "/", task.app.id);
			created = true;
			task.priority = 0;
		}

		// handle assignee
		if (!assignee.isPersistent()) {
			assignee = null;
		}
		boolean newUser = task.getAssignee() != assignee;
		task.setAssignee(assignee);

		// check app + list match
		if (task.list != null && task.list.app != task.app) {
			error();
		}

		if (task.validateAndSave()) {
			// sort to top
			if (created) {
				List<TasksAppTask> tasksToSort;
				if (task.list != null) {
					tasksToSort = task.list.getTasks(false);
				} else {
					tasksToSort = task.app.getInbox(false);
				}

				int prio = 1;
				for (TasksAppTask t : tasksToSort) {
					t.priority = prio++;
					t.save();
				}
			}

			if (newUser && task.getAssignee() != null) {
				try {
					TaskNotification.raise(task, UserLoader.getConnectedUser());
				} catch (Exception ex) {
					Logger.error(ex, "[TasksApp] Failed to raise notification: " + ex.getLocalizedMessage());
				}

			}
		} else {
			if (request.isAjax()) {
				error("validation failed");
			} else {
				validation.keep();
			}
		}

		if (request.isAjax()) {
			ok();
		} else {
			redirectToReferrer();
		}
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_TASK, params = "id")
	@CheckAuthenticity
	public static void updateTask(Long id, Boolean completed, Long listId) {
		TasksAppTask task = TasksAppTask.findById(id);
		notFoundIfNull(task);
		if (completed != null) {
			task.completed = completed;
		}
		if (listId != null) {
			if (listId == 0) {
				task.list = null;
			} else {
				TasksAppList list = TasksAppList.findById(listId);
				if (list != null && list.app == task.app) {
					task.list = list;
				}
			}
		}
		task.save();
	}

	// ajax
	@Transactional
	@Secure(permissions = AppsPermission.EDIT_TASK, params = "id")
	@CheckAuthenticity
	public static void deleteTask(Long id) {
		TasksAppTask task = TasksAppTask.findById(id);
		notFoundIfNull(task);
		task.delete();
	}

	// ajax
	@Transactional
	@Secure(permissions = AppsPermission.EDIT_TASKS, params = "id")
	@CheckAuthenticity
	public static void clearDone(Long id, Long listId) {
		List<TasksAppTask> tasksToClear;
		if (listId != null && listId > 0) {
			TasksAppList list = TasksAppList.findById(listId);
			notFoundIfNull(list);
			tasksToClear = list.getTasks(true);
		} else {
			models.TasksApp app = models.TasksApp.findById(id);
			notFoundIfNull(app);
			tasksToClear = app.getInbox(true);
		}

		for (TasksAppTask task : tasksToClear) {
			task.delete();
		}
	}

	@Secure(permissions = AppsPermission.EDIT_TASK, params = "id")
	@ShowExternals
	public static void userchooser(Long id, List<Long> selected, String term) {
		TasksAppTask task = TasksAppTask.findById(id);
		notFoundIfNull(task);

		JpqlSelect select = UserLoader.getConnectedUser().getUserChooserSelect(true, true, selected, term);

		List<User> invitableUsers = task.getAssignableUsers();
		if (invitableUsers != null) {
			List<Long> allowed = new ArrayList<Long>();

			// TODO: what is this for?
			if (UserLoader.getConnectedUser().superadmin) {
				allowed.add(UserLoader.getConnectedUser().id);
			}

			for (User user : invitableUsers) {
				allowed.add(user.id);
			}
			select.andWhere("id IN (" + Util.implode(allowed, ",") + ")");
		}

		EndlessHelper.prepare(User.find(select.toString(), select.getParams().toArray()), false);
		Users.renderUsersChooserJson();
	}
}

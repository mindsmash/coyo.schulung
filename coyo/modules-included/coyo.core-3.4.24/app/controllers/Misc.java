package controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import checks.EmailRegistrationCheck;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import libs.Protection;
import models.Activation;
import models.Application;
import models.Settings;
import notifiers.ApplicationMailer;
import play.Play;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Util;
import session.UserLoader;

/**
 * 
 * Handles all actions for downloads, help, about and inviting a user.
 * 
 * @author mindsmash GmbH
 * 
 */
public class Misc extends WebBaseController {

	private static final String SUPPORT_CENTER_URL = Play.configuration.getProperty("coyo.supportCenter.url", "http://support.mindsmash.com");

	@AllowExternals
	public static void downloads() {
		render();
	}

	public static void mobileAccess() {
		render();
	}

	@AllowExternals
	public static void help() {
		Settings settings = Settings.findApplicationSettings();
		if (!settings.contains("helpHtml") || StringUtils.isEmpty(settings.getString("helpHtml"))) {
			String url = getSupportCenterUrl();
			notFoundIfNull(url);
			redirect(url);
		}
		renderArgs.put("supportCenterUrl", getSupportCenterUrl());
		render();
	}

	@AllowExternals
	public static void about() {
		URL customVersion = Play.classloader.getResource("project.custom.version");
		if(customVersion != null) {
			try {
				renderArgs.put("customVersion", FileUtils.readFileToString(new File(customVersion.getPath())));
			} catch (IOException ignored) {
			}
		}
		
		render();
	}

	@Transactional
	@CheckAuthenticity
	public static void invite(@Required @Email @CheckWith(EmailRegistrationCheck.class) String email) {
		if (!Validation.hasErrors()) {
			// check already exists
			if (!Registration.emailExists(email)) {
				// brute-force protection
				if (Protection.max(Application.BRUTEFORCE_PROTECTION_MAX, Application.BRUTEFORCE_PROTECTION_BAN)) {
					forbidden("too many invitations from your IP. please wait a few minutes before trying again.");
				}

				Activation activation = new Activation(email);
				activation.save();

				ApplicationMailer.invite(email, UserLoader.getConnectedUser(), activation.token);
				flash.success(Messages.get("invitation.success"));
			}
		} else {
			flash.error(Messages.get("invitation.error"));
		}

		redirectToReferrer();
	}

	/**
	 * Determines the support center URL. If it is configured in the administration area, than this URL will be used.
	 * Otherwise this method will look for a configured URL in the play configuration. If this URL is also not
	 * available, null will be returned.
	 * 
	 * @return URL string or null if none found.
	 */
	@Util
	private static String getSupportCenterUrl() {
		String url = null;
		
		Settings settings = Settings.findApplicationSettings();
		if (settings.contains(ApplicationSettings.SUPPORT_CENTER_URL)) {
			String surl = settings.getString(ApplicationSettings.SUPPORT_CENTER_URL);
			if (StringUtils.isNotEmpty(surl)) {
				url = surl;
			}
		} else if (StringUtils.isNotEmpty(SUPPORT_CENTER_URL)) {
			url = SUPPORT_CENTER_URL;
		}

		if(url != null && url.startsWith("http")) {
			return url;
		}
		return null;
	}
}

package controllers;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import models.FormField;
import models.FormResult;
import models.User;
import models.app.FormsApp;
import notifiers.FormsMailer;
import org.joda.time.DateTimeZone;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import session.UserLoader;
import util.Util;
import acl.AppsPermission;
import acl.Permission;
import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;
import utils.DateUtils;

@AllowExternals
public class Forms extends WebBaseController {

	@Transactional
	@CheckAuthenticity
	public static void send(FormsApp app) {
		notFoundIfNull(app);

		Security.checkAndCancel(AppsPermission.FORM_SUBMIT, app);
		User user = UserLoader.getConnectedUser();

		// check maximum
		if (app.maxReached(user)) {
			flash.error(Messages.get("formsapp.error.max"));
			redirectToSender(app.sender, app.id);
		}

		// assemble form data
		Map<FormField, String> data = new LinkedHashMap<FormField, String>();
		for (FormField field : app.fields) {
			String key = "form.field" + field.id;
			String value = Util.implode(params.getAll(key), ",");
			data.put(field, value);

			if (field.required) {
				validation.required(key, value);
			}
		}

		if (validation.hasErrors()) {
			params.flash();
			validation.keep();

			flash.error(Messages.get("formsapp.error.validation"));

			redirectToReferrer();
		}

		// save result
		FormResult result = new FormResult();
		result.form = app;
		result.user = user;
		for (FormField field : data.keySet()) {
			result.addValue(field, data.get(field));
		}
		result.save();

		// send
		for (User recipient : app.receivers) {
			FormsMailer.result(app, data, recipient.email, user, recipient.language, recipient.getDateTimeZone(), false);
		}
		// send
		for (String recipient : app.externalRecipients) {
			final User recipientUser = User.findByEmail(recipient);
			if (recipientUser != null) {
				FormsMailer.result(app, data, recipient, user, recipientUser.language, recipientUser.getDateTimeZone(), false);
			} else {
				FormsMailer.result(app, data, recipient, user, Locale.ENGLISH, DateUtils.getDefaultDateTimeZone(), true);
			}
		}
		// send
		if (app.sendToUser && !app.receivers.contains(user)) {
			FormsMailer.result(app, data, user.email, user, user.language, user.getDateTimeZone(), false);
		}

		flash.success(Messages.get("formsapp.success.formSent"));
		redirectToSender(app.sender, app.id);
	}

	@Transactional
	@CheckAuthenticity
	public static void deleteResult(Long id) {
		FormResult result = FormResult.findById(id);
		notFoundIfNull(result);
		Security.checkAndCancel(Permission.EDIT_SENDER, result.form.sender.id);
		result.delete();
	}
}

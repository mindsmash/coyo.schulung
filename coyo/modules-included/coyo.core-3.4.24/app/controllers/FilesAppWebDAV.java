package controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import models.User;
import models.app.FilesApp;
import models.app.files.FileVersion;
import models.notification.FileNotification;
import play.Play;
import play.db.jpa.Transactional;
import play.libs.Codec;
import play.libs.IO;
import play.mvc.Before;
import play.mvc.Http.Header;
import play.mvc.Http.StatusCode;
import play.mvc.With;
import security.Secure;
import session.UserLoader;
import acl.AppsPermission;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;

@Slf4j
@AllowExternals
@ShowExternals
@With(TokenAuthController.class)
public class FilesAppWebDAV extends WebBaseController {

	/**
	 * Play closes the request body automatically when parsing parameters. therefore, we catch the request body stream
	 * here and save it to temp before reinitiating the request body manually. The body can be of two types:
	 * FileInputStream (long) or ByteArrayInputStream (short), depending on the content length.
	 */
	@Before(priority = -1)
	static void captureInput() throws IOException {
		if (request.method.equals("PUT")
				&& (request.body instanceof FileInputStream || request.body instanceof ByteArrayInputStream)) {
			log.debug("[WebDAV] Capturing file input for method [{}] with request body of type [{}]", request.method,
					request.body.getClass());
			File file = new File(Play.tmpDir.getAbsolutePath() + File.separator + Codec.UUID());
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			IO.copy(request.body, fos);
			fos.close();

			request.body = new FileInputStream(file);
			request.args.put("requestBody", file);
		}
	}

	@Before(priority = 20)
	static void prepare() throws IOException {
		User user = UserLoader.getConnectedUser();
		Object[] args = { request.method, request.path, user };
		log.debug("[WebDAV] {} -> {} [user={}]", args);
		renderArgs.put(TokenAuthController.WEBDAV_TOKEN_NAME, TokenAuthController.createToken(user.id));
	}

	@Transactional
	@Secure(permissions = AppsPermission.EDIT_FILE, params = { "id", "uid" })
	public static void receive(String token, Long id, String uid, String name) throws Exception {
		FilesApp app = FilesApp.findById(id);
		notFoundIfNull(app);
		models.app.files.File file = app.getFile(uid);
		notFoundIfNull(file);
		User user = UserLoader.getConnectedUser();

		if (file.isFolder()) {
			error("not supported");
		}

		if (request.method.equals("GET") || request.method.equals("HEAD")) {
			FileVersion version = file.getCurrentVersion();
			response.status = StatusCode.OK;
			response.contentType = file.getContentType();
			response.setHeader("Content-Disposition", "attachment");
			response.setHeader("Content-Length", version.getSize() + "");

			if (request.method.equals("GET")) {
				response.direct = version.get();
			}
		} else {
			if (request.method.equals("PUT")) {
				User lock = file.getLock();
				if (lock != null && lock != user) {
					response.status = StatusCode.FORBIDDEN;
				} else {
					File input = (File) request.args.get("requestBody");
					file.addVersion(user, new FileInputStream(input), input.length());

					FileNotification.raise(user, app, file, file.getCurrentVersion());

					response.status = StatusCode.CREATED;
				}
			} else if (request.method.equals("LOCK")) {
				if (file.lock(user)) {
					request.format = "xml";
					render("@lock");
				} else {
					response.status = 423; // locked
				}
			} else if (request.method.equals("UNLOCK")) {
				if (file.unlock(user)) {
					response.status = StatusCode.NO_RESPONSE;
				} else {
					response.status = StatusCode.FORBIDDEN;
				}
			} else {
				log.warn("[WebDAV] Request with http method [{}] is not supported", request.method);
				error("not supported");
			}
		}
	}

	public static void options(String token, Long id) {
		request.format = "xml";
		response.headers
				.put("Allow",
						new Header("Allow",
								"OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, COPY, MOVE, MKCOL, PROPFIND, PROPPATCH, LOCK, UNLOCK, ORDERPATCH"));
		response.headers
				.put("Public",
						new Header("Public",
								"OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, COPY, MOVE, MKCOL, PROPFIND, PROPPATCH, LOCK, UNLOCK, ORDERPATCH"));
		response.headers.put("DAV", new Header("DAV", "1, 2, ordered-collections"));
		response.headers.put("MS-Author-Via", new Header("MS-Author-Via", "DAV"));
		render();
	}
}
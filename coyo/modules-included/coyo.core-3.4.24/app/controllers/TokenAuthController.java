package controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import models.User;
import play.libs.Crypto;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Request;
import session.UserLoader;
import util.Util;
import utils.MobileUtils;

/**
 * Allows authentication via a "auth-token" request parameter.
 * 
 * A token may contain additional data
 */
public class TokenAuthController extends Controller {

	/**
	 * Name of header and token to check for
	 */
	public static final String HEADER_NAME = "auth-token";

	/**
	 * Alternative name of token to check for in url parameter. This is needed for WebDAV office integration.
	 */
	public static final String WEBDAV_TOKEN_NAME = "token";

	/**
	 * Alternative name of token to check for in url parameter. This is needed for iCal support.
	 */
	public static final String ICAL_TOKEN_NAME = "token";

	private static final Logger LOG = LoggerFactory.getLogger(TokenAuthController.class);

	@Before(priority = 0)
	public static void auth() {
		if (Auth.getCurrentId() == null) {
			handleTokenAuthentication();
		}
	}

	/**
	 * @return True if authentication was successful.
	 */
	@play.mvc.Util
	public static boolean handleTokenAuthentication() {
		try {
			// try to lookup user via oauth2 token
			User user = controllers.api.OAuth2.getOAuthUser();

			// no user found? then try access-token now
			if (user == null) {
				String token = getAccessToken();
				if (token != null) {
					long userId = Long.parseLong(getTokenData(token).get(0));
					user = User.find("id = ?", userId).first();
				}
			}

			// user found by either oauth or access token?
			if (user != null) {
				if (isMobileApp()) {
					// for mobile apps we need to set a session if any token auth was successful
					Auth.storeUserIdInSession(user.id);
				}
				// store user in arguments map of the current request for the {@link UserLoader}
				request.args.put("user", user);
				// token authentication was successful
				return true;

			// no user found means token authentication was not successful!
			} else {
				LOG.trace("[TokenAuth] Could not authenticate user via token [URL={}]. No token could be found.",
						request.url);
			}
		} catch (Exception e) {
			LOG.error("Error during token authentication: " + e.getLocalizedMessage(), e);
		}

		// token authentication was not successful
		return false;
	}

	@play.mvc.Util
	public static String getAccessToken() {
		if (request.headers.containsKey(HEADER_NAME)) {
			return request.headers.get(HEADER_NAME).value();
		} else if (params._contains(HEADER_NAME)) {
			return params.get(HEADER_NAME);
		} else if (params._contains(WEBDAV_TOKEN_NAME)) {
			return params.get(WEBDAV_TOKEN_NAME);
		}
		return null;
	}

	@play.mvc.Util
	public static List<String> getTokenData(String token) {
		return Util.parseList(Crypto.decryptAES(token), ":");
	}

	@play.mvc.Util
	public static String createToken(Long id, List<String> data) {
		data.add(0, id + "");
		return Crypto.encryptAES(Util.implode(data, ":"));
	}

	@play.mvc.Util
	public static String createToken(Long id) {
		return createToken(id, new ArrayList<String>());
	}

	@play.mvc.Util
	public static void setTokenHeader() {
		response.setHeader(HEADER_NAME, TokenAuthController.createToken(UserLoader.getConnectedUser().id));
	}

	@play.mvc.Util
	private static boolean isMobileApp() {
		Http.Header userAgent = Request.current().headers.get("user-agent");
		return userAgent != null && MobileUtils.isMobileCoyoApp(userAgent.value());
	}

}
package controllers;

import acl.Permission;
import binding.ModelIdBinder;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import injection.Inject;
import injection.InjectionSupport;
import models.Sender;
import models.User;
import models.UserGroup;
import models.app.App;
import models.dao.PageDao;
import models.notification.NewPageNotification;
import models.notification.PageInvitationNotification;
import models.page.Page;
import models.page.Page.PageVisibility;
import models.page.PageCategory;
import play.Logger;
import play.data.Upload;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import security.CheckCaller;
import security.Secure;
import session.UserLoader;
import utils.ImageUtils;
import validation.ImageUpload;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles all actions for the list of pages.
 *
 * @author Jan Marquardt
 */
@InjectionSupport
public class Pages extends WebBaseController {

	@Inject(configuration="coyo.di.dao.page", defaultClass = PageDao.class, singleton = true)
	private static PageDao pageDao;

	@Before(only = { "create", "edit", "save" })
	public static void prepareForm() {
		renderArgs.put("groups", UserGroup.find("ORDER BY name ASC").fetch());
		renderArgs.put("categories", PageCategory.findAllSorted());
		renderArgs.put("showForcedFollowersForm", CheckCaller.check(Permission.CREATE_FORCE_FOLLOW_PAGE));
	}

	public static void index() {
		renderArgs.put("count", pageDao.count(UserLoader.getConnectedUser()));
		renderArgs.put("myPages", UserLoader.getConnectedUser().getPages(null));
		renderArgs.put("categories", PageCategory.findAllSorted());
		render();
	}

	// ajax
	public static void load(boolean my, Long categoryId, String filterQuery) {
		List<Page> pages;
		if(my) {
			pages = pageDao.fetchSortedUserPages(UserLoader.getConnectedUser(), (EndlessHelper.getPage() - 1), EndlessHelper.getLength(), categoryId, filterQuery);
		} else {
			pages = pageDao.fetchSorted(UserLoader.getConnectedUser(), (EndlessHelper.getPage() - 1), EndlessHelper.getLength(), categoryId, filterQuery);
		}

		EndlessHelper.use(pages, true);
		render();
	}

	@Secure(permissions = Permission.ACCESS_PAGE, redirect = "Pages.index", params = "id")
	public static void show(Long id, Long appId, String slug, String appSlug) {
		Page page = Page.findById(id);
		notFoundIfNull(page);

		if (appId == null) {
			if (page.getDefaultApp() == null) {
				if (Security.check(Permission.EDIT_PAGE, page.id)) {
					Apps.choose(page.id);
				}
				index();
			}
			show(id, page.getDefaultApp().id, page.getSlug(), page.getDefaultApp().getSlug());
		}

		App app = App.findById(appId);
		notFoundIfNull(app);
		if (app.sender != page) {
			show(id, page.getDefaultApp().id, page.getSlug(), page.getDefaultApp().getSlug());
		}

		// try to always have the slug in the URL
		if (slug == null) {
			show(id, appId, page.getSlug(), app.getSlug());
		}

		// check app permission
		Security.checkAndCancel(Permission.ACCESS_APP, app.id);

		app.beforeRender(request);
		render(page, app);
	}

	@Secure(permissions = Permission.CREATE_PAGE, redirect = "Pages.index")
	public static void create() throws Exception {
		Page page = new Page();
		page.creator = UserLoader.getConnectedUser();
		page.members.add(UserLoader.getConnectedUser());
		page.admins.add(UserLoader.getConnectedUser());

		// default visibility
		if (!Security.check(Permission.CREATE_PAGE, PageVisibility.PUBLIC)) {
			page.visibility = PageVisibility.PRIVATE;
		}

		render(page);
	}

	@Secure(permissions = Permission.EDIT_PAGE, redirect = "Pages.index", params = "id")
	public static void edit(Long id) {
		Page page = Page.findById(id);
		if (page == null) {
			index();
		}
		render(page);
	}

	@Secure(permissions = Permission.DELETE_PAGE, redirect = "Pages.index", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		Page page = Page.findById(id);
		if (page != null) {
			page.delete();
			flash.success(Messages.get("page.delete.success"));
		}
	}

	/**
	 * @param typeName
	 *            Page type name
	 * @throws Exception
	 */
	@Transactional
	@CheckAuthenticity
	public static void save(Page page, @ImageUpload Upload avatar, List<Long> members, List<Long> memberGroups,
			@Required List<Long> adminIds, String apporder, boolean forceAllFollow, List<Long> forcedFollowers,
			List<Long> forcedFollowerGroups) throws Exception {
		boolean created = false;
		if (page.isPersistent()) {
			Security.checkAndRedirect(Permission.EDIT_PAGE, "Pages.index", page.id);
		} else {
			Security.checkAndRedirect(Permission.CREATE_PAGE, "Pages.index", page.visibility);
			// auto-follow creator on create
			page.creator = UserLoader.getConnectedUser();
			page.followers.add(page.creator);
			created = true;
		}

		List<User> oldAdmins = page.getAdmins();

		// clear validation (needed for deleted creators)
		Validation.clear();

		// bind members and admins
		List<User> oldMembers = new ArrayList<User>(page.members);
		oldMembers.addAll(page.admins);
		for (UserGroup group : page.memberGroups) {
			oldMembers.addAll(group.users);
		}
		page.members = ModelIdBinder.bind(User.class, members);
		page.memberGroups = ModelIdBinder.bind(UserGroup.class, memberGroups);
		page.admins = ModelIdBinder.bind(User.class, adminIds);

		if (CheckCaller.check(Permission.CREATE_FORCE_FOLLOW_PAGE)) {
			page.forceAllFollow = forceAllFollow;
			page.forcedFollowers = ModelIdBinder.bind(User.class, forcedFollowers);
			page.forcedFollowerGroups = ModelIdBinder.bind(UserGroup.class, forcedFollowerGroups);
		}

		// validate and save
		Validation.required("adminIds", page.admins);
		Validation.valid("page", page);

		// set avatar
		if (avatar != null) {
			try {
				ImageUtils.scaleToWidth(avatar.asStream(), avatar.getContentType(), page.avatar, Sender.AVATAR_WIDTH,
						true);
				ImageUtils.setThumb(avatar.asStream(), avatar.getContentType(), page.thumbnail, Sender.THUMB_WIDTH);
			} catch (Exception e) {
				Validation.addError("avatar", Messages.get("page.avatar.error"));
				Logger.warn(e, Messages.get("page.avatar.error"));
			}
		}

		if (Validation.hasErrors() || !page.validateAndSave()) {
			if (page.isPersistent()) {
				render("@edit", page);
			} else {
				render("@create", page);
			}
		}

		// notifications
		if (created && page.visibility == PageVisibility.PUBLIC) {
			NewPageNotification.raise(page);
		}

		// app order
		page.storeAppOrder(apporder);

		List<User> newAdmins = new ArrayList<>();
		List<User> currAdmins = page.getAdmins();
		for(User u : currAdmins) {
			if(!oldAdmins.contains(u) && (u != UserLoader.getConnectedUser())) {
				PageInvitationNotification.raise(page, u);
				newAdmins.add(u);
			}
		}
		// raise notifications
		for (User user : page.getAllMembers()) {
			if (!newAdmins.contains(user) && !oldMembers.contains(user) && user != UserLoader.getConnectedUser()) {
				PageInvitationNotification.raise(page, user);
			}
		}

		page.save();

		flash.success(Messages.get("page.create.success"));
		show(page.id, null, page.getSlug(), null);
	}

	@Secure(permissions = Permission.ACCESS_PAGE, redirect = "Pages.index", params = "id")
	public static void followers(Long id) {
		Page page = Page.findById(id);
		notFoundIfNull(page);

		if (request.isAjax()) {
			JpqlSelect select = Users.getUserListSelect();
			select.andWhere("? MEMBER OF u.following").param(page);
			Users.renderUserListItems(select);
		}

		render(page);
	}

	@Secure(permissions = Permission.ACCESS_PAGE, redirect = "Pages.index", params = "id")
	public static void admins(Long id) {
		Page page = Page.findById(id);
		notFoundIfNull(page);

		if (request.isAjax()) {
			JpqlSelect select = Users.getUserListSelect();
			select.select("u");
			select.from("Page p");
			select.andWhere("u MEMBER OF p.admins");
			select.andWhere("p = ?").param(page);
			Users.renderUserListItems(select);
		}

		render(page);
	}
}

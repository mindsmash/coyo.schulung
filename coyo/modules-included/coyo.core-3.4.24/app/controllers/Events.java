package controllers;

import acl.Permission;
import binding.ModelIdBinder;
import checks.TimeCheck;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import extensions.JodaJavaExtensions;
import forms.EventForm;
import forms.EventForm.EventSeriesUpdateType;
import forms.EventForm.EventUserForm;
import jobs.EventSeriesChildGeneratorScheduler;
import models.Sender;
import models.User;
import models.UserGroup;
import models.app.CalendarApp;
import models.event.CalendarFilter;
import models.event.Event;
import models.event.EventBase;
import models.event.EventSeries;
import models.event.EventSeriesChild;
import models.event.EventUser;
import models.event.EventUser.EventResponse;
import models.notification.EventChangedNotification;
import models.notification.EventInvitationNotification;
import models.notification.EventResponseNotification;
import models.notification.NewEventNotification;
import models.notification.Notification;
import models.notification.listapp.ListItemChangedNotification;
import models.workspace.Workspace;
import multitenancy.MTA;
import notifiers.ApplicationMailer;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import play.data.Upload;
import play.data.binding.As;
import play.data.validation.Validation;
import play.db.helper.JpqlSelect;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Router;
import security.Secure;
import session.UserLoader;
import storage.FlexibleBlob;
import util.Util;
import utils.DateUtils;
import utils.ImageUtils;
import validation.ImageUpload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles all actions regarding events.
 *
 * @author mindsmash GmbH
 */
public class Events extends WebBaseController {

	private static final String EVENTS_SHOW_WEEKEND_KEY = "eventsShowWeekend";
	private static final String EVENTS_VIEW_TYPE_KEY = "eventsViewType";

	public static void index() {
		renderArgs.put("filter", loadCalendar(UserLoader.getConnectedUser()));
		render();
	}

	@AllowExternals
	@Transactional
	public static void calendar(@As("yyyy-MM-dd") Date start, String type, Long calendarId) {
		if (start == null) {
			start = new Date();
		}

		final User connectedUser = UserLoader.getConnectedUser();

		// load and store view type
		if (StringUtils.isEmpty(type)) {
			// try to load view type from user properties or set to default
			type = connectedUser.properties.containsKey(EVENTS_VIEW_TYPE_KEY) ?
					connectedUser.properties.get(EVENTS_VIEW_TYPE_KEY) :
					"list";
		}

		if (!type.equals(connectedUser.properties.get(EVENTS_VIEW_TYPE_KEY))) {
			connectedUser.properties.put(EVENTS_VIEW_TYPE_KEY, type);
			connectedUser.save();
		}

		renderArgs.put("showWeekend", isShowWeekend());
		renderArgs.put("filter", loadCalendar(UserLoader.getConnectedUser()));
		renderArgs.put("start", new DateTime(start));
		renderArgs.put("today", new DateTime(UserLoader.getConnectedUser().getDateTimeZone()));
		render(type, calendarId);
	}

	@AllowExternals
	public static void loadCalendarList() {
		List<Event> events = Event.getUpcomingEventsWithPaging(UserLoader.getConnectedUser().getDateTimeZone(),
				loadCalendar(UserLoader.getConnectedUser()), EndlessHelper.getPage(), EndlessHelper.getLength());
		renderArgs.put("data", events);
		render();
	}

	@Secure(permissions = Permission.ACCESS_EVENT_SERIES, params = "id")
	public static void loadSeriesChildren(Long id) {
		JpqlSelect jpqlSelect = new JpqlSelect();
		jpqlSelect.select("esc").from("EventSeriesChild esc").where("esc.series.id = ?").andWhere("esc.endDate > ?")
				.orderBy("esc.startDate asc");
		jpqlSelect.params(id, new DateTime().withZone(DateTimeZone.UTC));
		EndlessHelper.prepare(EventSeriesChild.find(jpqlSelect.toString(), jpqlSelect.getParams().toArray()), true);
		render();
	}

	@AllowExternals
	@Secure(permissions = Permission.LIST_EVENT, params = "id")
	public static void popover(final Long id) {
		final Event event = Event.findById(id);
		notFoundIfNull(event);
		render(event);
	}

	public static void archive() {
		renderArgs.put("count", Event.getArchivedEvents(UserLoader.getConnectedUser().getDateTimeZone()).size());
		render();
	}

	public static void loadArchive() {
		EndlessHelper.prepare(Event.getArchivedEvents(UserLoader.getConnectedUser().getDateTimeZone()), true);
		render();
	}

	@AllowExternals
	@ShowExternals
	@Secure(permissions = Permission.ACCESS_EVENT, params = "id", redirect = "Events.index")
	public static void show(final Long id, final String slug) {
		final Event event = Event.findById(id);
		notFoundIfNull(event);

		// add slug if not there
		if (slug == null) {
			show(id, event.getSlug());
		}

		render(event);
	}

	@Before(only = { "form", "save" })
	public static void prepareForm() {
		renderArgs.put("groupedCalendars", CalendarApp.getGroupedCalendars(null));
	}

	@Secure(permissions = Permission.CREATE_EVENT, redirect = "Events.index")
	public static void create(final Long templateEventId) {
		render(templateEventId);
	}

	@AllowExternals
	@Secure(permissions = Permission.EDIT_EVENT, redirect = "Events.index", params = "id")
	public static void edit(final Long id) {
		final Event event = Event.findById(id);
		notFoundIfNull(event);
		render(event);
	}

	// ajax
	@AllowExternals
	public static void form(final Long id, final Long calendarId, final Long templateEventId) {
		EventForm event = null;

		if (templateEventId != null) {
			// try to load the event with the given ID to base our new event on
			// its data
			Security.checkAndRedirect(Permission.CREATE_EVENT, "Events.index");
			if (Security.check(Permission.ACCESS_EVENT, templateEventId)) {
				final Event templateEvent = Event.findById(templateEventId);
				if (templateEvent != null) {
					event = new EventForm(UserLoader.getConnectedUser(), templateEvent);
				}
			}
			if (event == null) {
				event = new EventForm(UserLoader.getConnectedUser());
			}
		} else if (id != null) {
			Security.checkAndRedirect(Permission.EDIT_EVENT, "Events.index", id);
			final Event model = Event.findById(id);
			event = new EventForm(model);
			notFoundIfNull(event);
		} else {
			if (calendarId == null) {
				Security.checkAndRedirect(Permission.CREATE_EVENT, "Events.index");
			} else {
				Security.checkAndRedirect(Permission.CREATE_EVENT, "Events.index", calendarId);
			}
			event = new EventForm(UserLoader.getConnectedUser());

			if (calendarId != null) {
				event.calendar = CalendarApp.findById(calendarId);
				//
				// // prepare admins
				// final Set<User> admins = new HashSet<User>();
				// admins.add(getConnectedUser());
				// admins.addAll(event.calendar.sender.getAdmins());
				// event.admins.clear();
				// event.admins.addAll(admins);
			}
		}
		render(event);
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void save(final EventForm event, final String startTime, final String endTime,
			final List<Long> guestIds, final List<Long> groupIds, final List<Long> adminIds,
			@ImageUpload final Upload avatar) throws Exception {
		if (event.isSeries()) {
			saveEventSeries(event, startTime, endTime, guestIds, groupIds, adminIds, avatar);
		} else {
			saveEvent(event, startTime, endTime, guestIds, groupIds, adminIds, avatar);
		}
	}

	/**
	 * Creates a new event series or edits an existing event series child.
	 */
	private static void saveEventSeries(final EventForm event, final String startTime, final String endTime,
			final List<Long> guestIds, final List<Long> groupIds, final List<Long> adminIds, final Upload avatar)
			throws IOException {
		final boolean created = !event.isPersistent();
		if (created) {
			checkCreatePermissions(event);
		} else {
			checkEditPermissions(event);
		}

		// check recurring event rule
		if (!DateUtils.isValidRecurrenceRule(event.recurrenceRule)) {
			Validation.addError("event.recurrenceRule", Messages.get("validation.invalidValue"));
		}

		// bind values to form model
		event.calendar = event.calendar.id == null ? null : CalendarApp.<CalendarApp>findById(event.calendar.id);
		bindStartAndEndDate(event, startTime, endTime);

		EventSeriesChild child = null;
		if (created) {
			// create new series
			final EventSeries series = new EventSeries(UserLoader.getConnectedUser(), event.recurrenceRule);

			// bind users and admins to form model
			final List<User> newGuests = bindUsers(event, guestIds, groupIds, adminIds);

			// bind form values to model
			event.bindToModel(series);

			// create first child!
			child = series.createChildEvent();
			series.children.add(child);

			// check if there will be an overlap
			checkOverlap(event, child);

			Validation.valid("event", event);
			if (Validation.hasErrors() || !series.validateAndSave()) {
				response.status = Http.StatusCode.BAD_REQUEST;
				render("@form", event, startTime, endTime);
			}

			saveAvatar(avatar, series);

			// raise notifications for event creation and invited users
			NewEventNotification.raise(child);

			for (final User user : newGuests) {
				EventInvitationNotification.raise(child, user);
			}

			// start task that generates child instances, but don't start it
			// immediately, as this could result in the
			// job not finding the new entity
			new EventSeriesChildGeneratorScheduler.EventSeriesChildGeneratorTask(MTA.getActiveTenant(), series.id)
					.in("1s");

		} else {
			// edit existing event series child
			child = EventSeriesChild.findById(event.id);
			notFoundIfNull(child);

			// init users from model
			final List<EventUser> users = child.users;
			for (final EventUser eventUser : users) {
				event.users.add(new EventUserForm(eventUser));
			}

			// bind users and admins to form model
			final List<User> newGuests = bindUsers(event, guestIds, groupIds, adminIds);

			// if start/end day and/or recurrence rule were changed: force
			// update type to THIS_AND_FOLLOWING and thus
			// cancellation of the old series and creation of a new one
			if (!JodaJavaExtensions.sameDay(event.startDate, child.startDate) || !JodaJavaExtensions
					.sameDay(event.endDate, child.endDate) || !event.recurrenceRule.equals(child.getRecurrenceRule())) {
				event.updateType = EventSeriesUpdateType.THIS_AND_FOLLOWING;
			}

			// remember whether start time has changed
			final boolean startChanged = !child.startDate.withZone(DateTimeZone.UTC).equals(event.startDate);

			// bind form values to model
			event.bindToModel(child);

			// make distinction between update types:
			// "this and following events" and "only this"
			switch (event.updateType) {
			case THIS_AND_FOLLOWING:
				checkOverlap(event, child);

				Validation.valid("event", event);
				if (Validation.hasErrors()) {
					response.status = Http.StatusCode.BAD_REQUEST;
					render("@form", event, startTime, endTime);
				}

				// delete all subsequent children of the series and mark it as
				// cancelled
				final EventSeries series = child.series;
				final List<EventSeriesChild> childrenOfOldSeriesToBeDeleted = new ArrayList<EventSeriesChild>();
				for (final EventSeriesChild existingChild : series.children) {
					if (existingChild.startDate.isAfter(child.startDate)) {
						childrenOfOldSeriesToBeDeleted.add(existingChild);
					}
				}

				// NOTE: its not sufficient to remove children from the list here - we have to delete them explicitly!
				series.children.removeAll(childrenOfOldSeriesToBeDeleted);
				series.isCancelled = true;
				series.save();

				// create new series based on the child
				final EventSeries newSeries = EventSeries.create(child, event.recurrenceRule);
				if (!newSeries.validateAndSave()) {
					response.status = Http.StatusCode.BAD_REQUEST;
					render("@form", event, startTime, endTime);
				}
				saveAvatar(avatar, newSeries);

				// delete child manually - see above
				for (final EventSeriesChild deletedChild : childrenOfOldSeriesToBeDeleted) {
					deletedChild.delete();
				}

				// send notifications to invited users
				for (final User user : child.getInvitedUsers()) {
					if (user.equals(child.creator)) {
						continue;
					}
					EventInvitationNotification.raise(child, user);
				}

				// start task that generates child instances for the new series
				new EventSeriesChildGeneratorScheduler.EventSeriesChildGeneratorTask(MTA.getActiveTenant(),
						newSeries.id).in("1s");
				break;

			case ONLY_THIS:
			default:
				// check if there will be an overlap
				checkOverlap(event);

				Validation.valid("event", event);
				if (Validation.hasErrors() || !child.validateAndSave()) {
					response.status = Http.StatusCode.BAD_REQUEST;
					render("@form", event, startTime, endTime);
				}

				// raise notifications for update and new users
				if (startChanged) {
					EventChangedNotification.raise(child);

				}
				for (final User user : newGuests) {
					EventInvitationNotification.raise(child, user);
				}

				saveAvatar(avatar, child);
			}
		}

		renderEvent(child);
	}

	private static void checkOverlap(final EventForm event, EventSeriesChild child) {
		if (event.calendar != null && event.calendar.eventsMayNotOverlap()) {
			Event firstOverlappingEvent = null;
			if (child != null) {
				// find event which overlaps with event series
				firstOverlappingEvent = event.calendar.getFirstOverlappingEvent(event.startDate, event.endDate,
						UserLoader.getConnectedUser().getDateTimeZone(), event.id, event.recurrenceRule,
						child.series.id);
			} else {
				// find event which overlaps with single event
				firstOverlappingEvent = event.calendar
						.getFirstOverlappingEvent(event.startDate, event.endDate, event.id);
			}
			if (firstOverlappingEvent != null) {
				Validation.addError("event.overlapsExisting", "validation.event.overlapsExisting",
						firstOverlappingEvent.name);
			}
		}
	}

	private static void checkOverlap(final EventForm event) {
		checkOverlap(event, null);
	}

	private static void saveEvent(final EventForm event, final String startTime, final String endTime,
			final List<Long> guestIds, final List<Long> groupIds, final List<Long> adminIds, final Upload avatar)
			throws IOException {
		final boolean created = !event.isPersistent();
		boolean startChanged = false;

		// find out whether we have to create a new event or edit an existing
		// one
		Event eventModel = null;
		if (created) {
			checkCreatePermissions(event);
			eventModel = new Event(UserLoader.getConnectedUser());
		} else {
			checkEditPermissions(event);
			eventModel = Event.findById(event.id);
			// init users from model
			final List<EventUser> users = eventModel.users;
			for (final EventUser eventUser : users) {
				event.users.add(new EventUserForm(eventUser));
			}
		}

		// bind values to form model
		event.calendar = event.calendar.id == null ? null : CalendarApp.<CalendarApp>findById(event.calendar.id);
		bindStartAndEndDate(event, startTime, endTime);

		// check changed start
		if (eventModel.isPersistent()) {
			final DateTime oldStartDate = eventModel.startDate;
			if (oldStartDate != null) {
				startChanged = !oldStartDate.withZone(DateTimeZone.UTC).equals(event.startDate);
			}
		}

		// check if there will be an overlap
		checkOverlap(event);

		final List<User> newGuests = bindUsers(event, guestIds, groupIds, adminIds);

		// bind form values to model and try to validate and save
		event.bindToModel(eventModel);
		Validation.valid("event", event);
		if (Validation.hasErrors() || !eventModel.validateAndSave()) {
			response.status = Http.StatusCode.BAD_REQUEST;
			render("@form", event, startTime, endTime);
		}

		saveAvatar(avatar, eventModel);

		// notifications
		for (final User user : newGuests) {
			EventInvitationNotification.raise(eventModel, user);
		}

		if (created) {
			NewEventNotification.raise(eventModel);
		} else if (startChanged) {
			EventChangedNotification.raise(eventModel);
		}

		renderEvent(eventModel);
	}

	private static void saveAvatar(final Upload avatar, final EventBase event) throws IOException {
		if (avatar != null) {
			// we have to choose the right target as the getter will always
			// return the series' values for child events
			FlexibleBlob avatarTarget = event.avatar;
			FlexibleBlob thumbnailTarget = event.thumbnail;
			if (event instanceof EventSeriesChild) {
				avatarTarget = new FlexibleBlob();
				thumbnailTarget = new FlexibleBlob();
			}
			ImageUtils.scaleToWidth(
					avatar.asStream(), avatar.getContentType(), avatarTarget, Sender.AVATAR_WIDTH, true);
			ImageUtils.setThumb(avatar.asStream(), avatar.getContentType(), thumbnailTarget, Sender.THUMB_WIDTH);
			if (event instanceof EventSeriesChild) {
				event.avatar = avatarTarget;
				event.thumbnail = thumbnailTarget;
			}
			event.save();
		}
	}

	private static void renderEvent(final Event event) {
		// IE fix, fake non-json content type, see
		// http://stackoverflow.com/questions/8151138/ie-jquery-form-multipart-json-response-ie-tries-to-download-response
		response.setContentTypeIfNotSet("text/html");

		final Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", event.id);
		args.put("slug", event.getSlug());
		renderJSON(Router.reverse("Events.show", args).url);
	}

	private static List<User> bindUsers(final EventForm eventForm, final List<Long> guestIds, final List<Long> groupIds,
			final List<Long> adminIds) {
		// invite/uninvite people now
		final List<User> newGuests = new ArrayList<User>();
		final List<User> guests = ModelIdBinder.bind(User.class, guestIds);

		// add group users to guests
		for (final UserGroup group : ModelIdBinder.bind(UserGroup.class, groupIds)) {
			guests.addAll(group.users);
		}

		if (eventForm.allowGuestInvite && guestIds != null) {
			// remove
			for (final EventUserForm eventUserForm : new ArrayList<EventUserForm>(eventForm.users)) {
				if (!guests.contains(eventUserForm.user)) {
					eventForm.users.remove(eventUserForm);
				}
			}

			// add
			for (final User user : guests) {
				if (eventForm.invite(user) && user != UserLoader.getConnectedUser()) {
					newGuests.add(user);
				}
			}
		}

		// admins
		eventForm.admins = ModelIdBinder.bind(User.class, adminIds);
		if (eventForm.admins.size() == 0) {
			eventForm.admins.add(UserLoader.getConnectedUser());
		}

		eventForm.creator = UserLoader.getConnectedUser();
		// auto-attend creator
		if (eventForm.invite(UserLoader.getConnectedUser())) {
			eventForm.respond(eventForm.creator, EventResponse.ATTENDING);
		}
		return newGuests;
	}

	private static void bindStartAndEndDate(final EventForm eventForm, String startTime, String endTime) {
		// check and prepare time
		if (eventForm.fulltime) {
			startTime = null;
			endTime = null;
		} else {
			if (eventForm.startDate != null && !TimeCheck.ok(startTime)) {
				Validation.addError("event.startDate", TimeCheck.MESSAGE);
			}
			if (eventForm.endDate != null && !TimeCheck.ok(endTime)) {
				Validation.addError("event.endDate", TimeCheck.MESSAGE);
			}
		}

		// time binding
		eventForm.startDate = DateUtils.timeToZoneDate(UserLoader.getConnectedUser(), eventForm.startDate, startTime);
		eventForm.endDate = DateUtils.timeToZoneDate(UserLoader.getConnectedUser(), eventForm.endDate, endTime);
	}

	private static void checkEditPermissions(final EventForm eventForm) {
		Security.checkAndRedirect(Permission.EDIT_EVENT, "Events.index", eventForm.id);
	}

	private static void checkCreatePermissions(final EventForm eventForm) {
		if (eventForm.calendar == null || eventForm.calendar.id == null) {
			Security.checkAndRedirect(Permission.CREATE_EVENT, "Events.index", eventForm.visibility);
		} else {
			Security.checkAndRedirect(Permission.CREATE_EVENT, "Events.index", eventForm.calendar.id);
		}
	}

	@Secure(permissions = Permission.DELETE_EVENT, redirect = "Events.index", params = "id")
	@Transactional
	@CheckAuthenticity
	@AllowExternals
	public static void delete(final Long id, final Boolean applyToSubsequent) {
		final Event event = Event.findById(id);

		if (event == null) {
			return;
		}

		// check if this is an event series child and the user wants to delete
		// all subsequent children as well
		if (event.isEventSeriesChild() && Boolean.TRUE.equals(applyToSubsequent)) {
			final EventSeriesChild child = (EventSeriesChild) event;
			final EventSeries series = child.series;
			series.removeChildrenStartingWith(child);
			if (series.children.isEmpty()) {
				series.delete();
			} else {
				series.isCancelled = true;
				series.save();
			}
		} else {
			event.delete();
		}
	}

	@AllowExternals
	@Secure(permissions = Permission.RESPOND_TO_EVENT, redirect = "Events.index", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void respond(final Long id, final EventResponse response, final Boolean applyToSubsequent) {
		final Event event = Event.findById(id);
		if (event != null && event.allowGuestInvite) {
			if (event.isEventSeriesChild() && Boolean.TRUE.equals(applyToSubsequent)) {
				final EventSeriesChild child = (EventSeriesChild) event;
				final EventSeries series = child.series;
				series.respond(UserLoader.getConnectedUser(), response, child);
				series.save();
			} else {
				EventUser eu = EventUser.find("event = ? AND user = ?", event, UserLoader.getConnectedUser()).first();
				if (eu == null) {
					eu = new EventUser(UserLoader.getConnectedUser(), event);
				}
				eu.response = response;
				eu.save();
				event.save();
			}

			EventResponseNotification.raise(event, UserLoader.getConnectedUser(), response);

			flash.success(Messages.get("event.respond.success"));
		}
	}

	@Secure(permissions = Permission.EDIT_EVENT, redirect = "Events.index", params = "id")
	public static void mail(final Long id) {
		final Event event = Event.findById(id);
		notFoundIfNull(event);
		render(event);
	}

	@CheckAuthenticity
	@Secure(permissions = Permission.EDIT_EVENT, redirect = "Events.index", params = "id")
	public static void sendMail(final Long id, final String title, final String message) {
		final Event event = Event.findById(id);
		notFoundIfNull(event);

		int count = 0;
		for (final EventUser eu : event.users) {
			if (eu.response.receiveUpdates()) {
				ApplicationMailer.eventMail(event, eu.user, title, message);
				count++;
			}
		}

		if (count == 1) {
			flash.success(Messages.get("event.mail.success.singular", count));
		} else if (count > 1) {
			flash.success(Messages.get("event.mail.success.plural", count));
		}
		show(event.id, event.getSlug());
	}

	// ajax
	@ShowExternals
	public static void userchooser(final Long id, final List<Long> selected, final String term) {
		JpqlSelect select;
		if (id == null) {
			select = UserLoader.getConnectedUser().getUserChooserSelect(false, true, selected, term);
		} else {
			final CalendarApp calendar = CalendarApp.findById(id);

			// determine if external users should be shown
			final boolean showExternals = calendar.sender instanceof Workspace;

			select = UserLoader.getConnectedUser().getUserChooserSelect(showExternals, true, selected, term);

			final List<User> invitableUsers = calendar.getInvitableUsers();
			if (invitableUsers != null) {
				final List<Long> allowed = new ArrayList<Long>();

				// TODO: what is this for?
				if (UserLoader.getConnectedUser().superadmin) {
					allowed.add(UserLoader.getConnectedUser().id);
				}

				for (final User user : invitableUsers) {
					allowed.add(user.id);
				}
				select.andWhere("id IN (" + Util.implode(allowed, ",") + ")");
			}
		}

		EndlessHelper.prepare(User.find(select.toString(), select.getParams().toArray()), false);
		Users.renderUsersChooserJson();
	}

	@Transactional
	@CheckAuthenticity
	public static void toggleShowWeekend(Boolean showWeekend) {
		final User connectedUser = UserLoader.getConnectedUser();
		if (null == showWeekend) {
			showWeekend = !isShowWeekend();
		}
		connectedUser.properties.put(EVENTS_SHOW_WEEKEND_KEY, Boolean.toString(showWeekend));
		connectedUser.save();
		renderJSON("{\"showWeekend\":" + showWeekend + "}");
	}

	@play.mvc.Util
	public static boolean isShowWeekend() {
		return Boolean.parseBoolean(UserLoader.getConnectedUser().properties.get(EVENTS_SHOW_WEEKEND_KEY));
	}

	/**
	 * Render event series view. Pass the series and the first event to the template.
	 */
	public static void series(Long id) {
		EventSeries series = EventSeries.findById(id);
		if ( series == null) {
			index();
		}
		EventSeriesChild eventSeriesChild = Collections.min(series.children, new Comparator<EventSeriesChild>() {
			@Override
			public int compare(EventSeriesChild o1, EventSeriesChild o2) {
				return o1.startDate.compareTo(o2.startDate);
			}
		});
		renderArgs.put("series", series);
		render(eventSeriesChild, UserLoader.getConnectedUser());
	}

	/**
	 * Helper method which loads the calendar filter, based on the given request parameters. Performs a security check
	 * if a calendar id was provided.
	 * If the request params contain a valid "calendarId", a filter for only displaying this calender will be loaded. If
	 * not, the normal calendar filter will be loaded from the connected user's settings.
	 */
	private static CalendarFilter loadCalendar(User user) {
		Long calendarId = Request.current().params.get("calendarId", Long.class);
		if (calendarId != null) {
			CalendarApp calendar = CalendarApp.findById(calendarId);
			Security.checkAndCancel(Permission.ACCESS_SENDER, calendar.sender.id);
			return CalendarFilter.loadSingleCalendar(calendar);
		}
		return CalendarFilter.loadDefault(user);
	}
}

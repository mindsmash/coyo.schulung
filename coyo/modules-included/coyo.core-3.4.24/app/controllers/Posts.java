package controllers;

import acl.Permission;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import controllers.utils.EndlessHelper;
import csrf.CheckAuthenticity;
import forms.PostForm;
import libs.Protection;
import models.Application;
import models.Sender;
import models.User;
import models.dao.UserDao;
import models.notification.NewPostNotification;
import models.notification.PostReportNotification;
import models.wall.Wall;
import models.wall.attachments.FilePostAttachment;
import models.wall.attachments.LinkPostAttachment;
import models.wall.post.Post;
import models.wall.post.SimplePost;
import notifiers.ApplicationMailer;
import org.apache.commons.lang.StringUtils;
import play.Play;
import play.data.Upload;
import play.data.validation.Valid;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import security.Secure;
import session.UserLoader;
import util.HttpUtil;
import util.Util;
import utils.WebLinkInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * Handles all actions for Posts.
 * 
 * @author mindsmash GmbH
 * 
 */
@AllowExternals
@ShowExternals
public class Posts extends WebBaseController {

	public static final boolean MAIL_SHARE_ENABLED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.postMailShare.enabled", "true"));
	public static final boolean POST_REPORTING_ENABLED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.postReporting.enabled", "true"));

	@Secure(permissions = Permission.ACCESS_POST, redirect = "/", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void toggleFollow(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		post.toggleFollowing(UserLoader.getConnectedUser());
	}

	@Secure(permissions = Permission.ACCESS_POST, redirect = "/", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void toggleRead(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		post.toggleRead(UserLoader.getConnectedUser());
	}

	@Secure(permissions = Permission.ACCESS_POST, redirect = "/", params = "id")
	@Transactional
	@CheckAuthenticity
	public static void toggleFlagged(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		post.toggleFlagged(UserLoader.getConnectedUser());
	}

	@Secure(permissions = Permission.ACCESS_POST, redirect = "/", params = "id")
	public static void show(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		render(post);
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void save(@Valid PostForm form) {
		Security.checkAndCancel(Permission.CREATE_POST, form.wall.id);

		Post post = new SimplePost();

		Wall wall = Wall.findById(form.wall.id);

		post.wall = wall;
		post.message = form.message;
		post.author = UserLoader.getConnectedUser();

		if (Security.check(Permission.POST_STICKY, wall.sender.id)) {
			post.important = form.important;
		}

		if (form.postAsWallOwner && Security.check(Permission.POST_AS_OTHER, wall.sender.id)) {
			post.author = wall.sender;
		}

		if (validation.hasErrors() || !post.validateAndSave()) {
			error();
		}

		post.refresh();

		// add basic followers
		if (post.author instanceof User) {
			post.follow((User) post.author);
		}
		if (wall.sender instanceof User) {
			post.follow((User) wall.sender);
		}

		UserLoader.getConnectedUser().getNewActivityStream(true);

		// attachments
		handlePostAttachments(post);

		if (!StringUtils.isEmpty(form.weblink)) {
			LinkPostAttachment la = new LinkPostAttachment();
			la.url = form.weblink;
			la.post = post;
			la.save();
		}

		post.refresh();

		// raise notification
		NewPostNotification.raise(post);

		render("@post", post, wall);
	}

	@play.mvc.Util
	public static boolean handlePostAttachments(Post post) {
		return post.createAttachments((List<Upload>) request.args.get("__UPLOADS"));
	}

	// ajax
	@Secure(permissions = Permission.DELETE_POST, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		post.delete();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void unsticky(Long id) throws InterruptedException {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		Security.checkAndCancel(Permission.POST_STICKY, post.wall.sender.id);

		post.important = false;
		post.save();
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void sticky(Long id) throws InterruptedException {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		Security.checkAndCancel(Permission.POST_STICKY, post.wall.sender.id);

		post.important = true;
		post.save();

		// clear existing read marks
		JPA.em().createQuery("UPDATE PostUser pu SET pu.isRead = false WHERE pu.post = ?").setParameter(1, post)
				.executeUpdate();
	}

	// ajax
	@Transactional
	@ShowExternals
	public static void post(Long id, Long wallId) {
		Post post = Post.findById(id);
		notFoundIfNull(post);
		Wall wall = Wall.findById(wallId);
		notFoundIfNull(wall);

		Cache.cacheEntity(post, UserLoader.getConnectedUser().getPrivateCacheKey());
		render(post, wall);
	}

	public static void fileAttachment(Long id) {
		FilePostAttachment fa = FilePostAttachment.findById(id);
		notFoundIfNull(fa);

		Security.checkAndCancel(Permission.ACCESS_POST, fa.post.id);

		Cache.cacheEntity(fa, UserLoader.getConnectedUser().getPrivateCacheKey());

		// IE8 SSL download fix
		if (request.secure) {
			response.setHeader("Cache-Control", "private, max-age=15");
		}
		renderBinary(fa.file.get(), fa.name, fa.file.length(), fa.file.type(), false);
	}
	
	/**
	 * returns a representation of the user with jobtitel and department
	 * @param user
	 * @return representation of the user with jobtitel and department
	 */
	private static String createUserListName(User user) {
		String value = user.getFullName();
		Boolean jobExists = user.jobTitle != null && !user.jobTitle.equals("");
		Boolean departmentExists = user.department != null && !user.department.equals("");
		
		if (jobExists || departmentExists) {
			String innerContent = "";
			
			if (jobExists && departmentExists) {
				innerContent = user.jobTitle + " / "  + user.department;
			} else if (jobExists) {
				innerContent = user.jobTitle;
			} else if (departmentExists) {
				innerContent = user.department;
			}
			
			value = value + " (" + innerContent + ")";
		}
		return value;
	}

	// ajax
	@ShowExternals
	public static void userList(Long id, String term) {
		final List<Map> users = new ArrayList<Map>();
		final Sender sender = Sender.findById(id);
		final List<User> all;

		if (!StringUtils.isEmpty(term) && !"@".equals(term)) {
			term = term.substring(1).toLowerCase();
		}

		all = UserDao.getUserToMention(sender, term, 10);

		if (all != null) {
			for (User user : all) {
				Map<String, String> data = new HashMap<String, String>();
				data.put("value", createUserListName(user));
				data.put("slug", "@" + user.slug);
				users.add(data);
			}
		}

		renderJSON(users);
	}

	// ajax
	public static void weblinkInfo(String url) {
		Map<String, String> data = new HashMap<String, String>();
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}

		WebLinkInfo info = WebLinkInfo.get(url);
		data.put("url", url);
		data.put("title", info.getTitle());
		data.put("description", info.getDescription());
		data.put("thumb", info.getThumbnailUrl());
		data.put("favicon", info.getFaviconUrl());
		renderJSON(data);
	}

	// modal
	public static void mailShare(Long id) {
		if (!MAIL_SHARE_ENABLED) {
			forbidden("feature disabled");
		}

		Post post = Post.findById(id);
		notFoundIfNull(post);
		render(post);
	}

	// ajax
	@CheckAuthenticity
	public static void doMailShare(Post post, String recipients, String message, boolean comments) {
		if (!MAIL_SHARE_ENABLED) {
			forbidden("feature disabled");
		}

		if (post.isSharable()) {
			// brute-force protection
			if (Protection.max(Application.BRUTEFORCE_PROTECTION_MAX, Application.BRUTEFORCE_PROTECTION_BAN)) {
				forbidden("too many mails from your IP. please wait a few minutes before trying again.");
			}

			// share, one mail per recipient
			for (String recipient : Util.parseEmailList(recipients)) {
				ApplicationMailer.sharePost(UserLoader.getConnectedUser(), post, comments, message, recipient);
			}

			ok();
		}
		error();
	}

	// modal
	public static void weblinkPreview(String url) {
		WebLinkInfo info = WebLinkInfo.get(url);
		render(info);
	}

	// embedded (iframe)
	public static void audioPreview(Long id) {
		FilePostAttachment attachment = FilePostAttachment.findById(id);
		notFoundIfNull(attachment);
		render(attachment);
	}

	// ajax
	@Transactional
	public static void load(long wallId) {
		Wall wall = Wall.findById(wallId);
		notFoundIfNull(wall);
		EndlessHelper.prepare(wall.getPostsQuery(UserLoader.getConnectedUser()), true);
		render(wall);
	}

	// modal
	@Secure(permissions = Permission.ACCESS_POST, params = "id")
	public static void report(Long id) {
		if (!POST_REPORTING_ENABLED) {
			forbidden("feature disabled");
		}

		Post post = Post.findById(id);
		notFoundIfNull(post);
		renderArgs.put("destination", HttpUtil.getReferrerUrl());
		render(post);
	}

	@Secure(permissions = Permission.ACCESS_POST, params = "id")
	@CheckAuthenticity
	@Transactional
	public static void doReport(Long id, String destination, String message) {
		if (!POST_REPORTING_ENABLED) {
			forbidden("feature disabled");
		}

		Post post = Post.findById(id);
		notFoundIfNull(post);
		PostReportNotification.raise(post, message, UserLoader.getConnectedUser());
		flash.success(Messages.get("post.report.success"));

		if (!StringUtils.isEmpty(destination)) {
			redirect(destination);
		}
		Senders.show(post.wall.sender.id, null);
	}
}

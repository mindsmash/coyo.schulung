package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import license.LicenseValidator;
import license.v2.License;
import models.Application;
import models.Settings;
import models.Tenant;
import models.User;
import models.UserRole;
import multitenancy.MTA;

import org.dbunit.dataset.NoSuchTableException;

import play.Logger;
import play.Play;
import play.data.FileUpload;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.Transactional;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginManager;
import utils.JPAUtils;
import acl.UserGrant;
import apps.AppDescriptor;
import apps.AppManager;
import checks.PasswordSecurityCheck;
import conf.ApplicationSettings;
import data.TenantDataImporter;
import data.TenantDataImporter.InvalidImportFileException;

/**
 * Handles all actions for the installation process.
 * 
 * @author mindsmash GmbH
 * 
 */
@Transactional
public class Installation extends Controller {

	private static final String TIMEZONE = "Europe/Berlin";

	@Before
	static void check() {
		if (User.count() > 0) {
			redirect("/");
		}
	}

	@Before(only = {"importFromFolder", "importFromUpload" })
	static void checkImportAllowed() {
		if (!Application.DATA_IMPORT_ALLOWED) {
			redirect("/");
		}
	}

	public static void chooser() {
		if (!Application.DATA_IMPORT_ALLOWED) {
			form();
		}
		render();
	}

	public static void form() {
		render();
	}

	public static void save(@Required @Email String email, @Required String firstname, @Required String lastname,
			@Required @CheckWith(PasswordSecurityCheck.class) String password, @Required String appname, String license) {
		checkAuthenticity();
		Settings settings = Settings.findApplicationSettings();

		// validate license if needed
		if (!LicenseValidator.valid() && Validation.required("license", license).ok
				&& !LicenseValidator.check(license)) {
			Validation.addError("license", "validation.invalid");
		}

		if (Validation.hasErrors()) {
			render("@form", email, password, appname, firstname, lastname, license);
		}

		// application settings
		settings.setProperty(ApplicationSettings.APPLICATION_NAME, appname);
		settings.setProperty(ApplicationSettings.DEFAULT_TIMEZONE, TIMEZONE);
		ApplicationSettings.applyDefaultSettings(settings);
		settings.save();

		if (null != license) {
			Tenant tenant = ((Tenant) MTA.getActiveTenant());
			tenant.license = license;
			tenant.save();
		}

		// activate all plugins and apps
		for (PluginBase plugin : PluginManager.getPlugins()) {
			if (plugin.getClass().getAnnotation(LoadPlugin.class).defaultActivated()) {
				PluginManager.activate(plugin);
			}
		}
		for (AppDescriptor app : AppManager.getApps()) {
			AppManager.toggle(app, "page", true);
			AppManager.toggle(app, "workspace", true);
		}

		// clear license cache
		LicenseValidator.clearCache();

		// create admin role
		UserRole role = new UserRole();
		role.name = "Admin";
		role.grants.addAll(UserGrant.allSimple());
		role.save();

		// create user
		User user = new User();
		user.email = email;
		user.firstName = firstname;
		user.lastName = lastname;
		user.password = password;
		user.active = true;
		user.superadmin = true;
		user.timezone = TIMEZONE;
		user.role = role;

		if (Play.langs.contains(Lang.get())) {
			user.language = Lang.getLocale();
		}

		user.save();

		Auth.succeedAuthentication(email, false);
	}

	public static void importForm() {
		File importDir = getImportDir();
		List<File> importFiles = new ArrayList<>();
		for (File file : importDir.listFiles()) {
			if (!file.isDirectory() && file.getName().toLowerCase().endsWith(".zip")) {
				importFiles.add(file);
			}
		}
		renderArgs.put("imports", importFiles);
		render();
	}

	public static void importFromFolder(String fileName) {
		notFoundIfNull(fileName);
		if (fileName.contains(File.separator) || fileName.contains("..")) {
			forbidden();
		}
		File file = new File(getImportDir(), fileName);
		if (!file.exists()) {
			notFound();
		}
		startImport(file);
	}

	public static void importFromUpload(FileUpload file) {
		if (file == null) {
			importForm();
		}
		startImport(file.asFile());
	}

	@Util
	private static void startImport(File file) {
		try {
			// delete existing data of active tenant
			List<Settings> existingSettings = Settings.findAll();
			for (Settings settings : existingSettings) {
				settings.delete();
			}
			JPAUtils.commitAndRefreshTransaction();

			new TenantDataImporter(file).startImport(MTA.getActiveTenant());
		} catch (InvalidImportFileException e) {
			Logger.warn(e, "[Installation] Encountered an invalid import file");
			if (e.getMessage() != null) {
				flash.error(e.getMessage());
			} else {
				flash.error(Messages.get("installation.import.error"));
			}
			flash.keep();
			importForm();
		} catch (NoSuchTableException e) {
			Logger.error(
					e,
					"[Installation] Table mismatch. Table %s from the import file does not exist in the target system.",
					e.getMessage());
			flash.error(Messages.get("installation.import.tableMismatch", e.getMessage()));
			flash.keep();
			importForm();
		} catch (Exception e) {
			Logger.error(e, "[Installation] Error importing data");
			flash.error(Messages.get("installation.import.error"));
			flash.keep();
			importForm();
		}

		redirect("/");
	}

	@Util
	private static File getImportDir() {
		File importDir = new File(Application.getDataDir(MTA.getActiveTenant()), "imports");
		if (!importDir.exists()) {
			importDir.mkdirs();
		}
		return importDir;
	}
}

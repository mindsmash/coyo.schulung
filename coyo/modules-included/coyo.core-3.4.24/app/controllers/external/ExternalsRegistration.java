package controllers.external;

import checks.PasswordSecurityCheck;
import controllers.Auth;
import controllers.Workspaces;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import models.Sender;
import models.User;
import models.notification.WorkspaceJoinNotification;
import models.workspace.Workspace;
import multitenancy.MTA;
import multitenancy.MultitenancyExclude;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.validation.CheckWith;
import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Lang;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Router;
import play.mvc.Util;
import session.UserLoader;

import java.util.HashMap;
import java.util.Map;

/**
 * Generic class for registering and authenticating external users. Can be used
 * by any specific controller to allow external user activation.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@ShowExternals
@Transactional
@MultitenancyExclude
public class ExternalsRegistration extends Controller {

	public static void login(String token, String password) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("token", token);
		args.put("password", password);
		String destination = Router.reverse("external.ExternalsRegistration.join", args).url;
		Auth.storeDestinationInSession(destination);
		Auth.login();
	}
	
	public static void join(String token, String password) {
		Workspace workspace = validateTokenAndGetWorkspace(token);
		if (!workspace.hasValidExternalShareToken()) {
			flash("error", Messages.get("externals.join.expiredToken"));
			render(workspace, token);
		}

		// password form
		if (StringUtils.isEmpty(password)
				|| (!Crypto.passwordHash(password).equals(workspace.externalSharePassword) && !password
						.equals(workspace.externalSharePassword))) {
			render(workspace, token);
		}

		// check if already logged in
		if (Auth.getCurrentId() != null) {
			joinWorkspace(UserLoader.getConnectedUser(), workspace);
			Workspaces.show(workspace.id, null, workspace.getSlug(), null);
		}

		// if not logged in, display registration form
		renderArgs.put("hashedPassword", Crypto.passwordHash(password));
		render("@form", token);
	}

	@CheckAuthenticity
	public static void save(String token, String hashedPassword, @Email @Required String email,
			@CheckWith(PasswordSecurityCheck.class) @Required String password, @Required String firstName,
			@Required String lastName) {
		// check if logged in
		if (Auth.getCurrentId() != null) {
			redirect("/");
		}

		Workspace workspace = validateTokenAndGetWorkspace(token);

		// force activate tenant
		MTA.activateMultitenancy(workspace.tenant);

		// check password again
		if (StringUtils.isEmpty(hashedPassword) || !hashedPassword.equals(workspace.externalSharePassword)) {
			join(token, null);
		}

		Logger.trace("finishing external regsitraiton for %s", email);

		// check if user already exists
		if (User.count("byEmail", email) > 0) {
			validation.addError("email", Messages.get("registration.userAlreadyRegistered"));
		} else {
			// check if user existed before
			((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
			if (User.count("byEmail", email) > 0) {
				validation.addError("email", Messages.get("registration.userExistedBefore"));
			}
			((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);
		}

		// check for validation errors
		if (validation.hasErrors()) {
			render("@form", token, hashedPassword, email, firstName, lastName);
		}

		// create user
		User user = new User();
		user.external = true;
		user.active = true;
		user.email = email;
		user.password = password;
		user.firstName = firstName;
		user.lastName = lastName;

		if (Play.langs.contains(Lang.get())) {
			user.language = Lang.getLocale();
		}

		user.save();
		user.refresh();

		// add user to workspace
		joinWorkspace(user, workspace);

		// login
		flash.success(Messages.get("externals.registration.success"));
		Auth.succeedAuthentication(user.email, true);
	}

	@Util
	private static void joinWorkspace(User user, Workspace workspace) {
		if (user.external && !workspace.isMember(user)) {
			workspace.addMember(user, false);
			workspace.save();

			WorkspaceJoinNotification.raise(workspace, user);
		}
	}

	@Util
	private static Workspace validateTokenAndGetWorkspace(String token) {
		notFoundIfNull(token);
		Workspace workspace = Workspace.find("externalShareToken = ?", token).first();
		notFoundIfNull(workspace);
		return workspace;
	}
}

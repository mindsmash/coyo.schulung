package controllers.external;

import models.User;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Util;
import session.UserLoader;
import controllers.Auth;
import controllers.SecureBaseController;
import controllers.Users;
import controllers.external.annotations.AllowExternals;

/**
 * 
 * Handles all actions for the startpage of external users.
 * 
 * @author mindsmash GmbH
 * 
 */
@AllowExternals
public class Externals extends SecureBaseController {

	@Before
	static void allowExternalsOnly() {
		if (!UserLoader.getConnectedUser().external) {
			redirect("/");
		}
	}

	public static void home() {
		User user = UserLoader.getConnectedUser();
		checkWorkspaceConstraint(user);
		Users.info(user.id, user.getSlug());
	}
	
	@Util
	public static void checkWorkspaceConstraint(User externalUser) {
		if (externalUser.workspaces.size() == 0) {
			Auth.executeLogout();
			flash.error(Messages.get("externals.noWorkspaces.logout"));
			Auth.login();
		}
	}
}

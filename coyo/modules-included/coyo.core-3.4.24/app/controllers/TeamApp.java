package controllers;

import java.util.*;

import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.WikiAppArticleJsonSerializer;
import libs.DateI18N;
import models.User;
import models.app.WikiAppArticle;
import models.app.WikiAppArticleVersion;
import models.notification.WikiArticleNotification;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.modules.pdf.PDF;
import play.modules.pdf.PDF.MultiPDFDocuments;
import play.modules.pdf.PDF.Options;
import play.modules.pdf.PDF.PDFDocument;
import play.mvc.Http;
import play.mvc.Router;
import play.mvc.Http.Request;
import play.templates.JavaExtensions;
import security.Secure;
import util.HttpUtil;
import utils.PDFUtil;
import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.internal.StringMap;

import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

public class TeamApp extends WebBaseController {
	
	@CheckAuthenticity
	@Transactional
	public static void order(Long appId) {
		models.app.TeamApp app = models.app.TeamApp.findById(appId);
		notFoundIfNull(app);

		Security.checkAndCancel(Permission.EDIT_SENDER, app.sender.id);
		
		if(app.members.size() > 1) {
			String userlist = request.params.data.get("userlist")[0];
			String[] userlistArrayStr = userlist.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			Long[] userlistArray = new Long[userlistArrayStr.length];
			for(int i = 0; i < userlistArrayStr.length; ++i) {
				try {
					userlistArray[i] = Long.parseLong(userlistArrayStr[i]);
				} catch (NumberFormatException nfe) {
					userlistArray[i] = 9999L;
				};
			}
			if(userlistArray.length > 0) {
				Map<Integer, User> r = new TreeMap<Integer, User>();
				int priority = 0;
				boolean listContainsAllItems = true;
				
				for (Long uID : userlistArray) {
					boolean foundInList = false;
					Iterator it = app.members.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pair = (Map.Entry)it.next();
						User user = (User)pair.getValue();
						if (Long.compare(user.id, uID) == 0) {
							r.put(priority++, user);
							foundInList = true;
							break;
						}
					}
					if(!foundInList) {
						listContainsAllItems = false;
						break;
					}
				}
				
				if(listContainsAllItems) {
					app.clearList();
					Iterator it = r.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pair = (Map.Entry)it.next();
						app.addMember((Integer)pair.getKey(), (User)pair.getValue());
					}
					app.save();
				}
			}
		}
	}
}

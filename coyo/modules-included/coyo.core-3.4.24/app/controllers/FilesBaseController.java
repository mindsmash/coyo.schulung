package controllers;

import java.util.ArrayList;
import java.util.List;

import jobs.Zip;
import models.app.FilesApp;
import models.app.files.File;
import models.app.files.FileVersion;
import play.Logger;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Util;

public class FilesBaseController extends Controller {

	@Util
	public static void downloadFile(FilesApp app, File file, FileVersion version, boolean download) {
		notFoundIfNull(file);

		if (version == null) {
			version = file.getCurrentVersion();
		}

		// file download
		if (!file.isFolder()) {
			// IE8 SSL download fix
			if (request.secure) {
				response.setHeader("Cache-Control", "private, max-age=15");
			}

			renderBinary(version.get(), file.getName(), version.getSize(), file.getContentType(), !download);
		} else { // zip download
			List<String> fileIds = new ArrayList<String>();
			List<? extends File> files = file.getChildren();
			for (File child : files) {
				fileIds.add(child.getUid());
			}

			downloadZip(app, file.getName(), fileIds);
		}
	}

	@Util
	public static void downloadZip(FilesApp app, String name, List<String> files) {
		Promise<java.io.File> promise = new Zip(app.id, name, files).now();
		java.io.File zip = await(promise);
		if (zip == null || !zip.exists()) {
			Logger.error("error creating zip for files [%s]", files.toArray());
			error();
		}
		renderBinary(zip, name + ".zip");
	}
}
package controllers;

import json.AppWidgetDefinitionSerializer;
import json.AppWidgetMappingSerializer;
import json.AvailableAppWidgetsAppSerializer;
import models.app.App;
import models.app.WidgetMapping;

import org.apache.commons.lang.StringUtils;

import play.db.jpa.Transactional;
import security.Secure;
import session.UserLoader;
import util.Util;
import acl.Permission;
import apps.AppWidget;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;

/**
 * Creates, deletes or edits new App Widget Instances for a Page or Workspace.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class AppWidgets extends WebBaseController {

	public static void catalog(Long appId) {
		App app = App.findById(appId);
		notFoundIfNull(app);

		Security.checkAndCancel(Permission.EDIT_SENDER, app.sender.id);

		AvailableAppWidgetsAppSerializer appSerializer = new AvailableAppWidgetsAppSerializer();
		AppWidgetDefinitionSerializer appWidgetSerializer = new AppWidgetDefinitionSerializer();

		// load and determine the available widgets
		JsonArray arr = new JsonArray();
		for (App sourceApp : app.sender.apps) {
			JsonObject sourceAppJson = (JsonObject) appSerializer.serialize(sourceApp, null, null);

			// if other app has widgets
			if (!sourceApp.getAppDescriptor().getWidgets().isEmpty()) {
				JsonArray widgetsArr = new JsonArray();

				for (AppWidget appWidget : sourceApp.getAppDescriptor().getWidgets().values()) {
					// check if already added to this app
					boolean selectable = WidgetMapping.count("targetApp = ? AND sourceApp = ? AND widgetKey = ?", app,
							sourceApp, appWidget.getKey()) == 0;
					widgetsArr.add(appWidgetSerializer.serialize(appWidget, selectable));
				}

				sourceAppJson.add("widgets", widgetsArr);
				arr.add(sourceAppJson);
			}
		}

		renderJSON(arr);
	}

	@Transactional
	@CheckAuthenticity
	public static void create(Long targetAppId, Long sourceAppId, String widgetKey, String title) {
		App targetApp = App.findById(targetAppId);
		App sourceApp = App.findById(sourceAppId);

		if (targetApp.sender != sourceApp.sender) {
			error();
		}

		Security.checkAndCancel(Permission.EDIT_SENDER, targetApp.sender.id);

		WidgetMapping wm = new WidgetMapping();
		wm.targetApp = targetApp;
		wm.sourceApp = sourceApp;
		wm.widgetKey = widgetKey;
		wm.title = title;
		wm.save();

		renderJSON(new AppWidgetMappingSerializer().serialize(wm, UserLoader.getConnectedUser()).toString());
	}

	@Transactional
	@CheckAuthenticity
	public static void delete(Long id) {
		WidgetMapping widgetMapping = WidgetMapping.findById(id);
		notFoundIfNull(widgetMapping);

		Security.checkAndCancel(Permission.EDIT_SENDER, widgetMapping.targetApp.sender.id);

		widgetMapping.delete();
		ok();
	}

	@Transactional
	@CheckAuthenticity
	public static void sort(Long appId, String sortOrder) {
		App app = App.findById(appId);
		Security.checkAndCancel(Permission.EDIT_SENDER, app.sender.id);

		int prio = 1;
		for (String stringId : Util.parseList(sortOrder, ",")) {
			if (StringUtils.isNumeric(stringId)) {
				WidgetMapping wm = WidgetMapping.findById(Long.valueOf(stringId));
				if (wm != null && wm.targetApp == app) {
					wm.priority = prio++;
					wm.save();
				}
			}
		}
	}

	@AllowExternals
	@ShowExternals
	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	public static void widgets(Long id) {
		App app = App.findById(id);
		notFoundIfNull(app);

		AppWidgetMappingSerializer serializer = new AppWidgetMappingSerializer();
		JsonArray arr = new JsonArray();
		for (WidgetMapping wm : app.widgets) {
			arr.add(serializer.serialize(wm, UserLoader.getConnectedUser()));
		}

		renderJSON(arr);
	}
}

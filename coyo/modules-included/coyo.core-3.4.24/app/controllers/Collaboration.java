package controllers;

import injection.Inject;
import injection.InjectionSupport;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Likable;
import models.Sender;
import models.Sharable;
import models.User;
import models.comments.Comment;
import models.comments.Commentable;
import models.dao.EventDao;
import models.event.Event;
import models.notification.PageFollowNotification;
import models.notification.UserFollowNotification;
import models.page.Page;
import models.statistics.TrackingRecord;
import models.wall.Wall;
import models.wall.post.Post;
import models.workspace.Workspace;
import models.workspace.WorkspaceMember;
import play.Logger;
import play.data.Upload;
import play.db.jpa.Transactional;
import play.mvc.Router;
import play.mvc.Util;
import play.templates.Template;
import play.templates.TemplateLoader;
import session.UserLoader;
import acl.Permission;
import binding.ModelIdBinder;
import conf.ApplicationSettings;
import controllers.external.annotations.AllowExternals;
import controllers.external.annotations.ShowExternals;
import csrf.CheckAuthenticity;
import events.type.LikeEvent;

/**
 * This controller serves as an abstract class for actions and operations shared by multiple entity types.
 * 
 * This controller handles the whole liking mechanism in an abstract way. Any model entity that implements Likable can
 * be used.
 * 
 * This controller also handles the whole follow mechanism.
 * 
 * @author Jan Marquardt
 */
@InjectionSupport
public class Collaboration extends WebBaseController {

	@Inject(configuration="coyo.di.dao.event", defaultClass = EventDao.class, singleton = true)
	private static EventDao eventDao;
	
	// ajax
	@AllowExternals
	@Transactional
	@CheckAuthenticity
	public static void like(Long id, String clazz) {
		notFoundIfNull(id);
		notFoundIfNull(clazz);
		
		Likable likable = loadLikable(id, clazz);
		notFoundIfNull(likable);

		Security.checkAndCancel(Permission.LIKE, likable);

		if (likable.likes(UserLoader.getConnectedUser())) {
			likable.unlike(UserLoader.getConnectedUser());
		} else {
			likable.like(UserLoader.getConnectedUser());
			TrackingRecord.trackOne("likes");
		}

		// live inform interested users
		for (User interested : likable.getInterestedUsers()) {
			new LikeEvent(id, clazz).raise(interested.id);
		}

		renderLikelist(likable);
	}

	// ajax
	@AllowExternals
	public static void likelist(Long id, String clazz) {
		notFoundIfNull(id);
		notFoundIfNull(clazz);
		
		Likable likable = loadLikable(id, clazz);
		notFoundIfNull(likable);

		Security.checkAndCancel(Permission.LIST_LIKES, likable);

		Cache.cacheEntity(likable.getModel(), UserLoader.getConnectedUser().getPrivateCacheKey());
		renderLikelist(loadLikable(id, clazz));
	}

	@Util
	private static void renderLikelist(Likable likable) {
		Map<String, Object> tplArgs = new HashMap<String, Object>();
		tplArgs.put("likable", likable);
		Template template = TemplateLoader.load("Collaboration/likelist.html");
		String likelist = template.render(tplArgs);
		likelist = likelist.replaceAll("\n", "");
		likelist = likelist.replaceAll("\t", "");

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("status", likable.likes(UserLoader.getConnectedUser()));
		data.put("likelist", likelist);
		renderJSON(data);
	}

	@Util
	public static Likable loadLikable(Long id, String clazz) {
		try {
			Method m = Class.forName(clazz).getMethod("findById", Object.class);
			return (Likable) m.invoke(null, id);
		} catch (Exception e) {
			Logger.warn(e, "error loading likable object with id [%s] and class [%s]", id, clazz);
		}
		return null;
	}

	// ajax
	@AllowExternals
	@Transactional
	@CheckAuthenticity
	public static void createComment(Comment comment, Long id, String clazz) {
		Commentable commentable = loadCommentable(id, clazz);
		notFoundIfNull(commentable);

		Security.checkAndCancel(Permission.CREATE_COMMENT, commentable);

		comment.source = commentable;
		comment.author = UserLoader.getConnectedUser();
		comment.save();

		handleAttachments(comment);

		commentable.addComment(comment);
	}

	// ajax
	@AllowExternals
	@Transactional
	@CheckAuthenticity
	public static void removeComment(Long commentId, Long id, String clazz) {
		Commentable commentable = loadCommentable(id, clazz);
		Comment comment = Comment.findById(commentId);
		notFoundIfNull(commentable);
		notFoundIfNull(comment);

		Security.checkAndCancel(Permission.DELETE_COMMENT, commentId, commentable);

		commentable.removeComment(comment);
		comment.delete();
	}

	// ajax
	@ShowExternals
	@AllowExternals
	public static void comments(Long id, String clazz, boolean expanded) {
		final Commentable commentable = loadCommentable(id, clazz);
		notFoundIfNull(commentable);

		// find last modified date
		Date lastModified = commentable.getModel().modified;
		for (Comment comment : commentable.getComments()) {
			if (comment.modified.after(lastModified)) {
				lastModified = comment.modified;
			}
		}

		Cache.cacheEntity(commentable.getModel(), lastModified, UserLoader.getConnectedUser().getPrivateCacheKey());
		render(id, clazz, commentable, expanded);
	}

	@Util
	public static Commentable loadCommentable(Long id, String clazz) {
		try {
			Method m = Class.forName(clazz).getMethod("findById", Object.class);
			return (Commentable) m.invoke(null, id);
		} catch (Exception e) {
			Logger.warn(e, "error loading commentable object with id [%s] and class [%s]", id, clazz);
		}
		return null;
	}

	public static void share(Long id, String clazz) {
		Sharable sharable = loadSharable(id, clazz);
		notFoundIfNull(sharable);

		Security.checkAndCancel(Permission.SHARE, sharable);

		// load possible entities to share to
		// TODO Shouldn't we use user.getPages(null) here? Why is members and followers listed here? Maybe create and
		// use a DAO here.
		List<Page> pages = Page.find(
				"SELECT p FROM Page p WHERE ? MEMBER OF p.admins OR ? MEMBER OF p.members OR ? MEMBER OF p.followers",
				UserLoader.getConnectedUser(), UserLoader.getConnectedUser(), UserLoader.getConnectedUser()).fetch();

		List<Event> events = eventDao.getUpcomingCreatedEvents(UserLoader.getConnectedUser());

		// TODO Shouldn't we use user.getActiveWorkspaces(null) here? Maybe create and use a DAO here.
		List<Workspace> workspaces = new ArrayList<Workspace>();
		for (WorkspaceMember wm : UserLoader.getConnectedUser().workspaces) {
			if (!wm.workspace.archived) {
				workspaces.add(wm.workspace);
			}
		}

		render(id, clazz, sharable, pages, events, workspaces);
	}

	
	@Transactional
	@CheckAuthenticity
	public static void doShare(Long id, String clazz, String message, Wall wall, List<Long> users, boolean important,
			boolean postAsWallOwner) {
		Sharable sharable = loadSharable(id, clazz);

		Security.checkAndCancel(Permission.SHARE, id, clazz);

		// find walls to share on
		List<Wall> walls = new ArrayList<Wall>();
		if (wall != null && wall.isPersistent()) {
			walls.add(wall);
		} else if (users != null && users.size() > 0) {
			for (User user : ModelIdBinder.bind(User.class, users)) {
				walls.add(user.getDefaultWall());
			}
		}

		if (walls.size() == 0 || sharable == null || !sharable.isSharable()) {
			redirectToReferrer();
		}

		for (Wall w : walls) {
			Logger.debug("sharing on wall %s of sender %s", w, w.sender);

			// create post
			Post post = sharable.share(UserLoader.getConnectedUser(), w, message);

			// additional secured options
			if (Security.check(Permission.POST_STICKY, w.sender.id)) {
				post.important = important;
			}
			if (postAsWallOwner && Security.check(Permission.POST_AS_OTHER, w.sender.id)) {
				post.author = w.sender;
			}
			post.save();
		}

		// update activity stream
		UserLoader.getConnectedUser().getNewActivityStream(true);

		if (!request.isAjax()) {
			ActivityStream.index(null, null);
		}
	}

	@Util
	public static Sharable loadSharable(Long id, String clazz) {
		try {
			Method m = Class.forName(clazz).getMethod("findById", Object.class);
			return (Sharable) m.invoke(null, id);
		} catch (Exception e) {
			Logger.warn(e, "error loading sharable object with id [%s] and class [%s]", id, clazz);
		}
		return null;
	}

	private static Sender prepareFollow(Long id) {
		Sender sender = Sender.findById(id);

		if (sender == null) {
			if (request.isAjax()) {
				notFoundIfNull(sender);
			} else {
				redirectToReferrer();
			}
		}

		// check autofollow
		if (sender instanceof User && getSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
			if (request.isAjax()) {
				error("not allowed");
			} else {
				redirectToReferrer();
			}
		}

		return sender;
	}

	@Transactional
	@CheckAuthenticity
	public static void follow(Long id) {
		Sender sender = prepareFollow(id);
		User user = UserLoader.getConnectedUser();

		if (!sender.followers.contains(user)) {
			sender.followers.add(user);
			sender.save();

			// raise notification
			if (sender instanceof Page) {
				PageFollowNotification.raise((Page) sender, user);
			} else if (sender instanceof User) {
				UserFollowNotification.raise((User) sender, user);
			}
		}

		if (!request.isAjax()) {
			redirectToReferrer();
		}
	}

	@Transactional
	@CheckAuthenticity
	public static void unfollow(Long id) {
		Sender sender = prepareFollow(id);
		User user = UserLoader.getConnectedUser();

		if (sender.followers.contains(user)) {
			sender.followers.remove(user);
			sender.save();
		}

		if (sender instanceof Page) {
			// cleanup user notification
			PageFollowNotification.cleanup((Page) sender);
		}

		if (!request.isAjax()) {
			redirectToReferrer();
		}
	}

	@Util
	public static String getShareUrl(Sharable sharable) {
		Map<String, Object> args = new HashMap<>();
		args.put("id", sharable.getId());
		args.put("clazz", sharable.getClass().getName());
		return Router.reverse("Collaboration.share", args).url;
	}


	@play.mvc.Util
	public static void handleAttachments(final Comment comment) {
		comment.addAttachments((List<Upload>) request.args.get("__UPLOADS"));
	}

}
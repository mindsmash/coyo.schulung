package controllers;

import java.util.*;

import json.Dates;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.WikiAppArticleJsonSerializer;
import libs.DateI18N;
import models.User;
import models.app.WikiAppArticle;
import models.app.WikiAppArticleVersion;
import models.notification.WikiArticleNotification;
import org.apache.commons.lang3.StringUtils;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.modules.pdf.PDF;
import play.modules.pdf.PDF.MultiPDFDocuments;
import play.modules.pdf.PDF.Options;
import play.modules.pdf.PDF.PDFDocument;
import play.mvc.Http;
import play.mvc.Router;
import play.templates.JavaExtensions;
import security.Secure;
import session.UserLoader;
import util.HttpUtil;
import utils.PDFUtil;
import acl.AppsPermission;
import acl.Permission;
import binding.JsonBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.StringMap;

import controllers.external.annotations.AllowExternals;
import csrf.CheckAuthenticity;

@AllowExternals
public class WikiApp extends WebBaseController {

	public static void overview(Long appId) {
		models.app.WikiApp app = models.app.WikiApp.findById(appId);
		notFoundIfNull(app);
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("overview", true);
		redirectToSender(app.sender, app.id, args);
	}
	
	public static void article(Long id) {
		WikiAppArticle article = WikiAppArticle.findById(id);
		notFoundIfNull(article);

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("article", id);
		redirectToSender(article.app.sender, article.app.id, args);
	}

	// ajax
	@Transactional
	public static void form(Long appId, Long id, Long parent) {
		models.app.WikiApp app = models.app.WikiApp.findById(appId);
		notFoundIfNull(app);

		Security.checkAndCancel(AppsPermission.EDIT_WIKI, app.id);

		WikiAppArticle article;
		if (id != null) {
			article = app.getArticle(id + "");
			notFoundIfNull(article);
			renderArgs.put("possibleLinks", WikiAppArticle.find("app = ? AND id != ? ORDER BY title ASC", app, id)
					.fetch());

			// lock
			if (!article.isLocked()) {
				article.lock = UserLoader.getConnectedUser();
				article.save();
			}
		} else {
			article = new WikiAppArticle();
			article.app = app;

			if (parent != null) {
				article.parent = WikiAppArticle.findById(parent);
			}

			renderArgs.put("possibleLinks", WikiAppArticle.find("app = ? ORDER BY title ASC", app).fetch());
		}

		// load possible parent articles
		renderArgs.put("parents", getParents(article));
		render(article);
	}

	public static void children(Long id, Long level) {
		notFoundIfNull(id);
		notFoundIfNull(level);
		WikiAppArticle parent = WikiAppArticle.findById(id);
		notFoundIfNull(parent);

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, parent.app.id);

		render(parent, level);
	}

	public static void navigation(Long appId) {
		models.app.WikiApp app = models.app.WikiApp.findById(appId);
		notFoundIfNull(app);

		// check permissions
		Security.checkAndCancel(Permission.ACCESS_APP, app.id);

		render(app);
	}

	@Transactional
	@CheckAuthenticity
	public static void save(WikiAppArticle article, String text, Boolean redirectToEdit) {
		Security.checkAndRedirect(AppsPermission.EDIT_WIKI, "/", article.app.id);

		// check lock
		if (article.isLocked()) {
			if (article.lock != UserLoader.getConnectedUser()) {
				response.status = Http.StatusCode.BAD_REQUEST;
				validation.addError(
						"text",
						Messages.get(
								"app.wiki.article.locked",
								article.lock.getDisplayName(),
								JavaExtensions.format(article.modified,
										DateI18N.getLocalizedDateFormat("date.format.full"))));
				renderArgs.put("parents", getParents(article));
				render("@form", article, text);
			}

			// unlock
			article.lock = null;
		}

		play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);

		if (!article.validateAndSave()) {
			response.status = Http.StatusCode.BAD_REQUEST;
			renderArgs.put("parents", getParents(article));
			render("@form", article, text);
		} else {
			article.addVersion(text, UserLoader.getConnectedUser());
			article.save();
			// notification
			WikiArticleNotification.raise(article, UserLoader.getConnectedUser());
		}

		// make sure article is opened
		String url = HttpUtil.removeParam(HttpUtil.getReferrerUrl(), "form");
		if (Boolean.TRUE.equals(redirectToEdit)) {
			// go back to the form
			url = HttpUtil.appendParam(url, "form", article.id);
		}
		url = HttpUtil.appendParam(url, "article", article.id);
		renderJSON(url);
	}

	@Transactional
	@CheckAuthenticity
	public static void unlock(Long id) {
		WikiAppArticle article = WikiAppArticle.findById(id);
		notFoundIfNull(article);

		Security.checkAndRedirect(AppsPermission.EDIT_WIKI, "/", article.app.id);

		// unlock
		article.lock = null;
		article.save();

		play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);
	}

	// ajax
	@Transactional
	@CheckAuthenticity
	public static void createDynamic(Long id, String title) {
		models.app.WikiApp app = models.app.WikiApp.findById(id);
		notFoundIfNull(app);

		play.cache.Cache.delete(app.tenant.toString() + "models.app.WikiApp-" + app.id);


		WikiAppArticle dynamic = new WikiAppArticle();
		dynamic.app = app;
		dynamic.title = title;
		dynamic.save();
		dynamic.addVersion("", UserLoader.getConnectedUser());

		// return URL
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("id", dynamic.id);
		renderJSON("{\"url\":" + new Gson().toJson(Router.reverse("WikiApp.article", args).url) + "}");
	}

	public static void delete(Long id) {
		WikiAppArticle article = WikiAppArticle.findById(id);
		notFoundIfNull(article);

		Security.checkAndRedirect(AppsPermission.DELETE_WIKI_ARTICLE, "/", article);

		// TODO where is the delete happening?!
		play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);

		render(article);
	}

	@Transactional
	@CheckAuthenticity
	public static void doDelete(Long id, boolean retainChildren) {
		WikiAppArticle article = WikiAppArticle.findById(id);
		notFoundIfNull(article);
		Security.checkAndRedirect(AppsPermission.DELETE_WIKI_ARTICLE, "/", article);
		play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);
		article.delete(retainChildren);
	}

	public static void history(Long id) {
		WikiAppArticle article = WikiAppArticle.findById(id);

		if (article == null) {
			redirect("/");
		}
		Security.checkAndRedirect(AppsPermission.EDIT_WIKI, "/", article.app.id);

		render(article);
	}

	@Transactional
	@CheckAuthenticity
	public static void revert(Long id, Long version) {
		WikiAppArticle article = WikiAppArticle.findById(id);
		notFoundIfNull(article);

		Security.checkAndRedirect(AppsPermission.EDIT_WIKI, "/", article.app.id);

		WikiAppArticleVersion oldVersion = article.getVersion(version);
		if (oldVersion != null) {
			// clear cache
			play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);
			article.addVersion(oldVersion.text, oldVersion.author);
		}
	}

	@Secure(permissions = AppsPermission.EDIT_WIKI, params = "id")
	public static void chooseHome(Long id) {
		models.app.WikiApp app = models.app.WikiApp.findById(id);
		notFoundIfNull(app);
		render(app);
	}

	@Secure(permissions = AppsPermission.EDIT_WIKI, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void setHome(Long id, Long articleId) {
		models.app.WikiApp app = models.app.WikiApp.findById(id);
		notFoundIfNull(app);

		Senders.show(app.sender.id, id);
	}

	@Secure(permissions = Permission.ACCESS_APP, params = "id")
	@Transactional
	@CheckAuthenticity
	public static void getRootArticles(final Long id) {
		final models.app.WikiApp app = models.app.WikiApp.findById(id);
		if (app == null) {
			error();
		}
		final List<WikiAppArticle> rootArticles = app.getRootArticles();


		String jsn = (String) play.cache.Cache.get(app.tenant.toString() + "models.app.WikiApp-" + id);

		if (StringUtils.isEmpty(jsn)) {
			jsn = createBuilder().create().toJson(rootArticles);
			play.cache.Cache.set(app.tenant.toString() + "models.app.WikiApp-" + id , jsn, "1h");
		}

		renderJSON(jsn);
	}

	@Transactional
	@CheckAuthenticity
	public static void moveArticle(@JsonBody final StringMap<Double> params) {
		if (params.containsKey("id") && params.containsKey("destId") && params.containsKey("destIndex")) {
			final Double articleIdDouble = params.get("id");

			if (articleIdDouble == null) {
				error("Missing article id");
			}

			final long id = articleIdDouble.longValue();
			final Long destId;

			if (params.get("destId") != null) {
				destId = params.get("destId").longValue();
			} else {
				destId = null;
			}

			final int destIndex = params.get("destIndex").intValue();

			try {
				// clear cache
				final WikiAppArticle article = WikiAppArticle.findById(id);
				play.cache.Cache.delete(article.app.tenant.toString() + "models.app.WikiApp-" + article.app.id);

				WikiAppArticle.moveArticle(id, destId, destIndex);
			} catch (Exception ex) {
				error("Failed to move atrticle " + id);
			}

		} else {
			error("Missing parameter");
		}
	}

	@CheckAuthenticity
	@Transactional
	public static void isEditable(long id) {
		renderJSON(Security.check(AppsPermission.EDIT_WIKI, id));
	}

	/**
	 * Renders one article, with or without its children or a whole wiki app as PDF. If an article is specified as
	 * parameter, only this article is rendered. If the article id is not passed or null, all articles of the wiki are
	 * rendered into one PDF document.
	 */
	@Secure(permissions = Permission.ACCESS_APP, params = "appId")
	@AllowExternals
	public static void pdf(Long appId, Long articleId, boolean withChildren) {
		models.app.WikiApp app = models.app.WikiApp.findById(appId);
		notFoundIfNull(app);

		MultiPDFDocuments documents = new MultiPDFDocuments();
		if (articleId != null && articleId > 0) {
			WikiAppArticle article = WikiAppArticle.findById(articleId);
			notFoundIfNull(article);
			addPDFDocument(documents, article);
			if (withChildren) {
				for (WikiAppArticle childArticle : getChildren(article)) {
					addPDFDocument(documents, childArticle);
				}
			}
		} else {
			for (WikiAppArticle rootArticle : app.getRootArticles()) {
				addPDFDocument(documents, rootArticle);
				for (WikiAppArticle childArticle : getChildren(rootArticle)) {
					addPDFDocument(documents, childArticle);
				}
			}
		}
		PDF.renderPDF(documents);
	}

	/**
	 * Helper method to add a single article as PDF to a MultiPDFDocuments container.
	 */
	private static void addPDFDocument(MultiPDFDocuments documents, WikiAppArticle article) {
		if(article != null) {
			WikiAppArticleVersion waav = article.getCurrentVersion();
			if(waav != null) {
				Map<String, Object> args = new HashMap<String, Object>();
				args.put("article", article);
				args.put("content", PDFUtil.replaceInternalImageLinks(waav.text));
				PDFDocument document = new PDFDocument("WikiApp/pdf.html", new Options(), args);
				documents.add(document);
			}
		}
	}

	/**
	 * Helper method to get all children of an article recursively. This method flattens the children's hierarchal
	 * structure.
	 */
	private static List<WikiAppArticle> getChildren(WikiAppArticle article) {
		List<WikiAppArticle> children = new ArrayList<WikiAppArticle>();
		if (article.children.size() > 0) {
			for (WikiAppArticle child : article.children) {
				children.add(child);
				children.addAll(getChildren(child));
			}
		}
		return children;
	}

	private static List<WikiAppArticle> getParents(WikiAppArticle article) {
		List<WikiAppArticle> parents = new ArrayList<WikiAppArticle>(article.app.articles);
		parents.remove(article);

		if (article.isPersistent()) {
			// remove all child articles of current article from possible
			// parents
			for (WikiAppArticle parentArticle : new ArrayList<WikiAppArticle>(parents)) {
				if (parentArticle.isParent(article)) {
					parents.remove(parentArticle);
				}
			}
		}
		return parents;
	}

	private static GsonBuilder createBuilder() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(WikiAppArticle.class, new WikiAppArticleJsonSerializer());
		builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		builder.registerTypeAdapter(Date.class, Dates.serializer);
		builder.registerTypeAdapter(Date.class, Dates.deserializer);

		return builder;
	}

}
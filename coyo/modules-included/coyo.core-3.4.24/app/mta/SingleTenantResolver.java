package mta;

import models.Tenant;
import multitenancy.TenantResolver;
import play.Play;
import play.exceptions.UnexpectedException;
import play.mvc.Http.Request;

/**
 * Retrieve the only tenant that exists in the system.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class SingleTenantResolver implements TenantResolver {

	public static final boolean CACHE_TENANT_IN_REQUEST_ARGS = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.mta.tenant.cache", "true"));

	@Override
	public Tenant getActiveTenant(Request request) {
		Tenant tenant;

		if (CACHE_TENANT_IN_REQUEST_ARGS &&request != null) {
			tenant = (Tenant) request.current().args.get("tenant");
			if (tenant != null) {
				return tenant;
			}
		}

		tenant = getSingleTenant();

		if (CACHE_TENANT_IN_REQUEST_ARGS) {
			request.current().args.put("tenant", tenant);
		}

		return  tenant;
	}

	/**
	 * The the Tenant.
	 * 
	 * @return
	 */
	public static Tenant getSingleTenant() {
		if (0 == Tenant.count()) {
			Tenant t = new Tenant();
			if (!t.validateAndSave()) {
				throw new UnexpectedException("Cannot create single tenant");
			}
		}
		return Tenant.find("").first();
	}
}

package mta;

import play.db.jpa.JPABase;
import models.TenantModel;
import multitenancy.MTA;
import fishr.AsyncIndexer;

public class MultitenancyAsyncIndexer extends AsyncIndexer {

	@Override
	protected JPABase loadModel(String serialized) {
		MTA.forceMultitenancyDeactivation();
		JPABase jpaBase = super.loadModel(serialized);
		if(jpaBase instanceof TenantModel) {
			MTA.activateMultitenancy(((TenantModel) jpaBase).tenant);
		}
		return jpaBase;
	}
}

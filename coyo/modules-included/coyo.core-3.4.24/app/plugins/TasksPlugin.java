package plugins;

import java.util.ArrayList;
import java.util.List;

import models.TasksAppTask;
import play.i18n.Messages;
import session.UserLoader;

@LoadPlugin
public class TasksPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "tasks";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void rightbarTasksList() {
		List<TasksAppTask> all = TasksAppTask
				.find("SELECT t FROM TasksAppTask t WHERE t.app.active = true AND t.completed = false AND ? MEMBER OF t.users ORDER BY (CASE WHEN t.due IS NULL THEN 1 ELSE 0 END), t.due ASC",
						UserLoader.getConnectedUser()).fetch(7);

		List<TasksAppTask> tasks = new ArrayList<TasksAppTask>();
		for (TasksAppTask task : all) {
			if (task.app.active && task.app.sender.isActive()) {
				tasks.add(task);
			}
		}

		if (tasks.size() > 0) {
			renderArgs.put("tasks", tasks);
			render("rightbar.html");
		}
	}

	@Override
	public String getName() {
		return Messages.get("plugin.tasks");
	}
}
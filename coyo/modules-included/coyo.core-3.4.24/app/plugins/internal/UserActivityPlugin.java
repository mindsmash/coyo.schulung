package plugins.internal;

import java.util.Date;

import models.comments.Comment;
import models.Settings;
import models.User;
import models.wall.post.Post;
import models.wall.post.PostUser;

import org.apache.commons.lang.time.DateUtils;

import play.cache.Cache;
import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;

/**
 * 
 * Shows the connected {@link User}s activity.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class UserActivityPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "userActivity";
	}

	@Override
	public String getName() {
		return Messages.get("plugin.userActivity");
	}

	@Override
	public boolean hasSettings() {
		return true;
	}

	@Override
	public void renderSettings() {
		renderArgs.put("isPublic", getSettings().getBoolean("public"));
		render("settings.html");
	}

	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("public", request.params.get("public"));
		settings.save();
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void activityStreamRightbar() {
		renderArgs.put("isPublic", getSettings().getBoolean("public"));
		render("rightbar.html");
	}

	@PluginHook(value = PluginHooks.AFTER_RIGHTBAR, actions = { "Users.info", "Users.wall" })
	public void userRightbar() {
		renderArgs.put("isPublic", getSettings().getBoolean("public"));
		render("rightbar.html");
	}

	public static Integer getUserActivity(User user) {
		if (user == null) {
			return 0;
		}

		String cacheKey = "userActivity-" + user.id;
		Integer activity = Cache.get(cacheKey, Integer.class);
		if (activity != null) {
			return activity;
		}

		float tmp = 0F;

		Date range = DateUtils.addDays(new Date(), -14);

		// posts (at least 1 post per day)
		float postPoints = 25F;
		tmp += postPoints * Post.count("author = ? AND created >= ?", user, range);

		// posts (at least 1 post per day)
		float commentPoints = 20F;
		tmp += commentPoints * Comment.count("author = ? AND created >= ?", user, range);

		// likes (at least 2 per day)
		float likePoints = 15F;
		long likes = PostUser.count("user = ? AND likes = true AND created >= ?", user, range);
		likes += Comment.count("? MEMBER OF e.likes AND created >= ?", user, range);
		tmp += likePoints * likes;

		// last profile update within range
		float updateProfilePoints = 20F;
		if (user.modified.after(range)) {
			tmp += updateProfilePoints;
		}

		Integer r = (int) Math.min(100, tmp);
		Cache.set(cacheKey, r, "15min");
		return r;
	}
}

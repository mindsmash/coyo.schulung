package plugins.internal;

import java.util.List;

import models.Settings;
import models.User;
import play.db.helper.JpqlSelect;
import play.db.jpa.JPA;
import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;
import util.Util;
import conf.ApplicationSettings;
import controllers.WebBaseController;

/**
 * 
 * Shows 10 random colleagues.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class MeetColleaguesPlugin extends PluginBase {

	private static final int PAGE_SIZE = 10;

	@Override
	public String getKey() {
		return "meetColleagues";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
			return;
		}

		User user = UserLoader.getConnectedUser();
		renderArgs.put("cacheKey", getKey() + "-"
				+ user.language.toString() + "-"
				+ user.id);
		render("rightbar.html");
	}

	public static List<User> getColleagues() {
		User user = UserLoader.getConnectedUser();
		JpqlSelect select = new JpqlSelect();
		select.from("User u").where("u.id != ? AND u.active = true AND u.external = false").param(user.id);

		if (!WebBaseController.getSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)) {
			List<Long> following = JPA.em()
					.createQuery("SELECT f.id FROM User u JOIN u.following f WHERE u.id = ? AND TYPE(f) = User")
					.setParameter(1, user.id).getResultList();
			if (following.size() > 0) {
				select.andWhere("u.id NOT IN (" + Util.implode(following, ",") + ")");
			}
		}

		/* select a random page to display */
		long count = User.count(select.toString(), select.getParams().toArray());
		int pages = (int) (count / PAGE_SIZE);
		int start = (int) (Math.random() * pages);

		// cannot use "ORDER BY RAND()" because it is way too slow
		// the password field is encrypted, hence somewhat random
		double rand = Math.random();
		if (rand < 0.2D) {
			select.orderBy("password");
		} else if (rand < 0.4D) {
			select.orderBy("id");
		} else if (rand < 0.6D) {
			select.orderBy("lastname");
		} else if (rand < 0.8D) {
			select.orderBy("firstname");
		}

		/* get users and cache list for 15 minutes */
		return User.find(select.toString(), select.getParams().toArray()).fetch(start, PAGE_SIZE);
	}

	@Override
	public String getName() {
		return Messages.get("plugin.meetColleagues");
	}
}

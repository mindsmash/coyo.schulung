package plugins.internal;

import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import conf.ApplicationSettings;
import controllers.WebBaseController;

/**
 * 
 * Shows an Plugin to invite a colleague to the network via his mail address.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class InviteOthersPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "inviteOthers";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		if (WebBaseController.getSettings().getBoolean(ApplicationSettings.REGISTRATION)) {
			render("rightbar.html");
		}
	}

	@Override
	public String getName() {
		return Messages.get("plugin.inviteOthers");
	}
}

package plugins.internal;

import models.Settings;

import org.apache.commons.lang.StringUtils;

import play.cache.Cache;
import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import utils.CacheUtils;

/**
 * 
 * Shows a system-wide notification text at the top of the page.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin(defaultActivated = false)
public class SystemNotificationPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "systemNotification";
	}

	@Override
	public boolean hasSettings() {
		return true;
	}

	private String getCacheKey() {
		return CacheUtils.createTenantUniqueCacheKey(getKey() + "-text");
	}

	@Override
	public void renderSettings() {
		renderArgs.put("text", getSettings().getString("text"));
		render("settings.html");
	}

	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("text", request.params.get("text"));
		settings.save();
		Cache.delete(getCacheKey());
	}

	@Override
	public String getName() {
		return Messages.get("plugin.systemNotification");
	}

	@PluginHook(PluginHooks.BEFORE_CONTENT)
	public void show() {
		String text = Cache.get(getCacheKey(), String.class);
		if (text == null) {
			text = getSettings().getString("text");
			if (text == null) {
				text = "";
			}
			Cache.set(getCacheKey(), text);
		}

		if (!StringUtils.isEmpty(text)) {
			renderArgs.put("text", text);
			render("show.html");
		}
	}
}

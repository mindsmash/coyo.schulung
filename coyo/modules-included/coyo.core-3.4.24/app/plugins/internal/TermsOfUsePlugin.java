package plugins.internal;

import injection.Inject;
import injection.InjectionSupport;
import models.Application;
import models.Settings;
import models.User;

import org.apache.commons.lang.StringUtils;

import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;

@InjectionSupport
@LoadPlugin(defaultActivated = false)
public class TermsOfUsePlugin extends PluginBase {

	public static final String KEY = "termsOfUse";

	@Inject(configuration = "coyo.termsOfUse.resultHandler.class", defaultClass = ResultHandler.class)
	public static ResultHandler resultHandler;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		return Messages.get("plugin.termsOfUse.title");
	}

	@Override
	public boolean hasSettings() {
		return true;
	}

	@Override
	public void renderSettings() {
		renderArgs.put("text", getText());
		renderArgs.put("mediaLibrary", Application.get().getMediaLibrary());
		render("settings.html");
	}

	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("text", request.params.get("text"));
		settings.save();

		if (Boolean.parseBoolean(request.params.get("reset"))) {
			JPA.em().createQuery("UPDATE User u SET termsOfUseAccepted = NULL").executeUpdate();
		}
	}

	@PluginHook(PluginHooks.NAVBAR_DROPDOWN)
	public void navbarButton() {
		render("navbarButton.html");
	}

	public String getTitle() {
		String title = getSettings().getString("title");
		if (StringUtils.isEmpty(title)) {
			title = getName();
		}
		return title;
	}

	public String getText() {
		return getSettings().getString("text");
	}

	/**
	 * Allows for custom logic to be implemented when users accept or deny the terms of use.
	 */
	public static class ResultHandler {

		public void handleAccept(User user) {
		}

		public void handleDeny(User user) {
		}

		public boolean allowDeny(User user) {
			return false;
		}
	}
}

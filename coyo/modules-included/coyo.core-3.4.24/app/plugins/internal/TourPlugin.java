package plugins.internal;

import models.User;
import play.Play;
import play.cache.Cache;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;
import theme.Theme;
import utils.JPAUtils;

/**
 * 
 * Helps the User to get to know all features of Coyo after his first login.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class TourPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "tour";
	}

	@Override
	public String getName() {
		return "Tour Plugin";
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "ActivityStream.index")
	public void home() {
		tour("home");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Pages.index")
	public void pages() {
		tour("pages");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Pages.show")
	public void page() {
		tour("page");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Events.index")
	public void events() {
		tour("events");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Events.show")
	public void event() {
		tour("event");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Workspaces.index")
	public void workspaces() {
		tour("workspaces");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Workspaces.show")
	public void workspace() {
		tour("workspace");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Colleagues.index")
	public void colleagues() {
		tour("colleagues");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Users.wall")
	public void profile() {
		tour("profile");
	}

	@PluginHook(value = PluginHooks.AFTER_CONTENT, actions = "Apps.choose")
	public void apps() {
		tour("apps");
	}

	private void tour(String tour) {
		// check if user has seen the tour
		User user = UserLoader.getConnectedUser();

		String cacheKey = "tour-" + tour + "-seen-by-" + user.id;
		if (Cache.get(cacheKey) != null)
			return;

		if (!"web".equals(Theme.getActiveTheme()))
			return;

		if (!user.properties.containsKey("tour-" + tour)) {
			if (Play.mode.isProd()) {
				user.properties.put("tour-" + tour, "seen");
				user.save();

				// store in cache if user already saw this
				Cache.set(cacheKey, Boolean.TRUE);

				JPAUtils.makeTransactionWritable();
			}

			render(tour + ".html");
		}
	}
}

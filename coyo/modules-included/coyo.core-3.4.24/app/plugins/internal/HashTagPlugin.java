package plugins.internal;

import models.Settings;
import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;

/**
 * Shows the tag cloud.
 *
 * @author mindsmash GmbH
 */
@LoadPlugin
public class HashTagPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "hashTag";
	}

	@Override
	public String getName() {
		return Messages.get("plugin.hashTag");
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void activityStreamRightbar() {
		render("rightbar.html");
	}
}
package plugins.internal;

import models.Settings;
import models.User;
import models.page.Page;
import models.wall.post.Post;

import org.apache.commons.lang.StringUtils;

import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;
import conf.ApplicationSettings;

/**
 * 
 * Displays which shows the {@link User} which steps are left to complete his profile.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class CompleteProfilePlugin extends PluginBase {

	@Override
	public String getKey() {
		return "completeProfile";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		// disable posts filter
		Post.disableDateFilter();

		User user = UserLoader.getConnectedUser();
		boolean step1 = !StringUtils.isEmpty(user.workPhone);
		boolean step2 = user.avatar.exists();
		boolean step3 = Settings.findApplicationSettings().getBoolean(ApplicationSettings.AUTOFOLLOW)
				|| user.getRealFollowingCount() > 0;
		boolean step4 = Page.count("? MEMBER OF e.followers", user) > 0;
		boolean step5 = Post.count("author = ? AND message != NULL", user) > 0;

		renderArgs.put("step1", step1);
		renderArgs.put("step2", step2);
		renderArgs.put("step3", step3);
		renderArgs.put("step4", step4);
		renderArgs.put("step5", step5);

		if (step1 && step2 && step3 && step4 && step5) {
			renderArgs.put("completed", true);
		}

		render("rightbar.html");
	}

	@Override
	public String getName() {
		return Messages.get("plugin.completeProfile");
	}
}

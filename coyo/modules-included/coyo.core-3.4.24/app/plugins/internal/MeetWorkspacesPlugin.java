package plugins.internal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import models.User;
import models.workspace.Workspace;
import play.db.jpa.JPA;
import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;
import acl.Permission;
import controllers.Security;

/**
 * 
 * Shows the workspaces with the most of my colleagues (people I follow) being members.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class MeetWorkspacesPlugin extends PluginBase {

	private static final int MAX = 5;

	@Override
	public String getKey() {
		return "meetWorkspaces";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		User user = UserLoader.getConnectedUser();
		renderArgs.put("cacheKey", getKey() + "-"
				+ user.language.toString() + "-"
				+ user.id);
		render("rightbar.html");
	}

	// TODO: add limit
	public static List<Workspace> getWorkspaces() {
		User user = UserLoader.getConnectedUser();
		List<Workspace> show = new ArrayList<>();

		/*
		 * This query selects all workspaces which the connected user is not a member of sorted by the number of
		 * workspace members that the connected user is also following (intersection).
		 */
		Query query = JPA
				.em()
				.createQuery(
						"SELECT ws, "
								+ "(SELECT COUNT(wm) FROM WorkspaceMember wm WHERE wm.workspace = ws AND wm.user IN (SELECT u FROM User u WHERE ? MEMBER OF u.followers)) "
								+ "AS membersIFollow FROM Workspace ws WHERE ws.archived = false AND NOT EXISTS (SELECT wm FROM WorkspaceMember wm WHERE wm.workspace = ws AND wm.user = ?) "
								+ "ORDER BY membersIFollow DESC, SIZE(ws.members) DESC").setParameter(1, user)
				.setParameter(2, user);
		List<Object[]> data = query.getResultList();

		List<Workspace> workspaces = new ArrayList<>();
		for (Object[] arr : data) {
			workspaces.add((Workspace) arr[0]);
		}

		for (Workspace ws : workspaces) {
			if (Security.check(Permission.ACCESS_WORKSPACE, ws.id)) {
				show.add(ws);
			}

			if (show.size() >= MAX) {
				break;
			}
		}

		return show;
	}

	@Override
	public String getName() {
		return Messages.get("plugin.meetWorkspaces");
	}
}

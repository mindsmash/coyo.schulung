package plugins.internal;

import java.util.List;

import models.User;
import models.event.Event;
import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;

/**
 * 
 * Shows all upcoming events for the connected {@link User}.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class UpcomingEventsPlugin extends PluginBase {

	public static final int EVENTS_TO_DISPLAY = 7;

	@Override
	public String getKey() {
		return "upcomingEvents";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		User user = UserLoader.getConnectedUser();
		renderArgs.put("user", user);
		renderArgs.put("cacheKey", getKey() + "-"
				+ UserLoader.getConnectedUser().language.toString() + "-"
				+ user.id);
		renderArgs.put("eventsToDisplay", EVENTS_TO_DISPLAY);
		render("rightbar.html");
	}

	@Override
	public String getName() {
		return Messages.get("plugin.upcomingEvents");
	}
}

package plugins.internal;

import onlinestatus.OnlineStatusServiceFactory;
import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;

/**
 * Shows users that are currently online.
 * 
 * @author mindsmash GmbH
 */
@LoadPlugin
public class OnlineUsersPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "onlineUsers";
	}

	@PluginHook(value = PluginHooks.AFTER_DEFAULT_SIDEBAR)
	public void afterSidebar() {
		renderArgs.put("onlineUserCount", OnlineStatusServiceFactory.getOnlineStatusService().getOnlineUserCount());
		render("sidebar.html");
	}

	@Override
	public String getName() {
		return Messages.get("plugin.onlineUsers");
	}
}

package plugins.internal;

import models.Settings;
import multitenancy.MTA;

import org.joda.time.DateTime;

import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;

/**
 * 
 * Shows upcoming birthdays in the sidebar.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
public class UpcomingBirthdaysPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "upcomingBirthdays";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		renderArgs.put("cacheKey", "upcomingBirthdayPlugin-" + getDaysToShow() + "-" + new DateTime().getYear() + "-"
				+ new DateTime().getDayOfYear() + "-"
				+ UserLoader.getConnectedUser().language.toString() + "-"
				+ MTA.getActiveTenant().id);
		renderArgs.put("daysToShow", getDaysToShow());
		render("rightbar.html");
	}

	@Override
	public boolean hasSettings() {
		return true;
	}

	@Override
	public void renderSettings() {
		renderArgs.put("daysToShow", getDaysToShow());
		render("settings.html");
	}

	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("daysToShow", request.params.get("daysToShow"));
		settings.save();
	}

	@Override
	public String getName() {
		return Messages.get("plugin.upcomingBirthdays");
	}

	public int getDaysToShow() {
		int daysToShow = 10;
		try {
			daysToShow = getSettings().getInteger("daysToShow");
		} catch (NumberFormatException ignored) {
		}
		return daysToShow;
	}
}

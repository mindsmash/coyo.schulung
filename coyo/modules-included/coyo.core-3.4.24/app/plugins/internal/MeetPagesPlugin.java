package plugins.internal;

import injection.Inject;
import injection.InjectionSupport;

import java.util.List;

import models.User;
import models.dao.PageDao;
import models.page.Page;
import play.i18n.Messages;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;
import session.UserLoader;

/**
 * 
 * Shows the pages with the most followers that the user is not already
 * following.
 * 
 * @author mindsmash GmbH
 * 
 */
@LoadPlugin
@InjectionSupport
public class MeetPagesPlugin extends PluginBase {

	@Inject(configuration = "coyo.di.dao.page", defaultClass = PageDao.class, singleton = true)
	private static PageDao pageDao;

	private static final int MAX = 5;

	@Override
	public String getKey() {
		return "meetPages";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void beforeRightbar() {
		User user = UserLoader.getConnectedUser();
		renderArgs.put("cacheKey", getKey() + "-"
				+ user.language.toString() + "-"
				+ user.id);
		render("rightbar.html");
	}

	public static List<Page> getPages() {
		return pageDao.getInterestingPages(UserLoader.getConnectedUser(), MAX);
	}

	@Override
	public String getName() {
		return Messages.get("plugin.meetPages");
	}
}

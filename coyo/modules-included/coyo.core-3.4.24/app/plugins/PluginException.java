package plugins;

/**
 * Offers plugins the possibility to throw exceptions.
 * 
 * @author mindsmash GmbH
 * 
 */
public class PluginException extends RuntimeException {

	public PluginException() {
		super();
	}

	public PluginException(String message) {
		super(message);
	}

	public PluginException(String message, Throwable cause) {
		super(message, cause);
	}
}

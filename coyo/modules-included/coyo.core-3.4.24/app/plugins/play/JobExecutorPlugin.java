package plugins.play;

import java.util.ArrayList;
import java.util.List;

import play.Play;
import play.PlayPlugin;
import play.jobs.Job;

/**
 * Executes jobs when JPA is done. Usually needed when a job requires a newly created DB object.
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
public class JobExecutorPlugin extends PlayPlugin {

	/**
	 * Use a specialized version of {@link ThreadLocal}, which returns an empty {@link ArrayList} if no value is set.
	 */
	private static class JobThreadLocal extends ThreadLocal<List<Job>> {
		@Override
		protected List<Job> initialValue() {
			return new ArrayList<>();
		}
	}

	private final static ThreadLocal<List<Job>> jobsToRunAfterAction = new JobThreadLocal();

	public static void addJob(Job job) {
		jobsToRunAfterAction.get().add(job);
	}

	@Override
	public void afterInvocation() {
		// these are skipped in test mode by default
		if ("test".equals(Play.id)) {
			return;
		}

		List<Job> jobs = jobsToRunAfterAction.get();
		if (jobs != null && !jobs.isEmpty()) {
			for (Job job : jobs) {
				job.now();
			}
		}
		jobsToRunAfterAction.set(new ArrayList<Job>());
	}
}

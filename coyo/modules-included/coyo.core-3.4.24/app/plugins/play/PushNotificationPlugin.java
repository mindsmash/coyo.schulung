package plugins.play;

import conf.ApplicationSettings;
import extensions.CommonExtensions;
import injection.Inject;
import injection.InjectionSupport;
import license.LicenseValidator;
import lombok.extern.slf4j.Slf4j;
import models.AbstractTenant;
import models.Settings;
import models.User;
import models.UserNotification;
import models.UserPushDevice;
import models.messaging.ConversationMember;
import models.wall.post.Post;
import multitenancy.MTA;
import notifiers.push.PNSPusher;
import notifiers.push.PushNotification;
import notifiers.push.Pusher;
import org.apache.commons.lang.StringUtils;
import play.Play;
import play.PlayPlugin;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import utils.HibernateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@InjectionSupport
/**
 * Provides push notification functionality. It stores and distributes push notifications for delivery. Notification
 * delivery is performed by a thread of {@link PushNotificationPlugin.PNSWorker} which is started during application start. On
 * application stop the {@link PushNotificationPlugin.PNSWorker} thread will be stopped. All push notifications are stored within a
 * guava cache (https://code.google.com/p/guava-libraries/wiki/CachesExplained) using a configurable item expiry time.
 * Expired push notifications will be dropped and therefore never be delivered.
 */
public class PushNotificationPlugin extends PlayPlugin {

	private static final LinkedBlockingQueue<PushNotification> NOTIFICATIONS = new LinkedBlockingQueue<>();

	@Inject(configuration = "coyo.pusher", singleton = true, defaultClass = PNSPusher.class)
	private static Pusher pusher;

	public static boolean running = false;

	/**
	 * Starts a new thread of {@link PNSWorker} that distributes all push notifications to the {@link Pusher}.
	 */
	@Override
	public void onApplicationStart() {
		running = true;
		new Thread(new PNSWorker(), PNSWorker.class.getName()).start();
	}

	/**
	 * Stops the thread running {@link PNSWorker}.
	 */
	@Override
	public void onApplicationStop() {
		running = false;
	}

	/**
	 * Check if push notification delivery is activated or not.
	 *
	 * @return
	 */
	private static boolean isActivated() {
		if (Settings.findApplicationSettings().getBoolean(ApplicationSettings.PUSH)) {
			if (LicenseValidator.valid()) {
				return true;
			} else {
				log.warn("[PNS] Skipping push notification sending because no valid license was found");
			}
		}
		return false;
	}

	/**
	 * Informs a user about a new notification.
	 */
	public synchronized static void push(UserNotification un) {
		if (!Play.runingInTestMode()) {
			final PushNotification notification = new PushNotification(un.user, un.notification.getText(un),
					new Long(un.user.getTotalUnreadCount()).intValue());
			addNotification(notification);
		}
	}

	/**
	 * Informs a user about a new post in a conversation.
	 */
	public synchronized static void push(ConversationMember target, Post post) {
		final PushNotification notification = new PushNotification(target.user,
				post.author.getDisplayName() + ": " + CommonExtensions.shorten(post.message, 100),
				new Long(target.user.getTotalUnreadCount()).intValue());
		addNotification(notification);
	}

	/**
	 * Updates the badge for an user
	 */
	public synchronized static void push(User user, int newBadge) {
		final PushNotification notification = new PushNotification(user, null, newBadge);
		addNotification(notification);
	}

	/**
	 * Stores push notifications for delivery by {@link PNSWorker}
	 *
	 * @param notification the {@link PushNotification} that shall be delivered to the {@link UserPushDevice}
	 */
	private synchronized static void addNotification(final PushNotification notification) {
		if (isActivated()) {
			try {
				NOTIFICATIONS.put(notification);
			} catch (InterruptedException ex) {
				log.error("[PNS] Failed to push notification on queue: " + ex.getLocalizedMessage(), ex);
			}
		}
	}

	/**
	 * PNS working thread that will be started by the {@link PushNotificationPlugin} during application start and will
	 * be stopped on application stop. It uses an implementation of {@link Pusher} to deliver the push notifications to
	 * the {@link UserPushDevice}.
	 */
	public static class PNSWorker implements Runnable {
		@Override
		public void run() {
			while (PushNotificationPlugin.running) {
				try {
					log.debug("[PNS] Looking for new push notifications to distribute");

					PushNotification notification;
					while ((notification = NOTIFICATIONS.take()) != null) {
						try {
							JPAPlugin.startTx(true);

							MTA.activateMultitenancy(notification.getUser().tenant);

							final User user = User.findById(notification.getUser().id);
							final AbstractTenant tenant = user.tenant;

							final List<UserPushDevice> pushDeviceList = new ArrayList<>();
							pushDeviceList.addAll(HibernateUtils.unproxy(user.pushDevices));

							user.pushDevices = pushDeviceList;

							log.debug("[PNS] Trying to push the notification via the Pusher");

							if (StringUtils.isNotBlank(notification.getMessage())
									&& notification.getBadge() != null) {
								pusher.push(user, notification.getMessage(), notification.getBadge());
							} else if (StringUtils.isNotBlank(notification.getMessage())) {
								pusher.push(user, notification.getMessage());
							} else if (notification.getBadge() != null) {
								pusher.push(user, notification.getBadge());
							}

						} catch (Exception ex) {
							log.error("[PNS] Failed to push notification for tenant: " + notification.getUser().tenant.getId(),
									ex.getLocalizedMessage(), ex);
						} finally {
							MTA.clearThreadLocals();
							try {
								if (JPA.isInsideTransaction()) {
									JPAPlugin.closeTx(true);
								}
							} catch (Exception ex) {
								log.error("[PNS] Failed to close JPA transaction: " + ex.getLocalizedMessage(), ex);
							}
						}
					}
				} catch (Throwable e) {
					log.error("[PNS] Unexpected error processing push notifications: " + e.getLocalizedMessage(), e);
				}
			}
		}
	}
}

package plugins.play;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.changelog.ChangeSet;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.ext.PlayLogger;
import liquibase.logging.LogFactory;
import liquibase.resource.ClassLoaderResourceAccessor;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.db.DB;

/**
 * Checks the database schema version and executes evolution scripts if
 * necessary. This is running before the Hibernate scripts.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class DbEvolutionsPlugin extends PlayPlugin {

	private static final String CHANGELOG_FILE = "db-evolutions.xml";
	private static final String CHANGELOG_FILE_PRE = "db-evolutions-pre.xml";
	private static final String CHANGELOG_FILE_POST = "db-evolutions-post.xml";
	
	public static boolean updated;
	
	@Override
	public void onApplicationStart() {
		if (Play.mode.isDev() && !Play.id.endsWith("-db")) {
			return;
		}

		// load connection
		Connection conn = DB.getConnection();

		try {
			System.setProperty("liquibase.databaseChangeLogTableName", "db_changelog");
			System.setProperty("liquibase.databaseChangeLogLockTableName", "db_changelock");

			/* prepare connection for liquibase */
			Database database = getDatabase(conn);

			/* check if main evolutions exist - fail early otherwise. */
			if (Play.classloader.getResource(CHANGELOG_FILE) == null) {
				throw new FileNotFoundException("could not find evolutions file [" + CHANGELOG_FILE + "]");
			}

			/* force custom logger */
			Map<String, liquibase.logging.Logger> loggers = new HashMap<String, liquibase.logging.Logger>();
			loggers.put("liquibase", new PlayLogger());
			Field field = LogFactory.class.getDeclaredField("loggers");
			field.setAccessible(true);
			field.set(null, loggers);

			/* execute custom evolutions before the main changes */
			if (Play.classloader.getResource(CHANGELOG_FILE_PRE) != null) {
				executeLiquibase(database, CHANGELOG_FILE_PRE);
			}
			
			/* execute main liquibase evolutions and update script */
			UpgradePlugin.loadUpdateScript().preLiquibase();
			executeLiquibase(database, CHANGELOG_FILE);
			UpgradePlugin.loadUpdateScript().postLiquibase();
			
			/* execute custom evolutions after the main changes */
			if (Play.classloader.getResource(CHANGELOG_FILE_POST) != null) {
				executeLiquibase(database, CHANGELOG_FILE_POST);
			}
			
			/* all updates were executed */
		} catch (Exception e) {
			throw new Error("[DbEvolutions] error executing database evolutions.", e);
		}
	}

	private void executeLiquibase(Database database, String changeLog) throws LiquibaseException, DatabaseException {
		Liquibase liquibase = new Liquibase(changeLog, new ClassLoaderResourceAccessor(Play.classloader),
				database);
		List<ChangeSet> changeSets = liquibase.listUnrunChangeSets(new Contexts());

		if (changeSets.size() > 0) {
			Logger.info("[DbEvolutions] Running database evolutions defined in file '%s'.", changeLog);
			liquibase.update(new Contexts());
			updated = true;
		} else {
			updated =  false;
		}
	}

	public static Database getDatabase(Connection conn) throws DatabaseException {
		return DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
	}
}

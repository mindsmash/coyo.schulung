package plugins.play;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.db.DB;
import plugins.play.dao.UpdateLogDao;
import utils.Version;

/**
 * This plugin checks on startup whether the Coyo server version to be run is newer or older than the version of the
 * current db schema. Therefore an update log is created in database. Every time a new version is run, an entry in the
 * database is created. To check whether the Coyo version and the database schema version are compatible, the latest
 * entry is retrieved and the version are compared.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class CheckDbSchemaPlugin extends PlayPlugin {

	@Override
	public void onApplicationStart() {
		if (Play.mode.isDev() && !Play.id.endsWith("-db") && !Play.id.endsWith("test")) {
			return;
		}
		Logger.debug("[CheckDbSchemaPlugin] Checking DB Schema");

		/*
		 * Get product and schema version from configuration. If none can be read, stop startup process with error.
		 */
		Version serverProductVersion = new Version(Play.configuration.getProperty("application.version", ""));
		Version serverSchemaVersion = Version.getSchemaVersionFromProductVersion(serverProductVersion);
		if (serverProductVersion == null || !serverProductVersion.isValid() || serverSchemaVersion == null
				|| !serverSchemaVersion.isValid()) {
			throw new RuntimeException(
					"[CheckDbSchemaPlugin] Fatal: Could not read product version from configuration. Please set parameter 'application.version' to a valid version number.");
		}
		Logger.info("[CheckDbSchemaPlugin] Coyo is running with version '%s'.", serverProductVersion);

		Connection connection = DB.getConnection();
		try {
			UpdateLogDao updateLog = new UpdateLogDao(connection);

			/*
			 * Check whether table exists. If not create table and add first entry.
			 */
			if (!updateLog.updateTableExists()) {
				updateLog.createUpdateTable();
				updateLog.addNewVersion(serverProductVersion, serverSchemaVersion);
			}

			/*
			 * Get installed product and schema version from database. Stop startup process with runtime exception if
			 * the versions can't be read.
			 */
			Map<String, Version> dbVersion = updateLog.getLatestVersion();
			Version dbProductVersion = dbVersion.get(updateLog.SQL_COLUMN_VERSION);
			Version dbSchemaVersion = dbVersion.get(updateLog.SQL_COLUMN_SCHEMA);
			if (dbSchemaVersion != null && dbSchemaVersion.isValid()) {
				Logger.info("[CheckDbSchemaPlugin] Database is running with schema version '%s'.", dbSchemaVersion);
			} else {
				throw new RuntimeException(
						"[CheckDbSchemaPlugin] Fatal: Schema version of database could not be retrieved.");
			}

			/*
			 * If the current (server) version is older than the db schema version, throw a runtime exception and stop
			 * startup.
			 */
			if (serverSchemaVersion.compareTo(dbSchemaVersion) < 0) {
				throw new RuntimeException(
						"[CheckDbSchemaPlugin] Fatal: Schema version of database is newer than Coyo version of server. Stopping server to prevent database from irreversible inconsistency.");
			}

			/*
			 * If the current server version is newer or older than installed version, insert a new entry to database.
			 * The case that the server version is older than the installed version can only happen for micro versions.
			 * Otherwise the schema would be different and the startup process has already been stopped.
			 */
			if (dbProductVersion != null && dbProductVersion.isValid()) {
				if (serverProductVersion.compareTo(dbProductVersion) != 0) {
					updateLog.addNewVersion(serverProductVersion, serverSchemaVersion);
					Logger.debug("[CheckDbSchemaPlugin] Updated version history with version '%s'",
							serverProductVersion);
				}
			} else {
				Logger.warn("[CheckDbSchemaPlugin] Could not get installed product version from database. Skipping entry in update log.");
			}
		} catch (SQLException e) {
			Logger.error(e, "[CheckDbSchemaPlugin] Error connection to database.");
		}
	}

}
package plugins.play;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import models.AbstractTenant;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import assets.AssetsManager;
import binding.ISO8601DateTimeBinder;
import controllers.Auth;
import controllers.api.UpdateService;
import controllers.external.annotations.ShowExternals;
import models.Application;
import models.Sender;
import models.User;
import multitenancy.MTA;
import onlinestatus.UserOnlineStatus;
import play.Play;
import play.PlayPlugin;
import play.data.binding.Binder;
import play.db.jpa.JPA;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.Forbidden;
import play.mvc.results.Redirect;
import play.mvc.results.RenderBinary;
import play.mvc.results.Result;
import play.vfs.VirtualFile;
import plugins.PluginManager;
import session.UserLoader;
import theme.Theme;
import utils.ErrorHelper;
import utils.LanguageUtils;
import utils.MobileUtils;

/**
 * Here the core can hook into play if needed.
 *
 * @author Jan Marquardt
 */
public class CorePlugin extends PlayPlugin {

	private static Logger LOG = LoggerFactory.getLogger(CorePlugin.class);

	private static Map<Long, Long> tenantUserCounts = new ConcurrentHashMap<>();

	@Override
	public void onConfigurationRead() {
		// print coyo logo
		LOG.info("----------------------------------------");
		LOG.info("----------------------------------------");
		LOG.info("  ______ _____  _     _  _____  ");
		LOG.info(" / _____) ___ \\| |   | |/ ___ \\");
		LOG.info("| /    | |   | | |___| | |   | |");
		LOG.info("| |    | |   | |\\_____/| |   | |");
		LOG.info("| \\____| |___| | \\___/ | |___| |");
		LOG.info(" \\______)_____/  (___)  \\_____/ (R)");
		LOG.info("----------------------------------------");
		LOG.info("----------------------------------------");

		// check timezone is UTC
		if (!TimeZone.getDefault().equals(TimeZone.getTimeZone("Etc/UTC"))) {
			if (Play.mode.isProd()) {
				String message = "application timezone is not UTC. try launching with \"-Duser.timezone=Etc/UTC\"";
				LOG.error(message);
				throw new RuntimeException(message);
			} else {
				TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
				LOG.warn("setting timezone to UTC manually because we are in dev mode");
			}
		}

		// check assets are activated
		if (Play.mode.isProd() && !AssetsManager.ACTIVE) {
			String message = "cannot start in PROD mode without assets being activated";
			LOG.error(message);
			throw new RuntimeException(message);
		}

		// check that user online timeout is not less than long polling
		// threshold
		if (UserOnlineStatus.ONLINE_TIMEOUT_MS < UpdateService.getTimeoutAsMillis()) {
			String message = "Configured value for 'coyo.session.lifetime' can't be less than 'coyo.updateservice.await.timeout' - please change the configuration";
			LOG.error(message);
			throw new RuntimeException(message);
		}
	}

	@Override
	public void onApplicationStart() {
		// add core public to views path
		Play.templatesPath.add(Play.modules.get("coyo.core").child("public"));

		// register type binders
		Binder.register(DateTime.class, new ISO8601DateTimeBinder());
	}

	public static void checkMobile() {
		/*
		 * if the user already has a theme setting in the cookie we skip further
		 * checks. Comment " && !Play.mode.isDev()" out when testing the
		 * desktop-mobile switch.
		 */
		if (Request.current().cookies.get(Theme.THEME_COOKIE_KEY) != null && !Play.mode.isDev()) {
			return;
		}

		if (!Request.current().headers.containsKey("user-agent")) {
			return;
		}
		if (MobileUtils.isMobileDevice(Request.current().headers.get("user-agent").value())) {
			Theme.setActiveTheme("mobile");
		} else {
			Theme.setActiveTheme("web");
		}
	}

	public static void setLanguage(User user) {
		if (user != null) {
			LanguageUtils.set(user.language.toString());
		}
	}

	@Override
	public boolean serveStatic(VirtualFile file, Request request, Response response) {
		// update theme
		checkMobile();

		return false;
	}

	@Override
	public void beforeActionInvocation(Method actionMethod) {
		if (JPA.isInsideTransaction()) {

			final AbstractTenant tenant = MTA.getActiveTenant();

			// no tenant present for tenant activation
			if (tenant != null && !Play.runingInTestMode() && MTA.isMultitenancyActive()) {
				Long userCount = tenantUserCounts.get(tenant.getId());
				if (userCount == null || userCount < 1) {
					userCount = User.count();
					tenantUserCounts.put(tenant.getId(), userCount);
				}
				// check for zero users -> install
				if ((userCount == null || userCount < 1)
						&& !Request.current().controller.equals("Installation")
						&& !Request.current().controller.equals("open.Application")
						&& !Request.current().controller.equals("Assets")) {
					throw new Redirect("/installation");
				}
			}

			// activate filters
			((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.SOFT_DELETE);
			((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.EXTERNAL);

			// maybe allow external users
			if (Request.current().controllerClass.isAnnotationPresent(ShowExternals.class)) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			} else if (actionMethod.isAnnotationPresent(ShowExternals.class)) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			} else if (Request.current().controllerClass == Auth.class) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			}

			/*
			 * Load user and check tenant.
			 * 
			 * TODO This should be removed in the future, because: a) the user
			 * is set to the request in SecureBaseController anyways b) the
			 * tenant is checked in TenantModel.checkTenant anyways
			 */
			try {
				final User user = UserLoader.getConnectedUser(false, true);
				setLanguage(user);
			} catch (Exception e) {
				ErrorHelper.handleWarning(LOG, e, "[CorePlugin] Could not load user with id [{}]. Forcing logout.");

				if (Request.current().isAjax()) {
					throw new Forbidden("");
				} else {
					// handle check fail
					Auth.executeLogout();
					throw new Redirect("/");
				}
			}
		}

		// update theme
		checkMobile();
	}

	@Override
	public void onActionInvocationResult(Result result) {
		/*
		 * Play URL redirects that are absolute (e.g. "/login") are broken if
		 * the application runs with a context. This snippet fixes wrong
		 * redirects by appending the context path to the redirect URL.
		 */
		if (!StringUtils.isEmpty(Play.ctxPath) && result instanceof Redirect) {
			Redirect redirect = (Redirect) result;

			String ctx = "/" + Play.ctxPath;
			ctx = ctx.replaceAll("//", "/");

			// if absolute URL and context not already included
			if (redirect.url.startsWith("/") && !redirect.url.startsWith(ctx)) {
				// append context
				String url = ctx + redirect.url;

				// avoid //
				url = url.replaceAll("//", "/");

				// inject
				redirect.url = url;
			}
		}

		/*
		 * The Internet Explorer 8 or later breaks HTTPS downloads if a no-cache
		 * header is sent. In order to fix this we check for downloads and set a
		 * new cache header.
		 */
		if (Request.current().secure && result instanceof RenderBinary) {
			String cacheControl = Response.current().getHeader("Cache-Control");
			if (cacheControl != null && cacheControl.indexOf("no-cache") >= 0) {
				Response.current().setHeader("Cache-Control", "private, max-age=15");
			}
		}

		if (!StringUtils.isEmpty(Application.X_FRAME_OPTIONS)) {
			Response.current().setHeader("X-Frame-Options", Application.X_FRAME_OPTIONS);
		}
	}

	@Override
	public void invocationFinally() {
		/*
		 * make sure not to set cache headers if we are sending cookies. See
		 * https://code.google.com/p/doctype-mirror/wiki/ArticleHttpCaching for
		 * details
		 */
		if (Response.current() != null && !Response.current().cookies.isEmpty()) {
			Response.current().setHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT"); // HTTP
			// 1.0
			Response.current().setHeader("Pragma", "no-cache"); // HTTP 1.0
			Response.current().setHeader("Cache-Control", "private, max-age=0, no-cache, no-store, must-revalidate"); // HTTP
			// 1.1
		}

		super.invocationFinally();
	}

	@Override
	public void afterInvocation() {
		PluginManager.clearThreadLocals();
		UserLoader.clearThreadLocalUser();
		UserLoader.clearUserCache();
	}
}

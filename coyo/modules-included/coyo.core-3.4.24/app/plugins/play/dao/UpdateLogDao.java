package plugins.play.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import play.Logger;
import utils.Version;

/**
 * Gives an easy access to the update_log database table without the use of the EntityManager.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 */
// TODO: Wrong package? I expected a Play Plugin here
public class UpdateLogDao {
	
	public static final String SQL_COLUMN_SCHEMA = "schema_version";

	public static final String SQL_COLUMN_VERSION = "product_version";

	private static final String SQL_UPDATE_TABLE_NAME = "db_updatelog";

	private static final String SQL_SELECT_VERSION = "SELECT " + SQL_COLUMN_VERSION + ", " + SQL_COLUMN_SCHEMA + " FROM " + SQL_UPDATE_TABLE_NAME + " ORDER BY timestamp DESC LIMIT 1";
	
	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + SQL_UPDATE_TABLE_NAME + " ("
			+ "id bigint(20) NOT NULL AUTO_INCREMENT, "
			+ "timestamp TIMESTAMP, "
			+ SQL_COLUMN_VERSION + " varchar(20), "
			+ SQL_COLUMN_SCHEMA + " varchar(20), "
			+ "PRIMARY KEY (id)"
			+ ");";

	private static final String SQL_INSERT_VERSION = "INSERT INTO " + SQL_UPDATE_TABLE_NAME 
			+ " (" + SQL_COLUMN_VERSION + ", " + SQL_COLUMN_SCHEMA + ") "
			+ "VALUES (?, ?)";
	
	private Connection connection;

	public UpdateLogDao(Connection connection) throws SQLException {
		this.connection = connection;
	}
	
	/**
	 * Retrieves the latest version from the 'update_log' table. If no entry could be found an empty Map is returned. A
	 * version data set exists of a Coyo version and a fitting schema version.
	 */
	public Map<String, Version> getLatestVersion() throws SQLException {
		CallableStatement statement = null;
		try {
			Map<String, Version> result = new HashMap<String, Version>(2);
			statement = connection.prepareCall(SQL_SELECT_VERSION);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				result.put(SQL_COLUMN_VERSION, new Version(resultSet.getString(SQL_COLUMN_VERSION)));
				result.put(SQL_COLUMN_SCHEMA, new Version(resultSet.getString(SQL_COLUMN_SCHEMA)));
			}
			return result;
		} finally {
			closeStatement(statement);
		}
	}
	
	/**
	 * Adds a new version to database. A version data set exists of a Coyo version and a fitting schema version.
	 */
	public int addNewVersion(Version serverCoyoVersion, Version serverSchemaVersion) throws SQLException {
		CallableStatement statement = null;
		try {
			statement = connection.prepareCall(SQL_INSERT_VERSION);
			statement.setString(1, serverCoyoVersion.toString());
			statement.setString(2, serverSchemaVersion.toString());
			return statement.executeUpdate();
		} finally {
			closeStatement(statement);
		}
	}

	/**
	 * Checks whether the 'update_log' table already exists.
	 */
	public boolean updateTableExists() throws SQLException {
		ResultSet tables = null;
		tables = connection.getMetaData().getTables(null, null, SQL_UPDATE_TABLE_NAME, null);
		while (tables.next()) {
			return tables.getString("TABLE_NAME").equals(SQL_UPDATE_TABLE_NAME);
		}
		return false;
	}

	/**
	 * Creates the 'update_log' table if it does not exist.
	 */
	public void createUpdateTable() throws SQLException {
		Logger.debug("[UpdateLog] Table '%s' does not exist. Try to creating it...", SQL_UPDATE_TABLE_NAME);
		CallableStatement statement = null;
		try {
			statement = connection.prepareCall(SQL_CREATE_TABLE);
			statement.executeUpdate();
			Logger.debug("[UpdateLog] Successfully created table '%s'.", SQL_UPDATE_TABLE_NAME);
		} finally {
			closeStatement(statement);
		}
	}

	/**
	 * Helper method to close a statement silently. This method is null safe.
	 */
	private void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				// do nothing
			}
		}
	}

}
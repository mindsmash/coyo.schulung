package plugins.play;

import java.util.List;

import models.Tenant;
import multitenancy.MTA;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.db.jpa.JPAPlugin;
import play.exceptions.UnexpectedException;
import upgrade.UpgradeScript;

/**
 * Works in combination with {@link DbEvolutionsPlugin}. Calls {@link UpgradeScript} after Hibernate has done its stuff
 * and the JPA context was initialized.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class UpgradePlugin extends PlayPlugin {

	@Override
	public void afterApplicationStart() {
		if (Play.mode.isDev() && !(Play.id.equals("local-db"))) {
			return;
		}

		// if update ran
		if (DbEvolutionsPlugin.updated) {
			try {
				UpgradeScript us = loadUpdateScript();
				us.postHibernate();

				try {
					JPAPlugin.startTx(false);

					List<Tenant> tenants = Tenant.findAll();
					for (Tenant tenant : tenants) {
						try {
							MTA.activateMultitenancy(tenant);
							us.upgradeTenant(tenant);
						} catch (Exception e) {
							Logger.error(e, "Error running upgrade script for tenant [%s]", tenant);
							throw e;
						}
					}

					JPAPlugin.closeTx(false);
				} catch (Exception e) {
					JPAPlugin.closeTx(true);
					throw e;
				}

				us.finish();
			} catch (Exception e) {
				throw new UnexpectedException(e);
			}
		}
	}

	public static UpgradeScript loadUpdateScript() {
		return new UpgradeScript();
	}
}

package plugins.play;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import play.PlayPlugin;
import play.exceptions.UnexpectedException;
import play.mvc.Scope.Session;

/**
 * Since we are using the 'application.sendOnlyWhenChanged' parameter, we need to make sure to deliver the session
 * cookie when the authenticityToken changed. This is almost always the case when the token was not available at request
 * start.
 * 
 * The fact that Play! is not taking this into consideration is a bug in versions prior to 1.3. This plugin can be
 * removed as soon as we are using playframework 1.3. See:
 * http://play.lighthouseapp.com/projects/57987/tickets/1688-sendonlyifchangedtrue-breaks-authenticitytoken-mechanism
 *
 * @author Jan Marquardt, mindsmash GmbH
 */
public class FixSessionPlugin extends PlayPlugin {

	@Override
	public void beforeActionInvocation(Method actionMethod) {
		if (!Session.current().all().containsKey("___AT")) {
			try {
				Session session = Session.current();
				Field field = session.getClass().getDeclaredField("changed");
				field.setAccessible(true);
				field.set(session, true);
			} catch (Exception e) {
				throw new UnexpectedException(e);
			}
		}
	}

}
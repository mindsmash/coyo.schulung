package plugins.play;

import events.Event;
import events.EventService;
import play.Logger;
import play.PlayPlugin;
import play.libs.F;
import play.libs.F.Tuple;
import play.mvc.Http.Request;

import java.util.ArrayList;
import java.util.List;

/**
 * Publishes pending events via the {@link EventService} after action
 * invocation.
 *
 * @author Jan Tammen, mindsmash GmbH
 */
public class EventServicePlugin extends PlayPlugin {

	/**
	 * Use a specialized version of {@link ThreadLocal}, which returns an empty
	 * {@link ArrayList} if no value is set.
	 */
	private static class EventThreadLocal extends ThreadLocal<List<F.Tuple<Long, Event>>> {
		@Override
		protected List<Tuple<Long, Event>> initialValue() {
			return new ArrayList<F.Tuple<Long, Event>>();
		}
	}

	private final static ThreadLocal<List<F.Tuple<Long, Event>>> eventsToPublishAfterAction = new EventThreadLocal();

	public static void addEventToPublish(Long userId, Event event) {
		eventsToPublishAfterAction.get().add(new F.Tuple<Long, Event>(userId, event));
	}

	@Override
	public void afterInvocation() {
		// handle events which have been postponed to be published after any invocation (action, job ...)
		// invocation
		List<F.Tuple<Long, Event>> events = eventsToPublishAfterAction.get();
		if (events != null && !events.isEmpty()) {
			Logger.debug("[plugins.play.EventServicePlugin] afterInvocation() - Publishing %d outstanding events",
					events.size());
			for (F.Tuple<Long, Event> event : events) {
				EventService.publishEvent(event._2, event._1);
			}
		}
		eventsToPublishAfterAction.set(new ArrayList<F.Tuple<Long, Event>>());
	}
}

package plugins;

import java.util.List;

import models.Settings;
import models.app.inno.Idea;
import models.app.inno.IdeaDAO;
import play.Play;
import play.i18n.Messages;
import play.mvc.Http.Request;
import session.UserLoader;

/**
 * Shows the tag cloud.
 *
 * @author Gerrit Dongus
 */
@LoadPlugin(defaultActivated = false)
public class InnoAppPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "innoApp";
	}

	@Override
	public String getName() {
		return Messages.get("plugin.inno.title");
	}

	@Override
	public boolean isLicenseRequired() {
		return Boolean.valueOf(Play.configuration.getProperty("coyo.innovationApp.licenseRequired", "true"));
	}

	@Override
	protected String getLicenseProductKey() {
		return "coyo-innovationApp";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void rightbar() {
		render("rightbar.html");
	}

	@PluginHook(value = PluginHooks.TOPBAR_LEFT)
	public void topbarRight() {
		final List<Idea> ideas = Idea.find("creator = ?", UserLoader.getConnectedUser()).fetch();
		if (ideas != null && !ideas.isEmpty()) {
			render("topbar.html");
		}
	}
}

package plugins;

import java.util.ArrayList;
import java.util.List;

import models.User;
import models.app.PollApp;
import session.UserLoader;
import acl.Permission;
import controllers.Security;

@LoadPlugin
public class PollPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "poll";
	}

	@Override
	public String getName() {
		return "Poll Plugin";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void rightbar() {
		User user = UserLoader.getConnectedUser();
		renderArgs.put("cacheKey", getKey() + "-"
				+ user.language.toString() + "-"
				+ user.id);
		renderArgs.put("pollsMax", 7);
		render("rightbar.html");
	}

	public static List<PollApp> getPolls() {
		List<PollApp> polls = new ArrayList<PollApp>();
		List<PollApp> all = PollApp
				.find("SELECT a FROM PollApp a WHERE a.active = true AND ? MEMBER OF a.sender.followers ORDER BY a.frozen, a.created DESC",
						UserLoader.getConnectedUser()).fetch();
		for (PollApp poll : all) {
			// max
			if (polls.size() >= 30) {
				break;
			}

			// check permissions
			if (poll.sender.isActive() && Security.check(Permission.ACCESS_SENDER, poll.sender.id)) {
				polls.add(poll);
			}
		}

		return polls;
	}
}

package plugins;

import java.util.ArrayList;
import java.util.List;

import models.User;
import models.app.WikiAppArticle;
import play.db.helper.JpqlSelect;
import session.UserLoader;
import acl.Permission;
import controllers.Security;

@LoadPlugin
public class WikiPlugin extends PluginBase {

	public static final int DISPLAY_LIMIT = 5;

	@Override
	public String getKey() {
		return "wiki";
	}

	@Override
	public String getName() {
		return "Wiki Plugin";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void rightbar() {
		User user = UserLoader.getConnectedUser();
		renderArgs.put("cacheKey", getKey() + "-"
				+ user.language.toString() + "-"
				+ user.id);
		render("rightbar.html");
	}

	public static List<WikiAppArticle> getArticles() {
		List<WikiAppArticle> articles = new ArrayList<WikiAppArticle>();

		/*
		 * query for all articles that belong to an active app, are part of a sender the user follows and which was
		 * published in the past. Note that we are fetching maximum 5 times more blog articles than we actually need.
		 * This is done since some articles could be dropped due to the security check.
		 */
		JpqlSelect select = new JpqlSelect();
		select.select("article FROM WikiAppArticle article LEFT OUTER JOIN article.app.sender sender");
		select.where("article.app.active = true");
		select.andWhere("? MEMBER OF sender.followers").param(UserLoader.getConnectedUser());
		select.orderBy("article.modified DESC");
		List<WikiAppArticle> all = WikiAppArticle.find(select.toString(), select.getParams().toArray()).fetch(
				DISPLAY_LIMIT * 3);

		for (WikiAppArticle article : all) {
			// max
			if (articles.size() >= DISPLAY_LIMIT) {
				break;
			}

			// check permissions
			if (article.app.isActive() && article.app.sender.isActive()
					&& Security.check(Permission.ACCESS_SENDER, article.app.sender.id)) {
				articles.add(article);
			}
		}

		return articles;
	}
}

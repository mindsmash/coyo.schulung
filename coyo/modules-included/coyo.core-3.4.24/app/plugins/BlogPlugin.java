package plugins;

import java.util.ArrayList;
import java.util.List;

import models.app.BlogAppArticle;

import org.joda.time.DateTime;

import play.db.helper.JpqlSelect;
import session.UserLoader;
import acl.Permission;
import controllers.Security;

@LoadPlugin
public class BlogPlugin extends PluginBase {

	public static final int DISPLAY_LIMIT = 5;

	@Override
	public String getKey() {
		return "blog";
	}

	@Override
	public String getName() {
		return "Blog Plugin";
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void rightbar() {
		renderArgs.put("cacheKey", getKey() + "-" + UserLoader.getConnectedUser().id);
		render("rightbar.html");
	}

	public static List<BlogAppArticle> getArticles() {
		List<BlogAppArticle> articles = new ArrayList<BlogAppArticle>();

		/*
		 * query for all articles that belong to an active app, are part of a sender the user follows and which was
		 * published in the past. Note that we are fetching maximum 5 times more blog articles than we actually need.
		 * This is done since some articles could be dropped due to the security check.
		 */
		JpqlSelect select = new JpqlSelect();
		select.select("article FROM BlogAppArticle article LEFT OUTER JOIN article.app.sender sender");
		select.where("article.app.active = true");
		select.andWhere("? MEMBER OF sender.followers").param(UserLoader.getConnectedUser());
		select.andWhere("article.publishDate <= ?").param(new DateTime());
		select.orderBy("article.publishDate DESC");
		List<BlogAppArticle> all = BlogAppArticle.find(select.toString(), select.getParams().toArray()).fetch(
				DISPLAY_LIMIT * 5);

		for (BlogAppArticle article : all) {
			// max
			if (articles.size() >= DISPLAY_LIMIT) {
				break;
			}

			// check permissions
			if (article.isPublished() && article.app.sender.isActive()
					&& Security.check(Permission.ACCESS_SENDER, article.app.sender.id)) {
				articles.add(article);
			}
		}

		return articles;
	}
}

package plugins;

import models.Settings;
import play.i18n.Messages;
import play.mvc.Http.Request;
import plugins.LoadPlugin;
import plugins.PluginBase;
import plugins.PluginHook;
import plugins.PluginHooks;

/**
 * Shows the tag cloud.
 *
 * @author mindsmash GmbH
 */
@LoadPlugin
public class LatestFilesPlugin extends PluginBase {

	@Override
	public String getKey() {
		return "latestfiles";
	}

	@Override
	public String getName() {
		return Messages.get("plugin.latestfiles");
	}

	@Override
	public boolean hasSettings() {
		return false;
	}

	@Override
	public void renderSettings() {
		renderArgs.put("isPublic", getSettings().getBoolean("public"));
		render("settings.html");
	}

	@Override
	public void saveSettings(Request request) {
		Settings settings = getSettings();
		settings.setProperty("public", request.params.get("public"));
		settings.save();
	}

	@PluginHook(value = PluginHooks.BEFORE_RIGHTBAR, actions = "ActivityStream.index")
	public void activityStreamRightbar() {
		render("rightbar.html");
	}
}

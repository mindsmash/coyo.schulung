package plugins;

/**
 * List of available events.
 * 
 * @author mindsmash GmbH
 */
public enum PluginEvents {
	USER_SAVE, PAGE_SAVE, WORKSPACE_SAVE, EVENT_SAVE, BEFORE_LOGIN, AFTER_LOGIN, BEFORE_LOGOUT, AFTER_LOGOUT;
}

package plugins;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import models.Settings;
import multitenancy.MTA;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.classloading.ApplicationClasses.ApplicationClass;
import utils.RequestCache;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 * 
 * The AppManager holds all {@link PluginBase}s to access all Plugin Informations.
 * 
 * @author mindsmash GmbH
 * 
 */
public class PluginManager {

	public static Map<String, PluginBase> plugins;

	private static ThreadLocal<List<PluginBase>> activePlugins = new ThreadLocal<>();

	private PluginManager() {
		// hide
	}

	private static Map<String, PluginBase> getPluginsMap() {
		if (plugins == null) {
			Logger.info("[PluginManager] Loading plugins");

			plugins = new LinkedHashMap<String, PluginBase>();

			for (ApplicationClass ac : Play.classes.getAnnotatedClasses(LoadPlugin.class)) {
				try {
					PluginBase plugin = (PluginBase) ac.javaClass.newInstance();
					Logger.debug("[PluginManager] Registering plugin [%s]", plugin.getKey());
					plugins.put(plugin.getKey(), plugin);
				} catch (Exception e) {
					Logger.error(e, "[PluginManager] Error loading plugin [%s], skipping.", ac);
				}
			}

			resetActivePluginList();
		}
		return plugins;
	}

	public static List<PluginBase> getPlugins() {
		List<PluginBase> plugins = new ArrayList<PluginBase>();
		plugins.addAll(getPluginsMap().values());
		Collections.sort(plugins);
		return plugins;
	}

	private static void resetActivePluginList() {
		Logger.debug("[PluginManager] Reseting list of active plugins");
		Cache.delete("PluginManager.sortedActivePlugins-" + MTA.getActiveTenant().id);
	}

	/**
	 * Loads a set of the keys of all active plugins for the current tenant. The keys are cached until plugins change (
	 * {@link #resetActivePluginList()}). The keys are sorted.
	 */
	private static synchronized List<String> loadActivePluginKeyList() {
		Monitor monitor = MonitorFactory.start("PluginManager.loadActivePluginKeyList");
		List<String> pluginKeys = Cache
				.get("PluginManager.sortedActivePlugins-" + MTA.getActiveTenant().id, List.class);

		if (pluginKeys == null) {
			List<PluginBase> plugins = new ArrayList<>();
			for (PluginBase plugin : getPlugins()) {
				if (isActive(plugin)) {
					plugins.add(plugin);
				}
			}
			Collections.sort(plugins);

			pluginKeys = new ArrayList<>();
			for (PluginBase plugin : plugins) {
				pluginKeys.add(plugin.getKey());
			}

			Cache.set("PluginManager.sortedActivePlugins-" + MTA.getActiveTenant().id, pluginKeys);
		}

		monitor.stop();
		return pluginKeys;
	}

	public static List<PluginBase> getActivePlugins() {
		if (activePlugins.get() == null) {
			Monitor monitor = MonitorFactory.start("PluginManager.getActivePlugins");
			List<PluginBase> plugins = new ArrayList<>();
			for (String key : loadActivePluginKeyList()) {
				PluginBase plugin = getPlugin(key);
				if (plugin != null) {
					plugins.add(plugin);
				}
			}

			activePlugins.set(plugins);
			monitor.stop();
		}

		return activePlugins.get();
	}

	public static PluginBase getPlugin(String key) {
		return getPluginsMap().get(key);
	}

	public static Settings getPluginSettings(String key) {
		return getPluginSettings(key, false);
	}

	public static Settings getPluginSettings(String key, boolean refresh) {
		Settings settings = (Settings) RequestCache.get("plugin-" + key + "-settings");

		if (settings == null || refresh) {
			settings = Settings.findOrCreate("plugin-" + key);
			RequestCache.put("plugin-" + key + "-settings", settings);
		}

		return settings;
	}

	public static boolean isActive(PluginBase plugin) {
		if (plugin == null) {
			return false;
		}

		// check license
		try {
			if (plugin.isLicenseRequired() && !plugin.isLicenseValid()) {
				Logger.debug("[PluginManager] Plugin %s is inactive because no valid license was found", plugin);
				return false;
			}
		} catch (Exception e) {
			Logger.debug(e, "[PluginManager] Error checking license for plugin: %s", plugin);
			Logger.warn("[PluginManager] Error checking license for plugin: %s", plugin);
		}

		Settings settings = getPluginSettings(plugin.getKey());
		return settings.getBoolean("active");
	}

	public static void activate(PluginBase plugin) {
		if (!getPluginsMap().containsKey(plugin.getKey())) {
			throw new IllegalArgumentException("plugin with key [" + plugin.getKey() + "] is not registered");
		}

		Settings settings = getPluginSettings(plugin.getKey(), true);
		settings.setProperty("active", "true");
		settings.save();
		resetActivePluginList();
	}

	public static void deactivate(PluginBase plugin) {
		if (!getPluginsMap().containsKey(plugin.getKey())) {
			throw new IllegalArgumentException("plugin with key [" + plugin.getKey() + "] is not registered");
		}

		Settings settings = getPluginSettings(plugin.getKey(), true);
		settings.setProperty("active", "false");
		settings.save();
		resetActivePluginList();
	}

	public static void sort(List<String> sortedPluginKeys) {
		int i = 1;
		for (String key : sortedPluginKeys) {
			PluginBase plugin = PluginManager.getPlugin(key);
			if (plugin != null) {
				plugin.setRank(i++);
			}
		}
		resetActivePluginList();
	}

	public static void raiseEvent(PluginEvents event, Object... args) {
		// iterate plugins
		for (PluginBase plugin : getActivePlugins()) {
			// iterate event methods
			for (Method method : plugin.getEvents()) {
				// check for correct event
				if (method.getAnnotation(PluginEvent.class).value() == event) {
					try {
						method.invoke(plugin, args);
					} catch (Exception e) {
						Logger.warn(e, "[PluginManager] Error executing event [%s] of plugin [%s] with args [%s]",
								event, plugin, args);
					}
				}
			}
		}
	}

	public static void clearThreadLocals() {
		if (activePlugins.get() != null) {
			for (PluginBase plugin : activePlugins.get()) {
				plugin.clearThreadLocals();
			}
			activePlugins.remove();
		}
	}
}

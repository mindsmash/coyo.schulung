package plugins;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import license.Licensable;
import models.Settings;
import play.data.validation.Validation;
import play.mvc.Http.Request;
import play.mvc.Scope.RenderArgs;
import play.utils.Java;
import plugins.results.TemplatePluginResult;

/**
 * <p>
 * Plugins are a way to extend and modify the default behavior of the main and core application. Each plugin can
 * implement custom public methods to hook into specific calling points during template and service processing.
 * </p>
 * 
 * <p>
 * Each hook method must be public and annotated with the {@link PluginHook} annotation. The list of available hooks can
 * be found here {@link PluginHooks} . If you need additional hooks please do not hesitate to contact the development
 * team. We are happy to work with you and add new hooks to the application.
 * </p>
 * 
 * <p>
 * To activate your plugin it needs to have the {@link LoadPlugin} annotation assigned.
 * </p>
 * 
 * @author mindsmash GmbH
 * 
 */
public abstract class PluginBase extends Licensable implements Comparable<PluginBase> {

	private Map<PluginHooks, List<Method>> hooks;
	private List<Method> events;

	/**
	 * The unique key of your plugin. If a plugin with that key already exists your plugin will be denied.
	 * 
	 * @return The unique key of your plugin, e.g. "myplugin"
	 */
	public abstract String getKey();

	/**
	 * @return Display name of your plugin
	 */
	public abstract String getName();

	@Override
	protected String getLicenseProductKey() {
		return "coyo-plugin-" + getKey();
	}

	public boolean hasSettings() {
		return false;
	}

	/**
	 * Each plugin can render it's own configuration form. This method will process the submission of such form. You can
	 * use the {@link #getSettings()} method to get hold of your plugin's settings object and persist custom properties
	 * there. You can work with the {@link Validation} object to validate and reject the form submission.
	 */
	public void saveSettings(Request request) {
	}

	/**
	 * Use this method to render your plugin's form (optional) with the {@link #render(String, Map)} method.
	 */
	public void renderSettings() {
	}

	protected RenderArgs renderArgs = new RenderArgs();

	/**
	 * Renders a template with arguments. Your groovy templates must be in the play views path (see
	 * http://www.playframework.org for more details). We will try to find your templates under
	 * {VIEWPATH}/plugins/{PLUGIN-KEY}/{TEMPLATE-NAME}. So calling "render('out.html');" from a plugin with key
	 * "fooplugin" will result in a lookup of "app/views/plugins/fooplugin/out.html".
	 * 
	 * @param template
	 *            The filename of your template
	 * @param args
	 *            Arguments to pass to the template
	 */
	protected void render(String template, Map<String, Object> args) {
		TemplatePluginResult r = new TemplatePluginResult();
		r.template = "plugins/" + getKey() + "/" + template;
		if (args != null) {
			r.args.putAll(args);
		}
		r.args.putAll(renderArgs.data);
		throw r;
	}

	/**
	 * Renders a template without arguments. See {@link #render(String, Map)} for details.
	 * 
	 * @param template
	 *            The filename of your template
	 */
	protected void render(String template) {
		render(template, null);
	}

	/**
	 * Loads the settings of this plugin. Each plugin has its own settings instance that can be used to load and persist
	 * custom settings. Please use {@link Settings#save()} to persist your changes.
	 * 
	 * @return Plugins's settings.
	 */
	protected Settings getSettings() {
		return PluginManager.getPluginSettings(getKey());
	}

	/**
	 * This method is used for quick access to this plugin's hook methods. If methods are loaded via reflection caching
	 * is very important.
	 * 
	 * @return All hook methods of this plugin. Hook methods MUST be annotated with the {@link PluginHook} annotation.
	 */
	public final Map<PluginHooks, List<Method>> getHooks() {
		if (hooks == null) {
			hooks = new HashMap<PluginHooks, List<Method>>();
			for (Method method : Java.findAllAnnotatedMethods(getClass(), PluginHook.class)) {
				PluginHooks key = method.getAnnotation(PluginHook.class).value();
				if (!hooks.containsKey(key)) {
					hooks.put(key, new ArrayList<Method>());
				}
				hooks.get(key).add(method);
			}
		}
		return hooks;
	}

	/**
	 * This method is used for quick access to this plugin's hook methods. If methods are loaded via reflection caching
	 * is very important.
	 * 
	 * @return All methods that matches the given hook.
	 */
	public final List<Method> getHookMethods(PluginHooks hook) {
		List<Method> methods = getHooks().get(hook);
		if (methods == null) {
			return new ArrayList<Method>();
		}
		return methods;
	}

	/**
	 * This method is used for quick access to this plugin's event method. If methods are loaded via reflection caching
	 * is very important.
	 * 
	 * @return All event methods of this plugin. Event methods MUST be annotated with the {@link PluginEvent}
	 *         annotation.
	 */
	public final List<Method> getEvents() {
		if (events == null) {
			events = Java.findAllAnnotatedMethods(getClass(), PluginEvent.class);
		}
		return events;
	}

	protected int getRank() {
		int rank = 9999;
		try {
			rank = getSettings().getInteger("rank");
		} catch (NumberFormatException ignored) {
		}
		return rank;
	}

	/**
	 * Sets the rank for this plugin. Use {@link PluginManager#sort(List)} to sort plugins.
	 * 
	 * @param rank
	 */
	protected void setRank(int rank) {
		Settings settings = getSettings();
		settings.setProperty("rank", rank + "");
		settings.save();
	}

	public void clearThreadLocals() {
		renderArgs.current.remove();
	}

	@Override
	public int compareTo(PluginBase o) {
		if (o == null)
			return 1;

		int c = getRank() - o.getRank();
		if (c != 0)
			return c;

		return getKey().compareTo(o.getKey());
	}

	@Override
	public String toString() {
		return "Plugin[" + getKey() + ", name=" + getName() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PluginBase) {
			return getKey().equals(((PluginBase) obj).getKey());
		}

		return super.equals(obj);
	}
}

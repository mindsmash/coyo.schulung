package plugins;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to annotate your plugin's hook methods. Hook methods must
 * be public.
 * 
 * @author mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PluginHook {

	/**
	 * The hook affected.
	 */
	PluginHooks value();

	/**
	 * Actions to limit the hook method to. You can use the * wildcard. E.g.
	 * "ActivityStream.*" or "Pages.index".
	 */
	String[] actions() default "*";
}

package plugins;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Each {@link PluginBase} must be annotated with this Annotation to get loaded when the Application is started. The
 * {@link PluginBase} is afterwards available in the {@link PluginManager}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LoadPlugin {

	/**
	 * Determines whether a plugin should be activated by default.
	 */
	boolean defaultActivated() default true;
}

package plugins.results;

import java.util.HashMap;
import java.util.Map;

public class TemplatePluginResult extends PluginResult {

	public String template;
	public Map<String, Object> args = new HashMap<String, Object>();
}

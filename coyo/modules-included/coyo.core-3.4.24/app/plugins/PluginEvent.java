package plugins;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to annotate your plugin's event methods. Event methods
 * must be public.
 * 
 * @author mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PluginEvent {

	/**
	 * The event affected.
	 */
	PluginEvents value();
}

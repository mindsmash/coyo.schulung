package data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jobs.TenantExportJob;
import models.AbstractTenant;
import models.Application;
import models.Sender;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.DefaultTableIterator;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.IRowValueProvider;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.NoSuchColumnException;
import org.dbunit.dataset.RowFilterTable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.datatype.DefaultDataTypeFactory;
import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.dataset.filter.IRowFilter;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.XmlDataSetWriter;
import org.dbunit.ext.db2.Db2DataTypeFactory;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.hibernate.EntityMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.DB2Dialect;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.Oracle8iDialect;
import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.persister.collection.BasicCollectionPersister;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.persister.entity.Joinable;
import play.Logger;
import play.Play;
import play.db.DB;
import play.db.jpa.JPA;
import storage.FlexibleBlob;
import util.Util;
import utils.dbunit.EnhancedMySqlDataTypeFactory;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author mindsmash GmbH
 */
public class TenantDataExporter {

	private static final String[] EXCLUDED_TABLES = { "activation", "tenant", "tenant_property", "db_*",
			"ht_*" };

	private static Map<String, Class> mainTables;
	private static Map<String, CollectionTable> collectionTables;

	public static final String EXPORT_METADATA_FOLDER = "metadata";
	public static final String EXPORT_BLOBDATA_FOLDER = "blobdata";
	public static final String EXPORT_INFO_FILE = "package.info";

	public static final String PKG_INFO_APP_VERSION = "applicationVersion";
	public static final String PKG_INFO_DB_VERSION = "dbSchemaVersion";
	public static final String PKG_INFO_CREATED = "created";

	/**
	 * Loads the correct datatype factory by looking at the hibernate dialect.
	 * 
	 * @return
	 */
	public static IDataTypeFactory loadDataTypeFactory() {
		try {
			Dialect dialect = ((SessionFactoryImplementor) getSessionFactory()).getDialect();
			if (dialect != null) {
				Class dialectClass = dialect.getClass();
				Logger.debug("[Export] Detected db dialect: %s", dialectClass);

				if (H2Dialect.class.isAssignableFrom(dialectClass)) {
					return new H2DataTypeFactory();
				} else if (MySQLDialect.class.isAssignableFrom(dialectClass)) {
					return new EnhancedMySqlDataTypeFactory();
				} else if (SQLServerDialect.class.isAssignableFrom(dialectClass)) {
					return new MsSqlDataTypeFactory();
				} else if (PostgreSQLDialect.class.isAssignableFrom(dialectClass)) {
					return new PostgresqlDataTypeFactory();
				} else if (DB2Dialect.class.isAssignableFrom(dialectClass)) {
					return new Db2DataTypeFactory();
				} else if (Oracle8iDialect.class.isAssignableFrom(dialectClass)) {
					return new OracleDataTypeFactory();
				}
			}
		} catch (Exception e) {
			Logger.warn(e, "[Export] Error auto-detecting dbunit datatype factory");
		}

		Logger.warn("[Export] Could not auto-detect a datatype factory. Using default datatype factory.");
		return new DefaultDataTypeFactory();
	}

	/**
	 * Loads a DBUnit database connection and applies the correct settings and dataType factory.
	 * 
	 * @return
	 * @throws DatabaseUnitException
	 * @throws SQLException
	 */
	public static IDatabaseConnection loadDatabaseConnection() throws DatabaseUnitException, SQLException {
		IDataTypeFactory dataTypeFactory = loadDataTypeFactory();
		Logger.debug("[Export] Using datatype factory: %s", dataTypeFactory);

		String schema = null;
		if (dataTypeFactory instanceof H2DataTypeFactory) {
			// h2 requires schema "PUBLIC"
			schema = "PUBLIC";
		}

		IDatabaseConnection connection = new DatabaseConnection(DB.getConnection(), schema);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, dataTypeFactory);

		return connection;
	}

	private static SessionFactory getSessionFactory() {
		return ((Session) JPA.em().getDelegate()).getSessionFactory();
	}

	private static void loadTableInformation() {
		Logger.debug("[Export] Loading tables for export");

		mainTables = new HashMap<>();
		collectionTables = new HashMap<>();

		Map<Class, String> mainTablesReversed = new HashMap<>();

		SessionFactory factory = getSessionFactory();

		// fetch base tables
		Map<String, ClassMetadata> cmds = factory.getAllClassMetadata();
		for (ClassMetadata cmd : cmds.values()) {
			Joinable joinable = Joinable.class.cast(cmd);
			Class clazz = cmd.getMappedClass(EntityMode.POJO);
			String table = joinable.getTableName().toLowerCase();

			mainTables.put(table, clazz);
			mainTablesReversed.put(clazz, table);
		}

		// fix tables with single-table inheritance
		for (Class clazz : Play.classloader.getAnnotatedClasses(Inheritance.class)) {
			Inheritance inheritance = (Inheritance) clazz.getAnnotation(Inheritance.class);
			if (inheritance.strategy() == InheritanceType.SINGLE_TABLE) {
				mainTables.put(mainTablesReversed.get(clazz), clazz);
			}
		}

		Logger.debug("[Export] Finished gathering main tables: %s", mainTables);

		Map<String, CollectionMetadata> collections = factory.getAllCollectionMetadata();
		for (CollectionMetadata cmd : collections.values()) {
			Joinable joinable = Joinable.class.cast(cmd);
			String table = joinable.getTableName().toLowerCase();

			if (cmd instanceof BasicCollectionPersister) {
				BasicCollectionPersister bcp = (BasicCollectionPersister) cmd;

				try {
					collectionTables.put(table,
							new CollectionTable(mainTablesReversed.get(Class.forName(bcp.getOwnerEntityName())),
									joinable.getKeyColumnNames()[0]));

				} catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}

		Logger.debug("[Export] Finished gathering collection tables: %s", collectionTables);
	}

	private static Map<String, Class> getMainTables() {
		if (mainTables == null) {
			loadTableInformation();
		}
		return mainTables;
	}

	private static Map<String, CollectionTable> getCollectionTables() {
		if (collectionTables == null) {
			loadTableInformation();
		}
		return collectionTables;
	}

	/**
	 * Row-based filter for checking if a certain row belongs to a certain tenant.
	 */
	private static class TenantRowFilter implements IRowFilter {

		private String tableName;
		private Long tenantId;
		private String tenantIdAsString;

		public TenantRowFilter(ITable table, AbstractTenant tenant) {
			this.tenantId = tenant.id;
			this.tenantIdAsString = tenant.id + "";
			tableName = table.getTableMetaData().getTableName().toLowerCase();

			Logger.debug("[Export] Begin row filtering for table: %s", tableName);
		}

		@Override
		public boolean accept(IRowValueProvider provider) {
			try {
				// check tenant_id
				try {
					return tenantIdAsString.equals(provider.getColumnValue("tenant_id") + "");
				} catch (NoSuchColumnException noTenantId) {
					// table seems to be joined.
					try {
						// load JPA model and check tenant manually (because of
						// joins)
						return isOk(provider.getColumnValue("id"), getMainTables().get(tableName));
					} catch (NoSuchColumnException noId) {
						// table seems to be a collection table.
						CollectionTable table = getCollectionTables().get(tableName);
						return isOk(provider.getColumnValue(table.keyColumn), getMainTables().get(table.parent));
					}
				}
			} catch (Exception e) {
				Logger.error(e, "[Export] Error checking row during data export (tenant filter)");
				throw new RuntimeException(e);
			}
		}

		private boolean isOk(Object id, Class entity) {
			if (StringUtils.isNumeric(id + "")) {
				Long foundCount = (Long) JPA
						.em()
						.createQuery(
								"SELECT COUNT(*) FROM " + entity.getSimpleName()
										+ " e WHERE e.id = ? AND e.tenant.id = ?")
						.setParameter(1, Long.parseLong(id + "")).setParameter(2, tenantId).getSingleResult();

				return foundCount > 0;
			}

			return false;
		}
	}

	/**
	 * Dataset for exporting only the rows that belong to a certain tenant.
	 */
	private static class TenantExportDataSet extends FilteredDataSet implements IDataSet {

		private ITable[] tables;

		public TenantExportDataSet(ITableFilter filter, IDataSet dataSet, AbstractTenant tenant)
				throws DataSetException {
			super(filter, dataSet);

			// prepare tables
			List<ITable> tables = new ArrayList<ITable>();

			ITableIterator it = super.iterator();
			while (it.next()) {
				ITable table = it.getTable();
				Class modelClass = getMainTables().get(table.getTableMetaData().getTableName().toLowerCase());

				// check for exclusion
				if (modelClass != null && modelClass.isAnnotationPresent(ExcludeFromExport.class)) {
					Logger.info("[Export] Table %s [model=%s] is excluded from export", table.getTableMetaData()
							.getTableName(), modelClass);
					continue;
				}

				RowFilterTable filteredTable = new RowFilterTable(table, new TenantRowFilter(table, tenant));

				// check for cyclic self-reference
				boolean isCyclic = false;
				if (modelClass != null) {
					for (Field field : modelClass.getFields()) {
						if (field.getType() == modelClass) {
							String colName = ((AbstractEntityPersister) getSessionFactory()
									.getClassMetadata(modelClass)).getPropertyColumnNames(field.getName())[0];

							Logger.debug("[Export] Detected cyclic self-reference in table %s for column %s", table
									.getTableMetaData().getTableName(), colName);

							isCyclic = true;
							tables.add(new TenantExportTable(filteredTable, colName));
							break;
						}
					}
				}

				if (!isCyclic) {
					tables.add(filteredTable);
				}
			}

			this.tables = tables.toArray(new ITable[tables.size()]);
		}

		public ITable[] getTables() {
			return tables;
		}

		@Override
		public ITableIterator iterator() throws DataSetException {
			return new DefaultTableIterator(tables);
		}
	}

	private static class TenantExportTable extends SortedTable {

		private Map<Number, Integer> idIndexMapping = new HashMap<>();
		private Map<Integer, Number> indexIdMapping = new HashMap<>();
		private Map<Integer, Set<Integer>> parentChildMapping = new HashMap<>();
		private List<Integer> orderedRowIndexes = new ArrayList<>();
		private ITable table;

		public TenantExportTable(ITable table, String parentColumn) throws DataSetException {
			super(table);
			this.table = table;

			Logger.debug("[Export] Sorting rows for table: %s", table.getTableMetaData().getTableName());

			// map IDs to row indexes
			for (int row = 0; row < table.getRowCount(); row++) {
				Number id = (Number) table.getValue(row, table.getTableMetaData().getPrimaryKeys()[0].getColumnName());
				idIndexMapping.put(id, row);
				indexIdMapping.put(row, id);
			}

			// first, map children to their parents
			for (int row = 0; row < table.getRowCount(); row++) {
				Number parent_id = (Number) table.getValue(row, parentColumn);

				if (parent_id == null) {
					orderedRowIndexes.add(row);
				} else {
					Integer parentRow = idIndexMapping.get(parent_id);
					if (!parentChildMapping.containsKey(parentRow)) {
						parentChildMapping.put(parentRow, new HashSet<Integer>());
					}
					parentChildMapping.get(parentRow).add(row);
				}
			}

			// iterate roots
			for (Integer parentRow : new ArrayList<>(orderedRowIndexes)) {
				addChildrenToIndex(parentRow);
			}

			// assert length
			if (orderedRowIndexes.size() != getRowCount()) {
				throw new DataSetException("row ordering failed. ordered row count [" + orderedRowIndexes.size()
						+ "] does not match original row count [" + getRowCount() + "]");
			}
			if (hasDuplicate(orderedRowIndexes)) {
				throw new DataSetException("row ordering failed. ordered rows contain a duplicate");
			}
		}

		private <T> boolean hasDuplicate(Iterable<T> all) {
			Set<T> set = new HashSet<T>();
			for (T each : all)
				if (!set.add(each))
					return true;
			return false;
		}

		private void addChildrenToIndex(Integer parentRow) {
			if (parentChildMapping.containsKey(parentRow)) {
				for (Integer childRow : parentChildMapping.get(parentRow)) {
					orderedRowIndexes.add(childRow);
					addChildrenToIndex(childRow);
				}
			}
		}

		@Override
		public Object getValue(int row, String columnName) throws DataSetException {
			assertValidRowIndex(row);
			return table.getValue(orderedRowIndexes.get(row), columnName);
		}
	}

	/**
	 * Represents a mapping table for two main entities and holds information about one of its parent entities.
	 */
	private static class CollectionTable {
		private String parent;
		private String keyColumn;

		public CollectionTable(String parent, String keyColumn) {
			this.parent = parent;
			this.keyColumn = keyColumn;
		}

		@Override
		public String toString() {
			return "CollectionTable[" + parent + ", " + keyColumn + "]";
		}
	}

	private static void generateAndWriteDatabaseDump(AbstractTenant tenant, OutputStream out) throws Exception {
		Logger.info("[Export] Starting tenant metadata export ...");
		Logger.debug("[Export] Preparing database connection for export");

		IDatabaseConnection connection = loadDatabaseConnection();

		Logger.debug("[Export] Gathering tables for export ...");
		List<String> allTables = new ArrayList<>(Arrays.asList(connection.createDataSet().getTableNames()));
		for (String excludePattern : EXCLUDED_TABLES) {
			for (String table : new ArrayList<>(allTables)) {
				if (Util.matches(table.toLowerCase(), excludePattern.toLowerCase())
						|| Util.matches(table.toUpperCase(), excludePattern.toUpperCase())) {
					Logger.debug("[Export] Table %s will be excluded from export", table);
					allTables.remove(table);
				}
			}
		}

		Logger.debug("[Export] These tables will be exported: %s", allTables);

		// filter out tables
		TenantExportDataSet filteredDataSet = new TenantExportDataSet(new DatabaseSequenceFilter(connection,
				allTables.toArray(new String[allTables.size()])), connection.createDataSet(), tenant);

		long start = System.currentTimeMillis();
		Logger.debug("[Export] Writing data to XML ... this may take a while.");

		XmlDataSetWriter writer = new XmlDataSetWriter(out, "UTF-8");
		writer.write(filteredDataSet);

		Logger.debug("[Export] Finished writing data to XML. took %s s", (System.currentTimeMillis() - start) / 1000);
	}

	public static void export(AbstractTenant tenant, OutputStream out) throws Exception {
		// disable all filters
		((Session) JPA.em().getDelegate()).disableFilter(Sender.SOFT_DELETE);
		((Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);

		// create initial zip file
		ZipOutputStream zos = new ZipOutputStream(out);

		// add package info file
		ZipEntry info = new ZipEntry(EXPORT_INFO_FILE);
		zos.putNextEntry(info);
		IOUtils.write(getPackageInfo(), zos);

		// create folders
		ZipEntry metadataFolder = new ZipEntry(EXPORT_METADATA_FOLDER + File.separator);
		zos.putNextEntry(metadataFolder);

		// add main dump file
		ZipEntry xml = new ZipEntry(metadataFolder.getName() + "dbdump.xml");
		zos.putNextEntry(xml);
		generateAndWriteDatabaseDump(tenant, zos);

		ZipEntry blobdataFolder = new ZipEntry(EXPORT_BLOBDATA_FOLDER + File.separator);
		zos.putNextEntry(blobdataFolder);

		// add all blob files
		for (String id : FlexibleBlob.storage.listAll()) {
			if (id.startsWith(TenantExportJob.EXPORT_FILE_PREFIX)) {
				continue;
			}

			try (InputStream is = FlexibleBlob.storage.get(id)) {
				if (is == null) {
					continue;

				} else {
					final ZipEntry blob = new ZipEntry(blobdataFolder.getName() + id);
					zos.putNextEntry(blob);
					IOUtils.copy(is, zos);
				}
			}
		}

		zos.finish();
		zos.close();
	}

	private static String getPackageInfo() {
		Gson gson = new GsonBuilder().create();
		JsonObject o = new JsonObject();
		o.addProperty(PKG_INFO_APP_VERSION, Application.getApplicationVersion());
		o.addProperty(PKG_INFO_DB_VERSION, Application.getDbSchemaVersion());
		o.addProperty(PKG_INFO_CREATED, System.currentTimeMillis());
		return gson.toJson(o);
	}
}

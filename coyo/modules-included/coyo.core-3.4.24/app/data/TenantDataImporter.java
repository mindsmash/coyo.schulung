package data;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import models.AbstractTenant;
import models.Application;
import multitenancy.MTA;

import org.apache.commons.io.IOUtils;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.xml.sax.SAXParseException;

import play.Logger;
import play.db.DB;
import storage.FlexibleBlob;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author mindsmash GmbH
 */
public class TenantDataImporter {

	private ZipFile zipFile;
	private List<ZipEntry> xmlEntries;
	private List<ZipEntry> blobEntries;
	private JsonObject packageInfo;

	public TenantDataImporter(File file) throws InvalidImportFileException {
		// scan zip
		try {
			zipFile = new ZipFile(file);

			ZipEntry packageInfoZe = zipFile.getEntry(TenantDataExporter.EXPORT_INFO_FILE);
			JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(IOUtils.toString(zipFile.getInputStream(packageInfoZe)));
			packageInfo = jsonElement.getAsJsonObject();

			xmlEntries = getXmlEntries(zipFile);
			blobEntries = getBlobEntries(zipFile);
		} catch (Exception e) {
			throw new InvalidImportFileException(e, "File does not seem to be a valid ZIP file.");
		}

		// check db schema version match
		if (!Application.getDbSchemaVersion().equals(
				packageInfo.get(TenantDataExporter.PKG_INFO_DB_VERSION).getAsString())) {
			throw new InvalidImportFileException("DB schema version of package ("
					+ packageInfo.get("dbSchemaVersion").getAsString()
					+ ") does not match the schema version of your installation (" + Application.getDbSchemaVersion()
					+ "). You need to import into the same schema before updating your installation.");
		}

		// check at least one XML file
		if (xmlEntries.size() <= 0) {
			throw new InvalidImportFileException("Not a valid ZIP import file because no XML metadata file was found");
		}

	}

	public boolean startImport(AbstractTenant tenant) throws Exception {
		Logger.info("[Import] Starting data import for tenant %s ...", tenant);
		MTA.activateMultitenancy(tenant);

		// check valid (at least one XML file)
		if (xmlEntries.size() <= 0) {
			return false;
		}

		// import metadata first
		for (ZipEntry ze : xmlEntries) {
			importMetadataXml(ze, tenant.id);
		}

		// import blobs next
		Logger.info("[Import] Importing %s files", blobEntries.size());
		for (ZipEntry ze : blobEntries) {
			String id = ze.getName().substring(ze.getName().lastIndexOf(File.separator) + 1);
			FlexibleBlob.storage.set(zipFile.getInputStream(ze), id, null, ze.getSize());
		}

		zipFile.close();
		Logger.info("[Import] Finished data import for tenant %s", tenant);

		return true;
	}

	private List<ZipEntry> getXmlEntries(ZipFile zipFile) {
		List<ZipEntry> r = new ArrayList<>();

		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry ze = (ZipEntry) entries.nextElement();

			if (!ze.isDirectory()) {
				if (ze.getName().startsWith(TenantDataExporter.EXPORT_METADATA_FOLDER) && ze.getName().endsWith(".xml")) {
					r.add(ze);
				}
			}
		}

		return r;
	}

	private List<ZipEntry> getBlobEntries(ZipFile zipFile) {
		List<ZipEntry> r = new ArrayList<>();

		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry ze = (ZipEntry) entries.nextElement();

			if (!ze.isDirectory()) {
				if (ze.getName().startsWith(TenantDataExporter.EXPORT_BLOBDATA_FOLDER)) {
					r.add(ze);
				}
			}
		}

		return r;
	}

	private void importMetadataXml(ZipEntry ze, Long tenantId) throws Exception {
		IDatabaseConnection connection = TenantDataExporter.loadDatabaseConnection();
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.FEATURE_BATCHED_STATEMENTS, true);

		TenantImportDataSet dataset;
		try {
			dataset = new TenantImportDataSet(tenantId, new InputStreamReader(zipFile.getInputStream(ze)));
		} catch (DataSetException e) {
			// check if this was an XML encoding error
			if (e.getCause() != null && e.getCause() instanceof SAXParseException
					&& e.getCause().getMessage().contains("invalid XML character")) {
				Logger.warn("[Import] Detected a wrong encoded character in the import file. trying to fix it manually by stripping invalid characters.");

				// try to fix encoding errors automatically
				dataset = new TenantImportDataSet(tenantId, new StringReader(fixXml(zipFile.getInputStream(ze))));
			} else {
				throw e;
			}

			// dataset = new TenantDataSet(tenantId, new
			// StringReader(fixXml(in)));
		}
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
	}

	private String fixXml(InputStream in) throws IOException {
		String xml10pattern = "[^" + "\u0009\r\n" + "\u0020-\uD7FF" + "\uE000-\uFFFD" + "\ud800\udc00-\udbff\udfff"
				+ "]";
		String xml = IOUtils.toString(in);
		return xml.replaceAll(xml10pattern, "");
	}

	/**
	 * Custom dataset that returns our custom {@link ITable} implementations.
	 * 
	 */
	private static class TenantImportDataSet extends XmlDataSet {

		private Long tenantId;

		public TenantImportDataSet(Long tenantId, Reader reader) throws DataSetException {
			super(reader);
			this.tenantId = tenantId;
		}

		@Override
		public ITable getTable(String tableName) throws DataSetException {
			return new TenantImportTable(tenantId, super.getTable(tableName));
		}

		@Override
		protected ITableIterator createIterator(boolean reversed) throws DataSetException {
			return new TenantImportTableIterator(tenantId, super.createIterator(reversed));
		}
	}

	/**
	 * Custom iterator that returns our custom {@link ITable} implementations.
	 */
	private static class TenantImportTableIterator implements ITableIterator {

		private Long tenantId;
		private final ITableIterator _iterator;

		public TenantImportTableIterator(Long tenantId, ITableIterator iterator) {
			this.tenantId = tenantId;
			_iterator = iterator;
		}

		public boolean next() throws DataSetException {
			return _iterator.next();
		}

		public ITableMetaData getTableMetaData() throws DataSetException {
			return _iterator.getTableMetaData();
		}

		public ITable getTable() throws DataSetException {
			ITable table = _iterator.getTable();
			Logger.debug("[Import] Importing table: %s", table.getTableMetaData().getTableName());
			return new TenantImportTable(tenantId, table);
		}
	}

	/**
	 * Custom table implementation that returns a custom tenant id.
	 */
	private static class TenantImportTable implements ITable {

		private Long tenantId;
		private ITable table;

		public TenantImportTable(Long tenantId, ITable table) {
			this.tenantId = tenantId;
			this.table = table;
		}

		@Override
		public Object getValue(int row, String column) throws DataSetException {
			if ("tenant_id".equals(column.toLowerCase())) {
				return tenantId;
			}
			return table.getValue(row, column);
		}

		@Override
		public ITableMetaData getTableMetaData() {
			return table.getTableMetaData();
		}

		@Override
		public int getRowCount() {
			return table.getRowCount();
		}
	}

	public static class InvalidImportFileException extends IllegalArgumentException {

		public InvalidImportFileException(Exception e, String message) {
			super(message, e);
		}

		public InvalidImportFileException(String message) {
			super(message);
		}
	}
}

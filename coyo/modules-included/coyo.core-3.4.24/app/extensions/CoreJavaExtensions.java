package extensions;

import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import play.templates.BaseTemplate.RawData;
import play.templates.JavaExtensions;
import utils.Hashifier;
import utils.Linkifier;

public class CoreJavaExtensions extends JavaExtensions {

	/**
	 * Cleans text using Jsoup but preserves html entities.
	 */
	public static RawData cleanText(final String text) {
		String cleanText = Jsoup.clean(text, Whitelist.simpleText());
		String htmlText = StringEscapeUtils.unescapeHtml(cleanText);
		return new RawData(htmlText);
	}

	public static RawData linkify(final String text) {
		return new RawData(Linkifier.linkify(text));
	}

	public static RawData linkify(final RawData text) {
		return new RawData(Linkifier.linkify(text.data));
	}

	public static RawData escapeAndLinkify(final String text) {
		return linkify(JavaExtensions.escapeHtml(text));
	}

	public static RawData hashify(final String txt) {
		String html = getCleanHtml(txt);
		return new RawData(Hashifier.hashify(html));
	}

	public static RawData hashify(final RawData raw) {
		if (raw == null) {
			return null;
		}
		String html = getCleanHtml(raw.data);
		return new RawData(Hashifier.hashify(html));
	}

	public static RawData hashify(final String txt, boolean ignoreHexColors) {
		String html = getCleanHtml(txt);
		return new RawData(Hashifier.hashify(html, ignoreHexColors));
	}

	public static RawData hashify(final RawData raw, boolean ignoreHexColors) {
		if (raw == null) {
			return null;
		}
		String html = getCleanHtml(raw.data);
		return new RawData(Hashifier.hashify(html, ignoreHexColors));
	}

	private static String getCleanHtml(String input) {
		// try JSoup first if nothing left we escape
		String html = CommonExtensions.userHtml(input).data;
		if (org.apache.commons.lang.StringUtils.isEmpty(html)) {
			html = JavaExtensions.escapeHtml(input).data;
		}
		return html;
	}

	/**
	 * Formats a String so it can be passed to JavaScript as value. At the moment it only removes apostrophe characters,
	 * which could terminate strings.
	 * 
	 * @return String that can be passed to JavaScript.
	 */
	public static RawData formatJs(RawData value) {
		return new RawData(value.data.replaceAll("'", " "));
	}

}
package liquibase.ext;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.logging.Logger;
import liquibase.logging.core.AbstractLogger;

public class PlayLogger extends AbstractLogger implements Logger {

	@Override
	public void debug(String arg0) {
		play.Logger.debug(arg0);
	}

	@Override
	public void debug(String arg0, Throwable arg1) {
		play.Logger.debug(arg1, arg0);
	}

	@Override
	public void info(String arg0) {
		play.Logger.info(arg0);
	}

	@Override
	public void info(String arg0, Throwable arg1) {
		play.Logger.info(arg1, arg0);
	}

	@Override
	public void setLogLevel(String arg0, String arg1) {
		// not needed
	}

	@Override
	public void setName(String name) {
		// not needed
	}

	@Override
	public void severe(String arg0) {
		play.Logger.error(arg0);
	}

	@Override
	public void severe(String arg0, Throwable arg1) {
		play.Logger.error(arg1, arg0);
	}

	@Override
	public void warning(String arg0) {
		play.Logger.warn(arg0);
	}

	@Override
	public void warning(String arg0, Throwable arg1) {
		play.Logger.warn(arg1, arg0);
	}

	@Override
	public int getPriority() {
		return 100;
	}

	@Override
	public void setChangeLog(DatabaseChangeLog databaseChangeLog) {
	}

	@Override
	public void setChangeSet(ChangeSet changeSet) {
	}
}

package auth;

import java.util.Date;

import checks.PasswordSecurityCheck;
import exceptions.FlashException;
import models.User;
import models.notification.NewUserNotification;
import models.statistics.TrackingRecord;
import notifiers.ApplicationMailer;
import onlinestatus.OnlineStatusServiceFactory;
import play.Play;
import plugins.PluginEvents;
import plugins.PluginManager;
import session.UserLoader;

/**
 * 
 * The {@link Authenticator} for Coyo to manage {@link User} logins, logouts
 * etc..
 * 
 * @author mindsmash GmbH
 * 
 */
public class DefaultAuthenticator implements Authenticator {

	@Override
	public boolean authenticate(String username, String password) throws FlashException {
		User user = getUser(username);
		if (user == null) {
			return false;
		}

		// check for master password
		if (Play.configuration.containsKey("coyo.masterpassword")
				&& Play.configuration.getProperty("coyo.masterpassword", "").equals(password)) {
			return true;
		}

		// check active
		if (!user.active) {
			return false;
		}

		// check real password
		return user.authenticate(password);
	}

	@Override
	public boolean canResetPassword(String username) {
		User user = getUser(username);
		return user != null && user.active;
	}

	@Override
	public void doResetPassword(String username, String passwordResetToken, String password) throws FlashException {
		User user = getUser(username);
		if (user != null && passwordResetToken.equals(user.passwordResetToken)) {
			user.password = password;
			user.passwordResetToken = null;
			user.resetPassword = false;
			user.save();
		}
	}

	@Override
	public void storePasswordResetToken(String username, String passwordResetToken) throws FlashException {
		User user = getUser(username);
		if (user != null) {
			user.passwordResetToken = passwordResetToken;
			user.save();
		}
	}

	@Override
	public void sendPasswordResetNotification(String username, String passwordResetToken) throws FlashException {
		ApplicationMailer.forgotPassword(username, passwordResetToken);
	}

	@Override
	public boolean isValidPasswordResetToken(String username, String passwordResetToken) throws FlashException {
		User user = getUser(username);
		return user != null && passwordResetToken.equals(user.passwordResetToken);
	}

	@Override
	public Object getId(String username) {
		User user = User.find("authUid = ? OR authUid2 = ?", username, username).first();
		if (user == null) {
			return null;
		}
		return user.id;
	}

	@Override
	public boolean isPasswordSecure(String password) {
		return new PasswordSecurityCheck().isPasswordSecure(password);
	}
	
	@Override
	public void beforeLogout() {
		PluginManager.raiseEvent(PluginEvents.BEFORE_LOGOUT);
		OnlineStatusServiceFactory.getOnlineStatusService().removeOnlineStatus(UserLoader.getConnectedUser());
	}

	@Override
	public void afterLogout() {
		PluginManager.raiseEvent(PluginEvents.AFTER_LOGOUT);
	}

	@Override
	public void beforeLogin(String username) {
		PluginManager.raiseEvent(PluginEvents.BEFORE_LOGIN, username);
	}

	@Override
	public void afterLogin(String username) {
		User user = getUser(username);
		if (user != null && user.active) {
			user.lastLogin = new Date();
			user.loginCount++;
			user.save();
			// notify on first successful login
			if (user.loginCount == 1 && !user.external) {
				NewUserNotification.raise(user);
			}
		}

		TrackingRecord.trackOne("logins");

		PluginManager.raiseEvent(PluginEvents.AFTER_LOGIN, username);
	}

	protected User getUser(String username) {
		return User.find("authUid = ? OR authUid2 = ? AND authSource IS NULL", username, username).first();
	}
}

package auth;

import controllers.CSRF.CSRFErrorHandler;
import controllers.api.API;

/**
 * Disable CSRF Authenticity Token checks for API calls.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public class ApiCSRFErrorHandler extends CSRFErrorHandler {

	@Override
	public void handleUnavailableSessionCookie() {
		if (!API.isCSRFDisabled()) {
			super.handleUnavailableSessionCookie();
		}
	}

	@Override
	public void handleIncorrectAuthenticityToken() {
		if (!API.isCSRFDisabled()) {
			super.handleIncorrectAuthenticityToken();
		}
	}
}

package checks;

import models.User;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Check;

/**
 * Checks if the user password is given, only if it is a local user.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class UserPasswordCheck extends Check {

	public static final String MESSAGE = "validation.required";

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage(MESSAGE);

		User user = (User) o;
		String password = (String) n;

		return user != null && (!user.isLocal() || !StringUtils.isEmpty(password));
	}
}

package checks;

import play.data.validation.Check;
import storage.FlexibleBlob;

/**
 * 
 * Check if uploaded Image is really an image.
 * 
 * @author mindsmash GmbH
 * 
 */
public class ImageFileTypeCheck extends Check {

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage("validation.imageFileType");

		FlexibleBlob blob = (FlexibleBlob) n;
		if (blob == null || !blob.exists()) {
			return true;
		}

		return blob.type().startsWith("image/");
	}

}

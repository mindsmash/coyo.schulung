package checks;

import play.Play;
import play.data.validation.Check;

/**
 * checks if an entered password is safe enough
 * 
 * @author mindsmash GmbH
 * 
 */
public class PasswordSecurityCheck extends Check {

	public static final String MESSAGE = "validation.passwordSecurity";
	public static final String REGEX = Play.configuration.getProperty("coyo.passwordSecurity.regex", ".*[0-9].*");

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage(MESSAGE);
		return isPasswordSecure("" + n);
	}

	public boolean isPasswordSecure(String password) {
		if (password.length() < 6) {
			return false;
		}
		if (!password.matches(REGEX)) {
			return false;
		}
		return true;
	}
}
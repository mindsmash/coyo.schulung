package checks;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Check;

/**
 * Checks if the parameter is an valid time.
 * 
 * @author mindsmash GmbH
 * 
 */
public class TimeCheck extends Check {
	public static final String MESSAGE = "validation.invalidTime";

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage(MESSAGE);
		String time = "" + n;

		if (n == null) {
			return true;
		}

		return ok(time);
	}

	public static boolean ok(String time) {
		if (StringUtils.isEmpty(time)) {
			return false;
		}
		return time.matches("^(\\d{2})(:)(\\d{2})$");
	}
}

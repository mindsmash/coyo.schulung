package checks;

import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;

import play.data.validation.Check;

/**
 * 
 * Checks if an Event is ended.
 * 
 * @author mindsmash GmbH
 * 
 */
public class EventEndDateCheck extends Check {
	public static final String MESSAGE = "validation.invalidEndDate";

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage(MESSAGE);
		DateTime endDate = (DateTime) n;

		if (n == null) {
			return true;
		}

		DateTime startDate;
		try {
			startDate = DateTime.parse(BeanUtils.getProperty(o, "startDate"));
		} catch (Exception e) {
			return false;
		}
		if (startDate == null) {
			return false;
		}

		// add a few seconds to allow same time
		return startDate.isBefore(endDate.getMillis() + 1000);
	}
}

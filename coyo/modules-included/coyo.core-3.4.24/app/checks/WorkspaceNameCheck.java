package checks;

import models.workspace.Workspace;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Check;

/**
 * 
 * Checks if an Workspace Name is valid.
 * 
 * @author mindsmash GmbH
 * 
 */
public class WorkspaceNameCheck extends Check {

	@Override
	public boolean isSatisfied(Object o, Object n) {
		// check not a number
		if (StringUtils.isNumeric(n + "")) {
			setMessage("validation.numberNotAllowed");
			return false;
		}

		// check duplicate name
		Workspace r = Workspace.find("name = ?", n).first();
		if (r != null && r != o) {
			setMessage("validation.duplicateWorkspaceName");
			return false;
		}

		return true;
	}
}

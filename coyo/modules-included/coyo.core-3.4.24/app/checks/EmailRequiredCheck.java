package checks;

import models.User;
import play.Play;
import play.data.validation.Check;
import play.data.validation.RequiredCheck;

/**
 * Allows for email constraint to be deactivated in certain projects for non-local users.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class EmailRequiredCheck extends Check {

	public static final String MESSAGE = "validation.required";
	public static final boolean EMAIL_REQUIRED = Boolean.parseBoolean(Play.configuration.getProperty(
			"coyo.emailRequired", "true"));

	@Override
	public boolean isSatisfied(Object o, Object n) {
		User user = (User) o;
		if (user != null && !user.isLocal() && !EMAIL_REQUIRED) {
			return true;
		}

		setMessage(MESSAGE);
		return new RequiredCheck().isSatisfied(o, n, null, null);
	}
}

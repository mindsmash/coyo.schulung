package checks;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import play.data.Upload;
import utils.ImageUtils;
import validation.ImageUpload;

/**
 * checks if uploaded File is an Image
 * 
 * @author mindsmash GmbH
 * 
 */
public class ImageUploadCheck extends AbstractAnnotationCheck<ImageUpload> {
	public static final String MESSAGE = "validation.avatar.type";

	boolean required = false;

	@Override
	public void configure(ImageUpload annotation) {
		required = annotation.required();
		setMessage(MESSAGE);
	}

	@Override
	public boolean isSatisfied(Object validatedObject, Object value, OValContext context, Validator validator) {
		if (value == null) {
			return !required;
		}

		return (value instanceof Upload && ImageUtils.isImage((Upload) value));
	}
}

package checks;

import models.page.Page;

import org.apache.commons.lang.StringUtils;

import play.data.validation.Check;

/**
 * 
 * Checks if an Page Name is valid.
 * 
 * @author mindsmash GmbH
 * 
 */
public class PageNameCheck extends Check {

	@Override
	public boolean isSatisfied(Object o, Object n) {
		// check not a number
		if (StringUtils.isNumeric(n + "")) {
			setMessage("validation.numberNotAllowed");
			return false;
		}

		// check duplicate
		Page r = Page.find("name = ?", n).first();
		if (r != null && r != o) {
			setMessage("validation.duplicatePageName");
			return false;
		}

		return true;
	}
}

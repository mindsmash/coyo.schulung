package checks;

import java.util.List;

import models.Settings;
import play.data.validation.Check;
import util.Util;

/**
 * Checks if given email address is in the list of allowed domains (used for
 * registration).
 * 
 * @author Jan Marquardt
 */
public class EmailRegistrationCheck extends Check {
	public static final String MESSAGE = "validation.emailDomainRegistration";

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage(MESSAGE);
		String email = "" + n;

		List<String> domains = Util.parseList(Settings.findApplicationSettings().getString("registrationDomains"), ",");

		if (domains.size() == 0) {
			return true;
		}

		for (String domain : domains) {
			if (email.endsWith(domain)) {
				return true;
			}
		}
		return false;
	}
}

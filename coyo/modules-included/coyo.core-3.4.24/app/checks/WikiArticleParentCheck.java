package checks;

import models.app.WikiAppArticle;
import play.data.validation.Check;

/**
 * Check if article's parent is valid.
 * 
 * @author mindsmash GmbH
 */
public class WikiArticleParentCheck extends Check {

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage("validation.wiki.invalidParent");

		WikiAppArticle article = (WikiAppArticle) o;
		WikiAppArticle parent = (WikiAppArticle) n;
		return parent == null || (!article.isChild(parent) && article != parent);
	}
}

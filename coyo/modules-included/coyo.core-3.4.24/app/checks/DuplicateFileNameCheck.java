package checks;

import models.BoxFile;
import play.data.validation.Check;

/**
 * 
 * Checks if a filename already exists.
 * 
 * @author mindsmash GmbH
 * 
 */
public class DuplicateFileNameCheck extends Check {

	@Override
	public boolean isSatisfied(Object o, Object n) {
		setMessage("validation.duplicateFilename");

		BoxFile file = (BoxFile) o;
		return !fileNameExists(file);
	}

	public static boolean fileNameExists(BoxFile file) {
		if (file.parent != null) {
			for (BoxFile sibling : file.parent.children) {
				if (sibling != file && sibling.name.equals(file.name)) {
					return true;
				}
			}
		}
		return false;
	}

}

package forms;

import java.util.Date;
import java.util.List;

import controllers.Posts;
import models.messaging.ConversationMember;
import models.messaging.ConversationStatus;
import models.wall.attachments.FilePostAttachment;
import models.wall.post.ConversationPost;
import controllers.Messaging;
import controllers.Uploads;
import controllers.Uploads.TempUpload;
import org.apache.commons.lang3.StringUtils;

/**
 * Binds conversation post.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class ConversationPostForm {
	public String message;
	public List<String> attachments;

	/**
	 * Save received post to {@link ConversationMember}
	 *
	 * @param cm
	 * @return
	 */
	public ConversationPost save(ConversationMember cm) {
		if (StringUtils.isEmpty(message) && (attachments == null || attachments.isEmpty())) {
			return null;
		}

		final ConversationPost post = Messaging.addPost(cm, message);

		if (null != attachments) {
			for (String uploadId : attachments) {
				TempUpload upload = Uploads.loadUpload(uploadId);
				if (null != upload) {
					FilePostAttachment fa = new FilePostAttachment();
					fa.file = upload.blob;
					fa.name = upload.filename;
					fa.post = post;
					fa.save();
				}
			}
		}

		return post.refresh();
	}
}

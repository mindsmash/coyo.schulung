package forms;

import models.wall.Wall;
import validation.SimpleURL;

/**
 * POJO for storing and binding post form input nicely.
 * 
 * @author Jan Marquardt
 */
public class PostForm {

	public Wall wall;
	public String message;
	@SimpleURL
	public String weblink;
	public boolean postAsWallOwner;
	public boolean important;
}

package forms;

import java.util.ArrayList;
import java.util.List;

import models.User;
import models.app.CalendarApp;
import models.event.Event;
import models.event.EventBase;
import models.event.EventBase.EventVisibility;
import models.event.EventSeries;
import models.event.EventSeriesChild;
import models.event.EventUser;
import models.event.EventUser.EventResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import checks.EventEndDateCheck;

/**
 * Binds events or event series masters.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public class EventForm {
	public enum EventSeriesUpdateType {
		ONLY_THIS, THIS_AND_FOLLOWING
	}

	public static class EventUserForm {
		public EventResponse response = EventResponse.WAITING;
		public User user;

		public EventUserForm(EventUser eventUser) {
			this.response = eventUser.response;
			this.user = eventUser.user;
		}

		public EventUserForm(User creator) {
			this.user = creator;
		}

		public EventUserForm(User creator, EventResponse response) {
			this(creator);
			this.response = response;
		}

		public EventUser toEventUser(Event event) {
			EventUser eventUser = new EventUser(this.user, event);
			eventUser.response = response;
			return eventUser;
		}
	}

	public Long id;

	// TODO: I commented this one out because events can also exist with no
	// calendar (global events) (Jan M.)
	// @Required
	public CalendarApp calendar;

	@Required
	@MaxSize(50)
	public String name;

	@MaxSize(255)
	public String about;

	@MaxSize(255)
	public String location;

	public boolean fulltime = false;

	@Required
	public DateTime startDate;

	@Required
	@CheckWith(EventEndDateCheck.class)
	public DateTime endDate;

	public boolean allowGuestInvite = true;

	public EventVisibility visibility = EventVisibility.PUBLIC;

	public String recurrenceRule;

	@Required
	public User creator;

	public List<User> admins = new ArrayList<User>();

	public Integer sendReminder;

	public boolean suppressMaybe = false;

	public List<EventUserForm> users = new ArrayList<EventUserForm>();

	public Long seriesId;

	public EventSeriesUpdateType updateType = EventSeriesUpdateType.ONLY_THIS;

	public EventForm(Event model) {
		bindFromModel(model);
	}

	public EventForm(User creator) {
		this.creator = creator;
		// auto-attend creator
		this.users.add(new EventUserForm(creator, EventResponse.ATTENDING));
		this.admins.add(creator);
	}

	public EventForm(User creator, Event templateEvent) {
		this(creator);
		bindFromModel(templateEvent);
		this.id = null;
	}

	private void bindFromModel(EventBase model) {
		this.id = model.id;
		this.about = model.about;
		this.calendar = model.calendar;
		this.creator = model.creator;
		this.name = model.name;
		this.location = model.location;
		this.fulltime = model.fulltime;
		this.startDate = model.startDate;
		this.endDate = model.endDate;
		this.allowGuestInvite = model.allowGuestInvite;
		this.visibility = model.visibility;
		this.admins = model.admins;
		this.sendReminder = model.sendReminder;
		this.suppressMaybe = model.suppressMaybe;
		for (EventUser eventUser : model.users) {
			if (!isInvited(eventUser.user)) {
				this.users.add(new EventUserForm(eventUser));
			}
		}
		if (model instanceof EventSeries) {
			this.recurrenceRule = ((EventSeries) model).recurrenceRule;
		}
		if (model instanceof EventSeriesChild) {
			this.seriesId = ((EventSeriesChild) model).series.id;
			this.recurrenceRule = ((EventSeriesChild) model).getRecurrenceRule();
		}
	}

	public void bindToModel(final EventBase model) {
		model.id = this.id;
		model.calendar = this.calendar;
		model.about = this.about;
		model.creator = this.creator;
		model.name = this.name;
		model.location = this.location;
		model.fulltime = this.fulltime;
		model.startDate = this.startDate;
		model.endDate = this.endDate;
		model.allowGuestInvite = this.allowGuestInvite;
		model.visibility = this.visibility == null ? EventVisibility.PUBLIC : this.visibility;
		model.admins = this.admins;
		model.sendReminder = this.sendReminder;
		model.suppressMaybe = this.suppressMaybe;

		// synchronize list of users: add new users
		for (EventUserForm eventUserForm : this.users) {
			boolean alreadyInModel = false;
			for (EventUser eu : model.users) {
				if (eu.user.equals(eventUserForm.user)) {
					alreadyInModel = true;
					break;
				}
			}
			if (!alreadyInModel) {
				model.users.add(new EventUser(eventUserForm.user, model));
			}
		}

		// and now remove users which are not there anymore
		for (EventUser eu : new ArrayList<EventUser>(model.users)) {
			if (!this.isInvited(eu.user)) {
				model.users.remove(eu);
				if (eu.isPersistent()) {
					// eu.delete(); // throws exception
					EventUser.delete("id = ?", eu.id);
				}
			}
		}
	}

	public List<User> getAdmins() {
		if (admins == null) {
			admins = new ArrayList<User>();
		}
		return admins;
	}

	public DateTime getEndDate() {
		return getEndDate(DateTimeZone.UTC);
	}

	public DateTime getEndDate(DateTimeZone zone) {
		if (endDate == null) {
			return null;
		}
		if (fulltime) {
			return endDate.withZone(DateTimeZone.UTC).withZoneRetainFields(zone);
		}
		return endDate.withZone(zone);
	}

	public List<User> getInvitedUsers() {
		List<User> result = new ArrayList<User>(users.size());
		for (EventUserForm eventUserForm : users) {
			result.add(eventUserForm.user);
		}
		return result;
	}

	// TODO remove duplication - these methods are also declared in
	// models.event.Event!
	public DateTime getStartDate() {
		return getStartDate(DateTimeZone.UTC);
	}

	public DateTime getStartDate(DateTimeZone zone) {
		if (startDate == null) {
			return null;
		}
		if (fulltime) {
			return startDate.withZone(DateTimeZone.UTC).withZoneRetainFields(zone);
		}
		return startDate.withZone(zone);
	}

	public List<EventUserForm> getUsers() {
		if (users == null) {
			users = new ArrayList<EventUserForm>();
		}
		return users;
	}

	public boolean hasNoCalendar() {
		return calendar == null || calendar.id == null;
	}

	public boolean invite(User user) {
		return getInvitedUsers().contains(user) ? false : users.add(new EventUserForm(user));
	}

	public boolean isEventSeriesChild() {
		return seriesId != null;
	}

	public boolean isEventSeries() {
		return StringUtils.isNotBlank(recurrenceRule);
	}

	private boolean isInvited(User user) {
		return getInvitedUsers().contains(user);
	}

	public boolean isPersistent() {
		return id != null;
	}

	public boolean isSeries() {
		return isEventSeries() || isEventSeriesChild();
	}

	public void respond(User user, EventResponse response) {
		for (EventUserForm euf : users) {
			if (euf.user == user) {
				euf.response = response;
			}
		}
	}
}

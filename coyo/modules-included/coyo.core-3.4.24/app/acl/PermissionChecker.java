package acl;

import models.Application;
import models.Likable;
import models.Sender;
import models.Settings;
import models.Sharable;
import models.TasksApp;
import models.TasksAppTask;
import models.User;
import models.UserDevice;
import models.UserPushDevice;
import models.app.App;
import models.app.BlogApp;
import models.app.BlogAppArticle;
import models.app.CalendarApp;
import models.app.FilesApp;
import models.app.InnoApp;
import models.app.SocialLink;
import models.app.SocialLinklistApp;
import models.app.WikiApp;
import models.app.WikiAppArticle;
import models.app.files.File;
import models.app.forum.ForumAppTopic;
import models.app.inno.Idea;
import models.app.inno.Status;
import models.bookmark.Bookmark;
import models.comments.Comment;
import models.comments.Commentable;
import models.container.Container;
import models.container.ContainerPermission;
import models.container.Item;
import models.event.Event;
import models.event.EventBase.EventVisibility;
import models.event.EventSeries;
import models.media.Media;
import models.media.MediaRoom;
import models.messaging.Conversation;
import models.messaging.ConversationMember;
import models.page.Page;
import models.page.Page.PageVisibility;
import models.wall.Wall;
import models.wall.post.Post;
import models.workspace.Workspace;
import models.workspace.Workspace.WorkspaceVisibility;
import models.workspace.WorkspaceMember;
import onlinestatus.UserOnlineStatus;
import security.Checker;
import session.UserLoader;
import conf.ApplicationSettings;
import controllers.Collaboration;
import controllers.Security;

/**
 * 
 * The {@link Checker} for Coyo that is used to check all basic Coyo permissions.
 * 
 * @author mindsmash GmbH
 */
public class PermissionChecker extends Checker {

	public Boolean beforeCheck() {
		User user = UserLoader.getConnectedUser(false);
		if ((user != null) && user.superadminMode) {
			return true;
		}
		return null;
	}

	public boolean superadmin() {
		return UserLoader.getConnectedUser().superadmin;
	}

	public boolean superadminMode() {
		return superadmin() && UserLoader.getConnectedUser().superadminMode;
	}

	public boolean adminInterfaceAccess() {
		return UserLoader.getConnectedUser().superadmin || userAdmin() || categoryAdmin() || bookmarkAdmin()
				|| themeAdmin() || statisticsAdmin() || teaserAdmin();
	}

	public boolean userAdmin() {
		return UserLoader.getConnectedUser().superadmin || UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_USER);
	}

	public boolean userEditAdmin(Long id) {
		if (UserLoader.getConnectedUser().superadmin) {
			return true;
		}
		User user = User.findById(id);
		return UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_USER) && !user.superadmin;
	}

	public boolean teaserAdmin() {
		return UserLoader.getConnectedUser().superadmin
				|| UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_TEASER);
	}

	public boolean categoryAdmin() {
		return UserLoader.getConnectedUser().superadmin
				|| UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_CATEGORIES);
	}

	public boolean bookmarkAdmin() {
		return UserLoader.getConnectedUser().superadmin
				|| UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_BOOKMARKS);
	}

	public boolean themeAdmin() {
		return UserLoader.getConnectedUser().superadmin
				|| UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_THEME);
	}

	public boolean statisticsAdmin() {
		return UserLoader.getConnectedUser().superadmin
				|| UserLoader.getConnectedUser().hasGrant(UserGrant.ADMIN_STATISTICS);
	}

	public boolean accessApp(App app) {
		return (accessSender(app.sender.id) && app.active) || editSender(app.sender.id);
	}

	public boolean accessApp(Long id) {
		return accessApp((App) App.findById(id));
	}

	public boolean readPost(Long id) {
		Post post = Post.findById(id);
		return readPost(post);
	}

	public boolean readPost(Post post) {
		if (post.wall.sender instanceof User) {
			User connectedUser = UserLoader.getConnectedUser();
			if (connectedUser.external && !connectedUser.id.equals(post.author.id)) {
				return false;
			} else {
				return (null != User.findById(post.wall.sender.id)) ? true : false;
			}
		} else {
			return accessSender(post.wall.sender);
		}
	}

	public boolean accessPost(Long id) {
		Post post = Post.findById(id);
		return accessPost(post);
	}

	public boolean accessPost(Post post) {
		return accessSender(post.wall.sender);
	}

	public boolean createPost(Long id) {
		Wall wall = Wall.findById(id);
		if (wall.restrictPosts) {
			return editSender(wall.sender.id);
		}
		return accessSender(wall.sender.id);
	}

	public boolean collaboratePost(Long id) {
		Post post = Post.findById(id);
		if (post.wall.restrictCollaboration) {
			return editSender(post.wall.sender.id);
		}
		return accessPost(id);
	}

	public boolean postSticky(Long id) {
		Sender sender = Sender.findById(id);
		return !(sender instanceof User) && editSender(id) && sender != UserLoader.getConnectedUser();
	}

	public boolean postAsOther(Long id) {
		Sender sender = Sender.findById(id);
		return !(sender instanceof User) && editSender(id);
	}

	public boolean listPage(Long id) {
		Page page = Page.findById(id);
		return listPage(page);
	}

	public boolean listPage(Page page) {
		return editPage(page) || page.visibility != PageVisibility.PRIVATE
				|| page.isMember(UserLoader.getConnectedUser());
	}

	public boolean accessPage(Long id) {
		Page page = Page.findById(id);
		return accessPage(page);
	}

	public boolean accessPage(Page page) {
		return !UserLoader.getConnectedUser().external
				&& (editPage(page) || page.visibility == PageVisibility.PUBLIC || page.isMember(UserLoader
						.getConnectedUser()));
	}

	public boolean createPage() {
		return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PAGE);
	}

	public boolean createPage(PageVisibility visibility) {
		if (visibility == PageVisibility.PUBLIC) {
			return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_PAGE);
		}
		return createPage();
	}

	public boolean createForceFollowPage() {
		return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_FORCE_FOLLOW_PAGE);
	}

	public boolean editPage(Long id) {
		Page page = Page.findById(id);
		if (page == null) {
			return false;
		} else {
			return editPage(page);
		}
	}

	public boolean editPage(Page page) {
		return page.admins.contains(UserLoader.getConnectedUser());
	}

	public boolean editPage(Long id, PageVisibility vis) {
		Page page = Page.findById(id);
		if (vis == PageVisibility.PUBLIC && page.visibility != vis) {
			return editPage(id) && UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_PAGE);
		}

		return editPage(id);
	}

	public boolean deletePage(Long id) {
		return editPage(id) && UserLoader.getConnectedUser().hasGrant(UserGrant.DELETE_PAGE);
	}

	public boolean listEvent(Long id) {
		Event event = Event.findById(id);
		return listEvent(event);
	}

	public boolean listEvent(Event event) {
		if (event == null) {
			return false;
		}

		if (accessEvent(event)) {
			return true;
		}

		// external users can only see where member of space
		if (UserLoader.getConnectedUser().external && (event.calendar == null || !listSender(event.calendar.sender))) {
			return false;
		}

		// check access to calendar
		if (event.calendar != null && event.calendar.active && !listSender(event.calendar.sender)) {
			return false;
		}

		return (event.calendar == null && event.visibility == EventVisibility.CLOSED);
	}

	public boolean accessEvent(Long id) {
		Event event = Event.findById(id);
		if (event == null) {
			return false;
		} else {
			return accessEvent(event);
		}
	}

	public boolean accessEvent(Event event) {
		// external users can only see where member of space
		if (UserLoader.getConnectedUser().external && (event.calendar == null || !listSender(event.calendar.sender.id))) {
			return false;
		}

		return editEvent(event) || event.isInvited(UserLoader.getConnectedUser())
				|| (event.calendar == null && event.visibility == EventVisibility.PUBLIC)
				|| (event.calendar != null && event.calendar.active && accessSender(event.calendar.sender.id));
	}

	public boolean createEvent() {
		return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_EVENT);
	}

	public boolean createEvent(EventVisibility visibility) {
		if (visibility == EventVisibility.PUBLIC) {
			return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_EVENT);
		}
		return createEvent();
	}

	public boolean createEvent(Long id) {
		CalendarApp app = CalendarApp.findById(id);
		return editSender(app.sender.id) || (!app.onlyAdminsEdit && accessSender(app.sender.id));
	}

	public boolean editEvent(Long id) {
		Event event = Event.findById(id);
		if (event == null) {
			return false;
		} else {
			return editEvent(event);
		}
	}

	public boolean editEvent(Event event) {
		if (event == null) {
			return false;
		} else {
			return event.admins.contains(UserLoader.getConnectedUser())
					|| (event.calendar != null && editSender(event.calendar.sender.id));
		}
	}

	public boolean editEvent(Long id, EventVisibility vis) {
		Event event = Event.findById(id);
		if (vis == EventVisibility.PUBLIC && event.visibility != vis) {
			return editEvent(event) && UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_EVENT);
		}

		return editEvent(event);
	}

	public boolean deleteEvent(Long id) {
		Event event = Event.findById(id);
		if (event.calendar != null) {
			return editEvent(event);
		}
		return editEvent(event) && UserLoader.getConnectedUser().hasGrant(UserGrant.DELETE_EVENT);
	}

	public boolean respondToEvent(Long id) {
		Event event = Event.findById(id);
		return event.allowGuestInvite && accessEvent(id);
	}

	public boolean accessEventSeries(Long id) {
		EventSeries series = EventSeries.findById(id);

		// external users can only see where member of space
		if (UserLoader.getConnectedUser().external) {
			return (series.calendar != null && accessSender(series.calendar.sender.id));
		}

		return (series.calendar == null)
				|| (series.calendar != null && series.calendar.active && accessSender(series.calendar.sender.id));
	}

	public boolean createConversation(Long targetUserId) {
		User currentUser = UserLoader.getConnectedUser();
		if (!currentUser.external) {
			return true;
		}
		User targetUser = User.findById(targetUserId);
		if (targetUser == null) {
			return false;
		}

		return currentUser.isInSameWorkspaceWith(targetUser);
	}

	public boolean listUser(Long id) {
		User user = User.findById(id);
		return listUser(user);
	}

	public boolean listUser(User user) {
		User currentUser = UserLoader.getConnectedUser();
		if (currentUser.external) {
			return !user.deleted && user.active && currentUser.isInSameWorkspaceWith(user);
		}
		return !user.deleted && user.active;
	}

	public boolean accessUser(Long id) {
		User user = User.findById(id);
		return accessUser(user);
	}

	public boolean accessUser(User user) {
		User connectedUser = UserLoader.getConnectedUser();
		if (connectedUser.external && !connectedUser.id.equals(user.id)) {
			return false;
		}

		return !user.deleted && user.active;
	}

	public boolean accessUserWall(Long id) {
		User user = User.findById(id);
		return accessUser(id) && !user.external;
	}

	public boolean editUserProfile(Long id) {
		return UserLoader.getConnectedUser().id.equals(id);
	}

	public boolean accessSender(Sender sender) {
		if (sender instanceof Application) {
			return !UserLoader.getConnectedUser().external;
		} else if (sender instanceof User) {
			return accessUser((User) sender);
		} else if (sender instanceof Page) {
			return accessPage((Page) sender);
		} else if (sender instanceof Event) {
			return accessEvent((Event) sender);
		} else if (sender instanceof Workspace) {
			return accessWorkspace((Workspace) sender);
		} else if (sender instanceof Conversation) {
			return accessConversation((Conversation) sender);
		}
		return false;
	}

	public boolean accessSender(Long id) {
		final Sender sender = Sender.findById(id);

		if (sender instanceof Application) {
			return !UserLoader.getConnectedUser().external;
		} else if (sender instanceof User) {
			return accessUser(id);
		} else if (sender instanceof Page) {
			return accessPage(id);
		} else if (sender instanceof Event) {
			return accessEvent(id);
		} else if (sender instanceof Workspace) {
			return accessWorkspace(id);
		} else if (sender instanceof Conversation) {
			return accessConversation(id);
		}
		return false;
	}

	public boolean listSender(Long id) {
		Sender sender = Sender.findById(id);
		return listSender(sender);
	}

	public boolean listSender(Sender sender) {
		if (sender instanceof User) {
			return listUser((User) sender);
		} else if (sender instanceof Page) {
			return listPage((Page) sender);
		} else if (sender instanceof Event) {
			return listEvent((Event) sender);
		} else if (sender instanceof Workspace) {
			return listWorkspace((Workspace) sender);
		} else if (sender instanceof Conversation) {
			return accessConversation((Conversation) sender);
		}
		return false;
	}

	public boolean editSender(Long id) {
		Sender sender = Sender.findById(id);
		return editSender(sender);
	}

	public boolean editSender(Sender sender) {
		if (sender instanceof User) {
			return editUserProfile(sender.id);
		} else if (sender instanceof Page) {
			return editPage((Page) sender);
		} else if (sender instanceof Event) {
			return editEvent((Event) sender);
		} else if (sender instanceof Workspace) {
			return editWorkspace((Workspace) sender);
		}
		return false;
	}

	public boolean accessMediaRoom(Long id) {
		MediaRoom collection = MediaRoom.findById(id);
		return accessMediaRoom(collection);
	}

	public boolean accessMediaRoom(MediaRoom collection) {
		return accessSender(collection.sender);
	}

	public boolean editMediaRoom(Long id) {
		MediaRoom collection = MediaRoom.findById(id);
		if (collection.allowGuestUpload) {
			return accessMediaRoom(id);
		}
		return editSender(collection.sender.id);
	}

	public boolean listWorkspace(Long id) {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.WORKSPACES)) {
			return false;
		}
		Workspace workspace = Workspace.findById(id);
		return listWorkspace(workspace);
	}

	public boolean listWorkspace(Workspace workspace) {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.WORKSPACES)) {
			return false;
		}

		// externals only see workspaces where member
		User user = UserLoader.getConnectedUser();
		if (user.external) {
			return workspace.isMember(user);
		}

		return workspace.visibility != WorkspaceVisibility.PRIVATE || workspace.isMember(user);
	}

	public boolean joinWorkspace(Long id) {
		Workspace workspace = Workspace.findById(id);
		return workspace.visibility == WorkspaceVisibility.PUBLIC;
	}

	public boolean leaveWorkspace(Long id) {
		Workspace workspace = Workspace.findById(id);
		return workspace.isMember(UserLoader.getConnectedUser());
	}

	public boolean accessWorkspace(Long id) {
		Workspace workspace = Workspace.findById(id);
		return accessWorkspace(workspace);
	}

	public boolean accessWorkspace(Workspace workspace) {
		if (!Settings.findApplicationSettings().getBoolean(ApplicationSettings.WORKSPACES)) {
			return false;
		}
		return workspace != null
				&& (workspace.isMember(UserLoader.getConnectedUser()) || (!UserLoader.getConnectedUser().external && workspace.visibility == WorkspaceVisibility.PUBLIC));
	}

	public boolean createWorkspace() {
		User user = UserLoader.getConnectedUser();
		return !user.external && user.hasGrant(UserGrant.CREATE_WORKSPACE);
	}

	public boolean createWorkspace(WorkspaceVisibility visibility) {
		if (visibility == WorkspaceVisibility.PUBLIC) {
			return UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_WORKSPACE);
		}
		return createWorkspace();
	}

	public boolean editWorkspace(Long id) {
		Workspace workspace = Workspace.findById(id);
		return editWorkspace(workspace);
	}

	public boolean editWorkspace(Workspace workspace) {
		User user = UserLoader.getConnectedUser();
		return !user.external && workspace.isAdmin(user);
	}

	public boolean editWorkspace(Long id, WorkspaceVisibility vis) {
		Workspace workspace = Workspace.findById(id);
		if (vis == WorkspaceVisibility.PUBLIC && workspace.visibility != vis) {
			return editWorkspace(id) && UserLoader.getConnectedUser().hasGrant(UserGrant.CREATE_PUBLIC_WORKSPACE);
		}

		return editWorkspace(id);
	}

	public boolean deleteWorkspace(Long id) {
		return editWorkspace(id) && UserLoader.getConnectedUser().hasGrant(UserGrant.DELETE_WORKSPACE);
	}

	public boolean inviteExternalsToWorkspace(Long id) {
		return editWorkspace(id) && UserLoader.getConnectedUser().hasGrant(UserGrant.INVITE_EXTERNALS_TO_WORKSPACE);
	}

	public boolean deletePost(Long id) {
		Post post = Post.findById(id);
		return post.author == UserLoader.getConnectedUser() || editSender(post.wall.sender.id);
	}

	public boolean like(Likable likable) {
		return likable.checkLikePermission(UserLoader.getConnectedUser());
	}

	public boolean listLikes(Likable likable) {
		return likable.checkLikePermission(UserLoader.getConnectedUser());
	}

	public boolean createComment(Commentable commentable) {
		return commentable.checkCreateCommentPermission(UserLoader.getConnectedUser());
	}

	public boolean deleteComment(Long id, Commentable commentable) {
		Comment comment = Comment.findById(id);

		// authors can always delete their own comments
		if (comment.author == UserLoader.getConnectedUser()) {
			return true;
		}

		return commentable.checkDeleteCommentPermission(comment, UserLoader.getConnectedUser());
	}

	public boolean accessConversation(Long id) {
		Conversation conversation = Conversation.findById(id);
		return accessConversation(conversation);
	}

	public boolean accessConversation(Conversation conversation) {
		ConversationMember cm = conversation.getMemberObject(UserLoader.getConnectedUser());
		return cm != null && !cm.isDeleted();
	}

	public boolean accessConversationMember(Long id) {
		ConversationMember cm = ConversationMember.findById(id);
		return cm != null && !cm.isDeleted() && cm.user == UserLoader.getConnectedUser();
	}

	public boolean share(Sharable sharable) {
		return sharable.isSharable() && !UserLoader.getConnectedUser().external;
	}

	public boolean share(Long id, String clazz) {
		return share(Collaboration.loadSharable(id, clazz));
	}

	public boolean follow(Sender sender) {
		if (sender instanceof User) {
			return !Settings.findApplicationSettings().getBoolean("autofollow")
					&& sender != UserLoader.getConnectedUser() && !UserLoader.getConnectedUser().external
					&& !sender.external;
		}
		return true;
	}

	public boolean follow(Long id) {
		return follow((Sender) Sender.findById(id));
	}

	public boolean createContainerItem(Long containerId) {
		Container container = Container.findById(containerId);
		return editSender(container.sender.id) || container.hasPermission(ContainerPermission.Create);
	}

	public boolean readContainerItem(Long itemId) {
		Item item = Item.findById(itemId);
		return readContainerItem(item);
	}

	public boolean readContainerItem(Item item) {
		return editSender(item.container.sender)
				|| (accessSender(item.container.sender) && item.canBeReadBy(UserLoader.getConnectedUser()));
	}

	public boolean editContainerItem(Long itemId) {
		Item item = Item.findById(itemId);
		return editSender(item.container.sender.id) || item.canBeUpdatedBy(UserLoader.getConnectedUser());
	}

	public boolean deleteContainerItem(Long itemId) {
		Item item = Item.findById(itemId);
		return editSender(item.container.sender.id) || item.canBeDeletedBy(UserLoader.getConnectedUser());
	}

	public boolean readAllContainerItems(Long containerId) {
		Container container = Container.findById(containerId);
		return editSender(container.sender.id) || container.allCanBeReadBy(UserLoader.getConnectedUser());
	}

	public boolean readContainerItems(Long containerId) {
		Container container = Container.findById(containerId);
		return editSender(container.sender.id) || container.canBeReadBy(UserLoader.getConnectedUser());
	}

	/**
	 * A user is allowed to delete bookmarks if he or she is the owner of the bookmark. Global bookmarks can be only
	 * deleted by admins.
	 */
	public boolean deleteBookmark(Long id) {
		Bookmark bookmark = Bookmark.findById(id);

		/* global booomarks */
		if (bookmark.sender == null) {
			return bookmarkAdmin();
		}

		/* personal bookmarks */
		User user = UserLoader.getConnectedUser();
		return (bookmark.sender.id == user.id);
	}

	public boolean editPushDevice(Long id) {
		UserPushDevice device = UserPushDevice.findById(id);
		return device.user == UserLoader.getConnectedUser();
	}

	/**
	 * Permission checks wether the current {@link User} is allowed to edit a certain {@link UserDevice}
	 *
	 * @param userDevice the {@link UserDevice}
	 * @return boolean
	 */
	public boolean editOAuth2Device(UserDevice userDevice) {
		final User user = UserLoader.getConnectedUser();
		return (user.superadmin || user == UserLoader.getConnectedUser());
	}

	/**
	 * Permission is used to check if a user is allowed to create a call and is granted if target user and current user
	 * are in the same workspace (if one of them is external)
	 *
	 * @param targetUser
	 *            the target user
	 * @return true/false
	 */
	public boolean createWebRTCCall(User targetUser) {
		if (targetUser == null) {
			return false;
		}

		User currentUser = UserLoader.getConnectedUser(false);
		return (currentUser != null)
				&& !((currentUser.external || targetUser.external) && !currentUser.isInSameWorkspaceWith(targetUser));
	}

	/**
	 * Permission is used to check if a user is allowed to create a call and is granted if target user and current user
	 * are in the same workspace (if one of them is external)
	 *
	 * @param id
	 *            target user ID
	 * @return true/false
	 */
	public boolean createWebRTCCall(Long id) {
		User u = User.findById(id);
		return createWebRTCCall(u);
	}

	/**
	 * Permission is used to check if a call can be established and is granted if: - Target user and current user are in
	 * the same workspace (if one of them is external) - The target user is online
	 *
	 * @param id
	 *            target user ID
	 * @return true/false
	 */
	public boolean accessWebRTCCall(Long id) {
		User targetUser = User.findById(id);
		return createWebRTCCall(targetUser)
				&& ((targetUser.getOnlineStatus().status == UserOnlineStatus.OnlineStatus.ONLINE) || (targetUser
						.getOnlineStatus().status == UserOnlineStatus.OnlineStatus.BUSY));
	}

	public boolean isWebRTCCallable(Long id) {
		User targetUser = User.findById(id);
		return (targetUser != null) && createWebRTCCall(targetUser)
				&& (targetUser.getOnlineStatus().status == UserOnlineStatus.OnlineStatus.ONLINE);
	}

	// ------------------------------------------------------------------------------------------------------------------------
	// APP PERMISSIONS
	// ------------------------------------------------------------------------------------------------------------------------

	public boolean createForumTopic(Long id) {
		return accessApp(id);
	}

	/**
	 * Permission is granted if the user is admin of the parent page / workspace or if he or she is the author of the
	 * topic, and the topic has now replies yet.
	 * <p/>
	 * This permission also allows to delete a topic!
	 */
	public boolean editForumTopic(Long id) {
		ForumAppTopic topic = ForumAppTopic.findById(id);
		return (Security.check(Permission.EDIT_SENDER, topic.app.sender.id) || topic.author.equals(UserLoader
				.getConnectedUser()) && topic.getPosts().size() < 2);
	}

	/**
	 * Permission is granted if the user can access the app and the topic is not closed.
	 */
	public boolean addForumPost(Long topicId) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		return !topic.closed && Security.check(Permission.ACCESS_APP, topic.app.id);
	}

	/**
	 * Permission is granted if the user is allowed to delete the post and the topic is not closed.
	 */
	public boolean deleteForumPost(Long postId, Long topicId) {
		ForumAppTopic topic = ForumAppTopic.findById(topicId);
		return !topic.closed && Security.check(Permission.DELETE_POST, postId);
	}

	public boolean editWiki(Long id) {
		WikiApp app = WikiApp.findById(id);
		if (app.onlyAdminsCanEdit) {
			return editSender(app.sender.id);
		}
		return accessApp(app);
	}

	public boolean deleteWikiArticle(Long id) {
		return deleteWikiArticle((WikiAppArticle) WikiAppArticle.findById(id));
	}

	public boolean deleteWikiArticle(WikiAppArticle article) {
		if (article.app.onlyAdminsCanDelete) {
			return editSender(article.app.sender.id);
		}
		return editWiki(article.app.id);
	}

	public boolean commentWikiArticle(WikiAppArticle article) {
		return accessApp(article.app) && article.app.usersCanComment;
	}

	public boolean deleteCommentWikiArticle(WikiAppArticle article) {
		return Security.check(Permission.EDIT_SENDER, article.app.sender.id);
	}

	public boolean editBlog(Long id) {
		BlogApp app = BlogApp.findById(id);
		return Security.check(Permission.EDIT_SENDER, app.sender.id)
				|| (app.allowGuestAuthor && Security.check(Permission.ACCESS_SENDER, app.sender.id));
	}

	public boolean accessBlogArticle(Long id) {
		return accessBlogArticle((BlogAppArticle) BlogAppArticle.findById(id));
	}

	public boolean accessBlogArticle(BlogAppArticle article) {
		return editBlogArticle(article) || article.isPublished();
	}

	public boolean likeBlogArticle(BlogAppArticle article) {
		return accessBlogArticle(article);
	}

	public boolean commentBlogArticle(BlogAppArticle article) {
		return accessBlogArticle(article) && article.app.usersCanComment && article.commentable;
	}

	public boolean likeMedia(Long id) {
		return likeMedia((Media) Media.findById(id));
	}

	public boolean likeMedia(Media media) {
		return accessMediaRoom(media.collection.room.getId());
	}

	public boolean commentMedia(Long id) {
		return commentMedia((Media) Media.findById(id));
	}

	public boolean commentMedia(Media media) {
		return accessMediaRoom(media.collection.room.getId());
	}

	public boolean editBlogArticle(Long id) {
		return editBlogArticle((BlogAppArticle) BlogAppArticle.findById(id));
	}

	public boolean editBlogArticle(BlogAppArticle article) {
		return Security.check(Permission.EDIT_SENDER, article.app.sender.id)
				|| (article.app.allowGuestAuthor && article.author == UserLoader.getConnectedUser());
	}

	public boolean editTasks(Long id) {
		TasksApp app = TasksApp.findById(id);
		return Security.check(Permission.ACCESS_SENDER, app.sender.id);
	}

	public boolean editTask(Long id) {
		TasksAppTask task = TasksAppTask.findById(id);
		return Security.check(Permission.ACCESS_SENDER, task.app.sender.id);
	}

	public boolean accessFile(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		File file = app.getFile(uid);
		return accessFile(app, file);
	}

	public boolean accessFile(FilesApp app, File file) {
		return file != null && Security.check(Permission.ACCESS_APP, app)
				&& file.checkAccessPermission(UserLoader.getConnectedUser());
	}

	public boolean editFile(Long id, String uid) {
		FilesApp app = FilesApp.findById(id);
		if (app.isReadOnly()) {
			return false;
		}

		File file = app.getFile(uid);
		if (!file.checkModifyPermission(UserLoader.getConnectedUser())) {
			return false;
		}

		if (app.adminsOnly) {
			return Security.check(Permission.EDIT_SENDER, app.sender.id);
		}

		User lock = file.getLock();
		if (lock != null && lock != UserLoader.getConnectedUser()) {
			return false;
		}

		return Security.check(Permission.ACCESS_APP, app.id);
	}

	public boolean addSocialLink(Long id) {
		SocialLinklistApp app = SocialLinklistApp.findById(id);
		if (app == null) {
			return false;
		}
		System.out.println(app.usersCanAddLinks);
		User user = UserLoader.getConnectedUser();
		return (accessApp(app) && app.usersCanAddLinks) || app.sender.getAdmins().contains(user);
	}

	public boolean editSocialLink(Long id, Long linkId) {
		SocialLinklistApp app = SocialLinklistApp.findById(id);
		if (app == null) {
			return false;
		}
		SocialLink link = SocialLink.findById(linkId);
		if (link == null) {
			return false;
		}
		User user = UserLoader.getConnectedUser();
		return (accessApp(app) && app.usersCanEditLinks) || app.sender.getAdmins().contains(user)
				|| (accessApp(app) && link.author == user);
	}

	public boolean editAllSocialLinks(Long id) {
		SocialLinklistApp app = SocialLinklistApp.findById(id);
		if (app == null) {
			return false;
		}
		User user = UserLoader.getConnectedUser();
		return accessApp(app) && (app.sender.getAdmins().contains(user) || app.usersCanEditLinks);
	}

	public boolean canModerate(final Sender sender, final User user, final Long appId) {
		final InnoApp app = InnoApp.findById(appId);
		if (sender.getAdmins().contains(user)) {
			return true;
		} else if (app.moderators.contains(user)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if an idea can be edited by the current user. To be editable an idea must not contain any likes, comments
	 * and needs the status NEW. A user needs to be a moderator in order to edit an idea.
	 *
	 * @param sender
	 * @param user
	 * @param appId
	 * @param ideaId
	 * @return
	 */
	public boolean editIdea(final Sender sender, final User user, final Long appId, final Long ideaId) {
		final Idea idea = Idea.findById(ideaId);

		if (!this.canModerate(sender, user, appId) && !(idea.creator.id == user.id)) {
			return false;
		}
		if (idea == null) {
			return false;
		}
		if (idea.comments.size() > 0) {
			return false;
		}
		if (!idea.getStatus().equals(Status.NEW)) {
			return false;
		}

		return true;
	}

	/**
	 * Check wether an user might access (like, comment, read) an idea Allowed are: creator, sender member, sender
	 * admins, sender follower
	 *
	 * @param sender
	 * @param user
	 * @param appId
	 * @param ideaId
	 * @return
	 */
	public boolean accessIdea(final Sender sender, final User user, final Long appId, final Long ideaId) {
		final Idea idea = Idea.findById(ideaId);

		if (this.canModerate(sender, user, appId)) {
			return true;

		} else if (user.id == idea.creator.id) {
			return true;

		} else if (sender instanceof Workspace) {
			final Workspace workspace = (Workspace) sender;
			if (workspace.getAdmins().contains(user)) {
				return true;
			} else if (workspace.followers.contains(user)) {
				return true;
			} else {
				for (WorkspaceMember wm : workspace.members) {
					if (wm.user == user) {
						return true;
					}
				}
			}

		} else if (sender instanceof Page) {
			final Page page = (Page) sender;
			if (page.getAdmins().contains(user)) {
				return true;
			} else if (page.members.contains(user)) {
				return true;
			} else if (page.followers.contains(user)) {
				return true;
			}
		}

		return false;
	}

	public boolean voteOnPoll(App app) {
		return accessApp(app);
	}

	public boolean submitForm(App app) {
		return accessApp(app);
	}
}

package acl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import acl.UserGrant.GrantDeclaring;

/**
 * Holds the list of user role grants (permissions). You can dynamically load other grants by using the
 * {@link GrantDeclaring} and {@link Grant} annotations.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
@GrantDeclaring
public class UserGrant {

	/**
	 * Annotate your user grant string fields with this annotation to load them. The fields need to be static and of
	 * type string. Groups are sorted by name in the form. Classes that declare grants with this annotation need to be
	 * annotated with {@link GrantDeclaring}.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public @interface Grant {
		String group();
	}

	/**
	 * Annotate the class that declares user grants with {@link Grant}.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public @interface GrantDeclaring {
	}

	@Grant(group = "pages")
	public static final String CREATE_PAGE = "createPage";
	@Grant(group = "pages")
	public static final String CREATE_PUBLIC_PAGE = "createPublicPage";
	@Grant(group = "pages")
	public static final String DELETE_PAGE = "deletePage";
	@Grant(group = "pages")
	public static final String CREATE_FORCE_FOLLOW_PAGE = "createForceFollowPage";

	@Grant(group = "events")
	public static final String CREATE_EVENT = "createEvent";
	@Grant(group = "events")
	public static final String CREATE_PUBLIC_EVENT = "createPublicEvent";
	@Grant(group = "events")
	public static final String DELETE_EVENT = "deleteEvent";

	@Grant(group = "workspaces")
	public static final String CREATE_WORKSPACE = "createWorkspace";
	@Grant(group = "workspaces")
	public static final String CREATE_PUBLIC_WORKSPACE = "createPublicWorkspace";
	@Grant(group = "workspaces")
	public static final String DELETE_WORKSPACE = "deleteWorkspace";
	@Grant(group = "workspaces")
	public static final String INVITE_EXTERNALS_TO_WORKSPACE = "inviteExternalsToWorkspace";

	@Grant(group = "admin")
	public static final String ADMIN_USER = "userAdmin";
	@Grant(group = "admin")
	public static final String ADMIN_TEASER = "teaserAdmin";
	@Grant(group = "admin")
	public static final String ADMIN_CATEGORIES = "categoryAdmin";
	@Grant(group = "admin")
	public static final String ADMIN_BOOKMARKS = "bookmarkAdmin";
	@Grant(group = "admin")
	public static final String ADMIN_THEME = "themeAdmin";
	@Grant(group = "admin")
	public static final String ADMIN_STATISTICS = "statisticsAdmin";

	private static Collection<UserGrantGroup> allGrantGroups;

	public static Collection<UserGrantGroup> groups() {
		if (allGrantGroups == null) {
			Map<String, UserGrantGroup> r = new TreeMap<>();

			for (ApplicationClass ac : Play.classes.getAnnotatedClasses(GrantDeclaring.class)) {
				try {
					for (Field field : ac.javaClass.getDeclaredFields()) {
						if (field.getType() != String.class) {
							Logger.error(
									"[UserGrant] Found user grant in [%s] that is not of required type String. Skipping.",
									ac.javaClass);
							continue;
						}

						if (!Modifier.isStatic(field.getModifiers())) {
							Logger.error("[UserGrant] Found user grant in [%s] that is not static. Skipping.",
									ac.javaClass);
							continue;
						}

						if (field.isAnnotationPresent(Grant.class)) {
							Grant grant = field.getAnnotation(Grant.class);
							if (!r.containsKey(grant.group())) {
								r.put(grant.group(), new UserGrantGroup(grant.group()));
							}
							r.get(grant.group()).grants.add((String) field.get(null));
						}
					}
				} catch (Exception e) {
					Logger.error(e, "[UserGrant] Error loading user grants");
				}
			}

			allGrantGroups = r.values();
		}
		return allGrantGroups;
	}

	public static List<String> allSimple() {
		List<String> r = new ArrayList<String>();
		for (UserGrantGroup group : groups()) {
			r.addAll(group.grants);
		}
		return r;
	}

	private UserGrant() {
		// hide
	}

	public static class UserGrantGroup {

		public String name;
		public List<String> grants = new ArrayList<String>();

		public UserGrantGroup(String name, String... grants) {
			this.name = name;
			for (String grant : grants) {
				this.grants.add(grant);
			}
		}
	}
}

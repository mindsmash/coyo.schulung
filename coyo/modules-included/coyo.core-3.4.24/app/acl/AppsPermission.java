package acl;

public class AppsPermission {

	private AppsPermission() {
		// hidden
	}

	public static final String ACCESS_FILE = "accessFile";
	public static final String EDIT_FILE = "editFile";

	public static final String EDIT_WIKI = "editWiki";
	public static final String DELETE_WIKI_ARTICLE = "deleteWikiArticle";
	public static final String COMMENT_WIKI_ARTICLE = "commentWikiArticle";
	public static final String DELETE_COMMENT_WIKI_ARTICLE = "deleteCommentWikiArticle";

	public static final String EDIT_BLOG = "editBlog";
	public static final String EDIT_BLOG_ARTICLE = "editBlogArticle";
	public static final String ACCESS_BLOG_ARTICLE = "accessBlogArticle";
	public static final String LIKE_BLOG_ARTICLE = "likeBlogArticle";
	public static final String COMMENT_BLOG_ARTICLE = "commentBlogArticle";

	public static final String EDIT_TASK = "editTask";
	public static final String EDIT_TASKS = "editTasks";

	public static final String CREATE_FORUM_TOPIC = "createForumTopic";
	public static final String EDIT_FORUM_TOPIC = "editForumTopic";
	public static final String DELETE_FORUM_POST = "deleteForumPost";
	public static final String ADD_FORUM_POST = "addForumPost";

	public static final String MODERATE_IDEA = "canModerate";
	public static final String EDIT_IDEA = "editIdea";
	public static final String ACCESS_IDEA = "accessIdea";

	public static final String POLL_VOTE = "voteOnPoll";

	public static final String FORM_SUBMIT = "submitForm";

	public static final String EDIT_SOCIAL_LINK = "editSocialLink";
	public static final String ADD_SOCIAL_LINK = "addSocialLink";
	public static final String EDIT_ALL_SOCIAL_LINKS = "editAllSocialLinks";

}

package acl;

/**
 * 
 * Defines all standard Permissions, that are used to access the {@link PermissionChecker} functions.
 * 
 * @author mindsmash GmbH
 */
public class Permission {

	private Permission() {
		// hidden
	}

	public static final String ADMINISTRATION = "superadmin";
	public static final String SUPERADMIN_MODE = "superadminMode";

	public static final String ADMIN_INTERFACE_ACCESS = "adminInterfaceAccess";

	public static final String ADMIN_USER = "userAdmin";
	public static final String ADMIN_TEASER = "teaserAdmin";
	
	/**
	 * This permission defines whether a user can edit another user in admin section
	 */
	public static final String ADMIN_EDIT_USER = "userEditAdmin";
	public static final String ADMIN_CATEGORIES = "categoryAdmin";
	public static final String ADMIN_BOOKMARKS = "bookmarkAdmin";
	public static final String ADMIN_THEME = "themeAdmin";
	public static final String ADMIN_STATISTICS = "statisticsAdmin";

	public static final String ACCESS_APP = "accessApp";

	public static final String ACCESS_POST = "accessPost";
	public static final String READ_POST = "readPost";

	public static final String CREATE_POST = "createPost";
	// TODO : check in controller too, currently only in template
	public static final String COLLABORATE_POST = "collaboratePost";
	public static final String DELETE_POST = "deletePost";
	public static final String POST_STICKY = "postSticky";
	public static final String POST_AS_OTHER = "postAsOther";

	public static final String CREATE_COMMENT = "createComment";
	public static final String DELETE_COMMENT = "deleteComment";

	public static final String LIKE_MEDIA = "likeMedia";
	public static final String COMMENT_MEDIA = "commentMedia";

	public static final String LIKE = "like";
	public static final String LIST_LIKES = "listLikes";

	public static final String LIST_PAGE = "listPage";
	public static final String ACCESS_PAGE = "accessPage";
	public static final String CREATE_PAGE = "createPage";
	public static final String CREATE_FORCE_FOLLOW_PAGE = "createForceFollowPage";
	public static final String EDIT_PAGE = "editPage";
	public static final String DELETE_PAGE = "deletePage";

	public static final String LIST_EVENT = "listEvent";
	public static final String ACCESS_EVENT = "accessEvent";
	public static final String CREATE_EVENT = "createEvent";
	public static final String EDIT_EVENT = "editEvent";
	public static final String DELETE_EVENT = "deleteEvent";
	public static final String RESPOND_TO_EVENT = "respondToEvent";
	public static final String ACCESS_EVENT_SERIES = "accessEventSeries";

	public static final String LIST_USER = "listUser";
	public static final String ACCESS_USER = "accessUser";
	public static final String ACCESS_USER_WALL = "accessUserWall";
	public static final String EDIT_USER_PROFILE = "editUserProfile";

	public static final String ACCESS_USER_NOTIFICATIONS = EDIT_USER_PROFILE;

	public static final String LIST_SENDER = "listSender";
	public static final String ACCESS_SENDER = "accessSender";
	public static final String EDIT_SENDER = "editSender";

	public static final String ACCESS_MEDIA_ROOM = "accessMediaRoom";
	public static final String EDIT_MEDIA_ROOM = "editMediaRoom";

	public static final String LIST_WORKSPACE = "listWorkspace";
	public static final String ACCESS_WORKSPACE = "accessWorkspace";
	public static final String JOIN_WORKSPACE = "joinWorkspace";
	public static final String LEAVE_WORKSPACE = "leaveWorkspace";
	public static final String CREATE_WORKSPACE = "createWorkspace";
	public static final String EDIT_WORKSPACE = "editWorkspace";
	public static final String DELETE_WORKSPACE = "deleteWorkspace";
	public static final String INVITE_EXTERNALS_TO_WORKSPACE = "inviteExternalsToWorkspace";

	public static final String ACCESS_CONVERSATION = "accessConversation";
	public static final String ACCESS_CONVERSATION_MEMBER = "accessConversationMember";
	public static final String CREATE_CONVERSATION = "createConversation";

	public static final String SHARE = "share";
	public static final String FOLLOW = "follow";

	public static final String CREATE_CONTAINER_ITEM = "createContainerItem";
	public static final String READ_CONTAINER_ITEM = "readContainerItem";
	public static final String EDIT_CONTAINER_ITEM = "editContainerItem";
	public static final String DELETE_CONTAINER_ITEM = "deleteContainerItem";
	public static final String READ_CONTAINER_ITEMS = "readContainerItems";
	public static final String READ_ALL_CONTAINER_ITEMS = "readAllContainerItems";

	public static final String DELETE_BOOKMARK = "deleteBookmark";

	public static final String EDIT_PUSH_DEVICE = "editPushDevice";

	public static final String EDIT_OAUTH2_DEVICE = "editOAuth2Device";

	public static final String CREATE_WEBRTC_CALL = "createWebRTCCall";
	public static final String ACCESS_WEBRTC_CALL = "accessWebRTCCall";
	public static final String IS_WEBRTC_CALLABLE = "isWebRTCCallable";
}

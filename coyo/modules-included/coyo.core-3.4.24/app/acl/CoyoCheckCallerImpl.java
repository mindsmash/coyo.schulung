package acl;

import models.User;
import play.Logger;
import play.mvc.Http;
import security.CheckCallerImpl;
import session.UserLoader;
import util.Util;

public class CoyoCheckCallerImpl extends CheckCallerImpl {

	/**
	 * Coyo custom implementation of {@link CheckCallerImpl} introduces a request scope cache layer for all permission
	 * checks.
	 * Permission check results are stored in the current {@link play.mvc.Http.Request} arguments which are only valid
	 * during the scope of one action invocation. If no cached results are found the default implementation will be used.
	 *
	 * @param permission
	 * @param args
	 * @return
	 */
	@Override public boolean check(String permission, Object... args) {
		final String permissionKey = getPermissionName(permission) + Util.implode(args, ",");

		if (Http.Request.current() != null) {
			final Boolean cachedPermission = (Boolean) Http.Request.current().args.get(permissionKey);
			if (cachedPermission != null) {
				Logger.debug("[Security] Reusing cached permission [%s] -> [%s]", permissionKey, cachedPermission);
				return cachedPermission;
			}
		}

		final boolean result = super.check(permission, args);

		if (Http.Request.current() != null) {
			Http.Request.current().args.put(permissionKey, result);
		}

		return result;
	}

	private static String getPermissionName(String permission) {
		if (permission.contains("#")) {
			return permission.substring(permission.indexOf("#") + 1);
		}
		return permission;
	}
}

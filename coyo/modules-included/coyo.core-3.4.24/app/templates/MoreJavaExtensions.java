package templates;

import play.templates.BaseTemplate.RawData;
import play.templates.JavaExtensions;

import com.google.gson.Gson;

/**
 * More Java Extensions to use in templates.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class MoreJavaExtensions extends JavaExtensions {

	public static RawData toJson(Object source) {
		return new RawData(new Gson().toJson(source));
	}
}

package apps;

import models.app.App;
import models.app.PollApp;
import play.i18n.Messages;

@LoadApp
public class PollAppDescriptor extends AppDescriptor {

	public static final String KEY = "polls";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		return Messages.get("app." + getKey());
	}

	@Override
	public String getDescription() {
		return Messages.get("app." + getKey() + ".description");
	}

	@Override
	public Class<? extends App> getAppModel() {
		return PollApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-signal";
	}
}

package apps;

import models.User;
import models.app.App;
import models.app.FilesApp;
import models.app.LocalFilesApp;
import models.app.files.File;
import play.i18n.Messages;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@LoadApp
public class LocalFilesAppDescriptor extends AppDescriptor {

	public static final String KEY = "files";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		return Messages.get("app." + getKey());
	}

	@Override
	public String getDescription() {
		return Messages.get("app." + getKey() + ".description");
	}

	@Override
	public Class<? extends App> getAppModel() {
		return LocalFilesApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-file";
	}

	@LoadAppWidget(forApp = LocalFilesAppDescriptor.class)
	public static class LatestFilesWidget extends AppWidget<FilesApp> {
		@Override
		public String getKey() {
			return "latest";
		}

		@Override
		public String getName() {
			return Messages.get("app.files.widget.latest.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.files.widget.latest.description");
		}
		
		@Override
		public JsonElement getRenderData(FilesApp app, User connectedUser) {
			JsonObject r = new JsonObject();
			r.addProperty("showUser", !app.anonymous);
			
			JsonArray arr = new JsonArray();
			for (File file : app.getLatestFiles(5)) {
				JsonObject o = new JsonObject();
				o.addProperty("uid", file.getUid());
				o.addProperty("name", file.getName());
				o.addProperty("modified", file.getModified(connectedUser).getMillis());

				JsonObject creator = new JsonObject();
				User user = file.getCurrentVersion().getCreator();
				creator.addProperty("id", user.id);
				creator.addProperty("name", user.getFullName());
				o.add("creator", creator);

				arr.add(o);
			}
			
			r.add("files", arr);
			return r;
		}
		
		@Override
		public String getCss() {
			return "app.files.widget";
		}
	}
}

package apps;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Each {@link AppDescriptor} must be annotated with this Annotation to get
 * loaded when the Application is started. The {@link AppDescriptor} is
 * afterwards available in the {@link AppManager}.
 * 
 * @author mindsmash GmbH
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LoadApp {

}

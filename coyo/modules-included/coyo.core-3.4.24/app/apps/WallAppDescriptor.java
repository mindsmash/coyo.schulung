package apps;

import models.app.App;
import models.app.WallApp;

@LoadApp
public class WallAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "wall";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return WallApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-list";
	}
}

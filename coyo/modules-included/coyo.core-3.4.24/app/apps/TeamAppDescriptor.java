package apps;

import models.app.App;
import models.app.TeamApp;

@LoadApp
public class TeamAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "team";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return TeamApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-user";
	}
}

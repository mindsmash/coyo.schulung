package apps;

import models.Sender;
import models.app.App;
import models.app.ListApp;

@LoadApp
public class ListAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "list";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return ListApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-list";
	}
}

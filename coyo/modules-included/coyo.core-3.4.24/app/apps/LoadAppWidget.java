package apps;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Each {@link AppWidget} must be annotated with this Annotation to get loaded when the Application is started.
 * 
 * @author mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LoadAppWidget {

	/**
	 * The {@link AppDescriptor} class that this widget belongs to.
	 */
	Class<? extends AppDescriptor> forApp();
}

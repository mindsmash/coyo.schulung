package apps;

import play.i18n.Messages;

public abstract class AbstractAppDescriptor extends AppDescriptor {

	@Override
	public String getName() {
		return Messages.get("app." + getKey());
	}

	@Override
	public String getDescription() {
		return Messages.get("app." + getKey() + ".description");
	}

	@Override
	public String getIconClass() {
		return "icon-play-circle";
	}
}

package apps;



import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import injection.Inject;
import injection.InjectionSupport;
import models.User;
import models.app.App;
import models.app.SocialLink;
import models.app.SocialLinklistApp;
import models.app.dao.SocialLinklistAppDao;
import play.i18n.Messages;

@InjectionSupport
@LoadApp
public class SocialLinklistAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "socialLinklist";

	@Inject(configuration = "coyo.di.dao.sociallinklist", singleton = true, defaultClass = SocialLinklistAppDao.class)
	private static SocialLinklistAppDao dao;
	
	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return SocialLinklistApp.class;
	}

	public String getIconClass() {
		return "icon-list";
	}
	
	@LoadAppWidget(forApp = SocialLinklistAppDescriptor.class)
	public static class SocialLinklistWidget extends AppWidget<SocialLinklistApp> {
		@Override
		public String getKey() {
			return "links";
		}

		@Override
		public String getName() {
			return Messages.get("app.socialLinklist.widget.links.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.socialLinklist.widget.links.description");
		}
		
		@Override
		public JsonElement getRenderData(SocialLinklistApp app, User connectedUser) {
			JsonArray arr = new JsonArray();
			for (SocialLink link : dao.getTopFiveLinks(app)) {
				JsonObject o = new JsonObject();
				o.addProperty("id", link.id);
				o.addProperty("title", link.title);
				o.addProperty("url", link.linkUrl);
				o.addProperty("faviconUrl", link.faviconUrl);
				 
				arr.add(o);
			}
			return arr;
		}
	}
}
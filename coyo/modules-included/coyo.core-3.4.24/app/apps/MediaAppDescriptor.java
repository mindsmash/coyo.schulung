package apps;

import models.app.App;
import models.app.MediaApp;

@LoadApp
public class MediaAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "media";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return MediaApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-picture";
	}
}

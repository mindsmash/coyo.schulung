package apps;

import models.TasksApp;
import models.app.App;

@LoadApp
public class TasksAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "tasks";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return TasksApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-ok";
	}
}

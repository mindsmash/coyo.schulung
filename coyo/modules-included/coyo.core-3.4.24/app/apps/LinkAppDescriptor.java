package apps;

import models.app.App;
import models.app.LinkApp;

@LoadApp
public class LinkAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "link";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return LinkApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-bookmark";
	}
}

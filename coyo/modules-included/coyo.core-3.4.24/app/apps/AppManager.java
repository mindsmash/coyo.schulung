package apps;

import lombok.extern.slf4j.Slf4j;
import models.Sender;
import models.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * The AppManager holds all {@link AppDescriptor}s to access all App Informations without having to initialize a new
 * Model Instance.
 *
 * @author mindsmash GmbH
 */
@Slf4j
public class AppManager {
	public static final Logger LOG = LoggerFactory.getLogger(AppManager.class);
	public static Map<String, AppDescriptor> apps;

	private AppManager() {
		// hide
	}

	private static Map<String, AppDescriptor> getAppsMap() {
		if (apps == null) {
			LOG.info("[AppManager] Loading apps");

			apps = new LinkedHashMap<String, AppDescriptor>();

			for (ApplicationClass ac : Play.classes.getAnnotatedClasses(LoadApp.class)) {
				try {
					AppDescriptor app = (AppDescriptor) ac.javaClass.newInstance();
					LOG.debug("[AppManager] Registering app [{}]", app.getKey());
					apps.put(app.getKey(), app);
				} catch (Exception e) {
					LOG.error("[AppManager] Error loading app [" + ac + "], skipping.", e);
				}
			}
		}
		return apps;
	}

	public static List<AppDescriptor> getApps() {
		List<AppDescriptor> r = new ArrayList<AppDescriptor>();
		r.addAll(getAppsMap().values());
		Collections.sort(r, new AppManager.AppComparator());
		return r;
	}

	public static List<AppDescriptor> getActiveApps(Sender sender) {
		return getActiveApps(sender.getSenderType());
	}

	public static List<AppDescriptor> getActiveApps(String senderType) {
		List<AppDescriptor> r = new ArrayList<AppDescriptor>();
		for (AppDescriptor app : getApps()) {
			if (isActive(app, senderType)) {
				r.add(app);
			}
		}
		Collections.sort(r, new AppManager.AppComparator());
		return r;
	}

	public static AppDescriptor getApp(String key) {
		return getAppsMap().get(key);
	}

	public static boolean isActive(AppDescriptor app, String senderType) {
		Settings settings = getAppSettings(app.getKey());

		// check license
		try {
			if (app.isLicenseRequired() && !app.isLicenseValid()) {
				LOG.debug("[AppManager] App {} is inactive because no valid license was found", app);
				return false;
			}
		} catch (Exception e) {
			LOG.warn("[AppManager] Error checking license for app: " + app, e);
		}

		// pre 2.5
		if (settings.getBoolean("active")) {
			return true;
		}

		return settings.getBoolean("active-" + senderType);
	}

	public static void toggle(AppDescriptor app, String senderType, boolean active) {
		if (!getAppsMap().containsKey(app.getKey())) {
			throw new IllegalArgumentException("app with key [" + app.getKey() + "] is not registered");
		}

		Settings settings = getAppSettings(app.getKey());

		// pre 2.5 - autofix
		if (settings.contains("active")) {
			settings.setProperty("active-page", "true");
			settings.setProperty("active-workspace", "true");
			settings.removeProperty("active");
		}

		settings.setProperty("active-" + senderType, Boolean.toString(active));
		settings.save();
	}

	protected static Settings getAppSettings(String key) {
		return Settings.findOrCreate("app-" + key);
	}

	public static class AppComparator implements Comparator<AppDescriptor> {

		@Override
		public int compare(AppDescriptor ad1, AppDescriptor ad2) {
			return ad1.getName().compareTo(ad2.getName());
		}

	}
}

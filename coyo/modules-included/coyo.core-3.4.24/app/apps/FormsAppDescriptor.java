package apps;

import models.app.App;
import models.app.FormsApp;
import play.i18n.Messages;

@LoadApp
public class FormsAppDescriptor extends AppDescriptor {

	public static final String KEY = "forms";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		return Messages.get("app." + getKey());
	}

	@Override
	public String getDescription() {
		return Messages.get("app." + getKey() + ".description");
	}

	@Override
	public Class<? extends App> getAppModel() {
		return FormsApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-list-alt";
	}
}

package apps;

import models.User;
import models.app.App;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Any app can come with a range of app widgets. App widgets can be added to the rightbar by admins of a page or
 * workspace.
 * 
 * You need to annotate your {@link AppWidget} with {@link LoadAppWidget} to load it.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public abstract class AppWidget<T extends App> {

	/**
	 * The unique key of your app widget.
	 * 
	 * @return The unique key of your widget, e.g. "mywidget"
	 */
	public abstract String getKey();

	/**
	 * @return Display name
	 */
	public abstract String getName();

	/**
	 * @return Description text (max two sentences)
	 */
	public abstract String getDescription();

	/**
	 * This method returns the JSON data that is required for rendering the widget template.
	 * 
	 * @param app The app that the widget is <b>from</b>.
	 * @return {@link JsonElement} or null
	 */
	public JsonElement getRenderData(T app, User connectedUser) {
		return null;
	}

	/**
	 * Allows the widget to define a CSS/LESS file which should also be loaded. You don't need to put a file suffix as
	 * the system will automatically determine whether it is a ".css" or ".less" file.
	 * 
	 * @return CSS/LESS file (without suffix) or null
	 */
	public String getCss() {
		return null;
	}

	/**
	 * Allows the widget to define a JS file which should also be loaded. You don't need to put a file suffix.
	 * 
	 * @return JS file (without suffix) or null
	 */
	public String getJs() {
		return null;
	}
}

package apps;

import java.util.List;

import json.FileAttachmentSerializer;
import json.ForumAppTopicSerializer;
import json.ForumPostSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import models.User;
import models.app.App;
import models.app.ForumApp;
import models.app.dao.ForumAppDao;
import models.app.forum.ForumAppTopic;
import models.app.forum.ForumPost;
import models.wall.attachments.PostAttachment;
import play.i18n.Messages;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

@LoadApp
public class ForumAppDescriptor extends AppDescriptor {

	public static final String KEY = "forum";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public String getName() {
		return Messages.get("app." + KEY);
	}

	@Override
	public String getDescription() {
		return Messages.get("app." + KEY + ".description");
	}

	@Override
	public String getIconClass() {
		return "icon-comment";
	}

	@Override
	public Class<? extends App> getAppModel() {
		return ForumApp.class;
	}
	
	@LoadAppWidget(forApp = ForumAppDescriptor.class)
	public static class LatestForumTopicsWidget extends AppWidget<ForumApp> {

		@Override
		public String getKey() {
			return "latest";
		}

		@Override
		public String getName() {
			return Messages.get("app.forum.widget.latest.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.forum.widget.latest.description");
		}
		
		@Override
		public String getCss() {
			return "app.forum.widget";
		}

		@Override
		public JsonElement getRenderData(ForumApp app, User connectedUser) {
			List<ForumAppTopic> topics = ForumAppDao.getLatestTopics(app, 5);
			return createBuilder().create().toJsonTree(topics);
		}
		
		private static GsonBuilder createBuilder() {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(ForumAppTopic.class, new ForumAppTopicSerializer());
			builder.registerTypeAdapter(ForumPost.class, new ForumPostSerializer());
			builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
			builder.registerTypeHierarchyAdapter(PostAttachment.class, new FileAttachmentSerializer());
			builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
			return builder;
		}
		
	}

}

package apps;

import java.util.Map;
import java.util.TreeMap;

import license.Licensable;
import models.Settings;
import models.app.App;
import play.Logger;
import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.i18n.Messages;

/**
 * <p>
 * The app descriptor defines key, name, description and settings of an app. Each app consists of an app descriptor and
 * at least one model that extends {@link App}.
 * </p>
 * 
 * <p>
 * While the app descriptor is used to register and manage apps during runtime, the app model is used to persist single
 * entities.
 * </p>
 * 
 * <p>
 * Each app must be registered with the {@link AppManager} on application startup. You can automatically register your
 * app by adding the {@link LoadApp} annotation to this class.
 * </p>
 * 
 * @author mindsmash GmbH
 * 
 */
public abstract class AppDescriptor extends Licensable {

	private Map<String, AppWidget> widgets;

	/**
	 * The unique key of your app. If an app with the same name is already registered the app will be denied and the
	 * existing app will not be overloaded.
	 * 
	 * @return Unique app key, e.g. "taskmanager"
	 */
	public abstract String getKey();

	/**
	 * The display name of your app. Uses {@link Messages} to localize.
	 * 
	 * @return App display name
	 */
	public abstract String getName();

	/**
	 * Short description of your app. Uses {@link Messages} to localize. Use around 20-30 words.
	 * 
	 * @return Short description.
	 */
	public abstract String getDescription();

	/**
	 * Each app has an icon in the left sidebar. Use the glyphicon icon classes to define your app's icon. Please see
	 * this URL to find out which icon classes are available.<br />
	 * http://twitter.github.com/bootstrap/base-css.html#icons
	 * 
	 * @return Icon class to use in left sidebar, e.g. "icon-music"
	 */
	public abstract String getIconClass();

	/**
	 * Your app must extend the {@link App} class to define it's own database model. You may add your own attributes and
	 * binding operations to that model, too. Extending the {@link App} class does not require any additional method
	 * implementations though.
	 * 
	 * @return The class of your app model.
	 */
	public abstract Class<? extends App> getAppModel();
	
	@Override
	protected String getLicenseProductKey() {
		return "coyo-app-" + getKey();
	}

	/**
	 * Each app can have a list of {@link AppWidget}s which can be added to the rightbar by admins of a page or
	 * workspace. This methods loads the {@link AppWidget}s that are annotated with {@link LoadAppWidget} in the natural
	 * order of its keys.
	 * 
	 * @return The map (key is the widget key) of available widgets for this app.
	 */
	public Map<String, AppWidget> getWidgets() {
		if (widgets == null) {
			widgets = new TreeMap<>();
			for (ApplicationClass ac : Play.classes.getAnnotatedClasses(LoadAppWidget.class)) {
				try {
					if (ac.javaClass.getAnnotation(LoadAppWidget.class).forApp() == getClass()) {
						AppWidget widget = (AppWidget) ac.javaClass.newInstance();
						widgets.put(widget.getKey(), widget);
						Logger.debug("[App] Loaded widget [%s] for app [%s]", widget.getKey(), getKey());
					}
				} catch (Exception e) {
					Logger.error("[App] Error loading widget [%s] for app [%s]. Skipping.", ac.javaClass, getKey());
				}
			}
		}
		return widgets;
	}

	/**
	 * Get a widget by its key.
	 * 
	 * @param widgetKey
	 * @return Widget or null.
	 */
	public AppWidget getWidget(String widgetKey) {
		return getWidgets().get(widgetKey);
	}

	/**
	 * Your app's settings can be used to store app specific settings. Use {@link Settings#save()} to persist your
	 * changes.
	 * 
	 * @return Your app's settings.
	 */
	protected Settings getSettings() {
		return AppManager.getAppSettings(getKey());
	}
}

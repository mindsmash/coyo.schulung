package apps;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import libs.DateI18N;
import models.User;
import models.app.App;
import models.app.CalendarApp;
import models.event.Event;
import org.joda.time.DateTime;
import play.i18n.Lang;
import play.i18n.Messages;

@LoadApp
public class CalendarAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "calendar";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return CalendarApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-calendar";
	}

	@LoadAppWidget(forApp = CalendarAppDescriptor.class)
	public static class UpcomingEventsWidget extends AppWidget<CalendarApp> {
		@Override
		public String getKey() {
			return "upcoming";
		}

		@Override
		public String getName() {
			return Messages.get("app.calendar.widget.upcoming.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.calendar.widget.upcoming.description");
		}

		@Override
		public JsonElement getRenderData(CalendarApp app, User connectedUser) {
			JsonArray arr = new JsonArray();
			for (Event event : app.getUpcomingEvents(connectedUser.getDateTimeZone(), 5)) {
				JsonObject o = new JsonObject();
				o.addProperty("id", event.id);
				o.addProperty("name", event.name);
				o.addProperty("fulltime", event.fulltime);
				o.addProperty("startDate", event.startDate.getMillis());

				final DateTime localDate = event.startDate.withZone(connectedUser.getDateTimeZone());

				o.addProperty("startDateAsText",
						localDate.toString(DateI18N.getShortFormat(), Lang.getLocale()) + " " + localDate
								.toString(DateI18N.getTimeFormat(), Lang.getLocale()));

				arr.add(o);
			}
			return arr;
		}

		@Override
		public String getCss() {
			return "app.calendar.widget";
		}
	}
}

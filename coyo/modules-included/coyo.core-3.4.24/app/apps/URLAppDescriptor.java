package apps;

import models.app.App;
import models.app.URLApp;

@LoadApp
public class URLAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "url";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return URLApp.class;
	}
}

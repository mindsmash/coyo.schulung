package apps;

import models.app.App;
import models.app.RSSApp;

@LoadApp
public class RSSAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "rss";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return RSSApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-download";
	}
}

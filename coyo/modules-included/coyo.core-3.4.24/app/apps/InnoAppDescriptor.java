package apps;

import models.app.App;
import models.app.InnoApp;
import play.Play;

@LoadApp
public class InnoAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "inno";

	@Override
	public String getKey() {
		return KEY;
	}
	
	@Override
	public boolean isLicenseRequired() {
		return Boolean.valueOf(Play.configuration.getProperty("coyo.innovationApp.licenseRequired", "true"));
	}
	
	@Override
	protected String getLicenseProductKey() {
		return "coyo-innovationApp";
	}

	@Override
	public Class<? extends App> getAppModel() {
		return InnoApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-star";
	}
}

package apps;

import models.app.App;
import models.app.ContentApp;

@LoadApp
public class ContentAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "content";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return ContentApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-book";
	}
}

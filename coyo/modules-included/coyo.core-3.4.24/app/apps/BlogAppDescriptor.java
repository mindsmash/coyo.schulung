package apps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;

import license.LicenseValidator;
import models.User;
import models.app.App;
import models.app.BlogApp;
import models.app.BlogAppArticle;
import play.i18n.Messages;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;

@LoadApp
public class BlogAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "blog";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return BlogApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-tags";
	}

	@LoadAppWidget(forApp = BlogAppDescriptor.class)
	public static class LatestBlogArticlesWidget extends AppWidget<BlogApp> {
		@Override
		public String getKey() {
			return "latest";
		}

		@Override
		public String getName() {
			return Messages.get("app.blog.widget.latest.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.blog.widget.latest.description");
		}

		@Override
		public JsonElement getRenderData(BlogApp app, User connectedUser) {
			JsonArray arr = new JsonArray();
			for (BlogAppArticle article : app.getLatestArticles(5)) {
				JsonObject o = new JsonObject();
				o.addProperty("id", article.id);
				o.addProperty("title", article.title);
				o.addProperty("url", article.getURL());
				o.addProperty("time", article.publishDate.getMillis());
				
				o.add("author", createBuilder().create().toJsonTree(article.author));

				arr.add(o);
			}
			return arr;
		}

		@Override
		public String getCss() {
			return "app.blog.widget";
		}
		
		private static GsonBuilder createBuilder() {
			return new GsonBuilder().registerTypeAdapter(User.class, new MinimalUserSerializer())
					.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		}
	}
}

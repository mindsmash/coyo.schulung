package apps;

import json.FileAttachmentSerializer;
import json.ForumAppTopicSerializer;
import json.HibernateProxyTypeAdapter;
import json.MinimalUserSerializer;
import json.PostSerializer;
import models.User;
import models.app.App;
import models.app.WikiApp;
import models.app.WikiAppArticle;
import models.app.forum.ForumAppTopic;
import models.app.forum.ForumPost;
import models.wall.attachments.PostAttachment;
import play.i18n.Messages;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@LoadApp
public class WikiAppDescriptor extends AbstractAppDescriptor {

	public static final String KEY = "wiki";

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public Class<? extends App> getAppModel() {
		return WikiApp.class;
	}

	@Override
	public String getIconClass() {
		return "icon-info-sign";
	}

	@LoadAppWidget(forApp = WikiAppDescriptor.class)
	public static class LatestWikiArticlesWidget extends AppWidget<WikiApp> {
		@Override
		public String getKey() {
			return "latest";
		}

		@Override
		public String getName() {
			return Messages.get("app.wiki.widget.latest.name");
		}

		@Override
		public String getDescription() {
			return Messages.get("app.wiki.widget.latest.description");
		}

		@Override
		public JsonElement getRenderData(WikiApp app, User connectedUser) {
			JsonArray arr = new JsonArray();
			for (WikiAppArticle article : app.getLatestArticles(5)) {
				JsonObject o = new JsonObject();
				o.addProperty("id", article.id);
				o.addProperty("title", article.title);
				o.addProperty("versionCount", article.getVersionCount());
				o.addProperty("time", article.getCurrentVersion().created.getTime());

				JsonElement author = createBuilder().create().toJsonTree(article.getCurrentVersion().author);
				o.add("author", author);
				arr.add(o);
			}
			return arr;
		}

		@Override
		public String getCss() {
			return "app.wiki.widget";
		}
		
		private static GsonBuilder createBuilder() {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(User.class, new MinimalUserSerializer());
			builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
			return builder;
		}
	}
}

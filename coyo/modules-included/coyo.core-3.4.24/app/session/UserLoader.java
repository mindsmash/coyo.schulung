package session;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import controllers.Auth;
import exceptions.TenantException;
import models.User;
import multitenancy.MTA;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.db.jpa.JPA;
import play.mvc.Http.Request;
import play.mvc.results.Forbidden;
import util.HttpUtil;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;

/**
 * Class for accessing the connected / current user.
 *
 * @author mindsmash GmbH
 */
public class UserLoader {

    private static final ThreadLocal<Long> threadLocalUser = new ThreadLocal<Long>();

    private static final Logger log = LoggerFactory.getLogger(UserLoader.class);

    public static final boolean CACHE_USER_IN_REQUEST_ARGS = Boolean.parseBoolean(Play.configuration.getProperty(
            "mindsmash.userloader.cache", "true"));

    public static final String CACHE_KEY_USER = "user";

    private UserLoader() {
    }

    /**
     * Loads current User
     *
     * @throws Exception
     * @throws ClassNotFoundException
     * @throws InvocationTargetException
     */
    public static User getConnectedUser(boolean cancel, boolean assertTenant) {
        if (Request.current() != null) {
            User user = getConnectedUserFromRequest(assertTenant);

            if (cancel && user == null) {
                log.debug("[UserLoader] Executing logout.");
                Auth.executeLogout();

                if (Request.current().isAjax()) {
                    throw new Forbidden("Access denied");
                } else {
                    log.debug("[UserLoader] Redirecting user to login [destination={}]", Request.current().url);
                    Auth.launchLogin(Request.current().url);
                }
            }
            return user;

        } else {
            User user = getConnectedUserWithoutRequest(assertTenant);
            if (cancel && user == null) {
                throw new IllegalStateException("User not found");
            }
            return user;
        }
    }

    public static User getConnectedUser(boolean cancel) {
        return getConnectedUser(cancel, false);
    }

    public static User getConnectedUser() {
        return getConnectedUser(true, false);
    }

    private static User getConnectedUserFromRequest(boolean assertTenant) {
        final Monitor monitor = MonitorFactory.start(UserLoader.class.getName() + ".getConnectedUserFromRequest()");

        try {
            // check for user in request
            if (CACHE_USER_IN_REQUEST_ARGS && Request.current() != null && Request.current().args
                    .containsKey(CACHE_KEY_USER)) {
                log.trace("[UserLoader] Getting user from request args");
                User user = (User) Request.current().args.get(CACHE_KEY_USER);

                if (assertTenant) {
                    assertCorrectTenant(user.id);
                }

                // check if managed and reload if not
                if (!JPA.em().contains(user)) {
                    user = User.findById(user.id);
                    Request.current().args.put(CACHE_KEY_USER, user);
                }
                return user;

            } else {
                // check if there is a current id
                String currentId = Auth.getCurrentId();
                if (StringUtils.isEmpty(currentId) || !StringUtils.isNumeric(currentId)) {
                    log.debug("[UserLoader] No valid user session found [currentId={}]", currentId);
                    return null;
                }

                // check if the user can be found
                User user = User.findById(Long.valueOf(currentId));
                if (user != null) {
                    if (assertTenant) {
                        assertCorrectTenant(user.id);
                    }

                    // cache user obj to avoid redundant user lookup
                    if (CACHE_USER_IN_REQUEST_ARGS && Request.current() != null) {
                        Request.current().args.put(CACHE_KEY_USER, user);
                    }
                    return user;

                }

                Object[] args = { Request.current().path, Request.current().action };
                log.info(
                        "[UserLoader] The user of current session does not seem to exist anymore [current URL: {}, current action: {}]",
                        args);

                return null;
            }
        } finally {
            monitor.stop();
        }

    }

    private static User getConnectedUserWithoutRequest(boolean assertTenant) {
        if (Request.current() == null && UserLoader.threadLocalUser != null && UserLoader.threadLocalUser.get() != null) {
            Long userId = UserLoader.threadLocalUser.get();

            Object[] args = {userId, Thread.currentThread(), Request.current()};
            log.debug("[UserLoader] Loading user [{}] from thread local [thread={}, request={}]", args);

            if (assertTenant) {
                assertCorrectTenant(userId);
            }

            return User.findById(userId);
        }
        return null;
    }

    /**
     * We are checking with a native query here to avoid the Hibernate filter to kick in.
     *
     * @see TenantModel.afterSelect() and TenantModel.checkTenant.
     */
    private static void assertCorrectTenant(Long userId) throws TenantException {
        BigInteger userTenantId = (BigInteger) JPA.em()
                .createNativeQuery("SELECT s.tenant_id FROM sender s WHERE s.id = ?").setParameter(1, userId)
                .getSingleResult();

        if (userTenantId.longValue() == MTA.getActiveTenant().id) {
            return; // ok
        } else {
            Object[] args = {userId, userTenantId, MTA.getActiveTenant().id, Request.current().host,
                    Request.current().path, Request.current().querystring, HttpUtil.getReferrerUrl()};

            log.warn(
                    "[UserLoader] Found user [{}] with tenant [{}] in session but the user does not belong to the active tenant [{}]. Details: [host={}, path={}, queryString={}, referrer={}]",
                    args);
            throw new TenantException();
        }
    }

    /**
     * You may set a custom current user id if you are not in a request scope. Can be used to supply the permission
     * checker with a user if no request is available (e.g. job). Make sure to clear the value when you are done using
     * this.
     *
     * @param id
     */
    public static void setThreadLocalUser(Long id) {
        if (Request.current() != null) {
            throw new IllegalStateException("Using thread local user in a request scope is not allowed");
        }
        UserLoader.threadLocalUser.set(id);
    }

    public static void clearThreadLocalUser() {
        UserLoader.threadLocalUser.remove();
    }

    public static void clearUserCache() {
        if (CACHE_USER_IN_REQUEST_ARGS &&  Request.current() != null) {
            Request.current().args.remove(CACHE_KEY_USER);
        }
    }
}

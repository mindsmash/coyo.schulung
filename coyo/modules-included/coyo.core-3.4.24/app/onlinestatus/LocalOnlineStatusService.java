package onlinestatus;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jobs.PublishOfflineEventJob;
import models.User;
import play.Logger;

import com.google.common.base.Predicate;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalCause;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Implementation of a user online status list which stores its data in a local
 * data structure, i.e. it is only suited for single node environments.
 *
 * @author Jan Tammen, mindsmash GmbH
 */
public class LocalOnlineStatusService implements OnlineStatusService {

	private Cache<Long, UserOnlineStatus> userOnlineStatusMap = CacheBuilder.newBuilder()
			.expireAfterWrite(UserOnlineStatus.ONLINE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
			.removalListener(new RemovalListener<Long, UserOnlineStatus>() {
				@Override
				public void onRemoval(RemovalNotification<Long, UserOnlineStatus> event) {
					//only expires if we do not replace by long poll or WebBaseController
					if (event.getCause().equals(RemovalCause.EXPIRED)) {
						try {
							Logger.debug("[OnlineStatusService] Entry evicted: " + event.getKey() + " -> "
									+ UserOnlineStatus.OnlineStatus.OFFLINE);
							new PublishOfflineEventJob(event.getKey(), event.getValue()).now();
						} catch (Exception ex) {
							Logger.error("[OnlineStatusService] Error: " + ex.getLocalizedMessage(), ex);
						}
					}
				}
			})
			.build();

	@Override
	public List<Long> getOnlineUserIds() {
		return Lists.newArrayList(Maps.filterValues(userOnlineStatusMap.asMap(), new Predicate<UserOnlineStatus>() {
			@Override
			public boolean apply(UserOnlineStatus uos) {
				return uos.isOnline();
			}
		}).keySet());
	}
	
	@Override
	public Collection<Long> getActiveUserIds() {
		return userOnlineStatusMap.asMap().keySet();
	}


	@Override
	public long getOnlineUserCount() {
		return getOnlineUserIds().size();
	}

	@Override
	public UserOnlineStatus getOnlineStatus(User user) {
		return getOnlineStatus(user.id);
	}
	
	@Override
	public UserOnlineStatus getOnlineStatus(Long userId) {
		final Map<Long, UserOnlineStatus> userOnlineStatus = userOnlineStatusMap.asMap();

		if (userOnlineStatus.containsKey(userId)) {
			return userOnlineStatus.get(userId);
		} else {
			return new UserOnlineStatus(UserOnlineStatus.OnlineStatus.OFFLINE);
		}
	}

	@Override
	public void removeOnlineStatus(User user) {
		if (user != null) {
			userOnlineStatusMap.asMap().remove(user.id);
			OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, getOnlineStatus(user), new UserOnlineStatus(UserOnlineStatus.OnlineStatus.OFFLINE));
		}
	}


	@Override
	public void refreshOnlineStatus(User user) {
		boolean raiseEvent = false;
		UserOnlineStatus newStatus = userOnlineStatusMap.asMap().get(user.id);
		UserOnlineStatus oldStatus = getOnlineStatus(user);
		Long userId = user.id;
		
		if (newStatus == null) {
			newStatus = new UserOnlineStatus(OnlineStatusSessionStore.getStatus());
			raiseEvent = true;
		}

		// always update map to avoid expiry of entries
		userOnlineStatusMap.asMap().put(userId, newStatus);
		
		// only raise event if user online state did not existed before (==offline)
		if (raiseEvent) {
			OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, oldStatus, newStatus);
		}
	}

	/**
	 * Set online status use TTL feature of cache map where entries expire after time
	 *
	 * @param user   Coyo user
	 * @param status Online status of user
	 */
	@Override
	public void setOnlineStatus(User user, UserOnlineStatus.OnlineStatus status) {
		UserOnlineStatus newStatus = new UserOnlineStatus(status);
		UserOnlineStatus oldStatus = getOnlineStatus(user);
		userOnlineStatusMap.put(user.id, newStatus);
		OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, oldStatus, newStatus);
	}
}

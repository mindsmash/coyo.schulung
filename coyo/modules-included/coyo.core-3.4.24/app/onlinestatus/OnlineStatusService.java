package onlinestatus;

import java.util.Collection;
import java.util.List;

import models.User;
import onlinestatus.UserOnlineStatus.OnlineStatus;

/**
 * Interface for a user online status list.
 * 
 * We distinguish between "online" users and "active" users. A user is "active" as long as he is surfing. However, a
 * user can only be "online" when his status is either online or away.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public interface OnlineStatusService {

	/**
	 * Returns the online status for the given user. Always returns a non-null value.
	 * 
	 * @param user
	 *            User for whom the online status should be returned.
	 * @return The user's online status.
	 */
	public UserOnlineStatus getOnlineStatus(User user);
	
	/**
	 * Returns the online status for the given user. Always returns a non-null value.
	 * 
	 * @param userId
	 *            User for whom the online status should be returned.
	 * @return The user's online status.
	 */
	public UserOnlineStatus getOnlineStatus(Long userId);

	/**
	 * @return Count of users who are online.
	 */
	long getOnlineUserCount();

	/**
	 * @return List of IDs of users who are online.
	 */
	List<Long> getOnlineUserIds();

	/**
	 * @return List of IDs of users who are active (including users marked as offline).
	 */
	Collection<Long> getActiveUserIds();

	/**
	 * @param user
	 *            User whose online status should be removed.
	 */
	void removeOnlineStatus(User user);

	/**
	 * @param user
	 *            User for whom the online status should be set.
	 * @param userOnlineStatus
	 *            The status that should be set.
	 */
	void setOnlineStatus(User user, OnlineStatus userOnlineStatus);

	/**
	 * Updates the online status for the given user or sets it to {@link UserOnlineStatus.OnlineStatus.ONLINE} if there
	 * was no status set for this user, yet.
	 * 
	 * @param user
	 *            User for whom the online status should be updated/set.
	 */
	void refreshOnlineStatus(User user);
}

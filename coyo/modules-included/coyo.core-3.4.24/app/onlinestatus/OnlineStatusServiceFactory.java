package onlinestatus;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import models.AbstractTenant;
import models.User;
import multitenancy.MTA;

import org.elasticsearch.common.collect.Maps;

import play.Logger;
import plugins.play.HazelcastPlugin;

import com.google.common.collect.ImmutableSet;

import events.type.OnlineStatusChangedEvent;

/**
 * Creates services that manage online status of users in local and MTA setups Entries will have a TTL and therefore
 * will expire Also handles event creation to inform other users about change of online status
 */
public class OnlineStatusServiceFactory {
	private static Map<AbstractTenant, OnlineStatusService> instances = Maps.newConcurrentMap();

	/**
	 * Please note that this method requires MTA to be active.
	 */
	public static OnlineStatusService getOnlineStatusService() {
		return getOnlineStatusService(MTA.getActiveTenant());
	}

	/**
	 * Either returns existing services or creates new one Distinguishes between local only or MTA setup
	 * 
	 * @param tenant
	 *            Active Tenant
	 * @return OnlineStatusService
	 */
	public static OnlineStatusService getOnlineStatusService(AbstractTenant tenant) {
		if (instances.containsKey(tenant)) {
			return instances.get(tenant);
		} else {
			if (HazelcastPlugin.ACTIVE) {
				// check to avoid creation of new services
				Logger.debug("[OnlineStatusServiceFactory] Creating service for multi node environment.");
				HazelOnlineStatusService service = new HazelOnlineStatusService(tenant);
				instances.put(tenant, service);
				return service;

			} else {
				// check to avoid creation of new services
				Logger.debug("[OnlineStatusServiceFactory] Creating service for single node environment.");
				LocalOnlineStatusService service = new LocalOnlineStatusService();
				instances.put(tenant, service);
				return service;
			}
		}
	}

	/**
	 * Removes instance - must be called when tenant gets deleted
	 * 
	 * @param tenant
	 *            non active tenant (deleted)
	 */
	public static void removeOnlineStatusInstance(AbstractTenant tenant) {
		instances.remove(tenant);
	}

	/**
	 * Sends event to followers or chat partners if online status of one user has changed (and they are online)
	 *
	 * @param userId
	 *            User id of the user whoms online status has changed
	 * @param oldStatus
	 *            previous online state
	 * @param newStatus
	 *            new online state
	 */
	public static void raiseEventIfNecessary(final Long userId, final UserOnlineStatus oldStatus,
			final UserOnlineStatus newStatus) {
		try {
			if (newStatus == null) {
				Logger.warn("[OnlineStatusService] Not sending status update event, since new status is not set.");

			} else if (!newStatus.equals(oldStatus)) {
				if (oldStatus == null) {
					Logger.warn("[OnlineStatusService] Sending status update event, with old status not set.");
				}
				final User user = User.findById(userId);
				
				if(user == null) {
					return;
				}
				
				final Collection<Long> followerIds = user.getFollowerIds();
				final List<Long> activeConversationsIds = user.getActiveConversationsUserIds();
				final Collection<Long> onlineUserIds = getOnlineStatusService(user.tenant).getActiveUserIds();
				final Long onlineUserCount = getOnlineStatusService(user.tenant).getOnlineUserCount();
				
				for (Long id : ImmutableSet.<Long> builder().addAll(followerIds).addAll(activeConversationsIds).build()) {
					// only raise events for users that currently are online AND myself
					if (id == user.id || onlineUserIds.contains(id)) {
						new OnlineStatusChangedEvent(user.id, oldStatus, newStatus, onlineUserCount).raise(id);
					}
				}
			}
		} catch (Exception ex) {
			Logger.error("[OnlineStatusService] Error: " + ex.getLocalizedMessage());
			throw ex;
		}
	}
}

package onlinestatus;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.DateTime;

import play.Logger;
import play.Play;

/**
 * Represents a user's online status composed of a status value and a timestamp.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public class UserOnlineStatus implements Serializable {

	// online status timeout in milliseconds
	public static final int ONLINE_TIMEOUT_MS = Integer.parseInt(Play.configuration.getProperty(
			"coyo.session.lifetime", "180000"));

	public enum OnlineStatus {
		ONLINE, BUSY, AWAY, OFFLINE
	}

	public Long lastUpdated;

	public OnlineStatus status;

	public UserOnlineStatus() {
		this(OnlineStatus.ONLINE);
	}

	public UserOnlineStatus(OnlineStatus status) {
		this.lastUpdated = System.currentTimeMillis();
		this.status = status;
	}

	public boolean isOnline() {
		return OnlineStatus.BUSY == status || OnlineStatus.AWAY == status || OnlineStatus.ONLINE == status;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
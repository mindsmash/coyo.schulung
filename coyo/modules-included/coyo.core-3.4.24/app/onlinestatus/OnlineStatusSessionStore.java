package onlinestatus;

import onlinestatus.UserOnlineStatus.OnlineStatus;
import play.Logger;
import play.mvc.Scope.Session;

/**
 * Stores and retrieves the online status of a user from his or her session.
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 *
 */
public class OnlineStatusSessionStore {

	private static final String SESSION_KEY_ONLINE_STATUS = "onlineStatus";

	/**
	 * Get the stored online status (the last set online status of a user) from his or her session. If no value could be
	 * found in the session, ONLINE is returned as default.
	 */
	public static OnlineStatus getStatus() {
		if (Session.current().contains(OnlineStatusSessionStore.SESSION_KEY_ONLINE_STATUS)) {
			String statusValue = Session.current().get(OnlineStatusSessionStore.SESSION_KEY_ONLINE_STATUS);
			try {
				OnlineStatus onlineStatus = OnlineStatus.valueOf(statusValue);
				return onlineStatus;
			} catch (Exception e) {
				Logger.warn(e, "Invalid online status found in user session.");
			}
		}
		return UserOnlineStatus.OnlineStatus.ONLINE;
	}

	/**
	 * Store the passed online status in the user's session
	 */
	public static void storeStatus(OnlineStatus status) {
		Session.current().put(OnlineStatusSessionStore.SESSION_KEY_ONLINE_STATUS, status);
	}

}
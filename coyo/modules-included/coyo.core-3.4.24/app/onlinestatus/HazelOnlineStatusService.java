package onlinestatus;

import com.google.common.collect.Lists;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.IMap;
import com.hazelcast.core.MapEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryEvictedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import com.hazelcast.map.listener.MapClearedListener;
import com.hazelcast.map.listener.MapEvictedListener;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import controllers.api.UpdateService;
import jobs.PublishOfflineEventJob;
import models.AbstractTenant;
import models.User;
import play.Logger;
import plugins.play.HazelcastPlugin;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of a user online status list which stores its data in a Hazelcast-based distributed data structure.
 *
 * @author Jan Tammen, mindsmash GmbH
 */
public class HazelOnlineStatusService implements OnlineStatusService {

	public static final String USER_ONLINE_STATUS_MAP = "userOnlineStatusMap-";

	private final static Predicate ONLINE_PREDICATE = new PredicateBuilder().getEntryObject().is("isOnline");

	/**
	 * This is the period in which a refresh of the user's online status is ignored and not updates in the cluster for
	 * performance reasons.
	 */
	private static final long REFRESH_IGNORE_PERIOD = Math.max(0, UpdateService.getTimeoutAsMillis() - 5000);

	private String mapName;
	/**
	 * We add the refresh ignore period to the default configured timeout.
	 */
	private static final long EXTENDED_ONLINE_TIMEOUT_MS = UserOnlineStatus.ONLINE_TIMEOUT_MS + REFRESH_IGNORE_PERIOD;

	public HazelOnlineStatusService(final AbstractTenant tenant) {
		if (tenant == null || tenant.getId() == null) {
			throw new IllegalArgumentException("Cannot initialize HazelOnlineStatusService without tenant.");
		} else {
			final Long tenantId = tenant.getId();
			this.mapName = USER_ONLINE_STATUS_MAP + tenantId;

			final IMap<Long, UserOnlineStatus> userOnlineStatusMap = HazelcastPlugin.getHazel().getMap(mapName);
			userOnlineStatusMap.addEntryListener(new MyEntryListener(), true);
		}
	}

	@Override
	public List<Long> getOnlineUserIds() {
		final IMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		return Lists.newArrayList(map.keySet(ONLINE_PREDICATE));
	}

	@Override
	public Collection<Long> getActiveUserIds() {
		final ConcurrentMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		return map.keySet();
	}

	@Override
	public UserOnlineStatus getOnlineStatus(User user) {
		return getOnlineStatus(user.id);
	}

	@Override
	public UserOnlineStatus getOnlineStatus(Long userId) {
		if (userId == null) {
			throw new IllegalStateException("Userid was null");
		}

		final ConcurrentMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		if (map.containsKey(userId)) {
			return map.get(userId);
		}

		return new UserOnlineStatus(UserOnlineStatus.OnlineStatus.OFFLINE);
	}

	@Override
	public void removeOnlineStatus(User user) {
		if (user != null) {
			final IMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);
			try {
				map.lock(user.id);
				map.remove(user.id);
			} finally {
				map.unlock(user.id);
			}

			OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, getOnlineStatus(user),
					new UserOnlineStatus(UserOnlineStatus.OnlineStatus.OFFLINE));
		}
	}

	@Override
	public long getOnlineUserCount() {
		final ConcurrentMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		return map.size();
	}

	/**
	 * Set online status use TTL feature of hazelcast map where entries expire after time
	 *
	 * @param user   Coyo user
	 * @param status Online status of user
	 */
	@Override
	public void setOnlineStatus(User user, UserOnlineStatus.OnlineStatus status) {
		final UserOnlineStatus newStatus = new UserOnlineStatus(status);
		final UserOnlineStatus oldStatus = getOnlineStatus(user);

		final IMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		try {
			map.lock(user.id);
			map.put(user.id, newStatus, EXTENDED_ONLINE_TIMEOUT_MS, TimeUnit.MILLISECONDS);
		} finally {
			map.unlock(user.id);
		}

		OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, oldStatus, newStatus);
	}

	/**
	 * Refreshes the online status of the current user.
	 * <p/>
	 * All status updates that are very close to the last status update are ignored according to the
	 * {@link #REFRESH_IGNORE_PERIOD}.
	 *
	 * @param user the current user
	 */
	@Override
	public void refreshOnlineStatus(User user) {
		final IMap<Long, UserOnlineStatus> map = HazelcastPlugin.getHazel().getMap(mapName);

		final UserOnlineStatus currentStatus = map.get(user.id);

		// status is unknown, set if from session and broadcast the change
		if (currentStatus == null) {
			UserOnlineStatus sessionStatus = new UserOnlineStatus(OnlineStatusSessionStore.getStatus());
			map.put(user.id, sessionStatus, EXTENDED_ONLINE_TIMEOUT_MS, TimeUnit.MILLISECONDS);
			OnlineStatusServiceFactory.raiseEventIfNecessary(user.id, getOnlineStatus(user), sessionStatus);
		// status known	- refresh if ignore period is <= 0 or last update >= than refresh ignore period
		} else {
			if (REFRESH_IGNORE_PERIOD <= 0
					|| (System.currentTimeMillis() - currentStatus.lastUpdated) >= REFRESH_IGNORE_PERIOD) {
				currentStatus.lastUpdated = System.currentTimeMillis();
				map.put(user.id, currentStatus, EXTENDED_ONLINE_TIMEOUT_MS, TimeUnit.MILLISECONDS);
			}
		}
	}

	/**
	 * Callbacks for hazlecast map events
	 */
	private static class MyEntryListener
			implements EntryAddedListener<Long, UserOnlineStatus>, EntryRemovedListener<Long, UserOnlineStatus>,
			EntryUpdatedListener<Long, UserOnlineStatus>, EntryEvictedListener<Long, UserOnlineStatus>,
			MapEvictedListener, MapClearedListener {

		@Override
		public void entryAdded(EntryEvent<Long, UserOnlineStatus> event) {
		}

		@Override
		public void entryRemoved(EntryEvent<Long, UserOnlineStatus> event) {
		}

		@Override
		public void entryUpdated(EntryEvent<Long, UserOnlineStatus> event) {
		}

		@Override
		public void entryEvicted(EntryEvent<Long, UserOnlineStatus> event) {
			if (event == null) {
				return;
			}
			final UserOnlineStatus userOnlineStatus = event.getValue();
			Logger.debug("[OnlineStatusService] Entry evicted: " + event.getKey() + " -> " + (userOnlineStatus != null ?
					userOnlineStatus.status.name() :
					"[null]"));

			if (userOnlineStatus != null) {
				try {
					new PublishOfflineEventJob(event.getKey(), userOnlineStatus).now();
				} catch (Exception ex) {
					Logger.error(ex, "[OnlineStatusService] Error: " + ex.getLocalizedMessage());
				}
			}
		}

		@Override
		public void mapEvicted(MapEvent event) {
		}

		@Override
		public void mapCleared(MapEvent event) {
		}

	}
}

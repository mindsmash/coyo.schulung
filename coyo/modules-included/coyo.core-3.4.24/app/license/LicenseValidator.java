package license;

import injection.Inject;
import injection.InjectionSupport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import license.v2.License;
import models.Sender;
import models.Settings;
import models.Tenant;
import models.User;
import multitenancy.MTA;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jpa.JPA;
import play.libs.Codec;
import play.libs.Crypto;
import play.templates.JavaExtensions;
import util.ServerUtils;
import utils.CacheUtils;
import utils.JPAUtils;

import com.google.gson.JsonObject;

import di.server.id.DefaultServerIdBuilder;
import di.server.id.ServerIdBuilder;
import di.user.DefaultUserNameBuilder;
import di.user.UserNameBuilder;

/**
 * 
 * Checks if the defined Coyo license is valid.
 * 
 * @author mindsmash GmbH
 * 
 */
@InjectionSupport
public class LicenseValidator {

	public static final String CONF_LICENSE = "license";
	public static final String PRODUCT = "coyo";


	@Inject(configuration = "coyo.di.server.id.generator", defaultClass=DefaultServerIdBuilder.class, singleton = true)
	private static ServerIdBuilder serverIdBuilder;
	
	private LicenseValidator() {
		// hide
	}

	public static boolean valid() {
		if (Play.mode.isDev()) {
			return true;
		}

		Boolean valid = (Boolean) Cache.get(getCacheKey());
		if (valid == null) {
			valid = check(loadLicenseString());
			Cache.set(getCacheKey(), valid, "5min");
		}
		return valid;
	}

	public static void clearCache() {
		Cache.delete(getCacheKey());
	}

	private static String getCacheKey() {
		return CacheUtils.createTenantUniqueCacheKey("licenseValid");
	}

	public static boolean check(String license) {
		if (StringUtils.isEmpty(license)) {
			return false;
		}

		return !hasError(info(license));
	}

	public static boolean hasError(Collection<LicenseInfo> info) {
		for (LicenseInfo infoItem : info) {
			if (infoItem.error) {
				return true;
			}
		}
		return false;
	}

	public static Collection<LicenseInfo> info(String license) {
		return info(PRODUCT, License.evaluate(license));
	}

	public static Collection<LicenseInfo> info(String product, JsonObject licenseData) {
		List<LicenseInfo> info = new ArrayList<>();

		try {
			info.add(new LicenseInfo("product", product, licenseData.get("product").getAsString(), !product
					.equals(licenseData.get("product").getAsString())));
		} catch (Exception e) {
			info.add(new LicenseInfo("product", "", "", true));
		}

		try {
			if (licenseData.has("version"))
				info.add(new LicenseInfo("version", getVersion(), licenseData.get("version").getAsString(),
						!getVersion().startsWith(licenseData.get("version").getAsString())));
		} catch (Exception e) {
			info.add(new LicenseInfo("version", "", "", true));
		}

		try {
			if (licenseData.has("server"))
				info.add(new LicenseInfo("server", getServerID(), licenseData.get("server").getAsString(),
						!getServerID().equals(licenseData.get("server").getAsString())));
		} catch (Exception e) {
			info.add(new LicenseInfo("server", "", "", true));
		}

		try {
			if (licenseData.has("valid"))
				info.add(new LicenseInfo("valid", JavaExtensions.format(new Date(), "yyyy-MM-dd"), JavaExtensions
						.format(new Date(licenseData.get("valid").getAsLong()), "yyyy-MM-dd"), new DateTime(licenseData
						.get("valid").getAsLong()).plusDays(1).withMillisOfDay(0).isBeforeNow()));
		} catch (Exception e) {
			info.add(new LicenseInfo("valid", "", "", true));
		}

		try {
			if (licenseData.has("size"))
				info.add(new LicenseInfo("size", getUserCount() + "", licenseData.get("size").getAsString(),
						getUserCount() > licenseData.get("size").getAsInt()));
		} catch (Exception e) {
			info.add(new LicenseInfo("size", "", "", true));
		}

		return info;
	}

	// public static JsonObject load() {
	// return License.evaluate(loadLicenseString());
	// }

	public static String loadLicenseString() {
		// try to load from conf (has priority)
		if (Play.configuration.containsKey(CONF_LICENSE)
				&& StringUtils.isNotEmpty(Play.configuration.getProperty(CONF_LICENSE))) {
			return Play.configuration.getProperty(CONF_LICENSE);
		}

		// try to load from tenant
		String tenantLicense = ((Tenant) MTA.getActiveTenant()).license;
		if (StringUtils.isNotEmpty(tenantLicense)) {
			return tenantLicense;
		}

		return null;
	}

	public static String getVersion() {
		return Play.configuration.getProperty("application.version");
	}
	
	public static String getServerID() {
		return serverIdBuilder.getServerId();
	}

	public static void resetServerID() {
		Settings settings = Settings.findApplicationSettings();
		settings.removeProperty("server.id");
		settings.removeProperty("server.mac");
		settings.save();
		clearCache();
	}

	public static long getUserCount() {
		// disable external filter
		boolean externalFilterEnabled = ((org.hibernate.Session) JPA.em().getDelegate())
				.getEnabledFilter(Sender.EXTERNAL) != null;
		try {
			if (externalFilterEnabled) {
				((org.hibernate.Session) JPA.em().getDelegate()).disableFilter(Sender.EXTERNAL);
			}

			return User.count("active = true");
		} finally {
			if (externalFilterEnabled) {
				((org.hibernate.Session) JPA.em().getDelegate()).enableFilter(Sender.EXTERNAL);
			}
		}
	}

	public static class LicenseInfo {
		public String key;
		public String currentValue;
		public String licenseValue;
		public boolean error;

		public LicenseInfo(String key, String currentValue, String licenseValue, boolean error) {
			this.key = key;
			this.currentValue = currentValue;
			this.licenseValue = licenseValue;
			this.error = error;
		}
	}
}

package license;

import java.util.Collection;

import license.LicenseValidator.LicenseInfo;
import license.v2.License;
import models.Settings;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.cache.Cache;
import utils.CacheUtils;

import com.google.gson.JsonObject;

/**
 * Makes some entity license aware. Currently used for apps and plugins.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public abstract class Licensable {

	public abstract String getKey();

	protected abstract Settings getSettings();

	/**
	 * You can use this flag to required an additional license for users to run this entity. The admin will then have to
	 * add the license in the administration area. You may override {@link #getLicenseInfo(JsonObject)} to implement
	 * your custom license logic.
	 * 
	 * @return True if a license is required.
	 */
	public boolean isLicenseRequired() {
		return false;
	}

	/**
	 * Calls {@link #getLicenseInfo(JsonObject)} to check if the license is valid. Caches the result for 5 minutes.
	 * 
	 * @param licenseData
	 * @return True if the license is valid.
	 */
	public boolean isLicenseValid() {
		if (Play.mode.isDev()) {
			return true;
		}

		String cacheKey = CacheUtils.createTenantUniqueCacheKey("licenseValid-" + getClass().getSimpleName() + "-"
				+ getLicenseProductKey());

		Boolean valid = (Boolean) Cache.get(cacheKey);
		if (valid != null) {
			return valid;
		}

		Collection<LicenseInfo> info = getLicenseInfo();
		valid = info != null && !LicenseValidator.hasError(info);

		Cache.set(cacheKey, valid, "5min");
		return valid;
	}

	protected abstract String getLicenseProductKey();

	/**
	 * This method validates the current license. The default implementation works like the default license. It
	 * validates:
	 * <p>
	 * - Product key against {@link #getLicenseProductKey()}<br>
	 * - Coyo version<br>
	 * - Server ID<br>
	 * - Valid until date<br>
	 * - User limit
	 * </p>
	 * 
	 * @return License info items. License is considered valid if no items are flagged with an error.
	 */
	public Collection<LicenseInfo> getLicenseInfo() {
		String license = getLicense();
		if (StringUtils.isNotEmpty(license)) {
			JsonObject o = License.evaluate(license);
			if (o != null) {
				return LicenseValidator.info(getLicenseProductKey(), o);
			}
		}

		return null;
	}

	/**
	 * Saves the license for this entity. Invalidates the license cache.
	 * 
	 * @param license
	 */
	public final void saveLicense(String license) {
		Settings settings = getSettings();
		settings.setProperty("license", license);
		settings.save();

		Cache.delete(CacheUtils.createTenantUniqueCacheKey("licenseValid-" + getClass().getSimpleName() + "-"
				+ getLicenseProductKey()));
	}

	/**
	 * If the license has not been saved via user-interface, this method also checks for the license in the play
	 * configuration with property key "license.<getLicenseProductKey()>".
	 * 
	 * @return License string or null
	 */
	public final String getLicense() {
		String license = getSettings().getString("license");

		// check in conf
		if (StringUtils.isEmpty(license) && Play.configuration.containsKey("license." + getLicenseProductKey())) {
			return Play.configuration.getProperty("license." + getLicenseProductKey());
		}

		return license;
	}
}

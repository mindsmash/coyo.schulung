%{
    _arg = _caller._arg;
    _class = _caller._class;
    _controlGroupClass = _caller._controlGroupClass;
    _label = _caller._label;
    _help = _caller._help;
    _value = _caller._value;
    _required = _caller._required;
    _id = _caller._id;
    _error = _caller._error;

    if(!_id) _id = _arg?.slugify();
    if(!_help) _help = '';
    if(!_label) _label = _arg;
    if(!_error) _error = _arg;

    // determine error field key
    if(!play.data.validation.Validation.hasError(_error) && _error.endsWith('.id')) {
        _error = _error.substring(0, _error.length() - 3);
    }

    play.templates.BaseTemplate.layoutData.get().put("_id", _id);
    play.templates.BaseTemplate.layoutData.get().put("_help", _help);
    play.templates.BaseTemplate.layoutData.get().put("_label", _label);
}%

<div class="control-group #{if _controlGroupClass != null} &{_controlGroupClass} #{/} #{ifError _error} error#{/}#{if _required} required#{/}">
    <label class="control-label" for="${_id}">&{_label}</label>
    <div class="controls">
        #{doBody /}
        #{ifError _error}
            <span class="help-inline">#{error _error /}</span>
        #{/}
        #{if _help}
            <p class="help-block">&{_help}</p>
        #{/}
    </div>
</div>
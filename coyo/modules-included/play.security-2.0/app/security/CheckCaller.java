package security;

import injection.Inject;
import injection.InjectionSupport;

/**
 * Use this class anywhere for checking permissions. It uses a implementation of {@link CheckCallerInterface} for the
 * actual permission checks.
 *
 * @author mindsmash GmbH
 */
@InjectionSupport
public class CheckCaller {

	@Inject(configuration = "security.checkcaller.class", defaultClass = CheckCallerImpl.class)
	public static CheckCallerInterface checkCallerInterface;

	private CheckCaller() {
	}

	public static boolean check(String permission) {
		return checkCallerInterface.check(permission, new Object[0]);
	}

	public static boolean check(String permission, Object... args) {
		return checkCallerInterface.check(permission, args);
	}
}

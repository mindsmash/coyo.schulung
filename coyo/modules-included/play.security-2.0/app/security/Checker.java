package security;

import controllers.Security;

/**
 * The checker class is called for all permission checks. The security module
 * will always try to call a method that has the same name as the permission
 * being checked. These specific methods do also accept arguments that can be
 * passed to the check methods of the {@link Security} controller. If such
 * specific method does not exist the permitted() method is called.
 * 
 * You can define your application's checker in your play config using the
 * parameter "mindsmash.security.checker.class".
 * 
 * In addition to specifying a checker in the configuration you can also specify
 * a specific checker for each permission by using the following pattern for a
 * permission's name: "class.path#permissionName".
 * 
 * @author mindsmash GmbH
 */
public class Checker {

	public boolean permitted(String permission) {
		return false;
	}

	/**
	 * If implemented, this method is always checked first before the real
	 * permission check. If true is returned it will cancel the preceding check
	 * and grant permission.
	 * 
	 * This is useful for checking global super permissions, e.g. if a user is
	 * superadmin.
	 * 
	 * @return True to grant permission, false to to deny and null to continue
	 *         normal permission check.
	 */
	public Boolean beforeCheck() {
		return null;
	}
}

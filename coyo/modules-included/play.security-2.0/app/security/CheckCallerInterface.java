package security;

/**
 * Used for permission checks by {@link CheckCaller}.
 */
public interface CheckCallerInterface {
	boolean check(String permission);

	boolean check(String permission, Object... args);
}

package security;

import play.Logger;
import play.Play;
import util.Util;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@link CheckCallerInterface}.
 */
public class CheckCallerImpl implements CheckCallerInterface {

	private static final String DEFAULT_CHECKER = Play.configuration
			.getProperty("mindsmash.security.checker.class", Checker.class.getName());

	private static final Map<String, Checker> CHECKER_POOL = new HashMap<>();

	public boolean check(String permission) {
		return check(permission, new Object[0]);
	}

	public boolean check(String permission, Object... args) {
		Logger.trace("[Security] Check with varargs called for permission[%s] with args[%s]", permission,
				Util.implode(args, ","));

		try {
			Checker checker = getChecker(permission);

			// try the beforeCheck method
			Boolean beforeCheckResult = checker.beforeCheck();
			if (beforeCheckResult != null) {
				return beforeCheckResult;
			}

			// try to invoke rule directly
			Class[] types = new Class[args.length];
			for (int i = 0; i < args.length; i++) {
				types[i] = args[i].getClass();
			}

			String permissionName = getPermissionName(permission);
			Method m = null;

			try {
				m = checker.getClass().getMethod(permissionName, types);
			} catch (NoSuchMethodException e) {
				m = findMatchingMethod(checker.getClass(), permissionName, types);
			}

			if (m != null) {
				m.setAccessible(true);
				boolean result = (Boolean) m.invoke(checker, args);

				Logger.trace("[Security] Check for permission[%s] returned[%s]", permission, result);
				return result;
			} else if (args.length == 0) {
				// try to use default method if no args
				return getChecker(permission).permitted(permission);
			} else {
				Logger.warn(
						"[Security] Check with args could not be performed because your checker has no method implementation for this permission [%s] with arguments [%s]",
						permission, Arrays.asList(types));
			}
		} catch (Exception e) {
			Logger.error(e, "[Security] Error checking permission [%s], returning false as if check did not pass",
					permission);
		}
		return false;
	}

	private static Method findMatchingMethod(Class clazz, String methodName, Class[] argumentTypes) {
		for (Method method : clazz.getMethods()) {
			// check method name
			if (methodName.equals(method.getName())) {
				// check param types
				Class[] types = method.getParameterTypes();
				if (types.length == argumentTypes.length) {
					boolean mismatch = false;
					for (int i = 0; i < types.length; i++) {
						// check assignable, not equal!
						if (!types[i].isAssignableFrom(argumentTypes[i])) {
							mismatch = true;
							break;
						}
					}

					if (!mismatch) {
						return method;
					}
				}
			}
		}
		return null;
	}

	private static String getPermissionName(String permission) {
		if (permission.contains("#")) {
			return permission.substring(permission.indexOf("#") + 1);
		}

		return permission;
	}

	private static Checker getChecker(String permission) throws Exception {
		// check if class is given by permission
		// (pattern: class.path#permission)
		if (permission.contains("#")) {
			String classpath = permission.substring(0, permission.indexOf("#"));
			return loadChecker(classpath);
		}

		return loadChecker(DEFAULT_CHECKER);
	}

	private static Checker loadChecker(String classpath) throws Exception {
		if (!CHECKER_POOL.containsKey(classpath)) {
			CHECKER_POOL.put(classpath, (Checker) Class.forName(classpath).newInstance());
			Logger.info("loaded checker: %s", classpath);
		}
		return CHECKER_POOL.get(classpath);
	}
}

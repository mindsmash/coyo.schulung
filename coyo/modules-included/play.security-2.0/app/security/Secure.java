package security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import controllers.Security;

/**
 * Annotate your controllers and methods with this annotation and load the
 * {@link Security} controller to enable automatic permissions checking.
 * 
 * @author mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface Secure {

	String[] permissions();

	/**
	 * Grant access if only one of the given permissions is availab.e.
	 */
	boolean or() default false;

	String flash() default "";

	String redirect() default "";

	String[] only() default {};

	String[] unless() default {};

	/**
	 * Maps action parameters to the permission check in the order you specify
	 * them here.
	 */
	String[] params() default {};
}

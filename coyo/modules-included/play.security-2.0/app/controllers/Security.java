package controllers;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

import play.i18n.Messages;
import play.mvc.ActionInvoker;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import play.utils.Java;
import security.CheckCaller;
import security.Secure;
import security.SecureController;
import security.SecureMethod;

/**
 * The Security controller checks for permissions. You can annotate your
 * controllers and methods with the {@link Secure} annotation and have the
 * Security controller check them automatically. In order for the annotation
 * checks to be executed you need to load the Security controller by annotating
 * your controllers with the {@link With} annotation.
 * 
 * Additionally you can call the static check methods from anywhere in your
 * controllers.
 * 
 * Use the "secure" tag in your template.
 * Example:
 * #{secure "manageSomethingPermission",
 * args:[2,true]}HTML#{/secure}#{else}...#{/else}
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class Security extends Controller {

	@Before(priority = 100)
	static void checkAnnotations() throws ClassNotFoundException, Exception {
		// try action annotation
		Secure secure = getActionAnnotation(Secure.class);
		if (secure != null) {
			checkAnnotation(secure);
		}

		// try controller annotation
		secure = getControllerInheritedAnnotation(Secure.class);
		if (secure != null) {

			// if only contains current action
			if (secure.only().length == 0 || Arrays.asList(secure.only()).contains(request.actionMethod)) {

				// if unless does not contain current action
				if (secure.unless().length == 0 || !Arrays.asList(secure.unless()).contains(request.actionMethod)) {
					checkAnnotation(secure);
				}
			}
		}

		// try controller map annotation
		SecureController secureCtrl = getControllerInheritedAnnotation(SecureController.class);
		if (secureCtrl != null) {
			for(SecureMethod secureMethod : secureCtrl.value()) {
				if(secureMethod.method().equals(request.actionMethod)) {
					checkAnnotation(secureMethod.secure());
					break;
				}
			}
		}
	}

	private static void checkAnnotation(Secure secure) throws ClassNotFoundException, Exception {
		// check permissions by or/and
		boolean fail = secure.or();

		// default args
		Object[] args = new Object[0];

		// map action parameters
		if (secure.params().length > 0) {
			args = new Object[secure.params().length];

			// get action parameters
			Method method = (Method) ActionInvoker.getActionMethod(request.action)[1];
			Map<Object, Object> actionParams = util.Util.arraysToMap(Java.parameterNames(method),
					ActionInvoker.getActionMethodArgs(method, null));

			// only use those requested
			int i = 0;
			for (String paramName : secure.params()) {
				args[i++] = actionParams.get(paramName);
			}
		}

		// check permissions
		for (String permission : secure.permissions()) {
			if (CheckCaller.check(permission, args) == secure.or()) {
				fail = !secure.or();
				break;
			}
		}

		if (fail) {
			cancel(secure.flash(), secure.redirect());
		}
	}

	private static void cancel(String flash, String redirect) {
		if (flash != null && !flash.equals("")) {
			Security.flash.error(Messages.get(flash));
		}
		if (redirect != null && !redirect.equals("")) {
			redirect(redirect);
		} else {
			forbidden();
		}
	}

	@Util
	public static void checkAndRedirectWithFlash(String permission, String redirect, String flash, Object... args) {
		if (!CheckCaller.check(permission, args)) {
			cancel(flash, redirect);
		}
	}

	@Util
	public static void checkAndRedirect(String permission, String redirect, Object... args) {
		if (!CheckCaller.check(permission, args)) {
			cancel(null, redirect);
		}
	}

	@Util
	public static void checkAndCancel(String permission, Object... args) {
		if (!CheckCaller.check(permission, args)) {
			cancel(null, null);
		}
	}

	@Util
	public static boolean check(String permission, Object... args) {
		return CheckCaller.check(permission, args);
	}

}

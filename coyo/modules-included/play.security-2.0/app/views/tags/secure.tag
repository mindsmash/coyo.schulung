%{executeNextElse=true;}%
#{if _args}
	#{if security.CheckCaller.check(_arg, _args.toArray())}
		%{executeNextElse=false;}%
		#{doBody/}
	#{/if}
#{/if}
#{else}
	#{if security.CheckCaller.check(_arg)}
		%{executeNextElse=false;}%
		#{doBody/}
	#{/if}
#{/else}
%{play.templates.TagContext.parent().data.put('_executeNextElse', executeNextElse);}%
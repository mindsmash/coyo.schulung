%{
if(!_length) _length = 20;
if(_filter == null) _filter = true;
if(_pagination == null) _pagination = true;
if(_sort == null) _sort = true;
if(!_sortColumn) _sortColumn = 0;
if(!_sortOrder) _sortOrder = "asc";
if(!_adapter) _adapter = play.configuration.getProperty('datatables.adapter.default', 'bootstrap3');
}%

#{addScript 'jquery.datatables/jquery.datatables.min', dir:'libs' /}

#{if _adapter == 'bootstrap3'}
#{addScript 'jquery.datatables/bootstrap3-adapter/js/datatables', dir:'libs' /}
#{addCss 'jquery.datatables/bootstrap3-adapter/css/datatables', dir:'libs' /}
#{/}#{elseif _adapter == 'bootstrap2'}
#{addScript 'jquery.datatables/bootstrap2-adapter/scripts/jquery.dataTables.bootstrap', dir:'libs' /}
#{addLess 'jquery.datatables/bootstrap2-adapter/stylesheets/tag.datatables', dir:'libs' /}
#{/}

<div id="${_arg}">
	#{doBody /}
</div>

<script type="text/javascript">
	var datatableSettings = {
				#{if _ajax}
				'sAjaxSource': '${_ajax}',
				'bServerSide': true,
				#{if _ajaxMethod}'sServerMethod': '${_ajaxMethod}',#{/}
				#{/}#{elseif _dynamic}
				'aaData': dataTablesData,
				'bDeferRender': true,
				"bSortClasses": false,
				#{/}
				'aoColumns' : [],
				'aoColumnDefs' : [],
				'iDisplayLength': ${_length},
				'sDom': 'ftp',
				'bLengthChange': false,
				#{if _adapter == 'bootstrap2'}'sPaginationType': 'bootstrap',#{/}
				'bInfo': false,
				'bFilter': ${_filter},
				'bPaginate': ${_pagination},
				'bSort': ${_sort},
				'aaSorting': [[ ${_sortColumn}, "${_sortOrder}" ]],
				'oLanguage': {
					'oAria': {
						'sSortAscending': '&{'datatables.sortAscending'}',
					'sSortDescending': '&{'datatables.sortDescending'}'
			},
			'oPaginate': {
		'sFirst': '&{'datatables.first'}',
				'sLast': '&{'datatables.last'}',
				'sNext': '&{'datatables.next'}',
				'sPrevious': '&{'datatables.previous'}'
	},
	'sEmptyTable': '&{'datatables.empty'}',
			'sProcessing': '&{'datatables.processing'}',
			'sSearch': '&{'datatables.search'}'
	}
	};
	
	// plugin for ordering select boxes
	// see: http://datatables.net/plug-ins/sorting/custom-data-source/dom-select
	jQuery.fn.dataTable.ext.order['dom-select'] = function(settings, col) {
	    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
	        return $('select', td).val();
	    } );
	};
	
	// plugin for sorting dates
	// see: http://www.datatables.net/blog/2014-12-18
	jQuery.fn.dataTable.moment = function (format, locale) {
	    var types = $.fn.dataTable.ext.type;
	 
	    // Add type detection
	    types.detect.unshift( function ( d ) {
	        return moment( d, format, locale, true ).isValid() ?
	            'moment-'+format :
	            null;
	    } );
	 
	    // Add sorting method - use an integer for the sorting
	    types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
	        return moment( d, format, locale, true ).unix();
	    };
	};

	$(document).ready(function() {
		// allow custom sorting by adding data-sType to TH
		// see http://datatables.net/plug-ins/sorting
		$('#${_arg} table thead th').each(function(i, el) {
			var $el = $(el),
				typeAttr = $el.attr('data-sType'),
				column = null;
			
			if (typeof typeAttr !== "undefined") {
				column = {"orderDataType" : typeAttr, 'sType' : typeAttr};
			}
			datatableSettings.aoColumns.push(column);
		});
	
		/* register german and english dateformat for sorting */
		$.fn.dataTable.moment('DD.MM.YYYY - H:mm');
		$.fn.dataTable.moment('YYYY-MM-DD - h:mm A');

		#{if _settings}datatableSettings = $.extend(datatableSettings, ${_settings});#{/}
		$('#${_arg} table').dataTable(datatableSettings);

		#{if _pagination}
		// if custom pagination location defined
		if($('#${_arg} .dataTables_paginate_wrapper').size() > 0) {
			$('#${_arg} .dataTables_paginate').appendTo($('#${_arg} .dataTables_paginate_wrapper'));
		}
		#{/}

		#{if _filter}
		// if custom filter location defined
		if($('#${_arg} .dataTables_filter_wrapper').size() > 0) {
			$('#${_arg} .dataTables_filter').appendTo($('#${_arg} .dataTables_filter_wrapper'));
		}
		#{/}
	});
</script>
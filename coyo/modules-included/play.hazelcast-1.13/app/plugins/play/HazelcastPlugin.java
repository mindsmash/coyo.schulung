package plugins.play;

import hazelcast.Configurator;
import hazelcast.cache.HazelcastCacheImpl;
import injection.Inject;
import injection.InjectionSupport;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.cache.Cache;
import play.cache.CacheImpl;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 * Hazelcast Plugin for Play.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
@InjectionSupport
public class HazelcastPlugin extends PlayPlugin {

	public static final boolean ACTIVE = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.hazelcast.active", "false"));

	public static final boolean CACHE_ACTIVE = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.hazelcast.cache.active", "false"));

	public static final String CLUSTER_NAME = Play.configuration.getProperty("mindsmash.hazelcast.clusterName",
			"default");
	public static final String CLUSTER_PASSWORD = Play.configuration.getProperty("mindsmash.hazelcast.clusterPassword",
			"default");

	@Inject(configuration = "mindsmash.hazelcast.configurator", defaultClass = Configurator.class)
	private static Configurator configurator;

	private static HazelcastInstance hazel;

	public static HazelcastInstance getHazel() {
		if (!ACTIVE) {
			throw new IllegalStateException("hazelcast plugin is not active");
		}

		if (null == hazel) {
			synchronized (HazelcastPlugin.class) {
				if (null == hazel) {
					hazel = Hazelcast.newHazelcastInstance(createConfig());
				}
			}
		}
		return hazel;
	}

	private static Config createConfig() {
		Config config = new Config();
		config.setProperty("hazelcast.logging.type", "log4j");
		config.setClassLoader(Play.classloader);
		config.getGroupConfig().setName(CLUSTER_NAME);
		config.getGroupConfig().setPassword(CLUSTER_PASSWORD);
		configurator.configure(config);
		return config;
	}

	@Override
	public void onApplicationStart() {
		if (ACTIVE) {
			System.setProperty("hazelcast.logging.type", "log4j");
			getHazel();
			configurator.afterStart(hazel);

			if (CACHE_ACTIVE) {
				Logger.info("initializing hazelcast cache");

				Cache.stop();
				Cache.forcedCacheImpl = (CacheImpl) HazelcastCacheImpl.getInstance();
				Cache.init();
			}
		} else {
			Logger.info("hazelcast plugin is not active");
		}
	}

	@Override
	public void onApplicationStop() {
		if (ACTIVE && isStarted()) {
			configurator.beforeStop(hazel);
			Hazelcast.shutdownAll();
			hazel = null;
		}
	}

	public boolean isStarted() {
		return Hazelcast.getAllHazelcastInstances().size() > 0;
	}
}

package hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.HazelcastInstance;

/**
 * Can be extended in your project to do more detailed hazelcast configuration.
 * See http://hazelcast.org/docs/3.0/manual/html-single/#ConfigGroup for
 * details.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class Configurator {

	public void afterStart(HazelcastInstance instance) {
	}
	
	public void beforeStop(HazelcastInstance instance) {
	}
	
	public void configure(Config config) {
	}
}

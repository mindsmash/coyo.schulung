package hazelcast;

import java.io.IOException;

import models.AbstractTenant;
import multitenancy.MTA;
import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Job;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

/**
 * Extend this class for distributed tenant tasks. This one is usually
 * implemented inline in your {@link TenantTaskScheduler}.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 * @param <V>
 */
abstract public class TenantTask<V> extends Job implements DataSerializable {

	protected Long tenantId;

	public TenantTask() {
	}

	public TenantTask(final Long tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
		out.writeLong(tenantId);
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
		tenantId = in.readLong();
	}

	@Override
	public void doJob() throws Exception {
		if (null == tenantId) {
			throw new IllegalArgumentException("Cannot execute tenant task without tenant");
		}

		AbstractTenant tenant = AbstractTenant.findById(tenantId);

		try {
			MTA.activateMultitenancy(tenant);
			Logger.debug("Running %s for tenant [%s]", getClass().getName(), tenant);

			beforeExecution();
			execute(tenant);
			afterExecution();
		} catch (Exception e) {
			Logger.error(e, "Error running %s for tenant [%s]", getClass().getName(), tenant);
			throw e;
		}
	}

	protected void beforeExecution() {
		((org.hibernate.Session) JPA.em().getDelegate()).enableFilter("softDelete");
		((org.hibernate.Session) JPA.em().getDelegate()).enableFilter("external");
	}

	abstract public void execute(final AbstractTenant tenant) throws Exception;

	protected void afterExecution() {
	}
}

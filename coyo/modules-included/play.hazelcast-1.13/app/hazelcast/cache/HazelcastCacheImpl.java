package hazelcast.cache;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.IMap;
import play.cache.CacheImpl;
import play.exceptions.CacheException;
import plugins.play.HazelcastPlugin;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HazelcastCacheImpl implements CacheImpl {
	private static HazelcastInstance manager = HazelcastPlugin.getHazel();
	private static HazelcastCacheImpl instance = new HazelcastCacheImpl();
	private static final String CACHE_KEY = "cache";

	public synchronized static HazelcastCacheImpl getInstance() {
		if (manager == null) {
			manager = HazelcastPlugin.getHazel();
		}
		if (instance == null) {
			instance = new HazelcastCacheImpl();
		}

		return instance;
	}

	/**
	 * Expiration is in SECONDS
	 */
	public void add(String key, Object value, int expiration) {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);

		map.putIfAbsent(key, value, expiration, TimeUnit.SECONDS);
	}

	public synchronized void clear() {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);
		map.clear();
	}

	public long decr(String key, int by) {
		final IAtomicLong count = manager.getAtomicLong(key);
		count.set(count.get() - by);

		return count.get();
	}

	public void delete(String key) {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);
		try {
			map.lock(key);
			map.remove(key);
		} finally {
			map.unlock(key);
		}
	}

	public Object get(String key) {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);

		return map.get(key);
	}

	public Map<String, Object> get(String[] keys) {
		Map<String, Object> result = new HashMap<String, Object>(keys.length);
		for (String key : keys) {
			result.put(key, get(key));
		}
		return result;
	}

	public long incr(String key, int by) {
		IAtomicLong count = manager.getAtomicLong(key);
		count.set(count.get() + by);

		return count.get();
	}

	public void replace(String key, Object value, int expiration) {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);
		try {
			map.lock(key);
			if (map.containsKey(key)) {
				map.put(key, value, expiration, TimeUnit.SECONDS);
			}
		} finally {
			map.unlock(key);
		}
	}

	public boolean safeAdd(String key, Object value, int expiration) {
		try {
			add(key, value, expiration);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean safeDelete(String key) {
		try {
			delete(key);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean safeReplace(String key, Object value, int expiration) {
		try {
			replace(key, value, expiration);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean safeSet(String key, Object value, int expiration) {
		try {
			set(key, value, expiration);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void set(String key, Object value, int expiration) {
		final IMap<String, Object> map = manager.getMap(CACHE_KEY);
		try {
			map.lock(key);
			map.put(key, value, expiration, TimeUnit.SECONDS);
		} finally {
			map.unlock(key);
		}
	}

	public void stop() {
		instance = null;
		manager = null;
	}

	/**
	 * Utility that check that an object is serializable.
	 */
	static void checkSerializable(Object value) {
		if (value != null && !(value instanceof Serializable)) {
			throw new CacheException("Cannot cache a non-serializable value of type " + value.getClass().getName(),
					new NotSerializableException(value.getClass().getName()));
		}
	}

}

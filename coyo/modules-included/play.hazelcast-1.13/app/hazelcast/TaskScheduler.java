package hazelcast;

import com.hazelcast.core.IAtomicLong;
import models.AbstractTenant;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import play.libs.Time.CronExpression;
import plugins.play.HazelcastPlugin;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/**
 * Have your cron-planned jobs extend this scheduler. Your jobs will then create
 * {@link TenantTask}
 */
abstract public class TaskScheduler extends Job {

	private static final String EXECUTOR_SERVICE_NAME = "default";

	private IAtomicLong last;

	public TaskScheduler() {
		if (HazelcastPlugin.ACTIVE) {
			last = HazelcastPlugin.getHazel().getAtomicLong(getClass().getName());
			last.compareAndSet(0, new Date().getTime());
		}
	}

	@Override
	public void doJob() throws Exception {
		if (HazelcastPlugin.ACTIVE) {
			final Date now = new Date();
			final On on = getClass().getAnnotation(On.class);
			final CronExpression cron = new CronExpression(on.value());
			final Date nextInvalidTimeAfter = cron.getNextInvalidTimeAfter(now);

			long lastAsTime = last.get();
			if ((lastAsTime == 0 || lastAsTime < nextInvalidTimeAfter.getTime()) && last
					.compareAndSet(lastAsTime, nextInvalidTimeAfter.getTime())) {

				Logger.debug("%s scheduled for %s / last executed at %s", getClass().getName(), now,
						new Date(lastAsTime));

				final ExecutorService executor = HazelcastPlugin.getHazel().getExecutorService(EXECUTOR_SERVICE_NAME);
				executor.submit((Callable) createTask());
			} else {
				Logger.debug("%s skips for %s / last executed at %s", getClass().getName(), now, new Date(lastAsTime));
			}
		} else {
			createTask().now();
		}
	}

	abstract protected Task createTask();
}
package hazelcast;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;
import models.AbstractTenant;
import multitenancy.MTA;
import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Job;

import java.io.IOException;

/**
 * Extend this class for distributed tasks. This one is usually
 * implemented inline in your {@link TaskScheduler}.
 *
 * @author Daniel Busch, mindsmash GmbH
 */
abstract public class Task<V> extends Job implements DataSerializable {

	public Task() {
	}

	@Override
	public void writeData(ObjectDataOutput out) throws IOException {
	}

	@Override
	public void readData(ObjectDataInput in) throws IOException {
	}

	@Override
	public void doJob() throws Exception {
		beforeExecution();
		execute();
		afterExecution();
	}

	protected void beforeExecution() {
		((org.hibernate.Session) JPA.em().getDelegate()).enableFilter("softDelete");
		((org.hibernate.Session) JPA.em().getDelegate()).enableFilter("external");
	}

	protected void afterExecution() {
	}
}

package hazelcast;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import models.AbstractTenant;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import play.libs.Time.CronExpression;
import plugins.play.HazelcastPlugin;

import com.hazelcast.core.IAtomicLong;

/**
 * Have your cron-planned jobs extend this scheduler. Your jobs will then create
 * {@link TenantTask}
 * 
 * @author Jan Marquardt, Daniel Busch, mindsmash GmbH
 */
abstract public class TenantTaskScheduler extends Job {

	private static final String EXECUTOR_SERVICE_NAME = "default";

	private IAtomicLong last;

	public TenantTaskScheduler() {
		if (HazelcastPlugin.ACTIVE) {
			last = HazelcastPlugin.getHazel().getAtomicLong(getClass().getName());
			last.compareAndSet(0, new Date().getTime());
		}
	}

	@Override
	public void doJob() throws Exception {
		if (HazelcastPlugin.ACTIVE) {
			Date now = new Date();

			On on = getClass().getAnnotation(On.class);
			CronExpression cron = new CronExpression(on.value());
			Date nextInvalidTimeAfter = cron.getNextInvalidTimeAfter(now);

			long lastAsTime = last.get();
			if ((lastAsTime == 0 || lastAsTime < nextInvalidTimeAfter.getTime())
					&& last.compareAndSet(lastAsTime, nextInvalidTimeAfter.getTime())) {

				Logger.debug("%s scheduled for %s / last executed at %s", getClass().getName(), now, new Date(
						lastAsTime));

				ExecutorService executor = HazelcastPlugin.getHazel().getExecutorService(EXECUTOR_SERVICE_NAME);

				List<AbstractTenant> tenants = AbstractTenant.findAll();
				for (AbstractTenant tenant : tenants) {
					if (tenant.isActive()) {
						executor.submit((Callable) createTask(tenant));
					}
				}
			} else {
				Logger.debug("%s skips for %s / last executed at %s", getClass().getName(), now, new Date(lastAsTime));
			}
		} else {
			List<AbstractTenant> tenants = AbstractTenant.findAll();
			for (AbstractTenant tenant : tenants) {
				if (tenant.isActive()) {
					createTask(tenant).now();
				}
			}
		}
	}

	abstract protected TenantTask createTask(AbstractTenant tenant);
}
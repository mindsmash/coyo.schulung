package binding;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.NoResultException;

import play.Logger;
import play.data.binding.Binder;
import play.data.binding.RootParamNode;
import play.db.Model;
import play.db.Model.Factory;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Bind Models from Json.
 *
 * @author Daniel Busch, mindsmash GmbH
 *
 */
public class JsonModelBinder {

	private JsonModelBinder() {

	}

	/*
	 * Bind JSON to an existing JPA Model.
	 */
	public static GenericModel bindModel(final String json, final String name, final Class clazz,
			final Annotation[] annotations, GenericModel persistentModel) {
		final Map<String, String[]> params = new HashMap<String, String[]>();
		final JsonObject jsonObject = (JsonObject) new JsonParser().parse(json);

		addParamsFromJsonObject(params, name, jsonObject);
		final RootParamNode newRootParamNode = RootParamNode.convert(params);

		if (null == persistentModel) {
			Factory factory = Model.Manager.factoryFor(clazz);
			String keyFieldName = factory.keyName();
			final JsonElement idElement = jsonObject.get(keyFieldName);
			Object id = null;
			try {
				id = idElement == null ? null : Binder.directBind(idElement.getAsString(), factory.keyType());
			} catch (Exception e) {
				Logger.warn(e, "Cannot bind json body because of failed directBind of ID field");
			}
			if (null != id) {
				try {
					final StringBuilder dql = new StringBuilder("select o from ").append(clazz.getName())
							.append(" o where ").append(keyFieldName).append(" = ?");
					persistentModel = (GenericModel) JPA.em().createQuery(dql.toString()).setParameter(1, id)
							.getSingleResult();
				} catch (NoResultException e) {
					// okay per business logic
				}
			}
		}

		if (null != persistentModel) {
			return GenericModel.edit(newRootParamNode, name, persistentModel, annotations);
		}

		// new entity
		return GenericModel.create(newRootParamNode, name, clazz, annotations);
	}

	/*
	 * Bind JSON to a new or existing JPA Model.
	 */
	public static GenericModel bindModel(final String json, final String name, final Class clazz,
			final Annotation[] annotations) {
		return bindModel(json, name, clazz, annotations, null);
	}

	/*
	 * Bind JSON to a new or existing JPA Model.
	 */
	public static GenericModel bindModel(final String json, final Class clazz) {
		return bindModel(json, "object", clazz, null, null);
	}

	/*
	 * Build root param node from json object to use the default play! binder.
	 */
	private static void addParamsFromJsonObject(final Map<String, String[]> params, final String rootName,
			final JsonObject jsonObject) {
		final Set<Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
		for (final Entry<String, JsonElement> entry : entrySet) {
			final String attributeName = entry.getKey();
			final JsonElement jsonElement = entry.getValue();
			handleJsonElement(params, rootName, attributeName, jsonElement);
		}
	}

	/*
	 * Handle a specific json element. Called from addParamsFromJsonObject.
	 */
	private static void handleJsonElement(final Map<String, String[]> params, final String name,
			final String attributeName, final JsonElement attributeValue) {
		// case 1: primitive value
		if (attributeValue.isJsonPrimitive()) {
			params.put(name + "." + attributeName, new String[] { attributeValue.getAsString() });
		}
		// case 2: object, handle it recursively
		else if (attributeValue.isJsonObject()) {
			addParamsFromJsonObject(params, name + "." + attributeName, attributeValue.getAsJsonObject());
		}
		// case 3: array, handle each of its elements
		else if (attributeValue.isJsonArray()) {
			final JsonArray jsonArray = attributeValue.getAsJsonArray();

			if (jsonArray != null && jsonArray.size() < 1) {
				params.put(name + "." + attributeName, new String[] { "[]" });
			}
			else {
				int i = 0;
				for (final JsonElement jsonElement : jsonArray) {
					handleJsonElement(params, name, String.format("%s[%d]", attributeName, i++), jsonElement);
				}
			}
		}
	}
}

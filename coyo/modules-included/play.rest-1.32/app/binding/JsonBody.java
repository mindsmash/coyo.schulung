package binding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks an action param to be bound as JSON from Request Body.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface JsonBody {

	/**
	 * The name of the key field of the model to be bound from Request Body.
	 * 
	 * The default value is 'id'.
	 * 
	 * @return
	 */
	String value() default "id";
}

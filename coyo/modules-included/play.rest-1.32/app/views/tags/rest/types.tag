%{
    models = [];
    for(controllerClass in play.Play.classloader.getAssignableClasses(_('controllers.REST'))) {
        resourceModel = _('controllers.REST$ObjectType').get(controllerClass)
        if(resourceModel != null) {
            models.add(resourceModel)
        }
    }
}%

%{ models.eachWithIndex() { item, i -> }%
	%{
		attrs = [:]
		attrs.put('type', item)
	}%
    #{doBody vars:attrs /}
%{ } }%
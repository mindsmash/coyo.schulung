package pagination;

import org.apache.commons.lang.StringUtils;

public class Order {
	private String column;
	private String columnRename;
	private Direction direction;

	/**
	 * The pagination directions.
	 */
	public static enum Direction {
		ASC, DESC;
	}

	public Order(String column, String columnRename, Direction direction) {
		this.column = column;
		this.columnRename = columnRename;
		this.direction = direction;
	}

	public String getColumn() {
		return column;
	}

	public String getColumnRename() {
		return columnRename;
	}

	public Direction getDirection() {
		return direction;
	}

	static Order createFromRequest(String column) {
		Order order = new Order(column, "", Direction.ASC);
		if (column.startsWith("!")) {
			order.direction = Direction.DESC;
			order.column = column.substring(1);
		}
		String[] splits = order.column.split("@", 2);
		if (splits.length > 1) {
			order.column = splits[0];
			order.columnRename = splits[1];
		}
		return order;
	}

	public String getRequestColumn() {
		String out = getColumn();
		if (StringUtils.isNotEmpty(getColumnRename())) {
			out += "@" + getColumnRename();
		}
		return out;
	}
}

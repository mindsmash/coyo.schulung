package pagination;

import java.util.List;

import pagination.Filter.ConcatType;
import play.db.jpa.GenericModel;

/**
 * Adapter for Data Backends.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public interface PaginationAdapter<T extends GenericModel> {

	/**
	 * Returns the total number of items found by the pagination adapter. This
	 * value is NOT the number of entities on the current page.
	 * 
	 * @return The total number of entities found (n >= 0).
	 */
	public long getTotalCount();

	/**
	 * Set the current page number. Note: The adapter's page number output may
	 * differ from its input (e.g. 1 for negative page numbers).
	 * 
	 * @param page
	 *            The page number.
	 * @return The actual page number set by this adapter (n > 0).
	 */
	public int setPage(final Integer page);

	/**
	 * Set the current page size. Note: The adapter's page size output may
	 * differ from its input (e.g. null or 1 for negative page sizes).
	 * 
	 * @param pageSize
	 *            The page size.
	 * @return The actual page size set by this adapter (n > 0 || n == null).
	 */
	public Integer setPageSize(final Integer pageSize);

	/**
	 * Adds a new order.
	 * 
	 * @param order
	 *            The new order.
	 * @throws NoSuchFieldException
	 *             if the field name is invalid.
	 */
	public void addOrder(Order order) throws NoSuchFieldException;

	/**
	 * Adds a new filter.
	 * 
	 * @param order
	 *            The new filter.
	 * @throws NoSuchFieldException
	 *             if the field name is invalid.
	 */
	public void addFilter(Filter filter) throws NoSuchFieldException;

	/**
	 * Set the concatenation type for all filters (i.e. AND or OR).
	 * 
	 * @param type
	 *            The concatenation type.
	 * @return The actual concatenation type set by this adapter (type != null).
	 */
	public ConcatType setFilterType(ConcatType type);

	public List<T> getItems();
}

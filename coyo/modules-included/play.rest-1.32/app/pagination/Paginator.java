package pagination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pagination.Filter.ConcatType;
import play.db.jpa.GenericModel;
import play.mvc.Http.Request;
import play.mvc.Http.Response;

/**
 * Pagination helper for requests.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public class Paginator<T extends GenericModel> implements Serializable {
	private PaginationAdapter<T> adapter;

	private static final String PAGE_KEY = "_page";
	private static final String LIMIT_KEY = "_pageSize";
	private static final String ORDER_KEY = "_orderBy";
	private static final String FILTER_KEY = "_filterBy";

	private int page = 1;
	private Integer pageSize = 10;
	private long totalCount = -1;
	private LinkedList<Order> orders = new LinkedList<Order>();
	private ArrayList<Filter> filters = new ArrayList<Filter>();

	/**
	 * Create a new PaginatedModelList.
	 */
	public Paginator(final PaginationAdapter<T> adapter) {
		this.adapter = adapter;
		setPage(page);
		setPageSize(pageSize);
		setOrders(orders);
		setFilters(filters);
	}

	/**
	 * Create a new PaginatedModelList, avoid duplicate calling of setters
	 */
	public Paginator(final PaginationAdapter<T> adapter, final int page, final Integer pageSize, final LinkedList<Order> orders, final ArrayList<Filter> filters, final ConcatType filterType) {
		this.adapter = adapter;
		setPage(page);
		setPageSize(pageSize);
		setOrders(orders);
		setFilterType(filterType);
		setFilters(filters);
	}

	/**
	 * Returns the current page.
	 * 
	 * @return
	 */
	public Integer getPage() {
		return page;
	}

	protected Paginator setPage(final Integer page) {
		this.page = adapter.setPage(page);
		return this;
	}

	/**
	 * Returns the page size.
	 * 
	 * @return
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	protected Paginator setPageSize(final Integer pageSize) {
		this.pageSize = adapter.setPageSize(pageSize);
		return this;
	}

	/**
	 * Returns the order columns.
	 * 
	 * @return
	 */
	public List<Order> getOrders() {
		return orders;
	}

	protected Paginator setOrders(final LinkedList<Order> orders) {
		this.orders.clear();
		for (Order order : orders) {
			try {
				adapter.addOrder(order);
				this.orders.add(order);
			} catch (NoSuchFieldException e) {
			} // ignore invalid query parameter
		}
		return this;
	}

	/**
	 * Returns the filters.
	 * 
	 * @return
	 */
	public List<Filter> getFilters() {
		return filters;
	}

	protected Paginator setFilters(final ArrayList<Filter> filters) {
		this.filters.clear();
		for (Filter filter : filters) {
			try {
				adapter.addFilter(filter);
				this.filters.add(filter);
			} catch (NoSuchFieldException e) {
			} // ignore invalid query parameter
		}
		return this;
	}

	protected Paginator setFilterType(final ConcatType type) {
		this.adapter.setFilterType(type);
		return this;
	}

	/**
	 * Returns the total number of pages.
	 * 
	 * @return
	 */
	public long getPageCount() {
		final long numberOfRows = this.getTotalCount();
		final Integer pageSize = getPageSize();
		if (null == pageSize) {
			return 1;
		}
		final long numberOfFullPages = getTotalCount() / pageSize;
		long numberOfPages = numberOfFullPages;
		if (numberOfRows % pageSize > 0) {
			numberOfPages++;
		}
		return numberOfPages;
	}

	/**
	 * Get the total item count.
	 * 
	 * @return
	 */
	public long getTotalCount() {
		if (totalCount == -1) {
			totalCount = adapter.getTotalCount();
		}
		return totalCount;
	}

	/**
	 * Get paginated and ordered items.
	 * 
	 * @return
	 */
	public List<T> getItems() {
		return adapter.getItems();
	}

	/**
	 * Create {@link Paginator} from {@link Request}.
	 * 
	 * @return
	 */
	public static Paginator createFromRequest(final PaginationAdapter adapter) {
		Request request = Request.current();
		Map<String, String[]> params = request.params.getRootParamNodeFromRequest().originalParams;

		Integer page = request.params.get(PAGE_KEY, Integer.class);
		Integer pageSize = request.params.get(LIMIT_KEY, Integer.class);
		ConcatType concatType = request.params.get(FILTER_KEY, ConcatType.class);

		LinkedList<Order> orders = new LinkedList<Order>();

		String[] orderParams = request.params.getAll(ORDER_KEY);
		if (null != orderParams) {
			orderParams = orderParams[0].split(",");
			for (String column : orderParams) {
				orders.add(Order.createFromRequest(column));
			}
		}

		ArrayList<Filter> filters = new ArrayList<Filter>();
		for (String column : params.keySet()) {
			if (!column.startsWith("_") && !column.equals("body")) {
				String[] queries = params.get(column);
				filters.add(Filter.createFromRequest(column, queries));
			}
		}

		return new Paginator((PaginationAdapter)adapter, page == null ? 0 : page, (Integer)pageSize, (LinkedList<Order>)orders, (ArrayList<Filter>)filters, (ConcatType)concatType);
	}

	/**
	 * Write pagination and order infos to response headers.
	 */
	public void writeResponseHeaders() {
		Response response = Response.current();
		response.setHeader("Pagination-Page", Integer.toString(getPage()));
		if (null != getPageSize()) {
			response.setHeader("Pagination-Page-Size", Integer.toString(getPageSize()));
		}
		response.setHeader("Pagination-Page-Count", Long.toString(getPageCount()));
		response.setHeader("Pagination-Total-Count", Long.toString(getTotalCount()));

		if (!orders.isEmpty()) {
			String responseOrd = "";
			String responseDir = "";
			for (Order o : orders) {
				if (responseOrd.length() > 0) {
					responseOrd += ",";
					responseDir += ",";
				}
				responseOrd += o.getRequestColumn();
				responseDir += o.getDirection().toString();
			}
			response.setHeader("Order-By", responseOrd);
			response.setHeader("Order-Dir", responseDir);
		}
	}
}

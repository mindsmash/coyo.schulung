package pagination;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class Filter {
	private String column;
	private String columnRename;
	private Map<String, CompareType> values;

	/**
	 * The filter comparison types.
	 */
	public static enum CompareType {
		EQUALS, LIKE;
	}

	/**
	 * The filter concatenation types.
	 */
	public static enum ConcatType {
		AND, OR;
	}

	public Filter(String column, Map<String, CompareType> values) {
		this.column = column;
		this.columnRename = null;
		this.values = values;
	}

	public Filter(String column, String columnRename, Map<String, CompareType> values) {
		this.column = column;
		this.columnRename = columnRename;
		this.values = values;
	}

	public String getColumn() {
		return column;
	}

	public String getColumnRename() {
		return columnRename;
	}

	public Map<String, CompareType> getValues() {
		return values;
	}

	static Filter createFromRequest(String column, String[] queries) {
		Map<String, CompareType> values = new HashMap<String, CompareType>();
		for (String query : queries) {
			if (null == query) {
				continue;
			} else if (query.startsWith("!")) {
				values.put(query.substring(1), CompareType.EQUALS);
			} else {
				values.put(query, CompareType.LIKE);
			}
		}
		String[] splits = column.split("@", 2);
		return splits.length > 1 ? new Filter(splits[0], splits[1], values) : new Filter(column, null, values);
	}

	public String getRequestColumn() {
		String out = getColumn();
		if (StringUtils.isNotEmpty(getColumnRename())) {
			out += "@" + getColumnRename();
		}
		return out;
	}
}

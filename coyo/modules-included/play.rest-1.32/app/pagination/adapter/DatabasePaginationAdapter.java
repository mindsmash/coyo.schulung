package pagination.adapter;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Query;

import models.BaseModel;
import pagination.Filter;
import pagination.Filter.CompareType;
import pagination.Filter.ConcatType;
import pagination.Order;
import pagination.PaginationAdapter;
import play.data.binding.Binder;
import play.db.helper.JpqlSelect;
import play.db.helper.SqlSelect;
import play.db.helper.SqlSelect.Where;
import play.db.jpa.JPA;

/**
 * Database pagination adapter.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public class DatabasePaginationAdapter<T extends BaseModel> implements PaginationAdapter {
	private Class<T> entityClass;
	private SqlSelect select;
	private SqlSelect count;
	private int page = 1;
	private Integer pageSize = null;
	private String orderBy = "";
	private ConcatType filterBy = ConcatType.AND;

	public DatabasePaginationAdapter(final Class<T> clazz, final SqlSelect select, final SqlSelect count) {
		this.entityClass = clazz;
		this.select = select;
		this.count = count;
	}

	public DatabasePaginationAdapter(final Class<T> clazz) {
		this(clazz, new JpqlSelect().select("e").from(clazz.getCanonicalName() + " e"), new JpqlSelect().select(
				"COUNT(*)").from(clazz.getCanonicalName() + " e"));
	}

	private Field getField(final String name) {
		for (Field field : entityClass.getFields()) {
			if (field.getName().equals(name)) {
				return field;
			}
		}
		return null;
	}

	@Override
	public long getTotalCount() {
		Query query = JPA.em().createQuery(count.toString());
		for (int i = 0; i < select.getParams().size(); i++) {
			query.setParameter(i + 1, select.getParams().get(i));
		}
		return (Long) query.getSingleResult();
	}

	@Override
	public int setPage(final Integer page) {
		if (null == page || 0 >= page) {
			this.page = 1;
		} else {
			this.page = page;
		}
		return this.page;
	}

	@Override
	public Integer setPageSize(final Integer pageSize) {
		if (null == pageSize) {
			this.pageSize = 10000;
		} else if (0 >= pageSize) {
			this.pageSize = 10;
		} else {
			this.pageSize = pageSize;
		}
		return this.pageSize;
	}

	@Override
	public void addOrder(Order order) throws NoSuchFieldException {
		if (null != getField(order.getColumn())) {
			String orderBy = "e." + order.getColumn() + " " + order.getDirection().toString();
			if (this.orderBy.length() > 0) {
				this.orderBy += ", ";
			}
			this.orderBy += orderBy;
		} else {
			throw new NoSuchFieldException(order.getColumn());
		}
	}

	private void applyMatchFilter(Where where, String column, String value, CompareType type, Class clazz) {
		boolean isString = clazz.isAssignableFrom(String.class);
		value = type == CompareType.LIKE ? "%" + value.toLowerCase() + "%" : value;
		String sqlComp = type == CompareType.LIKE ? " LIKE ?" : "= ?";
		String sqlVal = isString ? "LOWER(e." + column + ")" : column;
		try {
			where.orWhere(sqlVal + sqlComp).param(Binder.directBind(value, clazz));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addFilter(final Filter filter) throws NoSuchFieldException {
		if (filter.getValues().size() == 0) {
			return;
		}

		Field field = getField(filter.getColumn());
		if (null != field) {
			Where where = select.where();
			Map<String, CompareType> values = filter.getValues();
			if (values.size() == 1) {
				Entry<String, CompareType> entry = values.entrySet().iterator().next();
				if (entry.getKey().matches("\\[[^;]*;[^;]*\\]")) {
					// given filter is a RANGE filter: a <= x <= b
					String[] vals = entry.getKey().split(";", 2);
					String val1 = vals[0].substring(1);
					try {
						if (!val1.isEmpty()) {
							where.andWhere("e." + filter.getColumn() + ">= ?").param(
									Binder.directBind(val1, field.getType()));
						}
						String val2 = vals[1].substring(0, vals[1].length() - 1);
						if (!val2.isEmpty()) {
							where.andWhere("e." + filter.getColumn() + "<= ?").param(
									Binder.directBind(val2, field.getType()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// given filter is a MATCH filter: x = a
					applyMatchFilter(where, filter.getColumn(), entry.getKey(), entry.getValue(), field.getType());
				}
			} else {
				// given filter is a MATCH filter with alternatives: x = a OR...
				for (String value : values.keySet()) {
					applyMatchFilter(where, filter.getColumn(), value, values.get(value), field.getType());
				}
			}
			if(filterBy == ConcatType.AND) {
				select.andWhere(where);
				count.andWhere(where);
			} else if (filterBy == ConcatType.OR) {
				select.orWhere(where);
				count.orWhere(where);
			}
		} else {
			throw new NoSuchFieldException(filter.getColumn());
		}
	}

	@Override
	public ConcatType setFilterType(ConcatType type) {
		if (null == type) {
			this.filterBy = ConcatType.AND;
		} else {
			this.filterBy = type;
		}
		return this.filterBy;
	}

	@Override
	public List<T> getItems() {
		if (orderBy.length() > 0) {
			select.orderBy(orderBy);
		}
		Query query = JPA.em().createQuery(select.toString());
		for (int i = 0; i < select.getParams().size(); i++) {
			query.setParameter(i + 1, select.getParams().get(i));
		}
		if (null != pageSize) {
			query.setFirstResult((page - 1) * pageSize);
			query.setMaxResults(pageSize);
		}
		return query.getResultList();
	}
}

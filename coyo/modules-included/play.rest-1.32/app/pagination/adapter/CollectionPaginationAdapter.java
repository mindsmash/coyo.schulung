package pagination.adapter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import models.BaseModel;
import pagination.Filter;
import pagination.Filter.ConcatType;
import pagination.Order;
import pagination.Order.Direction;
import pagination.PaginationAdapter;
import play.db.jpa.GenericModel;
import services.ReflectionHelper;

/**
 * Pagination adapter for Collections of models
 * CAUTION: For now only ordering and pagination, but not yet filtering is implemented!
 * 
 * @author Lennart Kruse, mindsmash GmbH
 * 
 * @param <T>
 */
public class CollectionPaginationAdapter<T1 extends BaseModel, T2 extends GenericModel> implements PaginationAdapter {
	private Class<T1> entityClass;
	private ArrayList<T2> source; // allows sorting
	private int page = 1;
	private ConcatType filterBy = ConcatType.AND;
	private Integer pageSize = DEFAULT_PAGE_SIZE;
	private static final Integer DEFAULT_PAGE_SIZE = 10;

	public CollectionPaginationAdapter(Class<T1> entityClass, Collection<T2> source) {
		this.entityClass = entityClass;
		this.source = new ArrayList<T2>(source);
	}

	@Override
	public long getTotalCount() {
		// filters are applied to this.source directly
		return this.source.size();
	}

	@Override
	public int setPage(Integer page) {
		if (page == null || page < 1) {
			this.page = 1;
		} else {
			this.page = page;
		}
		return this.page;
	}

	@Override
	public Integer setPageSize(Integer pageSize) {
		if (pageSize == null || pageSize < 1) {
			this.pageSize = DEFAULT_PAGE_SIZE;
		} else {
			this.pageSize = pageSize;
		}
		return this.pageSize;
	}

	@Override
	public void addOrder(final Order order) throws NoSuchFieldException {
		Collections.sort(source, new Comparator<T2>() {

			@Override
			public int compare(T2 obj1, T2 obj2) {
				// if the requested values are not Comparable, we are lost anyway
				Comparable val1 = null;
				Comparable val2 = null;

				try {
					Field field = ReflectionHelper.getField(entityClass, order.getColumn());
					val1 = (Comparable) field.get(obj1);
					val2 = (Comparable) field.get(obj2);
				} catch (Exception e1) {
					try {
						Method method = ReflectionHelper.getMethod(entityClass, order.getColumn());
						val1 = (Comparable) method.invoke(obj1);
						val2 = (Comparable) method.invoke(obj2);
					} catch (Exception e2) {
						/* no op */
					}
				}

				if (val1 == null && val2 != null) {
					return order.getDirection() == Direction.ASC ? -1 : 1;
				} else if (val1 != null && val2 == null) {
					return order.getDirection() == Direction.ASC ? 1 : -1;
				} else if (val1 != null && val2 != null) {
					return order.getDirection() == Direction.ASC ? val1.compareTo(val2) : val2.compareTo(val1);
				}

				return 0;
			}

		});
	}

//	// TODO performance, this is called several times
//	private void removeItems(String fieldName, String operator, String refString) {
//		Iterator<T2> i = source.iterator();
//		while (i.hasNext()) {
//			GenericModel obj = i.next();
//			String fieldValue = null;
//			try {
//				fieldValue = ReflectionHelper.getField(entityClass, fieldName).get(obj).toString();
//			} catch (Exception e) {
//				try {
//					fieldValue = ReflectionHelper.getMethod(entityClass, fieldName).invoke(obj).toString();
//				} catch (Exception e2) {
//					e.printStackTrace();
//				}
//			}
//
//			if (fieldValue != null) {
//				switch (operator) {
//				case "<":
//					if (fieldValue.compareTo(refString) < 0)
//						i.remove();
//					break;
//				case "=":
//					if (fieldValue.compareTo(refString) == 0)
//						i.remove();
//					break;
//				case ">":
//					if (fieldValue.compareTo(refString) > 0)
//						i.remove();
//					break;
//				}
//			}
//		}
//	}

	// TODO test!
	// TODO Use this.filterBy
	@Override
	public void addFilter(Filter filter) throws NoSuchFieldException {
//		if (filter.getValues().size() == 0) {
//			return;
//		}
//
//		if (!ReflectionHelper.isValidField(entityClass, filter.getColumn())) {
//			throw new NoSuchFieldException(filter.getColumn());
//		}
//
//		Map<String, CompareType> values = filter.getValues();
//		if (values.size() == 1) {
//			Entry<String, CompareType> entry = values.entrySet().iterator().next();
//
//			if (entry.getKey().matches("\\[[^;]*;[^;]*\\]")) {
//				// given filter is a RANGE filter: a <= x <= b
//				String[] vals = entry.getKey().split(";", 2);
//				String val = vals[0].substring(1);
//				if (!val.isEmpty()) {
//					removeItems(filter.getColumn(), ">", val);
//				}
//				val = vals[1].substring(0, vals[1].length() - 1);
//				if (!val.isEmpty()) {
//					removeItems(filter.getColumn(), "<", val);
//				}
//
//			} else {
//				// given filter is a MATCH filter: x = a
//				// TODO "LIKE"
//				removeItems(filter.getColumn(), "=", entry.getKey());
//			}
//
//		} else {
//			// given filter is a MATCH filter with alternatives: x = a OR...
//			for (String value : values.keySet()) {
//				removeItems(filter.getColumn(), "=", value);
//			}
//		}
	}

	@Override
	public ConcatType setFilterType(ConcatType type) {
		if (null == type) {
			this.filterBy = ConcatType.AND;
		} else {
			this.filterBy = type;
		}
		return this.filterBy;
	}

	@Override
	public List getItems() {
		int fromIndex = (page-1) * pageSize;
		int toIndex = Math.min(fromIndex + pageSize + 1, source.size()); // toIndex is exclusive!
		return source.subList(fromIndex, toIndex);
	}
}

package mvc.results;

import java.lang.reflect.Method;

import play.exceptions.UnexpectedException;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.Result;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSerializer;

public class RenderPolymorphJson extends Result {

	String json;

	public RenderPolymorphJson(Object o) {
		json = new Gson().toJson(o);
	}

	public RenderPolymorphJson(Object o, boolean hierachieal, GsonBuilder builder, JsonSerializer<?>... adapters) {
		GsonBuilder gson = builder;

		for (JsonSerializer<?> adapter : adapters) {
			Class<?> type = getMethod(adapter.getClass(), "serialize").getParameterTypes()[0];
			if (hierachieal) {
				gson.registerTypeHierarchyAdapter(type, adapter);
			} else {
				gson.registerTypeAdapter(type, adapter);
			}
		}
		json = gson.create().toJson(o);
	}

	public void apply(Request request, Response response) {
		try {
			String encoding = getEncoding();
			setContentTypeIfNotSet(response, "application/json; charset=" + encoding);
			response.out.write(json.getBytes(encoding));
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	private Method getMethod(Class<?> clazz, String name) {
		for (Method m : clazz.getDeclaredMethods()) {
			if (m.getName().equals(name) && !m.isBridge()) {
				return m;
			}
		}
		return null;
	}
}

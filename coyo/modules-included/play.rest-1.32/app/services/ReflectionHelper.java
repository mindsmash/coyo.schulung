package services;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;

public class ReflectionHelper {

	public static Field getField(final Class clazz, final String name) {
		String[] parts = name.split("\\.", 2);
		if (0 == parts.length) {
			return null;
		}
		try {
			Field field = clazz.getField(parts[0]);
			return 1 == parts.length ? field : getField(field.getType(), parts[1]);
		} catch (NoSuchFieldException e1) {
			/* no op */
		}
		return null;
	}

	public static Method getMethod(final Class clazz, final String name) {
		String[] parts = name.split("\\.", 2);
		if (0 == parts.length) {
			return null;
		}
		try {
			for (Method method : clazz.getMethods()) {
				String methodName = method.getName();
				if (methodName.equals(parts[0]) || methodName.equals("get" + StringUtils.capitalize(name))) {
					return method;
				}
			}
		} catch (SecurityException e3) {
			/* no op */
		}
		return null;
	}

	public static boolean isValidField(final Class clazz, final String name) {
		return getField(clazz, name) != null || getMethod(clazz, name) != null;
	}
}

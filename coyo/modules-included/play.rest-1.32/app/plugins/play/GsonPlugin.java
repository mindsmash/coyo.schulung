package plugins.play;

import java.util.List;

import json.RegisterTypeAdapter;
import json.RegisterTypeAdapterFactory;
import play.Play;
import play.PlayPlugin;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.exceptions.UnexpectedException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;

/**
 * Configure a project JSON instance supporting annotations for registering type
 * adapters and serialize/un-serialize strategies.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class GsonPlugin extends PlayPlugin {

	private static final String DATE_FORMAT = Play.configuration.getProperty("mindsmash.json.dateFormat", "yyyy-MM-dd");
	private static Gson gson;

	@Override
	public void onApplicationStart() {
		try {
			gson = buildGson();
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	private Gson buildGson() throws InstantiationException, IllegalAccessException {
		GsonBuilder builder = new GsonBuilder();
		builder.setDateFormat(DATE_FORMAT);

		List<ApplicationClass> adapters = Play.classes.getAnnotatedClasses(RegisterTypeAdapter.class);
		for (ApplicationClass adapter : adapters) {
			RegisterTypeAdapter annotation = adapter.javaClass.getAnnotation(RegisterTypeAdapter.class);
			builder.registerTypeAdapter(annotation.value(), adapter.javaClass.newInstance());
		}

		List<ApplicationClass> factories = Play.classes.getAnnotatedClasses(RegisterTypeAdapterFactory.class);
		for (ApplicationClass factory : factories) {
			builder.registerTypeAdapterFactory((TypeAdapterFactory) factory.javaClass.newInstance());
		}

		return builder.create();
	}

	public static Gson gson() {
		if (null == gson) {
			throw new UnexpectedException("Plugin not yet initialized");
		}
		return gson;
	}
}

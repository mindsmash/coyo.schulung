package plugins.play;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import play.PlayPlugin;
import play.data.binding.RootParamNode;
import play.db.jpa.GenericModel;
import play.exceptions.UnexpectedException;
import binding.JsonBody;
import binding.JsonModelBinder;

import com.google.gson.Gson;

/**
 * JSON Body Binder Plugin. Binds Objects/Models from JSON Request Body.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * @author Jan Tammen, mindsmash GmbH
 */
public class JsonBodyBinderPlugin extends PlayPlugin {

	@Override
	public Object bind(final RootParamNode rootParamNode, final String name, final Class clazz, final Type type,
			final Annotation[] annotations) {
		if (null != annotations) {
			// check @JsonBody
			boolean jsonBodyPresent = false;
			for (final Annotation annotation : annotations) {
				if (annotation instanceof JsonBody) {
					jsonBodyPresent = true;
				}
			}
			if (!jsonBodyPresent) {
				return null;
			}
		} else {
			return null;
		}

		try {
			final String json = rootParamNode.getChild("body", true).getFirstValue(String.class);

			if (GenericModel.class.isAssignableFrom(clazz)) {
				return JsonModelBinder.bindModel(json, name, clazz, annotations);
			} else {
				return bindObject(json, clazz);
			}
		} catch (final Exception e) {
			throw new UnexpectedException("Cannot bind from Json Body", e);
		}
	}

	/**
	 * Bind POJO from json
	 */
	private Object bindObject(final String json, final Class clazz) {
		return new Gson().fromJson(json, clazz);
	}
}

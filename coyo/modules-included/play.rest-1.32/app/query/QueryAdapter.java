package query;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import play.db.Model;
import play.mvc.Http.Response;

/**
 * A generic query adapter for REST requests.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public abstract class QueryAdapter<T extends Model> {

	protected final Request<T> request;

	/**
	 * Builds a new query adapter on the basis of a request.
	 * 
	 * @param request the request.
	 */
	public QueryAdapter(Request<T> request) {
		this.request = request;
	}

	/**
	 * Returns the items found by the query adapter.
	 * 
	 * @return the entities of the current page.
	 */
	public abstract List<T> getItems();

	/**
	 * Returns the total number of items found by the query adapter.
	 * 
	 * @return the total number of entities found (n >= 0).
	 */
	public abstract long getTotalCount();

	/**
	 * Writes the JSON response headers if needed.
	 */
	public void writeHeaders() {
		Integer page = request.getPage();
		Integer pageSize = request.getPageSize();
		String[] order = request.getOrder();
		Response response = Response.current();

		if (null != page) {
			response.setHeader("Pagination-Page", page.toString());
		}

		if (null != pageSize) {
			response.setHeader("Pagination-Page-Size", pageSize.toString());
		}

		if (null != page && null != pageSize) {
			long totalCount = getTotalCount();
			long pageCount = totalCount / pageSize;
			if (totalCount % pageSize > 0) {
				pageCount += 1;
			}
			response.setHeader("Pagination-Page-Count", Long.toString(pageCount));
			response.setHeader("Pagination-Total-Count", Long.toString(totalCount));
		}

		if (null != order && order.length > 0) {
			String[] orderBy = new String[order.length];
			String[] orderDir = new String[order.length];
			for (int i = 0; i < order.length; i++) {
				String o = order[i];
				orderBy[i] = o.startsWith("!") ? o.substring(1) : o;
				orderDir[i] = o.startsWith("!") ? "DESC" : "ASC";
			}
			response.setHeader("Order-By", StringUtils.join(orderBy, ","));
			response.setHeader("Order-Dir", StringUtils.join(orderDir, ","));
		}
	}
}

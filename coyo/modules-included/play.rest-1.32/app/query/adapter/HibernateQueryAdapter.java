package query.adapter;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import play.db.Model;
import play.db.jpa.JPA;
import query.QueryAdapter;
import query.Request;
import query.filter.Combinator;
import query.filter.Filter;
import query.filter.Operator;

/**
 * A QueryAdapter implementation for Hibernate driven database connections.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 *
 * @param <T>
 */
public class HibernateQueryAdapter<T extends Model> extends QueryAdapter<T> {
	protected CriteriaBuilder builder;
	private TypedQuery<T> query;
	private TypedQuery<Long> count;

	public HibernateQueryAdapter(Request<T> request) {
		super(request);
		EntityManager em = JPA.em();
		builder = em.getCriteriaBuilder();

		CriteriaQuery<T> criteriaQuery = builder.createQuery(request.getClazz());
		CriteriaQuery<Long> criteriaCount = builder.createQuery(Long.class);
		Root<T> criteriaQueryRoot = criteriaQuery.from(request.getClazz());
		Root<T> criteriaCountRoot = criteriaCount.from(criteriaQuery.getResultType());
		criteriaQueryRoot.alias("e");
		criteriaCountRoot.alias("e");
		setFilterBasis(criteriaQueryRoot, criteriaQuery.select(criteriaQueryRoot));
		setFilterBasis(criteriaCountRoot, criteriaCount.select(builder.count(criteriaCountRoot)));

		if (request.hasOrder()) {
			criteriaQuery.orderBy(buildOrder(criteriaQueryRoot, request.getOrder()));
		}
		if (request.hasFilter()) {
			Predicate criteriaQueryWhere = buildFilter(criteriaQueryRoot, request.getFilter());
			criteriaQuery.where(criteriaQueryWhere);
			criteriaCount.where(criteriaQueryWhere);
		}

		query = em.createQuery(criteriaQuery);
		count = em.createQuery(criteriaCount);
	}

	protected CriteriaQuery setFilterBasis(Root root, CriteriaQuery query) {
		return query;
	}

	@Override
	public List<T> getItems() {
		Integer page = request.getPage();
		Integer pageSize = request.getPageSize();
		if (request.hasPagination()) {
			query.setFirstResult((page - 1) * pageSize);
			query.setMaxResults(pageSize);
		}
		return query.getResultList();
	}

	@Override
	public long getTotalCount() {
		return count.getSingleResult();
	}

	private Order[] buildOrder(Root<T> root, String... order) {
		Order[] result = new Order[order.length];
		for (int i = 0; i < order.length; i++) {
			boolean desc = order[i].startsWith("!");
			String[] orderParts = (desc ? order[i].substring(1) : order[i]).split("\\.");
			Path path = root;
			for (int j = 0; j < orderParts.length - 1; j++) {
				String name = orderParts[i];
				path = root.join(orderParts[j], JoinType.LEFT);
				path.alias(name);
			}
			Path field = path.get(orderParts[orderParts.length - 1]);
			result[i] = desc ? builder.desc(field) : builder.asc(field);
		}
		return result;
	}

	private Predicate buildFilter(Root<T> root, Filter filter) {
		if (filter instanceof Operator) {
			Operator op = (Operator) filter;

			String[] opParts = op.getKey().split("\\.");
			String key = opParts[opParts.length - 1];
			Path path = root;
			for (int i = 0; i < opParts.length - 1; i++) {
				String name = opParts[i];
				path = root.join(opParts[i]);
				path.alias(name);
			}

			switch (op.getMode()) {
				case EQ:
					if(op.getValue() == null) {
						return builder.isNull(path.get(key).as(op.getType()));
					} else {
						return builder.equal(path.get(key).as(op.getType()), op.getValue());
					}
				case NEQ:
					if(op.getValue() == null) {
						return builder.isNotNull(path.get(key).as(op.getType()));
					} else {
						return builder.notEqual(path.get(key).as(op.getType()), op.getValue());
					}
				case LT:
					if(Date.class.isAssignableFrom(op.getType())) {
						return builder.lessThan(path.get(key).as(op.getType()), (Date) op.getValue());
					} else {
						return builder.lt(path.get(key).as(op.getType()), (Number) op.getValue());
					}
				case LTE:
					if(Date.class.isAssignableFrom(op.getType())) {
						return builder.lessThanOrEqualTo(path.get(key).as(op.getType()), (Date) op.getValue());
					} else {
						return builder.le(path.get(key).as(op.getType()), (Number) op.getValue());
					}
				case GT:
					if(Date.class.isAssignableFrom(op.getType())) {
						return builder.greaterThan(path.get(key).as(op.getType()), (Date) op.getValue());
					} else {
						return builder.gt(path.get(key).as(op.getType()), (Number) op.getValue());
					}
				case GTE:
					if(Date.class.isAssignableFrom(op.getType())) {
						return builder.greaterThanOrEqualTo(path.get(key).as(op.getType()), (Date) op.getValue());
					} else {
						return builder.ge(path.get(key).as(op.getType()), (Number) op.getValue());
					}
				case IN:   return path.get(key).as(op.getType()).in(op.getValues());
				case LIKE:
					String str = (String) op.getValue();
					for (String s : new String[]{ "%", "_", "[", "]", "'" }) {
						str = str.replace(s, "\\" + s);
					}
					return builder.like(builder.lower(path.get(key).as(op.getType())), "%" + str.toLowerCase() + "%", '\\');
			}
		} else if (filter instanceof Combinator) {
			Combinator cb = (Combinator) filter;
			Filter[] fs = cb.getValues();
			Predicate[] ps = new Predicate[fs.length];
			for (int i = 0; i < fs.length; i++) {
				ps[i] = buildFilter(root, fs[i]);
			}
			switch (cb.getMode()) {
				case NOT: return builder.not(ps[0]);
				case AND: return builder.and(ps);
				case OR:  return builder.or(ps);
			}
		}
		return null;
	}
}

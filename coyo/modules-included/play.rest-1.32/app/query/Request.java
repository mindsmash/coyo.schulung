package query;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import play.db.Model;
import query.filter.Combinator;
import query.filter.Filter;
import query.filter.Operator;
import query.filter.Operator.Mode;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * A structured representation for a REST query.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public class Request<T extends Model> {

	private Class<T> clazz;

	private Integer page = null;

	private Integer pageSize = null;

	private String[] order = null;

	private Filter filter = null;

	/**
	 * Returns the reference class for this request. This is used to validate
	 * field names and method calls within the request.
	 * 
	 * @return the reference class.
	 */
	public Class<T> getClazz() {
		return clazz;
	}

	/**
	 * Returns the current page number for this request. Note that <tt>null</tt>
	 * is a valid return value in case the page number has not been set.
	 * 
	 * @return the page number.
	 */
	public Integer getPage() {
		return page;
	}

	/**
	 * Returns the current page size for this request. Note that <tt>null</tt>
	 * is a valid return value in case the page size has not been set.
	 * 
	 * @return the page size.
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	public boolean hasPagination() {
		return page != null && pageSize != null;
	}

	/**
	 * Returns the sorting order for this request. Multiple order criteria may
	 * be set with descending priority. A leading exclamation mark inverts the
	 * sorting order for this field. For example <tt>["one", "!two", "three"]</tt>
	 * translates to <i>one ASC, two DESC, three ASC</i>. Note that both
	 * <tt>null</tt> and <tt>[]</tt> are valid return values in case no sorting
	 * order has been set.
	 * 
	 * @return the sorting order.
	 */
	public String[] getOrder() {
		return order;
	}

	public boolean hasOrder() {
		return order != null && order.length > 0;
	}

	/**
	 * Returns the filters for this request.
	 * 
	 * @return the filters.
	 */
	public Filter getFilter() {
		return filter;
	}

	public boolean hasFilter() {
		return filter != null;
	}

	/**
	 * Builds a new request.
	 * 
	 * @param clazz the reference class.
	 * @param data the JSON request body.
	 * @throws IllegalArgumentException
	 */
	public Request(final Class<T> clazz, final JsonElement data) throws IllegalArgumentException {
		this.clazz = clazz;

		if (data.isJsonNull()) {
			return;
		} else if (!data.isJsonObject()) {
			throw new IllegalArgumentException("Missing argument");
		}
		JsonObject json = (JsonObject) data.getAsJsonObject();

		if (json.has("page")) {
			this.page = convert(json.get("page"), "page", Integer.class);
			if (this.page <= 0) {
				throw new IllegalArgumentException("Illegal argument value for key 'page': " + this.page);
			}
		}

		if (json.has("pageSize")) {
			this.pageSize = convert(json.get("pageSize"), "pageSize", Integer.class);
			if (this.pageSize <= 0) {
				throw new IllegalArgumentException("Illegal argument value for key 'pageSize': " + this.pageSize);
			}
		}

		if (this.page == null && this.pageSize != null) {
			throw new IllegalArgumentException("Missing argument: 'page'");
		} else if (this.page != null && this.pageSize == null) {
			throw new IllegalArgumentException("Missing argument: 'pageSize'");
		}

		if (json.has("order")) {
			JsonElement elem = json.get("order");
			if (elem.isJsonPrimitive()) {
				this.order = new String[] { elem.getAsString() };
			} else if (elem.isJsonArray()) {
				JsonArray elemArray = elem.getAsJsonArray();
				this.order = new String[elemArray.size()];
				for (int i = 0; i < elemArray.size(); i++) {
					JsonElement elemArrayElem = elemArray.get(i);
					if (elemArrayElem.isJsonPrimitive()) {
						this.order[i] = elemArrayElem.getAsString();
					} else {
						throw new IllegalArgumentException("Illegal argument value for key 'order[" + i + "]': " + elemArrayElem.toString());
					}
				}
			} else {
				throw new IllegalArgumentException("Illegal argument value for key 'order': " + elem.toString());
			}
			for (int i = 0; i < this.order.length; i++) {
				String name = this.order[i];
				if (null == getFieldType(clazz, name.startsWith("!") ? name.substring(1) : name)) {
					throw new IllegalArgumentException("Illegal argument value for key 'order[" + i + "]': " + this.order[i]);
				}
			}
		}
		if (json.has("filter")) {
			this.filter = json2Filter(json.get("filter"));
		}
	}

	private Filter json2Filter(final JsonElement json) throws IllegalArgumentException {
		if (!json.isJsonObject()) {
			throw new IllegalArgumentException("Illegal argument type for key 'filter': " + json.toString());
		}
		JsonObject filterObj = json.getAsJsonObject();

		if (!filterObj.has("mode")) {
			throw new IllegalArgumentException("Missing argument: 'filter.mode'");
		}
		JsonElement modeElem = filterObj.get("mode");
		if (!modeElem.isJsonPrimitive()) {
			throw new IllegalArgumentException("Illegal argument value for key 'filter.mode': " + modeElem.toString());
		}
		String mode = modeElem.getAsString();

		Operator.Mode opMode = Operator.getMode(mode);
		if (null != opMode) {
			if (!filterObj.has("key")) {
				throw new IllegalArgumentException("Missing argument: 'filter.key'");
			}
			JsonElement keyElem = filterObj.get("key");
			if (!keyElem.isJsonPrimitive()) {
				throw new IllegalArgumentException("Illegal argument value for key 'filter.key': " + keyElem.toString());
			}
			String key = keyElem.getAsString();
			Class keyType = getBoxedFieldType(clazz, key);
			if (null == keyType) {
				throw new IllegalArgumentException("Illegal argument value for key 'filter.key': " + keyElem.toString());
			}
			if (!filterObj.has("value")) {
				throw new IllegalArgumentException("Missing argument: 'filter.value'");
			}
			JsonElement valueElem = filterObj.get("value");
			if (opMode != Mode.IN && !valueElem.isJsonPrimitive() && !valueElem.isJsonNull()) {
				throw new IllegalArgumentException("Illegal argument value for key 'filter.value': " + valueElem.toString());
			} else if (opMode == Mode.IN && !valueElem.isJsonArray() && !valueElem.isJsonNull()) {
				throw new IllegalArgumentException("Illegal argument value for key 'filter.value': " + valueElem.toString());
			}
			if (opMode == Mode.IN) {
				return new Operator(opMode, key, null, convertArray(valueElem, "filter.value", keyType), keyType);
			} else {
				return new Operator(opMode, key, convert(valueElem, "filter.value", keyType), null, keyType);
			}
		}

		Combinator.Mode coMode = Combinator.getMode(mode);
		if (null != coMode) {
			if (!filterObj.has("values")) {
				throw new IllegalArgumentException("Missing argument: 'filter.values'");
			}
			JsonElement valuesElem = filterObj.get("values");
			if (!valuesElem.isJsonArray()) {
				throw new IllegalArgumentException("Illegal argument value for key 'filter.values': " + valuesElem.toString());
			}
			JsonArray valuesArray = filterObj.get("values").getAsJsonArray();
			Filter[] values = new Filter[valuesArray.size()];
			for (int i = 0; i < valuesArray.size(); i++) {
				values[i] = json2Filter(valuesArray.get(i));
			}
			return new Combinator(coMode, values);
		}

		throw new IllegalArgumentException("Illegal argument value for key 'filter.mode': " + mode);
	}

	private static <U> U convert(final JsonElement json, String key, Class<U> type) throws IllegalArgumentException {
		String errorStr = "Illegal argument type for key '" + key + "': " + json.toString();
		if (json.isJsonNull()) {
			return null;
		} else if (!json.isJsonPrimitive()) {
			throw new IllegalArgumentException(errorStr);
		}
		try {
			if (Enum.class.isAssignableFrom(type)) {
				return (U) Enum.valueOf(type.asSubclass(Enum.class), json.getAsString());
			} else if (String.class.isAssignableFrom(type)) {
				return type.cast(json.getAsString());
			} else if (Boolean.class.isAssignableFrom(type)) {
				return type.cast(json.getAsBoolean());
			} else if (Integer.class.isAssignableFrom(type)) {
				return type.cast(json.getAsInt());
			} else if (Long.class.isAssignableFrom(type)) {
				return type.cast(json.getAsLong());
			} else if (Double.class.isAssignableFrom(type)) {
				return type.cast(json.getAsDouble());
			} else if (Float.class.isAssignableFrom(type)) {
				return type.cast(json.getAsFloat());
			} else if (Character.class.isAssignableFrom(type)) {
				return type.cast(json.getAsCharacter());
			} else if (Byte.class.isAssignableFrom(type)) {
				return type.cast(json.getAsByte());
			} else if (Short.class.isAssignableFrom(type)) {
				return type.cast(json.getAsShort());
			} else if (Date.class.isAssignableFrom(type)) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				return type.cast(format.parse(json.getAsString()));
			}
		} catch (Exception e) {
			throw new IllegalArgumentException(errorStr, e);
		}
		throw new IllegalArgumentException(errorStr);
	}

	private static <U> U[] convertArray(final JsonElement json, String key, Class<U> type) throws IllegalArgumentException {
		String errorStr = "Illegal argument type for key '" + key + "': " + json.toString();
		if (json.isJsonNull()) {
			return null;
		} else if (json.isJsonPrimitive()) {
			throw new IllegalArgumentException(errorStr);
		} else {
			JsonArray jsonArray = json.getAsJsonArray();
			if (jsonArray.size() == 0) {
				throw new IllegalArgumentException(errorStr);
			}
			U[] array = (U[]) Array.newInstance(type, jsonArray.size());
			for (int i = 0; i < jsonArray.size(); i++) {
				array[i] = convert(jsonArray.get(i), key + "[" + i + "]", type);
			}
			return array;
		}
	}

	private static Class getBoxedFieldType(Class clazz, String key) {
		Class result = getFieldType(clazz, key);
		if (boolean.class.equals(result)) {
			return Boolean.class;
		} else if (int.class.equals(result)) {
			return Integer.class;
		} else if (long.class.equals(result)) {
			return Long.class;
		} else if (double.class.equals(result)) {
			return Double.class;
		} else if (float.class.equals(result)) {
			return Float.class;
		} else if (char.class.equals(result)) {
			return Character.class;
		} else if (byte.class.equals(result)) {
			return Byte.class;
		} else if (short.class.equals(result)) {
			return Short.class;
		}
		return result;
	}

	private static Class getFieldType(Class clazz, String key) {
		String[] parts = key.split("\\.", 2);
		if (0 == parts.length) {
			return null;
		}
		try {
			Field field = clazz.getField(parts[0]);
			return 1 == parts.length ? field.getType() : getFieldType(field.getType(), parts[1]);
		} catch (NoSuchFieldException e1) {
			try { // let's try method names instead
				for (Method method : clazz.getMethods()) {
					String methodName = method.getName();
					if (methodName.equals(parts[0]) || methodName.equals("get" + StringUtils.capitalize(key))) {
						return method.getReturnType();
					}
				}
			} catch (SecurityException e) {
				return null;
			}
			return null;
		} catch (SecurityException e) {
			return null;
		}
	}
}

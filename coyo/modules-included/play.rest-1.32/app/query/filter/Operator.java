package query.filter;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public class Operator<T> extends Filter<T> {

	public enum Mode {
		EQ,  // equals
		NEQ, // not equals
		LT,  // less than
		LTE, // less than equal
		GT,  // greater than equal
		GTE, // greater than
		IN,  // contains
		LIKE // like pattern
	}

	public static Mode getMode(String mode) {
		try {
			return Mode.valueOf(mode);
		} catch (Exception e) {
			return null;
		}
	}

	private Mode mode;

	private String key;

	private T value;

	private T[] values;

	private Class<T> type;

	public Operator(Mode mode, String key, T value, T[] values, Class<T> type) throws IllegalArgumentException {
		super();
		if ((mode == Mode.LT || mode == Mode.LTE || mode == Mode.GT || mode == Mode.GTE) && !Number.class.isAssignableFrom(type) && !Date.class.isAssignableFrom(type)) {
			throw new IllegalArgumentException("Illegal argument type for 'filter.mode." + mode.toString() + "': " + type.getName());
		} else if (mode == Mode.LIKE && !String.class.isAssignableFrom(type)) {
			throw new IllegalArgumentException("Illegal argument type for 'filter.mode." + mode.toString() + "': " + type.getName());
		} else if (mode != Mode.IN && value == null) {
			throw new IllegalArgumentException("Illegal argument type for 'filter.mode." + mode.toString() + "': " + type.getName());
		} else if (mode == Mode.IN && values == null) {
			throw new IllegalArgumentException("Illegal argument type for 'filter.mode." + mode.toString() + "': " + type.getName());
		}
		this.key = key;
		this.value = value;
		this.values = values;
		this.type = type;
		this.mode = mode;
	}

	public Mode getMode() {
		return mode;
	}

	public String getKey() {
		return key;
	}

	public T getValue() {
		return value;
	}

	public T[] getValues() {
		return values;
	}

	@Override
	public Class getType() {
		return type;
	}

	@Override
	protected String toString(int indent) {
		String pad = StringUtils.repeat(" ", indent);
		String str = String.format("%s %s %s (%s)", mode.toString(), key, value, type.getName());
		return pad + str;
	}

	@Override
	public String toString() {
		return toString(0);
	}
}

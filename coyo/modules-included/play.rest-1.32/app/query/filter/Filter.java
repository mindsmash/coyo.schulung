package query.filter;

/**
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public abstract class Filter<T> {

	protected abstract Class<T> getType();

	protected abstract String toString(int indent);
}

package query.filter;

import org.apache.commons.lang.StringUtils;

/**
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 * @param <T>
 */
public class Combinator extends Filter<Boolean> {

	public enum Mode {
		NOT, // negation
		AND, // conjunction
		OR   // disjunction
	}

	public static Mode getMode(String mode) {
		try {
			return Mode.valueOf(mode);
		} catch (Exception e) {
			return null;
		}
	}

	private Mode mode;

	private Filter[] values;

	public Combinator(Mode mode, Filter... values) throws IllegalArgumentException {
		super();
		if ((mode == Mode.NOT && values.length != 1) ||
			(mode == Mode.AND && values.length < 2) ||
			(mode == Mode.OR && values.length < 2)) {
			throw new IllegalArgumentException("Illegal argument length for key 'filter.value': " + values.length);
		}
		this.mode = mode;
		this.values = values;
	}

	public Mode getMode() {
		return mode;
	}

	public Filter[] getValues() {
		return values;
	}

	@Override
	protected Class<Boolean> getType() {
		return Boolean.class;
	}

	@Override
	protected String toString(int indent) {
		String pad = StringUtils.repeat(" ", indent);
		String str = pad + mode.toString();
		for (Filter value : values) {
			str += "\n" + value.toString(indent + 3);
		}
		return str;
	}

	@Override
	public String toString() {
		return toString(0);
	}
}

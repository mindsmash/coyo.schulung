package controllers;

/**
 * REST Module. Creates read-only REST-API for defined Models. Usage: For each
 * managed Model create a Controller that extends REST Controller. Annotate this
 * Controller with the For annotation.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 */

public abstract class RESTReadOnly extends REST {

	public static void create() throws Exception {
		notFound();
	}

	public static void update() throws Exception {
		notFound();
	}

	public static void delete(final String id) throws Exception {
		notFound();
	}
}

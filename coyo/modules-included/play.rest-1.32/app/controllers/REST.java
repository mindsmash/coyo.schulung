package controllers;

import injection.Inject;
import injection.InjectionSupport;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.DefaultGsonResolver;
import json.EmptyGsonResolver;
import json.GsonResolver;
import mvc.results.RenderPolymorphJson;
import pagination.PaginationAdapter;
import pagination.Paginator;
import pagination.adapter.DatabasePaginationAdapter;
import play.Play;
import play.data.binding.Binder;
import play.data.validation.Error;
import play.data.validation.Validation;
import play.db.Model;
import play.db.Model.Factory;
import play.db.jpa.GenericModel;
import play.exceptions.UnexpectedException;
import play.mvc.Controller;
import play.mvc.Http;
import query.QueryAdapter;
import query.Request;
import query.adapter.HibernateQueryAdapter;
import results.RenderJsonError;
import binding.JsonModelBinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

/**
 * REST Module. Creates REST-API for defined Models. Usage: For each managed
 * Model create a Controller that extends REST Controller. Annotate this
 * Controller with the For annotation.
 * 
 * @author Piers Wermbter. mindsmash GmbH
 * @author Daniel Busch, mindsmash GmbH
 * @author Fynn Feldpausch, mindsmash GmbH
 */
@InjectionSupport
public abstract class REST extends Controller {

	@Inject(configuration = "mindsmash.rest.gsonResolver", defaultClass = DefaultGsonResolver.class)
	private static GsonResolver gsonResolver;

	protected static Map<String, List<String>> readableErrorMap(Validation validation) {
		Map<String, List<String>> errorMap = new HashMap<String, List<String>>();
		for (Map.Entry<String, List<Error>> entry : validation.errorsMap().entrySet()) {
			List<String> messages = new ArrayList<String>();
			for (Error error : entry.getValue()) {
				messages.add(error.toString());
			}
			errorMap.put(entry.getKey(), messages);
		}
		return errorMap;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public @interface For {
		Class<? extends Model> value();

		Class<? extends PaginationAdapter> adapter() default DatabasePaginationAdapter.class;

		Class<? extends QueryAdapter> queryAdapter() default HibernateQueryAdapter.class;

		RestSerializer[] actionSerializers() default {};

	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.LOCAL_VARIABLE)
	public @interface RestSerializer {
		String[] actions();

		Class<? extends GsonResolver> resolver() default EmptyGsonResolver.class;

		Class<? extends JsonSerializer>[] serializers();

		boolean hierachieal() default false;
	}

	protected static Gson gson() {
		return gsonResolver.resolve().create();
	}

	protected static void renderJSON(Object object) {
		Class<? extends Controller> controllerClass = getControllerClass();
		if (controllerClass.isAnnotationPresent(For.class)) {
			For annotation = controllerClass.getAnnotation(For.class);
			if (annotation.actionSerializers().length > 0) {
				for (RestSerializer serializer : annotation.actionSerializers()) {
					if (Arrays.asList(serializer.actions()).contains(request.actionMethod)) {
						GsonBuilder builder;
						try {
							builder = serializer.resolver().newInstance().resolve();
						} catch (Exception e) {
							throw new UnexpectedException("Cannot instanciate GSON builder", e);
						}
						JsonSerializer[] adapters = new JsonSerializer[serializer.serializers().length];
						for (int i = 0; i < serializer.serializers().length; i++) {
							try {
								adapters[i] = serializer.serializers()[i].newInstance();
							} catch (Exception e) {
								throw new UnexpectedException("Cannot instanciate GSON serializer", e);
							}
						}
						throw new RenderPolymorphJson(object, serializer.hierachieal(), builder, adapters);
					}
				}
			}
		}
		Controller.renderJSON(gson().toJson(object));
	}

	public static void index() {
		if (getControllerClass() == REST.class) {
			forbidden();
		}
	}

	public static void all() throws Exception {
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);

		Class<? extends PaginationAdapter> adapterClass = getControllerClass().getAnnotation(For.class).adapter();
		PaginationAdapter adapter = adapterClass.getDeclaredConstructor(Class.class).newInstance(type.entityClass);
		Paginator paginator = Paginator.createFromRequest(adapter);

		paginator.writeResponseHeaders();
		renderJSON(paginator.getItems());
	}

	public static void query() throws Exception {
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);

		try {
			Class<? extends QueryAdapter> adapterClass = getControllerClass().getAnnotation(For.class).queryAdapter();
			Constructor<? extends QueryAdapter> adapterConstr = adapterClass.getDeclaredConstructor(Request.class);
			Request<? extends Model> request = new Request(type.entityClass, new JsonParser().parse(params.get("body")));
			QueryAdapter<? extends Model> adapter = adapterConstr.newInstance(request);

			adapter.writeHeaders();
			renderJSON(adapter.getItems());
		} catch (IllegalArgumentException e) {
			response.status = Http.StatusCode.BAD_REQUEST;
			renderJSON(e.getMessage());
		} catch (JsonSyntaxException e) {
			response.status = Http.StatusCode.BAD_REQUEST;
			renderJSON(e.getCause().getMessage());
		}
	}

	public static void show(final String id) throws Exception {
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);
		Model object = type.findById(id);
		notFoundIfNull(object);
		renderJSON(object);
	}

	public static void create() throws Exception {
		String body = params.get("body");
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);

		GenericModel object = JsonModelBinder.bindModel(body, "object", type.entityClass, null);
		if (!object.validateAndCreate()) {
			throw new RenderJsonError(readableErrorMap(validation));
		} else {
			renderJSON(object);
		}
		renderJSON(object);
	}

	public static void update(final String id) throws Exception {
		notFoundIfNull(id);
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);

		String body = params.get("body");
		Model persistentObject = type.findById(id);
		GenericModel object = JsonModelBinder.bindModel(body, "object", type.entityClass, null,
				(GenericModel) persistentObject);
		if (!object.validateAndSave()) {
			throw new RenderJsonError(readableErrorMap(validation));
		} else {
			renderJSON(object);
		}
	}

	public static void delete(final String id) throws Exception {
		ObjectType type = ObjectType.get(getControllerClass());
		notFoundIfNull(type);
		Model object = type.findById(id);
		notFoundIfNull(object);
		try {
			object._delete();
		} catch (Exception e) {
			if(e.getCause() instanceof RenderJsonError) {
				throw (RenderJsonError) e.getCause();
			} else {
				forbidden();
			}
		}
	}

	public static class ObjectType {

		public Class<? extends Controller> controllerClass;
		public Class<? extends Model> entityClass;
		public String name;
		public String modelName;
		public String controllerName;
		public String keyName;
		public Factory factory;

		public ObjectType(Class<? extends Model> modelClass) {
			this.modelName = modelClass.getSimpleName();
			this.entityClass = modelClass;
			this.factory = Model.Manager.factoryFor(entityClass);
			this.keyName = factory.keyName();
		}

		public boolean fieldExists(String fieldName) {
			if (fieldName != null) {
				try {
					entityClass.getField(fieldName);
				} catch (NoSuchFieldException e) {
					return false;
				}
			}
			return true;
		}

		public ObjectType(String modelClass) throws ClassNotFoundException {
			this((Class<? extends Model>) Play.classloader.loadClass(modelClass));
		}

		public static ObjectType forClass(String modelClass) throws ClassNotFoundException {
			return new ObjectType(modelClass);
		}

		public static ObjectType get(Class<? extends Controller> controllerClass) {
			Class<? extends Model> entityClass = getEntityClassForController(controllerClass);
			if (entityClass == null || !Model.class.isAssignableFrom(entityClass)) {
				return null;
			}
			ObjectType type = new ObjectType(entityClass);

			type.name = controllerClass.getSimpleName().replace("$", "");
			type.controllerName = controllerClass.getSimpleName().toLowerCase().replace("$", "");
			type.controllerClass = controllerClass;
			return type;
		}

		public static Class<? extends Model> getEntityClassForController(Class<? extends Controller> controllerClass) {
			if (controllerClass.isAnnotationPresent(For.class)) {
				return controllerClass.getAnnotation(For.class).value();
			} else {
				return null;
			}
		}

		public Model findById(String id) throws Exception {
			if (id == null) {
				return null;
			}

			Factory factory = Model.Manager.factoryFor(entityClass);
			Object boundId = Binder.directBind(id, factory.keyType());
			return factory.findById(boundId);
		}
	}
}

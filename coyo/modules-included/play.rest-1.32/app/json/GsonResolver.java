package json;

import com.google.gson.GsonBuilder;

/**
 * Resolve GSON builder instance for REST controller.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public interface GsonResolver {

	/**
	 * Resolve the configured GSON builder.
	 * 
	 * @return
	 */
	public GsonBuilder resolve();
}

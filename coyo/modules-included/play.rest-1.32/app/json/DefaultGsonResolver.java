package json;

import play.Play;

import com.google.gson.GsonBuilder;

/**
 * Resolves minimal configured GSON builder instance.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class DefaultGsonResolver implements GsonResolver {

	protected static final String DATE_FORMAT =
		Play.configuration.getProperty("mindsmash.rest.dateFormat", "yyyy-MM-dd");

	@Override
	public GsonBuilder resolve() {
		return new GsonBuilder()
			.addSerializationExclusionStrategy(new RestExclusionStrategy())
			.setDateFormat(DATE_FORMAT);
	}
}

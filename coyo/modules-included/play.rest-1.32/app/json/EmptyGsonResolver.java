package json;

import com.google.gson.GsonBuilder;

/**
 * Resolves plain GSON builder instance.
 * 
 * @author Fynn Feldpausch, mindsmash GmbH
 * 
 */
public class EmptyGsonResolver implements GsonResolver {

	@Override
	public GsonBuilder resolve() {
		return new GsonBuilder();
	}
}

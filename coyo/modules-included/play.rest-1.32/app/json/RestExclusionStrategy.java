package json;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * Exclude fields and classes annotated with {@link RestExclude}
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class RestExclusionStrategy implements ExclusionStrategy {

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
                return null != clazz.getAnnotation(RestExclude.class);
        }

        @Override
        public boolean shouldSkipField(FieldAttributes field) {
                return null != field.getAnnotation(RestExclude.class);
        }

}
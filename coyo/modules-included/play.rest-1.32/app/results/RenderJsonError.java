package results;

import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.RenderJson;

/**
 * 400 Bad Request with application/json.
 * 
 * @author Jan Tammen, mindsmash GmbH
 */
public class RenderJsonError extends RenderJson {

	public RenderJsonError(final Object o) {
		super(o);
	}

	@Override
	public void apply(final Request request, final Response response) {
		response.status = Http.StatusCode.BAD_REQUEST;
		super.apply(request, response);
	}
}

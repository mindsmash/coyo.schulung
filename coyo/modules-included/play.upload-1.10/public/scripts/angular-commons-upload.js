(function() {
	
	/**
	 * Module for ajax upload support.
	 * 
	 * @author Daniel Busch, mindsmash GmbH
	 */
	var module = angular.module('commons.upload', []);
	
	module.constant('uploadConfig', {
	});
	
	module.factory('UploadApi', ['uploadConfig', '$rootScope', '$timeout', '$log', function(uploadConfig, $rootScope, $timeout, $log) {
		var Api = function(name) {
			this.name = name;
		};
		
		angular.extend(Api.prototype, {
			up : null,
			files : [],
			init : function (settings) {
				var api = this;
				
				$timeout(function() {
					
					api.up = new plupload.Uploader({
						runtimes : 'html5,flash,silverlight,html4',
						multi_selection: (angular.isDefined(settings.multiple)) ? settings.multiple : true,
						drop_element : settings.dropId,
						browse_button : settings.browseId,
						url : settings.url
					});
					
					api.up.bind('FilesAdded', angular.bind(api, function(up, files) {
						for (var i = 0; i < files.length; i++) {
							api.files.push(files[i]);
						}
						api.$scope.$apply();
						_.defer(function() {
							api.notify('start');
							up.start();
						});
					}));
	
					api.up.bind('UploadProgress', angular.bind(api, function(up, file) {
						var f = api.getFile(file.id);
						angular.extend(f, file);
						api.$scope.$apply();
					}));
					
					api.up.bind('FileUploaded', angular.bind(api, function(up, file, response) {
						var f = api.getFile(file.id);
						angular.extend(f, file, {
							uploadId : response.response
						});
						api.$scope.$apply();
					}));
					
					api.up.bind('UploadComplete', angular.bind(api, function(up, files) {
						api.notify('complete');
						api.$scope.$apply();
					}));
					
					api.up.bind('PostInit', angular.bind(api, function(up) {
						api.notify('init');
						api.$scope.$apply();
					}));
					
					api.up.bind('Error', angular.bind(api, function(up, err) {
						$log.error('[Upload] Error uploading file: ' + err.message);
						api.notify('error');
						api.$scope.$apply();
					}));
					
					api.up.init();
				}, 0);
			},
			$scope : null,
			scope : function(scope) {
				this.$scope = scope;
				return this;
			},
			getFile : function(id) {
				var files = this.files;
				for (var i = 0; i < files.length; i++) {
					if (id === files[i].id) {
						return files[i];
					}
				}
			},
			remove : function(index) {
				this.files.splice(index, 1);
				this.notify('complete');
			},
			
			notify : function(eventId) {
				this.$scope.$emit('upload-api:' + eventId, this.name, _.map(this.files, function(file) {
					return file.uploadId;
				}));
			}
		});
		
		return {
			_apis : {},
			get : function(name) {
				if (!this._apis[name]) {
					this._apis[name] = new Api(name);
				}
				return this._apis[name];
			}
		};
	}]);
	
	module.directive('upload', [ 'UploadApi', function(UploadApi) {
		return {
			template : '<div class="files" ng-show="api.files.length"><ul><li ng-repeat="f in api.files">{{f.name}} <span ng-show="f.size"> ({{f.size | filesize}}) </span><div class="progress pull-right" ng-show="f.percent != 100"><div class="bar" style="width: {{f.percent || 0}}%;"></div></div><a href ng-click="api.remove($index)" class="close pull-right" ng-show="f.percent == 100">&times;</a></li></ul></div>',
			replace : true,
			require : 'ngModel',
			link: function($scope, elem, attrs, ngModel) {
				var options = $scope.$eval(attrs.upload);
				$scope.api = UploadApi.get(options.name);
				$scope.api.scope($scope).init(options);
				
				$scope.$on('upload-api:init', function(e, name) {
					if (angular.isDefined(ngModel.$viewValue)) {
						$scope.api.files = ngModel.$viewValue;
					}
				});
				
				$scope.$on('upload-api:complete', function(e, name, uploads) {
					if (name === options.name) {
						ngModel.$setViewValue(uploads);
					}
				});
				
				$scope.$watch(attrs.ngModel, function(value, old) {
					if (!_.isArray(value) || value.length === 0) {
						$scope.api.files = [];
					}
				});
			}
		};
	}]);
	
	module.directive('uploadTrigger', [ 'UploadApi', function(UploadApi) {
		return {
			link: function($scope, elem, attrs) {
				this.api = $scope.api = UploadApi.get(attrs.uploadTrigger);
			}
		};
	}]);
	

	/**
	 * This directive can be used to trigger a refresh after a specific
	 * value has changed. We use that to refresh plUpload when it's
	 * container changed from an invisible state to visible. Otherwise the
	 * upload trigger would simply not work in IE.
	 */
	module.directive('uploadRefresh', ['$timeout', 'UploadApi', function($timeout, UploadApi) {
		return {
			link: function($scope, elem, attrs) {
				$scope.$watch(attrs.uploadRefresh, function(newValue, oldValue) {
					if (newValue) {
						$timeout(function() {
							$scope.api.up.refresh();
						});
					}
				});
			}
		};
	}]);
	
	module.directive('uploadDropzone', [ 'UploadApi', function(UploadApi) {
		return {
			link: function($scope, elem, attrs) {
				this.api = $scope.api = UploadApi.get(attrs.uploadDropzone);
				
				elem.on('dragover', function(event) {
					event.originalEvent.dataTransfer.dropEffect = "copy";
				});

				elem.on('dragenter', function() {
					elem.addClass('dragover');
				});

				elem.on('dragleave', function() {
					elem.removeClass('dragover');
				});

				elem.on('drop', function() {
					elem.removeClass('dragover');
				});
			}
		};
	}]);
	
	module.filter('filesize', function() {
		return function(input) {
			if (!_.isNumber(input)) {
				return input;
			}
			if (input > 1024 * 1024 * 1024) {
				return (input / (1024 * 1024 * 1024)).toFixed(1) + 'gb';
			}
			if (input > 1024 * 1024) {
				return (input / (1024 * 1024)).toFixed(1) + 'mb';
			}
			if (input > 1024) {
				return (input / 1024).toFixed(1) + 'kb';
			}
			return input + 'b';
		};
	});
})();
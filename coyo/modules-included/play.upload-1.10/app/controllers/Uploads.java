package controllers;

import java.io.Serializable;
import java.util.Date;

import play.cache.Cache;
import play.data.Upload;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import storage.FlexibleBlob;

/**
 * Generic upload controller. Takes uploads and stores them.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class Uploads extends Controller {

	public static void upload(Upload file) {
		if (file != null) {
			TempUpload temp = storeUpload(file);
			if (null != temp) {
				renderHtml(temp.id);
			}
		}
		badRequest();
	}

	@Util
	public static TempUpload storeUpload(Upload upload) {
		if (upload != null && upload.getSize().longValue() > 0L) {

			FlexibleBlob blob = new FlexibleBlob();
			blob.set(upload.asStream(), upload.getContentType(), upload.getSize());

			TempUpload temp = new TempUpload(blob, upload.getFileName());

			Cache.set(getCacheKey(temp.id), temp);
			return temp;
		}
		return null;
	}

	@Util
	public static TempUpload loadUpload(String id) {
		return Cache.get(getCacheKey(id), TempUpload.class);
	}

	private static String getCacheKey(String id) {
		return "play-uploads-" + id;
	}

	public static class TempUpload implements Serializable {
		public FlexibleBlob blob;
		public String filename;
		public String id = Codec.UUID();
		public long created = new Date().getTime();

		public TempUpload(FlexibleBlob blob, String filename) {
			this.blob = blob;
			this.filename = filename;

		}
	}
}
package storage.local;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

import play.Play;
import play.db.jpa.Blob;
import play.exceptions.UnexpectedException;
import play.libs.IO;
import storage.FlexibleBlobStorage;
import util.StoreDirectoryUtils;
import utils.HardDriveUtil;

/**
 * {@link FlexibleBlobStorage} implementation to store files locally on disc.
 * Similar to Play!'s {@link Blob}.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class LocalFileStorage implements FlexibleBlobStorage {

	public static final String STORE = Play.configuration.getProperty("attachments.path", "attachments");

	@Override
	public InputStream get(String id) {
		File file = getFile(id);
		if (file.exists() && !file.isDirectory()) {
			try {
				return new FileInputStream(file);
			} catch (Exception e) {
				throw new UnexpectedException(e);
			}
		}
		return null;
	}

	@Override
	public File getAsFile(String id) {
		return getFile(id);
	}

	@Override
	public void set(InputStream is, String id, String type) {
		IO.write(is, getFile(id));
	}

	@Override
	public void set(InputStream is, String id, String type, Long length) {
		IO.write(is, getFile(id));
	}

	@Override
	public long length(String id) {
		return getFile(id).length();
	}

	@Override
	public long storageLength() {
		return HardDriveUtil.getCurrentUsage();
	}

	@Override
	public boolean exists(String id) {
		return getFile(id).exists();
	}

	protected File getFile(String id) {
		return new File(getStore(), id);
	}

	/**
	 * Get the file store.
	 * 
	 * @return
	 */
	protected File getStore() {
		return StoreDirectoryUtils.getStore(STORE);
	}

	@Override
	public boolean delete(String id) {
		File file = getFile(id);
		return file.exists() && file.delete();
	}
	
	@Override
	public void finalize(String id) {
	}

	@Override
	public List<String> listAll() {
		return Arrays.asList(getStore().list());
	}
}

package storage.local;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;

/**
 * File storage that stores files in sub folders to avoid a very large directory index. This is better for performance
 * in very large setups with thousands of files.
 * 
 * Each file will be stored in a sub-folder that starts with x (configurable) first characters of the file id.
 * 
 * This implementation is backwards-compatible so that it can be used even if the customer used a flat file structure in
 * the past. To activate backwards compatibility, set the configuration parameter "mindsmash.storage.large.backwards" to
 * "true".
 * 
 * @author Jan Marquardt, mindsmash GmbH
 */
public class LargeLocalFileStorage extends LocalFileStorage {

	private static final int FOLDER_GROUP_LENGTH = Integer.parseInt(Play.configuration.getProperty(
			"mindsmash.storage.large.folderGroupLength", "2"));

	private static final boolean BACKWARDS_COMPATIBLE = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.storage.large.backwards", "false"));

	@Override
	protected File getFile(String id) {
		File structured = new File(getFolder(id), id);

		if (!BACKWARDS_COMPATIBLE) {
			return structured;
		}

		File flat = super.getFile(id);
		if (flat.exists()) {
			if (structured.exists()) {
				try {
					/* Check whether the two files have the same content */
					if (FileUtils.contentEquals(flat, structured)) {
						flat.delete();
						return structured;
					} else {
						/* Well, we don't know what the file "structured" contains right now, so leave it alone... */
						Logger.error(
								"[LargeLocalFileStorage] The files [%s] and [%s] are different in content, cannot move the first one...",
								flat, structured);
						return flat;
					}
				} catch (IOException e) {
					Logger.error(e, "[LargeLocalFileStorage] Cannot compare content of [%s] and [%s]", flat, structured);
					return flat;
				}
			} else {
				try {
					FileUtils.moveFile(flat, structured);
				} catch (IOException e) {
					Logger.error(e, "[LargeLocalFileStorage] Error moving file from [%s] to [%s]", flat, structured);
					return flat;
				}
			}
		}

		return structured;
	}

	protected File getFolder(String id) {
		// strip special chars
		String stripped = id.replaceAll("[^a-zA-Z0-9]+", "");

		File folder;
		if (StringUtils.isEmpty(stripped) || stripped.length() < FOLDER_GROUP_LENGTH) {
			folder = new File(getStore(), "other");
		} else {
			folder = new File(getStore(), "sub-" + stripped.substring(0, FOLDER_GROUP_LENGTH));
		}

		if (!folder.exists()) {
			folder.mkdirs();
		}

		return folder;
	}
}

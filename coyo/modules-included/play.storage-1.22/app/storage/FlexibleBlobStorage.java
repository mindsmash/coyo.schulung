package storage;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Describes a data store backend for the play.storage module. Implement this
 * interface to provice a data store that stores the FlexbibleBlob contents.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public interface FlexibleBlobStorage {

	/**
	 * List all available IDs.

	 * @return
	 */
	public List<String> listAll();
	
	/**
	 * Get the contents as stream.
	 * 
	 * @param id
	 * @return
	 */
	public InputStream get(String id);

	/**
	 * Get the contents as file.
	 * 
	 * @param id
	 * @return
	 */
	public File getAsFile(String id);

	/**
	 * Store the content of the given type from stream.
	 * 
	 * @param is
	 * @param id
	 * @param type
	 */
	public void set(InputStream is, String id, String type);

	/**
	 * Store the content of the given type from stream. Using this is always
	 * better than using {@link #set(InputStream, String, String)}
	 * 
	 * @param is
	 * @param id
	 * @param type
	 * @param length
	 */
	public void set(InputStream is, String id, String type, Long length);

	/**
	 * Get the data length.
	 * 
	 * @param id
	 * @return
	 */
	public long length(String id);

	/**
	 * Get the full storage length
	 * 
	 * @return
	 */
	public long storageLength();

	/**
	 * True when the data for the given id exists in store.
	 * 
	 * @param id
	 * @return
	 */
	public boolean exists(String id);

	/**
	 * Delete data of given id in store.
	 * 
	 * @param id
	 * @return
	 */
	public boolean delete(String id);

	/**
	 * Called when a {@link FlexibleBlob} is garbage collected. Can be used to
	 * clear caches or close open streams.
	 * 
	 * @param id
	 */
	public void finalize(String id);
}

package storage;

import fishr.search.ContentIndexable;
import injection.Inject;
import injection.InjectionSupport;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;

import play.db.Model.BinaryField;
import play.libs.Codec;
import storage.local.LocalFileStorage;

/**
 * A flexible blob that can write blob to arbitrary storages.
 * 
 * Implements hibernate {@link UserType} whichs allows {@link FlexibleBlob}s to
 * be persisted with JPA Entities.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
@InjectionSupport
public class FlexibleBlob implements BinaryField, UserType, Serializable, ContentIndexable {

	@Inject(configuration = "mindsmash.storage.class", defaultClass = LocalFileStorage.class)
	public static FlexibleBlobStorage storage;

	private String type;
	private String id;
	
	public FlexibleBlob() {
	}

	public FlexibleBlob(String id, String type) {
		this.id = id;
		this.type = type;
	}

	@Override
	public Object deepCopy(Object o) throws HibernateException {
		if (o == null) {
			return null;
		}
		return new FlexibleBlob(((FlexibleBlob) o).id, ((FlexibleBlob) o).type);
	}

	private static boolean equal(Object a, Object b) {
		return a == b || (a != null && a.equals(b));
	}

	@Override
	public boolean equals(Object o, Object o1) throws HibernateException {
		if (o instanceof FlexibleBlob && o1 instanceof FlexibleBlob) {
			return equal(((FlexibleBlob) o).id, ((FlexibleBlob) o1).id)
					&& equal(((FlexibleBlob) o).type, ((FlexibleBlob) o1).type);
		}
		return equal(o, o1);
	}

	@Override
	public int hashCode(Object o) throws HibernateException {
		return o.hashCode();
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, Object o) throws HibernateException, SQLException {
		@SuppressWarnings("deprecation")
		String val = (String) StringType.INSTANCE.get(resultSet, names[0]);

		if (val == null || val.length() == 0 || !val.contains("|") || val.startsWith("null")) {
			return new FlexibleBlob();
		}
		String[] parts = val.split("[|]");
		if (parts.length < 2) {
			return new FlexibleBlob();
		}
		return new FlexibleBlob(parts[0], parts[1]);
	}

	@Override
	public void nullSafeSet(PreparedStatement ps, Object o, int i) throws HibernateException, SQLException {
		if (o != null && ((FlexibleBlob) o).id != null && !"null".equals(((FlexibleBlob) o).id)) {
			ps.setString(i, ((FlexibleBlob) o).id + "|" + ((FlexibleBlob) o).type);
		} else {
			ps.setNull(i, Types.VARCHAR);
		}
	}

	@Override
	public Object replace(Object o1, Object o2, Object o3) throws HibernateException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Object assemble(Serializable serializable, Object o) throws HibernateException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Serializable disassemble(Object o) throws HibernateException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Class returnedClass() {
		return FlexibleBlob.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[] { Types.VARCHAR };
	}

	/**
	 * Get the blob content as stream.
	 */
	@Override
	public InputStream get() {
		if (id != null && exists()) {
			return storage.get(id);
		}
		return null;
	}

	@Override
	@Deprecated
	public void set(InputStream is, String type) {
		set(is, type, null);
	}

	/**
	 * Preferred over {@link #set(InputStream, String)}.
	 * 
	 * @param is
	 * @param type
	 * @param length
	 */
	public void set(InputStream is, String type, Long length) {
		this.type = type;
		if (id == null) {
			id = Codec.UUID();
		}

		if (id == null || "null".equals(id) || id.indexOf('|') != -1) {
			throw new IllegalArgumentException(String.format(
					"Flexible blob id must not be null and must not contain a pipe [%s]", id));
		}

		if (length == null) {
			storage.set(is, id, type);
		} else {
			storage.set(is, id, type, length);
		}
	}

	/**
	 * Get the blob length.
	 */
	@Override
	public long length() {
		return storage.length(id);
	}

	/**
	 * Get the blob type.
	 */
	@Override
	public String type() {
		return type;
	}

	/**
	 * True when blob exists in datastore.
	 */
	@Override
	public boolean exists() {
		return null != id && storage.exists(id);
	}

	/**
	 * Delete the stored data.
	 */
	public void delete() {
		if (exists()) {
			storage.delete(id);
		}
	}

	/**
	 * Get the data as tmp file.
	 * 
	 * @return
	 */
	public File asFile() {
		return storage.getAsFile(id);
	}

	@Override
	protected void finalize() throws Throwable {
		if (id != null) {
			storage.finalize(id);
		}

		super.finalize();
	}
}

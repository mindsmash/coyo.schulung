package plugins.play;

import play.Play;
import play.PlayPlugin;
import tags.AssetsFastTags;
import assets.AssetsManager;

/**
 * Clear the asset directory on application start, if the "clearOnStartup" parameter is set.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class AssetsPlugin extends PlayPlugin {

	public static final boolean CLEAR_ON_STARTUP = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.assets.clearOnStartup", "false"));

	@Override
	public void onConfigurationRead() {
		if (CLEAR_ON_STARTUP) {
			AssetsManager.clearAll();
		}
	}

	@Override
	public void afterInvocation() {
		AssetsFastTags.clearThreadLocals();
	}
}

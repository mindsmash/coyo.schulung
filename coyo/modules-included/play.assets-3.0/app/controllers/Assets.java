package controllers;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.libs.IO;
import play.libs.Time;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import assets.AssetsManager;
import assets.handling.AssetsHandler;

/**
 * Handle asset requests.
 *
 * @author Daniel Busch, mindsmash GmbH
 */
public class Assets extends Controller {

    private static final String CACHE_MAX_AGE = Play.configuration.getProperty("mindsmash.assets.cacheFor", "1d");
    private static final boolean CACHE_PUBLIC = Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.assets.cachePublic", "true"));

    @Before
    static void cache() {
        if (StringUtils.isNotEmpty(CACHE_MAX_AGE)) {
            int maxAge = Time.parseDuration(CACHE_MAX_AGE);
            response.setHeader("Cache-Control", (CACHE_PUBLIC ? "public, " : "") + "max-age=" + maxAge);
        }
    }

    /**
     * Compile single less file and write to output stream.
     */
    public static void less(String path) throws Exception {
        Logger.trace("[Assets] Delivering single less file [%s]", path);

        InputStream fin = null;
        BufferedInputStream in = null;

        try {
            fin = IOUtils.toInputStream(AssetsManager.stylesheetHandler.getCompiledAsset(path));
            in = new BufferedInputStream(fin);

            response.contentType = "text/css";
            IO.copy(in, response.out);
        } finally {
            try {
                if (null != in) {
                    in.close();
                }
            } finally {
                if (null != fin) {
                    fin.close();
                }
            }
        }
    }

    public static void css(String key) {
        renderAsset(AssetsManager.stylesheetHandler, key);
    }

    public static void js(String key) {
        renderAsset(AssetsManager.scriptHandler, key);
    }

    @Util
    private static void renderAsset(AssetsHandler handler, String key) {
        try {
            renderBinary(IOUtils.toInputStream(handler.getMergedAssets(key)), key + "." + handler.suffix,
                    handler.contentType, true);
        } catch (Exception e) {
            Logger.debug(e, "[Assets] Error loading assets for key: %s", key);
            Logger.warn("[Assets] Error loading assets for key: %s", key);
            error("Error loading asset");
        }
    }
}

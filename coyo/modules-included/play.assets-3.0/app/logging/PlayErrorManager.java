package logging;

import play.Logger;

import com.google.javascript.jscomp.BasicErrorManager;
import com.google.javascript.jscomp.CheckLevel;
import com.google.javascript.jscomp.ErrorFormat;
import com.google.javascript.jscomp.JSError;
import com.google.javascript.jscomp.MessageFormatter;

/**
 * The error manager using the play logger.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class PlayErrorManager extends BasicErrorManager {
	private final MessageFormatter formatter;

	/**
	 * Creates an instance.
	 */
	public PlayErrorManager(MessageFormatter formatter) {
		this.formatter = formatter;
	}

	/**
	 * Creates an instance with a source-less error formatter.
	 */
	public PlayErrorManager() {
		this(ErrorFormat.SOURCELESS.toFormatter(null, false));
	}

	@Override
	public void println(CheckLevel level, JSError error) {
		switch (level) {
		case ERROR:
			Logger.error(error.format(level, formatter));
			break;
		case WARNING:
			// log warnings as debug to avoid log spamming
			Logger.debug(error.format(level, formatter));
			break;
		}
	}

	@Override
	protected void printSummary() {
		if (getErrorCount() + getWarningCount() > 0) {
			Logger.debug("[JSCompiler] %d error(s), %d warning(s)", getErrorCount(), getWarningCount());
		}
	}

}

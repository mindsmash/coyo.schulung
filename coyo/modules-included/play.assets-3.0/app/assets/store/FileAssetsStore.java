package assets.store;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.exceptions.UnexpectedException;
import util.StoreDirectoryUtils;

public class FileAssetsStore implements AssetsStore {

	/**
	 * Directory to store the assets.
	 */
	public static final File ASSETS_DIR = new File(Play.tmpDir, "assets");

	@Override
	public void clearAll() {
		try {
			Logger.info("[FileAssetsStore] Clearing asset directory [%s]", ASSETS_DIR.getAbsolutePath());
			StoreDirectoryUtils.cleanDirectoryIfExists(ASSETS_DIR);
		} catch (IOException e) {
			throw new UnexpectedException(e);
		}
	}

	@Override
	public void clear(String id) {
		try {
			File dir = getDirForId(id);
			Logger.info("[FileAssetsStore] Clearing asset directory [%s]", dir.getAbsolutePath());
			StoreDirectoryUtils.cleanDirectoryIfExists(dir);
		} catch (IOException e) {
			throw new UnexpectedException(e);
		}
	}

	private File getDirForId(String id) {
		File store = ASSETS_DIR;
		if (!StringUtils.isEmpty(id)) {
			store = new File(store, id);
		}
		return StoreDirectoryUtils.ensureStoreExists(store);
	}

	@Override
	public void lock(String id, String asset, String expiration) {
		Cache.safeSet("asset-lock-" + id + "-" + asset, "true", expiration);
	}

	@Override
	public void unlock(String id, String asset) {
		Cache.safeDelete("asset-lock-" + id + "-" + asset);
	}

	@Override
	public boolean isLocked(String id, String asset) {
		return Cache.get("asset-lock-" + id + "-" + asset) != null;
	}

	@Override
	public String read(String id, String asset) {
		try {
			return FileUtils.readFileToString(new File(getDirForId(id), asset));
		} catch (IOException e) {
			Logger.debug(e, "[FileAssetsStore] Error reading asset for asset %s (id=%s)", asset, id);
			Logger.warn("[FileAssetsStore] Error reading asset for asset %s (id=%s)", asset, id);
			return null;
		}
	}

	@Override
	public void write(String id, String asset, String content) {
		try {
			File file = new File(getDirForId(id), asset);
			if (file.exists()) {
				file.delete();
			}
			FileUtils.write(file, content);
		} catch (IOException e) {
			Logger.debug(e, "[FileAssetsStore] Error writing asset for asset %s (id=%s)", asset, id);
			Logger.warn("[FileAssetsStore] Error writing asset for asset %s (id=%s)", asset, id);
		}
	}

	@Override
	public boolean exists(String id, String asset) {
		return new File(getDirForId(id), asset).exists();
	}

	@Override
	public void delete(String id, String asset) {
		File file = new File(getDirForId(id), asset);
		if (file.exists()) {
			file.delete();
		}
	}
}
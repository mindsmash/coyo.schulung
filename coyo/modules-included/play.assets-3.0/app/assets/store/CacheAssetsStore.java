package assets.store;

import java.io.Serializable;

import play.cache.Cache;

public class CacheAssetsStore implements AssetsStore {

	@Override
	public void clearAll() {
		Cache.clear();
	}

	@Override
	public void clear(String id) {
		Cache.set("assets-maxAge-" + id, System.currentTimeMillis());
	}

	public long getMaxAge(String id) {
		Long maxAge = Cache.get("assets-maxAge-" + id, Long.class);
		if (maxAge == null) {
			maxAge = 0L;
		}
		return maxAge;
	}

	@Override
	public void lock(String id, String asset, String expiration) {
		Cache.safeSet(getCacheKey(id, asset) + "-lock", "true", expiration);
	}

	@Override
	public void unlock(String id, String asset) {
		Cache.safeDelete(getCacheKey(id, asset) + "-lock");
	}

	@Override
	public boolean isLocked(String id, String asset) {
		return Cache.get(getCacheKey(id, asset) + "-lock") != null;
	}

	@Override
	public String read(String id, String asset) {
		CachedAsset cached = Cache.get(getCacheKey(id, asset), CachedAsset.class);
		if (cached != null) {
			return cached.content;
		}
		return null;
	}

	@Override
	public void write(String id, String asset, String content) {
		Cache.set(getCacheKey(id, asset), new CachedAsset(content));
	}

	@Override
	public boolean exists(String id, String asset) {
		CachedAsset cached = Cache.get(getCacheKey(id, asset), CachedAsset.class);
		return cached != null && cached.created >= getMaxAge(id);
	}

	@Override
	public void delete(String id, String asset) {
		Cache.delete("asset-" + id + "-" + asset);
	}

	private String getCacheKey(String id, String asset) {
		return "asset-" + id + "-" + asset;
	}

	public static class CachedAsset implements Serializable {
		public String content;
		public long created;

		public CachedAsset(String content) {
			this.content = content;
			this.created = System.currentTimeMillis();
		}
	}
}
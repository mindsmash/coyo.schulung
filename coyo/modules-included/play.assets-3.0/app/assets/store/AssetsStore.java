package assets.store;

import assets.id.IdResolver;

/**
 * A store for assets. In this context, the "uid" is a generic string that is resolved through {@link IdResolver} and
 * that can be used as a discriminator for assets (f.e. package version or tenant id).
 * 
 * @author Sven Hoffmann, mindsmash GmbH
 * @author Jan Marquardt, mindsmash GmbH
 */
public interface AssetsStore {

	void lock(String uid, String asset, String expiration);

	void unlock(String uid, String asset);

	boolean isLocked(String uid, String asset);

	String read(String uid, String asset);

	void write(String uid, String asset, String content);

	boolean exists(String uid, String asset);

	void delete(String uid, String asset);

	void clearAll();

	void clear(String uid);

}

package assets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import play.Logger;
import play.Play;
import assets.handling.AssetsHandler;
import assets.handling.ScriptHandler;
import assets.handling.StylesheetHandler;

/**
 * Public interface for accessing asset URLs and clearing assets.
 * 
 * @author Jan Marquardt, Daniel Busch, mindsmash GmbH
 */
public class AssetsManager {

	/**
	 * Is asset management active. This variable is mainly used in the play template tags.
	 */
	public static final boolean ACTIVE = Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.assets.active",
			"false"));

	/**
	 * True to always re-generate assets from scratch (only works in DEV mode)
	 */
	public static final boolean DEBUG = Boolean.parseBoolean(Play.configuration.getProperty("mindsmash.assets.debug",
			"false")) && Play.mode.isDev();

	public static final ScriptHandler scriptHandler = new ScriptHandler();
	public static final StylesheetHandler stylesheetHandler = new StylesheetHandler();
	
	public static final String JS_SUFFIX = "js";
	public static final String CSS_SUFFIX = "css";

	/**
	 * Get the relative compressed script URL.
	 * 
	 * @param scripts
	 * @return
	 */
	public static String getCompressedScriptUrl(Collection<String> scripts) {
		return getCompressedScriptUrl(scripts, false);
	}

	/**
	 * Get the compressed script URL.
	 * 
	 * @param scripts
	 * @return
	 */
	public static String getCompressedScriptUrl(Collection<String> scripts, boolean absolute) {
		return scriptHandler.getCompressedFileUrl(scripts, absolute);
	}

	/**
	 * Get the relative compressed stylesheet URL.
	 * 
	 * @param styles
	 * @return
	 */
	public static String getCompressedStylesheetUrl(Collection<String> styles) {
		return getCompressedStylesheetUrl(styles, false);
	}

	/**
	 * Get the compressed script URL.
	 * 
	 * @return
	 */
	public static String getCompressedStylesheetUrl(Collection<String> styles, boolean absolute) {
		return stylesheetHandler.getCompressedFileUrl(styles, absolute);
	}

	/**
	 * Clear complete base directory and all generated files.
	 * 
	 * @throws IOException
	 */
	public static void clearAll() {
		if (AssetsHandler.store == null) {
			Logger.warn("[AssetsManager] Skip clearing all assets because the store is null");
			return;
		}

		AssetsHandler.store.clearAll();
	}

	/**
	 * Clear assets generated for the currently resolvable id.
	 * 
	 * @throws IOException
	 */
	public static void clearId() {
		if (AssetsHandler.store == null) {
			Logger.warn("[AssetsManager] Skip clearing assets because the store is null");
			return;
		}

		if (AssetsHandler.idResolver == null) {
			Logger.warn("[AssetsManager] Skip clearing assets because the id resolver is null");
			return;
		}

		AssetsHandler.store.clear(AssetsHandler.idResolver.resolve(JS_SUFFIX));
		AssetsHandler.store.clear(AssetsHandler.idResolver.resolve(CSS_SUFFIX));
	}
}

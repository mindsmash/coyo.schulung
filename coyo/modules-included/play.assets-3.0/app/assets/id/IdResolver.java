package assets.id;

/**
 * Resolver for a unique identifier for assets.
 * 
 * Implementation to be set in mindsmash.assets.idResolver property in the configuration. Based on the implementation an
 * ID for the asset compression is set.
 * 
 * @author Daniel Busch, mindsmash GmbH
 */
public interface IdResolver {

	/**
	 * Resolve a unique id.
	 * 
	 * @return
	 */
	String resolve(String assetType);
}

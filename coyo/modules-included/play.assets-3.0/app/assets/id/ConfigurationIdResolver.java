package assets.id;

import play.Play;

/**
 * Resolve the ID from the configuration mindsmash.assets.id property.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class ConfigurationIdResolver implements IdResolver {

	@Override
	public String resolve(String assetType) {
		return Play.configuration.getProperty("mindsmash.assets.id", "default");
	}
}

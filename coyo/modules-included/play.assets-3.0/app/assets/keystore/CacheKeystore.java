package assets.keystore;

import java.util.Collection;
import java.util.List;

import play.cache.Cache;
import exceptions.KeystoreException;

/**
 * Stores the key in the play default cache.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class CacheKeystore implements Keystore {

	@Override
	public void put(String key, Collection<String> assets) {
		Cache.set(key, assets);
	}

	@Override
	public Collection<String> get(String key) throws KeystoreException {
		Collection<String> result = Cache.get(key, Collection.class);
		if (null == result) {
			throw new KeystoreException("Cannot find assets for key : " + key);
		}
		return result;
	}

}

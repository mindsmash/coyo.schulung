package assets.keystore;

import java.util.Collection;
import java.util.List;

import exceptions.KeystoreException;

/**
 * Stores the assets key.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public interface Keystore {

	/**
	 * Set an asset key with a list of asset filenames.
	 * 
	 * @param key
	 * @param assets
	 */
	public void put(String key, Collection<String> assets);

	/**
	 * Get a list of asset filenames by key.
	 * 
	 * @param key
	 * @return
	 * @throws KeystoreException
	 */
	public Collection<String> get(String key) throws KeystoreException;
}

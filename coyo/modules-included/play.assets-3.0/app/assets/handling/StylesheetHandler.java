package assets.handling;

import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.lesscss.LessCompiler;

import play.templates.Template;
import play.templates.TemplateLoader;
import play.vfs.VirtualFile;
import theme.Theme;

/**
 * Handle stylesheet files.
 * 
 * @author Jan Marquardt, mindsmash GmbH
 * 
 */
public class StylesheetHandler extends AssetsHandler {

	private LessCompiler lessCompiler = new LessCompiler();

	public StylesheetHandler() {
		super("css", "text/css");
	}

	/**
	 * Directory to store compiled less files
	 */

	protected static Pattern cssUrlPattern = Pattern
			.compile("url[\\s]*\\([\\s]*['\"]?[\\s]*((?!/|'|\"|http).+?)[\\s]*['\"]?[\\s]*\\)");

	@Override
	protected String getAssetSourceFileName(String asset) {
		if (Theme.resourceExists(asset + ".less")) {
			return asset + ".less";
		}
		return asset + ".css";
	}

	protected String fixRelativeCssUrls(String css, String resourceUrl) {
		// fix relative URLs
		// remove file name from URL to get URL path
		String urlPath = resourceUrl.replaceAll("/[^/]*$", "");
		return cssUrlPattern.matcher(css).replaceAll("url(" + urlPath + "/$1)");
	}

	@Override
	protected String compileAsset(String asset) throws Exception {
		String fileName = getAssetSourceFileName(asset);

		String compiled = "";
		if (fileName.endsWith(".less")) {
			Template tmp = TemplateLoader.load(VirtualFile.open(getAssetSourceFile(asset)));
			compiled = lessCompiler.compile(tmp.render());
		} else {
			compiled = FileUtils.readFileToString(getAssetSourceFile(asset));
		}

		return fixRelativeCssUrls(compiled, getAssetSourceUrl(asset));
	}
}

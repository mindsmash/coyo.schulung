package assets.handling;

import injection.Inject;
import injection.InjectionSupport;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.libs.Codec;
import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.templates.JavaExtensions;
import theme.Theme;
import assets.AssetsManager;
import assets.id.ConfigurationIdResolver;
import assets.id.IdResolver;
import assets.keystore.CacheKeystore;
import assets.keystore.Keystore;
import assets.store.AssetsStore;
import assets.store.FileAssetsStore;
import exceptions.AssetLockedException;
import exceptions.AssetsHandlerException;

/**
 * Handler for text based assets.
 *
 * @author Daniel Busch, Jan Marquardt, mindsmash GmbH
 *
 */
@InjectionSupport
public abstract class AssetsHandler {

	@Inject(configuration = "mindsmash.assets.keystore", defaultClass = CacheKeystore.class)
	public static Keystore keystore;

	@Inject(configuration = "mindsmash.assets.idResolver", defaultClass = ConfigurationIdResolver.class)
	public static IdResolver idResolver;

	@Inject(configuration = "mindsmash.assets.store", defaultClass = FileAssetsStore.class)
	public static AssetsStore store;

	private static final int LOCK_MAX_TRIALS = 100;
	private static final int LOCK_TRIAL_INTERVAL = 100;
	private static final String LOCK_PERIOD = "10s";

	/**
	 * File suffix.
	 */
	public String suffix;

	/**
	 * File content/mime type.
	 */
	public String contentType;

	public AssetsHandler(String suffix, String contentType) {
		this.suffix = suffix;
		this.contentType = contentType;
	}

	/**
	 * Get the URL to the compressed file.
	 *
	 * @param assets
	 * @return
	 */
	public String getCompressedFileUrl(Collection<String> assets, boolean absolute) {
		String hash = buildFileHash(assets);
		keystore.put(hash, assets);
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("key", hash);
		ActionDefinition reverse = Router.reverse(controllers.Assets.class.getName() + "." + suffix, args);
		if (absolute) {
			reverse.absolute();
		}
		return reverse.url;
	}

	/**
	 * Build the file name hash.
	 *
	 * @param assets
	 * @return
	 */
	protected String buildFileHash(Collection<String> assets) {
		StringBuilder result = new StringBuilder();

		String id = idResolver.resolve(suffix);
		if (!StringUtils.isEmpty(id)) {
			result.append(id).append("-");
		}

		for (String asset : assets) {
			result.append(getAssetSourceUrl(asset)).append(",");
		}

		return Codec.hexMD5(result.toString());
	}

	/**
	 * Get the compressed asset files. Creates the merged asset file if it does not exist.
	 *
	 * @return
	 * @throws Exception
	 */
	public String getMergedAssets(String key) throws Exception {
		Collection<String> assets = keystore.get(key);

		if (null == assets) {
			throw new AssetsHandlerException("Assets for key " + key + " not found");
		}

		String id = idResolver.resolve(suffix);

		if (store.exists(id, key) && !AssetsManager.DEBUG) {
			return readAndWaitForLock(id, key);
		}

		store.lock(id, key, LOCK_PERIOD);

		// try to build complete merged string first
		StringBuilder builder = new StringBuilder();
		for (String asset : assets) {
			builder.append(getCompiledAsset(asset));
		}

		String merged = builder.toString();
		store.write(id, key, merged);
		store.unlock(id, key);

		return merged;
	}

	/**
	 * Compiles a single asset.
	 *
	 * @param asset
	 * @return
	 * @throws Exception
	 */
	public String getCompiledAsset(String asset) throws Exception {
		String key = JavaExtensions.slugify(getAssetSourceUrl(asset));
		if (!StringUtils.isEmpty(Theme.getActiveTheme())) {
			key = Theme.getActiveTheme() + "-" + key;
		}

		String id = idResolver.resolve(suffix);

		if (store.exists(id, key) && !AssetsManager.DEBUG) {
			return readAndWaitForLock(id, key);
		}

		// lock
		store.lock(id, key, LOCK_PERIOD);

		try {
			Logger.trace("compiling asset: %s", asset);
			String compiled = compileAsset(asset);
			store.write(id, key, compiled);
			return compiled;
		} catch (Exception e) {
			// delete file on failure
			store.delete(id, key);
			throw new AssetsHandlerException("Cannot compile asset: " + asset, e);
		} finally {
			// release lock
			store.unlock(id, key);
		}
	}

	private String readAndWaitForLock(String id, String key) throws Exception {
		// check for lock and wait
		int trials = 0;
		while (store.isLocked(id, key)) {
			Thread.sleep(LOCK_TRIAL_INTERVAL);

			if (trials++ > LOCK_MAX_TRIALS) {
				throw new AssetLockedException("asset " + key + " was locked for too long. cancelling");
			}
		}

		return store.read(id, key);
	}

	/**
	 * Get the file name of an asset.
	 *
	 * @param asset
	 * @return
	 */
	abstract protected String getAssetSourceFileName(String asset);

	/**
	 * Get the resource for a given asset.
	 *
	 * @param asset
	 * @return
	 */
	protected File getAssetSourceFile(String asset) {
		return Theme.getResource(getAssetSourceFileName(asset));
	}

	/**
	 * Get a file resource URL.
	 *
	 * @param asset
	 * @return
	 */
	public String getAssetSourceUrl(String asset) {
		return Theme.getResourceUrl(getAssetSourceFileName(asset));
	}

	/**
	 * Compiles the a given asset into the target file.
	 *
	 * @param asset
	 * @return
	 */
	abstract protected String compileAsset(String asset) throws Exception;
}

package assets.handling;

import java.io.IOException;

import logging.PlayErrorManager;

import org.apache.commons.io.FileUtils;

import play.Play;
import theme.Theme;

import com.google.javascript.jscomp.CompilationLevel;
import com.google.javascript.jscomp.Compiler;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.SourceFile;

/**
 * Handle Javascript assets.
 * 
 * @author Daniel Busch, mindsmash GmbH
 * 
 */
public class ScriptHandler extends AssetsHandler {

	/**
	 * Is js compression active
	 */
	private static final boolean COMPRESS = Boolean.parseBoolean(Play.configuration.getProperty(
			"mindsmash.assets.jscompression", "false"));

	public ScriptHandler() {
		super("js", "application/javascript");
	}

	@Override
	protected String getAssetSourceFileName(String asset) {
		return asset + "." + suffix;
	}

	@Override
	public String getCompiledAsset(String asset) throws Exception {
		if (!COMPRESS) {
			return FileUtils.readFileToString(Theme.getResource(getAssetSourceFileName(asset)));
		}
		return super.getCompiledAsset(asset);
	}

	@Override
	protected String compileAsset(String asset) throws IOException {
		Compiler c = new Compiler(new PlayErrorManager());
		CompilerOptions options = new CompilerOptions();
		CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(options);
		c.compile(SourceFile.fromCode("extern.js", ""),
				SourceFile.fromFile(Theme.getResource(getAssetSourceFileName(asset))), options);
		return c.toSource();
	}
}

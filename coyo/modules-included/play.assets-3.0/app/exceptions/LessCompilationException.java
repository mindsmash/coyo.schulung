package exceptions;

import play.exceptions.PlayException;

public class LessCompilationException extends PlayException {

	public LessCompilationException() {
		super();
	}

	public LessCompilationException(String message) {
		super(message);
	}

	public LessCompilationException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getErrorTitle() {
		return "LESS compilation error";
	}

	@Override
	public String getErrorDescription() {
		return getMessage();
	}

}

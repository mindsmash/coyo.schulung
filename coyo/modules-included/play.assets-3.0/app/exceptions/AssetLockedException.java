package exceptions;

public class AssetLockedException extends Exception {

	public AssetLockedException() {
		super();
	}

	public AssetLockedException(String message) {
		super(message);
	}

	public AssetLockedException(String message, Throwable cause) {
		super(message, cause);
	}
}

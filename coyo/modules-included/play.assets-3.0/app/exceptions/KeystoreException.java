package exceptions;

import play.exceptions.PlayException;

public class KeystoreException extends PlayException {

	public KeystoreException() {
		super();
	}

	public KeystoreException(String message) {
		super(message);
	}

	public KeystoreException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getErrorTitle() {
		return "Asset handling error";
	}

	@Override
	public String getErrorDescription() {
		return getMessage();
	}

}

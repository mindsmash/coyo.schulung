package exceptions;

import play.exceptions.PlayException;

public class JavaScriptCompressionException extends PlayException {

	public JavaScriptCompressionException() {
		super();
	}

	public JavaScriptCompressionException(String message) {
		super(message);
	}

	public JavaScriptCompressionException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getErrorTitle() {
		return "Java compression error";
	}

	@Override
	public String getErrorDescription() {
		return getMessage();
	}

}

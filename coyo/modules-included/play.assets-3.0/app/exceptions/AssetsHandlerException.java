package exceptions;

import play.exceptions.PlayException;

public class AssetsHandlerException extends PlayException {

	public AssetsHandlerException() {
		super();
	}

	public AssetsHandlerException(String message) {
		super(message);
	}

	public AssetsHandlerException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getErrorTitle() {
		return "Asset handling error";
	}

	@Override
	public String getErrorDescription() {
		return getMessage();
	}

}

package tags;

import groovy.lang.Closure;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import play.mvc.Router;
import play.mvc.Router.ActionDefinition;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import theme.Theme;
import assets.AssetsManager;

public class AssetsFastTags extends FastTags {

	private static ThreadLocal<Set<String>> scripts = new ThreadLocal<>();
	private static ThreadLocal<Set<String>> css = new ThreadLocal<>();
	private static ThreadLocal<Set<String>> less = new ThreadLocal<>();

	public static void _addScript(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		String dir;
		if (args.containsKey("dir")) {
			dir = (String) args.get("dir");
		} else {
			dir = "scripts";
		}

		if (scripts.get() == null) {
			scripts.set(new LinkedHashSet<String>());
		}
		scripts.get().add(dir + File.separator + args.get("arg"));
	}

	public static void _loadScripts(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		if (scripts.get() == null) {
			return;
		}

		if (!AssetsManager.ACTIVE) {
			for (String item : scripts.get()) {
				out.println("<script type=\"text/javascript\" src=\"" + Theme.getResourceUrl(item + ".js")
						+ "\"></script>");
			}
		} else {
			out.println("<script type=\"text/javascript\" src=\"" + AssetsManager.getCompressedScriptUrl(scripts.get())
					+ "\"></script>");
		}
		scripts.remove();
	}

	public static void _addCss(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String dir;
		if (args.containsKey("dir")) {
			dir = (String) args.get("dir");
		} else {
			dir = "stylesheets";
		}

		if (css.get() == null) {
			css.set(new LinkedHashSet<String>());
		}
		css.get().add(dir + File.separator + args.get("arg"));
	}

	public static void _loadCss(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (css.get() == null) {
			return;
		}

		for (String item : css.get()) {
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + Theme.getResourceUrl(item + ".css")
					+ "\">");
		}
		css.remove();
	}

	public static void _addLess(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String dir;
		if (args.containsKey("dir")) {
			dir = (String) args.get("dir");
		} else {
			dir = "stylesheets";
		}

		if (less.get() == null) {
			less.set(new LinkedHashSet<String>());
		}
		less.get().add(dir + File.separator + args.get("arg"));
	}

	public static void _loadLess(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
			int fromLine) {
		if (less.get() == null) {
			return;
		}

		if (!AssetsManager.ACTIVE) {
			for (String item : less.get()) {
				if (Theme.resourceExists(item + ".less")) {
					Map<String, Object> map = new HashMap<>();
					map.put("path", item);
					ActionDefinition ad = Router.reverse("Assets.less", map);
					out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ad.url + "\">");
				} else {
					out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\""
							+ Theme.getResourceUrl(item + ".css") + "\">");
				}
			}
		} else {
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\""
					+ AssetsManager.getCompressedStylesheetUrl(less.get()) + "\">");
		}
		less.remove();
	}

	public static void clearThreadLocals() {
		scripts.remove();
	}
}
